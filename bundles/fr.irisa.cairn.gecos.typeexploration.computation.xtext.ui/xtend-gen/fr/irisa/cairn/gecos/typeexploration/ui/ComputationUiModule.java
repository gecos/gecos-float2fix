/**
 * generated by Xtext 2.14.0
 */
package fr.irisa.cairn.gecos.typeexploration.ui;

import fr.irisa.cairn.gecos.typeexploration.ui.AbstractComputationUiModule;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.xtend.lib.annotations.FinalFieldsConstructor;

/**
 * Use this class to register components to be used within the Eclipse IDE.
 */
@FinalFieldsConstructor
@SuppressWarnings("all")
public class ComputationUiModule extends AbstractComputationUiModule {
  public ComputationUiModule(final AbstractUIPlugin plugin) {
    super(plugin);
  }
}
