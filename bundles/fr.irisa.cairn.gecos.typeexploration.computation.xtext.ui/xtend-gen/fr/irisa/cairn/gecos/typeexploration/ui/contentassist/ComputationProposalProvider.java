/**
 * generated by Xtext 2.14.0
 */
package fr.irisa.cairn.gecos.typeexploration.ui.contentassist;

import fr.irisa.cairn.gecos.typeexploration.ui.contentassist.AbstractComputationProposalProvider;

/**
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#content-assist
 * on how to customize the content assistant.
 */
@SuppressWarnings("all")
public class ComputationProposalProvider extends AbstractComputationProposalProvider {
}
