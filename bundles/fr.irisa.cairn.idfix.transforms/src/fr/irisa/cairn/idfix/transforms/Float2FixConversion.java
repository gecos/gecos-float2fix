package fr.irisa.cairn.idfix.transforms;

import java.io.IOException;

import org.apache.logging.log4j.Logger;

import fr.irisa.cairn.gecos.model.c.generator.XtendCGenerator;
import fr.irisa.cairn.idfix.backend.AbstractFixedPointBackendModel;
import fr.irisa.cairn.idfix.backend.FixedPtSpecificationApplier;
import fr.irisa.cairn.idfix.backend.c.CFunctionsFixedPointBackend;
import fr.irisa.cairn.idfix.backend.c.InlineCFixedPointBackend;
import fr.irisa.cairn.idfix.backend.cpp.ACFixedBackend;
import fr.irisa.cairn.idfix.backend.cpp.SCFixedBackend;
import fr.irisa.cairn.idfix.evaluation.IDFixEvaluation;
import fr.irisa.cairn.idfix.evaluation.IDFixEvaluation.EvaluationType;
import fr.irisa.cairn.idfix.frontend.IDFixConvFrontEnd;
import fr.irisa.cairn.idfix.model.factory.IDFixUserInformationAndTimingFactory;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixProject;
import fr.irisa.cairn.idfix.model.informationandtiming.Section;
import fr.irisa.cairn.idfix.model.userconfiguration.RUNTIME_MODE;
import fr.irisa.cairn.idfix.optimization.IDFixConvOptimization;
import fr.irisa.cairn.idfix.optimization.extension.cost.DefaultCostProvider;
import fr.irisa.cairn.idfix.optimization.extension.cost.ICostProvider;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.backend.OutputNoiseVerificationSimulator;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.logging.IDFixLogger;
import gecos.gecosproject.GecosProject;

/**
 * Float to fix conversion pass.
 * Convert the c source code specified in float data type provide by the user to a c source code specified in fixed data type.
 * @author Nicolas Simon
 * @date Apr 12, 2013
 */
public class Float2FixConversion {
	
	protected IdfixProject _idfixProject;
	protected String _noiseConstraint;
	protected String _operatorLibraryPath;
	protected int _optimAlgorithm;
	protected int _nbSimulation;
	
	public Float2FixConversion(IdfixProject idfixProject, String noiseConstraint, String operatorLibraryPath, int optimAlgorithm, int nbSimulation){
		_idfixProject = idfixProject;
		_noiseConstraint = noiseConstraint;
		_operatorLibraryPath = operatorLibraryPath;
		_optimAlgorithm = optimAlgorithm;
		_nbSimulation = nbSimulation;
	}
	
	public GecosProject compute() {
		Logger logger = IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_GLOBAL);
		if(_idfixProject.getUserConfiguration().getRuntimeMode() == RUNTIME_MODE.DEBUG){
			logger.info("DEBUG mode activated");
			logger.info("   - More log available");
			logger.info("   - Compile and run binary in debug mode");
			logger.info("   - IDFixEval generate more graph");
			logger.info("   - Information about system output generate by the noise function");
			logger.info("   - Information about system output generate by the noise simulator\n");
		}
		else if(_idfixProject.getUserConfiguration().getRuntimeMode() == RUNTIME_MODE.TRACE){
			logger.info("TRACE mode activated");
			logger.info("   - More log available");
			logger.info("   - Compile and run binary in debug/trace mode");
			logger.info("   - IDFixEval generate more graph");
			logger.info("   - Many Information about system in general generate by the noise function (very slow in the optimization part)");
			logger.info("   - Information about system output generate by the noise simulator\n");
		}
		
		frontEnd();
		
		idfixEval();
			
		wlo();
		
		// TODO need to finish this part
		if(_idfixProject.getUserConfiguration().backendSimulationIsActivated()){
			if(new OutputNoiseVerificationSimulator(_idfixProject).simulate());
				_idfixProject.plotSaveInformations();
		}
		
		backEnd();
		
		if(_idfixProject.getUserConfiguration().isGenerateSummary())
			_idfixProject.generateLogInformations();
		
		_idfixProject.finalize();
		if(_idfixProject.getUserConfiguration().isOptimizationGraphDisplay()){
			try{
				System.out.println();
				System.out.println("Press \"enter\" to finish the script");
				System.out.println("Press \"enter\" to finish the script");
				System.out.println("Press \"enter\" to finish the script");
				System.in.read();
			} catch (IOException e){
				throw new RuntimeException(e);
			}
		}
		
		return _idfixProject.getGecosProjectAnnotedAndNotUnrolled(); 
	}

	/**
	 * FrontEnd part
	 * - Annotate operators, datas, affectations
	 * - Flatten the source code
	 * - Generate the SFG file
	 */
	protected void frontEnd() {
		new IDFixConvFrontEnd(_idfixProject, _nbSimulation).compute();
	}	
	
	/**
	 *  Run the idfix eval binary.
	 *  - Determine the dynamic of operators and datas
	 * 	- Generate the C noise library
	 */
	protected void idfixEval() {
		new IDFixEvaluation(_idfixProject, EvaluationType.ALL).compute();
	}
	
	/**
	 * Create the cost provider used in the optimization part
	 * @return
	 */
	protected ICostProvider createCostProvider(){
		return new DefaultCostProvider(_idfixProject.getFixedPointSpecification());
	}
	
	/**
	 * Run the wordlength optimization process.
	 */
	protected void wlo() {
		ICostProvider costProvider = this.createCostProvider();
		IDFixConvOptimization optim = new IDFixConvOptimization(_idfixProject, costProvider, _noiseConstraint, _operatorLibraryPath, _optimAlgorithm);
		optim.compute();
	}
	
	/**
	 * Apply the fixed-point specification to the original CDFG.
	 * Generate fixed-point code.
	 */
	protected void backEnd() {
		long timingStart,  timingStop, sectionTimingSart, sectionTimingStop;
		Section backend = IDFixUserInformationAndTimingFactory.SECTION("BackEnd");
		_idfixProject.getProcessInformations().getTiming().getSections().add(backend);
		sectionTimingSart = System.currentTimeMillis();
		
		timingStart = System.currentTimeMillis();
		new FixedPtSpecificationApplier(_idfixProject).compute();
		AbstractFixedPointBackendModel fixedPtBackend = getGenerator(_idfixProject.getGecosProjectAnnotedAndNotUnrolled());
		fixedPtBackend.compute();
		timingStop = System.currentTimeMillis();
		backend.addTimeLog("data type transformation", timingStart, timingStop);
	
		new XtendCGenerator(_idfixProject.getGecosProjectAnnotedAndNotUnrolled(), _idfixProject.getOutputFolderPath()).compute();
		
		sectionTimingStop = System.currentTimeMillis();
		backend.setTime((sectionTimingStop - sectionTimingSart) / 1000f);
	}

	protected AbstractFixedPointBackendModel getGenerator(GecosProject proj) {
		switch(_idfixProject.getUserConfiguration().getDataTypeRegneration()) {
		case AC_FIXED:
			return new ACFixedBackend(proj);
		case SC_FIXED:
			return new SCFixedBackend(proj);
		case CINLINE:
			return new InlineCFixedPointBackend(proj);
		case CMACROS:
			return new CFunctionsFixedPointBackend(proj); //XXX
		default:
			throw new RuntimeException("Not supported yet for " + _idfixProject.getUserConfiguration().getDataTypeRegneration());
		}	
	}
}

