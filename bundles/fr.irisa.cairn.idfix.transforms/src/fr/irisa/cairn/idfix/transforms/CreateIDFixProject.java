package fr.irisa.cairn.idfix.transforms;

import fr.irisa.cairn.gecos.model.cdtfrontend.CDTFrontEnd;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.idfix.model.factory.IDFixUserIDFixProjectFactory;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixProject;
import gecos.gecosproject.GecosProject;

import java.io.File;

/**
 * Gecos script command let to create an IDFIX project
 * 
 * @author nicolas simon
 * @date 22/09/14
 */
public class CreateIDFixProject {
	private String _pathCFile = null;
	private GecosProject _gecosProject = null;
	private String _pathOutputFolder;
	private String _configurationFilePath = null;
	
	public CreateIDFixProject(String pathCFile, String pathOutputFolder){
		this(pathCFile, pathOutputFolder, null);
	}
	
	public CreateIDFixProject(String pathCFile, String pathOutputFolder, String configurationFilePath){
		_pathCFile = pathCFile;
		_pathOutputFolder = pathOutputFolder;
		_configurationFilePath = configurationFilePath;
	}
	
	public CreateIDFixProject(GecosProject gecosProject, String pathOutputFolder){
		this(gecosProject, pathOutputFolder, null);
	}
	
	public CreateIDFixProject(GecosProject gecosProject, String pathOutputFolder, String configurationFilePath){
		_gecosProject = gecosProject;
		_pathOutputFolder = pathOutputFolder;
		_configurationFilePath = configurationFilePath;
	}
	
	public IdfixProject compute(){
		if(_gecosProject == null){
			_gecosProject = GecosUserCoreFactory.project("IDFixProject", _pathCFile);
		}
		new CDTFrontEnd(_gecosProject).compute();
		if(_configurationFilePath != null)
			return IDFixUserIDFixProjectFactory.IDFIXPROJECT(_gecosProject, _pathOutputFolder, new File(_configurationFilePath).toPath());
		else
			return IDFixUserIDFixProjectFactory.IDFIXPROJECT(_gecosProject, _pathOutputFolder);
	}
}
