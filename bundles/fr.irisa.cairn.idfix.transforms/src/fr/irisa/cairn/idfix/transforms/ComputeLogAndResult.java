package fr.irisa.cairn.idfix.transforms;


/**
 * Class appelé via script Gecos.
 * Permet de générer une documentation (dans le dossier de sortie) a partir des différentes informations issue de IDFixConv et IDFixEval 
 * 
 * @author nicolas
 *
 */
public class ComputeLogAndResult {

//	private ProcedureSet _ps;
//	private final FixPtSpecif _fixPtSpecif;
//	private String _outputDirPath;
//	private LogTiming _logTiming;
//	
//	private FileWriter _texFile;
//	private BufferedWriter _bufferTexFile;
//	private File _tempDir;
//	private String _pathTempDir;
//	private String _pathTempFile;
//	
//	public ComputeLogAndResult(ProcedureSet ps, FixPtSpecif fixPtSpecif, String outputDirPath, LogTiming log){
//		_ps = ps;
//		_fixPtSpecif = fixPtSpecif;
//		_logTiming = log;
//		_outputDirPath = outputDirPath;
//		
//		_pathTempDir = outputDirPath + ConstantPathAndName.LOG_TEMPORARY_FOLDER_NAME;
//		_pathTempFile = _pathTempDir + ConstantPathAndName.LOG_FILE_NAME;
//		_texFile = null;
//		_bufferTexFile = null;
//		_tempDir = null;
//	}
//	
//	public void compute() throws ComputeLogAndResultException {
//		try {	
//			// Création du dossier temporaire utilisé au niveau de la création du .tex et des .ps
//			this._tempDir = new File(this._pathTempDir);
//			if(!_tempDir.mkdir()){
//				System.out.println("*** Notice : .tmp/ folder deleted now due to a interruption of the last execution");
//				deleteDirectory(_tempDir);
//				if(!_tempDir.mkdir())
//					throw new FilerException("Temporary directory not created");
//			}
//			
//			this._texFile = new FileWriter(_pathTempFile+".tex", true);
//			this._bufferTexFile = new BufferedWriter(_texFile);		
//			
//			
//			creationFileAndHeader();
//		//	dotCreationFromProcedureSet();
//		//	dotToPsConversion();
//		//	writeGrahProcedureCode();
//			writeTimingIDFix();
////			writeIDFixConvInformation(); // FIXME need to debug latex code here 
////			writeIDFixEvalInformation();
//			this._bufferTexFile.write("\\end{document}\n");
//			this._bufferTexFile.close();
//		
//			compileTexFile();
//			
//			// Suppression des fichiers temporaire et du dossier temporaire
//			deleteDirectory(_tempDir);
//
//		} catch (IOException e) {
//			throw new ComputeLogAndResultException(e);
//		} catch (InterruptedException e) {
//			throw new ComputeLogAndResultException(e);
//		}
//	}
//	
//	
//	/**
//	 * Delete files and sub director of the param
//	 * 
//	 * @author nicolas Simon
//	 * @param directory
//	 * @throws FilerException
//	 */
//	
//	private void deleteDirectory(File directory) throws FilerException{
//		if(directory.isDirectory()){
//			File[] listDirFile = directory.listFiles();
//			for(int i = 0 ; i < listDirFile.length ; i++){
//				if(listDirFile[i].isFile()) {
//					if(!listDirFile[i].delete())
//						throw new FilerException("Problem to delete " + listDirFile[i].getName());
//				}
//				else {
//					deleteDirectory(listDirFile[i]);
//				}
//			}
//			
//			if(!directory.delete())
//				throw new FilerException("Problem to delete "+directory.getName());			
//			
//		}
//		else{
//			throw new FilerException(directory.getName() + "is not a directory");
//		}
//		
//	}
//
//	/**
//	 * Create the output file and copy contents of header.tex resource file
//	 * 
//	 * @author nicolas Simon
//	 * 
//	 */
//	private void creationFileAndHeader() throws IOException {
//		File logAndResult = new File(this._pathTempFile+".tex");
//		if(!logAndResult.exists()){
//			logAndResult.createNewFile();
//		}			
//		
//		FileWriter fw = new FileWriter(logAndResult);
//		BufferedWriter bw = new BufferedWriter(fw);
//		PrintWriter fichierSortie = new PrintWriter(bw); 
//		
//		InputStream inputStream = new FileInputStream(ConstantPathAndName.GEN_RESOURCES_FOLDER_PATH + "header.tex");
//		InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//		BufferedReader bufferReader = new BufferedReader(inputStreamReader);
//		String ligne;
//		
//		while ((ligne = bufferReader.readLine()) != null) {
//			fichierSortie.println(ligne);
//		}
//		bufferReader.close();
//		fichierSortie.close();		
//	}
//	
//	/**
//	 * Create dot procedure file from the different procedure of the system. Create the application callgraph
//	 * 
//	 * @author nicolas simon
//	 */
//	@SuppressWarnings("unused")
//	private void dotCreationFromProcedureSet() throws FilerException, FileNotFoundException{
//		File tempDirDot = new File(this._pathTempDir + "dot/");
//		if(!tempDirDot.mkdir()){
//			System.out.println("*** Notice : .tmp/dot/ folder deleted now due to a interruption of the last execution");
//			deleteDirectory(tempDirDot);
//			if(!tempDirDot.mkdir())
//				throw new FilerException("Temporary directory not created");
//		}
//		
//		GecosDotPrinter modelDotty = new GecosDotPrinter(this._pathTempDir+"dot/", this._ps);
//		modelDotty.compute();
//		
//		if(this._ps.listProcedures().size() > 1){
//			CallGraphBuilder callGraphBuilder = new CallGraphBuilder(_ps);
//			String pathCallGraphFile = this._pathTempDir + "dot/callGraph.dot";
//			PrintStream stream = new PrintStream(pathCallGraphFile);
//			CallGraph callGraph = callGraphBuilder.compute();
//			callGraph.outputDot(stream);
//		}
//	}
//	
//	
//	
//	/**
//	 * 	Create a temporary directory to stock ps converted from dot procedure file
//	 * 	
//	 * @author nicolas Simon
//	 * @throws IOException 
//	 * @throws InterruptedException 
//	 */
//	@SuppressWarnings("unused")
//	private void dotToPsConversion() throws IOException, InterruptedException {
//		
//		File directoryDotGraph = new File(this._pathTempDir + "dot/");
//		File tempDirEps = new File(this._pathTempDir + "eps/");
//		File[] listFile = null;
//		Process p;
//		
//		if(!tempDirEps.mkdir()){
//			System.out.println("*** Notice : .tmp/eps/ folder deleted now due to a interruption of the last execution");
//			deleteDirectory(tempDirEps);
//			if(!tempDirEps.mkdir())
//				throw new FilerException("Temporary directory not created");
//		}
//		
//		
//		if (directoryDotGraph.isDirectory()) {
//			listFile = directoryDotGraph.listFiles();
//		} 
//		else {
//			throw new FilerException(this._pathTempDir + "dot/" + " is not a directory");
//		}
//				
//		if (listFile != null) {
//			for (int i = 0; i < listFile.length; i++) {
//				if (listFile[i].isFile()) {
//						
//						String[] splitExt = listFile[i].getName().split("\\.");
//						if(splitExt.length != 2){
//							throw new FilerException("Splitting error of dot files");
//						}
//												
//						String cmd = "dot -Teps " + listFile[i].getAbsolutePath() + " -o " + _pathTempDir + "eps/" + splitExt[0] + ".eps";
//						p = Runtime.getRuntime().exec(cmd);
//						p.waitFor();
//						BufferedReader reader=new BufferedReader(new InputStreamReader(p.getInputStream())); 
//						String line=reader.readLine(); 
//						while(line!=null) 
//						{ 
//							System.out.println(line); 
//							line=reader.readLine(); 
//						} 
//				}
//				else{
//					System.err.println("WARNING : Directory in " + this._pathTempDir + "dot/");
//				}
//			}
//		}
//		else{
//			throw new FilerException("Dot files missing");
//		}
//	}
//
//	
//	
//	/**
//	 * Write the chapter "Procedures CDFG". procedure eps file are writen.
//	 * 
//	 * @author nicolas simon
//	 * @throws IOException
//	 */
//	@SuppressWarnings("unused")
//	private void writeGrahProcedureCode() throws IOException {
//		this._bufferTexFile.write("\\chapter{CDFG procedures}\n");
//		File tempDirEps = new File(this._pathTempDir + "eps/");
//		if(!tempDirEps.exists()){
//			throw new FilerException("Folder not found");
//		}
//		
//		File[] listTempDirFile = tempDirEps.listFiles();
//		for(int i = 0 ; i < listTempDirFile.length ; i++){
//			if(listTempDirFile[i].isFile()){
//				this._bufferTexFile.write("\\begin{figure}[!h]\n\\begin{center}\n\\includegraphics[width=0.75\\textwidth]{"+this._pathTempDir+"eps/"+ listTempDirFile[i].getName() +"}\n\\end{center}\n\\caption{ Procedure : " + listTempDirFile[i].getName() + "}\n\\end{figure} \n");
//			}
//		}
//	}
//	
//	/**
//	 * Use the logTiming structure and write timing of IDFixConv and IDFixEval into a table
//	 * 
//	 * @auhtor nicolas simon
//	 * @throws IOException
//	 */
//	private void writeTimingIDFix() throws IOException {		
//		boolean color_font = true;
//		//double timingTotal = 0.0;
//		String runIDFix = "IDFixEval";
//		DecimalFormat decFormat = new DecimalFormat("0.000");
//		
//		LogTiming logTimingIDFixConv = _logTiming;
//		
//		this._bufferTexFile.write("\\chapter{IDFix timing}\n");
//		this._bufferTexFile.write("\\begin{table}[h!]\n\\begin{scriptsize}\n\\begin{center}\n\\arrayrulecolor{color_cadre}\n\\begin{tabular}{ll||ll}\n\\hline\n");
//		this._bufferTexFile.write("\\multicolumn{2}{c||}{\\tmenu{IDFixConv timing}} & \\multicolumn{2}{c}{\\tmenu{IDFixEval Timing}} \\\\\\hline");
//		
//		for(LogTimingType sectionIDFixConv : logTimingIDFixConv.getListTiming()){
//			if(color_font){
//				this._bufferTexFile.write("\\rowcolor{color_fond}  \\ttab{\\textbf{"+ sectionIDFixConv.getSection() +"}}  &  \\ttab{"+ decFormat.format(sectionIDFixConv.getTiming()) +"}   &  \\ttab{}  &  \\ttab{}\\\\");
//			}
//			else{
//				this._bufferTexFile.write("\\ttab{\\textbf{"+ sectionIDFixConv.getSection() +"}}  &  \\ttab{"+ decFormat.format(sectionIDFixConv.getTiming()) +"}   &  \\ttab{}  &  \\ttab{}\\\\");
//			}
//			color_font = !color_font;
//			//timingTotal += sectionIDFixConv.getTiming();
//			
//			if(sectionIDFixConv.getSection().equals(runIDFix)){
//				LogTiming logTimingIDFixEval = LogTimingIO.loadFixPtSpecif(_outputDirPath + ConstantPathAndName.IDFIX_EVAL_OUTPUT_FOLDER_NAME + "timing_IDFixEval.xml");
//				for(LogTimingType sectionIDFixEval : logTimingIDFixEval.getListTiming()){
//					if(sectionIDFixEval.getTiming() == -1){
//						if(color_font){
//							this._bufferTexFile.write("\\rowcolor{color_fond}  \\ttab{}  &  \\ttab{}   &  \\ttab{}  &  \\ttab{}\\\\ ");
//						}
//						else{
//							this._bufferTexFile.write("\\ttab{}  &  \\ttab{}   &  \\ttab{}  &  \\ttab{}\\\\ ");
//						}
//					}					
//					else{
//						if(color_font){
//							this._bufferTexFile.write("\\rowcolor{color_fond}  \\ttab{}  &  \\ttab{}   &  \\ttab{\\textbf{"+ sectionIDFixEval.getSection() +"}}  &  \\ttab{"+ decFormat.format(sectionIDFixEval.getTiming()) +"}\\\\ ");
//						}
//						else{
//							this._bufferTexFile.write("\\ttab{}  &  \\ttab{}   &  \\ttab{\\textbf{"+ sectionIDFixEval.getSection() +"}}  &  \\ttab{"+ decFormat.format(sectionIDFixEval.getTiming()) +"}\\\\ ");
//						}
//					}
//					color_font = !color_font;
//				}
//			}
//			this._bufferTexFile.write("\\hline");
//			
//		}
//		//this.bufferTexFile.write("\\multicolumn{4}{c}{Total timing : " + decFormat.format(timingTotal) +"}\\\\\\hline\n");
//		this._bufferTexFile.write("\n\\end{tabular}\n\\end{center}\n\\end{scriptsize}\n\\caption{Timing IDFix-Conv + IDFix-Eval}\n\\end{table}\n");
//		this._bufferTexFile.write("\\textit{Notation : The system have not been changed since the last execution. Do not necessary to rerun IDFixEval}\n");
//	}
//	
//	/**
//	 * Write important information from IDFixEval 
//	 * 	- User noise constraint
//	 *  - Kind of optimisation used
//	 *  - Noise obtained for the solution
//	 *  - Solution cost
//	 *  - Triplet obtained
//	 * 
//	 * @author nicolas simon
//	 * @throws IOException
//	 */
//	@SuppressWarnings("unused")
//	private void writeIDFixConvInformation() throws IOException{
//		boolean color_font = true;
//		this._bufferTexFile.write("\\chapter{IDFixConv informations}\n");
//		
//		this._bufferTexFile.write("\\textit{Notation : \n");
//		this._bufferTexFile.write("\\begin{itemize}");
//		this._bufferTexFile.write("\\item wd represent the total number of bit");
//		this._bufferTexFile.write("\\item wip represent the number of bit of the integer plus sign bit");
//		this._bufferTexFile.write("\\item s represent if the data is signed or not");
//		this._bufferTexFile.write("\\end{itemize}");
//		this._bufferTexFile.write("}\\\\ \\ \\\\");
//		
//		this._bufferTexFile.write("\\begin{itemize}");
//		this._bufferTexFile.write("\\item User constraint : " + _logTiming.getUserConstraint()+ " db\n");
//		this._bufferTexFile.write("\\item Optimisation used : " + _logTiming.getTypeOptiUsed()+ "\n");
//		this._bufferTexFile.write("\\item Noise obtained for the solution (" + _logTiming.getNbEvalPb() + " iteration) : " + _logTiming.getNoiseResult()+ " db\n");
//		this._bufferTexFile.write("\\item Solution cost (" + _logTiming.getNbEvalCost() +" iterations) : " + _logTiming.getCostResult()+ "\n");
//		this._bufferTexFile.write("\\end{itemize}\\\\");
//		
//		// Tableau des informations pour les données
//		color_font = true;
//		this._bufferTexFile.write("\\begin{table}[h!]\n\\begin{scriptsize}\n\\begin{center}\n\\arrayrulecolor{color_cadre}\n\\begin{tabular}{l|l|l}\n\\hline\n");
//		this._bufferTexFile.write("\\multicolumn{1}{c|}{\\tmenu{IDFixEval CDFG number}} & \\multicolumn{1}{c|}{\\tmenu{AcFixed Specif}} & \\multicolumn{1}{c}{\\tmenu{Dynamic range}} \\\\\\hline");
//		for(DataId dataID : _fixPtSpecif.getListFixPtType().keySet()){
//			FixPtType fixPtType = _fixPtSpecif.getListFixPtType().get(dataID);
//			if(color_font){
//				this._bufferTexFile.write("\\rowcolor{color_fond}  \\ttab{"+ dataID.getName() +" ("+ dataID.getNum() +")}  &  \\ttab{wd : "+ fixPtType.getTypeAcFixed().getWd() + ", wip : " + fixPtType.getTypeAcFixed().getWip() + ", s : " + fixPtType.getTypeAcFixed().isS() +"} & \\ttab{["+fixPtType.getDyn().getLowerBound() + ", " + fixPtType.getDyn().getUpperBound() + "]}\\\\ ");
//			}
//			else{
//				this._bufferTexFile.write("\\ttab{"+ dataID.getName() +" ("+ dataID.getNum() +")}  &  \\ttab{wd : "+ fixPtType.getTypeAcFixed().getWd() + ", wip : " + fixPtType.getTypeAcFixed().getWip() + ", s : " + fixPtType.getTypeAcFixed().isS() +"} & \\ttab{["+fixPtType.getDyn().getLowerBound() + ", " + fixPtType.getDyn().getUpperBound() + "]}\\\\ ");
//			
//			}
//			color_font = !color_font;
//		}
//		this._bufferTexFile.write("\\hline\n\\end{tabular}\n\\end{center}\n\\end{scriptsize}\n\\caption{Data informations}\n\\end{table}");
//		
//		// Tableau des informations pour les opérateurs
//		this._bufferTexFile.write("\\begin{table}[h!]\n\\begin{scriptsize}\n\\begin{center}\n\\arrayrulecolor{color_cadre}\n\\begin{tabular}{l|l|l}\n\\hline\n");
//		this._bufferTexFile.write("\\multicolumn{1}{c|}{\\tmenu{Ops}} & \\multicolumn{1}{c|}{\\tmenu{Triplet obtained}} & \\multicolumn{1}{c}{\\tmenu{Dynamic range}} \\\\\\hline");
//		for(FixPtOpsSpecif fixPtOps :_fixPtSpecif.getListFixPtOpsSpecif()){
//			if(color_font){
//				this._bufferTexFile.write("\\rowcolor{color_fond}  \\ttab{"+ AnnotateOperators.getOpCDFGName(AnnotateOperators.getNumOpCDFG(Integer.parseInt(fixPtOps.getNumOp()))) +" ("+ fixPtOps.getNumOp() +")}  & \\ttab{wd : "+ fixPtOps.getInput1().getTypeAcFixed().getWd() + ", wip : " +  fixPtOps.getInput1().getTypeAcFixed().getWip() +"|" + "wd : "+ fixPtOps.getInput2().getTypeAcFixed().getWd() + ", wip : " +  fixPtOps.getInput2().getTypeAcFixed().getWip() + "|" + "wd : "+ fixPtOps.getOutput().getTypeAcFixed().getWd() + ", wip : " +  fixPtOps.getOutput().getTypeAcFixed().getWip() +" }&  \\ttab{["+ fixPtOps.getInput1().getDyn().getLowerBound()+", " + fixPtOps.getInput1().getDyn().getUpperBound() +"][" + fixPtOps.getInput2().getDyn().getLowerBound()+", " + fixPtOps.getInput2().getDyn().getUpperBound() +"]["+ fixPtOps.getOutput().getDyn().getLowerBound()+", " + fixPtOps.getOutput().getDyn().getUpperBound() +"]"+"}\\\\ ");
//			}
//			else{
//				this._bufferTexFile.write("\\ttab{"+ AnnotateOperators.getOpCDFGName(AnnotateOperators.getNumOpCDFG(Integer.parseInt(fixPtOps.getNumOp()))) +" ("+ fixPtOps.getNumOp() +")}  & \\ttab{wd : "+ fixPtOps.getInput1().getTypeAcFixed().getWd() + ", wip : " +  fixPtOps.getInput1().getTypeAcFixed().getWip() +"|" + "wd : "+ fixPtOps.getInput2().getTypeAcFixed().getWd() + ", wip : " +  fixPtOps.getInput2().getTypeAcFixed().getWip() + "|" + "wd : "+ fixPtOps.getOutput().getTypeAcFixed().getWd() + ", wip : " +  fixPtOps.getOutput().getTypeAcFixed().getWip() +" }&  \\ttab{["+ fixPtOps.getInput1().getDyn().getLowerBound()+", " + fixPtOps.getInput1().getDyn().getUpperBound() +"][" + fixPtOps.getInput2().getDyn().getLowerBound()+", " + fixPtOps.getInput2().getDyn().getUpperBound() +"]["+ fixPtOps.getOutput().getDyn().getLowerBound()+", " + fixPtOps.getOutput().getDyn().getUpperBound() +"]"+"}\\\\ ");
//			
//			}
//			color_font = !color_font;
//		}
//		this._bufferTexFile.write("\\hline\n\\end{tabular}\n\\end{center}\n\\end{scriptsize}\n\\caption{Operator informations}\n\\end{table}");
//		
//	}
//	
//	/**
//	 * Write some important information from IDFixEval
//	 *  TODO Finir l'implémentation de cette méthode
//	 * 
//	 * @author nicolas simon
//	 * @throws IOException
//	 */
//	@SuppressWarnings("unused")
//	private void writeIDFixEvalInformation() throws IOException{
//		_logTiming.getLogIDFixEval().setNbNodeBrIDFixEval(1);
//		_logTiming.getLogIDFixEval().setNbNodeAfterAggregationIDFixEval(1);
//		_logTiming.getLogIDFixEval().setNbNodeIDFixEval(1);
//		_logTiming.getLogIDFixEval().setNbNodeWithNoiseIDFixEval(1);
//		LogTimingIO.saveLogTiming("/home/nicolas/test.xml", _logTiming);
//		
////		LogTiming log = new LogTimingImpl();
////		log = LogTimingIO.loadFixPtSpecif("/home/nicolas/test.xml");
////		System.out.println(log.getUserConstraint());
////		System.out.println(log.getLogIDFixEval().getNbNodeAfterAggregationIDFixEval());
//		
//	}
//	
//	/**
//	 * Compile and convert the tex file generated to a pdf file into the output folder
//	 * 
//	 * @author nicolas simon
//	 * @throws IOException
//	 * @throws InterruptedException 
//	 */
//	private void compileTexFile() throws IOException, InterruptedException{
//		List<String> cmd = new LinkedList<String>();
//		Process process;
//		ProcessBuilder pb;
//		
//		cmd.add("latex");
//		cmd.add("-output-directory");
//		cmd.add(this._pathTempDir);
//		cmd.add("-interaction=nonstopmode");
//		cmd.add(this._pathTempFile+".tex");
//		
//		pb = new ProcessBuilder(cmd);
//		System.out.println("*** Notice: Command = " + cmd);
//		
//		process = pb.start();
//		process.waitFor();
//
//		
//		cmd.add("latex");
//		cmd.add("-output-directory");
//		cmd.add(this._pathTempDir);
//		cmd.add("-interaction=nonstopmode");
//		cmd.add(this._pathTempFile+".tex");
//		
//		pb = new ProcessBuilder(cmd);
//		System.out.println("*** Notice: Command = " + cmd);
//		
//		process = pb.start();
//		process.waitFor();
//		
//		cmd.clear();
//		cmd.add("dvips");
//		cmd.add("-o");
//		cmd.add(this._pathTempFile+".ps");
//		cmd.add(this._pathTempFile+".dvi");
//		pb = new ProcessBuilder(cmd);
//		System.out.println("*** Notice: Command = " + cmd);
//		
//		process = pb.start();
//		process.waitFor();
//	
//		
//		cmd.clear();
//		cmd.add("ps2pdf");
//		cmd.add(this._pathTempFile+".ps");
//		cmd.add(this._outputDirPath + ConstantPathAndName.LOG_FILE_NAME + ".pdf");
//		
//		pb = new ProcessBuilder(cmd);
//		System.out.println("*** Notice: Command = " + cmd);
//		
//		process = pb.start();
//		process.waitFor();
//	}
//	

}
