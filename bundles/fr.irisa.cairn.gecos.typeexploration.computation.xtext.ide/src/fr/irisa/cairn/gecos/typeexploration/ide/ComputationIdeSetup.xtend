/*
 * generated by Xtext 2.14.0
 */
package fr.irisa.cairn.gecos.typeexploration.ide

import com.google.inject.Guice
import fr.irisa.cairn.gecos.typeexploration.ComputationRuntimeModule
import fr.irisa.cairn.gecos.typeexploration.ComputationStandaloneSetup
import org.eclipse.xtext.util.Modules2

/**
 * Initialization support for running Xtext languages as language servers.
 */
class ComputationIdeSetup extends ComputationStandaloneSetup {

	override createInjector() {
		Guice.createInjector(Modules2.mixin(new ComputationRuntimeModule, new ComputationIdeModule))
	}
	
}
