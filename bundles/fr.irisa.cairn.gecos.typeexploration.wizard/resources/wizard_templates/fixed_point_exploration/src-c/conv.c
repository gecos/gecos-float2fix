#define TYPE_0 double
#define N 125


//!!NOTE: adding a main function is optional.
// If no main you have to inject values for inputs


#pragma EXPLORE_FIX W={16} I={1}
TYPE_0 C[3] = {0.2, 0.35, 0.45};


void conv(
#pragma EXPLORE_FIX W={8..16} I={10}
	TYPE_0 in[N],
#pragma EXPLORE_FIX W={10..16} I={10}
	TYPE_0 out[N])
{
	$inject(in, $random_uniform(0,20,1)); // inject uniform random values between 0 and 20 (1 is the seed)

	int i;
	for (i = 1; i < N-1; i++)
		out[i] = C[0]*in[i-1] + C[1]*in[i] + C[2]*in[i+1];

	$save(out);
}


//int main() {
//	TYPE_0 in[N];
//
//	//!! typedef can be used to allow type operations (casting and sizeof ...)
//	//!! However, one typedef can currently be used by one Symbol only !
//	typedef double TYPE_out;
//	TYPE_out *out = (TYPE_out *)malloc(N * sizeof(TYPE_out));
//
//	int i;
//	for(i=0; i<N; i++) {
//		in[i] = 0.25*i;
//	}
//
//	conv(in, out);
//}
