#define TYPE_0 double
#define N 125


//!! NOTE: currently ct_float does requires explicit casting when working with different ct_float types
//!! Hence, this exploration enforces all symbols to have the same type


void conv(
#pragma EXPLORE_FLOAT W={8..32} E={1..6}
		TYPE_0 in[N],

//#pragma EXPLORE_FLOAT W={16} E={4}
#pragma EXPLORE_CONSTRAINT SAME = in
		TYPE_0 out[N])
{

//#pragma EXPLORE_FLOAT W={16} E={4}
#pragma EXPLORE_CONSTRAINT SAME = in
	TYPE_0 C[3] = {0.2, 0.35, 0.45};

	int i;
	for (i = 1; i < N-1; i++)
		out[i] = C[0]*in[i-1] + C[1]*in[i] + C[2]*in[i+1];

	$save(out);
}


int main() {
	TYPE_0 in[N];

	//!! typedef can be used to allow type operations (casting and sizeof ...)
	//!! However, one typedef can currently be used by one Symbol only !
	typedef double TYPE_out;
	TYPE_out *out = (TYPE_out *)malloc(N * sizeof(TYPE_out));

	int i;
	for(i=0; i<N; i++) {
		in[i] = 0.25*i;
	}

	conv(in, out);
}
