package fr.irisa.cairn.gecos.typeexploration.ui.examples;

import org.eclipse.core.runtime.Platform;
import org.eclipse.ui.INewWizard;
import org.osgi.framework.Bundle;

import fr.irisa.cairn.gecos.core.ui.examples.utils.BasicGecosExampleWizard;
import fr.irisa.cairn.gecos.typeexploration.ui.Activator;

public class FixedPointTypesExploration extends BasicGecosExampleWizard implements INewWizard {	
	@Override
	public String getResourcesFolderToCopyIntoProject() {
		return "resources/wizard_templates/fixed_point_exploration/";
	}

	@Override
	public String getPageName() {
		return "Automatic fixed-point types exploration";
	}

	@Override
	public String getPageDescription() {
		return "This wizard will create a example project for exploring fixed-point datatypes.";
	}

	@Override
	protected Bundle getPluginBundle() {
		return Platform.getBundle(Activator.PLUGIN_ID);
	}
}
