package fr.irisa.cairn.idfix.hls.optimization.algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import fr.irisa.cairn.idfix.model.fixedpointspecification.Operation;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixProject;
import fr.irisa.cairn.idfix.model.informationandtiming.Section;
import fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace;
import fr.irisa.cairn.idfix.optimization.extension.cost.ICostProvider;
import fr.irisa.cairn.idfix.optimization.utils.DefaultOptimization;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.optimization.INoiseFunctionResultSimulator;
import fr.irisa.cairn.idfix.utils.exceptions.OptimException;
import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;

public class HLSTabuSearch extends DefaultOptimization {
	private boolean _dichotomicSearch = true;
	private boolean _directionUp = true;
	
	public HLSTabuSearch(IdfixProject project, ICostProvider costProvider, float userConstraint, String operatorLibrary,
			Section sectionTimeLog, INoiseFunctionResultSimulator simulator) {
		super(project, costProvider, userConstraint, operatorLibrary, sectionTimeLog, simulator);
	}
	
	@Override
	public String getName() {
		return "HLS tabu search";
	}

	@Override
	protected double getFinalNoisePowerResult() {
		try {
			return noiseEvaluationProcess(_solutionSpace);
		} catch (OptimException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected double getFinalCostResult() {
		return costEvaluationProcess(_solutionSpace);
	}
	
	@Override
	protected float costEvaluationProcess(SolutionSpace space) {
		_fixedPointSpecification.updateDataFixedInformation();
		return super.costEvaluationProcess(space);
	}

	@Override
	protected boolean optimAlgorithm() throws IllegalAccessException, OptimException, SimulationException {		
		double currentNoise;
		
		_solutionSpace.setAllOperatorToMax();
		currentNoise = noiseEvaluationProcess(_solutionSpace);
		
		if(currentNoise > _userConstraint){
			return false;
		}
		
		if(_dichotomicSearch && _directionUp){
			startingSolutionSearchDichotomic();
			if(noiseEvaluationProcess(_solutionSpace) < _userConstraint){
				_directionUp = false;
			}
		}
		else if(_directionUp){
			_solutionSpace.setAllOperatorToMin();
		}
		
		tabuSearch();
		_solutionSpace.useSolutionSpaceSaved();
		
		_fixedPointSpecification.updateDataFixedInformation();
		return true;
	}
	
	/**
	 * Search a start solution thanks to a dichotomy algorithm
	 * 
	 * @throws OptimException 
	 */
	private void startingSolutionSearchDichotomic() throws OptimException {
		int indexMin, indexMax, indexDich;
		double noise;
		Map<Operation, Integer> resultSaving = new HashMap<Operation, Integer>();
		_solutionSpace.setAllOperatorToMax();
		
		for(Operation op : _solutionSpace.getOperators()){
			_solutionSpace.setOperatorTo(op, 0);
			noise = noiseEvaluationProcess(_solutionSpace);
			if(noise > _userConstraint){
				indexMin = -1;
				indexMax = _solutionSpace.getMaxIndexOf(op);
				indexDich = (indexMax - indexMin) / 2;
				while(indexMin + 1 != indexMax){
					_solutionSpace.setOperatorTo(op, indexDich);
					noise = noiseEvaluationProcess(_solutionSpace);
					
					if(noise < _userConstraint){
						indexMax = indexDich;
						indexDich -= (indexMax - indexMin) / 2; 
					}
					else{
						indexMin = indexDich;
						indexDich += (indexMax - indexMin) / 2;
					}
				}
			
				resultSaving.put(op, indexMax);
			}
			else{
				resultSaving.put(op, 0);
			}
			_solutionSpace.setOperatorTo(op, _solutionSpace.getMaxIndexOf(op));
		}
		
		for(Operation op : resultSaving.keySet()){
			_solutionSpace.setOperatorTo(op, resultSaving.get(op));
		}
	}
	
	private void tabuSearch() throws OptimException{
		double finalNoise = Double.NEGATIVE_INFINITY;
		double baseNoise;
		double nextNoise;
		
		ArrayList<Operation> tabuList = new ArrayList<Operation>();
		int indexMax;
		int indexMin = 0;
		
		double bestCriterion, currentCriterion;
		Operation bestLocalOperatorDirection;
		
		baseNoise = noiseEvaluationProcess(_solutionSpace);
		while(tabuList.size() < _solutionSpace.getOperators().size()){
			bestCriterion = _directionUp?Double.POSITIVE_INFINITY:Double.NEGATIVE_INFINITY;
			bestLocalOperatorDirection = null;
			for(Operation op : _solutionSpace.getOperators()){
				if(!tabuList.contains(op)){
					indexMax = _solutionSpace.getMaxIndexOf(op);
					if(_directionUp && _solutionSpace.getCurrentIndexOf(op) < indexMax){
						_solutionSpace.indexPlusOneTo(op);
						nextNoise = noiseEvaluationProcess(_solutionSpace);
						
						currentCriterion = computeCriterion(baseNoise, nextNoise);
						if(currentCriterion < bestCriterion){
							bestCriterion = currentCriterion;
							bestLocalOperatorDirection = op;
						}
						
						_solutionSpace.indexMinusOneTo(op);
					}
					else if(!_directionUp && _solutionSpace.getCurrentIndexOf(op) > indexMin){
						_solutionSpace.indexMinusOneTo(op);
						nextNoise = noiseEvaluationProcess(_solutionSpace);
						
						currentCriterion = computeCriterion(baseNoise, nextNoise);
						if(currentCriterion > bestCriterion){
							bestCriterion = currentCriterion;
							bestLocalOperatorDirection = op;
						}
						
						_solutionSpace.indexPlusOneTo(op);
					}
					else{
						tabuList.add(op);
					}
				}
			}
						
			if(tabuList.size() == _solutionSpace.getOperators().size()) // TODO a modifier le break
				break;

			if(bestLocalOperatorDirection == null) // Security : Impossible to have null
				throw new OptimException("No best local direction found");
			
			System.out.println(bestLocalOperatorDirection);
			if(_directionUp){
				if(bestCriterion >= 0){
					jumpToAnotherBranch();
				}
				else{
					_solutionSpace.indexPlusOneTo(bestLocalOperatorDirection);
				}
				
				baseNoise = noiseEvaluationProcess(_solutionSpace);
				if(baseNoise < _userConstraint){
					_directionUp = false;
					tabuList.add(bestLocalOperatorDirection);
				}
			}
			else{
				_solutionSpace.indexMinusOneTo(bestLocalOperatorDirection);
				
				baseNoise = noiseEvaluationProcess(_solutionSpace);
				if(baseNoise >= _userConstraint){
					_directionUp = true;
				}
			}
				
			if(baseNoise <= _userConstraint && baseNoise > finalNoise){
				_solutionSpace.saveCurrentSolutionSpace();
				finalNoise = baseNoise;
			}
		}
	}
	
	/**
	 * 	Criterion for direction search, lower is better
	 * 
	 * @param idNext
	 * @param id
	 * @return
	 */
	protected double computeCriterion(double baseNoisePower, double nextNoisePower) {
			return (nextNoisePower - baseNoisePower);		
	}
	
	/**
	 * Special case which happening in some system. This case doesn't permit to find a direction which improve the solution.
	 * This case have a special treatment. We find a new solution from the stucked solution. 
	 * 
	 * @author nicolas simon
	 * @throws OptimException  
	 */
	private void jumpToAnotherBranch() throws OptimException {
		double nextNoisePower;
		double baseNoisePower;
		double delta;
		LinkedList<Operation> orderedList = new LinkedList<Operation>();
		SortedMap<Double, List<Operation>> deltaMap = new TreeMap<Double, List<Operation>>(); // Map triée en fonction de la key (ici la key représente le delta pour le choix de la direction)
		List<Operation> applyPlus = new LinkedList<Operation>();
				
		for(Operation op : _solutionSpace.getOperators()){
			if(_solutionSpace.getCurrentIndexOf(op) < _solutionSpace.getMaxIndexOf(op)){
				_solutionSpace.indexPlusOneTo(op);
				applyPlus.add(op);
			}
		}
				
		baseNoisePower = noiseEvaluationProcess(_solutionSpace);
		for(Operation op : applyPlus){
			_solutionSpace.indexMinusOneTo(op);
			nextNoisePower = noiseEvaluationProcess(_solutionSpace);
			delta = computeCriterion(baseNoisePower, nextNoisePower);
			if(deltaMap.containsKey(delta)){
				deltaMap.get(delta).add(op);
			}
			else{
				List<Operation> listTmp = new ArrayList<Operation>();
				listTmp.add(op);
				deltaMap.put(delta, listTmp);
			}
			
			_solutionSpace.indexPlusOneTo(op);
		}
		
		for(Operation op : applyPlus){
			_solutionSpace.indexMinusOneTo(op);
		}
		
		// Ajout dans une liste les opérateurs dans l'ordre
		for(double d : deltaMap.keySet()){
			for(Operation i : deltaMap.get(d)){
				orderedList.addFirst(i);
			}
		}
		
		baseNoisePower = noiseEvaluationProcess(_solutionSpace); // bruit de référence
		for(Operation op : orderedList){
			_solutionSpace.indexPlusOneTo(op);
			nextNoisePower = noiseEvaluationProcess(_solutionSpace);
			if(nextNoisePower < baseNoisePower)
				break;
		}
	}
}
