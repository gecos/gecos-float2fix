package fr.irisa.cairn.idfix.hls.optimization.cost;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import com.google.common.io.Files;

import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.c.generator.XtendCGenerator;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.idfix.backend.FixedPtSpecificationApplier;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Data;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixProject;
import fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace;
import fr.irisa.cairn.idfix.optimization.extension.cost.ICostProvider;
import fr.irisa.cairn.idfix.utils.IDFixUtils;
import fr.irisa.cairn.idfix.utils.exceptions.SFGModelCreationException;
import fr.irisa.r2d2.gecos.framework.utils.SystemExec;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;
import gecos.types.ACFixedType;
import gecos.types.FunctionType;
import gecos.types.Type;
import gecos.types.VoidType;

public class BashScriptCostProvider implements ICostProvider {
	private String _script;
	private IdfixProject _idfixProject;
	private File _debugFolder;
	private File _resultFile;
	private File _fixedInformationFile;
	private int _iteration;
	
	public BashScriptCostProvider(String script, IdfixProject idfixproject) throws IOException{
		_script = script;
		_idfixProject = idfixproject;
		
		_debugFolder = new File("debug");
		_debugFolder.mkdir();
		
		_resultFile = File.createTempFile("idfix_result_cost_bash_script", null);
		_resultFile.deleteOnExit();
		_fixedInformationFile = File.createTempFile("idfix_fixed_information_cost_bash_script", null);
		_fixedInformationFile.deleteOnExit();
		
		_iteration = 0;
	}
	
	private String getFileName(String file) {
		String fileName = file;
		File f = new File(file);
		if (f.isFile())
			fileName = f.getName();
		return fileName;
	}		
	
	@Override
	public float getCostValue(SolutionSpace space) { 
		// Need to create a copy of the procedure set to not modify the current one
		ProcedureSet psRegenerated = _idfixProject.getGecosProjectAnnotedAndNotUnrolled().getSources().get(0).getModel().copy();
		GecosProject projectregenerated = GecosUserCoreFactory.project("GecosProjectFixed");
		GecosSourceFile source = GecosUserCoreFactory.source(((GecosSourceFile) _idfixProject.getGecosProjectAnnotedAndNotUnrolled().getSources().get(0)).getName() ,psRegenerated);
		projectregenerated.getSources().add(source);
		
		File file = new File(_debugFolder, "solution_space_" + _iteration);
		try {
			file.createNewFile();
			PrintWriter writer = new PrintWriter(file);
			writer.println(space.toString());
			writer.println();
			for(Data data : space.getDatas()){
				writer.println(data.toString());
			}
			writer.println();writer.println();
			writer.close();
		} catch (IOException e1) {
			throw new RuntimeException(e1);
		}
		
		// Generate fixed source file
		new FixedPtSpecificationApplier(psRegenerated, _idfixProject.getFixedPointSpecification()).compute();
		GecosUserAnnotationFactory.pragma(psRegenerated, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION+"#include <ac_fixed.h>");
		new XtendCGenerator(psRegenerated, _idfixProject.getOutputFolderPath()).compute();
		
		// Generate information on the function parameters and on the return function type needed by the script
		writeInputOutputToFile(psRegenerated); 
		
		// Call script
		String filename = getFileName(projectregenerated.getSources().get(0).getName());
		File sourcefile = new File(_idfixProject.getOutputFolderPath(), filename);
	
		try {
			Files.copy(sourcefile, new File(_debugFolder, "solution_file_" + _iteration +".c"));
		} catch (IOException e1) {
			throw new RuntimeException(e1);
		}
	
		StringBuilder builder = new StringBuilder(_script);
		builder.append(" ").append(sourcefile.getPath());
		builder.append(" ").append(_resultFile.getPath());
		builder.append(" ").append(_fixedInformationFile.getPath());
		SystemExec.shell(builder.toString());
		try {
			BufferedReader reader = new BufferedReader(new FileReader(_resultFile));
			String line = reader.readLine();
			if(line == null){
				reader.close();
				throw new RuntimeException("No information contained in the result file");
			}
			
			reader.close();
			_iteration++;
			return (float)1.023e-3;
//			return Float.parseFloat(line);
		} catch (IOException e) {
			throw new RuntimeException("Error during reading the temporary file result");
		}
	}
	
	/**
	 * Write on a temporary file the bit width information needed by the script (parameters and return type)
	 * example:
	 * 	param_0:N0
	 * 	param_1:N1
	 * 	return:N2
	 */
	private void writeInputOutputToFile(ProcedureSet ps) {
		try {
			Procedure p = IDFixUtils.getMainProcedure(ps);
			PrintWriter writer = new PrintWriter(_fixedInformationFile);
			int paramNumber = 0;
			for(ParameterSymbol s : p.listParameters()){
				if(IDFixUtils.isInputVar(s) || IDFixUtils.isOutputVar(s)){
					Data data = _idfixProject.getFixedPointSpecification().findDataFromSymbol(s);
					writer.print("param_" + paramNumber);
					writer.print(":");
					writer.println(data.getFixedInformation().getBitWidth());
				}
			}
			
			FunctionType ftype = (FunctionType) p.getSymbol().getType();
			TypeAnalyzer tanalyzer = new TypeAnalyzer(ftype.getReturnType());
			if(tanalyzer.isBase()){
				Type type = tanalyzer.getBase();
				if(type instanceof ACFixedType){
					ACFixedType fixedtype = (ACFixedType) type;
					writer.print("return");
					writer.print(":");
					writer.println(fixedtype.getBitwidth());
				}
				else if(!(type instanceof VoidType)){
					writer.close();
					throw new RuntimeException("Return type not managed " + type);
				}
			}
			else{
				writer.close();
				throw new RuntimeException("Return type not managed " + ftype.getReturnType());
			}
			
			writer.close();
		} catch (SFGModelCreationException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException("Error during writing in the temporary file the fixed point information of the input and output variable");
		}
	}		
}