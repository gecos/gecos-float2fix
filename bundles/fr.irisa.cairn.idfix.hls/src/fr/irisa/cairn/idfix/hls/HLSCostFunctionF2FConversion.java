package fr.irisa.cairn.idfix.hls;

import java.io.File;
import java.io.IOException;

import fr.irisa.cairn.idfix.hls.optimization.algorithms.HLSMinPlusOne;
import fr.irisa.cairn.idfix.hls.optimization.algorithms.HLSMinPlusOneWithoutCost;
import fr.irisa.cairn.idfix.hls.optimization.algorithms.HLSTabuSearch;
import fr.irisa.cairn.idfix.hls.optimization.cost.BashScriptCostProvider;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixProject;
import fr.irisa.cairn.idfix.optimization.IDFixConvOptimization;
import fr.irisa.cairn.idfix.optimization.extension.cost.ICostProvider;
import fr.irisa.cairn.idfix.transforms.Float2FixConversion;

public class HLSCostFunctionF2FConversion extends Float2FixConversion{
	private String _script;
	private final static int _optimization_number = 6;
	private File _debugFolder;
	
	public HLSCostFunctionF2FConversion(String script, IdfixProject idfixProject, String noiseConstraint, String operatorLibraryPath, int nbSimulation){
		super(idfixProject, noiseConstraint, operatorLibraryPath, _optimization_number, nbSimulation);
		_script = script;
		_debugFolder = new File("debug");
		_debugFolder.mkdir();
	}
	
//	@Override
//	protected ICostProvider createCostProvider(){
//		try {
//			return new BashScriptCostProvider(_script, _idfixProject);
//		} catch (IOException e) {
//			throw new RuntimeException("Error during temporary result file creation");
//		}
//	}

	@Override
	protected void wlo() {
		IDFixConvOptimization.addAlgorithm(_optimization_number, HLSMinPlusOne.class);
		super.wlo();
	}
}

