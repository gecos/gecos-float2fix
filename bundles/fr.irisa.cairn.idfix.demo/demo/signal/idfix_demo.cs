source_file = "src/lti/basic/filter/deriche5.c";
architecture_model = "utils/fake_architecture_model.xml";
user_constraint = "-50";
non_lti_simulation_sample = 0;

	#+  - 0 => Branch and Bound algorithm
	#  - 1 => Branch and Bound algorithm with fixed size operators
	#  - 2 => Min+1
	#  - 3 => Min+1 adaptative
	#  - 4 => tabu search
	#  - 5 => GRASP
optimization_used = 4;

idfix_project = CreateIDFixProject(source_file, "output/", "userconfiguration.cfg");
Float2FixConversion(idfix_project,user_constraint, architecture_model, optimization_used, non_lti_simulation_sample);