#define N 5

#pragma MAIN_FUNC
void deriche() {

#pragma DYNAMIC [0,1]
	float img[N][N];
#pragma OUTPUT
	float out[N][N];

	// Temporary arrays
	float T11[N][N];
	float T12[N][N];

	float T[N][N];
	float T21[N][N];
	float T22[N][N];

	// Coefficients
	float a[8] = {
			-0.1886816585, 0.1102090676, -0.1836817793, 0.1144412108,
			-0.1886816585, 0.1102090676, -0.1836817793, 0.1144412108
	};
	float b[2] = {1.557601566, -0.6065306597};
	double c[2] = {1, 1};

	// Iterators
	int i,j;

	for (i=0; i<N; i++) {
		for (j=0; j<N; j++) {
			if (j==0) {
				T11[i][j] = a[0]*img[i][j];
			} else if (j==1) {
				T11[i][j] = a[0]*img[i][j] + a[1]*img[i][j-1] + b[0]*T11[i][j-1];
			} else {
				T11[i][j] = a[0]*img[i][j] + a[1]*img[i][j-1] + b[0]*T11[i][j-1] + b[1]*T11[i][j-2];
			}
		}
	}

	for (i=0; i<N; i++) {
		for (j=N-1; j>=0; j--) {
			if (j==N-1) {
				T12[i][j] = 0;
			} else if (j==N-2) {
				T12[i][j] = a[2]*img[i][j+1];
			} else if (j==N-3) {
				T12[i][j] = a[2]*img[i][j+1] + a[3]*img[i][j+2] + b[0]*T11[i][j+1];
			} else {
				T12[i][j] = a[2]*img[i][j+1] + a[3]*img[i][j+2] + b[0]*T11[i][j+1] + b[1]*T11[i][j+2];
			}
		}
	}

	for (i=0; i<N; i++) {
		for (j=0; j<N; j++) {
			T[i][j] = c[0] * (T11[i][j] + T12[i][j]);
		}
	}

	for (j=0; j<N; j++) {
		for (i=0; i<N; i++) {
			if (i==0) {
				T21[i][j] = a[4]*T[i][j];
			} else if (i==1) {
				T21[i][j] = a[4]*T[i][j] + a[5]*T[i-1][j] + b[0]*T21[i-1][j];
			} else {
				T21[i][j] = a[4]*T[i][j] + a[5]*T[i-1][j] + b[0]*T21[i-1][j] + b[1]*T21[i-2][j];
			}
		}
	}

	for (j=0; j<N; j++) {
		for (i=N-1; i>=0; i--) {
			if (i==N-1) {
				T22[i][j] = 0;
			} else if (i==N-2) {
				T22[i][j] = a[6]*T[i+1][j];
			} else if (i==N-3) {
				T22[i][j] = a[6]*T[i+1][j] + a[7]*T[i+2][j] + b[0]*T22[i+1][j];
			} else {
				T22[i][j] = a[6]*T[i+1][j] + a[7]*T[i+2][j] + b[0]*T22[i+1][j] + b[1]*T22[i+2][j];
			}
		}
	}

	for (i=0; i<N; i++) {
		for (j=0; j<N; j++) {
			out[i][j] = c[1] * (T21[i][j] + T22[i][j]);
		}
	}
}
