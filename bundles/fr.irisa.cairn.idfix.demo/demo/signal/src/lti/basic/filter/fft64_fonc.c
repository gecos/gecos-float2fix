#define NN 64
#define NStage 6

void stage(float xr[NN], float xi[NN], float wr[NN], float wi[NN], int nbStage);
int puiss2(int puiss);
void butterflyGroup(float xr[NN], float xi[NN], float wr[NN], float wi[NN], int a, int nbButterFlyPerGroup, int nbSampleInGroup);
void butterfly(float xr[NN], float xi[NN], float wr[NN], float wi[NN], int nbButter, int nbButterFlyPerGroup);

#pragma MAIN_FUNC
void fft(
#pragma DYNAMIC [-1, 1]
		float Xr[NN],
#pragma DYNAMIC [-1, 1]
		float Xi[NN],
#pragma OUTPUT
		float Yr[NN],
#pragma OUTPUT
		float Yi[NN]) {

	int i;

	float xr[NN];
	float xi[NN];

	float wr[NN];
	float wi[NN];

	for (i = 0; i < NN; i++) {
		xr[i] = Xr[i];
		xi[i] = Xi[i];
	}

	for (i = 0; i < NN; i++) {
		wr[i] = 0.79;
		wi[i] = 1.34;
	}

	for (i = 0; i < NStage; i++) {
		stage(xr, xi, wr, wi, i);
	}

	for (i = 0; i < NN; i++) {
		Yr[i] = xr[i];
		Yi[i] = xi[i];
	}

}

int puiss2(int puiss) {
	int i;
	int ret;
	ret = 1;
	for (i = 0; i < puiss; i++) {
		ret *= 2;
	}
	return ret;
}

void stage(float xr[NN], float xi[NN], float wr[NN], float wi[NN], int nbStage) {
	int i;
	int nbGroup;
	int nbSampleInGroup;
	int nbButterFlyPerGroup;
	int div;

	div = puiss2(nbStage);
	nbButterFlyPerGroup = (NN / (2 * div));
	nbSampleInGroup = nbButterFlyPerGroup * 2;
	nbGroup = NN / nbSampleInGroup;
	for (i = 0; i < nbButterFlyPerGroup; i++) {
		butterflyGroup(xr, xi, wr, wi, i, nbButterFlyPerGroup, nbSampleInGroup);
	}

}

void butterflyGroup(float xr[NN], float xi[NN], float wr[NN], float wi[NN], int a, int nbButterFlyPerGroup, int nbSampleInGroup) {
	int i;

	for (i = a; i < NN; i = i + nbSampleInGroup) {
		butterfly(xr, xi, wr, wi, i, nbButterFlyPerGroup);
	}
}

void butterfly(float xr[NN], float xi[NN], float wr[NN], float wi[NN], int nbButter, int nbButterFlyPerGroup){
	float sous_r;
	float sous_i;

	sous_r = xr[nbButter] - xr[nbButterFlyPerGroup + nbButter];
	sous_i = xi[nbButter] - xi[nbButterFlyPerGroup + nbButter];

	xr[nbButter] = xr[nbButter] + xr[nbButterFlyPerGroup + nbButter];
	xi[nbButter] = xi[nbButter] + xi[nbButterFlyPerGroup + nbButter];

	xr[nbButterFlyPerGroup + nbButter] = sous_r * wr[nbButter] - sous_i * wi[nbButter];
	xi[nbButterFlyPerGroup + nbButter] = sous_r * wi[nbButter] + sous_i * wr[nbButter];
}
