
//#include <math.h>

#define SIGN -1

#define NN 64

// DIF FFT or IFFT
#pragma MAIN_FUNC
void fft(
#pragma DYNAMIC [-1, 1]
		double Xr[NN],
#pragma DYNAMIC [-1, 1]
		double Xi[NN],
#pragma OUTPUT
		double Yr[NN],
#pragma OUTPUT
		double Yi[NN]) {
	// N is the FFT size obtained from size of the Matlab array
	// EXP = log2(N)
	// SIGN=-1: FFT;   SIGN=1: IFFT

	double XXr[NN];
	double XXi[NN];

	int ii, jj, ll;
	int NA, NG, N1, N2;

	double Wr[NN];
	double Wi[NN];
	double C, S;

	double tr;

	double ti;
	//double pi = acos(-1);
	double pi = 3.14159265358979323846;
	float a, b ,c;

	a = 5;
	b = a*3;
	c = a + b;

	for (ii = 0; ii < NN; ii++) {
		XXr[ii] = Xr[ii];
		XXi[ii] = Xi[ii];
	}
	for (ii = 0; ii < NN; ii++) {
		//Wr[ii] = cos(2 * pi * ii / N);
		//Wr[ii] = 0.5*ii+1;
		Wr[ii] = 0.79;
		//Wi[ii] = sign * sin(2 * pi * ii / N);
		//Wi[ii] = 0.25*ii+1;
		Wi[ii] = 1.34;
	}


	N2 = NN;
	while (N2 > 0) { // for main stage
		N1 = N2; // Number of samples in a group
		NG = NN / N1; // Number of groups
		N2 = N2 / 2; // Number of butterflies in a group

		NA = 0;
		for (jj = 0; jj < N2; jj++) { // for stage butterflty
			C = Wr[NA];
			S = Wi[NA];
			NA = NA + NG;
			ii = jj;
			while (ii < NN) {
				ll = ii + N2;
				tr = XXr[ii] - XXr[ll];
				ti = XXi[ii] - XXi[ll];
				XXr[ii] = XXr[ii] + XXr[ll];
				XXi[ii] = XXi[ii] + XXi[ll];
				XXr[ll] = tr * C - ti * S;
				XXi[ll] = tr * S + ti * C;
				ii = ii + N1;
			}
		}
	}

	for (ii = 0; ii < NN; ii++) {
		if (SIGN == 1) {
			Yr[ii] = XXr[ii] / NN;
			Yi[ii] = XXi[ii] / NN;
		}
		else {
			Yr[ii] = XXr[ii];
			Yi[ii] = XXi[ii];
		}
	}
}

