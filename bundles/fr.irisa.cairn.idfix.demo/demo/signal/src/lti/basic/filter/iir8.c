/* IIR filter written in C */

#define N 8

#pragma MAIN_FUNC
float iir8(
#pragma DYNAMIC [-1,1]
		float xn) {

#pragma OUTPUT
	float output;

	float b[9] = {0.001162,0.009297,0.032538,0.065076,0.081345,0.065076,0.032538,0.009297,0.001162};
	float a[9] = {1.000000,-2.227607,3.070593,-2.621630,1.561471,-0.628929,0.168485,-0.026845,0.001952};


#pragma DELAY
	static float x[9];
#pragma DELAY
	static float y[9];

	int i;

	x[0] = xn;

	y[0] = x[0] * b[0];
	for (i = 1; i < N + 1; i++){
		y[0] += x[i] * b[i] - y[i] * a[i];
	}

	output = y[0];

	for (i = N; i > 0; i--){
		y[i] = y[i - 1];
		x[i] = x[i - 1];
	}

	return output;
}

