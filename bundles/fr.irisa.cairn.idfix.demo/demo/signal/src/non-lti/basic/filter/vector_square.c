#define N 4

#pragma MAIN_FUNC
float vector_square(
#pragma DYNAMIC [-9.7,4.5]
		float sample[N]) {

#pragma OUTPUT
	float y;

	float acc[N/2];

	acc[0] = sample[0] * sample[0] + sample[1] * sample[1];
	acc[1] = sample[2] * sample[2] + sample[3] * sample[3];

	y = acc[0] + acc[1];

	return y;
}
