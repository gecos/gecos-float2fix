#pragma MAIN_FUNC
float polynome(
#pragma DYNAMIC [-5.7, 9.8]
	float x
){
#pragma OUTPUT
	float y;

	float a[4] = {0.9982, -0.9661, 0.7804, 0.3488};
	float x2 , x3;

	x2 = x*x;
	x3 = x2*x;
	y = (a[0] + a[1]*x) + (a[2]*x2 + a[3]*x3);

	return y;
}

