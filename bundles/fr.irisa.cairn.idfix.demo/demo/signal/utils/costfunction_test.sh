#!/bin/bash

LOGIN=msingh
SERVER=montjoie

if [ "$1" == "" ];
then
        echo "Missing source file path parameter"
        exit 1
else
        source_file=$1
fi

if [ ! -f "${source_file}" ];
then
        echo "source file not found"
fi

if [ "$2" == "" ];
then
        echo "Missing result file path parameter"
        exit 1
else
        result_file=$2
fi

if [ ! -f "${result_file}" ];
then
        echo "Result file not found"
fi

if [ "$3" == "" ];
then
        echo "Missing fixed point information file path parameter"
        exit 1
else
        fixed_point_information=$3
fi

if [ ! -f "${fixed_point_information}" ];
then
        echo "fixed_point_information not found"
fi

echo $source_file
echo $result_file
echo $fixed_point_information

scp $fixed_point_information ${LOGIN}@${SERVER}:~/main/info.txt
scp $source_file ${LOGIN}@${SERVER}:~/main/fir64.cpp
ssh ${LOGIN}@${SERVER} /home/${LOGIN}/main/testing.sh
scp ${LOGIN}@${SERVER}:~/main/result.txt $result_file
exit

exit 0