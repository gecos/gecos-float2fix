#!/bin/bash

systemc_library=/home/nicolas/Tools/gecosmars/workspace/fr.irisa.cairn.idfix.utils/resources/systemc-2.3.0/bin/lib-linux64

if [ "$1" == "" ];
then
    echo "Need the binaries output of the conversion script"
    exit 1
else
    binaries_folder=$1
fi 

if [ "$2" == "" ];
then
    echo "Need to precise the number of simulation sample"
    exit 1
else
    if [[ $2 =~ [0-9]+$ ]];
    then 
        simulation_sample=$2
    else
        echo "The number of simulation sample need to be a positive integer"
        exit 1
    fi
fi 

if [ "$3" == "" ];
then
    echo "Need to precise in which mode who compute the result (mean or worst)"
    exit 1
else
    if [ "$3" == "worst" ] || [ "$3" == "mean" ];
    then
        mode=$3
    else
        echo "Invalid mode given as parameter (use mean or worst mode)"
        exit 1
    fi
fi

echo "Simulation process"
echo "Mode used: ${mode}  number of sample: ${simulation_sample}"

# Check if binaries of the conversion script is present
sample_generator=sample_generator
if [ ! -f "${binaries_folder}/${sample_generator}" ];
then
    echo "${binaries_folder}/${sample_generator} not found"
    exit 1
fi

idfix_float_binary_name=float_version
idfix_fixed_binary_name=fixed_version
if [ ! -f "${binaries_folder}/${idfix_float_binary_name}" ];
then
    echo "${binaries_folder}/${idfix_float_binary_name} not found"
    exit 1
fi
if [ ! -f "${binaries_folder}/${idfix_fixed_binary_name}" ];
then
    echo "${binaries_folder}/${idfix_fixed_binary_name} not found"
    exit 1
fi

output_folder=_output_simulation
generated_folder=${output_folder}/generated
log_folder=${output_folder}/log
if [ ! -d "${output_folder}" ];
then
    mkdir ${output_folder} ${generated_folder} ${log_folder}
else
    if [ ! -d "${generated_folder}" ];
    then
        mkdir ${generated_folder}
    fi
    if [ ! -d "${log_folder}" ];
    then
        mkdir ${log_folder}
    fi
fi


generated_sample_re=generated_sample_re
generated_sample_im=generated_sample_im
generated_steervect_re=generated_steervect_re
generated_steervect_im=generated_steervect_im
generated_matrix_re=generated_matrix_re
generated_matrix_im=generated_matrix_im
generated_decim_re=generated_decim_re
generated_decim_im=generated_decim_im
if [ -f "${generated_folder}/${generated_sample_re}" ];
then
    echo "delete ${generated_folder}/${generated_sample_re}"
    rm ${generated_folder}/${generated_sample_re}
fi
if [ -f "${generated_folder}/${generated_sample_im}" ];
then
    echo "delete ${generated_folder}/${generated_sample_im}"
    rm ${generated_folder}/${generated_sample_im}
fi
if [ -f "${generated_folder}/${generated_steervect_re}" ];
then
    echo "delete ${generated_folder}/${generated_steervect_re}"
    rm ${generated_folder}/${generated_steervect_re}
fi
if [ -f "${generated_folder}/${generated_steervect_im}" ];
then
    echo "delete ${generated_folder}/${generated_steervect_im}"
    rm ${generated_folder}/${generated_steervect_im}
fi
if [ -f "${generated_folder}/${generated_decim_re}" ];
then
    echo "delete ${generated_folder}/${generated_decim_re}"
    rm ${generated_folder}/${generated_decim_re}
fi
if [ -f "${generated_folder}/${generated_decim_im}" ];
then
    echo "delete ${generated_folder}/${generated_decim_im}"
    rm ${generated_folder}/${generated_decim_im}
fi
if [ -f "${generated_folder}/${generated_matrix_re}" ];
then
    echo "delete ${generated_folder}/${generated_matrix_re}"
    rm ${generated_folder}/${generated_matrix_re}
fi
if [ -f "${generated_folder}/${generated_matrix_im}" ];
then
    echo "delete ${generated_folder}/${generated_matrix_im}"
    rm ${generated_folder}/${generated_matrix_im}
fi


file_re_name=file_re.bin
file_im_name=file_im.bin
idfix_float_file_re_name=float_${file_re_name}
idfix_fixed_file_re_name=fixed_${file_re_name}
idfix_float_file_im_name=float_${file_im_name}
idfix_fixed_file_im_name=fixed_${file_im_name}
if [ -f "${generated_folder}/${idfix_float_file_re_name}" ];
then
    echo "delete ${binaries_folder}/${idfix_float_file_re_name}"
	rm ${generated_folder}/${idfix_float_file_re_name}
fi
if [ -f "${generated_folder}/${idfix_fixed_file_re_name}" ];
then
    echo "delete ${binaries_folder}/${idfix_fixed_file_re_name}"
	rm ${generated_folder}/${idfix_fixed_file_re_name}
fi
if [ -f "${generated_folder}/${idfix_float_file_im_name}" ];
then
    echo "delete ${binaries_folder}/${idfix_float_file_im_name}"
	rm ${generated_folder}/${idfix_float_file_im_name}
fi
if [ -f "${generated_folder}/${idfix_fixed_file_im_name}" ];
then
    echo "delete ${binaries_folder}/${idfix_fixed_file_im_name}"
	rm ${generated_folder}/${idfix_fixed_file_im_name}
fi




if [ -f "${generated_folder}/${idfix_float_file_re_name}_PulseCompression_out" ];
then
    echo "delete ${binaries_folder}/${idfix_float_file_re_name}_PulseCompression_out"
	rm ${generated_folder}/${idfix_float_file_re_name}_PulseCompression_out
fi
if [ -f "${generated_folder}/${idfix_float_file_im_name}_PulseCompression_out" ];
then
    echo "delete ${binaries_folder}/${idfix_float_file_im_name}_PulseCompression_out"
	rm ${generated_folder}/${idfix_float_file_im_name}_PulseCompression_out
fi
if [ -f "${generated_folder}/${idfix_float_file_re_name}_X_2_out" ];
then
    echo "delete ${binaries_folder}/${idfix_float_file_re_name}_X_2_out"
	rm ${generated_folder}/${idfix_float_file_re_name}_X_2_out
fi
if [ -f "${generated_folder}/${idfix_float_file_im_name}_X_2_out" ];
then
    echo "delete ${binaries_folder}/${idfix_float_file_im_name}_X_2_out"
	rm ${generated_folder}/${idfix_float_file_im_name}_X_2_out
fi
if [ -f "${generated_folder}/${idfix_float_file_re_name}_CovarCalculation_out" ];
then
    echo "delete ${binaries_folder}/${idfix_float_file_re_name}_CovarCalculation_out"
	rm ${generated_folder}/${idfix_float_file_re_name}_CovarCalculation_out
fi
if [ -f "${generated_folder}/${idfix_float_file_im_name}_CovarCalculation_out" ];
then
    echo "delete ${binaries_folder}/${idfix_float_file_im_name}_CovarCalculation_out"
	rm ${generated_folder}/${idfix_float_file_im_name}_CovarCalculation_out
fi
if [ -f "${generated_folder}/${idfix_float_file_re_name}_Matrix_invert_out" ];
then
    echo "delete ${binaries_folder}/${idfix_float_file_re_name}_Matrix_invert_out"
	rm ${generated_folder}/${idfix_float_file_re_name}_Matrix_invert_out
fi
if [ -f "${generated_folder}/${idfix_float_file_im_name}_Matrix_invert_out" ];
then
    echo "delete ${binaries_folder}/${idfix_float_file_im_name}_Matrix_invert_out"
	rm ${generated_folder}/${idfix_float_file_im_name}_Matrix_invert_out
fi
if [ -f "${generated_folder}/${idfix_float_file_re_name}_Weight_Calculation_out" ];
then
    echo "delete ${binaries_folder}/${idfix_float_file_re_name}_Weight_Calculation_out"
	rm ${generated_folder}/${idfix_float_file_re_name}_Weight_Calculation_out
fi
if [ -f "${generated_folder}/${idfix_float_file_im_name}_Weight_Calculation_out" ];
then
    echo "delete ${binaries_folder}/${idfix_float_file_im_name}_Weight_Calculation_out"
	rm ${generated_folder}/${idfix_float_file_im_name}_Weight_Calculation_out
fi
if [ -f "${generated_folder}/${idfix_float_file_re_name}_X_3_out" ];
then
    echo "delete ${binaries_folder}/${idfix_float_file_re_name}_X_3_out"
	rm ${generated_folder}/${idfix_float_file_re_name}_X_3_out
fi
if [ -f "${generated_folder}/${idfix_float_file_im_name}_X_3_out" ];
then
    echo "delete ${binaries_folder}/${idfix_float_file_im_name}_X_3_out"
	rm ${generated_folder}/${idfix_float_file_im_name}_X_3_out
fi
if [ -f "${generated_folder}/${idfix_float_file_re_name}_ApplyAdaptiveFilter_out" ];
then
    echo "delete ${binaries_folder}/${idfix_float_file_re_name}_ApplyAdaptiveFilter_out"
	rm ${generated_folder}/${idfix_float_file_re_name}_ApplyAdaptiveFilter_out
fi
if [ -f "${generated_folder}/${idfix_float_file_im_name}_ApplyAdaptiveFilter_out" ];
then
    echo "delete ${binaries_folder}/${idfix_float_file_im_name}_ApplyAdaptiveFilter_out"
	rm ${generated_folder}/${idfix_float_file_im_name}_ApplyAdaptiveFilter_out
fi


if [ -f "${generated_folder}/${idfix_fixed_file_re_name}_PulseCompression_out" ];
then
    echo "delete ${binaries_folder}/${idfix_fixed_file_re_name}_PulseCompression_out"
	rm ${generated_folder}/${idfix_fixed_file_re_name}_PulseCompression_out
fi
if [ -f "${generated_folder}/${idfix_fixed_file_im_name}_PulseCompression_out" ];
then
    echo "delete ${binaries_folder}/${idfix_fixed_file_im_name}_PulseCompression_out"
	rm ${generated_folder}/${idfix_fixed_file_im_name}_PulseCompression_out
fi
if [ -f "${generated_folder}/${idfix_fixed_file_re_name}_X_2_out" ];
then
    echo "delete ${binaries_folder}/${idfix_fixed_file_re_name}_X_2_out"
	rm ${generated_folder}/${idfix_fixed_file_re_name}_X_2_out
fi
if [ -f "${generated_folder}/${idfix_fixed_file_im_name}_X_2_out" ];
then
    echo "delete ${binaries_folder}/${idfix_fixed_file_im_name}_X_2_out"
	rm ${generated_folder}/${idfix_fixed_file_im_name}_X_2_out
fi
if [ -f "${generated_folder}/${idfix_fixed_file_re_name}_CovarCalculation_out" ];
then
    echo "delete ${binaries_folder}/${idfix_fixed_file_re_name}_CovarCalculation_out"
	rm ${generated_folder}/${idfix_fixed_file_re_name}_CovarCalculation_out
fi
if [ -f "${generated_folder}/${idfix_fixed_file_im_name}_CovarCalculation_out" ];
then
    echo "delete ${binaries_folder}/${idfix_fixed_file_im_name}_CovarCalculation_out"
	rm ${generated_folder}/${idfix_fixed_file_im_name}_CovarCalculation_out
fi
if [ -f "${generated_folder}/${idfix_fixed_file_re_name}_Matrix_invert_out" ];
then
    echo "delete ${binaries_folder}/${idfix_fixed_file_re_name}_Matrix_invert_out"
	rm ${generated_folder}/${idfix_fixed_file_re_name}_Matrix_invert_out
fi
if [ -f "${generated_folder}/${idfix_fixed_file_im_name}_Matrix_invert_out" ];
then
    echo "delete ${binaries_folder}/${idfix_fixed_file_im_name}_Matrix_invert_out"
	rm ${generated_folder}/${idfix_fixed_file_im_name}_Matrix_invert_out
fi
if [ -f "${generated_folder}/${idfix_fixed_file_re_name}_Weight_Calculation_out" ];
then
    echo "delete ${binaries_folder}/${idfix_fixed_file_re_name}_Weight_Calculation_out"
	rm ${generated_folder}/${idfix_fixed_file_re_name}_Weight_Calculation_out
fi
if [ -f "${generated_folder}/${idfix_fixed_file_im_name}_Weight_Calculation_out" ];
then
    echo "delete ${binaries_folder}/${idfix_fixed_file_im_name}_Weight_Calculation_out"
	rm ${generated_folder}/${idfix_fixed_file_im_name}_Weight_Calculation_out
fi
if [ -f "${generated_folder}/${idfix_fixed_file_re_name}_X_3_out" ];
then
    echo "delete ${binaries_folder}/${idfix_fixed_file_re_name}_X_3_out"
	rm ${generated_folder}/${idfix_fixed_file_re_name}_X_3_out
fi
if [ -f "${generated_folder}/${idfix_fixed_file_im_name}_X_3_out" ];
then
    echo "delete ${binaries_folder}/${idfix_fixed_file_im_name}_X_3_out"
	rm ${generated_folder}/${idfix_fixed_file_im_name}_X_3_out
fi
if [ -f "${generated_folder}/${idfix_fixed_file_re_name}_ApplyAdaptiveFilter_out" ];
then
    echo "delete ${binaries_folder}/${idfix_fixed_file_re_name}_ApplyAdaptiveFilter_out"
	rm ${generated_folder}/${idfix_fixed_file_re_name}_ApplyAdaptiveFilter_out
fi
if [ -f "${generated_folder}/${idfix_fixed_file_im_name}_ApplyAdaptiveFilter_out" ];
then
    echo "delete ${binaries_folder}/${idfix_fixed_file_im_name}_ApplyAdaptiveFilter_out"
	rm ${generated_folder}/${idfix_fixed_file_im_name}_ApplyAdaptiveFilter_out
fi


export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${systemc_library}
sample_generator_log=sample_generator.log
idfix_float_log=float.log
idfix_fixed_log=fixed.log


for I in `seq 1 ${simulation_sample}`
do
    echo "$I/${simulation_sample}" >> ${log_folder}/${sample_generator_log}
	./${binaries_folder}/${sample_generator} ${generated_folder}/${generated_sample_re} ${generated_folder}/${generated_sample_im} \
		${generated_folder}/${generated_decim_re} ${generated_folder}/${generated_decim_im} \
		${generated_folder}/${generated_steervect_re} ${generated_folder}/${generated_steervect_im} \
		${generated_folder}/${generated_matrix_re} ${generated_folder}/${generated_matrix_im} >> ${log_folder}/${sample_generator_log}
   #float
    ./${binaries_folder}/${idfix_float_binary_name} ${generated_folder}/${generated_sample_re} ${generated_folder}/${generated_sample_im} \
		${generated_folder}/${generated_decim_re} ${generated_folder}/${generated_decim_im} \
		${generated_folder}/${generated_steervect_re} ${generated_folder}/${generated_steervect_im} \
		${generated_folder}/${generated_matrix_re} ${generated_folder}/${generated_matrix_im} \
        ${generated_folder}/${idfix_float_file_re_name} ${generated_folder}/${idfix_float_file_im_name}>> ${log_folder}/${idfix_float_log}
    #fixed
    ./${binaries_folder}/${idfix_fixed_binary_name} ${generated_folder}/${generated_sample_re} ${generated_folder}/${generated_sample_im} \
    	${generated_folder}/${generated_decim_re} ${generated_folder}/${generated_decim_im} \
		${generated_folder}/${generated_steervect_re} ${generated_folder}/${generated_steervect_im} \
		${generated_folder}/${generated_matrix_re} ${generated_folder}/${generated_matrix_im} \
        ${generated_folder}/${idfix_fixed_file_re_name} ${generated_folder}/${idfix_fixed_file_im_name} >> ${log_folder}/${idfix_fixed_log} 2>&1
done

controller=tools/controller/controller
echo "Result ${generated_folder}/${idfix_fixed_file_re_name}_PulseCompression_out ${generated_folder}/${idfix_fixed_file_re_name}_PulseCompression_out"
./${controller} ${mode} ${simulation_sample} ${generated_folder}/${idfix_float_file_re_name}_PulseCompression_out ${generated_folder}/${idfix_fixed_file_re_name}_PulseCompression_out
echo "Result ${generated_folder}/${idfix_fixed_file_im_name}_PulseCompression_out ${generated_folder}/${idfix_fixed_file_im_name}_PulseCompression_out"
./${controller} ${mode} ${simulation_sample} ${generated_folder}/${idfix_float_file_im_name}_PulseCompression_out ${generated_folder}/${idfix_fixed_file_im_name}_PulseCompression_out
echo
echo "Result ${generated_folder}/${idfix_fixed_file_re_name}_X_2_out ${generated_folder}/${idfix_fixed_file_re_name}_X_2_out"
./${controller} ${mode} ${simulation_sample} ${generated_folder}/${idfix_float_file_re_name}_X_2_out ${generated_folder}/${idfix_fixed_file_re_name}_X_2_out
echo "Result ${generated_folder}/${idfix_fixed_file_im_name}_X_2_out ${generated_folder}/${idfix_fixed_file_im_name}_X_2_out"
./${controller} ${mode} ${simulation_sample} ${generated_folder}/${idfix_float_file_im_name}_X_2_out ${generated_folder}/${idfix_fixed_file_im_name}_X_2_out
echo
echo "Result ${generated_folder}/${idfix_fixed_file_re_name}_CovarCalculation_out ${generated_folder}/${idfix_fixed_file_re_name}_CovarCalculation_out"
./${controller} ${mode} ${simulation_sample} ${generated_folder}/${idfix_float_file_re_name}_CovarCalculation_out ${generated_folder}/${idfix_fixed_file_re_name}_CovarCalculation_out
echo "Result ${generated_folder}/${idfix_fixed_file_im_name}_CovarCalculation_out ${generated_folder}/${idfix_fixed_file_im_name}_CovarCalculation_out"
./${controller} ${mode} ${simulation_sample} ${generated_folder}/${idfix_float_file_im_name}_CovarCalculation_out ${generated_folder}/${idfix_fixed_file_im_name}_CovarCalculation_out
echo
echo "Result ${generated_folder}/${idfix_fixed_file_re_name}_Matrix_invert_out ${generated_folder}/${idfix_fixed_file_re_name}_Matrix_invert_out"
./${controller} ${mode} ${simulation_sample} ${generated_folder}/${idfix_float_file_re_name}_Matrix_invert_out ${generated_folder}/${idfix_fixed_file_re_name}_Matrix_invert_out
echo "Result ${generated_folder}/${idfix_fixed_file_im_name}_Matrix_invert_out ${generated_folder}/${idfix_fixed_file_im_name}_Matrix_invert_out"
./${controller} ${mode} ${simulation_sample} ${generated_folder}/${idfix_float_file_im_name}_Matrix_invert_out ${generated_folder}/${idfix_fixed_file_im_name}_Matrix_invert_out
echo
echo "Result ${generated_folder}/${idfix_fixed_file_re_name}_Weight_Calculation_out ${generated_folder}/${idfix_fixed_file_re_name}_Weight_Calculation_out"
./${controller} ${mode} ${simulation_sample} ${generated_folder}/${idfix_float_file_re_name}_Weight_Calculation_out ${generated_folder}/${idfix_fixed_file_re_name}_Weight_Calculation_out
echo "Result ${generated_folder}/${idfix_fixed_file_im_name}_Weight_Calculation_out ${generated_folder}/${idfix_fixed_file_im_name}_Weight_Calculation_out"
./${controller} ${mode} ${simulation_sample} ${generated_folder}/${idfix_float_file_im_name}_Weight_Calculation_out ${generated_folder}/${idfix_fixed_file_im_name}_Weight_Calculation_out
echo
echo "Result ${generated_folder}/${idfix_fixed_file_re_name}_X_3_out ${generated_folder}/${idfix_fixed_file_re_name}_X_3_out"
./${controller} ${mode} ${simulation_sample} ${generated_folder}/${idfix_float_file_re_name}_X_3_out ${generated_folder}/${idfix_fixed_file_re_name}_X_3_out
echo "Result ${generated_folder}/${idfix_fixed_file_im_name}_X_3_out ${generated_folder}/${idfix_fixed_file_im_name}_X_3_out"
./${controller} ${mode} ${simulation_sample} ${generated_folder}/${idfix_float_file_im_name}_X_3_out ${generated_folder}/${idfix_fixed_file_im_name}_X_3_out
echo
echo "Result ${generated_folder}/${idfix_fixed_file_re_name}_ApplyAdaptiveFilter_out ${generated_folder}/${idfix_fixed_file_re_name}_ApplyAdaptiveFilter_out"
./${controller} ${mode} ${simulation_sample} ${generated_folder}/${idfix_float_file_re_name}_ApplyAdaptiveFilter_out ${generated_folder}/${idfix_fixed_file_re_name}_ApplyAdaptiveFilter_out
echo "Result ${generated_folder}/${idfix_fixed_file_im_name}_ApplyAdaptiveFilter_out ${generated_folder}/${idfix_fixed_file_im_name}_ApplyAdaptiveFilter_out"
./${controller} ${mode} ${simulation_sample} ${generated_folder}/${idfix_float_file_im_name}_ApplyAdaptiveFilter_out ${generated_folder}/${idfix_fixed_file_im_name}_ApplyAdaptiveFilter_out
echo
echo

echo "Result ${generated_folder}/${idfix_fixed_file_re_name} ${generated_folder}/${idfix_fixed_file_re_name}"
./${controller} ${mode} ${simulation_sample} ${generated_folder}/${idfix_float_file_re_name} ${generated_folder}/${idfix_fixed_file_re_name}
echo
echo "Result ${generated_folder}/${idfix_float_file_im_name} ${generated_folder}/${idfix_float_file_im_name}"
./${controller} ${mode} ${simulation_sample} ${generated_folder}/${idfix_float_file_im_name} ${generated_folder}/${idfix_fixed_file_im_name}

exit
