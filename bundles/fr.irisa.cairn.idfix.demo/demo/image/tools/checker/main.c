
#include <iostream>
#include <string>
#include <fstream>
#include <cmath>

int main(int argc, char *argv[]){
	if(argc != 3){
		std::cerr << "bad parameters" << std::endl;
		return 0;
	}

	std::string filepath1 = argv[1];
	std::string filepath2 = argv[2];

	std::cout << "file 1 => " << filepath1 << std::endl;
	std::cout << "file 2 => " << filepath2 << std::endl;

	std::ifstream is1, is2;

	// Open and read file1
	is1.open(filepath1.c_str(), std::ifstream::binary);
	if(!is1.is_open()){
		std::cerr << "File 1 not open" << std::endl;
		return 0;
	}

	is1.seekg (0, std::ifstream::end);
	int length1 = is1.tellg();
	is1.seekg (0, std::ifstream::beg);
	float *buffer1 = new float [length1/sizeof(float)];
	is1.read((char*)buffer1,length1);
	is1.close();

	// Open and read file2
	is2.open(filepath2.c_str(), std::ifstream::binary);
	if(!is2.is_open()){
		std::cerr << "File 2 not open" << std::endl;
		return 0;
	}

	is2.seekg (0, std::ifstream::end);
	int length2 = is2.tellg();
	is2.seekg (0, std::ifstream::beg);
	float *buffer2 = new float [length2/sizeof(float)];
	is2.read((char*)buffer2,length2);
	is2.close();

	if(length1 != length2){
		std::cerr << "Size not equal between the two file" << std::endl;

		delete [] buffer1;
		delete [] buffer2;

		return 0;
	}

	int difference_number = 0;
	float max_diff = 0.;

	for(int i = 0 ; i < length1/sizeof(float) ; i++){
		if(buffer1[i] != buffer2[i]){
			difference_number++;
			float diff = std::abs(buffer1[i] - buffer2[i]);
			if(max_diff < diff)
				max_diff = diff;
		}
	}

	std::cout << "Number of diff = " << difference_number << std::endl;
	std::cout << "Max diff = " << max_diff << std::endl;


	delete [] buffer1;
	delete [] buffer2;
	return 0;
}
