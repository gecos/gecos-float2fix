#include <iostream>
#include <string>
#include <fstream>
#include <cmath>
#include <stdexcept>

struct Result {
    double meanT;
    double variance;
    double noisePower;
    double noisePowerDB;
};

struct Info {
    double sum = 0.;
    double sum2 = 0.;
    double max_diff = 0.;
};

int main(int argc, char *argv[]){
    if(argc != 5){
        std::cerr << "bad parameters" << std::endl;
        return -1;
    }

    std::string mode = argv[1];
    if(!(mode.compare("mean") == 0 || mode.compare("worst") == 0)){
        std::cerr << "Incorrect mode specified (use mean or worst mode)" << std::endl;
        return -1;
    }

    int sample_number;
    try{
        sample_number = std::stoi(argv[2], NULL);
    } catch(std::invalid_argument e){
        std::cerr << "Invalid argument 2, need to have the number of simulation sample" << std::endl;
        return -1;
    }

    std::string filepath1 = argv[3];
    std::string filepath2 = argv[4];
    std::cout << "file 1 => " << filepath1 << std::endl;
    std::cout << "file 2 => " << filepath2 << std::endl;

    std::ifstream is1, is2;
    // Open and read file1
    is1.open(filepath1.c_str(), std::ifstream::binary);
    if(!is1.is_open()){
        std::cerr << "File 1 not open" << std::endl;
        return -1;
    }

    is2.open(filepath2.c_str(), std::ifstream::binary);
    if(!is2.is_open()){
        std::cerr << "File 2 not open" << std::endl;
        return -1;
    }

	is1.seekg (0, std::ifstream::end);
	int length1 = is1.tellg();
	is1.seekg (0, std::ifstream::beg);

	is2.seekg (0, std::ifstream::end);
	int length2 = is2.tellg();
	is2.seekg (0, std::ifstream::beg);

    if(length1 != length2){
		std::cerr << "Size not equal between the two file" << std::endl;
        return -1;
    }

    int element_number = length1/(sample_number*sizeof(float));
    float *float_value = new float[element_number];
    float *fixed_value = new float[element_number];

    Info *value_info = new Info[element_number];

    for(int i = 0 ; i < sample_number ; i++){
	    is1.read((char*)float_value, element_number*sizeof(float));
	    is2.read((char*)fixed_value, element_number*sizeof(float));

        for(int j = 0 ; j < element_number ; j++){
            double diff = float_value[j] - fixed_value[j];
            double temp = std::abs(diff);
            if(value_info[j].max_diff < temp){
                value_info[j].max_diff = temp;
            }

            value_info[j].sum += diff;
            value_info[j].sum2 += diff*diff;
        }
    }
	is1.close();
    is2.close();

    Result *value_result = new Result[element_number];
    for(int i = 0 ; i < element_number ; i++){
        value_result[i].meanT = value_info[i].sum/sample_number;
        value_result[i].variance = (value_info[i].sum2/sample_number) - (value_result[i].meanT * value_result[i].meanT);
        value_result[i].noisePower = value_info[i].sum2/sample_number;
        value_result[i].noisePowerDB = 10.*log10(value_result[i].noisePower);
    }


    Result final_result;
    if(mode.compare("worst") == 0){
        double max_noisePower = value_result[0].noisePowerDB;
        final_result = value_result[0];
        for(int i = 0 ; i < element_number ; i++){
            if(value_result[i].noisePowerDB > max_noisePower){
                max_noisePower = value_result[i].noisePowerDB;
                final_result = value_result[i];
            }
        }
    }
    else{
        double meanT = 0., variance = 0., noisePower = 0.;
        for(int i = 0 ; i < element_number ; i++){
             meanT += value_result[i].meanT;
             variance += value_result[i].variance;
             noisePower += value_result[i].noisePower;
        }

        final_result.meanT = meanT / element_number;
        final_result.variance = variance / element_number;
        final_result.noisePower = noisePower / element_number;
        final_result.noisePowerDB = 10.*log10(final_result.noisePower);
    }
    std::cout << "Mode used : " << mode << std::endl;
    std::cout << "Number of simulation : " << sample_number << std::endl;
    std::cout << "Simulation noise power (db) : " << final_result.noisePowerDB << std::endl;
    std::cout << "Simulation error mean : " << final_result.meanT << std::endl;
    std::cout << "Simulation error variance : " << final_result.variance << std::endl;

    return 0;
}
