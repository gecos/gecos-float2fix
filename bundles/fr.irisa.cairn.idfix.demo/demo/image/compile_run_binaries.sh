#!/bin/sh

data_type=$1
image_input=$2
image_output=$3

# User paramaters
makeproject_path=./regen/float2fix_version/make_project
ac_include=./tools/ac_types/
systemc_include=./tools/systemc-2.3.0/bin/include
systemc_library=./tools/systemc-2.3.0/bin/lib-linux64

controller_bin=./tools/controller/controller

output_folder=regen/build
binaries_folder=${output_folder}/bin
generated_folder=${output_folder}/generated
log_folder=${output_folder}/log

if [ ! -d "${output_folder}" ];
then
    mkdir -p ${output_folder} ${binaries_folder} ${generated_folder} ${log_folder}
else
    if [ ! -d "${binaries_folder}" ];
    then
        mkdir ${binaries_folder}
    fi
    if [ ! -d "${generated_folder}" ];
    then
        mkdir ${generated_folder}
    fi
    if [ ! -d "${log_folder}" ];
    then
        mkdir ${log_folder}
    fi
fi

# Float and fixed version
# Delete temporary folder binary and output file of the last run
# And compile
echo ""
echo "1/3 - Run fixed conversion and and compile of the float/fixed version"
idfix_float_binary_name=float_version
idfix_fixed_binary_name=fixed_version
idfix_float_log=float.log
idfix_fixed_log=fixed.log
if [ -f "${binaries_folder}/${idfix_float_binary_name}" ];
then
    echo "delete ${binaries_folder}/${idfix_float_binary_name}"
    rm ${binaries_folder}/${idfix_float_binary_name}
fi
if [ -f "${binaries_folder}/${idfix_fixed_binary_name}" ];
then
    echo "delete ${binaries_folder}/${idfix_fixed_binary_name}"
    rm ${binaries_folder}/${idfix_fixed_binary_name}
fi
if [ -f "${image_output}_float" ];
then
    echo "delete ${image_output}_float"
    rm ${image_output}_float
fi
if [ -f "${image_output}_bin_float" ];
then
    echo "delete ${image_output}_bin_float"
    rm ${image_output}_bin_float
fi
if [ -f "${image_output}_fixed" ];
then
    echo "delete ${image_output}_fixed"
    rm ${image_output}_fixed
fi
if [ -f "${image_output}_bin_fixed" ];
then
    echo "delete ${image_output}_bin_fixed"
    rm ${image_output}_bin_fixed
fi

g++ -g -I${ac_include} \
    -D${data_type} ${makeproject_path}/fixed/*.c -lm -o fixed_version 2>&1 | tee -a ${log_folder}/compilation_fixed.log 
	
g++ -g -DFLOAT_VERSION ${makeproject_path}/float/*.c -lm -o float_version 2>&1 | tee -a ${log_folder}/compilation_float.log

if [ ! -f "${idfix_float_binary_name}" ];
then
    echo "${binaries_folder}/${idfix_float_binary_name} not found"
    exit 1
fi
if [ ! -f "${idfix_fixed_binary_name}" ];
then
    echo "${binaries_folder}/${idfix_fixed_binary_name} not found"
    exit 1
fi

mv ${idfix_float_binary_name} ${binaries_folder}
mv ${idfix_fixed_binary_name} ${binaries_folder}


echo "run ./${binaries_folder}/${idfix_float_binary_name} ${image_input} ${image_output}_float ${image_output}_bin_float ${image_output}_gauss_float"
./${binaries_folder}/${idfix_float_binary_name} ${image_input} ${image_output}_float ${image_output}_bin_float ${image_output}_gauss_float 2>&1 | tee -a ${log_folder}/${idfix_float_log}

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${systemc_library}
echo "run ./${binaries_folder}/${idfix_fixed_binary_name} ${image_input} ${image_output}_fixed ${image_output}_bin_fixed ${image_output}_gauss_fixed"
./${binaries_folder}/${idfix_fixed_binary_name} ${image_input} ${image_output}_fixed ${image_output}_bin_fixed ${image_output}_gauss_fixed 2>&1 | tee -a ${log_folder}/${idfix_fixed_log}

echo ""
echo "2/3 - Checker "
echo "./tools/checker/a.out ${image_output}_bin_float ${image_output}_bin_fixed"
./tools/checker/a.out ${image_output}_bin_float ${image_output}_bin_fixed

echo ""
echo "3/3 - Controller"
echo "./${controller_bin} ${image_output}_bin_float ${image_output}_bin_fixed"
./${controller_bin} worst 262144 ${image_output}_bin_float ${image_output}_bin_fixed
echo ""

exit
