#define MC	3
#define NC	3


#pragma NOISE_CONSTRAINT -50
#pragma MAIN_FUNC
float imag2d_gauss(
#pragma DYNAMIC[0, 255]
	float in[MC][NC]){

	float coef[MC][NC] = {
		0.044919223845157216, 0.12210310992677306, 0.044919223845157216,
		0.12210310992677306, 0.33191066491227894, 0.12210310992677306,
		0.044919223845157216, 0.12210310992677306, 0.044919223845157216
	};

	int i, j;

#pragma OUTPUT
	float out = 0;

	for(i = 0; i < MC; i++) {
		for(j = 0; j < NC; j++) {
			out += in[i][j] * coef[i][j];
		}
	}

    return out;
}
