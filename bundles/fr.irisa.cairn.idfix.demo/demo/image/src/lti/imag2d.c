#define MC	3
#define NC	3


#pragma NOISE_CONSTRAINT -20
#pragma MAIN_FUNC
float imag2d(
#pragma DYNAMIC[0, 255]
	float in[MC][NC]){

	float coef[MC][NC] = {{0.95, 1.95, 0.95}, {1.95, -11.6, 1.95}, {0.95, 1.95, 0.95}};

	int i, j;

#pragma OUTPUT
	float out = 0;

	for(i = 0; i < MC; i++) {
		for(j = 0; j < NC; j++) {
			out += in[i][j] * coef[i][j];
		}
	}

    return out;
}
