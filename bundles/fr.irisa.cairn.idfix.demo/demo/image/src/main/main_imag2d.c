#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#include "header.h"

#ifdef FLOAT_VERSION
#elif SCFIXED_VERSION
	#define SC_INCLUDE_FX
	#include "systemc.h"
#elif ACFIXED_VERSION
#else
	#error "Need to precise which version to compile (ACFIXED_VERSION, SCFIXED_VERSION, or FLOAT_VERSION)"
#endif

#define MC 	3
#define NC 	3

#ifdef SCFIXED_VERSION
	int sc_main(int argc, char *argv[]) {

#else
	int main(int argc, const char **argv) {
#endif

	int i,j,a,b;
	char tmp[128];
	FILE *outfile, *outfilegauss, *outfile_binary, *infile;
	int N, M;

	/* check parameters */
	if(argc < 5) {
		fprintf(stderr, "Usage: %s input_file output_file\n", argv[0]);
		exit(-1);
	}

	infile = fopen(argv[1], "r");
	if(infile == NULL) {
		fprintf(stderr, "couldn't open file \"%s\"!\n", argv[1]);
		exit(-2);
	}

	outfile = fopen(argv[2], "w");
	if(outfile == NULL) {
		fprintf(stderr, "couldn't open file \"%s\"!\n", argv[2]);
		exit(-2);
	}

	outfile_binary = fopen(argv[3], "wb");
	if(outfile_binary == NULL) {
		fprintf(stderr, "couldn't open file \"%s\"!\n", argv[3]);
		exit(-2);
	}

	outfilegauss = fopen(argv[4], "w");
	if(outfilegauss == NULL) {
		fprintf(stderr, "couldn't open file \"%s\"!\n", argv[2]);
		exit(-2);
	}

	/* init data */
	fgets(tmp, 128, infile);
	N = atoi(tmp);
	fgets(tmp, 128, infile);
	M = atoi(tmp);

	{
		float in[M][N];
		float out[M][N];
		float out_gauss[M][N];
		float tmpout;

		FIX_imag2d_gauss_in tmpin_gauss[MC][NC];
		FIX_imag2d_in tmpin_edge[MC][NC];


		for(i=0; i<M; i++) {
			for(j=0; j<N; j++) {
				fgets(tmp, 128, infile);
				in[i][j] = (float) atof(tmp);
				out[i][j] = 0;
			}
		}
		fclose(infile);


		for(i=1; i<M-1; i++) {
			for(j=1; j<N-1; j++) {
				// init tmpin
				for(a=0; a<MC; a++)
					for(b=0; b<NC; b++)
						tmpin_gauss[a][b]= in[i-MC/2 + a][j-NC/2 + b];

#ifdef ACFIXED_VERSION
				tmpout = imag2d_gauss(tmpin_gauss).to_double();
#else
				tmpout = imag2d_gauss(tmpin_gauss);
#endif

				if(tmpout > 255)
					out_gauss[i][j] = 255;
				else if(tmpout < 0)
					out_gauss[i][j] = 0;
				else
					out_gauss[i][j] = tmpout;
			}
		}

		/* 2- call function */
		for(i=1; i<M-1; i++) {
			for(j=1; j<N-1; j++) {
				// init tmpin
				for(a=0; a<MC; a++)
					for(b=0; b<NC; b++)
						tmpin_edge[a][b]= out_gauss[i-MC/2 + a][j-NC/2 + b];

#ifdef ACFIXED_VERSION
				tmpout = imag2d(tmpin_edge).to_double();
#else
				tmpout = imag2d(tmpin_edge);
#endif

				if(tmpout > 255)
					out[i][j] = 255;
				else if(tmpout < 0)
					out[i][j] = 0;
				else
					out[i][j] = tmpout;
			}
		}

		/* output result */
		fprintf(outfile, "%d\n%d\n", M, N);
		fprintf(outfilegauss, "%d\n%d\n", M, N);
		for (i = 0; i < M; i++) {
			for (j = 0; j < N; j++) {
				fprintf(outfilegauss, "%f", out_gauss[i][j]);
				fprintf(outfile, "%f", out[i][j]);
				fwrite(&out[i][j], sizeof(float), 1, outfile_binary);
				fprintf(outfile, "\n");
				fprintf(outfilegauss, "\n");
			}
		}
		fclose(outfile);
		fclose(outfilegauss);
		fclose(outfile_binary);
	}

	return 0;
}
