debug(1);

#GenInputFromImage("./images/", "./2dimages");


lti_source_folder="src/lti";
notlti_source_folder="src/non_lti";

output_folder="regen/float2fix_version/make_project";
operator_library="utils/real_architecture_model.xml";
user_configuration="userconfiguration.cfg";

# Make the conversion and generate header needed
MakeProject(lti_source_folder, notlti_source_folder, output_folder, user_configuration, operator_library);

echo("Done!");