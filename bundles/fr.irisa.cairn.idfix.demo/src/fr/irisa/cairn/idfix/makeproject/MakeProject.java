package fr.irisa.cairn.idfix.makeproject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.irisa.cairn.gecos.model.c.generator.ExtendableCoreCGenerator;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableTypeCGenerator;
import fr.irisa.cairn.idfix.genimage.GenDiffBetweenOutputs;
import fr.irisa.cairn.idfix.genimage.GenImageFromOutputs;
import fr.irisa.cairn.idfix.utils.IDFixUtils;
import fr.irisa.r2d2.gecos.framework.utils.SystemExec;
import gecos.annotations.PragmaAnnotation;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;

public class MakeProject {
	private String _userConfigurationPath;
	private File _sourceLTIFolder, _sourceNotLTIFolder, _outputFolder, _idfixOutputFolder, _fixedOutputFolder, _floatOutputFolder;
	private String _operator_library;
	private File floatHeaderFile, fixedHeaderFile;
	
	public MakeProject(String sourceLTIFolderPath, String sourceNotLTIFolderPath, String outputFolderPath, String userConfigurationPath, String operator_library){
		_userConfigurationPath = userConfigurationPath;
		_operator_library = operator_library;
		System.out.println(_userConfigurationPath);
		System.out.println(_operator_library);
		_sourceLTIFolder = new File(sourceLTIFolderPath);
		if(!_sourceLTIFolder.isDirectory())
			throw new RuntimeException("The source folder " + sourceLTIFolderPath + " not found or not exist");
		
		_sourceNotLTIFolder = new File(sourceNotLTIFolderPath);
		if(!_sourceNotLTIFolder.isDirectory())
			throw new RuntimeException("The source folder " + sourceNotLTIFolderPath + " not found or not exist");
		
		_outputFolder = new File(outputFolderPath);
		if(!_outputFolder.isDirectory())
			if(!_outputFolder.mkdirs())
				throw new RuntimeException("Impossible to create output directory");
		
		_idfixOutputFolder = new File(outputFolderPath, "idfix");
		if(!_idfixOutputFolder.isDirectory())
			if(!_idfixOutputFolder.mkdirs())
				throw new RuntimeException("Impossible to create idfix output directory");
		
		_fixedOutputFolder = new File(outputFolderPath, "fixed");
		if(!_fixedOutputFolder.isDirectory())
			if(!_fixedOutputFolder.mkdirs())
				throw new RuntimeException("Impossible to create output fixed source directory");
		
		_floatOutputFolder = new File(outputFolderPath, "float");
		if(!_floatOutputFolder.isDirectory())
			if(!_floatOutputFolder.mkdirs())
				throw new RuntimeException("Impossible to create output float source directory");
	}
	
	public void compute() throws IOException{
		// Float header
		floatHeaderFile = new File(_floatOutputFolder, "header.h");
		String headername = floatHeaderFile.getName().split("\\.")[0];
		PrintWriter floatPrinter = new PrintWriter(floatHeaderFile);
		floatPrinter.println("#ifndef _" + headername + "_H_");
		floatPrinter.println("#define _" + headername + "_H_");
		floatPrinter.println();
		
		// Fixed header
		fixedHeaderFile = new File(_fixedOutputFolder, "header.h");
		headername = fixedHeaderFile.getName().split("\\.")[0];
		PrintWriter fixedPrinter = new PrintWriter(fixedHeaderFile);
		fixedPrinter.println("#ifndef _" + headername + "_H_");
		fixedPrinter.println("#define _" + headername + "_H_");
		
		fixedPrinter.println("#ifdef SCFIXED_VERSION");
		fixedPrinter.println("#define SC_INCLUDE_FX");
		fixedPrinter.println("#include \"systemc.h\"");
		fixedPrinter.println("#elif ACFIXED_VERSION");
		fixedPrinter.println("#include <ac_fixed.h>");
		fixedPrinter.println("#endif");
		fixedPrinter.println();
		
		fixedPrinter.close();
		floatPrinter.close();
		
		// Loop on each source file (.c extension) contain in the source folder to write
		// in float and fixed header the alias for each parameter type of each procedure.
		runFlowOnFolder(floatPrinter, fixedPrinter, _sourceLTIFolder, 0);
		runFlowOnFolder(floatPrinter, fixedPrinter, _sourceNotLTIFolder, 1000);
		
		floatPrinter = new PrintWriter(new FileOutputStream(floatHeaderFile, true));
		fixedPrinter = new PrintWriter(new FileOutputStream(fixedHeaderFile, true));
		fixedPrinter.println("#endif");
		fixedPrinter.close();
		floatPrinter.println("#endif");
		floatPrinter.close();
		
		
		/* Compile original, base, float/fixed version and run binaries */
		String imgOutDir = "regen/build/generated/";
		
		SystemExec.shell("./compile_run_binaries.sh ACFIXED_VERSION 2dimages/lena_512x512.input  " + imgOutDir+"lena_512x512.output");

		System.out.println("Images generation...");
		new GenImageFromOutputs(imgOutDir + "lena_512x512.output_gauss_float", imgOutDir + "regen_gauss_image_float").compute();
		new GenImageFromOutputs(imgOutDir + "lena_512x512.output_gauss_fixed", imgOutDir + "regen_gauss_image_fixed").compute();
		new GenDiffBetweenOutputs(imgOutDir + "lena_512x512.output_gauss_float", imgOutDir + "lena_512x512.output_gauss_fixed", imgOutDir + "diff_gauss").compute();

		new GenImageFromOutputs(imgOutDir + "lena_512x512.output_float", imgOutDir + "regen_image_float").compute();
		new GenImageFromOutputs(imgOutDir + "lena_512x512.output_fixed", imgOutDir + "regen_image_fixed").compute();
		new GenDiffBetweenOutputs(imgOutDir + "lena_512x512.output_float", imgOutDir + "lena_512x512.output_fixed", imgOutDir + "diff").compute();
	}

	private void runFlowOnFolder(PrintWriter floatPrinter, PrintWriter fixedPrinter, File sourceFolder, int nbSample)
			throws IOException {
		for(File file : sourceFolder.listFiles()){
			if(file.isFile() && file.getName().endsWith(".c")){
				try{		
					
					new MakeProjectUnit(file, _idfixOutputFolder, _userConfigurationPath, 
							_operator_library, nbSample, floatHeaderFile, fixedHeaderFile, _floatOutputFolder, _fixedOutputFolder).compute();
				} catch (Exception e){
					System.err.println("Error during the conversion of the source file " + file.getName());
					System.err.println("Continue on other source file");
					e.printStackTrace();
				}
			}
		}
	}

	private void extractTypeDef(PrintWriter printer, GecosProject gproject) {
		for(ProcedureSet ps : gproject.listProcedureSets()){
			for(Procedure p : ps.listProcedures()){
				for(ParameterSymbol psymbol : p.listParameters()){
					printer.println("typedef " + ExtendableTypeCGenerator.eInstance.generate(psymbol.getType())
									+ " FIX_" + p.getSymbolName() + "_" +psymbol.getName() +";");
				}
				printer.println(ExtendableCoreCGenerator.eInstance.generate(p.getSymbol())+";");
			}
			printer.println();
		}
	}
	
	private float extractNoiseConstraint(GecosProject gproject){		
		for(ProcedureSet ps : gproject.listProcedureSets()){
			Procedure pmain = IDFixUtils.getMainProcedure(ps);
			if(null == pmain){
				throw new RuntimeException("No main function found. Impossible to extract the noise constraint");
			}
			
			PragmaAnnotation pragma = pmain.getSymbol().getPragma();
			if(pragma != null){
				for(String content : pragma.getContent()){
					Pattern pattern = Pattern.compile("NOISE_CONSTRAINT\\s(-[0-9]+(\\.[0-9]+)?)");
					Matcher matcher = pattern.matcher(content);
					if(matcher.find()){
						return Float.parseFloat(matcher.group(1));
					}
					
				}				
			}
		}
		throw new RuntimeException("Pragma noise constraint not found");
	}
}
