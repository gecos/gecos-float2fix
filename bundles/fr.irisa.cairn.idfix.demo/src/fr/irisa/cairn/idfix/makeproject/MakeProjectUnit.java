package fr.irisa.cairn.idfix.makeproject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.irisa.cairn.gecos.model.c.generator.ExtendableCoreCGenerator;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableTypeCGenerator;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixProject;
import fr.irisa.cairn.idfix.transforms.CreateIDFixProject;
import fr.irisa.cairn.idfix.transforms.Float2FixConversion;
import fr.irisa.cairn.idfix.utils.IDFixUtils;
import gecos.annotations.PragmaAnnotation;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;

public class MakeProjectUnit {
	private File file;
	private File _idfixOutputFolder;
	private String _userConfigurationPath, _operator_library;
	private int nbSample;
	private PrintWriter floatPrinter, fixedPrinter;
	private String _floatOutputFolder, _fixedOutputFolder;
	
	private File mainFile = new File("./src/main/main_imag2d.c");
	
	public MakeProjectUnit(String filePath, String idfixOutputFolderPath, String userConfigurationPath, String operatorLibrary, int sampleNumber,
			String floatPrinterPath, String fixedPrinterPath, String floatOutputFolder, String fixedOutputFolder) throws FileNotFoundException{
		file = new File(filePath);
		_idfixOutputFolder = new File(idfixOutputFolderPath);
		_userConfigurationPath = userConfigurationPath;
		_operator_library = operatorLibrary;
		nbSample = sampleNumber;
		floatPrinter = new PrintWriter(new FileOutputStream(new File(floatPrinterPath), true));
		fixedPrinter = new PrintWriter(new FileOutputStream(new File(fixedPrinterPath), true));
		_floatOutputFolder = floatOutputFolder;
		_fixedOutputFolder = fixedOutputFolder;
	}
	
	public MakeProjectUnit(File filePath, File idfixOutputFolderPath, String userConfigurationPath, String operatorLibrary, int sampleNumber,
			File floatPrinterPath, File fixedPrinterPath, File floatOutputFolder, File fixedOutputFolder) throws FileNotFoundException {
		
		this(filePath.getAbsolutePath(), idfixOutputFolderPath.getAbsolutePath(), userConfigurationPath, operatorLibrary, sampleNumber,
			floatPrinterPath.getAbsolutePath(), fixedPrinterPath.getAbsolutePath(), floatOutputFolder.getAbsolutePath(), fixedOutputFolder.getAbsolutePath());
	}

	public void compute() throws IOException{
		IdfixProject idfixproject = new CreateIDFixProject(file.getAbsolutePath(), _idfixOutputFolder.getAbsolutePath(), _userConfigurationPath).compute();
		float noise_constraint = extractNoiseConstraint(idfixproject.getGecosProject());
		extractTypeDef(floatPrinter, idfixproject.getGecosProject());
		
		new Float2FixConversion(idfixproject, Float.toString(noise_constraint), _operator_library, 4, nbSample).compute();
		
		extractTypeDef(fixedPrinter, idfixproject.getGecosProjectAnnotedAndNotUnrolled());
		
		// Copy float and fixed source file in specific directory which purpose is to create
		// a compilation hierarchy folder
		File fixedFile = new File(idfixproject.getOutputFolderPath(), file.getName());
		if(!fixedFile.isFile()){
			throw new RuntimeException("Fixed source file not found.");
		}
		Files.copy(file.toPath(), new File(_floatOutputFolder, file.getName()).toPath(), StandardCopyOption.REPLACE_EXISTING);
		Files.copy(mainFile.toPath(), new File(_floatOutputFolder, mainFile.getName()).toPath(), StandardCopyOption.REPLACE_EXISTING);
		
		Files.copy(fixedFile.toPath(), new File(_fixedOutputFolder, file.getName()).toPath(), StandardCopyOption.REPLACE_EXISTING);
		Files.copy(mainFile.toPath(), new File(_fixedOutputFolder, mainFile.getName()).toPath(), StandardCopyOption.REPLACE_EXISTING);
		
		floatPrinter.close();
		fixedPrinter.close();
	}
	
	private void extractTypeDef(PrintWriter printer, GecosProject gproject) {
		for(ProcedureSet ps : gproject.listProcedureSets()){
			for(Procedure p : ps.listProcedures()){
				for(ParameterSymbol psymbol : p.listParameters()){
					printer.println("typedef " + ExtendableTypeCGenerator.eInstance.generate(psymbol.getType())
									+ " FIX_" + p.getSymbolName() + "_" +psymbol.getName() +";");
				}
				printer.println(ExtendableCoreCGenerator.eInstance.generate(p.getSymbol())+";");
			}
			printer.println();
		}
	}
	
	private float extractNoiseConstraint(GecosProject gproject){		
		for(ProcedureSet ps : gproject.listProcedureSets()){
			Procedure pmain = IDFixUtils.getMainProcedure(ps);
			if(null == pmain){
				throw new RuntimeException("No main function found. Impossible to extract the noise constraint");
			}
			
			PragmaAnnotation pragma = pmain.getSymbol().getPragma();
			if(pragma != null){
				for(String content : pragma.getContent()){
					Pattern pattern = Pattern.compile("NOISE_CONSTRAINT\\s(-[0-9]+(\\.[0-9]+)?)");
					Matcher matcher = pattern.matcher(content);
					if(matcher.find()){
						return Float.parseFloat(matcher.group(1));
					}
					
				}				
			}
		}
		throw new RuntimeException("Pragma noise constraint not found");
	}
	
}
