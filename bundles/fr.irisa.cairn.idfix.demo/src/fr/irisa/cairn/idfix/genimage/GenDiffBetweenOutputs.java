package fr.irisa.cairn.idfix.genimage;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import javax.imageio.ImageIO;

public class GenDiffBetweenOutputs {

	private String _imagePath1, _imagePath2, _output;
	
	
	public GenDiffBetweenOutputs(String imagePath1, String imagePath2, String output){
		_imagePath1 = imagePath1;
		_imagePath2 = imagePath2;
		_output = output;
	}
	
	
	public void compute(){
		try{
		BufferedReader img1Reader = new BufferedReader(new FileReader(_imagePath1));
		BufferedReader img2Reader = new BufferedReader(new FileReader(_imagePath2));
		
		int height1 = (int) Float.valueOf(img1Reader.readLine()).longValue();
		int width1 = (int) Float.valueOf(img1Reader.readLine()).longValue();

		int height2 = (int) Float.valueOf(img2Reader.readLine()).longValue();
		int width2 = (int) Float.valueOf(img2Reader.readLine()).longValue();
		
		if(height1 != height2)
			throw new RuntimeException("Images have not the same heigth");
		if(width1 != width2)
			throw new RuntimeException("Images have not the same width");
			
		BufferedImage output = new BufferedImage(width1, height1, BufferedImage.TYPE_BYTE_GRAY);
		WritableRaster outputRaster = output.getRaster();
		
		for(int x = 0 ; x < height1 ; x++){
			for(int y = 0; y < width1 ; y++){
				float value1 = Float.valueOf(img1Reader.readLine());
				float value2 = Float.valueOf(img2Reader.readLine());
//				float value1 = (float) 160.454010;
//				float value2 = (float) 161.250000;
				int diffpixel = (int) value1 - (int) value2; 
				
				if(diffpixel != 0)
					outputRaster.setPixel(x, y, new float[] {0});
				else
					outputRaster.setPixel(x, y, new float[] {255});
//				outputRaster.setPixel(x, y, new float[] {diffpixel});
			}
		}
		img1Reader.close();
		img2Reader.close();
		
		File imgFile = new File(_output+".bmp");
		ImageIO.write(output, "bmp", imgFile);
		} catch (Exception e){
			throw new RuntimeException(e);
		}		
	}
	
}
