package fr.irisa.cairn.idfix.genimage;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import javax.imageio.ImageIO;

public class GenImageFromOutputs {
	
	private String _2doutput_file_path, _image_output_path;
	
	public GenImageFromOutputs(String output_path, String output_file_path){
		_2doutput_file_path = output_path;
		_image_output_path = output_file_path;
	}
	
	
	public void compute() {
		BufferedReader outFile;
		try {
			outFile = new BufferedReader(new FileReader(_2doutput_file_path));
		
			int height = (int) Float.valueOf(outFile.readLine()).longValue();//XXX
			int width = (int) Float.valueOf(outFile.readLine()).longValue();
			
			BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
			WritableRaster img_data = img.getRaster();
			
			for(int x=0; x<height; x++) {
				for(int y=0; y<width; y++) {
//					img.setRGB(x, y, Integer.valueOf(outFile.readLine()));
					Integer pixel = (int) Float.valueOf(outFile.readLine()).longValue();
					img_data.setPixel(x, y, new int[] {pixel});
				}
			}
			outFile.close();
			
			File imgFile = new File(_image_output_path+".bmp");
			ImageIO.write(img, "bmp", imgFile);
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
