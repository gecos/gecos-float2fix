package fr.irisa.cairn.idfix.genimage;

import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.imageio.ImageIO;

public class GenInputFromImages {
	
	private String _img_dir;
	private String _output_dir;
	static private final String OUTPUT_EXTENSION = ".input";
		
	public GenInputFromImages(String image_dir, String output_dir){
		_img_dir = image_dir;
		_output_dir = output_dir.endsWith("/")?output_dir:output_dir+'/';
	}
	
	
	public void compute() {
		
		File imgDir = new File(_img_dir);
		if(!imgDir.exists() || !imgDir.isDirectory()) {
			System.err.println("invalide image directory");
			return;
		}
		
		File outputDir = new File(_output_dir);
		if(!outputDir.isDirectory())
			if(!outputDir.mkdirs())
				throw new RuntimeException("Impossible to create output directory");
				
		
		for(File imgFile : imgDir.listFiles()) {
			String imgName = imgFile.getName();
			
			try {
				BufferedImage img = ImageIO.read(imgFile);
				if(img.getType() != BufferedImage.TYPE_BYTE_GRAY) {
					System.err.println("Image is not byte grayscale! " + imgName);
					continue;
				}
				Raster data = img.getData();				
				PrintWriter output = new PrintWriter(_output_dir+imgName.substring(0,imgName.lastIndexOf("."))+OUTPUT_EXTENSION);
				output.println(data.getHeight());
				output.println(data.getWidth());
				for(int x=0; x<data.getHeight(); x++) {
					for(int y=0; y<data.getWidth(); y++) {
						int[] pixel= new int[1];
						data.getPixel(x, y, pixel);
						output.println(pixel[0]);
					}
				}
				output.close();
			} catch (IOException e) {
				System.err.println("Failed for: " + imgName);
			}
		}
	}
}
