

void traceAffectationValue(int affect, double value){
	jclass cls = globalEnv->GetObjectClass(globalJobj);
	jmethodID mid = globalEnv->GetMethodID(cls, "traceAffectationValue", "(ID)V");
	globalEnv->CallVoidMethod(globalJobj, mid, affect, value);
}
