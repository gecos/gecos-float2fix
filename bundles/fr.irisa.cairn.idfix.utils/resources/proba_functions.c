// === Bibliothèques non standards ===
#include "probabilities.h"

// === definition de fontion ===


void init_sequence(block ** pt_block_parcours_1, block ** pt_block_parcours_2, int *id){
	// Noeud source de la structure optimisée pour la construction (ordonnée) et pour l'extraction des probabillités des traces globales (utile pour le débogage).
	(*pt_block_parcours_1) = malloc(sizeof(block));    
	(*pt_block_parcours_1)->id = *id;
	(*id)++;
	(*pt_block_parcours_1)->valeur = -1;	// Valeur par défaut
	(*pt_block_parcours_1)->nbTimes = 0;  // Valeur par défaut
	(*pt_block_parcours_1)->suiv = NULL;
	(*pt_block_parcours_1)->downLevel = NULL;

	// Noeud source de la liste de parcours secondaire pour optimiser l'extraction des traces relatives (utilisé par Acc-Eval pour le calcul des K, L et M)
	(*pt_block_parcours_2) = malloc(sizeof(block));    
	(*pt_block_parcours_2)->id = *id;
	(*id)++;
	(*pt_block_parcours_2)->valeur = -1;	// Valeur par défaut
	(*pt_block_parcours_2)->nbTimes = 0;	// Valeur par défaut
	(*pt_block_parcours_2)->suiv = NULL;
	(*pt_block_parcours_2)->downLevel = NULL;
}

void add_new_sequence(block * block_parcours_1, block * block_parcours_2, int * id, int nbTimes, int nbElement, ...){

	int i, valeur;
	va_list vl;
	va_start(vl, nbElement);

	// Création des blocs
	// Structure optimisée pour la construction (ordonnée) et optimiser pour extraire les traces globals et relatives.
	// Elle posède deux entrées pour le parcours
	
	//printf("---------------------- add_new_sequence -------------------- \n"); // xxx: delete this
	for (i = 0; i < nbElement - 1; i++){
		valeur = va_arg(vl, int);
		block_parcours_1 = add_element(block_parcours_1, block_parcours_2, id, valeur, 0, false); // le 0 correspond à nbTimes = 0  mais il ne sert a rien car on a false
	}
	valeur = va_arg(vl, int);
	add_element(block_parcours_1, block_parcours_2, id, valeur, nbTimes, true); // le nbTimes est écrit dans le noeud normalement feuille (il peut y avoir des cas particulier)
}


block * add_element (block * block_parcours_1, block * block_parcours_2, int *id, int valeur, int nbTimes, bool flag_end_sequence){
	block *sTemp = NULL;
	block *sTempDownLevel = NULL;

	

	if(block_parcours_1->downLevel == NULL){ // Création du premier d'un étage niveau
		//printf("parcour_1: cas 1 \"Création du premier d'un étage niveau\":  valeur: %d\n", valeur);
		// --- Liste Vide ---
		// --- Parcours 1 ---
		sTemp = malloc(sizeof(block));
		block_parcours_1->downLevel = sTemp;
		sTemp->id = *id;
		(*id)++;
		sTemp->valeur = valeur;
		if(flag_end_sequence == true){
			sTemp->nbTimes = nbTimes; // Si c'est un noeud feuille
		}else{
			sTemp->nbTimes = 0;
		}
		sTemp->suiv = NULL;
		sTemp->downLevel = NULL;

		// --- Parcours 2 ---
		add_element_block_parcours_2(sTemp, block_parcours_2, id, sTemp->valeur);
		return sTemp; // retourne l'élément nouvellement créé
	}else{
		
		// --- Recherche dans la liste ---
		block *s = block_parcours_1->downLevel;
		block *sPred = block_parcours_1;

		// Segmentation fault
		while (s->suiv != NULL && s->valeur < valeur){
			sPred = s;
			s = s->suiv;
			//printf(" z \n");
		}

		// Plusieur cas possible
		assert(s != NULL);
		if(s->valeur == valeur){ 				// L'élément existe déjà
			//printf("parcour_1: cas 2 \" L'élément existe déjà\":  valeur: %d\n", valeur);
			// --- Parcours 1 ---
			if(flag_end_sequence == true){
				s->nbTimes += nbTimes; 			// Si c'est une noeud feuille MAJ
			}
			// --- Parcours 2 ---
			// NOP
			return s;
		}else if(s->valeur > valeur){ 					// L'élément n'existe pas, ajout avant du courant
			//printf("parcour_1: cas 3 \" L'élément n'existe pas, ajout avant du courant\":  valeur: %d\n", valeur);
			sTemp = malloc(sizeof(block));
			sTemp->id = *id;
			(*id)++;
			if(sPred == block_parcours_1){
				block_parcours_1->downLevel = sTemp;	// Première position
				//printf("premier\n");
			}else{
				sPred->suiv = sTemp;
				//printf("deuxieme ou plus\n");
			}

			sTemp->valeur = valeur;
			if(flag_end_sequence == true){
				sTemp->nbTimes = nbTimes; 			// Si c'est une noeud feuille MAJ
			}else{
				sTemp->nbTimes = 0;
			}

			sTemp->suiv = s;
			sTemp->downLevel = NULL;

			// --- Parcours 2 ---
			add_element_block_parcours_2(sTemp,block_parcours_2, id, sTemp->valeur);
			return sTemp;
		}else{ // if(s->valeur < valeur)
			//printf("parcour_1: cas 4 \" L'élément n'existe pas\":  valeur: %d\n", valeur);

			sTemp = malloc(sizeof(block));

			sTemp->id = *id;
			(*id)++;
			sTemp->valeur = valeur;
			if(flag_end_sequence == true){
				sTemp->nbTimes = nbTimes; 			// Si c'est une noeud feuille MAJ
			}else{
				sTemp->nbTimes = 0;
			}

			if(s->suiv != NULL){				// dernier de la liste non vide
				sTemp->suiv = s->suiv;
			}else{
				sTemp->suiv = NULL;
			}
			sTemp->downLevel = NULL;

			s->suiv = sTemp;

			// --- Parcours 2 ---
			add_element_block_parcours_2(sTemp,block_parcours_2, id, sTemp->valeur);
			return sTemp;
		}
	}
}

void add_element_block_parcours_2 (block * block_parcours_1, block * block_parcours_2, int *id, int valeur){
	block *s;
	block *sPred;
	block *sTemp = NULL;
	block *sTempDownLevel = NULL;

	assert(block_parcours_1 != NULL);
	assert(block_parcours_2 != NULL);


	s = block_parcours_2->downLevel;
	sPred = block_parcours_2;

	if(s == NULL){		// Liste vide
		//printf("parcour_2: cas 1 \"Liste vide\":  valeur: %d\n", valeur);

		block_parcours_2->downLevel = malloc(sizeof(block));
		sTemp = block_parcours_2->downLevel;
		sTemp->id = *id;
		(*id)++;
		sTemp->valeur = block_parcours_1->valeur;
		sTemp->nbTimes = 0;
		sTemp->suiv = NULL;
		sTemp->downLevel = malloc(sizeof(block));

		sTempDownLevel = sTemp->downLevel;
		sTempDownLevel->id = *id;
		(*id)++;
		sTempDownLevel->valeur = -1;
		sTempDownLevel->nbTimes = 0;
		sTempDownLevel->suiv = NULL;
		sTempDownLevel->downLevel = block_parcours_1;

	}else{
		// --- recherche dans la liste ---

		//printf("Debut while\n");


		while (s->suiv != NULL && s->valeur < valeur){
			sPred = s;
			s = s->suiv;
		}
		//printf("Fin while\n");
		assert(s != NULL);

		if(s->valeur == valeur){ 				// L'élément existe déjà
			//printf("parcour_2: cas 2 \"L'élément existe déjà\":  valeur: %d\n", valeur);
			assert(s->downLevel != NULL);
			sTempDownLevel = s->downLevel;

			while ((sTempDownLevel->suiv != NULL) && (sTempDownLevel->downLevel != block_parcours_1)){
				sTempDownLevel = sTempDownLevel->suiv;
			}
			if(sTempDownLevel != block_parcours_1){ // ajout d'un élément en fin de chaine
				sTempDownLevel->suiv = malloc(sizeof(block));
				sTempDownLevel = sTempDownLevel->suiv;

				sTempDownLevel->id = *id;
				(*id)++;
				sTempDownLevel->valeur = -1;
				sTempDownLevel->nbTimes = 0;
				sTempDownLevel->suiv = NULL;
				sTempDownLevel->downLevel = block_parcours_1;

			} // else NOP, l'élément existe déjà

		}else if(s->valeur > valeur){ 				// L'élément n'existe pas, ajout avant du courant
			//printf("parcour_2: cas 3 \"L'élément n'existe pas, ajout avant du courant\":  valeur: %d\n", valeur);
			sTemp = malloc(sizeof(block));
			if(sPred == block_parcours_2){
				block_parcours_2->downLevel = sTemp;	// Première position
			}else{
				sPred->suiv = sTemp;
			}

			sTemp->id = *id;
			(*id)++;
			sTemp->valeur = valeur;
			sTemp->nbTimes = 0;
			sTemp->suiv = s;
			sTemp->downLevel = malloc(sizeof(block));;

			sTempDownLevel = sTemp->downLevel;
			sTempDownLevel->id = *id;
			(*id)++;
			sTempDownLevel->valeur = -1;
			sTempDownLevel->nbTimes = 0;
			sTempDownLevel->suiv = NULL;
			sTempDownLevel->downLevel = block_parcours_1;

		}else{ // if(s->valeur < valeur)			// L'élément n'existe pas, ajout en dernier
			//printf("parcour_2: cas 4 \"L'élément n'existe pas, ajout en dernier\":  valeur: %d\n", valeur);

			s->suiv = malloc(sizeof(block));
			sTemp = s->suiv;

			sTemp->id = *id;
			(*id)++;
			sTemp->valeur = valeur;
			sTemp->nbTimes = 0;
			sTemp->suiv = NULL;
			sTemp->downLevel = malloc(sizeof(block));

			sTempDownLevel = sTemp->downLevel;
			sTempDownLevel->id = *id;
			(*id)++;
			sTempDownLevel->valeur = -1;
			sTempDownLevel->nbTimes = 0;
			sTempDownLevel->suiv = NULL;
			sTempDownLevel->downLevel = block_parcours_1;
		}
	}
}


int probabilite_sequence(block * block_parcours_2, int nbElement, int  * tabElement){
	int p =0 ,i=0;

	block *s, *s2;

	// === recherche des points d'entrés (première élément) ===
	assert(nbElement > 0);
	assert(block_parcours_2 != NULL);

	s = block_parcours_2->downLevel;
	//printf("test s->valeur = %d\n", s->valeur);

	//printf("point d'entre tabElement[0] = %d\n", tabElement[0]);
	while( (s!= NULL) && (s->valeur < tabElement[0] )){
		s = s->suiv;
	}
	if((s!= NULL) && (tabElement[0] == s->valeur)){
		//printf("tabElement[0] == s->valeur === %d \n",s->valeur);
		s2 = s->downLevel;
		//printf("test valeur: %d\n", s2->valeur);
		assert(s2 != NULL);
		while(s2 != NULL){
			//printf("Appel de probabilite_sequence_routine (point d'entré)\n");
			p += probabilite_sequence_routine(s2->downLevel, nbElement, tabElement);
			s2 = s2->suiv;
		}

	}// else: p =0
	
	return p;
}

int probabilite_sequence_routine(block * s, int nbElement, int  * tabElement){
	int p = 0;
	block * s2;
	assert(s != NULL);

	//printf("tabElement[0] = %d\n", tabElement[0]);
	if(s->valeur == tabElement[0]){
		//printf("etape 1 :s->valeur  = %d\n", s->valeur );
		if(nbElement == 1){					// Cas d'arret 
		//printf("etape 2 : nbElement == 1 Cas d'arret \n");
			assert(s->nbTimes != 0); // puisqu'on est dans un noeud feuille
			p = s->nbTimes;		
		}else{
		//printf("etape 3.1 : nbElement != 1\n");
			
			s2 = s->downLevel;
			while((s2!= NULL) && (s2->valeur < tabElement[1])){

				
				s2 = s2->suiv;
			}
			//printf("tabElement[i]: %d, %d, %d, %d\n", tabElement[0],tabElement[1],tabElement[2],tabElement[3]);
			//printf("etape 3.2 : tabElement[1] == s2->valeur:  %d == %d\n", tabElement[1], s2->valeur);
			if((s2!= NULL) && (tabElement[1] == s2->valeur)){
				p = probabilite_sequence_routine(s2, nbElement - 1,&tabElement[1]);
			}// else nop Cas d'arret
			
		}
	}// else nop Cas d'arret
	return p;
}


// probabilité croisée de deux sous traces d'exécution
int probabilite_croise_sequence(block * block_parcours_2, int nbElement, int  * tabElement, int nbElement2, int  * tabElement2){
	int * tp_temp;
	int i, temp, t;
	int flag;
	int p =0;
	//printf(" === probabilite_croise_sequence ===\n");

	//printf("nbElement = %d; nbElement2 = %d\n", nbElement, nbElement2);

	if(nbElement < nbElement2){
		//printf("	permutation\n");
		// --- Permutation ---
		temp = nbElement;
		nbElement = nbElement2;
		nbElement2 = temp;

		tp_temp = tabElement;
		tabElement = tabElement2;
		tabElement2 = tp_temp;
	}//else NOP

	// --- Recherche du premier numéro de block commun ---
	for( i=0; i<nbElement; i++){
		//printf("	for: i = %d: tabElement[i] = %d\n", i, tabElement[i]);
		if(tabElement[i] == tabElement2[0]){
			//printf("	flag = 1\n");
			flag = 1;
			break;
		}
	}

	if(flag == 1){
		t = 0;
		while((nbElement > (i+t))  && (nbElement2 > t) && (tabElement[i+t] == tabElement2[t])){
			//printf("[%d, %d]  |  [%d, %d] sur [%d, %d]\n",tabElement[i+t] ,tabElement2[t], (i+t), t, nbElement, nbElement2);
			if(((nbElement-1) == (i+t)) && ((nbElement2-1) == t)){
				//printf("probabilite_sequence nbElement = %d\n", nbElement);
				p = probabilite_sequence(block_parcours_2, nbElement,tabElement); // 4(NbElement) , 0(num block 1), 2(num block 2), ....);
			}
			t++;
		}
	}// else{ NOP p =0
	return p;
}



// Destruction du graphe proprement
void free_all_sequence(block ** pt_block_parcours_1, block ** pt_block_parcours_2){
	block * block_parcours_1 = *pt_block_parcours_1;
	block * block_parcours_2 = *pt_block_parcours_2;
	block *s, *s2, *stemp, *stemp2;

	s = block_parcours_2->downLevel;
	while(s != NULL){
		assert(s->downLevel != NULL);

		s2 = s->downLevel;
		while(s2 != NULL){
			assert(s2->downLevel != NULL);
			free(s2->downLevel);
			stemp2 = s2;
			s2 = s2->suiv;
			free(stemp2);
		}
		stemp = s;
		s = s->suiv;
		free(stemp);
	}

	free(block_parcours_1);
	free(block_parcours_2);
}

void displayDot( block * block_parcours_1, block * block_parcours_2, const char * nom){

	char str[10];
	FILE *file;
	block *s;
	block *sPred;
	block *sTemp = NULL;
	block *sTempDownLevel = NULL;

	char * mon_fichier = "./outProba.dot";
	
	printf(" === Display Dot File: probabilites.c === \n");	

	file = fopen(mon_fichier, "w");
	assert(file != NULL);


	fprintf(file, "digraph G {" );
	//if(block_parcours_1->downLevel != NULL){ // Création du premier d'un étage niveau

		// --- Parcours de la liste ---
		fprintf(file, "E%d [label=\"%d\", shape=Msquare];\n", block_parcours_1->id, block_parcours_1->valeur);
		displayDotRoutineParcours1(file, block_parcours_1);

		fprintf(file, "E%d [label=\"%d\", shape=Msquare, color=red];\n", block_parcours_2->id, block_parcours_2->valeur);
		displayDotRoutineParcours2(file, block_parcours_2);

	//}
	fprintf(file, "}" );
	fclose(file);  // on ferme le fichier
}

void displayDotRoutineParcours1(FILE * file, block *  block_parcours_1){

	block *s = block_parcours_1->downLevel;
	block *sPred = NULL;

	while (s != NULL){
		// Création du noeud
		if(s->nbTimes == 0){	// Noeud non feuille
			fprintf(file, "E%d [label=\"%d\"];\n", s->id, s->valeur);
		}else{				// Noeud feuille
			fprintf(file, "E%d [label=\"%d \\n [%d]\", color=green];\n", s->id, s->valeur, s->nbTimes);
		}

		// Création de l'arc
		if(s == block_parcours_1->downLevel){	// Premier d'un étage
			fprintf(file, "E%d->E%d [style=dotted];\n",block_parcours_1->id, s->id);
		}else{											// Deuxième ou plus d'un étage
			fprintf(file, "E%d->E%d;\n",sPred->id, s->id);

		}

		// Appelle récuirsif
		displayDotRoutineParcours1(file, s);


		sPred = s;
		s = s->suiv;
	}
}

void displayDotRoutineParcours2(FILE * file, block *  block_parcours_2){
	block *s = block_parcours_2->downLevel;
	block *sPred;
	block *s2 = NULL;
	block *sPred2 = NULL;

	while (s != NULL){
		assert( s->downLevel!= NULL);
		s2 = s->downLevel;

		// Création du noeud

			fprintf(file, "E%d [label=\"%d\",shape=octagon, color=red];\n", s->id, s->valeur);

			if(s == block_parcours_2->downLevel){
				fprintf(file, "E%d->E%d [style=dotted, color=red];\n",block_parcours_2->id, s->id);
			}else{
				fprintf(file, "E%d->E%d [color=red];\n",sPred->id, s->id);
			}

		while (s2 != NULL){
			// Création du noeud
			fprintf(file, "E%d [label=\"%d\",shape=octagon, color=red];\n", s2->id, s2->valeur);

			if(s2 == s->downLevel){
				fprintf(file, "E%d->E%d [style=dotted, color=red];\n",s->id, s2->id);
			}else{
				fprintf(file, "E%d->E%d [color=red];\n",sPred2->id, s2->id);
			}

			fprintf(file, "E%d->E%d [style=dotted, color=red];\n",s2->id, s2->downLevel->id);

			sPred2 = s2;
			s2 = s2->suiv;
		}

		/*if(s != block_parcours_2){
			fprintf(file, "E%d->E%d;\n",sPred->id, s->id);
		}*/
		sPred = s;
		s = s->suiv;
	}
}



/* 
// Partie permetant de tester cette librairie
// init_automated_generate_sequence est normalement: généré par Gecos dynamiquement (pour la compilation en librairie dynamique)
// main: représente les instruction appellé normelement par le ComputePb.c (pour la compilation en librairie dynamique oune exécutable de test)

// === Compilation de la librairie === (necessite init_automated_generate_sequence)
// gcc -o probabilities.o -c probabilities.c
// gcc -o libprobabilities.so -shared probabilities.o

// === Compilation en exécutable pour tester === (necessite init_automated_generate_sequence et main)
// gcc -g -o exe probabilities.c


int init_automated_generate_sequence(block * block_parcours_1, block * block_parcours_2, int * id){
	// --- Debut: Code Généré dynamiquement par Gecos ---
	int nb_total_times = 10;
	add_new_sequence(block_parcours_1, block_parcours_2, id, 5, 2, 4, 9); // 5(NbTimes), 2(NbElement) , 0(num block1), 2(num block 2)
	add_new_sequence(block_parcours_1, block_parcours_2, id, 5, 2, 4, 7);
	add_new_sequence(block_parcours_1, block_parcours_2, id, 5, 2, 4, 8);
	add_new_sequence(block_parcours_1, block_parcours_2, id, 5, 2, 4, 12);
	add_new_sequence(block_parcours_1, block_parcours_2, id, 5, 2, 4, 5);

	add_new_sequence(block_parcours_1, block_parcours_2, id, 9, 1,1);
	add_new_sequence(block_parcours_1, block_parcours_2, id, 9, 4,1, 2,4,5);
	add_new_sequence(block_parcours_1, block_parcours_2, id, 4, 4,1, 2,4,6);
	add_new_sequence(block_parcours_1, block_parcours_2, id, 6, 5,1, 2,4,5,5);
	add_new_sequence(block_parcours_1, block_parcours_2, id, 1, 4,1, 2,4,5);
	add_new_sequence(block_parcours_1, block_parcours_2, id, 10,4,10,3,4,5);
	add_new_sequence(block_parcours_1, block_parcours_2, id, 70,4,2, 2,4,5);

	return nb_total_times;
	// --- Fin: Code Généré dynamiquement par Gecos ---
}

// --- Méthode de test manuel...
void main(){
	// --- Initialisation ---
	int id =0;
	int nb_total_times;

	block * block_parcours_1;    
	block * block_parcours_2;  
  
	// Pour les test
	int p;
	int tabSeq[2] = {4,6};
	int tabSeq2[4] = {1,2,4,6};
	

	// --- initialisation ---
	init_sequence(&block_parcours_1, &block_parcours_2, &id);
	nb_total_times = init_automated_generate_sequence(block_parcours_1, block_parcours_2, &id);



	// --- Affichage ---
	displayDot(block_parcours_1, block_parcours_2,"toto");


	// --- Extracteur de probabilité de la séquence ---
	p = probabilite_sequence(block_parcours_2, 2,tabSeq ); // 4(NbElement) , 0(num block 1), 2(num block 2), ....)

	printf("La probabilité de la séquence est de %d sur %d\n", p, nb_total_times);


	p = probabilite_croise_sequence(block_parcours_2, 2,tabSeq, 4, tabSeq2); // 4(NbElement) , 0(num block 1), 2(num block 2), ....)

	printf("La probabilité croisé des deux sequences est de %d sur %d\n", p, nb_total_times);

	// --- Destructeur ---
	printf(" === Destructeur === \n");
	free_all_sequence(&block_parcours_1, &block_parcours_2);


	printf(" === Fin === \n");

}

*/




