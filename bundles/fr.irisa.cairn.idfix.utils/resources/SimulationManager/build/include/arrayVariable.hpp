/**
 * @file arrayVariable.hpp
 * @brief represent array variable use to save data during simulation
 *
 * @version 0.1
 * @date 19/11/2013
 * @author nicolas simon (nicolas.simon(at)irisa.fr)
 */

#ifndef __ARRAY_VARIABLE_HPP__
#define __ARRAY_VARIABLE_HPP__

#include "variable.hpp"
#include <list>
#include <map>

namespace simumanager {
    /**
     * @class ArrayVariable
     * @brief Implements array variable structure use to save array data during simulation
     */
    template <class T> class ArrayVariable : public Variable {
        private:
            std::map<unsigned int, std::list<T> > m_values;
            
        public:
            ArrayVariable(std::string name):Variable(name){};
            ~ArrayVariable(){}
            void addValue(unsigned int index, T value);
            void writeHuman(std::fstream &fs);
            void writeBinary(std::fstream &fs);
    };
}

#endif
