/**
 * @file variable.hpp
 * @brief base class representing variable use to save data during simulation
 *
 * @version 0.1
 * @date 19/11/2013
 * @author nicolas simon (nicolas.simon(at)irisa.fr)
 */


#ifndef __VARIABLE_HPP__
#define __VARIABLE_HPP__

#include <string>
#include <fstream>

namespace simumanager {
    class Variable {
        protected:
            std::string m_name;
            Variable(std::string name):m_name(name){};
        public:
            virtual ~Variable(){};
            std::string getName(){return m_name;}
            virtual void writeHuman(std::fstream &fs) = 0;
            virtual void writeBinary(std::fstream &fs) = 0;
    };
}

#endif
