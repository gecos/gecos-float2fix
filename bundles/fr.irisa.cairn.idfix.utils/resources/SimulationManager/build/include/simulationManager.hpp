/**
 * @file simulationManager.hpp
 * @brief use to save all variable for each iteration of the simulation
 *
 * @version 0.1
 * @date 19/11/2013
 * @author nicolas simon (nicolas.simon(at)irisa.fr)
 */

#ifndef __SIMULATION_MANAGER_HPP__
#define __SIMULATION_MANAGER_HPP__

#include <list>
#include <map>

#include "variable.hpp"

namespace simumanager {
    class SimulationManager {
        private:
            std::map<unsigned int, Variable*> m_values;
    
        public:
            ~SimulationManager();

            Variable* getVariable(unsigned int profiling_number){return m_values[profiling_number];}
            void saveVariable(unsigned int profiling_number, Variable *variable);
            void writeHuman(std::string outputFolderPath);
            void writeBinary(std::string outputFolderPath);
    };
}

#endif
