/**
 * @file scalarVariable.hpp
 * @brief represent scalar variable use to save data during simulation
 *
 * @version 0.1
 * @date 19/11/2013
 * @author nicolas simon (nicolas.simon(at)irisa.fr)
 */

#ifndef __SCALAR_VARIABLE_HPP__
#define __SCALAR_VARIABLE_HPP__

#include "variable.hpp"

#include <iostream>
#include <list>

namespace simumanager {
    template<class T> class ScalarVariable : public Variable {
        private:
            unsigned int m_number_adding;
            std::list<T> m_values;

        public:
            ScalarVariable(std::string name):Variable(name), m_number_adding(0){};
            ~ScalarVariable(){};
            void addValue(T value);
            unsigned int getNumberAdding(){return m_number_adding;}
            void writeHuman(std::fstream &fs);
            void writeBinary(std::fstream &fs);
    };
}


#endif
