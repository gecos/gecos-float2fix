/**
 * @file csimulationManager.h
 * @brief C wrapper of simulation manager library
 *
 * @version 0.1
 * @date 19/11/2013
 * @author nicolas simon (nicolas.simon(at)irisa.fr)
 */

#ifndef __C_SIMULATION_MANAGER_H__
#define __C_SIMULATION_MANAGER_H__

#ifdef __cplusplus
extern "C" {
#endif

    typedef void CSimulationManager;
    
    CSimulationManager* newSimulationManager();
    void deleteSimulationManager(CSimulationManager* manager);
    void SimulationManager_writeHuman(CSimulationManager* manager, const char* outputFolderPath);
    void SimulationManager_writeBinary(CSimulationManager* manager, const char* outputFolderPath);

    void SimulationManager_saveDoubleVariable(CSimulationManager* manager, unsigned int profiling_number, const char* name, double value);
    void SimulationManager_saveFloatVariable(CSimulationManager* manager, unsigned int profiling_number, const char* name, float value);
    void SimulationManager_saveLongVariable(CSimulationManager* manager, unsigned int profiling_number, const char* name, long value);
    void SimulationManager_saveIntVariable(CSimulationManager* manager, unsigned int profiling_number, const char* name, int value);
    void SimulationManager_saveUIntVariable(CSimulationManager* manager, unsigned int profiling_number, const char* name, unsigned int value);
    
    void SimulationManager_saveDoubleArrayVariable(CSimulationManager* manager, unsigned int profiling_number, unsigned int index, const char* name, double value);
    void SimulationManager_saveFloatArrayVariable(CSimulationManager* manager, unsigned int profiling_number, unsigned int index, const char* name, float value);
    void SimulationManager_saveLongArrayVariable(CSimulationManager* manager, unsigned int profiling_number, unsigned int index, const char* name, long value);
    void SimulationManager_saveIntArrayVariable(CSimulationManager* manager, unsigned int profiling_number, unsigned int index, const char* name, int value);
    void SimulationManager_saveUIntArrayVariable(CSimulationManager* manager, unsigned int profiling_number, unsigned int index, const char* name, unsigned int value);

#ifdef __cplusplus
}
#endif

#endif // __C_SIMULATION_MANAGER_H__
