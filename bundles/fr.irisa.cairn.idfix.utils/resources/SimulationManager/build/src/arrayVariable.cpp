/**
 * @file arrayVariable.cpp
 *
 * @version 0.1
 * @date 22/11/2013
 * @author nicolas simon (nicolas.simon(at)irisa.fr)
 */

#include "arrayVariable.hpp"

#include <iostream>
#include <cassert>

namespace simumanager {
    template <class T> void ArrayVariable<T>::addValue(unsigned int index, T value){
        m_values[index].push_back(value); 
    }

    template <class T> void ArrayVariable<T>::writeBinary(std::fstream &fs){
        typename std::map<unsigned int, std::list<T> >::iterator itMap;
        typename std::list<T>::iterator itList;
        unsigned int size;
        short varKind = 1;
        fs.write((char*)&varKind, sizeof(varKind));
        size = m_values.size();
        fs.write((char*)&size, sizeof(size));
        for(itMap = m_values.begin() ; itMap != m_values.end() ; itMap++){
            fs.write((char*)&itMap->first, sizeof(itMap->first));
            size = itMap->second.size();
            fs.write((char*)&size, sizeof(size));
            for(itList = itMap->second.begin() ; itList != itMap->second.end() ; itList++){
                fs.write((char*)&(*itList), sizeof(*itList));
            }
        }
    }
    
    template <class T> void ArrayVariable<T>::writeHuman(std::fstream &fs){
        typename std::map<unsigned int, std::list<T> >::iterator itMap;
        typename std::list<T>::iterator itList;
        fs << "VariableKind: " << 1 << std::endl;
        fs << "nbElemsSave: " << m_values.size() << std::endl;
        for(itMap = m_values.begin() ; itMap != m_values.end() ; itMap++){
            fs << "index: " << itMap->first << std::endl;
            fs << "size: " << itMap->second.size() << std::endl;
            for(itList = itMap->second.begin() ; itList != itMap->second.end() ; itList++){
                fs << *itList << std::endl;
            }
            fs << std::endl;
        }
    }
    template class ArrayVariable<double>;
    template class ArrayVariable<float>;
    template class ArrayVariable<int>;
    template class ArrayVariable<unsigned int>;
    template class ArrayVariable<long>;
}
