/**
 * @file simulationManager.cpp
 * @brief use to save all variable for each iteration of the simulation
 *
 * @version 0.1
 * @date 19/11/2013
 * @author nicolas simon (nicolas.simon(at)irisa.fr)
 */

#include "simulationManager.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
namespace simumanager {

	SimulationManager::~SimulationManager(){
		for(std::map<unsigned int, Variable*>::iterator it = m_values.begin() ; it != m_values.end() ; it++){
			delete it->second;
		}
	}

    void SimulationManager::saveVariable(unsigned int profiling_number, Variable *variable){
        if(m_values[profiling_number] != NULL){
            std::cerr << "Variable already created. Problem during the construction of the manager variable mapping" << std::endl;
            exit(1);
        }

        m_values[profiling_number] = variable;
    }

    void SimulationManager::writeHuman(std::string outputFolderPath){
        std::fstream fs;
        std::string pathOutput;

        for(std::map<unsigned int, Variable*>::iterator it = m_values.begin() ; it != m_values.end() ; it++){
            std::ostringstream oss;
            oss << (it->second)->getName() << "_" << it->first << ".human";
            pathOutput = outputFolderPath;
            pathOutput += "/";
            pathOutput += oss.str();
            fs.open(pathOutput.c_str(), std::fstream::out);
            if(!fs.is_open()){
                std::cerr << "File to write simulation result can not be open (human version)!" << std::endl;
                exit(1);
            }
            fs << it->first << std::endl;
            (it->second)->writeHuman(fs);
            fs.close();
        }
    }

    void SimulationManager::writeBinary(std::string outputFolderPath){
        std::fstream fs;
        std::string pathOutput;

        for(std::map<unsigned int, Variable*>::iterator it = m_values.begin() ; it != m_values.end() ; it++){
            std::ostringstream oss;
            oss << (it->second)->getName() << "_" << it->first << ".bin";
            pathOutput = outputFolderPath;
            pathOutput += "/";
            pathOutput += oss.str();
            fs.open(pathOutput.c_str(), std::fstream::out | std::fstream::binary);
            if(!fs.is_open()){
                std::cerr << "File to write simulation result can not be open (binary version)!" << std::endl;
                exit(1);
            }
            fs.write((char*)&it->first, sizeof(it->first)); 
            (it->second)->writeBinary(fs);
            fs.close();
        }
    }
}
