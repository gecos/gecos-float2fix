/**
 * @file csimulationManager.cpp
 * @brief C wrapper of simulation manager library
 *
 * @version 0.1
 * @date 19/11/2013
 * @author nicolas simon (nicolas.simon(at)irisa.fr)
 */

#include "csimulationManager.h"
#include <iostream>
#include <assert.h>
#include <stdlib.h>

#include "simulationManager.hpp"
#include "scalarVariable.hpp"
#include "arrayVariable.hpp"

/******************************************/
/*         SimulationManager              */
/******************************************/
CSimulationManager* newSimulationManager(){
    return new simumanager::SimulationManager();
}

void deleteSimulationManager(CSimulationManager* manager){
	delete (simumanager::SimulationManager*) manager;
}

/******************************************/
/*           Scalar Variable              */
/******************************************/

void SimulationManager_saveDoubleVariable(CSimulationManager* manager, unsigned int profiling_number, const char* name, double value){
    simumanager::SimulationManager* cppmanager = (simumanager::SimulationManager*) manager;
    simumanager::Variable* var = cppmanager->getVariable(profiling_number);
    simumanager::ScalarVariable<double> *scvar;
    if(var == NULL){
        scvar = new simumanager::ScalarVariable<double>(std::string(name));
        scvar->addValue(value);
        cppmanager->saveVariable(profiling_number, scvar);
    }
    else{
        scvar = dynamic_cast< simumanager::ScalarVariable<double>* >(var);
        assert(scvar != NULL && "A scalar variable have the same profiling number as an array variable");
        if(scvar->getName() != name){
            std::cerr << "A scalar variable (" << name << ") have the same name as an other (" << scvar->getName() << ")";
            exit(1);
        }
        scvar->addValue(value);
    }
}
void SimulationManager_saveFloatVariable(CSimulationManager* manager, unsigned int profiling_number, const char* name, float value){
    simumanager::SimulationManager* cppmanager = (simumanager::SimulationManager*) manager;
    simumanager::Variable* var = cppmanager->getVariable(profiling_number);
    simumanager::ScalarVariable<float> *scvar;
    if(var == NULL){
        scvar = new simumanager::ScalarVariable<float>(std::string(name));
        scvar->addValue(value);
        cppmanager->saveVariable(profiling_number, scvar);
    }
    else{
        scvar = dynamic_cast< simumanager::ScalarVariable<float>* >(var);
        assert(scvar != NULL && "A scalar variable have the same profiling number as an array variable");
        if(scvar->getName() != name){
            std::cerr << "A scalar variable (" << name << ") have the same name as an other (" << scvar->getName() << ")";
            exit(1);
        }
        scvar->addValue(value);
    }
}
void SimulationManager_saveLongVariable(CSimulationManager* manager, unsigned int profiling_number, const char* name, long value){
    simumanager::SimulationManager* cppmanager = (simumanager::SimulationManager*) manager;
    simumanager::Variable* var = cppmanager->getVariable(profiling_number);
    simumanager::ScalarVariable<long> *scvar;
    if(var == NULL){
        scvar = new simumanager::ScalarVariable<long>(std::string(name));
        scvar->addValue(value);
        cppmanager->saveVariable(profiling_number, scvar);
    }
    else{
        scvar = dynamic_cast< simumanager::ScalarVariable<long>* >(var);
        assert(scvar != NULL && "A scalar variable have the same profiling number as an array variable");
        if(scvar->getName() != name){
            std::cerr << "A scalar variable (" << name << ") have the same name as an other (" << scvar->getName() << ")";
            exit(1);
        }
        scvar->addValue(value);
    }
}
void SimulationManager_saveIntVariable(CSimulationManager* manager, unsigned int profiling_number, const char* name, int value){
    simumanager::SimulationManager* cppmanager = (simumanager::SimulationManager*) manager;
    simumanager::Variable* var = cppmanager->getVariable(profiling_number);
    simumanager::ScalarVariable<int> *scvar;
    if(var == NULL){
        scvar = new simumanager::ScalarVariable<int>(std::string(name));
        scvar->addValue(value);
        cppmanager->saveVariable(profiling_number, scvar);
    }
    else{
        scvar = dynamic_cast< simumanager::ScalarVariable<int>* >(var);
        assert(scvar != NULL && "A scalar variable have the same profiling number as an array variable");
        if(scvar->getName() != name){
            std::cerr << "A scalar variable (" << name << ") have the same name as an other (" << scvar->getName() << ")";
            exit(1);
        }
        scvar->addValue(value);
    }
}
void SimulationManager_saveUIntVariable(CSimulationManager* manager, unsigned int profiling_number, const char* name, unsigned int value){
    simumanager::SimulationManager* cppmanager = (simumanager::SimulationManager*) manager;
    simumanager::Variable* var = cppmanager->getVariable(profiling_number);
    simumanager::ScalarVariable<unsigned int> *scvar;
    if(var == NULL){
        scvar = new simumanager::ScalarVariable<unsigned int>(std::string(name));
        scvar->addValue(value);
        cppmanager->saveVariable(profiling_number, scvar);
    }
    else{
        scvar = dynamic_cast< simumanager::ScalarVariable<unsigned int>* >(var);
        assert(scvar != NULL && "A scalar variable have the same profiling number as an array variable");
        if(scvar->getName() != name){
            std::cerr << "A scalar variable (" << name << ") have the same name as an other (" << scvar->getName() << ")";
            exit(1);
        }
        scvar->addValue(value);
    }
}

/******************************************/
/*            Array Variable              */
/******************************************/

void SimulationManager_saveDoubleArrayVariable(CSimulationManager* manager, unsigned int profiling_number, unsigned int index, const char* name, double value){
    simumanager::SimulationManager* cppmanager = (simumanager::SimulationManager*) manager;
    simumanager::Variable* var = cppmanager->getVariable(profiling_number);
    simumanager::ArrayVariable<double> *arrvar;
    if(var == NULL){
        arrvar = new simumanager::ArrayVariable<double>(std::string(name));
        arrvar->addValue(index, value);
        cppmanager->saveVariable(profiling_number, arrvar);
    }
    else{
        arrvar = dynamic_cast< simumanager::ArrayVariable<double>* >(var);
        assert(arrvar != NULL && "An array variable have the same profiling number as a scalar variable");
        if(arrvar->getName() != name){
            std::cerr << "An array variable (" << name << ") have the same name as an other (" << arrvar->getName() << ")";
            exit(1);
        }
        arrvar->addValue(index, value);
    }
}
void SimulationManager_saveFloatArrayVariable(CSimulationManager* manager, unsigned int profiling_number, unsigned int index, const char* name, float value){
    simumanager::SimulationManager* cppmanager = (simumanager::SimulationManager*) manager;
    simumanager::Variable* var = cppmanager->getVariable(profiling_number);
    simumanager::ArrayVariable<float> *arrvar;
    if(var == NULL){
        arrvar = new simumanager::ArrayVariable<float>(std::string(name));
        arrvar->addValue(index, value);
        cppmanager->saveVariable(profiling_number, arrvar);
    }
    else{
        arrvar = dynamic_cast< simumanager::ArrayVariable<float>* >(var);
        assert(arrvar != NULL && "An array variable have the same profiling number as a scalar variable");
        if(arrvar->getName() != name){
            std::cerr << "An array variable (" << name << ") have the same name as an other (" << arrvar->getName() << ")";
            exit(1);
        }
        arrvar->addValue(index, value);
    }
}
void SimulationManager_saveLongArrayVariable(CSimulationManager* manager, unsigned int profiling_number, unsigned int index, const char* name, long value){
    simumanager::SimulationManager* cppmanager = (simumanager::SimulationManager*) manager;
    simumanager::Variable* var = cppmanager->getVariable(profiling_number);
    simumanager::ArrayVariable<long> *arrvar;
    if(var == NULL){
        arrvar = new simumanager::ArrayVariable<long>(std::string(name));
        arrvar->addValue(index, value);
        cppmanager->saveVariable(profiling_number, arrvar);
    }
    else{
        arrvar = dynamic_cast< simumanager::ArrayVariable<long>* >(var);
        assert(arrvar != NULL && "An array variable have the same profiling number as a scalar variable");
        if(arrvar->getName() != name){
            std::cerr << "An array variable (" << name << ") have the same name as an other (" << arrvar->getName() << ")";
            exit(1);
        }
        arrvar->addValue(index, value);
    }
}
void SimulationManager_saveIntArrayVariable(CSimulationManager* manager, unsigned int profiling_number, unsigned int index, const char* name, int value){
    simumanager::SimulationManager* cppmanager = (simumanager::SimulationManager*) manager;
    simumanager::Variable* var = cppmanager->getVariable(profiling_number);
    simumanager::ArrayVariable<int> *arrvar;
    if(var == NULL){
        arrvar = new simumanager::ArrayVariable<int>(std::string(name));
        arrvar->addValue(index, value);
        cppmanager->saveVariable(profiling_number, arrvar);
    }
    else{
        arrvar = dynamic_cast< simumanager::ArrayVariable<int>* >(var);
        assert(arrvar != NULL && "An array variable have the same profiling number as a scalar variable");
        if(arrvar->getName() != name){
            std::cerr << "An array variable (" << name << ") have the same name as an other (" << arrvar->getName() << ")";
            exit(1);
        }
        arrvar->addValue(index, value);
    }
}
void SimulationManager_saveUIntArrayVariable(CSimulationManager* manager, unsigned int profiling_number, unsigned int index, const char* name, unsigned int value){
    simumanager::SimulationManager* cppmanager = (simumanager::SimulationManager*) manager;
    simumanager::Variable* var = cppmanager->getVariable(profiling_number);
    simumanager::ArrayVariable<unsigned int> *arrvar;
    if(var == NULL){
        arrvar = new simumanager::ArrayVariable<unsigned int>(std::string(name));
        arrvar->addValue(index, value);
        cppmanager->saveVariable(profiling_number, arrvar);
    }
    else{
        arrvar = dynamic_cast< simumanager::ArrayVariable<unsigned int>* >(var);
        assert(arrvar != NULL && "An array variable have the same profiling number as a scalar variable");
        if(arrvar->getName() != name){
            std::cerr << "An array variable (" << name << ") have the same name as an other (" << arrvar->getName() << ")";
            exit(1);
        }
        arrvar->addValue(index, value);
    }
}

/******************************************/
/*            Output command              */
/******************************************/

void SimulationManager_writeHuman(CSimulationManager* manager, const char* outputFolderPath){
    ((simumanager::SimulationManager*) manager)->writeHuman(std::string(outputFolderPath));
}

void SimulationManager_writeBinary(CSimulationManager* manager, const char* outputFolderPath){
    ((simumanager::SimulationManager*) manager)->writeBinary(std::string(outputFolderPath));
}
