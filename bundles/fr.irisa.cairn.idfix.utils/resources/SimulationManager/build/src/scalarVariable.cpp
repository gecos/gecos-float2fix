#include "scalarVariable.hpp"

template <class T> static std::string XMIScalarName();
template <> std::string XMIScalarName<float>(){
    return "FloatValue"; 
}
template <> std::string XMIScalarName<double>(){
    return "DoubleValue"; 
}
template <> std::string XMIScalarName<long>(){
    return "LongValue"; 
}
template <> std::string XMIScalarName<int>(){
    return "IntValue"; 
}
template <> std::string XMIScalarName<unsigned int>(){
    return "UIntValue"; 
}
template <class T> static std::string XMITypeName();
template <> std::string XMITypeName<float>(){
    return "FLOAT"; 
}
template <> std::string XMITypeName<double>(){
    return "DOUBLE"; 
}
template <> std::string XMITypeName<long>(){
    return "LONG"; 
}
template <> std::string XMITypeName<int>(){
    return "INTEGER"; 
}
template <> std::string XMITypeName<unsigned int>(){
    return "UINTEGER"; 
}


namespace simumanager{
    template<class T> void ScalarVariable<T>::addValue(T value){
        m_number_adding++;
        m_values.push_back(value);
    }
    
    
    template<class T> void ScalarVariable<T>::writeHuman(std::fstream &fs){
        typename std::list<T>::iterator it;
        fs << "VariableKind: " << 0 << std::endl;
        fs << m_number_adding << std::endl;
        for(it = m_values.begin() ; it != m_values.end() ; it++){
            fs << *it << std::endl; 
        }
        fs << std::endl;
    }
    
    template<class T> void ScalarVariable<T>::writeBinary(std::fstream &fs){
        typename std::list<T>::iterator it;
        short varKind = 0;
        fs.write((char*)&varKind, sizeof(varKind));
        fs.write((char*)&m_number_adding, sizeof(m_number_adding));
        for(it = m_values.begin() ; it != m_values.end() ; it++){
            fs.write((char*)&(*it), sizeof(*it));
        }
    }
    template class ScalarVariable<double>;
    template class ScalarVariable<float>;
    template class ScalarVariable<long>;
    template class ScalarVariable<int>;
    template class ScalarVariable<unsigned int>;
}
