#include "Tool.h"
#include <limits.h>

int max(int i1, int i2) {
	if (i1 > i2) {
		return i1;
	} else {
		return i2;
	}
}

int min(int i1, int i2) {
	if (i1 < i2) {
		return i1;
	} else {
		return i2;
	}
}

/**
 * CalculBruitMean
 * \param double q : quantification step
 * \param int k   : number of bits eliminated
 * \return double
 */
double CalculBruitMean(double q, int k, int m) {
	double temp;
	switch (m) {
	case 0: // Troncation
		if (k > 0) {
			temp = (q / 2) * (1 - pow(2, -k));
			if (temp == INFINITY) {
				printf("*** ERROR ComputePB\n");
				printf("*** ERROR CalculBruitMean : Infinity\n");
				printf("*** q: %f\n", q);
				printf("*** k: %i\n", k);
				exit(0);
			}
			return temp;
		} else {
			return 0;
		}
		break;
	case 1: // Rounding
		if (k > 0) {
			temp = (q / 2) * pow(2, -k);
			if (temp == INFINITY) {
				printf("*** ERROR ComputePB\n");
				printf("*** ERROR CalculBruitMean : Infinity\n");
				printf("*** q: %f\n", q);
				printf("*** k: %i\n", k);
				exit(0);
			}
			return temp;
		} else {
			return 0;
		}
		break;
	default:
		printf("*** ERROR ComputePB\n");
		printf("*** ERROR CalculBruitMean : Type de quantification non géré\n");
		printf("Type de quantification utilisé : %i\n", m);
		exit(0);
	}
}
/**
 * CalculBruitVar
 * \param double q : quantification step
 * \param int k   : number of bits eliminated
 * \return double
 */
double CalculBruitVar(double q, int k, int m) {
	double temp;
	switch (m) {
	case 0: // Troncation
	case 1: // Rounding
		if (k > 0) {
			temp = ((q * q) / 12) * (1 - pow(2, -2 * k));
			if (temp == INFINITY) {
				printf("*** ERROR ComputePB\n");
				printf("*** ERROR CalculBruitVar : Infinity\n");
				printf("*** q: %f\n", q);
				printf("*** k: %i\n", k);
				exit(0);
			}
			return temp;
		} else {
			return 0;
		}
		break;
	default:
		printf("*** ERROR ComputePB\n");
		printf("*** ERROR CalculBruitVar : Type de quantification non géré\n");
		printf("Type de quantification utilisé : %i\n", m);
		exit(0);
	}
}

/**
 * CalculBruitMean
 * \param double q : quantification step
 * \param int k   : number of bits eliminated
 * \return double
 */
double M(double q, int k, int m) {
	switch (m) {
	case 0: // Troncation
		if (k > 0) {
			return (q / 2) * (1 - pow(2, -k));
		} else {
			return 0;
		}
		break;
	case 1: // Rounding
		if (k > 0) {
			return (q / 2) * pow(2, -k);
		} else {
			return 0;
		}
		break;
	default:
		printf("*** ERROR ComputePB\n");
		printf("*** ERROR CalculBruitMean : Type de quantification non géré\n");
		printf("Type de quantification utilisé : %i\n", m);
		exit(0);
	}
}
/**
 * CalculBruitVar
 * \param double q : quantification step
 * \param int k   : number of bits eliminated
 * \return double
 */
double V(double q, int k, int m) {
	switch (m) {
	case 0: // Troncation
	case 1: // Rounding
		if (k > 0) {
			return ((q * q) / 12) * (1 - pow(2, -2 * k));
		} else {
			return 0;
		}
		break;
	default:
		printf("*** ERROR ComputePB\n");
		printf("*** ERROR CalculBruitVar : Type de quantification non géré\n");
		printf("Type de quantification utilisé : %i\n", m);
		exit(0);
	}
}

/**
 * fK
 * \param int k : number of bits eliminated
 * \return int
 */
int fK(int k) {
	if (k > 0) {
		return k;
	} else {
		return 0;
	}
}

/**
 *Return the number of significative bit of a constant
 */
int nbBitSignif(double value, int fw){
	double tmp = value * pow(2,fw);
	if(tmp > LLONG_MAX){
		printf("*** ERROR ComputePB (nbBitSignif function)\n");
		printf("*** Value cannot hold in long long int overflow in the computation of the non significant bit of constant\n");
		exit(0);
	}

	long long int valueb = tmp;
	int i = 0;
	while(i < fw){

		if(valueb%2 == 1)
			break;
		else
			valueb = valueb >> 1;
		i++;
	}

	return fw - i;
}
