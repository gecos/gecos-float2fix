#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <stdlib.h>

int max(int i1, int i2);
int min(int i1, int i2);
double CalculBruitMean(double q, int k, int m);
double CalculBruitVar(double q, int k, int m);
double M(double q, int k, int m);
double V(double q, int k, int m);
int fK(int k);
int nbBitSignif(double value, int fw);
