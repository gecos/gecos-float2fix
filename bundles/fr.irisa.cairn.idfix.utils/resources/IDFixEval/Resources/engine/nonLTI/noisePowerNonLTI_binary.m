function [K, L] = noisePowerNonLTI_binary(outputIDFixFolder, outputFileName)

pathFile = strcat(outputIDFixFolder , outputFileName);
fid = fopen(pathFile,'wb');

fprintf('\nAdd Path: ');
outputIDFixFolder
addpath(outputIDFixFolder);

fprintf('\nLoad simulated values... ')
simulatedValues

fprintf('\nLoad arithmetic operation...')
MatlabArithmericOp

fprintf('\nLoad transfer function...')
ParamFTNonLti

fprintf('\nK and L matrix value processing...')
[K,L] = NoisePower(Hg);

fprintf('\nWrite K matrix in binary file...')
nbOut = size(K,1);
nbBr = size(K,2);
for(n=1:nbOut)
	for(m=1:nbBr)
		fwrite(fid,K(n,m), 'double');
	end
end

fprintf('\nWrite L matrix in binary file...')
nbOut = size(L,1);
nbBr = size(L,2);
for(n = 1:nbOut)
	for(m = 1:nbBr)
		for(j = m:nbBr)
			fwrite(fid, L(n, m, j), 'double');
		end
	end
end
fclose(fid)
fprintf('\nend')


