%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        Output Noise power                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%Fonction calculant l'expression de la puissance du bruit de quantification
%en sortie d'un syst�me


function [K, L]=NoisePower(Input);

%addpath('../@ft')

warning off;

Ne=size(Input,2); % Br
Na=size(Input,1); % Sortie
for(n=1:Na)
	for(m=1:Ne)
		%    Input(m).Size=[1,1];
		%[Input(m).Num,Input(m).Den]=CreerFT(Input(m));
		%Hr(n,m).ft=ft(Input(n,m));
		Hr(n,m).ft=imp(Input(n,m).tf);%DeterminationRepImpuls(Input(m).Num,Input(m).Den); %%D�termination de la r�ponse impulsionnelle%
		Hr(n,m).ft=coeflineaire(Hr(n,m).ft);%%Calcul des coefficients de pr�diction%
		Hr(n,m).ft=rep(Hr(n,m).ft);%%somme des termes de r�ponse impulsionnelle% 
		Hr(n,m).ft=rep2(Hr(n,m).ft);%somme des termes de r�ponse impulsionnelle au carr�e%
	end
end

for(n=1:Na)
	for(m=1:Ne)
		K(n,m)=covariance(Hr(n,m).ft);%calcul des termes de covariance a partir de la r�ponse impulsionnnelle au carr�e%
		% B(m)=me(H(m).ft);
		for(p=m:Ne)
			L(n,m,p)=moyenne(Hr(n,m).ft,Hr(n,p).ft);%calcul des termes de moyenne% 
   		end
	end
end
