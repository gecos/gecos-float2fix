function [K, L] = noisePowerNonLTI(outputIDFixFolder, outputFileName)

fprintf('\nAdd Path: ');
outputIDFixFolder
addpath(outputIDFixFolder);

fprintf('\nLoad simulated values... ')
simulatedValues

fprintf('\nLoad arithmetic operation...')
MatlabArithmericOp

fprintf('\nLoad transfer function...')
ParamFTNonLti

fprintf('\nK and L matrix value processing...')
[K,L] = NoisePower(Hg);

fprintf('\nWrite K and L matrix in binary file...')
pathFile = strcat(outputIDFixFolder , outputFileName);
fid = fopen(pathFile,'w');

nbOut = size(K,1);
nbBr = size(K,2);
fprintf(fid,'\tstatic float HSumCarree[%i]={',nbOut * nbBr);

if(nbBr~=0)
	fprintf(fid, '%.30f', K(1,1));
end
for(m=2:nbBr)
	fprintf(fid,', %.30f',K(1,m));
end

for(n=2:nbOut)
	for(m=1:nbBr)
		fprintf(fid,', %.30f',K(n,m));
	end
end
fprintf(fid,'};\n\n');

nbOut = size(L,1);
nbBr = size(L,2);
fprintf(fid,'\tstatic float HSumCroise[%i]={\n',nbOut*nbBr*nbBr);

if(nbBr~=0)
	fprintf(fid, '%.30f', L(1,1,1));
end
for(j=2:nbBr)
	fprintf(fid,', %.30f',L(1,1,j));
end

for(m = 2:nbBr)
	for(j = 1:nbBr)
		fprintf(fid, ', %.30f', L(1, m, j));
	end
end

for(n = 2:nbOut)
	for(m = 1:nbBr)
		for(j = 1:nbBr)
			fprintf(fid, ', %.30f', L(n, m, j));
		end
	end
end
fprintf(fid, '};')
fclose(fid)
fprintf('\nend')
