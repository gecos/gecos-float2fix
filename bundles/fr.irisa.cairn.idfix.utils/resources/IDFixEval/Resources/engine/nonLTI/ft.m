classdef ft
%%Cr�ation de la classe fonction de transfert
   

   properties 
      Num
      Den
      h
      v
      m
      coef
   end
   
   % Class methods
   methods
      function obj = ft(c)
         % Construct a ft object using the coefficients supplied
         if isa(c,'ft')
            obj.Num = c.Num;
            obj.Den = c.Den;
            obj.h = c.h;
            obj.coef = c.coef;
            obj.v = c.v;
            obj.m = c.m;
         else
            obj.Num = c.Num;
            obj.Den = c.Den;
            obj.h = 0;
            obj.coef = 0;
            obj.v = 0;
            obj.m = 0;
         end
      end
      
      
      
      function disp(obj)
      % Fonction d'affichage
       Q=length(obj.Num);
       P=length(obj.Den);
       for(k=1:Q)
           disp(['Num(' int2str(k) ').g=' Num2str(obj.Num(k).g(1))]);
           for(p=2:length(obj.Num(k).g))
               disp(['         ' Num2str(obj.Num(k).g(p))]);
           end
           disp('  ');
       end
       for(k=1:P)
           disp(['Den(' int2str(k) ').f=' Num2str(obj.Den(k).f(1))]);
           for(p=2:length(obj.Den(k).f))
               disp(['         ' Num2str(obj.Den(k).f(p))]);
           end
           disp('  ');
       end
      end
      
      
      function r = plus(obj1,obj2)
         % Addition de 2 ft

         obj(1) = ft(obj1);
         obj(2) = ft(obj2);
         
         [Mq,indq]=max([length(obj(1).Num) length(obj(2).Num)]);
         [Mp,indp]=max([length(obj(1).Den) length(obj(2).Den)]);

         r.Num=obj(indq).Num;
         r.Den=obj(indp).Den;
         
         for(k=1:min(length(obj(1).Num),length(obj(2).Num)))
            r.Num(k).g=obj(1).Num(k).g+obj(2).Num(k).g;
         end
         for(k=1:min(length(obj(1).Den),length(obj(2).Den)))
            r.Den(k).f=obj(1).Den(k).f+obj(2).Den(k).f;
         end
         r=ft(r);
      end % plus
      

      
      function r = mtimes(obj1,obj2)
         % Multiplication de 2 ft
         obj(1) = ft(obj1);
         obj(2) = ft(obj2);
         Q1=length(obj(1).Num);
         Q2=length(obj(2).Num);
         for(k=1:Q1+Q2-1)
            r.Num(k).g=0;
         end
         for(m=1:Q1)
             for(p=1:Q2)
               r.Num(m+p-1).g=r.Num(m+p-1).g+obj(1).Num(m).g.*obj(2).Num(p).g; 
            end
         end
         P1=length(obj(1).Den);
         P2=length(obj(2).Den);
         for(k=1:P1+P2-1)
            r.Den(k).f=0;
         end
         for(p=1:P2)
            for(m=1:P1)
               r.Den(m+p-1).f=r.Den(m+p-1).f+obj(1).Den(m).f.*obj(2).Den(p).f; 
            end
         end 
         r=ft(r);
      end
            
         
         %D�termination de la reponse impulsionnelle
         
          function obj=imp(obj)
              obj=ft(obj);
              Q=length(obj.Num);
              P=length(obj.Den);

              M=1;
              for(p=1:Q)
                  if(length(obj.Num(p).g)>M)
                      M=length(obj.Num(p).g);
                  end
              end
              for(p=1:P)
                  if(length(obj.Den(p).f)>M)
                      M=length(obj.Den(p).f);
                  end
              end
              obj.h(1:M,1)=obj.Num(1).g;
              for(k=2:P+Q)
                 obj.h(:,k)=0;
                 for(p=1:min(k-1,P))
                    obj.h(:,k)=obj.h(:,k)+obj.h(:,k-p).*obj.Den(p).f;
                 end
                 if(k<=Q)
                    obj.h(:,k)=obj.h(:,k)+obj.Num(k).g;
                 end
              end
%               disp(' ');
%               disp('r�ponse impulsionnelle : ')
%               for(k=1:size(obj.h,2))
%                   for(m=1:size(obj.h,1))
%                     disp(['    h(',int2str(k),':)=',num2str(obj.h(m,k))]);
%                   end
%                   disp(' ')
%               end
          end
          
          %D�termination des coefficients lin�aires
          
          function obj=coeflineaire(obj)
              Q=length(obj.Num);
              P=length(obj.Den);
                if(P~=1) % si le syst�me est r�cursif alors calcul des coef de pr�diction
                   if(length(obj.Den(P).f)~=1) 
                    %%cr�ation de la matrice R
                        for(i=1:P)
                            for(k=1:P)
                                R(i,k)=mean(obj.h(:,Q+i-1).*obj.h(:,Q+k-1));
                            end
                        end
                        %%cr�ation de la matrice Q
                        for(k=1:P)
                             S(k)=mean(obj.h(:,P+Q).*obj.h(:,Q+k-1));
                        end
                        coef1=inv(R)*S;
                   else
                       for(k=1:P)
                           coef1(k)=obj.Den(k).f;
                       end
                   end
                   obj.coef=diag(ones(P-1,1),1);
                   obj.coef(P,:)=flipud(coef1');
                else%%syst�me non r�c alors coef nuls
                    obj.coef(1)=0;
                end
%                 disp(' ');
%                 disp('coefficients lin�aires : ')
%                 for(k=1:length(obj.coef))
%                      disp(['  coef(',int2str(k),')=',num2str(obj.coef(P,P-k+1))])
%                 end
%                 disp(' ');
                obj=ft(obj);
          end
          
          function obj=rep(obj)
              Q=length(obj.Num);
              P=length(obj.Den);
              Somme=0;
              if(P~=1)
                  for(m=1:P)
                      for(k=1:Q)
                          Gamma(k,m,:)=obj.h(:,k+m-1) ;
                      end
                      theta(m,1:length(obj.h(:,1)))=0;
                      for(n=1:m-1)
                          theta(m,:)=theta(m,:)+obj.h(:,n)';
                      end
                  end
                  for(m=1:Q-1)
                      Somme=Somme+Gamma(m,:,:);
                  end
                  Somme=(Somme+inv(eye(P)-obj.coef)*mean(reshape(Gamma(Q,:,k),P,1),3)+mean(theta,2))/P;
              else
                  for(k=1:Q)
                      Somme=Somme+obj.h(:,k);
                  end

              end

              obj.m=mean(Somme) ;
          end
          
          
          function L=moyenne(obj1,obj2)
             obj1=ft(obj1);
             obj2=ft(obj2);

             L=mean(obj1.m.*obj2.m);
          end
          
          function obj=rep2(obj)   
              Q=length(obj.Num);
              P=length(obj.Den);  
              Somme=0;
              if(P~=1)
                  for(m=1:P)
                      phi(m,m,1:length(obj.h(:,1)))=0;
                      for(k=1:Q)
                          Gamma(k,m,:)=obj.h(:,k+m-1) ;
                      end
                      for(n=1:m-1)
                          phi(m,m,:)=phi(m,m,:)+reshape(obj.h(:,n).*obj.h(:,n),1,1,length(obj.h(:,1)));
                      end
                  end
                  for(l=1:length(obj.h(:,1)))
                      for(k=1:Q)
                          G(k,1:P,1:P,l)=reshape(Gamma(k,:,l),P,1)*reshape(Gamma(k,:,l),1,P);
                      end
                  end
                  %S=dlyap(obj.coef,reshape(mean(mean(G(Q,:,:,:),1),4),P,P));
                   S=inv(1-obj.coef^2)*reshape(mean(mean(G(Q,:,:,:),1),4),P,P);
                  for(m=1:Q-1)
                      Somme=Somme+G(m,:,:,:);
                  end
                  Somme=1/P*trace(S+mean(phi(:,:,:),3)+Somme);

              else
                 for(k=1:Q)
                     Somme=Somme+obj.h(:,k).^2;
                 end 
              end
              obj.v=mean(Somme);
          end
          
          function K=covariance(obj)
              obj=ft(obj);
              K=mean(obj.v);
          end

%function p=me(obj)
    %         obj=ft(obj);
     %       p=obj.m;
      %    end

      end % methods 
%       
end   % classdef




