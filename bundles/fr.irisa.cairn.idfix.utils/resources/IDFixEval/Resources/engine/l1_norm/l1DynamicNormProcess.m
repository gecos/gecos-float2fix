function l1DynamicNormProcess(outputIDFixFolder, outputFileName, impulseNumberPoint)

pathFileValeurHgDb = strcat(outputIDFixFolder , outputFileName);
fid=fopen(pathFileValeurHgDb,'w');

fprintf('\nAdd Path: ');
outputIDFixFolder
addpath(outputIDFixFolder);

fprintf('\nLoad transfer functions...');
ParamDynFT;

fprintf('\nDynamic processing...');
% initialisation de la dynamique des noeuds autre que source
for i = 1 : nombreNoeudInter,
    Noeud(TabNumeroInter(i)).Dyn(1) = 0;
    Noeud(TabNumeroInter(i)).Dyn(2) = 0;
end

for i = 1 : nombreSortie,
    Noeud(TabNumeroSortie(i)).Dyn(1) = 0;
    Noeud(TabNumeroSortie(i)).Dyn(2) = 0;
end

for i = 1 : nombreSource,
	
	Ix = TabNumeroEntree(i);

        for j = 1 : nombreNoeud,
	
		Iy = j;		
		m = size(Hg(Ix,Iy).tf,1);
		
            if ( m ~= 0 )
                [range] = L1_norm(Noeud(Ix).Dyn(1), Noeud(Ix).Dyn(2), Hg(Ix,Iy).tf, impulseNumberPoint);
			
                Noeud(Iy).Dyn(1) = Noeud(Iy).Dyn(1) + range(1);
                Noeud(Iy).Dyn(2) = Noeud(Iy).Dyn(2) + range(2);
            end
        end
end

fprintf('\nWriting information in file...');
fprintf(fid,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n');
fprintf(fid,'<Dyn>\n');
for i = 1 : nombreNoeud,
	fprintf(fid,'<node id=\"%2i\">\n',i);
	%fprintf(fid,'<attr key=\"NumeroGraph\" value=\"%2i\"/>\n',i+1); 
	fprintf(fid,'<attr key=\"DynMax\" value=\"%2f\"/>\n',Noeud(i).Dyn(2));
	fprintf(fid,'<attr key=\"DynMin\" value=\"%2f\"/>\n',Noeud(i).Dyn(1));
    fprintf(fid,'</node>\n');
    
    %fprintf(fid,'%2f\n', Noeud(i).Dyn); 
end
fprintf(fid,'</Dyn>');
fclose(fid);
fprintf('\nEnd');
