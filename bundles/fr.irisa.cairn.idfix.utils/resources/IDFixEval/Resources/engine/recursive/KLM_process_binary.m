function KLM_process_binary(outputIDFixFolder, generatedFileName, numberImpulsePoint)

pathFile = strcat(outputIDFixFolder , generatedFileName);
fid = fopen(pathFile,'wb');

fprintf('\nAdd Path: ');
outputIDFixFolder
addpath(outputIDFixFolder);

fprintf('\nLoad transfer function...');
ParamFT;

fprintf('\nLoad transfer function...');
numberImpulsePoint

% Export de la moyenne des H carrée : HSumCarree[Iout][Iin]
fprintf('\nK matrix value processing and writing in file ...')
for IndiceTabSortie = 1 : nombreSortie,
    Iout = TabNumeroSortie(IndiceTabSortie);
    for Iin = 1 : nombreSource
        [m n] = size(Hg(Iout,Iin).tf);
        if (m ~= 0)
                Hg(Iout,Iin).h = impulse(Hg(Iout,Iin).tf, numberImpulsePoint);
                HSumCarree = sum(Hg(Iout,Iin).h.^2 );
		fwrite(fid, HSumCarree, 'double'); 
        end
    end   
end

fprintf('\nL matrix value processing and writing in file ...')
for IndiceTabSortie = 1 : nombreSortie,
    Iout = TabNumeroSortie(IndiceTabSortie);
    for Iin1 = 1 : nombreSource
        m = size(Hg(Iout,Iin1).tf,1);
        if (m ~= 0)
            Hg(Iout,Iin1).h = impulse(Hg(Iout,Iin1).tf, numberImpulsePoint);
        end		
        for Iin2 = Iin1 : nombreSource
            n = size(Hg(Iout,Iin2).tf,1);
            if ((m ~= 0) && (n ~= 0))
            	Hg(Iout,Iin2).h = impulse(Hg(Iout,Iin2).tf, numberImpulsePoint);
                HSumCroise = sum(sum(Hg(Iout,Iin1).h*Hg(Iout,Iin2).h.'));
                fwrite(fid, HSumCroise, 'double');
            end
        end
    end   
end
fclose(fid);
fprintf('\nend')
