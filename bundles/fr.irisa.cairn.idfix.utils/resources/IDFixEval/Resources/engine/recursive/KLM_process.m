function KLM_process(outputIDFixFolder, generatedFileName, numberImpulsePoint)

pathFile = strcat(outputIDFixFolder , generatedFileName);
fid = fopen(pathFile,'w');

fprintf('\nAdd Path: ');
outputIDFixFolder
addpath(outputIDFixFolder);

fprintf('\nLoad transfer function...');
ParamFT;

fprintf('\nLoad transfer function...');
numberImpulsePoint

fprintf(fid,'\n int TabNumeroSortie[]={'); 
for IndiceTabSortie = 1 : nombreSortie,

    Iout = TabNumeroSortie(IndiceTabSortie);

    if (IndiceTabSortie ~= nombreSortie)
        fprintf(fid,'%d,',Iout-1); 
  else
      fprintf(fid,'%d};',Iout-1);   
  end
end

% Export de la moyenne des H carrée : HSumCarree[Iout][Iin]
fprintf('\nK matrix value processing and writing in file ...')
fprintf(fid,'\n// === Moyenne des H carrées ===');
fprintf(fid,'\nstatic float HSumCarree[%d*%d] = {',nombreSortie,nombreSource); 
mod_trois = 0;
for IndiceTabSortie = 1 : nombreSortie,
    Iout = TabNumeroSortie(IndiceTabSortie);
    for Iin = 1 : nombreSource
        [m n] = size(Hg(Iout,Iin).tf);
        if (m ~= 0)
                Hg(Iout,Iin).h = impulse(Hg(Iout,Iin).tf, numberImpulsePoint);
                HSumCarree = sum(Hg(Iout,Iin).h.^2 );
                fprintf(fid,'%.30f',HSumCarree); 
                if ((IndiceTabSortie ~= nombreSortie) || (Iin ~= nombreSource))
                    fprintf(fid,', ');
                    if (mod_trois == 2)
                        fprintf(fid, '\n');
                        mod_trois = 0;
                else
                    mod_trois = mod_trois + 1;
                end
            end
        end
    end   
end
fprintf(fid,'};'); 

fprintf('\nL matrix value processing and writing in file ...')
fprintf(fid,'\n\n// === Moyenne des H croisés ==='); 
fprintf(fid,'\nstatic float HSumCroise[%d*%d*%d] = {',nombreSortie,nombreSource,nombreSource); 
mod_trois = 0;
for IndiceTabSortie = 1 : nombreSortie,
    Iout = TabNumeroSortie(IndiceTabSortie);
    for Iin1 = 1 : nombreSource
        m = size(Hg(Iout,Iin1).tf,1);
        if (m ~= 0)
            Hg(Iout,Iin1).h = impulse(Hg(Iout,Iin1).tf, numberImpulsePoint);
        end		
        for Iin2 = 1 : nombreSource
            n = size(Hg(Iout,Iin2).tf,1);
            if ((m ~= 0) && (n ~= 0))
                Hg(Iout,Iin2).h = impulse(Hg(Iout,Iin2).tf, numberImpulsePoint);
                HSumCroise = sum(sum(Hg(Iout,Iin1).h*Hg(Iout,Iin2).h.'));
                fprintf(fid,'%.30f',HSumCroise); 
                if ((IndiceTabSortie ~= nombreSortie) || (Iin1 ~= nombreSource) || (Iin2 ~= nombreSource))
                    fprintf(fid,',');
                    if (mod_trois == 2)
                        fprintf(fid, '\n');
                        mod_trois = 0;
                    else
                        mod_trois = mod_trois + 1;
                    end
                end
            end
        end
    end   
end
fprintf(fid,'};'); 
fclose(fid);
fprintf('\nend')
