#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdio.h>

#include "graph/dfg/Gfd.hh"


#ifndef BOOLEEN
#define BOOLEEN

#define boolean int
#define true 1
#define false 0

#endif

#define DOCUMENT "Document"
#define GRAPH "Graph"
#define INFOLIST "InfoList"
#define NODELIST "NodeList"
#define COMPUTATIONTIME "ComputationTime"
#define CYCLETIME "CycleTime"
#define NODENUMBER "NodeNumber"
#define FUNCTIONNUMBER "FunctionNumber"
#define NODE "Node"
#define NODENAME "NodeName"
#define NODENUM "NodeNum"
#define NEXTNODE "NextNode"
#define PREVIOUSNODE "PreviousNode"
#define OPERATION "Operation"
#define VARIABLE "Variable"
#define TYPENUMOP "TypeNum"
#define NBIN "NbIn"
#define FUNCTION "Function"
#define STARTDATE "StartDate"
#define ENDDATE "EndDate"
#define TYPENUMVAR "TypeNum"
#define INOUT "InOut"
#define TYPEVAR "TypeVar"
#define LOOP "Loop"
#define VARNAME "VarName"
#define VARVALUE "VarValue"

//TODO Fichier et méthode necessaire?? NS

// define indispensable pour la gestion des attributs


void ouvrirbalise(FILE * fic_user, char *xmlelement /*,char ** xmlattlist */) {
	fprintf(fic_user, "<%s>", xmlelement);
}

void fermerbalise(FILE * fic_user, char *xmlelement) {
	fprintf(fic_user, "</%s>\n", xmlelement);
}

void caracterebalise(FILE * fic_user, char *xmlcharacter) {
	fprintf(fic_user, "%s", xmlcharacter);
}

void generationbalise(FILE * fic_user, char *xmlelement, char *xmlcharacter) {
	ouvrirbalise(fic_user, xmlelement /*,NULL */);
	caracterebalise(fic_user, xmlcharacter);
	fermerbalise(fic_user, xmlelement);
}

void debutdocument(FILE * fic_user) {
	fprintf(fic_user, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE Graph SYSTEM \"Graph.dtd\">\n");
}

void Gfd::GFD2XML(const char *nomfichier) const {

	FILE *fic_user;

	fic_user = fopen(nomfichier, "w");

	debutdocument(fic_user);
	generationbalise(fic_user, (char *) VARVALUE, (char *) "30");

}

//int main(int argc, char **argv) {


//      char * filename;
//      filename = argv[1];

//      GFD2XML(filename);


//      return(0);
//}
