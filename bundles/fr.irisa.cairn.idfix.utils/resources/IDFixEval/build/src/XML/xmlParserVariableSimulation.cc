/**
 \author Nicolas Simon 
 \date 11/07/2011

 Lecture du fichier simulatedValues.xml indiquant le résultat de la simulation pour chaque noeud réprésentant une variable dans le code original.
 */
#include <map>
#include <stack>
#include <string.h>
#include <sstream>
#include <libxml/SAX2.h>

#include "graph/dfg/Gfd.hh"
#include "utils/Tool.hh"


typedef enum {
	OUT_AFFECTATION, IN_AFFECTATION, VALUES, NOT_DATA_IN_FLOW
} TypeEtatXml;


TypeEtatXml etat;

class t_simulatedNodeCtx {
public:
	list<NoeudData*> simulatedNodeList;
	fstream * matFile;

	t_simulatedNodeCtx(list<NoeudData*> simulatedNode_in, fstream * matFile_in){
		simulatedNodeList = simulatedNode_in;
		matFile = matFile_in;
	}
};

// Callback function for libxml - start of an XML element (a block)
void simulatedValuesElementStart(void *ctx, const xmlChar * fullName, const xmlChar ** atts) {
	list<NoeudData*>::iterator itSimulatedNodeList;
	NoeudData* nData = NULL;

	// Get the context
	t_simulatedNodeCtx *context = (t_simulatedNodeCtx *) ctx;

	// Detection et création des entêtes des valeurs simulées pour les variables qui ne sont pas des entrées mais seulement des affectations
	if (xmlStrEqual(fullName, BAD_CAST("affectationList"))) {
		etat = IN_AFFECTATION;
		int affectNumber = atoi((const char*) atts[1]);
	
		for(itSimulatedNodeList = context->simulatedNodeList.begin() ; itSimulatedNodeList != context->simulatedNodeList.end() ; itSimulatedNodeList++){
			if((*itSimulatedNodeList)->getNumAffect() == affectNumber){
				nData = (*itSimulatedNodeList);
				break;
			}
		}
		
		if(nData == NULL){
			etat = NOT_DATA_IN_FLOW;
		}
		else{
			*(context->matFile) << nData->getName().data() << " = [";		
		}
	}
	// Detection et création des entêtes pour les variables qui sont des entrées du graphe et donc du code associé
	else if(xmlStrEqual(fullName, BAD_CAST("variableList"))) {
		etat = IN_AFFECTATION;
		int numDataCDFG = atoi((const char*) atts[1]);
		string index = (const char*) atts[3]; 
		for(itSimulatedNodeList = context->simulatedNodeList.begin() ; itSimulatedNodeList != context->simulatedNodeList.end() ; itSimulatedNodeList++){
			if((*itSimulatedNodeList)->getNumCDFG() == numDataCDFG && (*itSimulatedNodeList)->getIndexArray() == index){
				nData = (*itSimulatedNodeList);
				break;
			}
		}
//		assert(nData != NULL);
        if(nData != NULL)
    		*(context->matFile) << nData->getName().data() << " = [";
        else
            etat = NOT_DATA_IN_FLOW;
	}
	else if(xmlStrEqual(fullName, BAD_CAST("values")) && etat != NOT_DATA_IN_FLOW) {
		etat = VALUES;
	}
}

// Callback function for libxml - end of an XML element (a block)
void simulatedValuesElementEnd(void *ctx, const xmlChar * fullName) {
	// Get the context
	t_simulatedNodeCtx *context = (t_simulatedNodeCtx *) ctx;
	if(etat == IN_AFFECTATION){
		etat = OUT_AFFECTATION;
		*(context->matFile) << " ];"<<endl<<endl;
	}
	else if (etat == VALUES){
		*(context->matFile)<<" ";
		etat = IN_AFFECTATION;
	}
}


void simulatedValuesCharacters(void* ctx, const xmlChar * ch, int len){
	t_simulatedNodeCtx *context = (t_simulatedNodeCtx*) ctx;
	if(etat == VALUES){
		*(context->matFile)<<(const char*) xmlStrndup(ch, len);
	}
}

/**
 * Récupere les informations issue de la simulation effectué de IDFix-Conv nécessaire pour le traitement non LTI au sein de IDFix-Eval
 *
 * \param path : Chemin du fichier xml a parser
 * \param pathGeneratedFile : Chemin du fichier générer en fonction des données fourni via le xml
 *
 * \returns false on error
 */
bool Gfd::readSimulatedNode(const char* path, string pathGeneratedFile) {
	// Setup XML parser
	// We use a SAX parser, not a DOM one, as it is better for this data
	// structure (we don't want a tree).
	// See http://www.jamesh.id.au/articles/libxml-sax/libxml-sax.html for
	// more information about XML parsing.
	xmlSAXHandler xmlHandler;

	// Set all pointers in the handler to NULL
	memset(&xmlHandler, 0, sizeof(xmlHandler));

	// Called when the document ends
	xmlHandler.startElement = simulatedValuesElementStart;
	// Called when an element (a block) starts
	xmlHandler.endElement = simulatedValuesElementEnd;
	// Called when an element (a block) ends
	xmlHandler.characters = simulatedValuesCharacters;

	// Browse the file and examine the blocks
	NodeGraphContainer::iterator it;
	list<NoeudData*> simulatedNodeList;
	list<NoeudData*>::iterator itSimulatedNodeList;
	NoeudData* nData;

	// Création de la list contenant les noeuds data représentant un noeud simulé pour l'ajout dans la structure de donnée fourni au parser xml
	for(it = this->tabNode->begin() ; it != this->tabNode->end() ; it++){
		nData = dynamic_cast<NoeudData*> (*it);
		if(nData == NULL)
			continue;

		if(nData->getNumAffect() != -1 || nData->isNodeInput()){
			simulatedNodeList.push_back(nData);
		}
	}

	fstream matFile;
	matFile.open(pathGeneratedFile.data(), fstream::out);
	assert(matFile.is_open());
	t_simulatedNodeCtx user_data(simulatedNodeList, &matFile);

	if (xmlSAXUserParseFile(&xmlHandler, &user_data, path) < 0) {
		trace_error("Parsing probabilities failed\n");
		exit(0);
		matFile.close();
		return false;
	}
	matFile.close();
	return true;
}
