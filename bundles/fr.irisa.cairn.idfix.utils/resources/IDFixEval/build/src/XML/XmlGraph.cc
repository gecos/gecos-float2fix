/**
 *  \author Daniel Menard
 *  \date   22.2.2008
 *  \version 1.0
 */
#include <stdlib.h>
#include <math.h>
#include <string>
#include <stdio.h>
#include <utils/Tool.hh>
#include <cctype>
#include <sstream>

#include "graph/dfg/datanode/NoeudData.hh"
#include "graph/dfg/datanode/NoeudSrc.hh"
#include "graph/dfg/EdgeGFD.hh"
#include "graph/dfg/Gfd.hh"
#include "graph/NoeudGraph.hh"
#include "graph/dfg/operatornode/kind/ADDNode.hh"
#include "graph/dfg/operatornode/kind/DELAYNode.hh"
#include "graph/dfg/operatornode/kind/DIVNode.hh"
#include "graph/dfg/operatornode/kind/MULNode.hh"
#include "graph/dfg/operatornode/kind/PHINode.hh"
#include "graph/dfg/operatornode/kind/SETNode.hh"
#include "graph/dfg/operatornode/kind/SUBNode.hh"
#include "graph/toolkit/TypePrint.hh"
#include "graph/toolkit/Types.hh"
#include "graph/toolkit/TypeVar.hh"
#include "graph/Edge.hh"
#include "xml/XmlGraph.hh"

//------------------------------------------------ XmlAtt ------------------------------------------------
/**
 * 	Constructeur de la classe #XmlAtt
 *	\param Key cle (chaine de caracteres)
 *	\param Value valeur de la cle (chaine de caracteres)
 */
XmlAtt::XmlAtt(string key, string value) {
	this->key = key;
	this->value = value;
}

/**
 * 	Destructeur de la classe #XmlAtt
 */
XmlAtt::~XmlAtt() {
}

//------------------------------------------------ XmlElt ------------------------------------------------
/**
 * Constructeur de la classe #XmlElt
 * \param in
 */
XmlElt::XmlElt(XmlGraph & in) :
	ownerGraph(in) {
}

/**
 *	Destructeur de la classe #XmlElt
 */
XmlElt::~XmlElt() {
}

/**
 * 	Ajout d'un attribut a l'element courant #XmlElt
 *	\param Key : cle (chaine de caracteres)
 *	\param Value : valeur de la cle (chaine de caracteres)
 */
void XmlElt::addAttributs(char *Key, char *Value) {
	ListeAtt.push_front(XmlAtt(Key, Value));
}

//------------------------------------------------ XmlEdge ------------------------------------------------

/**
 * 	Constructeur de la classe #XmlEdge
 *
 *	\param Depart Numero du noeud de depart (chaine de caracteres)
 *	\param Arrivee Numero du noeud d'arrivee (chaine de caracteres)
 */
XmlEdge::XmlEdge(XmlGraph & in, char *Depart, char *Arrivee) :
	XmlElt(in) {
	this->Depart = atoi(Depart);

	this->Arrivee = atoi(Arrivee);

}

/**
 * 	Destructeur de la classe #XmlEdge
 */
XmlEdge::~XmlEdge() {

}

//------------------------------------------------ XmlNode ------------------------------------------------
/**
 * 	Constructeur de la classe #XmlNode
 *	\param Nom : Nom du noeud (chaine de caracteres)
 *	\param Id : Numero du noeud (chaine de caracteres)
 */
XmlNode::XmlNode(XmlGraph & in, char *Nom, char *Id) :
	XmlElt(in) {
	this->Nom = new string(Nom); // Nom du noeud
	this->Num = atoi(Id); // Numero du noeud
}

/**
 * 	Destructeur de class de la classe #XmlNode
 */
XmlNode::~XmlNode() {
	delete Nom;
}

/**
 * 	typeVarFonction de creation d'un #NoeudGraph à partir d'un #XmlNode
 *	\return : #NoeudGraph cree
 */
NoeudGraph *XmlNode::createNode(bool type) {
	//list < XmlAtt >::iterator IndexListAtt;      // Liste des Attribus (XML)

	NoeudGraph *Node = NULL;
	TypeXmlOpsData getTypeNodeGraph = DATA_XML; // Initialisation aleatoire

	TypeVarFonct typeVarFct = VAR; // Type de variable  ( VAR_ENTREE, VAR_SORTIE, VAR, VAR_INT, CONST, CONST_SORTIE)

	NoeudData *Data;
	NoeudOps *Ops = NULL;
	TypeVar *TypeVarData;
	float ValData(0);
	DataDynamic dataDynamic;
	dataDynamic.valMax = 0;
	dataDynamic.valMin = 0;
	int numOpsCDFG = -1;
	int numDataCDFG = -1;
	int numOpsSFG = -1;
	int numAffect = -1; // Numero d'affectation si le noeud a été simulé pour la partie non LTI, -1 sinon
	string originalName;
	string varName;
	string keyName = "";

	bool variableWidth = false; // Indique si la largeur en bit de du noeud data est variable (via l'utilisateur) on non. non variable par défaut
	DynamicProcess dynamicProcess = INTERVAL; // mode de calcul de la dynamique par défaut 
	float probDeb = -1; // -1 par defaut même si on est pas en mode OVERFLOW_PROBA pour la dynamique 
	string indexTabData = ""; // Défini l'index de la donnée si celle-ci est contenu dans un tableau, vide si non précisé. Ceci sert uniquement pour l'affichage 
	int blockNum = -1;

	//DEBUG("precision de la fonction STRTOD a verifier\n");
	for (list<XmlAtt>::iterator it = ListeAtt.begin(); it != ListeAtt.end(); ++it) {

		if (!it->getKey().compare("NAME")) { // DATA
			keyName = it->getValue();
		}
		if (!it->getKey().compare("VALUE")) { // DATA:VARIABLE_VAL_KNOWN &&DATA:CONSTANT
			ValData = atof(it->getValue().c_str());
		}
		/*if (!it->getKey().compare("NB_IN")) { // OP
		 string valeur = it->getValue();
		 nbIn = atoi(valeur.c_str());
		 }*/

		if (!it->getKey().compare("NUM_BASIC_BLOCK")) { // ALL
			string val = it->getValue();
            blockNum = atoi(val.c_str());
            if(blockNum < 0){
                trace_error("A basic block number is negative. Wrong information");
                exit(0);
            }
			ownerGraph.addBlock(blockNum); //xxx
			//DEBUG("Creation bloc %d %d\n",blockNum, altNum);
		}

		if (!it->getKey().compare("VAL_MIN")) { // DATA:VARIABLE_VAL_UNKNOWN:INPUT
			dataDynamic.valMin = atof(it->getValue().c_str());
		}
		if (!it->getKey().compare("VAL_MAX")) { // DATA:VARIABLE_VAL_UNKNOWN:INPUT
			dataDynamic.valMax = atof(it->getValue().c_str());
		}

		if (!it->getKey().compare("NUM_OP_CDFG")) { // OP
			string valeur = it->getValue();
			numOpsCDFG = atoi(valeur.c_str());
		}
		if (!it->getKey().compare("NUM_OP_SFG")) { // OP
			string valeur = it->getValue();
			numOpsSFG = atoi(valeur.c_str());
		}
		if (!it->getKey().compare("NUM_DATA_CDFG")) { // DATA:VARIABLE_VAL_UNKNOWN && DATA:VARIABLE_VAL_KNOWN
			string valeur = it->getValue();
			numDataCDFG = atoi(valeur.c_str());
		}

		if (!it->getKey().compare("CLASS")) { // ALL
			if (!it->getValue().compare("DATA")) {
				getTypeNodeGraph = DATA_XML;
			}
			else if (!it->getValue().compare("OP")) {
				getTypeNodeGraph = OPS_XML;
            }
        }
        if (!it->getKey().compare("NATURE")) { // ALL
            if (!it->getValue().compare("VARIABLE_VAL_UNKNOWN")) {

                for (list<XmlAtt>::iterator it2 = ListeAtt.begin(); it2 != ListeAtt.end(); ++it2) {
                    if (!it2->getKey().compare("PROPERTY")) {
                        if (!it2->getValue().compare("INPUT")) {
                            typeVarFct = VAR_ENTREE;
                        }
                        else if (!it2->getValue().compare("OUTPUT")) {
                            typeVarFct = VAR_SORTIE;
                        }
                        else {
                            typeVarFct = VAR;
                        }
                    }
                }
            }
            else if (!it->getValue().compare("VARIABLE_VAL_KNOWN") || !it->getValue().compare("CONSTANT")) {
                typeVarFct = CONST;
            }
            else if (!it->getValue().compare("ADD")) {
                if(blockNum < 0){
                    trace_error("An operator have not a basic block information\n");
                    exit(0);
                }
                Ops = new gfd::ADDNode(ownerGraph.getBlock(blockNum)); 
                Ops->setNumero(getNum());
            }
            else if (!it->getValue().compare("SUB")) {
                if(blockNum < 0){
                    trace_error("An operator have not a basic block information\n");
                    exit(0);
                }
                Ops = new gfd::SUBNode(ownerGraph.getBlock(blockNum)); 
                Ops->setNumero(getNum());
            }
            else if (!it->getValue().compare("MUL")) {
                if(blockNum < 0){
                    trace_error("An operator have not a basic block information\n");
                    exit(0);
                }
                Ops = new gfd::MULNode(ownerGraph.getBlock(blockNum)); 
                Ops->setNumero(getNum());
            }
            else if (!it->getValue().compare("DIV")) {
                if(blockNum < 0){
                    trace_error("An operator have not a basic block information\n");
                    exit(0);
                }
                Ops = new gfd::DIVNode(ownerGraph.getBlock(blockNum)); 
                Ops->setNumero(getNum());
            }
            else if(!it->getValue().compare("NEG")){
                trace_error("Operator NEG not implemented yet");
                exit(0);
            }
            else if (!it->getValue().compare("SHR")) {
                trace_error("Operator SHR not implemented yet");
                exit(0);
            }
            else if (!it->getValue().compare("SHL")) {
                trace_error("Operator SHL not implemented yet");
                exit(0);
            }
            else if (!it->getValue().compare("ABS")) {
                trace_error("Operator ABS not implemented yet");
                exit(0);
            }
            else if (!it->getValue().compare("DELAY")) {
                if(blockNum < 0){
                    trace_error("An operator have not a basic block information\n");
                    exit(0);
                }
                Ops = new gfd::DELAYNode(ownerGraph.getBlock(blockNum)); 
                Ops->setNumero(getNum());
            }
            else if (!it->getValue().compare("EQ")) {
                if(blockNum < 0){
                    trace_error("An operator have not a basic block information\n");
                    exit(0);
                }
                Ops = new gfd::SETNode(ownerGraph.getBlock(blockNum)); 
                Ops->setNumero(getNum());
            }
            else if (!it->getValue().compare("PHI")) {
                Ops = new gfd::PHINode(ownerGraph.getBlock(-1)); 
                Ops->setNumero(getNum());
            }
            else if(!it->getValue().compare("SQRT")){
                trace_error("Operator SQRT not implemented yet");
                exit(0);
            }
        }

        if(!it->getKey().compare("DYNAMIC_PROCESSING")){
            if(!it->getValue().compare("OVERFLOW"))
                dynamicProcess = OVERFLOW_PROBA;
        }

        if (!it->getKey().compare("OVERFLOW_PROBABILITY")) {
            probDeb = atof(it->getValue().c_str());
        }

        if (!it->getKey().compare("INDEXES")){
			indexTabData = it->getValue();
		}

		if (!it->getKey().compare("NUM_AFFECT")) {
			numAffect = atoi(it->getValue().data());
		}

		if (!it->getKey().compare("VARIABLE_WIDTH")){
			if(!it->getValue().compare("TRUE")){
				variableWidth = true;
			}
		}
	}
	// Construction

	// Attributs du nom
	originalName = keyName;
	varName = RuntimeParameters::generateVarName(originalName);

	if (getTypeNodeGraph == DATA_XML) // Ou CLASS_VARIABLE
	{

		//cout << "DATA_XML" << endl;
		//if(VarFct == VAR_ENTREE) cout << "       VAR_ENTREE"<<endl;
		//if(VarFct == VAR_SORTIE) cout << "       VAR_SORTIE"<<endl;

		//Creation d'un NoeudData;
		TypeVarData = new TypeVar(typeVarFct);

		if(indexTabData != "")
			originalName += indexTabData;


		if (typeVarFct == VAR_ENTREE && type) {
			Data = new NoeudSrc(DATA_SRC, getNum(), varName, TypeVarData, originalName);
		}
		else {
			Data = new NoeudData(DATA_BASE, getNum(), varName, TypeVarData, originalName);
		}
		Data->setTypeSignalBruit(SIGNAL);

		if (dataDynamic.valMin == 0 && dataDynamic.valMax == 0) { // no dynamic, like constant
			dataDynamic.valMax = ValData;
			dataDynamic.valMin = ValData;
			Data->setDataDynamic(dataDynamic);
		}
		else {
			Data->setDataDynamic(dataDynamic);
		}

		Data->setNumCDFG(numDataCDFG);
		Data->setValeur(ValData);
		Data->setNumAffect(numAffect);
		Data->setVariableWidth(variableWidth);
		Data->setIndexArray(indexTabData);

		if(ProgParameters::getDynProcess() != "interval"){
			Data->setDynProcess(dynamicProcess);
			Data->setProbDeb(probDeb);
		}
		else{
			Data->setDynProcess(INTERVAL);
			Data->setProbDeb(-1);
		}

		Node = (NoeudGraph *) (Data);

	}

	else if (getTypeNodeGraph == OPS_XML) {
		if (numOpsCDFG != -1) {
			Ops->setNumCDFG(numOpsCDFG); // Set num in CDFG
		}
		if (numOpsSFG != -1) {
			Ops->setNumSFG(numOpsSFG); // Set num in SFG
		}
		Node = (NoeudGraph *) (Ops);
	}
	return Node;
}

//------------------------------------------------ XmlGraph ------------------------------------------------
/**
 * 	Constructeur de la classe #XmlGraph
 */

XmlGraph::XmlGraph(Gfd * dest) {
	NoeudCourant = NULL;
	EdgeCourant = NULL;
	m_output = dest;
}

/**
 * 	Destructeur de la classe #XmlGraph
 */
XmlGraph::~XmlGraph() {
	for (list<XmlNode *>::iterator it = ListeNode.begin(); it != ListeNode.end(); ++it) {
		delete (*it);
	}
	for (list<XmlEdge *>::iterator it = ListeEdge.begin(); it != ListeEdge.end(); ++it) {
		delete (*it);
	}
}

/**
 * 	typeVarFonction d'ajout d'un #XmlNode dans la liste #ListeNode du #XmlGraph
 *	 L'insertion dans la liste est faite afin que les noeuds soient odronnee
 *	 (croissant) par rapport a leur numero
 *
 *	\param Nom : Nom du noeud (chaine de caracteres)
 *	\param Id : Numero du noeud (chaine de caracteres)
 */
void XmlGraph::addXmlNode(char *Nom, char *Id) {
	list<XmlNode *>::iterator itNode;

	NoeudCourant = new XmlNode(*this, Nom, Id);
	// cout << "Le nom : " << NoeudCourant->getNom()->data() << "; L'ID : " << NoeudCourant->getNum()  << endl;

	// Parcours de la liste pour Insertion dans l'ordre croissant
	for (itNode = ListeNode.begin(); itNode != ListeNode.end() && (*itNode)->getNum() < NoeudCourant->getNum(); ++itNode) {
	}
	ListeNode.insert(itNode, NoeudCourant);
}

/**
 * 	typeVarFonction d'ajout d'un #XmlEdge dans la liste #ListeEdge du #XmlGraph+
 *
 *	\param Depart : Numero du noeud de depart (chaine de caracteres)
 *	\param Arrivee : Numero du noeud d'arrivee (chaine de caracteres)
 */
void XmlGraph::addXmlEdge(char *Depart, char *Arrivee) {
	ListeEdge.push_back(new XmlEdge(*this, Depart, Arrivee));
	EdgeCourant = ListeEdge.back();
}

/**
 * 	Add an attribute to the current edge or node
 *
 *	\param Key the key to insert
 *	\param Value the value to assign to the key
 *	\param TypeDeNoeud NODE or EDGE (#TypeNodeXml) element type
 */
void XmlGraph::addAttributs(char *Key, char *Value, TypeNodeXml TypeDeNoeud) {
	if (TypeDeNoeud == EDGE) {
		EdgeCourant->addAttributs(Key, Value);
	}
	else {
		if (TypeDeNoeud == NODE) {
			NoeudCourant->addAttributs(Key, Value);
		}
	}
}

/**
 Add a block with number #id to the graph if it's not yet there.
 */
void XmlGraph::addBlock(int id) {
	if (m_output->getBlocks().find(id) == m_output->getBlocks().end()){
		m_output->getBlocks().insert(pair<int , Block> (id, Block(id)));
    }
}

Block & XmlGraph::getBlock(int id) {
    std::map<int, Block>::iterator myBlock = m_output->getBlocks().find(id);
	if (myBlock == m_output->getBlocks().end()){
		trace_error("Il manque une definition de block %d dans le XML\n", id);
		exit(-1);
	}
	return myBlock->second;
}

/**
 * TypeVarFonction de creation d'un #Gfd a partir du #XmlGraph
 *		Parcours de la liste #ListeNode et creation des noeuds #NoeudGraph
 *		Parcours de la liste #ListeEdge et creation des arcs #Edge
 */
void XmlGraph::xmlGraph2GFD(bool noiseEval) {
	list<XmlNode *>::iterator IndexListNode; // Lists des Noeud (xml)
	list<XmlEdge *>::iterator IndexListEdge; // Liste des ponts ou liens entre les noeuds (xml)
	NoeudGraph *NodeGraph;
	NoeudGraph *NodeDepart;
	NoeudGraph *NodeArrivee;
	int numInput;

	// Creation des Noeud Graph a l'aide des Noeud XML
	for (IndexListNode = ListeNode.begin(); IndexListNode != ListeNode.end(); ++IndexListNode) {
		//printf("\n Noeud %d: %s",IndexListNode->getNum() ,IndexListNode->getNom()->data());

		if (noiseEval) {
			NodeGraph = ((*IndexListNode)->createNode(false));
		}
		else {
			NodeGraph = ((*IndexListNode)->createNode(true));
		}

		if (NodeGraph->getTypeNodeGraph() == GFD) {
			if (((NoeudGFD *) NodeGraph)->getTypeNodeGFD() == DATA) {
				m_output->addNoeudData((NoeudData *) NodeGraph);
			}
			else {
				m_output->addNoeudGraph(NodeGraph);
			}
		}
		else {
			m_output->addNoeudGraph(NodeGraph);
		}
	}

	// Creation des Edge a l'aide des Edge XML
	for (IndexListEdge = ListeEdge.begin(); IndexListEdge != ListeEdge.end(); ++IndexListEdge) {
		NodeDepart = m_output->getElementGf((*IndexListEdge)->getDepart());
		NodeArrivee = m_output->getElementGf((*IndexListEdge)->getArrivee());
		numInput = -1;

		for (list<XmlAtt>::iterator it = (*IndexListEdge)->getListeAtt().begin(); it != (*IndexListEdge)->getListeAtt().end(); ++it) {
			if (!it->getKey().compare("NUM_INPUT")) { // Egde
				string valeur = it->getValue();
				numInput = atoi(valeur.c_str());
			}
		}

		if (((NoeudGFD *) NodeArrivee)->getTypeNodeGFD() == OPS) {
			if (((NoeudOps *) NodeArrivee)->getNbInput() <= numInput) {
				trace_error("Numero d'entrée de l'edge superieur au nombre d'operande\n");
				trace_error("((NoeudOps *) NodeArrivee)->getNbInput() : %i numInput : %i\n",((NoeudOps *) NodeArrivee)->getNbInput(), numInput);
                trace_error("Name op : %s\n", NodeArrivee->getName().c_str());
				exit(-1);
			}
			NodeDepart->edgeDirectedTo(NodeArrivee, numInput);
		}
		else {
			NodeDepart->edgeDirectedTo(NodeArrivee);

		}

	}
	// Liaisons des NoeudInit et NoeudFin avec le reste du graph GFD
	m_output->liaisonsSuccPredInitFin();
}

void Gfd::liaisonsSuccPredInitFin(void) {
	NodeGraphContainer::iterator it;
	it = tabNode->begin();
	++it;
	++it;
	
	for (; it != tabNode->end(); ++it) { // Parcours la liste des noeud a partie du 3eme (on ne passe pas part Init et Fin)
		if ((*it)->getNbPred() == 0) {
			this->getNoeudInit()->addEdge(*it);
		}
		if ((*it)->getNbSucc() == 0) {
			this->getNoeudFin()->addEdge(*it);
		}
	}
}

