// === Bibliothèques standard
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdio.h>

#include "graph/dfg/Gfd.hh"
#include "utils/Tool.hh"
#include "xml/XmlGraph.hh"
#include "xml/XmlGraph.hh"

// === Bibliothèques non standard
// g++ -o test test.o -L/usr/lib -lxml2 -lz -liconv -lm

#ifndef XML_LIB
#define XML_LIB

// === Bibliothèques non standard (XML)
#include <libxml/xmlreader.h>
#include <libxml/SAX2.h>
#include <libxml/xmlstring.h>

#endif

/* Machine d'etat pour le parser */

typedef enum {
	ETAT_DEBUT, ETAT_DOCUMENT_IN, ETAT_GRAPH_IN, ETAT_NODE_IN, ETAT_ATT_NODE_IN, ETAT_EDGE_IN, ETAT_ATT_EDGE_IN, ETAT_DOCUMENT_OUT, ETAT_GRAPH_OUT, ETAT_ERROR
} TypeEtatXml;

/* Variables globales utiles */

/**
 * \var 	EtatXml
 * \brief 	Etat courant de la machine d'etats pour parser le fichier XML
 * \ingroup InterfaceXML */
TypeEtatXml EtatXml;

/**
 * \var 	PtrXmlGraph
 * \brief 	Pointeur sur le graph genere lors de l'analyse du fichier XML
 * \ingroup InterfaceXML */
XmlGraph *PtrXmlGraph;

/**
 * \var 	myhandler
 * \brief 	Handler sur le fichier XML
 * \ingroup InterfaceXML */
xmlSAXHandler myhandler;

// Mise en place du handler et des callbacks

/**
 * TypeVarFonction activee lors de la detection du debut du fichier XML
 */
void userStartDocument(void * /*ctx */) {
	if (EtatXml == ETAT_DEBUT) {
		EtatXml = ETAT_DOCUMENT_IN;
	}
}

/**
 * TypeVarFonction activee lors de la detection du debut d'un element lors de l'analyse du fichier XML
 */
void userStartElement(void * /*ctx */, const xmlChar * fullname, const xmlChar ** atts) {

	switch (EtatXml) {
	//On entre dans le document
	case ETAT_DOCUMENT_IN:
		if (xmlStrEqual(fullname, BAD_CAST("graph")) == 1 || xmlStrEqual(fullname, BAD_CAST("Graph")) == 1 || xmlStrEqual(fullname, BAD_CAST("fr.irisa.cairn.float2fix:Graph")) == 1) {
			EtatXml = ETAT_GRAPH_IN;
		}
		else {
			EtatXml = ETAT_DOCUMENT_OUT; // Absence de graphe donc sortie du document
		}
		break;

	case ETAT_GRAPH_IN:
		if (xmlStrEqual(fullname, BAD_CAST("node")) == 1) {
			EtatXml = ETAT_NODE_IN;
			PtrXmlGraph->addXmlNode((char *) atts[1], (char *) atts[3]); // name, id
		}
		else {
			EtatXml = ETAT_EDGE_IN;
			PtrXmlGraph->addXmlEdge((char *) atts[1], (char *) atts[3]); // id_pred, id_succ
		}
		break;

	case ETAT_NODE_IN:
		EtatXml = ETAT_ATT_NODE_IN;
		PtrXmlGraph->addAttributs((char *) atts[1], (char *) atts[3], NODE); // key, value
		break;

	case ETAT_EDGE_IN:
		EtatXml = ETAT_ATT_EDGE_IN;
		PtrXmlGraph->addAttributs((char *) atts[1], (char *) atts[3], EDGE); // key, value
		break;

		// Par convention on passe a l'etat document out si il y a un probleme
		//On entre dans le graphe, le passage a l'etat 2 n est pas necessaire
		//Tous les cas non connus
	default:
		EtatXml = ETAT_ERROR;
		fprintf(stderr, "\n ERROR: start element: %s\n", fullname);
		assert(false);
	}
}

/**
 * TypeVarFonction activée lors de la détection de la fin d'un élément lors de l'analyse du fichier XML
 */
void userEndElement(void * /*ctx */, const xmlChar * fullname) {

	//printf("Element end\n");
	//printf("%s\n",fullname);

	switch (EtatXml) {

	//On sort du document vide
	case ETAT_DOCUMENT_IN:
		EtatXml = ETAT_ERROR;
		fprintf(stderr, "\n ERROR : No graph read: %s", fullname);
		assert(false);
		break;

	case ETAT_GRAPH_IN:
		EtatXml = ETAT_GRAPH_OUT;
		break;

	case ETAT_NODE_IN:
		EtatXml = ETAT_GRAPH_IN;
		break;

	case ETAT_ATT_NODE_IN:
		EtatXml = ETAT_NODE_IN;
		break;

	case ETAT_EDGE_IN:
		EtatXml = ETAT_GRAPH_IN;
		break;

	case ETAT_ATT_EDGE_IN:
		EtatXml = ETAT_EDGE_IN;
		break;

		//Tous les cas non connus
	default:
		EtatXml = ETAT_ERROR;
		fprintf(stderr, "\n ERROR: end element: %s\n", fullname);
		assert(false);
	}
}

/**
 * TypeVarFonction activée lors de la détection du début du fichier XML
 */
void userEndDocument(void * /*ctx */) {
	if (EtatXml == ETAT_GRAPH_OUT) {
		EtatXml = ETAT_DEBUT;
	}
}

/**
 * TypeVarFonction activée au cours du traitement d'un élément
 */
void userCharacters(void * /*ctx */, const xmlChar * /*ch */, int /*len */) {
}

void userIgnorableWhitespace(void * /*ctx */, const xmlChar * /*ch */, int /*len */) {
}

/**
 * 	Creation d'un Gfd à partir d'un fichier XML
 *	\param FichierXML : nom du fichier
 *	\param FichierDTD : Fichier permettant à la validation du xml
 */
void Gfd::lectureFluxXML(const char *FichierXML, const char *FichierDTD) {
	xmlDtdPtr dtd = NULL;

	// Global XmlGraph *PtrXmlGraph;
	// Global TypeEtatXml EtatXml;
	// Global xmlSAXHandler myhandler;
	myhandler.startDocument = userStartDocument;
	myhandler.startElement = userStartElement;
	myhandler.endDocument = userEndDocument;
	myhandler.endElement = userEndElement;
	myhandler.characters = userCharacters;
	myhandler.ignorableWhitespace = userIgnorableWhitespace;


	printf(TOOL_XML_PARSING);

	// Parsed de la dtd
	dtd = xmlParseDTD(NULL, (xmlChar *) FichierDTD);
	if (dtd == NULL) {
		fputs("DTD not parsed successfully\n", stderr);
	}
	else {
		trace_debug(STEP_XML_VALIDATED, FichierXML); //". XML file validation started"

		PtrXmlGraph = new XmlGraph(this);
		xmlSAXUserParseFile(&myhandler, NULL, FichierXML);

		trace_debug(STEP_XML_CONVERT); //". XML file conversion started"

		//Construction du graphe
		PtrXmlGraph->xmlGraph2GFD(this->noiseEval);

		trace_debug(STEP_XML_END); //". SFG has been created from the XML description"

		delete PtrXmlGraph;
		xmlFreeDtd(dtd);

	}
	xmlCleanupParser();
	this->addVarIntNodes();
	this->sortListEdgePred();
}

