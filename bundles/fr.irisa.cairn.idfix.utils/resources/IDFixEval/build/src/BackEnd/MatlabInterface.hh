/**
 * MatlabInterface.hh
 *
 *  Created on: 14 nov. 2008
 *      Author: cloatre
 *
 *  \class MatlabInterface
 *	\brief Classe definissant les methodes permettant d'utiliser un fichier .mat
 *
 * utilisation de la librairie:  libmatio-1.3.3
 *
 * instructions:
 *
 * telecharger la librairie
 * dezippez le dossier
 *
 * installez: ./configure; make; make install dans shell
 *
 * dans les proprietes du projet:
 *
 * Compiler-->directorie : ajoutez le repertoire des sources pour avoir "matio.h", exemple:  "/home/cloatre/Eclipse/matio-1.3.3/src"
 * Linker-->Libraries : matio (option -I) et usr/local/lib (option -L) pour avoir acces a "matio.a"
 *
 * Ensuite penser a autoriser l'utilisation de l'interface matlab:
 *
 * fichier "MatlabInterface.hh" ====>  ajouter un "#define LIB_MATIO_133_INSTALLEE"
 */

#ifndef _MATLABINTERFACE_HH_
#define _MATLABINTERFACE_HH_

#define LIB_MATIO_133_INSTALLEE

// === Bibliothèques standard
#include <vector> /* For STL */
#include <iostream>
#include <cstring>

#include "graph/dfg/NoeudGraph.hh"

// === Namespaces
using namespace std;

/*#include <math.h>
 #include <matio.h>
 #include <matio_private.h>*/

/*
 *	Classe definissant les methodes permettant d'utiliser un fichier .mat
 * \class MatlabInterface
 */
class MatlabInterface {
public:
	/// ======================================= Constructeurs - Destructeurs
	MatlabInterface() { /// Constructeurdefaut
#ifndef LIB_MATIO_133_INSTALLEE
		cerr<<"attention: flag #def LIB_MATIO_133_INSTALLEE n'est pas defini:";
		cerr<<"a definir si la librairie libmatio-1.3.3 est installee(cf instructions en haut de ce fichier:"<<__FILE__<<endl;
#endif
	}
	;
	MatlabInterface(MatlabInterface & obj) {
	}
	; /// Constructeur
	~MatlabInterface() {
	}
	; /// Destructeur

	// ======================================= Methodes
	int readInputMatlabVector(string *pathStimuliFile, float* nom_pt, string *nom); /// Lit un fichier matlab et stocke les donnees float a partir de l'@ nom_pt
	int writeInputMatlabVector(string *pathStimuliFile, string *nom, int taille); /// Écrit dans un fichier matlab un vecteur de taille N rempli de valeurs aleatoires entre 0 et 1
	int writeOutputMatlabVector(string *pathStimuliFile, string *nom, float* nom_pt, int taille); /// Erit dans un fichier matlab un vecteur de taille N rempli a partir d'un tableau de taille N
	int getSizeVectorMatlabFile(string *pathStimuliFile); /// Lit la taille des vecteurs d'un fichier mat

	int simuFichier(); /// Simule le fichier C genere, pour le debug on fait des copier-coller

};

#endif // _MATLABINTERFACE_HH_
