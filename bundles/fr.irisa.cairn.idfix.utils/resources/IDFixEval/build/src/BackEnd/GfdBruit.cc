/**
 *  \author Daniel Menard, Mohammad DIAB, Jean-Charles Naud
 *  \date   10.06.2002
 *  \brief Description de quelques méthodes utilisées pour générer le computePb.c de manière générique
 */
// === Bibliothèques standard
#include <string>
#include <iostream>
#include <math.h>
#include <sstream>

#include "graph/dfg/datanode/NoeudSrcBruit.hh"
#include "graph/dfg/Gfd.hh"
#include "graph/dfg/operatornode/NoeudOps.hh"
#include "graph/toolkit/TypeVar.hh"
#include "utils/Tool.hh"

#include "graph/Edge.hh"

// === Namespaces
using namespace std;

// === Variables global
//int NumeroSrcBruit = 0;
char chaine[10];

/**
 *  Definition du predicat pour pouvoir trier la liste des getElementSuccPred
 *  \return bool : false, si les elements sont inverses
 */
bool predicatOrdreElt(Edge * elt1, Edge * elt2) {
	return elt1->getNumInput() < elt2->getNumInput();
}

/**
 * Methode qui compte le nombre de source de bruit
 * \return int : nombre de sources de bruit
 */
int Gfd::calculNombreSrcBruit() {
	return this->getNbNoiseNode();
}

/**
 * Methodes comptant le nombre max de KQ associes aux sources de bruit
 * \return int : nombre de sources
 */
int Gfd::calculNombreKQAssocieSrcBruit() {

	int result = 0, nbCourant;
	vector<NoeudSrcBruit*>::iterator it;
	// Recherche de la taille maximal du vecteur (Pas de quantification) présent dans chaque noeudScrBruit
	for(int i = 0 ; i < this->getNbNoiseNode() ; i++){
		nbCourant = (int) this->getNoiseNode(i)-> getTypeVarRefVectQuantif().size();
		if (nbCourant > result) {
			result = nbCourant;
		}
	}
	return result;

}


/**
 * Methodes comptant le nombre de SFG OPS qui correspond au numéro SFG OPS maximum
 * \return int : nombre de SFG
 */
int Gfd::calculNombreSFG() {
	NoeudGFD * noeudGFD;
	NoeudOps * noeudOps;
	int result = 0, nbCourant;
	NodeGraphContainer::iterator it;

	for (it = this->tabNode->begin(); it != this->tabNode->end(); ++it) {
		if((*it)->getTypeNodeGraph() == GFD) {
			noeudGFD = (NoeudGFD *) *it;
			if(noeudGFD->getTypeNodeGFD() == OPS) {
				noeudOps = (NoeudOps *) noeudGFD;
				nbCourant = noeudOps->getNumSFG();
				//cout << "SFG = " << nbCourant << ";" << endl;
				if (nbCourant > result) {
					result = nbCourant;
				}
			}
		}
	}
	return result+1;
}

/**
 * Methodes comptant le nombre de CDFG OPS qui correspond au numéro CDFG OPS maximum
 * \return int : nombre de CDFG
 */
int Gfd::calculNombreCDFG() {
	NoeudGFD * noeudGFD;
	NoeudOps * noeudOps;
	int result = 0, nbCourant;
	NodeGraphContainer::iterator it;

	for (it = this->tabNode->begin(); it != this->tabNode->end(); ++it) {
		if((*it)->getTypeNodeGraph() == GFD) {
			noeudGFD = (NoeudGFD *) *it;
			if(noeudGFD->getTypeNodeGFD() == OPS) {
				noeudOps = (NoeudOps *) noeudGFD;
				nbCourant = noeudOps->getNumCDFG();
				//cout << "CDFG = " << nbCourant << ";" << endl;
				if (nbCourant > result) {
					result = nbCourant;
				}
			}
		}
	}
	return result+1;
}
