/**
 * \file GenerateConstantVariable.hh
 * \author cloatre
 * \date   19 janv. 2010
 */

// === Bibliothèques standard
#include <fstream>
#include <iostream>

#include "graph/dfg/datanode/NoeudData.hh"
#include "graph/dfg/Gfd.hh"
#include "utils/Tool.hh"

// === Bibliothèques non standard

// === Namespaces
using namespace std;

/**
 * Methode pour generer le fichier contenant les variables initialisées de l'application
 *
 *  by Loic Cloatre le 19/01/2010
 */
void Gfd::generateConstantVariable() {
	fstream File22Txt_pf;
	string tempPathFile; 
	NoeudGFD* nodeGFD;

	tempPathFile = ProgParameters::getOutputGenDirectory() + CONSTANT_VAR_FILENAME;

	if (this->nbNode < 3) {
		trace_error("il n'y a pas de noeuds dans le graphe: ");
		exit(-1);
	}

	File22Txt_pf.open((tempPathFile).c_str(), fstream::out); //ouverture fichier en mode ecriture
	assert(File22Txt_pf.is_open());

	File22Txt_pf << "% definition of constant " << endl;
	for(int i = 2; i < this->getNbNode() ; i++){
		nodeGFD = dynamic_cast<NoeudGFD*> (this->getElementGf(i));
		assert(nodeGFD != NULL);
		if(nodeGFD->getTypeNodeGFD() == DATA && ((NoeudData*) nodeGFD)->isNodeConst()){
				File22Txt_pf << ((NoeudData *) nodeGFD)-> getName() << " = ";
				File22Txt_pf << ((NoeudData *) nodeGFD)-> getValeur() << ";" << endl;
		}
	}
	File22Txt_pf << "% end of definition of constant " << endl;
	File22Txt_pf.close();

}
