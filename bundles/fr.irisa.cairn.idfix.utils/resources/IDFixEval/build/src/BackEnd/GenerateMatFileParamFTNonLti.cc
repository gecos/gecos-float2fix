/**
 *  \file GenerateMatFileParamFT.cc
 *  \author cloatre
 *  \date   23 févr. 2009
 */

// === Bibliothèques standard
#include <sstream>

#include "graph/NoeudGraph.hh"
#include "graph/tfg/EdgeGFT.hh"
#include "graph/tfg/GFT.hh"
#include "graph/tfg/NodeGFT.hh"
#include "utils/graph/cycledismantling/GraphPath.hh"
#include "utils/Tool.hh"

// === Bibliothèques non standard

// === Namespaces
using namespace std;

/**
 * Methodes principale pour generer le fichier matlab ParamFT.m
 *
 *  \param  pathFile: chemin du fichier matlab a creer
 *  \return bool: false if no node in graph
 *
 *  \ingroup T31
 *  by Loic Cloatre le 23/02/2009
 */
bool Gft::genereMatFileParamFTNonLti(string pathFile) {
	bool ret = true;
	fstream FileMat_pf;
	fstream FileTxt_pf;
	fstream FileMatclone_pf;
	NodeGraphContainer::iterator it;

	string ligne;

	// Personne ne set le numéro des edges, donc on va le faire ici
	int currentNum = 0;
	for (NodeGraphContainer::const_iterator it = tabNode->begin(); it != tabNode->end(); it++) {
		for (int i = (*it)->getNbSucc(); i > 0; i--) {
			if((*it)->getTypeNodeGraph() != INIT && (*it)->getElementSucc(i - 1)->getTypeNodeGraph() != FIN)
				(*it)->getEdgeSucc(i - 1)->setNumEdge(++currentNum);
		}
	}

	if (this->nbNode < 3) {
		trace_warn("il n'y a pas de noeuds dans le graphe");
		ret = false;
	}
	else {
		FileMat_pf.open(pathFile.c_str(), fstream::out); //ouverture fichier en mode ecriture
		if (!FileMat_pf.is_open()) {
			trace_error("Erreur lors de l'ouverture de ParamFTNonLti.m");
			exit(1);
		}

		FileMat_pf << NOMBRE_SOURCE_BRUIT << " = " << this->nbInput << " ;" << endl;
		FileMat_pf << NOMBRE_SORTIE << " = " << this->nbOutput << " ;" << endl;

		FileMat_pf << TAB_NUMERO_SORTIE << "= [ ";
		for (int i = 0; i < this->nbOutput; i++) {
			FileMat_pf << this->getOutputGf(i)->getNumero() - 1 << " "; // lclDecremente
		}
		FileMat_pf << "];" << endl << endl;
		FileMat_pf << "%-----  Definition  des fonctions de transfert partielles -----------" << endl;
		this->genereFTPartielles(FileMat_pf);

		FileMat_pf << "%-----  Definition  des fonctions de transfert globales -----------" << endl;
		this->genereFTGlobalesNonLti(FileMat_pf);


		FileMat_pf << "%**********fin **********" << endl << endl;
		FileMat_pf.close();
	}

	return ret;
}

/*
 *	Fonction determinant les fonction de transfert globales a partir des fonctions de tranfert partielles
 *
 *	Nicolas Simon
 *	20/07/11
 *
 */

void Gft::genereFTGlobalesNonLti(fstream & outputMatFile) {
	NoeudGFT *noeudEntree = NULL;
	NoeudGFT *noeudSortie = NULL;
	GraphPath *listeDesCircuit = NULL;
	bool tousCircuitsTraites = false;
	string FTGlob, FTGlobInit, tempString;
	bool ftHasElement;

	outputMatFile<<"zero.Blocks = [0 0; ];"<<endl<<"zero.Num(1).g = [ 0 ];"<<endl<<"zero.Den(1).f = [ 1 ];"<<endl<<"zero.tf = ft(zero);"<<endl<<endl;

	for (int j = 0; j < this->nbOutput; j++) { // pour chaque sortie
		noeudSortie = dynamic_cast<NoeudGFT *> (this->getOutputGf(j)); //on recupere la sortie coloriee
		
		ostringstream num2;
		//num2 << noeudSortie->getNumero() - 1; 
		num2 << j + 1; 
		

		for (int i = 2; i < this->nbNode; i++) { //pour chaque entree on calcule la fonction de transfert sauf init et fin
			noeudEntree = dynamic_cast<NoeudGFT *> (this->getElementGf(i)); //on recupere le noeud sur lequel on va calculer sa FT avec la sortie
			if (noeudEntree->isNodeSource() == true) { // Ce noeud est bien une entrée
				ostringstream num1;
				num1 << noeudEntree->getNumero() - 1; //lclDecremente  //recuperation du numero, Br1 commence au numero 2 donc on enleve 1

				if (noeudSortie != noeudEntree) { //pour eviter Hg(3,3) par ex
					FTGlob = "Hg(" + num2.str() + "," + num1.str() + ").tf = "; //Hg(6,1).tf =
					ftHasElement = false;

					listeDesCircuit = noeudSortie->enumerationCheminVersSource(noeudEntree);
					GraphPath *debut = listeDesCircuit;

					if (listeDesCircuit != NULL) {
						while (!tousCircuitsTraites) {
							if (listeDesCircuit->pathSize() > 1) {
								tempString = listeDesCircuit-> getExpressionFTGlobalePourMatlab(noeudSortie-> getNumero() - 1);
								//on calcule l'expression d'un chemin
								if ((tempString.size() != 0) && ftHasElement) {
									FTGlob = FTGlob + " + " + tempString; //s'il y a d'autre chemin, on somme les FT
								}
								else {
									FTGlob = FTGlob + tempString; //premier element
								}
								ftHasElement = true;
							}
							else {
								tempString = "";
							}

							if (listeDesCircuit->Suiv != NULL) {
								listeDesCircuit = listeDesCircuit->Suiv;
							}
							else {
								tousCircuitsTraites = true;
								if (ftHasElement == false) {
									FTGlob += "zero.tf;"; //cas de noeud isole
								}
								else {
									FTGlob += ";";
								}
							}
						}
						tousCircuitsTraites = false; //raz flag
					}
					else { //pas de cycle
						FTGlob += "zero.tf;"; //cas ou on lie une sortie a une entree n'appartenant pas a son graphe
					}

					listeDesCircuit = NULL;

					delete debut;
					outputMatFile << FTGlob << endl; //on ecrit le resultat dans le fichier
				}
			}
		}
	}
}

