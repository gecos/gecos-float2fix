/**
 *  \file GenerateMatFileParamFT.cc
 *  \author cloatre
 *  \date   23 févr. 2009
 */
// === Bibliothèques standard
#include <algorithm>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>

#include "graph/NoeudGraph.hh"
#include "graph/tfg/EdgeGFT.hh"
#include "graph/tfg/GFT.hh"
#include "graph/tfg/NodeGFT.hh"
#include "utils/graph/cycledismantling/GraphPath.hh"
#include "utils/Tool.hh"


// === Bibliothèques non standard

// === Namespaces
using namespace std;

/**
 * Methodes principale pour generer le fichier matlab ParamFT.m
 *
 *  \param  pathFile: chemin du fichier matlab a creer
 *  \return bool: false if no node in graph
 *
 *  \ingroup T31
 *  by Loic Cloatre le 23/02/2009
 */
bool Gft::genereMatFileParamFT(string pathFile) {
	bool ret = true;
	fstream FileMat_pf;
	fstream FileTxt_pf;
	fstream FileMatclone_pf;
	NodeGraphContainer::iterator it;

	string tempPathFileConstant;
	string tempPathFile;
	string ligne;

	tempPathFileConstant = ProgParameters::getOutputGenDirectory() + CONSTANT_VAR_FILENAME;
	tempPathFile = ProgParameters::getOutputGenDirectory() + PARAMFT_FILENAME;

	// Personne ne set le numéro des edges, donc on va le faire ici
	int currentNum = 0;
	for (NodeGraphContainer::const_iterator it = tabNode->begin(); it != tabNode->end(); it++) {
		for (int i = (*it)->getNbSucc(); i > 0; i--) {
			if((*it)->getTypeNodeGraph() != INIT && (*it)->getElementSucc(i - 1)->getTypeNodeGraph() != FIN)
				(*it)->getEdgeSucc(i - 1)->setNumEdge(++currentNum);
		}
	}

	if (this->nbNode < 3) {
		trace_warn("il n'y a pas de noeuds dans le graphe");
		ret = false;
	}
	else {
		FileMat_pf.open(tempPathFile.c_str(), fstream::out); //ouverture fichier en mode ecriture
		if (!FileMat_pf.is_open()) {
			trace_error("Erreur lors de l'ouverture de ParamFT.m");
			exit(1);
		}

		//recopie de la valeur des constantes de l'application

		assert(!tempPathFileConstant.empty());
		FileTxt_pf.open(tempPathFileConstant.c_str(), fstream::in); //ouverture fichier en mode ecriture

		if (!FileTxt_pf.is_open()) {
			trace_error("Erreur lors de l'ouverture du fichier ConstantVariable");
			exit(1);
		}

		while (getline(FileTxt_pf, ligne)) {
			FileMat_pf << ligne << endl;
		}

		FileTxt_pf.close();

		FileMat_pf << NOMBRE_SOURCE << " = " << this-> nbInput << " ;" << endl;
		FileMat_pf << NOMBRE_SORTIE << " = " << this-> nbOutput << " ;" << endl;

		FileMat_pf << TAB_NUMERO_SORTIE << "= [ ";
		for (int i = 0; i < this->nbOutput; i++) {
			FileMat_pf << this->getOutputGf(i)->getNumero() - 1 << " "; //lclDecremente
		}
		FileMat_pf << "];" << endl << endl;
		FileMat_pf << "%-----  Definition  des fonctions de transfert partielles -----------" << endl;


		this->genereFTPartielles(FileMat_pf);

		FileMat_pf << "%-----  Definition  des fonctions de transfert globales -----------" << endl<<endl;

		this->genereFTGlobales(FileMat_pf);

		FileMat_pf << "%**********fin **********" << endl << endl;
		FileMat_pf.close();
	}

	return ret;
}

/**
 * Methode pour generer les fonctions de transferts partielles entre chaque noeud et ses succeseurs
 *
 *  \param  outputMatFile: fichier ou ecrire
 *
 *  \ingroup T311
 *  by Loic Cloatre le 23/02/2009
 */
void Gft::genereFTPartielles(fstream & outputMatFile) {
	NodeGraphContainer::iterator it = this->tabNode->begin(); //on pointe sur le init
	it++; //on pointe sur le fin
	it++; //on pointe sur le premier noeud

	while (it != this->tabNode->end()) { //tant que tous les noeuds n'ont pas ete visites
		if (((NoeudGFT *) (*it))->isNodeOutput()) { // Ce noeud est une sortie, inutile donc de chercher sa FT.
			it++; //on incremente pour tester le noeud suivant
		}
		else { // Ce noeud n'est pas une sortie donc on cherche sa FT
			for (int i = 0; i < (*it)->getNbSucc(); i++) { // On parcours les listSucc du noeud
				NoeudGFT *noeudTraite = dynamic_cast<NoeudGFT *> (*it);

				if ((*it)->getElementSucc(i)->getNumero() > 1) { //pour eviter fin
					outputMatFile << dynamic_cast<EdgeGFT*> (noeudTraite->getEdgeSucc(i))->getMatlabExpression();
				}
			}
			it++; //on incremente pour tester le noeud suivant
		}
	}
}

/**
 * Methodes pour generer les fonctions de transferts globales entre chaque noeud et la sortie
 *
 *	pour un graphe avec 6 noeuds et le noeud 6 est la sortie,
 *	on obtient Hg(6,1).tf = ...
 *	           Hg(6,2).tf = ...
 *	...        Hg(6,5).tf = ...
 *
 *	pour cela on va reutiliser la fonction d'enumeration de circuit:
 *	on cree un lien de la sortie vers le noeud cible
 *	on liste les différents chemins(on considère que le graphe d'origine est acyclique)
 *	ex: circuits: [1 3 6] [1 2 3 6] donnera Hg(6,1).tf= H(6,3).tf * H(3,1).tf   + H(6,3).tf * H(3,2).tf* H(2,1).tf;
 *
 *  \param  outputMatFile: fichier ou ecrire
 *
 *  \ingroup T312
 */
void Gft::genereFTGlobales(fstream & outputMatFile) {
	NoeudGFT *noeudEntree = NULL;
	NoeudGFT *noeudSortie = NULL;
	GraphPath *listeDesCircuit = NULL;
	bool tousCircuitsTraites = false;
	string FTGlob, FTGlobInit, tempString;
	bool ftHasElement;

	for (int j = 0; j < this->nbOutput; j++) { // pour chaque sortie
		noeudSortie = dynamic_cast<NoeudGFT *> (this->getOutputGf(j)); //on recupere la sortie coloriee

		ostringstream num2;
		num2 << noeudSortie->getNumero() - 1; // recuperation du numero, Br1 commence au numero 2 donc on enleve 1
		// -->le numero de sortie est normalement toujours 1 avec la renumerotation

		for (int i = 2; i < this->nbNode; i++) { //pour chaque entree on calcule la fonction de transfert sauf init et fin
			noeudEntree = dynamic_cast<NoeudGFT *> (this->getElementGf(i)); //on recupere le noeud sur lequel on va calculer sa FT avec la sortie
			if (noeudEntree->isNodeSource() == true) { // Ce noeud est bien une entrée
				ostringstream num1;
				num1 << noeudEntree->getNumero() - 1; //lclDecremente  //recuperation du numero, Br1 commence au numero 2 donc on enleve 1

				if (num1.str() != num2.str()) { //pour eviter Hg(3,3) par ex
					FTGlob = "Hg(" + num2.str() + "," + num1.str() + ").tf = "; //Hg(6,1).tf =
					ftHasElement = false;

					listeDesCircuit = noeudSortie->enumerationCheminVersSource(noeudEntree);
					GraphPath *debut = listeDesCircuit;

					if (listeDesCircuit != NULL) {
						while (!tousCircuitsTraites) {
							if (listeDesCircuit->pathSize() > 1) {
								tempString = listeDesCircuit-> getExpressionFTGlobalePourMatlab(noeudSortie-> getNumero() - 1);
								//on calcule l'expression d'un chemin
								if ((tempString.size() != 0) && ftHasElement) {
									FTGlob = FTGlob + " + " + tempString; //s'il y a d'autre chemin, on somme les FT
								}
								else {
									FTGlob = FTGlob + tempString; //premier element
								}
								ftHasElement = true;
							}
							else {
								tempString = "";
							}

							if (listeDesCircuit->Suiv != NULL) {
								listeDesCircuit = listeDesCircuit->Suiv;
							}
							else {
								tousCircuitsTraites = true;
								if (ftHasElement == false) {
									FTGlob += "0;"; //cas de noeud isole
								}
								else {
									FTGlob += ";";
								}
							}
						}
						tousCircuitsTraites = false; //raz flag
					}
					else { //pas de cycle
						FTGlob += "0;"; //cas ou on lie une sortie a une entree n'appartenant pas a son graphe
					}

					listeDesCircuit = NULL;

					delete debut;
					outputMatFile << FTGlob << endl; //on ecrit le resultat dans le fichier
				}
			}
		}
	}
}

/**
 * Methode qui à partir d'un circuit donne l'expression de la FT: [2 3 4] donne Hg(3,2)*Hg(4,3)
 *
 *  \param  int numero de la sortie pour eviter de traiter le rebouclage introduit
 *
 *  \ingroup T311
 *  by Loic Cloatre le 23/02/2009
 */
string GraphPath::getExpressionFTGlobalePourMatlab(int numeroSortie) {
	std::ostringstream expression;
	list<EdgeGFT*>::const_iterator it;

	it = m_Edges.begin();
	expression << "H(" << (*it)->getNumEdge() << ").tf";
	it++;
	for (; it != m_Edges.end(); it++) {
		expression << " * H(" << (*it)->getNumEdge() << ").tf";
	}

	// Si le chemin est en fait un circuit, on a traité les liens i>i+1 mais il faut encore faire i+1>0
	// pour faire le tour complet du circuit.
	/*
	 ostringstream depart, arrivee;
	 depart << this->Path->contenu[this->Path->size - 1] - 1;     //pour aller du dernier noeud du circuit
	 arrivee << this->Path->contenu[0] - 1;       //au premier noeud du circuit
	 if (this->Path->contenu[this->Path->size - 1] - 1 != numeroSortie)   //on ne copie pas le rebouclage artificiel
	 {
	 if (ret.size() != 0){
	 ret = ret + "*H(" + arrivee.str() + "," + depart.str() + ").tf";       //cas ou il y a deja une expression
	 }
	 else {
	 ret = "H(" + arrivee.str() + "," + depart.str() + ").tf";      //cas ou c'est la premiere expression
	 }
	 }
	 */
	return expression.str();
}
