#include <iostream>
#include <fstream>
#include <sstream>

#include "graph/dfg/datanode/NoeudData.hh"
#include "graph/dfg/datanode/NoeudSrcBruit.hh"
#include "graph/dfg/NoeudGFD.hh"
#include "graph/NoeudGraph.hh"
#include "graph/dfg/operatornode/NoeudOps.hh"
#include "graph/toolkit/TypePrint.hh"
#include "backend/NoisePowerExpressionGeneration.hh"



#define TAB "\t"
#define BUFFER_USING 0

// Function used to precompute the WFPSFG/WFPSFGEff
int WFPSFGToWFPSFGEff_NoiseSourceSort(NoeudGraph* node, list<NoeudGraph *> * listTriBr);
void WFPSFGToWFPSFGEff_InputCase(Gfd* gfd, NoeudSrcBruit* noiseNode);
void WFPSFGToWFPSFGEff_GeneralCase1(Gfd* gfd, NoeudSrcBruit* noiseNode);
void WFPSFGToWFPSFGEff_GeneralCase2(Gfd* gfd, NoeudSrcBruit* noiseNode);
void WFPSFGToWFPSFGEff_GeneralCase3(Gfd* gfd, NoeudSrcBruit* noiseNode);
void WFPSFGToWFPSFGEff_OutputCase(Gfd* gfd, NoeudSrcBruit* noiseNode);
void WFPEffOperatorDetermination(Gfd* gfd, NoeudOps* opsNode);

// Function used to generate the core of the noise function
void ComputePb_ecritureEntete(Gft * gft, Gfd * grapheDepart, fstream & fileCpp_pf, fstream & fileHpp_pf);
void ComputePb_declarationVariable(fstream & fileCpp_pf);
void ComputePb_ecritureGainBrVersSortie(fstream &fileCpp_pf);
void ComputePb_initialisationTableaux(fstream &fileCpp_pf);
void ComputePb_ecritureMeanVarBr(Gfd * grapheDepart, fstream & fileCpp_pf, fstream & fileHpp_pf);
void ComputePb_ecritureExpressionPby(fstream & fileCpp_pf);
void ComputePb_WFP_cdfg_to_WFP_sfg(Gfd *gfd, fstream & fileCpp_pf, fstream & fileHpp_pf);
void ComputePb_ecritureWFP_sfg_eff(Gfd *gfd, fstream & fileCpp_pf, fstream & fileHpp_pf);

#if BUFFER_USING
void ComputePb_writeReadingCompressedGainFile_buffer(fstream & fileCpp_pf);
void ComputePb_writeReadingGainFile_buffer(fstream & fileCpp_pf);
#else
void ComputePb_writeReadingCompressedGainFile(fstream & fileCpp_pf);
void ComputePb_writeReadingGainFile(fstream & fileCpp_pf);
#endif

/**
 * Main function to generate ComputePb file
 *
 *  \param  Gfl* gflRegroupe: GFL composed of one or more GFL
 *  \author Jean-Charles Naud
 */
void Gft::generateNoiseFunction(Gfd * grapheDepart) {
	fstream fileCpp_pf; // .c principale
	fstream fileCpp_func;
	fstream fileHpp_pf;
	fstream fileTool_pf;
	NodeGraphContainer::iterator it;
	string tempPathFile = ProgParameters::getOutputGenDirectory() + ProgParameters::getOutputFilename(); // Chemin du fichier ComputePb.cc
	string tempPathFileH = ProgParameters::getOutputGenDirectory() + "ComputePb.h";

	assert((ProgParameters::getOutputFormat() == "C") || (ProgParameters::getOutputFormat() == "CJni"));
	assert(this->nbNode > 2); // Y a-t-il des Noeud dans le grahpe

	fileCpp_pf.open(tempPathFile.c_str(), fstream::out); // Ouverture du fichier en mode écriture
	assert(fileCpp_pf.is_open());

	fileHpp_pf.open(tempPathFileH.c_str(), fstream::out);
	assert(fileHpp_pf.is_open());

	trace_debug("Ouverture du fichier %s en écriture\n", tempPathFile.data());

	// === Écriture de l'entête du fichier .cc"
	ComputePb_ecritureEntete(this, grapheDepart, fileCpp_pf, fileHpp_pf);

	if (ProgParameters::getOutputFormat() == "C") {
		//fileCpp_pf << endl << "double ComputePb(int WFP[][3], int WFPquantMode[][3], int tabInputWd[], int tabInputWdquantMode[]){" << endl << endl;
		fileCpp_pf << endl << RESULT_TYPE<< " ComputePb(int * " << __NOISEPOWER_WFP__ << ", int *" << __NOISEPOWER_WFPQuantif__ << ", int *" << __NOISEPOWER_InputWD__ << ", int *" << __NOISEPOWER_InputWDQuantif__ << ", int outputMode){" << endl;
	}
	if (ProgParameters::getOutputFormat() == "CJni") {
		fileCpp_pf << "/**" << endl;
		fileCpp_pf << " * Class:     fr_irisa_cairn_float2fix_noiseAndCost_NEvalLibNatives" << endl;
		fileCpp_pf << " * Method:    computePbCC" << endl;
		fileCpp_pf << " * Signature: ([LSolutionSpaceModel/Triplet;[Ljava/lang/String;)F" << endl;
		fileCpp_pf << " */" << endl;
		fileCpp_pf << "JNIEXPORT j"<< RESULT_TYPE << " JNICALL Java_fr_irisa_cairn_idfix_optimization_utils_NEvalLibNatives_computePbCC" << endl;
		//fileCpp_pf << "(JNIEnv * env, jclass class, jobjectArray arrayObject, jobjectArray tabInputWd){" << endl;
		fileCpp_pf << "(JNIEnv * env, jclass class, jintArray jWFP, jintArray jWFPquantMode, jintArray jtabInputWd, jintArray jtabInputWdquantMode){" << endl;
		fileCpp_pf << "int outputMode = 0;" << endl;
	}
	// === Coeur du traitement
	ComputePb_declarationVariable(fileCpp_pf);


	ComputePb_initialisationTableaux(fileCpp_pf);

	ComputePb_WFP_cdfg_to_WFP_sfg(grapheDepart, fileCpp_pf, fileHpp_pf);

	fileCpp_pf << endl << TAB << "// === Ecriture des WFP_sfg_eff" << endl;
	ComputePb_ecritureWFP_sfg_eff(grapheDepart, fileCpp_pf, fileHpp_pf);

	fileCpp_pf << endl << TAB << "// === Ecriture des K,Q et Mean et Var de chaque Bruit \"br\" " << endl;
	ComputePb_ecritureMeanVarBr(grapheDepart, fileCpp_pf, fileHpp_pf);

	fileCpp_pf << endl << TAB << "// === Ecriture de l'expression du bruit Pby en sortie" << endl;
	ComputePb_ecritureExpressionPby(fileCpp_pf);

	fileCpp_pf << endl << "}" << endl;

	fileCpp_pf.close();
	fileHpp_pf.close();
}

/**
 * Écriture de l'entête du fichier .cc
 * \author Jean-Charles Naud
 */
void ComputePb_ecritureEntete(Gft * gft, Gfd * grapheDepart, fstream & fileCpp_pf, fstream & fileHpp_pf) {
	// === Écriture de l'entête du fichier .cc
	fileHpp_pf << "#include <stdio.h>" << endl;
	fileHpp_pf << "#include <math.h>" << endl;
	fileHpp_pf << "#include <float.h>" << endl;
	fileHpp_pf << "#include <assert.h>" << endl;
	fileHpp_pf << "#include <stdlib.h>" << endl;

	if (ProgParameters::getOutputFormat() == "CJni") {
		fileHpp_pf << "#include <jni.h>" << endl << endl;
		fileHpp_pf << "#include \"natives.h\"" << endl;
	}
	else {
		fileHpp_pf << endl;
	}

	fileCpp_pf << "/**\n * \\file CalculRSQB.cc" << endl;
	fileCpp_pf << " * \\author Generated by ID.Fix" << endl;
	fileCpp_pf << " */" << endl;
	fileCpp_pf << "#include \"ComputePb.h\"" << endl;

	fileCpp_pf << "// Number of noise sources" << endl;
	fileCpp_pf << "#define NB_SRCE_NOISE " << grapheDepart->calculNombreSrcBruit() << endl;
	fileCpp_pf << "// Number of output" << endl;
	fileCpp_pf << "#define NB_OUTPUT " << gft->getNbOutput() << endl;
	fileCpp_pf << "// Number of quantification into the system" << endl;
	fileCpp_pf << "#define NB_K_Q_ASSOCIATE " << grapheDepart->calculNombreKQAssocieSrcBruit() << endl;
	fileCpp_pf << "// Number of different CDFG number" << endl;
	fileCpp_pf << "#define NB_CDFG " << grapheDepart->calculNombreCDFG() << endl;
	fileCpp_pf << "// Number of different SFG number" << endl;
	fileCpp_pf << "#define NB_SFG " << grapheDepart->calculNombreSFG() << endl << endl;
}

/**
 * Declaration des variables locales
 * \author Jean-Charles Naud
 */
void ComputePb_declarationVariable(fstream & fileCpp_pf) {
	/* Comment prendre en compte le type de bruit ? Est-ce à l'utilisateur de le spécifier ? */
	/* Pour l'instant le changer ici */
	/* Les variables suivantes sont déclarées en static, car sinon elles font péter la taille de la pile dans l'exécution native java... */

	fileCpp_pf << TAB << "static " << MEAN_VAR_TYPE << " " << __NOISEPOWER_XBMean__ << "[NB_SRCE_NOISE]; // Mean of each noise source" << endl;
	fileCpp_pf << TAB << "static " << MEAN_VAR_TYPE << " " << __NOISEPOWER_XBVar__ << "[NB_SRCE_NOISE]; // Variance of each noise source" << endl << endl;

	fileCpp_pf << TAB << "static int K[NB_K_Q_ASSOCIATE]; // Number of eliminated bit" << endl;
	fileCpp_pf << TAB << "static " << MEAN_VAR_TYPE << " Q[NB_K_Q_ASSOCIATE]; // Quantification step" << endl<<endl;

	fileCpp_pf << TAB << RESULT_TYPE << " ret_Pby = 0, variance, mean, maximumdb = " << RESULT_MIN << ";" << endl;
	fileCpp_pf << TAB << "int *" << __NOISEPOWER_WFPSFG__ << " = (int*) malloc(NB_SFG*3*sizeof(int)); // Number of bit triplet for each operator (using the SFG number)" << endl;
	fileCpp_pf << TAB << "int *" << __NOISEPOWER_WFPSFGEff__ << " = (int*) malloc(NB_SFG*3*sizeof(int)); // Number of effective bit triplet for each operator (using the SFG number)" << endl;
	fileCpp_pf << TAB << "int *" << __NOISEPOWER_WFPSFGQuantif__ << " = (int*) malloc(NB_SFG*3*sizeof(int)); // Kind of quantification used for each operand of each operator (using SFG number)" << endl << endl;
	fileCpp_pf << TAB << "int i, j, itOut;" << endl;
#if DEBUG
	fileCpp_pf << TAB << "FILE* pfileDebug;" << endl;
#endif
#if TRACE
	fileCpp_pf << TAB << "FILE *pfileTrace;" << endl;
#endif

	if(RuntimeParameters::isLTIGraph() && !RuntimeParameters::isRecursiveGraph() && RuntimeParameters::isBinaryWriteWithoutZero()){
		fileCpp_pf << TAB << "unsigned short int nbBrDependant;"<< endl;
		fileCpp_pf << TAB << "unsigned short int* brDependant = NULL;"<<endl;
		fileCpp_pf << TAB << "int temp;"<<endl;
	}
	fileCpp_pf << TAB << "long fileVarSize, fileMeanSize;"<<endl;
	fileCpp_pf << TAB << "size_t result;"<<endl;
	fileCpp_pf << TAB << "char* pathFileGainBin = \""<<ProgParameters::getOutputGenDirectory()<<NOISE_KLM_FILEMANE<<"\";"<<endl;
	fileCpp_pf << TAB << "FILE *fileVarGain, *fileMeanGain;" << endl;
	fileCpp_pf << TAB << GAIN_TYPE << " hSumCarre;"<<endl;
#if BUFFER_USING
	fileCpp_pf << TAB << "int nbElemRead, totalElemRead;" << endl;
	fileCpp_pf << TAB << "int sizeBuffer = 1024*1024;" << endl;
	fileCpp_pf << TAB << "int k;" << endl;
	fileCpp_pf << TAB << GAIN_TYPE << " *buffer;" << endl;
	fileCpp_pf << TAB << "buffer = (" << GAIN_TYPE<<"*) malloc(sizeof(" << GAIN_TYPE << ")*sizeBuffer);" << endl;
#else
	fileCpp_pf << TAB << GAIN_TYPE << " hSumCroise;"<<endl;
#endif
	fileCpp_pf << TAB << "fileVarGain = fopen(pathFileGainBin, \"rb\");"<<endl;
	fileCpp_pf << TAB << "fileMeanGain = fopen(pathFileGainBin, \"rb\");"<<endl;
	fileCpp_pf << TAB << "if(fileVarGain == NULL){"<<endl;
	fileCpp_pf << TAB << TAB << "printf(\"Erreur d'ouverture du fichier binaire des gains\");" << endl;
	fileCpp_pf << TAB << TAB << "exit(0);" << endl;
	fileCpp_pf << TAB << "}" << endl;
	fileCpp_pf << TAB << "if(fileMeanGain == NULL){"<<endl;
	fileCpp_pf << TAB << TAB << "printf(\"Erreur d'ouverture du fichier binaire des gains\");" << endl;
	fileCpp_pf << TAB << TAB << "exit(0);" << endl;
	fileCpp_pf << TAB << "}" << endl;

#if DEBUG
		fileCpp_pf << TAB << "pfileDebug = fopen(\"noisefunction_debug.log\", \"w\");"<<endl;
		fileCpp_pf << TAB << "if(pfileDebug == NULL){"<<endl;
		fileCpp_pf << TAB << TAB << "printf(\"Impossible to open the log debug file \");" << endl;
		fileCpp_pf << TAB << TAB << "exit(0);" << endl;
		fileCpp_pf << TAB << "}" << endl;
#endif
#if TRACE
		fileCpp_pf << TAB << "pfileTrace = fopen(\"noisefunction_trace.log\", \"w\");"<<endl;
		fileCpp_pf << TAB << "if(pfileTrace == NULL){"<<endl;
		fileCpp_pf << TAB << TAB << "printf(\"Impossible to open the log debug file \");" << endl;
		fileCpp_pf << TAB << TAB << "exit(0);" << endl;
		fileCpp_pf << TAB << "}" << endl;
#endif
}

/**
 * Initialisation des Tableaux
 * \author Jean-Charles Naud
 */
void ComputePb_initialisationTableaux(fstream & fileCpp_pf) {
	fileCpp_pf << TAB << "for (j = 0; j < NB_SRCE_NOISE; j++) {" << endl;
	fileCpp_pf << TAB << TAB << __NOISEPOWER_XBMean__ << "[j] = 0;" << endl;
	fileCpp_pf << TAB << TAB << __NOISEPOWER_XBVar__ << "[j] = 0;" << endl;
	fileCpp_pf << TAB << "}" << endl;
	if (ProgParameters::getOutputFormat() == "CJni") {
		fileCpp_pf << TAB << "// Note : en CJni, le tableau WFP est linéarisé, i.e. WFP[i][j] est transformé en WFP[i*3 + j] (WFP a toujours une deuxième dimension de 3)" << endl;
		fileCpp_pf << TAB << "// Pour retrouver le numéro de l'opérateur à partir de l'indice unique, il faut donc diviser l'indice par 3" << endl;
		fileCpp_pf << TAB << "// et pour savoir le numéro de l'entrée/sortie, il faut prendre le modulo 3" << endl;
		fileCpp_pf << TAB << "jint * " << __NOISEPOWER_WFP__ << " = (*env)->GetIntArrayElements(env, jWFP, NULL);" << endl;
		fileCpp_pf << TAB << "jint * " << __NOISEPOWER_WFPQuantif__ << " = (*env)->GetIntArrayElements(env, jWFPquantMode, NULL);" <<endl;
		fileCpp_pf << TAB << "jint * " << __NOISEPOWER_InputWD__ << " = (*env)->GetIntArrayElements(env,jtabInputWd,NULL);" << endl;
		fileCpp_pf << TAB << "jint * " << __NOISEPOWER_InputWDQuantif__ << " = (*env)->GetIntArrayElements(env, jtabInputWdquantMode, NULL);" <<endl;
	}

	fileCpp_pf << TAB << "// Init WFP_sfg[NB_SFG]" << endl;
}

void ComputePb_WFP_cdfg_to_WFP_sfg(Gfd *gfd, fstream & fileCpp_pf, fstream & fileHpp_pf) {
	list<string>::iterator itStr;
	fstream fileTempCpp;
	int numeroIndexFileC = 0;
	int counter_Nbr_WFP = 0;
	ostringstream oss;
	string pathTempCpp;

	oss << numeroIndexFileC;
	pathTempCpp = ProgParameters::getOutputGenComputePbDirectory() + "computePb_WFP_CDFG_to_WFP" + oss.str() + ".c";
	fileTempCpp.open(pathTempCpp.c_str(), fstream::out);
	assert(fileTempCpp.is_open());

	fileTempCpp << "#include \"Tool.h\"" << endl;

#if TRACE
	fileTempCpp << "void computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(int *" << __NOISEPOWER_WFPSFG__ << ", int *" << __NOISEPOWER_WFPSFGQuantif__ << ", int *" << __NOISEPOWER_WFP__ << ", int *" << __NOISEPOWER_WFPQuantif__ << ", FILE *pfileTrace) {" << endl;
	fileCpp_pf << TAB << "computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(" << __NOISEPOWER_WFPSFG__ << ", " << __NOISEPOWER_WFPSFGQuantif__ << ", " << __NOISEPOWER_WFP__ << ", " << __NOISEPOWER_WFPQuantif__ << ", pfileTrace);" << endl;
	fileHpp_pf << "void computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(int *" << __NOISEPOWER_WFPSFG__ << ", int *" << __NOISEPOWER_WFPSFGQuantif__ << ", int *" << __NOISEPOWER_WFP__ << ", int *" << __NOISEPOWER_WFPQuantif__ << ", FILE *pfileTrace);" << endl;
#else
	fileTempCpp << "void computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(int *" << __NOISEPOWER_WFPSFG__ << ", int *" << __NOISEPOWER_WFPSFGQuantif__ << ", int *" << __NOISEPOWER_WFP__ << ", int *" << __NOISEPOWER_WFPQuantif__ << ") {" << endl;
	fileCpp_pf << TAB << "computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(" << __NOISEPOWER_WFPSFG__ << ", " << __NOISEPOWER_WFPSFGQuantif__ << ", " << __NOISEPOWER_WFP__ << ", " << __NOISEPOWER_WFPQuantif__ << ");" << endl;
	fileHpp_pf << "void computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(int *" << __NOISEPOWER_WFPSFG__ << ", int *" << __NOISEPOWER_WFPSFGQuantif__ << ", int *" << __NOISEPOWER_WFP__ << ", int *" << __NOISEPOWER_WFPQuantif__ << ");" << endl;
#endif
	for (itStr = gfd->getBeginWFPSFG() ; itStr != gfd->getEndWFPSFG() ; ++itStr, counter_Nbr_WFP++) {
		// 23500 = 1 MO
		// 2350 = 100 ko
		// 1175 = 50 ko
		if(counter_Nbr_WFP > 2350){
			ostringstream oss1;
			counter_Nbr_WFP = 0;
			numeroIndexFileC++;

			fileTempCpp << "}" << endl;
			fileTempCpp.close();
			assert(!fileTempCpp.is_open());

			oss1 << numeroIndexFileC;
			pathTempCpp = ProgParameters::getOutputGenComputePbDirectory() + "computePb_WFP_CDFG_to_WFP" + oss1.str() + ".c";
			fileTempCpp.open(pathTempCpp.c_str(), fstream::out);
			assert(fileTempCpp.is_open());
			fileTempCpp << "#include \"Tool.h\"" << endl;

#if TRACE
			fileTempCpp << "void computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(int *" << __NOISEPOWER_WFPSFG__ << ", int *" << __NOISEPOWER_WFPSFGQuantif__ << ", int *" << __NOISEPOWER_WFP__ << ", int *" << __NOISEPOWER_WFPQuantif__ << ", FILE *pfileTrace) {" << endl;
			fileCpp_pf << TAB << "computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(" << __NOISEPOWER_WFPSFG__ << ", " << __NOISEPOWER_WFPSFGQuantif__ << ", " << __NOISEPOWER_WFP__ << ", " << __NOISEPOWER_WFPQuantif__ << ", pfileTrace);" << endl;
			fileHpp_pf << "void computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(int *" << __NOISEPOWER_WFPSFG__ << ", int *" << __NOISEPOWER_WFPSFGQuantif__ << ", int *" << __NOISEPOWER_WFP__ << ", int *" << __NOISEPOWER_WFPQuantif__ << ", FILE *pfileTrace);" << endl;
#else
			fileTempCpp << "void computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(int *" << __NOISEPOWER_WFPSFG__ << ", int *" << __NOISEPOWER_WFPSFGQuantif__ << ", int *" << __NOISEPOWER_WFP__ << ", int *" << __NOISEPOWER_WFPQuantif__ << ") {" << endl;
			fileCpp_pf << TAB << "computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(" << __NOISEPOWER_WFPSFG__ << ", " << __NOISEPOWER_WFPSFGQuantif__ << ", " << __NOISEPOWER_WFP__ << ", " << __NOISEPOWER_WFPQuantif__ << ");" << endl;
			fileHpp_pf << "void computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(int *" << __NOISEPOWER_WFPSFG__ << ", int *" << __NOISEPOWER_WFPSFGQuantif__ << ", int *" << __NOISEPOWER_WFP__ << ", int *" << __NOISEPOWER_WFPQuantif__ << ");" << endl;
#endif
		}
		fileTempCpp << *itStr << endl;
	}

	fileTempCpp << "}" << endl;
	fileTempCpp.close();

}

/**
 * Ecriture des WFPeff
 * \author Jean-Charles Naud
 */
void ComputePb_ecritureWFP_sfg_eff(Gfd *gfd, fstream & fileCpp_pf, fstream & fileHpp_pf) {
	// Ecriture des expressions des WFP effectif
	list<string>::iterator itStr;
	fstream fileTempCpp;
	int numeroIndexFileC = 0;
	int counter_Nbr_WFP = 0;
	ostringstream oss;
	string pathTempCpp;

	oss << numeroIndexFileC;
	cout<<ProgParameters::getOutputGenComputePbDirectory()<<endl;
	pathTempCpp = ProgParameters::getOutputGenComputePbDirectory() + "computePb_WFP_SFG_eff" + oss.str() + ".c";
	fileTempCpp.open(pathTempCpp.c_str(), fstream::out);
	assert(fileTempCpp.is_open());

	fileTempCpp << "#include \"Tool.h\"" << endl;

#if TRACE
	fileTempCpp << "void computePb_WFP_SFG_eff" << numeroIndexFileC << "(int *" << __NOISEPOWER_WFPSFG__ << ", int *" << __NOISEPOWER_WFPSFGEff__ << ", int *" << __NOISEPOWER_InputWD__ << ", FILE *pfileTrace) {" << endl;
	fileCpp_pf << TAB << "computePb_WFP_SFG_eff" << numeroIndexFileC << "(" << __NOISEPOWER_WFPSFG__ << ", " << __NOISEPOWER_WFPSFGEff__ << ", " << __NOISEPOWER_InputWD__ << ", pfileTrace);" << endl;
	fileHpp_pf << "void computePb_WFP_SFG_eff" << numeroIndexFileC << "(int *" << __NOISEPOWER_WFPSFG__ << ", int *" << __NOISEPOWER_WFPSFGEff__ << ", int *" << __NOISEPOWER_InputWD__ << ", FILE *pfileTrace);" << endl;
#else
	fileTempCpp << "void computePb_WFP_SFG_eff" << numeroIndexFileC << "(int *" << __NOISEPOWER_WFPSFG__ << ", int *" << __NOISEPOWER_WFPSFGEff__ << ", int *" << __NOISEPOWER_InputWD__ << ") {" << endl;
	fileCpp_pf << TAB << "computePb_WFP_SFG_eff" << numeroIndexFileC << "(" << __NOISEPOWER_WFPSFG__ << ", " << __NOISEPOWER_WFPSFGEff__ << ", " << __NOISEPOWER_InputWD__ << ");" << endl;
	fileHpp_pf << "void computePb_WFP_SFG_eff" << numeroIndexFileC << "(int *" << __NOISEPOWER_WFPSFG__ << ", int *" << __NOISEPOWER_WFPSFGEff__ << ", int *" << __NOISEPOWER_InputWD__ << ");" << endl;
#endif

	for (itStr = gfd->getBeginWFPSFGEff() ; itStr != gfd->getEndWFPSFGEff() ; ++itStr, counter_Nbr_WFP++) {
		// 23500 = 1 MO
		// 2350 = 100 ko
		// 1175 = 50 ko
		if(counter_Nbr_WFP > 2350){
			ostringstream oss1;
			counter_Nbr_WFP = 0;
			numeroIndexFileC++;

			fileTempCpp << "}" << endl;
			fileTempCpp.close();
			assert(!fileTempCpp.is_open());

			oss1 << numeroIndexFileC;
			pathTempCpp = ProgParameters::getOutputGenComputePbDirectory() + "computePb_WFP_SFG_eff" + oss1.str() + ".c";
			fileTempCpp.open(pathTempCpp.c_str(), fstream::out);
			assert(fileTempCpp.is_open());
			fileTempCpp << "#include \"Tool.h\"" << endl;

#if TRACE
			fileTempCpp << "void computePb_WFP_SFG_eff" << numeroIndexFileC << "(int *" << __NOISEPOWER_WFPSFG__ << ", int *" << __NOISEPOWER_WFPSFGEff__ << ", int *" << __NOISEPOWER_InputWD__ << ", FILE *pfileTrace) {" << endl;
			fileCpp_pf << TAB << "computePb_WFP_SFG_eff" << numeroIndexFileC << "(" << __NOISEPOWER_WFPSFG__ << ", " << __NOISEPOWER_WFPSFGEff__ << ", " << __NOISEPOWER_InputWD__ << ", pfileTrace);" << endl;
			fileHpp_pf << "void computePb_WFP_SFG_eff" << numeroIndexFileC << "(int *" << __NOISEPOWER_WFPSFG__ << ", int *" << __NOISEPOWER_WFPSFGEff__ << ", int *" << __NOISEPOWER_InputWD__ << ", FILE *pfileTrace);" << endl;
#else
			fileTempCpp << "void computePb_WFP_SFG_eff" << numeroIndexFileC << "(int *" << __NOISEPOWER_WFPSFG__ << ", int *" << __NOISEPOWER_WFPSFGEff__ << ", int *" << __NOISEPOWER_InputWD__ << ") {" << endl;
			fileCpp_pf << TAB << "computePb_WFP_SFG_eff" << numeroIndexFileC << "(" << __NOISEPOWER_WFPSFG__ << ", " << __NOISEPOWER_WFPSFGEff__ << ", " << __NOISEPOWER_InputWD__ << ");" << endl;
			fileHpp_pf << "void computePb_WFP_SFG_eff" << numeroIndexFileC << "(int *" << __NOISEPOWER_WFPSFG__ << ", int *" << __NOISEPOWER_WFPSFGEff__ << ", int *" << __NOISEPOWER_InputWD__ << ");" << endl;
#endif
		}
		fileTempCpp << *itStr << endl;
	}

	fileTempCpp << "}" << endl;
	fileTempCpp.close();
}

/**
 * Ecriture des Means et Var de chaque Bruit "br"
 * \author Jean-Charles Naud
 */
void ComputePb_ecritureMeanVarBr(Gfd * grapheDepart, fstream & fileCpp_pf, fstream & fileHpp_pf) {
	// Parcours des NoeudSrcBruit pour écrire les expression des moments
	fstream fileTempCpp;
	int numeroIndexFileC = 0;
	int counter_Nbr_WFP = 0;
	ostringstream oss;
	string pathTempCpp;

	oss << numeroIndexFileC;
	pathTempCpp = ProgParameters::getOutputGenComputePbDirectory() + "computePb_Mean_Var_Br" + oss.str() + ".c";
	fileTempCpp.open(pathTempCpp.c_str(), fstream::out);
	assert(fileTempCpp.is_open());

	fileTempCpp << "#include \"Tool.h\"" << endl;

#if TRACE
	fileTempCpp << "void computePb_Mean_Var_Br" << numeroIndexFileC << "(int *K, " << MEAN_VAR_TYPE << " *Q, int *" << __NOISEPOWER_WFPSFGEff__ << ", int *" << __NOISEPOWER_WFPSFGQuantif__ << ", int *" << __NOISEPOWER_InputWD__ << ", int *" << __NOISEPOWER_InputWDQuantif__ << ", " << MEAN_VAR_TYPE << " *" << __NOISEPOWER_XBMean__ << ", " << MEAN_VAR_TYPE << " *" << __NOISEPOWER_XBVar__ << ", FILE *pfileTrace) {" << endl;
	fileCpp_pf << TAB << "computePb_Mean_Var_Br" << numeroIndexFileC << "(K, Q, " << __NOISEPOWER_WFPSFGEff__ << ", " << __NOISEPOWER_WFPSFGQuantif__ << ", " << __NOISEPOWER_InputWD__ << ", " << __NOISEPOWER_InputWDQuantif__ << ", " << __NOISEPOWER_XBMean__ << ", " << __NOISEPOWER_XBVar__ << ", pfileTrace);" << endl;
	fileHpp_pf << "void computePb_Mean_Var_Br" << numeroIndexFileC << "(int *K, " << MEAN_VAR_TYPE << " *Q, int *" << __NOISEPOWER_WFPSFGEff__ << ", int *" << __NOISEPOWER_WFPSFGQuantif__ << ", int *" << __NOISEPOWER_InputWD__ << ", int *" << __NOISEPOWER_InputWDQuantif__ << ", " << MEAN_VAR_TYPE << " *" << __NOISEPOWER_XBMean__ << ", " << MEAN_VAR_TYPE << " *" << __NOISEPOWER_XBVar__ << ", FILE *pfileTrace);" << endl;
#else
	fileTempCpp << "void computePb_Mean_Var_Br" << numeroIndexFileC << "(int *K, " << MEAN_VAR_TYPE << " *Q, int *" << __NOISEPOWER_WFPSFGEff__ << ", int *" << __NOISEPOWER_WFPSFGQuantif__ << ", int *" << __NOISEPOWER_InputWD__ << ", int *" << __NOISEPOWER_InputWDQuantif__ << ", " << MEAN_VAR_TYPE << " *" << __NOISEPOWER_XBMean__ << ", " << MEAN_VAR_TYPE << " *" << __NOISEPOWER_XBVar__ << ") {" << endl;
	fileCpp_pf << TAB << "computePb_Mean_Var_Br" << numeroIndexFileC << "(K, Q, " << __NOISEPOWER_WFPSFGEff__ << ", " << __NOISEPOWER_WFPSFGQuantif__ << ", " << __NOISEPOWER_InputWD__ << ", " << __NOISEPOWER_InputWDQuantif__ << ", " << __NOISEPOWER_XBMean__ << ", " << __NOISEPOWER_XBVar__ << ");" << endl;
	fileHpp_pf << "void computePb_Mean_Var_Br" << numeroIndexFileC << "(int *K, " << MEAN_VAR_TYPE << " *Q, int *" << __NOISEPOWER_WFPSFGEff__ << ", int *" << __NOISEPOWER_WFPSFGQuantif__ << ", int *" << __NOISEPOWER_InputWD__ << ", int *" << __NOISEPOWER_InputWDQuantif__ << ", " << MEAN_VAR_TYPE << " *" << __NOISEPOWER_XBMean__ << ", " << MEAN_VAR_TYPE << " *" << __NOISEPOWER_XBVar__ << ");" << endl;
#endif

	for(int i = 0 ; i<grapheDepart->getNbNoiseNode() ; i++){
		// 3250 = 1 MO
		// 325 = 100 ko
		// 162 = 50 ko
		if(counter_Nbr_WFP > 325){
			ostringstream oss1;
			counter_Nbr_WFP = 0;
			numeroIndexFileC++;

			fileTempCpp << "}" << endl;
			fileTempCpp.close();
			assert(!fileTempCpp.is_open());

			oss1 << numeroIndexFileC;
			pathTempCpp = ProgParameters::getOutputGenComputePbDirectory() + "computePb_Mean_Var_Br" + oss1.str() + ".c";
			fileTempCpp.open(pathTempCpp.c_str(), fstream::out);
			assert(fileTempCpp.is_open());
			fileTempCpp << "#include \"Tool.h\"" << endl;

#if TRACE
			fileTempCpp << "void computePb_Mean_Var_Br" << numeroIndexFileC << "(int *K, " << MEAN_VAR_TYPE << " *Q, int *" << __NOISEPOWER_WFPSFGEff__ << ", int *" << __NOISEPOWER_WFPSFGQuantif__ << ", int *" << __NOISEPOWER_InputWD__ << ", int *" << __NOISEPOWER_InputWDQuantif__ << ", " << MEAN_VAR_TYPE << " *" << __NOISEPOWER_XBMean__ << ", " << MEAN_VAR_TYPE << " *" << __NOISEPOWER_XBVar__ << ", FILE *pfileTrace) {" << endl;
			fileCpp_pf << TAB << "computePb_Mean_Var_Br" << numeroIndexFileC << "(K, Q, " << __NOISEPOWER_WFPSFGEff__ << ", " << __NOISEPOWER_WFPSFGQuantif__ << ", " << __NOISEPOWER_InputWD__ << ", " << __NOISEPOWER_InputWDQuantif__ << ", " << __NOISEPOWER_XBMean__ << ", " << __NOISEPOWER_XBVar__ << ", pfileTrace);" << endl;
			fileHpp_pf << "void computePb_Mean_Var_Br" << numeroIndexFileC << "(int *K, " << MEAN_VAR_TYPE << " *Q, int *" << __NOISEPOWER_WFPSFGEff__ << ", int *" << __NOISEPOWER_WFPSFGQuantif__ << ", int *" << __NOISEPOWER_InputWD__ << ", int *" << __NOISEPOWER_InputWDQuantif__ << ", " << MEAN_VAR_TYPE << " *" << __NOISEPOWER_XBMean__ << ", " << MEAN_VAR_TYPE << " *" << __NOISEPOWER_XBVar__ << ", FILE *pfileTrace);" << endl;
#else
			fileTempCpp << "void computePb_Mean_Var_Br" << numeroIndexFileC << "(int *K, " << MEAN_VAR_TYPE << " *Q, int *" << __NOISEPOWER_WFPSFGEff__ << ", int *" << __NOISEPOWER_WFPSFGQuantif__ << ", int *" << __NOISEPOWER_InputWD__ << ", int *" << __NOISEPOWER_InputWDQuantif__ << ", " << MEAN_VAR_TYPE << " *" << __NOISEPOWER_XBMean__ << ", " << MEAN_VAR_TYPE << " *" << __NOISEPOWER_XBVar__ << ") {" << endl;
			fileCpp_pf << TAB << "computePb_Mean_Var_Br" << numeroIndexFileC << "(K, Q, " << __NOISEPOWER_WFPSFGEff__ << ", " << __NOISEPOWER_WFPSFGQuantif__ << ", " << __NOISEPOWER_InputWD__ << ", " << __NOISEPOWER_InputWDQuantif__ << ", " << __NOISEPOWER_XBMean__ << ", " << __NOISEPOWER_XBVar__ << ");" << endl;
			fileHpp_pf << "void computePb_Mean_Var_Br" << numeroIndexFileC << "(int *K, " << MEAN_VAR_TYPE << " *Q, int *" << __NOISEPOWER_WFPSFGEff__ << ", int *" << __NOISEPOWER_WFPSFGQuantif__ << ", int *" << __NOISEPOWER_InputWD__ << ", int *" << __NOISEPOWER_InputWDQuantif__ << ", " << MEAN_VAR_TYPE << " *" << __NOISEPOWER_XBMean__ << ", " << MEAN_VAR_TYPE << " *" << __NOISEPOWER_XBVar__ << ");" << endl;
#endif
		}
		counter_Nbr_WFP += grapheDepart->getNoiseNode(i)->getNbSuppressKBits();
		grapheDepart->getNoiseNode(i)->writeCalculMoments_SmallSize(fileTempCpp);
	}

	fileTempCpp << "}" << endl;
	fileTempCpp.close();
}


/**
 * Ecriture de l'expression du bruit Pby en sortie
 * \author Jean-Charles Naud
 */
void ComputePb_ecritureExpressionPby(fstream & fileCpp_pf) {
		fileCpp_pf << TAB << "ret_Pby = 0;"<< endl;

		// Get back file size and set the descriptor file at the right place
		fileCpp_pf << TAB << "fseek(fileVarGain , 0 , SEEK_END);"<<endl;
		fileCpp_pf << TAB << "fileVarSize = ftell(fileVarGain);"<<endl;
		fileCpp_pf << TAB << "rewind (fileVarGain);"<<endl;
		fileCpp_pf << TAB << "fseek(fileMeanGain , 0 , SEEK_END);"<<endl;
		fileCpp_pf << TAB << "fileMeanSize = ftell(fileMeanGain);"<<endl;
		fileCpp_pf << TAB << "rewind (fileMeanGain);"<<endl;
		fileCpp_pf << TAB << "fseek(fileMeanGain, NB_OUTPUT * NB_SRCE_NOISE * sizeof(" << GAIN_TYPE << "), SEEK_SET);" << endl;

		fileCpp_pf << TAB << "for ( itOut = 0 ; itOut < NB_OUTPUT ; itOut++){" << endl;
		fileCpp_pf << TAB << TAB << "mean = 0;" << endl;
		fileCpp_pf << TAB << TAB << "variance = 0;" << endl;
		fileCpp_pf << TAB << TAB << "for( i = 0 ; i < NB_SRCE_NOISE ; i++){" << endl;

		// Var accumulation
		fileCpp_pf << TAB << TAB << TAB << "result = fread(&hSumCarre, sizeof(" << GAIN_TYPE << "), 1, fileVarGain);"<<endl;
		fileCpp_pf << TAB << TAB << TAB << "if(result > fileVarSize) {fputs (\"Reading error\",stderr); exit (3);}" <<endl;
		fileCpp_pf << TAB << TAB << TAB << "variance += " << __NOISEPOWER_XBVar__ << "[i]*hSumCarre;" <<endl;
		fileCpp_pf << TAB << TAB << "}"<<endl;

		// Mean accumulation
		if(RuntimeParameters::isLTIGraph() && !RuntimeParameters::isRecursiveGraph() && RuntimeParameters::isBinaryWriteWithoutZero()){
#if BUFFER_USING
			ComputePb_writeReadingCompressedGainFile_buffer(fileCpp_pf);
#else
			ComputePb_writeReadingCompressedGainFile(fileCpp_pf);
#endif
		}
		else{
#if BUFFER_USING
			ComputePb_writeReadingGainFile_buffer(fileCpp_pf);
#else
			ComputePb_writeReadingGainFile(fileCpp_pf);
#endif
		}
        fileCpp_pf << TAB << TAB << RESULT_TYPE << " outputNoisePowerDb = 10*log10(variance + mean);"<<endl;
		fileCpp_pf << TAB << TAB << "ret_Pby += (variance+mean);"<<endl;
		fileCpp_pf << TAB << TAB << "if(maximumdb < outputNoisePowerDb)" << endl;
		fileCpp_pf << TAB << TAB << TAB << "maximumdb = outputNoisePowerDb;" << endl;
#if DEBUG
        fileCpp_pf << TAB << TAB << "fprintf(pfileDebug, \"Output : [%i]\\n\", itOut);" << endl;
        fileCpp_pf << TAB << TAB << "fprintf(pfileDebug, \"mean: %e\\n\", mean);" << endl;
        fileCpp_pf << TAB << TAB << "fprintf(pfileDebug, \"variance: %e\\n\", variance);" << endl;
        fileCpp_pf << TAB << TAB << "fprintf(pfileDebug, \"noise power: %f\\n\\n\", outputNoisePowerDb);" << endl;
#endif
		fileCpp_pf << TAB << "}"<<endl<<endl;
		fileCpp_pf << TAB << "fclose(fileVarGain);" << endl;
		fileCpp_pf << TAB << "fclose(fileMeanGain);" << endl;

	if (ProgParameters::getOutputFormat() == "CJni") {
		fileCpp_pf << TAB << "(*env)->ReleaseIntArrayElements(env,jWFP," << __NOISEPOWER_WFP__ << ",0);" << endl;
		fileCpp_pf << TAB << "(*env)->ReleaseIntArrayElements(env,jWFPquantMode ," << __NOISEPOWER_WFPQuantif__ << ",0);" << endl;
		fileCpp_pf << TAB << "(*env)->ReleaseIntArrayElements(env,jtabInputWd," << __NOISEPOWER_InputWD__ << ",0);" << endl;
		fileCpp_pf << TAB << "(*env)->ReleaseIntArrayElements(env,jtabInputWdquantMode," << __NOISEPOWER_InputWDQuantif__ << ",0);" << endl;
	}

	fileCpp_pf << TAB << "free(" << __NOISEPOWER_WFPSFG__ << ");" << endl;
	fileCpp_pf << TAB << "free(" << __NOISEPOWER_WFPSFGEff__ << ");" << endl;
	fileCpp_pf << TAB << "free(" << __NOISEPOWER_WFPSFGQuantif__ << ");" << endl << endl;
    fileCpp_pf << TAB << "ret_Pby = ret_Pby/NB_OUTPUT;" << endl;
#if DEBUG
    fileCpp_pf << TAB << "fprintf(pfileDebug, \"maximum noise power (db): %f\\n\\n\", maximumdb);" << endl;
    fileCpp_pf << TAB << "fprintf(pfileDebug, \"mean noise powere (db): %f\\n\\n\", 10*log10(ret_Pby));" << endl;
    fileCpp_pf << TAB << "fclose(pfileDebug);" << endl;
#endif
#if TRACE
    fileCpp_pf << TAB << "fclose(pfileTrace);" << endl;
#endif
//	fileCpp_pf << TAB << "return 10*log10(ret_Pby);" << endl;
    fileCpp_pf << TAB << "if(outputMode == 0){" << endl;
    fileCpp_pf << TAB << TAB << "return maximumdb;" << endl;
    fileCpp_pf << TAB << "} else if (outputMode == 1){" << endl;
    fileCpp_pf << TAB << TAB << "return 10*log10(ret_Pby);" << endl;
    fileCpp_pf << TAB << "} else {" << endl;
    fileCpp_pf << TAB << TAB << "fputs (\"Output mode not valid\",stderr); exit (4);" << endl;
    fileCpp_pf << TAB << "}" << endl;
}

#if BUFFER_USING
void ComputePb_writeReadingCompressedGainFile_buffer(fstream& fileCpp_pf) {
	fileCpp_pf << TAB << TAB << "result = fread(&nbBrDependant, sizeof(unsigned short int), 1, fileMeanGain);"<< endl;
	fileCpp_pf << TAB << TAB << "if(result > fileMeanSize) {fputs (\"Reading error\",stderr); exit (3);}" << endl;
	fileCpp_pf << TAB << TAB << "brDependant = (unsigned short int*) malloc(nbBrDependant * sizeof(unsigned short int));" << endl;
	fileCpp_pf << TAB << TAB << "if(brDependant == NULL){" << endl;
	fileCpp_pf << TAB << TAB << TAB << "printf(\"Allocation tableau brDependant echoué\\n\");" << endl;
	fileCpp_pf << TAB << TAB << TAB << "exit(0);" << endl;
	fileCpp_pf << TAB << TAB << "}" << endl;
	fileCpp_pf << TAB << TAB << "for( temp = 0 ; temp < nbBrDependant ; temp++){" << endl;
	fileCpp_pf << TAB << TAB << TAB	<< "result = fread(&brDependant[temp], sizeof(unsigned short int), 1, fileMeanGain);" << endl;
	fileCpp_pf << TAB << TAB << TAB	<< "if(result > fileMeanSize) {fputs (\"Reading error\",stderr); return 3;}" << endl;
	fileCpp_pf << TAB << TAB << "}" << endl;
	fileCpp_pf << TAB << TAB << "for( i = 0 ; i < nbBrDependant ; i++){"<<endl;
	fileCpp_pf << TAB << TAB << TAB << "totalElemRead = 0;"<<endl;
	fileCpp_pf << TAB << TAB << TAB << "nbElemRead = 0;"<<endl;
	fileCpp_pf << TAB << TAB << TAB << "for( j = i ; j < nbBrDependant ; ){"<<endl;

	fileCpp_pf << TAB << TAB << TAB << TAB << "if(totalElemRead + sizeBuffer < nbBrDependant - j ){"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << TAB << "nbElemRead = sizeBuffer;"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "}"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "else{"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << TAB << "nbElemRead = nbBrDependant - j - totalElemRead;"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "}"<<endl;

	fileCpp_pf << TAB << TAB << TAB << TAB << "totalElemRead += nbElemRead;"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "result = fread(buffer, sizeof(" << GAIN_TYPE << "), nbElemRead, fileMeanGain);"<<endl;

	fileCpp_pf << TAB << TAB << TAB << TAB << "if(result > fileMeanSize) {fputs (\"Reading error\",stderr); return 4;}" <<endl;

	fileCpp_pf << TAB << TAB << TAB << TAB << "for(k = 0 ; k < nbElemRead ; k++){"<<endl;

	fileCpp_pf << TAB << TAB << TAB << TAB << TAB << "if( i == j){"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << TAB << TAB << "mean += " << __NOISEPOWER_XBMean__ << "[brDependant[i]] * " << __NOISEPOWER_XBMean__ << "[brDependant[j]] * buffer[k];"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << TAB << "}"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << TAB << "else{"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << TAB << TAB << "mean += 2*" << __NOISEPOWER_XBMean__ << "[brDependant[i]] * " << __NOISEPOWER_XBMean__ << "[brDependant[j]] * buffer[k];"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << TAB << "}"<<endl;
#if TRACE
	fileCpp_pf << TAB << TAB << TAB << TAB << "fprintf(pfileTrace, \"Xb_Mean[%i]: %e\\n\", brDependant[i], Xb_Mean[brDependant[i]]);" << endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "fprintf(pfileTrace, \"Xb_Mean[%i]: %e\\n\", brDependant[j], Xb_Mean[brDependant[j]]);" << endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "fprintf(pfileTrace, \"hSumCroise: %e\\n\", buffer[k]);" << endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "fprintf(pfileTrace, \"mean: %e\\n\\n\", mean);" << endl;
#endif

	fileCpp_pf << TAB << TAB << TAB << TAB << TAB << "j++;"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "}"<<endl;
	fileCpp_pf << TAB << TAB << TAB << "}"<<endl;
	fileCpp_pf << TAB << TAB << "}"<<endl;
	fileCpp_pf << TAB << TAB << "free(brDependant);" << endl << endl;
}

void ComputePb_writeReadingGainFile_buffer(fstream& fileCpp_pf) {
	fileCpp_pf << TAB << TAB << "for( i = 0 ; i < NB_SRCE_NOISE ; i++){"<<endl;
	fileCpp_pf << TAB << TAB << TAB << "totalElemRead = 0;"<<endl;
	fileCpp_pf << TAB << TAB << TAB << "nbElemRead = 0;"<<endl;
	fileCpp_pf << TAB << TAB << TAB << "for( j = i ; j < NB_SRCE_NOISE ; ){"<<endl;

	fileCpp_pf << TAB << TAB << TAB << TAB << "if(totalElemRead + sizeBuffer < NB_SRCE_NOISE - j ){"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << TAB << "nbElemRead = sizeBuffer;"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "}"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "else{"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << TAB << "nbElemRead = NB_SRCE_NOISE - j - totalElemRead;"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "}"<<endl;

	fileCpp_pf << TAB << TAB << TAB << TAB << "totalElemRead += nbElemRead;"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "result = fread(buffer, sizeof(" << GAIN_TYPE << "), nbElemRead, fileMeanGain);"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "if(result > fileMeanSize) {fputs (\"Reading error\",stderr); exit (3);}" <<endl;

	fileCpp_pf << TAB << TAB << TAB << TAB << "for(k = 0 ; k < nbElemRead ; k++){"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << TAB << "if(i == j){"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << TAB << TAB << "mean += " << __NOISEPOWER_XBMean__ << "[i]*" << __NOISEPOWER_XBMean__ << "[j]*buffer[k];"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << TAB << "}"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << TAB << "else{"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << TAB << TAB << "mean += 2*" << __NOISEPOWER_XBMean__ << "[i]*" << __NOISEPOWER_XBMean__ << "[j]*buffer[k];"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << TAB << "}"<<endl;
#if TRACE
	fileCpp_pf << TAB << TAB << TAB << "fprintf(pfileTrace, \"Xb_Mean[%i]: %e\\n\", i, Xb_Mean[i]);" << endl;
	fileCpp_pf << TAB << TAB << TAB << "fprintf(pfileTrace, \"Xb_Mean[%i]: %e\\n\", j, Xb_Mean[j]);" << endl;
	fileCpp_pf << TAB << TAB << TAB << "fprintf(pfileTrace, \"hSumCroise: %e\\n\", buffer[k]);" << endl;
	fileCpp_pf << TAB << TAB << TAB << "fprintf(pfileTrace, \"mean: %e\\n\\n\", mean);" << endl;
#endif

	fileCpp_pf << TAB << TAB << TAB << TAB << TAB << "j++;"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "}"<<endl;
	fileCpp_pf << TAB << TAB << TAB << "}"<<endl;
	fileCpp_pf << TAB << TAB << "}"<<endl;

}

#else

void ComputePb_writeReadingCompressedGainFile(fstream& fileCpp_pf) {
	fileCpp_pf << TAB << TAB << "result = fread(&nbBrDependant, sizeof(unsigned short int), 1, fileMeanGain);"<< endl;
	fileCpp_pf << TAB << TAB << "if(result > fileMeanSize) {fputs (\"Reading error\",stderr); exit (3);}" << endl;
	fileCpp_pf << TAB << TAB << "brDependant = (unsigned short int*) malloc(nbBrDependant * sizeof(unsigned short int));" << endl;
	fileCpp_pf << TAB << TAB << "if(brDependant == NULL){" << endl;
	fileCpp_pf << TAB << TAB << TAB << "printf(\"Allocation tableau brDependant echoué\\n\");" << endl;
	fileCpp_pf << TAB << TAB << TAB << "exit(0);" << endl;
	fileCpp_pf << TAB << TAB << "}" << endl;
	fileCpp_pf << TAB << TAB << "for( temp = 0 ; temp < nbBrDependant ; temp++){" << endl;
	fileCpp_pf << TAB << TAB << TAB	<< "result = fread(&brDependant[temp], sizeof(unsigned short int), 1, fileMeanGain);" << endl;
	fileCpp_pf << TAB << TAB << TAB	<< "if(result > fileMeanSize) {fputs (\"Reading error\",stderr); return 3;}" << endl;
	fileCpp_pf << TAB << TAB << "}" << endl;
	fileCpp_pf << TAB << TAB << "for( i = 0 ; i < nbBrDependant ; i++){" << endl;
	fileCpp_pf << TAB << TAB << TAB << "for( j = i ; j < nbBrDependant ; j++){"	<< endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "result = fread(&hSumCroise, sizeof(" << GAIN_TYPE << "), 1, fileMeanGain);" << endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "if(result > fileMeanSize) {fputs (\"Reading error\",stderr); return 4;}"	<< endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "if( i == j){" << endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << TAB << "mean += " << __NOISEPOWER_XBMean__ << "[brDependant[i]] * "
			<< __NOISEPOWER_XBMean__ << "[brDependant[j]] * hSumCroise;" << endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "}" << endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "else{" << endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << TAB << "mean += 2*" << __NOISEPOWER_XBMean__ << "[brDependant[i]] * "
			<< __NOISEPOWER_XBMean__ << "[brDependant[j]] * hSumCroise;" << endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "}" << endl;
#if TRACE
            fileCpp_pf << TAB << TAB << TAB << TAB << "fprintf(pfileTrace, \"Xb_Mean[%i]: %e\\n\", brDependant[i], Xb_Mean[brDependant[i]]);" << endl;
            fileCpp_pf << TAB << TAB << TAB << TAB << "fprintf(pfileTrace, \"Xb_Mean[%i]: %e\\n\", brDependant[j], Xb_Mean[brDependant[j]]);" << endl;
            fileCpp_pf << TAB << TAB << TAB << TAB << "fprintf(pfileTrace, \"hSumCroise: %e\\n\", hSumCroise);"<<endl;
            fileCpp_pf << TAB << TAB << TAB << TAB << "fprintf(pfileTrace, \"mean: %e\\n\\n\", mean);" << endl;
#endif

	fileCpp_pf << TAB << TAB << TAB << "}" << endl;
	fileCpp_pf << TAB << TAB << "}" << endl;
	fileCpp_pf << TAB << TAB << "free(brDependant);" << endl << endl;
}

void ComputePb_writeReadingGainFile(fstream& fileCpp_pf) {
	fileCpp_pf << TAB << TAB << "for( i = 0 ; i < NB_SRCE_NOISE ; i++){"<<endl;
	fileCpp_pf << TAB << TAB << TAB << "for( j = i ; j < NB_SRCE_NOISE ; j++){"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "result = fread(&hSumCroise, sizeof(" << GAIN_TYPE << "), 1, fileMeanGain);"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "if(result > fileMeanSize) {fputs (\"Reading error\",stderr); exit (3);}" <<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "if(i == j){"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << TAB << "mean += " << __NOISEPOWER_XBMean__ << "[i]*" << __NOISEPOWER_XBMean__ << "[j]*hSumCroise;"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "}"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "else{"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << TAB << "mean += 2*" << __NOISEPOWER_XBMean__ << "[i]*" << __NOISEPOWER_XBMean__ << "[j]*hSumCroise;"<<endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "}"<<endl;
#if TRACE
	fileCpp_pf << TAB << TAB << TAB << TAB << "fprintf(pfileTrace, \"Xb_Mean[%i]: %e\\n\", i, Xb_Mean[i]);" << endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "fprintf(pfileTrace, \"Xb_Mean[%i]: %e\\n\", j, Xb_Mean[j]);" << endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "fprintf(pfileTrace, \"hSumCroise: %e\\n\", hSumCroise);" << endl;
	fileCpp_pf << TAB << TAB << TAB << TAB << "fprintf(pfileTrace, \"mean: %e\\n\\n\", mean);" << endl;
#endif
	fileCpp_pf << TAB << TAB << TAB << "}"<<endl;
	fileCpp_pf << TAB << TAB << "}"<<endl;
}
#endif






/**
 *  Methode ecrit dans le fichier le calcul des moments d'ordre 1 et 2 de la source de bruit
 *  \param 	fichier ou ecrire
 * \author Jean-Charles Naud
 */
void NoeudSrcBruit::writeCalculMoments_SmallSize(fstream & fileCpp_pf) {
	int i = 0;

	fileCpp_pf << TAB << "// " << this->getName().data() << " ;nbK = " << this->m_KbitsElimine.size() << endl;
	if (this->m_KbitsElimine.size() != 0) {
		for (i = 0; i < (int) this->m_KbitsElimine.size(); i++) {
			//cout<<this->getName()<<" "<<m_PasQuantif[i]<<endl;
			fileCpp_pf << TAB << "K[" << i << "] = " << m_KbitsElimine[i] << ";" << endl;
			fileCpp_pf << TAB << "Q[" << i << "] = " << m_PasQuantif[i] << ";" << endl;
#if TRACE
            fileCpp_pf << TAB << "fprintf(pfileTrace, \"K[" << i << "]: %i\\n\", K[" << i << "]);" << endl;
            fileCpp_pf << TAB << "fprintf(pfileTrace, \"Q[" << i << "]: %f\\n\", Q[" << i << "]);" << endl;
#endif
		}
		/*Debug: cout<<"Pas Quantif: "<<this->getName()<<" "<<this->getModeQuantif()<<endl;*/
		fileCpp_pf << TAB << __NOISEPOWER_XBMean__ << "[" << this->getNumSrc() - 1 << "] = ";

		//cout<<" "<<this->m_KbitsElimine.size()<<endl;

		for (i = 0; i < (int) this->m_KbitsElimine.size(); i++) {
			if (this->m_SigneMoyenne[i] == 1) { // Positif: +1
				fileCpp_pf << "+M(Q[" << i << "],K[" << i << "],"<< m_ModeQuantif[i]<<")*" << m_Constante[i]; // Xb_Mean[0] = CalculBruitMean(Q0,K0)+
			}
			else { // Negatif: -1
				fileCpp_pf << "-M(Q[" << i << "],K[" << i << "],"<< m_ModeQuantif[i]<<")*" << m_Constante[i]; // Xb_Mean[0] = CalculBruitMean(Q0,K0)+
			}
		}
		fileCpp_pf << ";"<<endl;
		fileCpp_pf << TAB << __NOISEPOWER_XBVar__ << "[" << this->getNumSrc() - 1 << "] = "; // Xb_Var[0] =
		// On enleve 1 car Br1 est represente par l'indice 0
		//xxx changer le mode d'arrondi de manière dynamique.
		for (i = 0; i < (int) this->m_KbitsElimine.size() - 1; i++) {
			fileCpp_pf << "V(Q[" << i << "],K[" << i << "],"<< m_ModeQuantif[i]<<")*" << m_Constante[i]*m_Constante[i] << " + "; // Xb_Var[0] = CalculBruitVar(Q0,K0)+
		}
		fileCpp_pf << " V(Q[" << i << "],K[" << i << "],"<< m_ModeQuantif[i]<<")*" << m_Constante[i]*m_Constante[i]; // Xb_Var[0] = CalculBruitVar(Q0,K0)+ CalculBruitVar(Q1,K1);
		fileCpp_pf << ";"<<endl;

		ostringstream oss117;
		oss117 << this->getNumSrc()-1;
#if TRACE
		fileCpp_pf << TAB << "fprintf(pfileTrace, \"Xb_Mean[%d] : %e - Xb_Var[%d] : %e\"," + oss117.str() + ",Xb_Mean[" << this->getNumSrc() - 1 << "]," + oss117.str() + ",Xb_Var[" << this->getNumSrc() - 1 << "]);  " << endl;
		fileCpp_pf << TAB << "fprintf(pfileTrace, \"  K = %d\\n\\n\",K["<<i<<"]);" << endl;
#endif
		fileCpp_pf << endl;
	}
}

void WFPCDFGToWFPSFG(Gfd* gfd){
	NoeudGFD * noeudGFD;
	NoeudOps * noeudOps;
	NodeGraphContainer::iterator it;
	int NumSFG, NumCDFG;
	string str;
	std::ostringstream ossSFG, ossCDFG;

	// === Expression des liens entre WFP_sfg et WFP_cdgf
	for (it = gfd->getBeginTabNode(); it != gfd->getEndTabNode(); ++it) {
		if ((*it)->getTypeNodeGraph() == GFD) {
			noeudGFD = (NoeudGFD *) *it;
			if (noeudGFD->getTypeNodeGFD() == OPS) {
				noeudOps = (NoeudOps *) noeudGFD;
				NumSFG = noeudOps->getNumSFG();
				NumCDFG = noeudOps->getNumCDFG();
				if (NumSFG >= 0 && NumCDFG >= 0) {
					ossSFG.str("");
					ossCDFG.str("");
					ossSFG << NumSFG;
					ossCDFG << NumCDFG;

					str = TAB + __NOISEPOWER_WFPSFG__ + "[" + ossSFG.str() + "*3+0] = " + __NOISEPOWER_WFP__ + "[" + ossCDFG.str() + "*3+0];";
					gfd->addWFPSFG(str);
					str = TAB + __NOISEPOWER_WFPSFG__ + "[" + ossSFG.str() + "*3+1] = " + __NOISEPOWER_WFP__ + "[" + ossCDFG.str() + "*3+1];";
					gfd->addWFPSFG(str);
					str = TAB + __NOISEPOWER_WFPSFG__ + "[" + ossSFG.str() + "*3+2] = " + __NOISEPOWER_WFP__ + "[" + ossCDFG.str() + "*3+2];";
					gfd->addWFPSFG(str);
					str = TAB + __NOISEPOWER_WFPSFGQuantif__ + "[" + ossSFG.str() + "*3+0] = " + __NOISEPOWER_WFPQuantif__ + "[" + ossCDFG.str() + "*3+0];";
					gfd->addWFPSFG(str);
					str = TAB + __NOISEPOWER_WFPSFGQuantif__ + "[" + ossSFG.str() + "*3+1] = " + __NOISEPOWER_WFPQuantif__ + "[" + ossCDFG.str() + "*3+1];";
					gfd->addWFPSFG(str);
					str = TAB + __NOISEPOWER_WFPSFGQuantif__ + "[" + ossSFG.str() + "*3+2] = " + __NOISEPOWER_WFPQuantif__ + "[" + ossCDFG.str() + "*3+2];";
					gfd->addWFPSFG(str);

#if TRACE
					str = "fprintf(pfileTrace, \"WFP_sfg[" + ossSFG.str() + "*3+0]: %i\\n\", WFP_sfg[" + ossSFG.str() + "*3+0]);";
					str += "\nfprintf(pfileTrace, \"WFP_sfg[" + ossSFG.str() + "*3+1]: %i\\n\", WFP_sfg[" + ossSFG.str() + "*3+1]);";
					str += "\nfprintf(pfileTrace, \"WFP_sfg[" + ossSFG.str() + "*3+2]: %i\\n\", WFP_sfg[" + ossSFG.str() + "*3+2]);";
					str += "\nfprintf(pfileTrace, \"WFP_sfg_ModeQuantif[" + ossSFG.str() + "*3+0]: %i\\n\", WFP_sfg_ModeQuantif[" + ossSFG.str() + "*3+0]);";
					str += "\nfprintf(pfileTrace, \"WFP_sfg_ModeQuantif[" + ossSFG.str() + "*3+1]: %i\\n\", WFP_sfg_ModeQuantif[" + ossSFG.str() + "*3+1]);";
					str += "\nfprintf(pfileTrace, \"WFP_sfg_ModeQuantif[" + ossSFG.str() + "*3+2]: %i\\n\", WFP_sfg_ModeQuantif[" + ossSFG.str() + "*3+2]);";
					gfd->addWFPSFG(str);
#endif
				}
			}
		}
	}
}


void WFPSFGToWFPSFGEff(Gfd* gfd){
    list<NoeudGraph *> * listTriBr = new list<NoeudGraph *> ;
    NodeGraphContainer::iterator it;
    NoeudSrcBruit * nodeSrcBruit;
    NoeudGFD * nodeGFD, * nodeGFD2;
    NoeudData* nodeData;
    NoeudOps* nodeOpSucc;
    string str;
    std::ostringstream ossBr, ossOpSucc, ossData, ossNumIn, ss;
    bool hasVariableWidth;

    // === Construction de listTriBr et détection des cycles avec (Etat = R2 et R3)
    gfd->resetInfoVisite();
    for (it = gfd->getBeginTabOutput(); it != gfd->getEndTabOutput(); ++it) {
    	WFPSFGToWFPSFGEff_NoiseSourceSort((*it), listTriBr);
    }

    // === Affichage listTriBr (debug)
    /*cout << "------ listTriBr  -------" << endl;
      for (it = listTriBr->begin(); it != listTriBr->end(); ++it) {
      cout << (*it)->getName().data() << endl;
      }

    // === Affichage des cycles détectés (debug)
    cout << "------ cycles détectés --------" << endl;
    it = this->tabNode->begin();
    ++it;
    for (++it; it != this->tabNode->end(); ++it) {

    if ((*it)->getEtat() == R1) {
    cout << "R1 = " << (*it)->getName().data() << endl;
    }
    if ((*it)->getEtat() == R2) {
    cout << "R2 = " << (*it)->getName().data() << endl;
    }
    if ((*it)->getEtat() == R3) {
    cout << "R3 = " << (*it)->getName().data() << endl;
    }
    }
    cout << "--------------------------" << endl;*/
    // === Écriture : 1) WFP_sfg_eff des entrées constantes
    str = TAB;
    str += "// WFP_sfg_eff par rapport aux entrées constantes";
    gfd->addWFPSFGEff(str);
    it = gfd->getBeginTabNode();
    ++it;
    for (++it; it != gfd->getEndTabNode(); ++it) {
        nodeGFD = dynamic_cast<NoeudGFD *> (*it);
        assert(nodeGFD!=NULL);
        if (nodeGFD->getTypeNodeGFD() == DATA) {
            nodeData = (NoeudData *) *it;
            if (nodeData->getTypeVar()->getTypeVarFonct() == CONST) {
                for(int i = 0 ; i < nodeData->getNbSucc() ; i++){
                    nodeOpSucc = dynamic_cast<NoeudOps *> (nodeData->getElementSucc(i));
                    assert(nodeOpSucc!=NULL);
                    // === Save Nums
                    hasVariableWidth = nodeData->getVariableWidth();
                    ossData.str("");
					ossNumIn.str("");
					ossOpSucc.str("");
                    ss.str("");
					ossData << nodeData->getNumCDFG();
					ossNumIn << nodeData->getEdgeSucc(i)->getNumInput();
					ossOpSucc << nodeOpSucc->getNumSFG();
					// Ecriture WFP_sfg_eff par rapport aux entrées et constantes
					if (!hasVariableWidth) {
						// WFP_sfg_eff[P1][j] = WFP[P1][j];
						str = TAB + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]";
						//str += " = " + __NOISEPOWER_WFPSFG__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "];";
						ss << scientific << nodeData->getValeur();
						str += " = nbBitSignif("+ ss.str() + ", " + __NOISEPOWER_WFPSFG__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
#if TRACE
						str += "\nfprintf(pfileTrace, \"nbBitSignif(%f, %i) = %i\\n\", " + ss.str() + ", WFP_sfg[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "], nbBitSignif("+ ss.str() + ", WFP_sfg[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]));";
						str += "\nfprintf(pfileTrace, \"WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]: %i\\n\", WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
#endif
						gfd->addWFPSFGEff(str);
					}
					else {
						// WFP_sfg_eff[P1][j] = min(WFP[P1][j], tabInputWd[xDataP"]);
						str = TAB + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]";
						ss << scientific << nodeData->getValeur();
						str += " = min(nbBitSignif(" + ss.str() + ", " + __NOISEPOWER_WFPSFG__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]),";
						//str += " " + __NOISEPOWER_InputWD__ + "[" + ossData.str() + "]);";
						str += " nbBitSignif("+ ss.str() + ", " + __NOISEPOWER_InputWD__ + "[" + ossData.str() + "]));";
#if TRACE
						str += "\nfprintf(pfileTrace, \"nbBitSignif(%f, %i) = %i\\n\", " + ss.str() + ", tabInputWd[" + ossData.str() + "], nbBitSignif("+ ss.str() + ", tabInputWd[" + ossData.str() + "]));";
						str += "\nfprintf(pfileTrace, \"WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]: %i\\n\", WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
#endif
						gfd->addWFPSFGEff(str);
					}
				}
			}
		}
	}

	// === Écriture : 2) WFP_sfg_eff a l'aide des br
	list<NoeudGraph*>::iterator itList;
	for (itList = listTriBr->begin(); itList != listTriBr->end(); ++itList) {
		nodeSrcBruit = dynamic_cast<NoeudSrcBruit *> (*itList);
		assert(nodeSrcBruit != NULL);
		str = TAB;
		str += "// " + nodeSrcBruit->getName();  // Affichage de commentaire dans le computePb a travers la liste correspondant aux expressions de bruit dans les noeud src bruit
		gfd->addWFPSFGEff(str);

		nodeGFD = dynamic_cast<NoeudGFD *> (nodeSrcBruit->getElementPred(0));
		nodeGFD2 = dynamic_cast<NoeudGFD *> (nodeSrcBruit->getElementSucc(0));
		assert(nodeGFD != NULL && nodeGFD2 != NULL);

		if (nodeGFD->getTypeNodeGFD() == DATA) { // Cas GENERAL ou INPUT
			nodeData = (NoeudData *) nodeGFD;
			if (nodeData->isNodeSource()) { // Cas INPUT
				WFPSFGToWFPSFGEff_InputCase(gfd, nodeSrcBruit);
			}
			else {
				nodeGFD=dynamic_cast<NoeudGFD *> (nodeGFD->getElementPred(0));
				if(nodeGFD->getTypeNodeGFD()==OPS) { // Cas GENERAL
					WFPSFGToWFPSFGEff_GeneralCase1(gfd, nodeSrcBruit); // Cas général 1
				}
				else{
					WFPSFGToWFPSFGEff_GeneralCase3(gfd, nodeSrcBruit); // Cas général 3
				}
			}
		}
		else if (nodeGFD->getTypeNodeGFD() == OPS){
			nodeData = dynamic_cast<NoeudData *> (nodeGFD2);
			assert(nodeData != NULL);

			if(!nodeData->isNodeOutput()){// Cas OUTPUT probleme ici
				WFPSFGToWFPSFGEff_GeneralCase2(gfd, nodeSrcBruit); //Cas general 2
			}
			else{
				WFPSFGToWFPSFGEff_OutputCase(gfd, nodeSrcBruit);
			}

		}
		else { // Cas OUTPUT
			trace_error("Problème de construction au niveau des Br");
			exit(0);
		}
	}

	// Affichage pour verifier le contenu des différents Br
	/*for (itList = listTriBr->begin(); itList != listTriBr->end(); ++itList) {
	  nodeSrcBruit = dynamic_cast<NoeudSrcBruit *> (*itList);
	  cout<<nodeSrcBruit->getName()<<endl;
	  for (int i = 0; i < nodeSrcBruit->getNbSuppressKBits(); i++) {
	  cout << TAB << "K[" << i << "] = " << nodeSrcBruit->getNumSuppressKBits(i) << ";" << endl;
	  cout << TAB << "Q[" << i << "] = " << nodeSrcBruit->getNumStepQuantif(i) << ";" << endl;
	  }
	  cout<<endl;
	  }*/
	delete (listTriBr);
}

int WFPSFGToWFPSFGEff_NoiseSourceSort(NoeudGraph* node, list<NoeudGraph *> * listTriBr) {
	int count;
	int test, testTemp;
	NoeudGFD * nodeGFD;
	NoeudData *nodeData;
	//this->(ENTRAITEMENT);
	nodeGFD = dynamic_cast<NoeudGFD *> (node);
	assert(nodeGFD != NULL);

	switch (nodeGFD->getTypeNodeGFD()) {
		case DATA:
			nodeData = (NoeudData *) nodeGFD;

			if (nodeData->isNodeSource()) {
				node->setEtat(VISITE);
				return 0; // return: fin et pas de cycle detecté.
			}
			else {
				switch (node->getEtat()) {
					case NONVISITE:
						node->setEtat(ENTRAITEMENT);
						test = WFPSFGToWFPSFGEff_NoiseSourceSort(nodeData->getElementPred(0), listTriBr);
						// Decente
						if (node->getEtat() == R1) {
							node->setEtat(R3);
						}
						else { // xxx enlever pour la version 2
							if (test == 1) {
								node->setEtat(R2);
								if (nodeData->getTypeNodeData() == DATA_SRC_BRUIT) { // "br"
									listTriBr->push_back(node);
								}
							}
							else {
								node->setEtat(VISITE); // xxx enlever pour la version 2
								if (nodeData->getTypeNodeData() == DATA_SRC_BRUIT) { // "br"
									listTriBr->push_back(node);
								}
							}
						}
						return 0;
						break;
					case VISITE: // Déjà traité
					case R2: // Déjà traité
					case R3: // Déjà traité
						return 0;// return: fin et pas de cycle detecté.
						break;
					case ENTRAITEMENT: // Déctection d'un cycle
					case R1: // Déctection d'un cycle
						node->setEtat(R1);
						return 1; // return: cycle detecté.
						break;
					default:
						trace_error("Type non pris en compte (DATA)");
						exit(EXIT_FAILURE);
				}
			}
		case OPS:
			switch (node->getEtat()) {
				case NONVISITE:
					node->setEtat(ENTRAITEMENT);
					test = 0;
					for (count = 0; count < node->getNbPred(); count++) {
						nodeGFD = dynamic_cast<NoeudGFD*> (node->getElementPred(count));
						assert(nodeGFD != NULL);
						testTemp = WFPSFGToWFPSFGEff_NoiseSourceSort(nodeGFD, listTriBr);
						if (testTemp == 1) {
							test = 1;
						}
					}
					if (test == 1) { // au moin un des pred est un noeud data de rebouclage.
						node->setEtat(R2);
					}
					else {
						node->setEtat(VISITE);
					}
					return 0;
					break;
				case VISITE: // Déjà traité
				case R2: // Déjà traité
					return 0; // Logiquement, jamais utilisé
					break;
				case ENTRAITEMENT:
					trace_error("OP ENTRAITEMENT: Impossible");
					exit(EXIT_FAILURE);
					break;
				default:
					trace_error("Type non pris en compte (OP)");
					exit(EXIT_FAILURE);
			}
			break;
		default:
			trace_error("Type non pris en compte (OP)");
			exit(EXIT_FAILURE);
	}
	return 0;
}

/**
 *	Pré calcul de la propagation des Wfp effectif cas par défaut
 * \author Jean-Charles Naud
 *
 */
void WFPSFGToWFPSFGEff_GeneralCase1(Gfd* gfd, NoeudSrcBruit* noiseNode) {
	NoeudOps * nodeOpPred, *nodeOpSucc;
	NoeudData* nodeData;
	bool hasVariableWidth;
	string str;
	std::ostringstream ossBr, ossOpSucc, ossOpPred, ossData, ossNumIn;
	ossBr.str("");
	nodeOpSucc = dynamic_cast<NoeudOps *> (noiseNode->getElementSucc(0));
	nodeData = dynamic_cast<NoeudData *> (noiseNode->getElementPred(0));
	nodeOpPred = dynamic_cast<NoeudOps *> (nodeData->getElementPred(0));

	// === Save Nums
	hasVariableWidth = nodeData->getVariableWidth();
	ossOpSucc << nodeOpSucc->getNumSFG();
	ossNumIn << noiseNode->getEdgeSucc(0)->getNumInput();
	ossOpPred << nodeOpPred->getNumSFG();
	ossData << nodeData->getNumCDFG();

	if (!hasVariableWidth) { // Data sans pragma
		// = Ecriture WFP_sfg_eff
		WFPEffOperatorDetermination(gfd, nodeOpPred);
		// WFP_sfg_effs[P2][in] = min(WFP_sfg_eff[P1][2], WFP_sfg[P2][in] )
		str = TAB + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "] = ";
		str += "min(" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpPred.str() + "*3+2], "; // si 2 est la sortie
		str += TAB + __NOISEPOWER_WFPSFG__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
#if TRACE
		str += "\nfprintf(pfileTrace, \"WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]: %i\\n\", WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
#endif
		gfd->addWFPSFGEff(str);

		// = Écriture K I
		str = nodeOpPred->KDetermination();
		noiseNode->addSuppressKBits(str);

		// = Écriture Q I
		str = "(pow(2, (-(" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpPred.str() + "*3+2]))))";
		noiseNode->addStepQuantif(str);

		// = Ecriture Mode Quantif I
		str = __NOISEPOWER_WFPSFGQuantif__ + "[" + ossOpPred.str() + "*3+2]";
		noiseNode->addModeQuantif(str);

		noiseNode->addSigneMean(1);
		noiseNode->addConstante(1);

		// = Ecriture K IV
		str  = TAB + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpPred.str() + "*3+2]"; // Si 2 est la sortie
		str += " - " + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]";
		noiseNode->addSuppressKBits(str);

		// = Écriture Q IV
		str = "(pow(2, (-(" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]))))";
		noiseNode->addStepQuantif(str);

		// = Ecriture Mode Quantif IV
		str = __NOISEPOWER_WFPSFGQuantif__ + "[" + ossOpSucc.str() + "*3+"+ ossNumIn.str() +"]";
		noiseNode->addModeQuantif(str);

		noiseNode->addSigneMean(1);
		noiseNode->addConstante(1);
	}
	else { // Data avec pragma
		// = Ecriture WFP_sfg_eff
		WFPEffOperatorDetermination(gfd, nodeOpPred);
		str = TAB + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "] = ";
		str += "min(min (" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpPred.str() + "*3+2], "; // si 2 est la sortie
		str += __NOISEPOWER_InputWD__ + "[" + ossData.str() + "]), ";
		str += TAB + __NOISEPOWER_WFPSFG__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
#if TRACE
		str += "\nfprintf(pfileTrace, \"WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]: %i\\n\", WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
#endif
		gfd->addWFPSFGEff(str);

		// = Ecriture K I
		str = nodeOpPred->KDetermination();
		noiseNode->addSuppressKBits(str);

		// = Écriture Q I
		str = "(pow(2, (-(" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpPred.str() + "*3+2]))))";
		noiseNode->addStepQuantif(str);

		// = Ecriture Mode Quantif I
		str = __NOISEPOWER_WFPSFGQuantif__ + "[" + ossOpPred.str() + "*3+2]";
		noiseNode->addModeQuantif(str);

		noiseNode->addSigneMean(1);
		noiseNode->addConstante(1);

		// = Ecriture K II
		str = " fK(" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpPred.str() + "*3+2]"; // Si 2 est la sortie
		str += " - " + __NOISEPOWER_InputWD__ + "[" + ossData.str() + "])";
		noiseNode->addSuppressKBits(str);

		// = Écriture Q II
		str = "(pow(2, (-(min(" + __NOISEPOWER_InputWD__ + "["+ossData.str()+"], " + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpPred.str() + "*3+2])))))";
		noiseNode->addStepQuantif(str);

		// = Ecriture Mode Quantif II
		str = __NOISEPOWER_InputWDQuantif__ + "[" + ossData.str() + "]";
		noiseNode->addModeQuantif(str);

		noiseNode->addSigneMean(1);
		noiseNode->addConstante(1);

		// = Ecriture K III
		str = " min(" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpPred.str() + "*3+2],";
		str += " " + __NOISEPOWER_InputWD__ + "[" + ossData.str() + "])";
		str += " - " + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]";
		noiseNode->addSuppressKBits(str);

		// = Ecriture Q III
		str = "(pow(2, (-(" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]))))";
		noiseNode->addStepQuantif(str);


		//  = Ecriture Mode Quantif III
		str = __NOISEPOWER_WFPSFGQuantif__ + "[" + ossOpSucc.str() + "*3+"+ ossNumIn.str() +"]";
		noiseNode->addModeQuantif(str);

		noiseNode->addSigneMean(1);
		noiseNode->addConstante(1);
	}
}

/**
 * Pré calcul de la propagation des Wfp effectif cas général2 (modèle à 3 sources de bruit)
 * \author Jean-Charles Naud
 *
 */
void WFPSFGToWFPSFGEff_GeneralCase2(Gfd* gfd, NoeudSrcBruit* noiseNode) {
	NoeudOps * nodeOpPred;
	NoeudData* nodeData;
	bool hasVariableWidth;
	string str;
	std::ostringstream ossBr, ossOpSucc, ossOpPred, ossData, ossNumIn;
	ossBr.str("");
	nodeData = dynamic_cast<NoeudData *> (noiseNode->getElementSucc(0));
	nodeOpPred = dynamic_cast<NoeudOps *> (noiseNode->getElementPred(0));

	// === Save Nums
	hasVariableWidth = nodeData->getVariableWidth();
	ossOpPred << nodeOpPred->getNumSFG();
	ossData << nodeData->getNumCDFG();
	ossNumIn << noiseNode->getEdgePred(0)->getNumInput();

	if (!hasVariableWidth) { // Data sans pragma
		// = Ecriture WFP_sfg_eff
		WFPEffOperatorDetermination(gfd, nodeOpPred);

		// = Écriture K I
		str = nodeOpPred->KDetermination();
		noiseNode->addSuppressKBits(str);

		// = Ecriture Q I
		str = "(pow(2, (-(" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpPred.str() + "*3+2]))))";
		noiseNode->addStepQuantif(str);

		// = Ecriture Mode Quantif I
		str = __NOISEPOWER_WFPSFGQuantif__ + "[" + ossOpPred.str() + "*3+2]";
		noiseNode->addModeQuantif(str);

		noiseNode->addSigneMean(1);
		noiseNode->addConstante(1);

	}
	else { // Data avec pragma
		// = Ecriture WFP_sfg_eff
		WFPEffOperatorDetermination(gfd, nodeOpPred);

		// = Écriture K I
		str = nodeOpPred->KDetermination();
		noiseNode->addSuppressKBits(str);

		// = Ecriture Q I
		str = "(pow(2, (-(" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpPred.str() + "*3+2]))))";
		//}
		noiseNode->addStepQuantif(str);

		// = Ecriture Mode Quantif I
		str = __NOISEPOWER_WFPSFGQuantif__ + "[" + ossOpPred.str() + "*3+2]";
		noiseNode->addModeQuantif(str);

		noiseNode->addSigneMean(1);
		noiseNode->addConstante(1);

		// = Ecriture K II
		// WFP_sfg_eff[P1][2] - tabInputWd[D1]
		str = " fK(" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpPred.str() + "*3+2]"; // Si 2 est la sortie
		str += " - " + __NOISEPOWER_InputWD__ + "[" + ossData.str() + "])";
		noiseNode->addSuppressKBits(str);

		// = Ecriture Q II
		str = "(pow(2, (-(min(" + __NOISEPOWER_InputWD__ + "[" + ossData.str() + "], " + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpPred.str() + "*3+2])))))";
		noiseNode->addStepQuantif(str);

		// = Ecriture Mode Quantif II
		str = __NOISEPOWER_InputWDQuantif__ + "[" + ossData.str() + "]";
		noiseNode->addModeQuantif(str);

		noiseNode->addSigneMean(1);
		noiseNode->addConstante(1);
	}
}

/**
 * Pré calcul de la propagation des Wfp effectif cas général3 (modèle des sources corrélé + gestion rebouclage)
 * \author Jean-Charles Naud
 *
 */
void WFPSFGToWFPSFGEff_GeneralCase3(Gfd* gfd, NoeudSrcBruit* noiseNode) {
	NoeudOps * nodeOpPred, *nodeOpSucc;
	NoeudSrcBruit* nodeSrcBrPred;
	NoeudData* nodeData;
	bool hasVariableWidth;
	string str;
	std::ostringstream ossBr, ossOpSucc, ossOpPred, ossData, ossNumIn;
	ossBr.str("");
	nodeOpSucc = dynamic_cast<NoeudOps *> (noiseNode->getElementSucc(0));
	nodeData = dynamic_cast<NoeudData *> (noiseNode->getElementPred(0));
	nodeSrcBrPred= dynamic_cast<NoeudSrcBruit *> (nodeData->getElementPred(0));
	nodeOpPred = dynamic_cast<NoeudOps *> (nodeSrcBrPred->getElementPred(0));

	// === Save Nums
	hasVariableWidth = nodeData->getVariableWidth();
	ossOpSucc << nodeOpSucc->getNumSFG();
	ossNumIn << noiseNode->getEdgeSucc(0)->getNumInput();
	ossOpPred << nodeOpPred->getNumSFG();
	ossData << nodeData->getNumCDFG();

	if (noiseNode->getEtat() == R2) { // Rebouclage : Les WFP_sfg_effs des noeuds precedants existent (on prend le pire cas)
		if (!hasVariableWidth) { // Data sans pragma

			// = Écriture WFP_sfg_eff
			// WFP_sfg_effs[P2][in] = min(WFP_sfg[P1][2], WFP_sfg[P2][in] )
			str = TAB + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "] = ";
			str += "min(" + __NOISEPOWER_WFPSFG__ + "[" + ossOpPred.str() + "*3+2], "; // si 2 est la sortie (on prend le pire cas)
			str += __NOISEPOWER_WFPSFG__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
#if TRACE
            str += "\nfprintf(pfileTrace, \"WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]: %i\\n\", WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
#endif
			gfd->addWFPSFGEff(str);

			// = Écriture K IV
			// WFP_sfg = WFP_sfg_eff normalement
			str = " " + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpPred.str() + "*3+2]"; // Si 2 est la sortie
			str += " - " + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]";
			noiseNode->addSuppressKBits(str);

			// = Écriture Q IV
			str = "(pow(2, (-(" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]))))";
			noiseNode->addStepQuantif(str);

			str = __NOISEPOWER_WFPSFGQuantif__ + "[" + ossOpSucc.str() + "*3+"+ ossNumIn.str() +"]";
			noiseNode->addModeQuantif(str);

			noiseNode->addSigneMean(1);
			noiseNode->addConstante(1);
		}
		else { // Data avec pragma
			// = Ecriture WFP_sfg_eff
			str = TAB + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "] = ";
			str += "min(min (" + __NOISEPOWER_WFPSFG__ + "[" + ossOpPred.str() + "*3+2], "; // si 2 est la sortie (on prend le pire cas)
			str += __NOISEPOWER_InputWD__ + "[" + ossData.str() + "]), ";
			str += __NOISEPOWER_WFPSFG__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
#if TRACE
           str += "\nfprintf(pfileTrace, \"WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]: %i\\n\", WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
#endif
			gfd->addWFPSFGEff(str);
			// == Ecriture K III
			str = " min(" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpPred.str() + "*3+2],";
			str += " " + __NOISEPOWER_InputWD__ + "[" + ossData.str() + "])";
			str += " - " + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]";
			noiseNode->addSuppressKBits(str);

			// = Ecriture Q III
			str = "(pow(2, (-(" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]))))";
			noiseNode->addStepQuantif(str);

			// = Ecriture Mode Quantif III
			str = __NOISEPOWER_WFPSFGQuantif__ + "[" + ossOpSucc.str() + "*3+"+ ossNumIn.str() +"]";
			noiseNode->addModeQuantif(str);

			noiseNode->addSigneMean(1);
			noiseNode->addConstante(1);

		}
	}
	else { // Pas de rebouclage, on fait tout et (on prend pas le pire cas)
		if (!hasVariableWidth) { // Data sans pragma
			// = Ecriture WFP_sfg_eff
			str = TAB + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "] = ";
			str += "min(" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpPred.str() + "*3+2], "; // si 2 est la sortie
			str += __NOISEPOWER_WFPSFG__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
#if TRACE
           str += "\nfprintf(pfileTrace, \"WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]: %i\\n\", WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
#endif
			gfd->addWFPSFGEff(str);

			// = Ecriture K IV
			str = __NOISEPOWER_WFPSFGEff__ + "[" + ossOpPred.str() + "*3+2]"; // Si 2 est la sortie
			str += " - " + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]";
			noiseNode->addSuppressKBits(str);

			// = Écriture Q IV
			str = "(pow(2, (-(" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]))))";
			noiseNode->addStepQuantif(str);

			// = Ecriture Mode Quantif IV
			str = __NOISEPOWER_WFPSFGQuantif__ + "[" + ossOpSucc.str() + "*3+"+ ossNumIn.str() +"]";
			noiseNode->addModeQuantif(str);

			noiseNode->addSigneMean(1);
			noiseNode->addConstante(1);
		}
		else { // Data avec pragma
			// = Ecriture WFP_sfg_eff
			str = TAB + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "] = ";
			str += "min(min (" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpPred.str() + "*3+2], "; // si 2 est la sortie
			str += __NOISEPOWER_InputWD__ + "[" + ossData.str() + "]), ";
			str += __NOISEPOWER_WFPSFG__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
#if TRACE
         str += "\nfprintf(pfileTrace, \"WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]: %i\\n\", WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
#endif
			gfd->addWFPSFGEff(str);

			// == Ecriture K III
			str = " min(" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpPred.str() + "*3+2],";
			str += " " + __NOISEPOWER_InputWD__ + "[" + ossData.str() + "])";
			str += " - " + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]";
			noiseNode->addSuppressKBits(str);

			// = Écriture Q III
			str = "(pow(2, (-(" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]))))";
			noiseNode->addStepQuantif(str);

			// = Ecriture Mode Quantif II
			str = __NOISEPOWER_WFPSFGQuantif__ + "[" + ossOpSucc.str() + "*3+"+ ossNumIn.str() +"]";
			noiseNode->addModeQuantif(str);

			noiseNode->addSigneMean(1);
			noiseNode->addConstante(1);
		}
	}
}
/**
 * Pré calcul de la propagation des Wfp effectif cas des entrées
 * \author Jean-Charles Naud
 *
 */
void WFPSFGToWFPSFGEff_InputCase(Gfd* gfd, NoeudSrcBruit* noiseNode) {
	NoeudOps *nodeOpSucc;
	NoeudData* nodeData;
	bool hasVariableWidth;
	string str;
	std::ostringstream ossBr, ossOpSucc, ossData, ossNumIn;

	nodeOpSucc = dynamic_cast<NoeudOps *> (noiseNode->getElementSucc(0));
	nodeData = dynamic_cast<NoeudData *> (noiseNode->getElementPred(0));
	//cout<<"cas Input pour le noeud "<<noiseNode->getName()<<endl;
	// === Save Num
	hasVariableWidth = nodeData->getVariableWidth();
	ossOpSucc << nodeOpSucc->getNumSFG();
	ossNumIn << noiseNode->getEdgeSucc(0)->getNumInput();
	ossData << nodeData->getNumCDFG();

	if (!hasVariableWidth) {
		// WFP_sfg_eff[P2][j] = WFP[P2][j];
		//  = Ecriture WFP effectif
		str = __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]";
		str += " = " + __NOISEPOWER_WFPSFG__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "];";
#if TRACE
       str += "\nfprintf(pfileTrace, \"WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]: %i\\n\", WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
#endif
		gfd->addWFPSFGEff(str);
		// = Écriture K III
		str = "1000";
		noiseNode->addSuppressKBits(str);

		// = Écriture Q III
		str = "(pow(2, (-(" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]))))";
		noiseNode->addStepQuantif(str);

		// = Ecriture Mode Quantif III
		str = __NOISEPOWER_WFPSFGQuantif__ + "[" + ossOpSucc.str() + "*3+"+ ossNumIn.str() +"]";
		noiseNode->addModeQuantif(str);

		noiseNode->addSigneMean(1);
		noiseNode->addConstante(1);
	}
	else {
		// WFP_sfg_eff[P2][j] = min(WFP[P2][j], tabInputWd[xDataP"]);
		//  = Ecriture WFP effectif
		str = TAB + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]";
		str += " = min(" + __NOISEPOWER_WFPSFG__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "],";
		str += " " + __NOISEPOWER_InputWD__ + "[" + ossData.str() + "]);";
#if TRACE
      str += "\nfprintf(pfileTrace, \"WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]: %i\\n\", WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
#endif
		gfd->addWFPSFGEff(str);

		// = Écriture K IV
		str = __NOISEPOWER_InputWD__ + "[" + ossData.str() + "]";
		str += " - " + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]";
		noiseNode->addSuppressKBits(str);


		// = Écriture Q IV
		str = "(pow(2, (-(" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]))))";
		noiseNode->addStepQuantif(str);

		// = Ecriture Mode Quantif IV
		str = __NOISEPOWER_WFPSFGQuantif__ + "[" + ossOpSucc.str() + "*3+"+ ossNumIn.str() +"]";
		noiseNode->addModeQuantif(str);

		noiseNode->addSigneMean(1);
		noiseNode->addConstante(1);
	}
}

/**
 * Pré calcul de la propagation des Wfp effectif cas des sorties
 * \author Jean-Charles Naud
 *
 */
void WFPSFGToWFPSFGEff_OutputCase(Gfd* gfd, NoeudSrcBruit* noiseNode) {
	NoeudOps *nodeOpPred;
	NoeudData* nodeData;
	bool hasVariableWidth;
	string str;
	std::ostringstream ossBr, ossOpPred, ossData, ossNumIn;
	//cout<<"cas output pour le noeud "<<noiseNode->getName()<<endl;
	nodeData = dynamic_cast<NoeudData *> (noiseNode->getElementSucc(0));
	nodeOpPred = dynamic_cast<NoeudOps *> (noiseNode->getElementPred(0));

	// === Save Nums
	hasVariableWidth = nodeData->getVariableWidth();
	ossOpPred << nodeOpPred->getNumSFG();
	ossData << nodeData->getNumCDFG();

	if (!hasVariableWidth) { // Data sans pragma
		// = Ecriture WFP_sfg_eff
		WFPEffOperatorDetermination(gfd, nodeOpPred);

		// = Écriture K I
		str = nodeOpPred->KDetermination();
		noiseNode->addSuppressKBits(str);

		// = Écriture Q I
		str = "(pow(2, (-(" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpPred.str() + "*3+2]))))";
		noiseNode->addStepQuantif(str);

		// = Ecriture mode quantif I
		str = __NOISEPOWER_WFPSFGQuantif__ + "[" + ossOpPred.str() + "*3+2]";
		noiseNode->addModeQuantif(str);

		noiseNode->addSigneMean(1);
		noiseNode->addConstante(1);
	}
	else { // Data avec pragma

		// = Ecriture WFP_sfg_eff
		WFPEffOperatorDetermination(gfd, nodeOpPred);

		// = Écriture K I
		str = nodeOpPred->KDetermination();
		noiseNode->addSuppressKBits(str);

		// = Écriture Q I
		str = "(pow(2, (-(" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpPred.str() + "*3+2]))))";
		noiseNode->addStepQuantif(str);

		// = Ecriture mode quantif I
		str = __NOISEPOWER_WFPSFGQuantif__ + "[" + ossOpPred.str() + "*3+2]";
		noiseNode->addModeQuantif(str);

		noiseNode->addSigneMean(1);
		noiseNode->addConstante(1);

		// = Écriture K II
		str = " fK(" + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpPred.str() + "*3+2]";
		str += " - " + __NOISEPOWER_InputWD__ + "[" + ossData.str() + "])";
		noiseNode->addSuppressKBits(str);

		// = Écriture Q II
		str = "(pow(2, (-(min(" + __NOISEPOWER_InputWD__ + "[" + ossData.str() + "], " + __NOISEPOWER_WFPSFGEff__ + "[" + ossOpPred.str() + "*3+2])))))";
		noiseNode->addStepQuantif(str);

		// = Ecriture Mode Quantif II
		str = __NOISEPOWER_InputWDQuantif__ + "[" + ossData.str() + "]";
		noiseNode->addModeQuantif(str);

		noiseNode->addSigneMean(1);
		noiseNode->addConstante(1);
	}
}

/**
 * Calcul du Wfp effectif pour une opération
 * \author Jean-Charles Naud
 */

void WFPEffOperatorDetermination(Gfd* gfd, NoeudOps* opsNode) {
	string str;

	str = TAB;
	str += "// " + typeEtatConvToStr(opsNode->getEtat());
	gfd->addWFPSFGEff(str);
	if (opsNode->getEtat() == VISITE || opsNode->getEtat() == R4) {
		// === Expression des WFPeff présent aux sorties des opérations
        str = opsNode->WFPEffDetermination();
#if TRACE
        std::ostringstream ossOps;
        ossOps << opsNode->getNumSFG(); //int to str
        str += "\nfprintf(pfileTrace, \"WFP_sfg_eff[" + ossOps.str() + "*3+2]: %i\\n\", WFP_sfg_eff[" + ossOps.str() + "*3+2]);";
#endif
		gfd->addWFPSFGEff(str);
		if (opsNode->getEtat() == VISITE) {
			opsNode->setEtat(NONVISITE);
		}
		if (opsNode->getEtat() == R2) {
			opsNode->setEtat(R4);
		}
	}
}


void NoisePowerExpressionLibraryCompilation(){
	string command;
	ostringstream oss;

	// Copy the makefile and files used to compile the noise function library
	ifstream inmakefile(string(ProgParameters::getResourcesDirectory() + COMPUTEPB_DIR + "Makefile").c_str(), ios::binary);
	ofstream outmakefile(string(ProgParameters::getOutputGenDirectory() + "Makefile").c_str(), ios::binary);
	if(!inmakefile.is_open() || !outmakefile.is_open()){
		trace_error("Error during the noise function compilation process");
		trace_debug("Error during the copy of the Makefile used to compile the noise function");
		exit(1);
	}
	outmakefile << inmakefile.rdbuf();
	inmakefile.close();
	outmakefile.close();

	ifstream intoolheader(string(ProgParameters::getResourcesDirectory() + COMPUTEPB_DIR + "Tool.h").c_str(), ios::binary);
	ofstream outtoolheader(string(ProgParameters::getOutputGenDirectory() + "Tool.h").c_str(), ios::binary);
	if(!intoolheader.is_open() || !outtoolheader.is_open()){
		trace_error("Error during the noise function compilation process");
		trace_debug("Error during the copy of the Tool header used to compile the noise function");
		exit(1);
	}
	outtoolheader << intoolheader.rdbuf();
	intoolheader.close();
	outtoolheader.close();

	ifstream intoolsource(string(ProgParameters::getResourcesDirectory() + COMPUTEPB_DIR + "Tool.c").c_str(), ios::binary);
	ofstream outtoolsource(string(ProgParameters::getOutputGenDirectory() + "Tool.c").c_str(), ios::binary);
	if(!intoolsource.is_open() || !outtoolsource.is_open()){
		trace_error("Error during the noise function compilation process");
		trace_debug("Error during the copy of the Tool source used to compile the noise function");
		exit(1);
	}
	outtoolsource << intoolsource.rdbuf();
	intoolsource.close();
	outtoolsource.close();

	oss << ProgParameters::getNbThread();
	command = "cd " + ProgParameters::getOutputGenDirectory() +";make -j " + oss.str();
	trace_debug("Commande exécutée : \n");
	trace_debug("%s\n",command.data());
	if(system(command.data()) != 0){
		trace_error("Error during the noise function compilation process");
		trace_debug("Error during the compilation");
		exit(0);
	}
}

