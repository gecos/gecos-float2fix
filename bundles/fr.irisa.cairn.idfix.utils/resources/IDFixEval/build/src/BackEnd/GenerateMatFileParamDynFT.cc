/**
 *  \file GenerateMatFileParamFT.cc
 *  \author cloatre
 *  \date   23 févr. 2009
 */
// === Bibliothèques standard
#include <algorithm>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>

#include "graph/NoeudGraph.hh"
#include "graph/tfg/EdgeGFT.hh"
#include "graph/tfg/GFT.hh"
#include "graph/tfg/NodeGFT.hh"
#include "utils/graph/cycledismantling/GraphPath.hh"
#include "utils/Tool.hh"

// === Bibliothèques non standard

// === Namespaces
using namespace std;

/**
 * Methodes principale pour generer le fichier matlab ParamDynFT.m
 *
 *  \param  pathFile: chemin du fichier matlab a creer
 *  \return bool: false if no node in graph
 *
 *  \ingroup T31
 *  by Nicolas Simon le 19/07/2010
 */

bool Gft::genereMatFileParamFTDyn(string pathFile) {
	bool ret = true;
	fstream FileMat_pf;
	fstream FileTxt_pf;
	fstream FileDyn_pf;
	NodeGraphContainer::iterator it;

	string tempPathFileConstant;
	string tempPathFile;
	string ligne;

	tempPathFileConstant = ProgParameters::getOutputGenDirectory();

	tempPathFileConstant += CONSTANT_VAR_FILENAME;
	tempPathFile += pathFile;

	// Personne ne set le numéro des edges, donc on va le faire ici
	int currentNum = 0;
	for (NodeGraphContainer::const_iterator it = tabNode->begin(); it != tabNode->end(); it++) {
		for (int i = (*it)->getNbSucc(); i > 0; i--) {
			if((*it)->getTypeNodeGraph() != INIT && (*it)->getElementSucc(i - 1)->getTypeNodeGraph() != FIN)
				(*it)->getEdgeSucc(i - 1)->setNumEdge(++currentNum);
		}
	}

	if (this->nbNode < 3) {
		trace_warn("il n'y a pas de noeuds dans le graphe");
		return false;
	}
	else {
		FileMat_pf.open(tempPathFile.c_str(), fstream::out); //ouverture fichier en mode ecriture
		assert(FileMat_pf.is_open()); //Erreur lors de l'ouverture du fichier ParamDynFT.m

		//recopie de la valeur des constantes de l'application
		assert(!tempPathFileConstant.empty());
		FileTxt_pf.open(tempPathFileConstant.c_str(), fstream::in); //ouverture fichier en mode ecriture
		assert(FileTxt_pf.is_open());  // erreur ouverture fichier ConstantVariable

		while (getline(FileTxt_pf, ligne)) {
			FileMat_pf << ligne << endl;
		}

		FileTxt_pf.close();

		// Information sur le graph traité
		FileMat_pf << NOMBRE_SOURCE << " = " << this->nbInput << " ;" << endl;
		FileMat_pf << NOMBRE_SORTIE << " = " << this->nbOutput << " ;" << endl;
		FileMat_pf << "nombreNoeudInter = " <<this->getNbVarInt()<<" ;"<<endl;
		FileMat_pf << NOMBRE_NOEUD << " = " << this->nbNode - 2 << " ;" << endl;

		// Ajout du Tableau contenant les noeuds sorties 
		FileMat_pf << TAB_NUMERO_SORTIE << "= [ ";
		for (int i = 0; i < this->nbOutput; i++) {
			FileMat_pf << this->getOutputGf(i)->getNumero() - 1 << " "; //lclDecremente
		}
		FileMat_pf << "];" << endl;
		
		// Ajout du Tableau contenant les noeuds entrées 
		FileMat_pf << TAB_NUMERO_ENTREE << "= [ ";
		for (int i = 0; i < this->nbInput; i++) {
			FileMat_pf << this->getInputGf(i)->getNumero() - 1 << " "; //lclDecremente
		}
		FileMat_pf << "];" << endl;
	
		// Ajout du tableau contenant les noeuds intermediaires
		FileMat_pf << "TabNumeroInter = [ ";
	   	for(int i = 0 ; i<this->getNbVarInt() ; i++){
			FileMat_pf << this->getVarIntGf(i)->getNumero() - 1 << " ";
		}
		FileMat_pf << "];" <<endl<<endl;

		FileMat_pf << "%-----  Ajout des dynamics des entrees -----------" << endl;
		this->genereInputDyn(FileMat_pf); // Genereration de la partie dynamique des entrée en fonction du type de calcul utilisé

		FileMat_pf << "%-----  Definition des listSucc de chaque Noeud -----------" << endl;
		// Successeur de chaque noeud
		NodeGraphContainer::iterator it2;
		it2 = this->tabNode->begin();
		it2++;
		it2++;

		for (int i = 2; i < this->getNbNode(); i++) {

			if (!dynamic_cast<NoeudGFT *> (*it2)->isNodeSink()) {
				FileMat_pf << "Noeud(" << (*it2)->getNumero() - 1 << ")" << ".Suc= [ ";
				for (int j = 0; j < (*it2)->getNbSucc(); j++) {
					if ((*it2)->getElementSucc(j)->getNumero() != (*it2)->getNumero()) {
						FileMat_pf << (*it2)->getElementSucc(j)->getNumero() - 1 << " ";
					}
				}
				FileMat_pf << "];" << endl;
			}
			it2++;
		}
		FileMat_pf << endl;

		FileMat_pf << "%-----  Definition  des fonctions de transfert partielles -----------" << endl;
		this->genereFTPartielles(FileMat_pf);
		FileMat_pf << "%-----  Definition  des fonctions de transfert globales -----------" << endl;
		this->genereFTGlobalesDyn(FileMat_pf);


		FileMat_pf << "%**********fin **********" << endl << endl;
		FileMat_pf.close();
	}

	return ret;

}
/*
 * Generation de la partie definissant la dynamique des entrée du ParamDynFT en fonction des différents type de calcul
 *
 *	Nicolas Simon 22/04/11
 */
 
void Gft::genereInputDyn(fstream & outputMatFile){
	NoeudGFT* tmpNode;
	NoeudData* refParam;
	fstream overflow_file;
	string ligne;

	if(ProgParameters::getDynProcess() == "interval"){
		//Ajout de la dynamique pour les NoeudSrc
		DataDynamic dataDynamic;

		for (int i = 0; i < this->nbInput; i++) {
			tmpNode = dynamic_cast<NoeudGFT *> (getInputGf(i));
			refParam = dynamic_cast<NoeudData*> (tmpNode->getRefParamSrc());
			assert(refParam != NULL); // si assert non valide => Noeud input GFT/GFL sans lien avec un noeudSrc

			dataDynamic = refParam->getDataDynamic();
			outputMatFile << "Noeud(" << tmpNode->getNumero() - 1 << ").Dyn = [ " << dataDynamic.valMin << " " << dataDynamic.valMax << " ];" << endl;
		}
	}
	else if(ProgParameters::getDynProcess() == "overflow-proba"){
		
		outputMatFile << "load('" <<ProgParameters::getOverflowPathFile().data()<<"');"<<endl;
		
		for(int i = 0; i<this->nbInput; i++) {
			tmpNode = dynamic_cast<NoeudGFT *> (getInputGf(i));
			refParam = dynamic_cast<NoeudData*> (tmpNode->getRefParamSrc());
			assert(refParam != NULL); // si assert non valide => Noeud input GFT/GFL sans lien avec un noeudSrc
			
			outputMatFile << "Noeud(" << tmpNode->getNumero() - 1 << ").DynOver = "<< refParam->getOriginalName().data() << ";" <<endl;
			outputMatFile << "Noeud(" << tmpNode->getNumero() - 1 << ").Dyn = [ "<< refParam->getDataDynamic().valMin <<", "<< refParam->getDataDynamic().valMax <<" ];"<<endl;
		}
		outputMatFile << endl;
		
		outputMatFile << "%-----  Ajout des Probabilités de débordement des noeuds  -----------" << endl;
		for(int i = 0; i<this->nbNode; i++) {
			tmpNode = dynamic_cast<NoeudGFT*> (getElementGf(i));
			if(tmpNode == NULL) // Init ou fin
				continue;

			if(tmpNode->getRefParamSrc()->getTypeNodeGFD() == DATA){ // Cela peut diriger vers un noeud OPS (exemple : PHI)
				refParam = dynamic_cast<NoeudData*> (tmpNode->getRefParamSrc());
				assert(refParam != NULL);

				outputMatFile << "Noeud(" <<tmpNode->getNumero() - 1 << ").Deb = "<< refParam->getProbDeb()<< ";" <<endl;
			}
		}
	}
	else {
		trace_error("Type de calcul de la dynamique non défini");
		exit(-1);
	}
	outputMatFile << endl;
}


/**
 * Methodes pour generer les fonctions de transferts globales entre chaque noeud et les sources (utilisé pour la partie dynamique)
 *
 */
void Gft::genereFTGlobalesDyn(fstream & outputMatFile) {
	NoeudGFT *noeudNonSource = NULL;
	NoeudGFT *noeudSource = NULL;
	GraphPath *listeDesCircuit = NULL;
	bool tousCircuitsTraites = false;
	string FTGlob, FTGlobInit, tempString;
	bool ftHasElement;
	//ostringstream 

	for (int j = 0; j < this->nbInput; j++)   
	{
		noeudSource = dynamic_cast<NoeudGFT *> (this->getInputGf(j)); 

		ostringstream num2;
		num2 << noeudSource->getNumero() - 1;

		for (int i = 2; i < this->nbNode; i++) {
			noeudNonSource = dynamic_cast<NoeudGFT *> (this->getElementGf(i)); 
			if (!noeudNonSource->isNodeSource()) { 
				ostringstream num1;
				num1 << noeudNonSource->getNumero() - 1; 

				if (num1.str() != num2.str()) { //pour eviter Hg(3,3) par ex
					FTGlob = "Hg(" + num2.str() + "," + num1.str() + ").tf = "; //Hg(6,1).tf =
					ftHasElement = false;

					listeDesCircuit = noeudNonSource->enumerationCheminVersSource(noeudSource);
					GraphPath *debut = listeDesCircuit;

					if (listeDesCircuit != NULL) {
						while (!tousCircuitsTraites) {
							if (listeDesCircuit->pathSize() > 1) {
								tempString = listeDesCircuit-> getExpressionFTGlobalePourMatlab(noeudSource-> getNumero() - 1);
								//on calcule l'expression d'un chemin
								if ((tempString.size() != 0) && ftHasElement) {
									FTGlob = FTGlob + " + " + tempString; //s'il y a d'autre chemin, on somme les FT
								}
								else {
									FTGlob = FTGlob + tempString; //premier element
								}
								ftHasElement = true;
							}
							else {
								tempString = "";
							}

							if (listeDesCircuit->Suiv != NULL) {
								listeDesCircuit = listeDesCircuit->Suiv;
							}
							else {
								tousCircuitsTraites = true;
								if (ftHasElement == false) {
									FTGlob += "0;"; //cas de noeud isole
								}
								else {
									FTGlob += ";";
								}
							}
						}
						tousCircuitsTraites = false; //raz flag
					}
					else { //pas de cycle
						FTGlob += "0;"; //cas ou on lie une sortie a une entree n'appartenant pas a son graphe
					}
					listeDesCircuit = NULL;

					delete debut;
					outputMatFile << FTGlob << endl; //on ecrit le resultat dans le fichier
				}
			}
		}
	}
}

