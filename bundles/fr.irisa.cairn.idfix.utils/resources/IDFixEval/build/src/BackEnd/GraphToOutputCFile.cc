/**
 * \file GraphToOutputCFile.cc
 * \author cloatre
 * \date   5 nov. 2008
 * This file contains all function to navigate in a GFD graph in order to generate a C file using
 * floating point or fixed point representation
 */

// === Bibliothèques .hh associe
#include <sstream>

#include "graph/dfg/datanode/NoeudData.hh"
#include "graph/dfg/Gfd.hh"
#include "graph/dfg/operatornode/NoeudOps.hh"
#include "graph/toolkit/TypeVar.hh"

// === Namespaces
using namespace std;


/*
 *	Determine and tag (TypeSimu) node which split the signal and noise part of the graph
 *
 *	Nicolas Simon
 *	17/05/11
 */
void Gfd::addTypeSimuNode(){
	NoeudOps* nOps;
	NoeudData* nPred1;
	NoeudData* nPred2;
	for(int i = 0 ; i<this->getNbNode() ; i++){
		nOps = dynamic_cast<NoeudOps*> (this->getElementGf(i));
		if(nOps == NULL)
			continue;

		assert(nOps->getNbPred() < 3); // Aucune opération à 3 opérande gérée pour le moment
		if(nOps->getNbPred() == 2){
			nPred1 = dynamic_cast<NoeudData*> (nOps->getElementPred(0));
			nPred2 = dynamic_cast<NoeudData*> (nOps->getElementPred(1));
			assert(nPred1 != NULL && nPred2 != NULL);

			if(nPred1->getTypeSignalBruit() == BRUIT && nPred2->getTypeSignalBruit() == SIGNAL && !nPred2->isNodeSource())
			{
				nPred2->setTypeSimu(true);
			}
			else if(nPred1->getTypeSignalBruit() == SIGNAL && nPred2->getTypeSignalBruit() == BRUIT && !nPred1->isNodeSource())
			{
				nPred1->setTypeSimu(true);
			}
		}
	}
}

/*
 *	Detecte tout les noeuds typeSimu est lance une récursivité sur ce noeud pour obtenir son opération arithmétique qui permet d'obtenir sa valeur en fonction des variables simulées
 *
 *	Nicolas Simon
 *	13/07/11
 *
 */
void Gfd::genArithmeticOperation(string pathFile){
	NoeudData* nData;
	NodeGraphContainer::iterator it;
	fstream matFile, FileTxt_pf;

	matFile.open(pathFile.data(), fstream::out);
	assert(matFile.is_open());

	matFile << "\% Ecriture des constantes" << endl;
	for(int i = 0 ; i < this->getNoeudInit()->getNbSucc() ; i++){
		nData = dynamic_cast<NoeudData*> (this->getNoeudInit()->getElementSucc(i));
		assert(nData != NULL);
		if(nData->isNodeConst())
			matFile << nData->getName().data() << " = " << nData->getValeur() << ";" << endl;
	}
	matFile<<endl<<endl;

	for(it = this->tabNode->begin() ; it != this->tabNode->end() ; it++){
		nData = dynamic_cast<NoeudData*> (*it);
		if(nData == NULL)
			continue;

		if(nData->getTypeSimu()){
			nData->genArithmeticOperation(matFile);
		}
	}

	matFile.close();
}

/*
 *	 Recursivité sur les noeuds Data permettant de determiner l'expression arithmétique d'un typeSimu.	
 *
 *	Nicolas Simon
 *	13/07/11
 */
NoeudData* NoeudData::genArithmeticOperation(fstream & matFile){
	NoeudOps* nOps;
	assert(this->getNbPred() == 1);

	if(this->isNodeSource() || this->getNumAffect() != -1 || this->isNodeConst()){
		return this;
	}
	else{
		nOps = dynamic_cast<NoeudOps*> (this->getElementPred(0));
		assert(nOps != NULL);
		return nOps->arithmeticOperationGeneration(matFile);
	}
}
