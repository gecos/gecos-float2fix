/*
 * XmlDyn.cc
 *
 *  Created on: Jul 20, 2010
 *      Author: nicolas
 */

#include <stdlib.h>
#include <math.h>
#include <string>
#include <iostream>
#include <stdio.h>

#include "dynamic/XmlDyn.hh"
#include "graph/lfg/GFL.hh"
#include "graph/lfg/NoeudGFL.hh"

XmlDyn::XmlDyn() {
	NoeudCourant = NULL;
}

XmlDyn::~XmlDyn() {
	list<XmlDynNode *>::iterator IndexListNode;
	for (IndexListNode = this->ListNode.begin(); IndexListNode != this->ListNode.end();) {
		delete *IndexListNode++;
	}

}

void XmlDyn::addNode(char *Id) {
	NoeudCourant = new XmlDynNode(Id);
	this->ListNode.push_back(NoeudCourant);
}

XmlDynNode::XmlDynNode(char *Id) {

	//this->Nom = new string(Nom);

	this->Num = atoi(Id);
}

XmlDynNode::~XmlDynNode() {
	list<XmlDynAtt *>::iterator it;
	for (it = ListAtt.begin(); it != ListAtt.end();) {
		delete (*it++);
	}
}

void XmlDyn::addAtt(char *Key, char *Value) {
	NoeudCourant->addAtt(Key, Value);
}

void XmlDynNode::addAtt(char *Key, char *Value) {
	XmlDynAtt *att = new XmlDynAtt(Key, Value);
	this->ListAtt.push_back(att);
}

XmlDynAtt::XmlDynAtt(char *Key, char *Value) {
	this->Key = Key;
	this->Value = atof(Value);
}

XmlDynAtt::~XmlDynAtt() {

}

void XmlDyn::Affiche() {
	list<XmlDynNode *>::iterator IndexListNode;
	for (IndexListNode = this->ListNode.begin(); IndexListNode != this->ListNode.end(); IndexListNode++) {
		cout << "ID : " << (*IndexListNode)->getNum() << endl;
		(*IndexListNode)->Affiche();
	}

}

void XmlDynNode::Affiche() {
	list<XmlDynAtt *>::iterator IndexListAtt;
	for (IndexListAtt = this->ListAtt.begin(); IndexListAtt != this->ListAtt.end(); IndexListAtt++) {
		cout << "Key : " << (*IndexListAtt)->getKey() << endl;
		cout << "Value : " << (*IndexListAtt)->getValue() << endl << endl;
	}
}

void XmlDyn::XmlDyn2GFL(Gfl * Graph) {

	list<XmlDynNode *>::iterator IndexListNode;
	list<XmlDynAtt *>::iterator it;
	NoeudGFL *NGfl;
	DataDynamic Dyn;
	int Numero;

	for (IndexListNode = this->ListNode.begin(); IndexListNode != this->ListNode.end(); IndexListNode++) {
		
		Numero = 1 + (*IndexListNode)->getNum();
		NGfl = ((NoeudGFL *) Graph->getElementGf(Numero));
		assert(NGfl->getRefParamSrc() != NULL); 
		if(NGfl->getRefParamSrc()->getTypeNodeGFD() == DATA){
			/*
			 * Bug sur les iterator de la liste d'attribut non reglé
			 * Utilisation d'une méthode d'accès au donné du xml pas très crédible
			 * 17/03/11 NS
			 */

			/* it = (*IndexListNode)->GetListAtt().begin();
			   for(; it != (*IndexListNode)->GetListAtt().end() ; it++){
			   if ((*it)->getKey() == "DynMax")
			   Dyn.valMax = (*it)->getValue();
			   else if ((*it)->getKey() == "DynMin")
			   Dyn.valMin = (*it)->getValue();
			   else
			   cerr<<"Mauvais attribut dans le xml"<<endl;exit(-1);
			   }*/
			//if (!NGfl->isNodeSink()) {
			Dyn.valMax = (*IndexListNode)->GetListAtt().front()->getValue();
			Dyn.valMin = (*IndexListNode)->GetListAtt().back()->getValue();
			// Le noeud GFL n'a pas de lien avec un Noeud Src

			if (NGfl->getTypeNoeudGFL() == VAR_GFL && NGfl->getRefParamSrc(1) != NULL  ) {
				((NoeudData*)NGfl->getRefParamSrc(0))->setDataDynamic(Dyn);
				((NoeudData*)NGfl->getRefParamSrc(1))->setDataDynamic(Dyn);
			}
			else {
				((NoeudData*)NGfl->getRefParamSrc())->setDataDynamic(Dyn);
			}
		}
		//}
	}
}
