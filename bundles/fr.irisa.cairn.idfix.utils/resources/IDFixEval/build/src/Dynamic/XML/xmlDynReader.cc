// === Bibliothèques standard
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdio.h>


// === Bibliothèques non standard
#include "utils/Tool.hh"
#include "dynamic/XmlDyn.hh"
#include "graph/lfg/GFL.hh"
// g++ -o test test.o -L/usr/lib -lxml2 -lz -liconv -lm

#ifndef XML_LIB
#define XML_LIB

// === Bibliothèques non standard (XML)
#include <libxml/xmlreader.h>
#include <libxml/SAX2.h>
#include <libxml/xmlstring.h>

#endif

/* Machine d'etat pour le parser */

typedef enum {
	ETAT_DEBUT, ETAT_DOCUMENT_IN, ETAT_GRAPH_IN, ETAT_NODE_IN, ETAT_ATT_NODE_IN, ETAT_DOCUMENT_OUT, ETAT_GRAPH_OUT, ETAT_ERROR
} TypeEtatXml;

/* Variables globales utiles */

/**
 * \var 	EtatXml
 * \brief 	Etat courant de la machine d'etats pour parser le fichier XML
 * \ingroup InterfaceXML */
TypeEtatXml EtatXmlDyn;

/**
 * \var 	PtrXmlGraph
 * \brief 	Pointeur sur le graph genere lors de l'analyse du fichier XML
 * \ingroup InterfaceXML */
XmlDyn *PtrXmlDyn;

/**
 * \var 	myhandler
 * \brief 	Handler sur le fichier XML
 * \ingroup InterfaceXML */
xmlSAXHandler myhandlerdyn;

/**
 * \var 	myhandler
 * \brief 	pointeur de contexte pour l'analyse du fichier XML
 * \ingroup InterfaceXML */
xmlParserCtxtPtr myctxdyn;

/* Mise en place du handler et des callbacks */

/**
 * 	typeVarFonction activee lors de la detection du debut du fichier XML
 *
 */
void userStartDocumentdyn(void * /*ctx */) {

	//printf("Document start\n");
	if (EtatXmlDyn == ETAT_DEBUT) {
		EtatXmlDyn = ETAT_DOCUMENT_IN;
	};

}

/**
 * 	typeVarFonction activee lors de la detection du debut d'un element lors de l'analyse du fichier XML
 *
 */
void userStartElementdyn(void * /*ctx */, const xmlChar * fullname, const xmlChar ** atts) {

	switch (EtatXmlDyn) {

	//On entre dans le document
	case ETAT_DOCUMENT_IN: {
		if (xmlStrEqual(fullname, BAD_CAST("Dyn")) == 1) {

			PtrXmlDyn = new XmlDyn();
			EtatXmlDyn = ETAT_GRAPH_IN;
		}
		else if (xmlStrEqual(fullname, BAD_CAST("dyn")) == 1) {

			PtrXmlDyn = new XmlDyn();
			EtatXmlDyn = ETAT_GRAPH_IN;
		}
		else {
			EtatXmlDyn = ETAT_DOCUMENT_OUT; //absence de graphe donc sortie du document
		};
	}
		break;

	case ETAT_GRAPH_IN: {
		if (xmlStrEqual(fullname, BAD_CAST("node")) == 1) {
			EtatXmlDyn = ETAT_NODE_IN;
			PtrXmlDyn->addNode((char *) atts[1]); //(char * Nom, char* Id)
		}
		else {
			EtatXmlDyn = ETAT_ERROR;
		}
	}
		break;

	case ETAT_NODE_IN: {

		if (xmlStrEqual(fullname, BAD_CAST("attr")) == 1) {
			EtatXmlDyn = ETAT_ATT_NODE_IN;

			PtrXmlDyn->addAtt((char *) atts[1], (char *) atts[3]);
		}
		else {
			EtatXmlDyn = ETAT_ERROR;
			trace_error("unknown label: %s", fullname);
		}
	}
		break;
		//Par convention on passe a l etat document out si il y a un probleme

		//On entre dans le graphe
		//le passage a l etat 2 n est pas necessaire

		//Tous les cas non connus
	default: {
		trace_error("start element: %s\n", fullname);

	}
	}
}

/**
 * 	typeVarFonction activ�e lors de la d�tection de la fin d'un �l�ment lors de l'analyse du fichier XML
 *
 */
void userEndElementdyn(void * /*ctx */, const xmlChar * fullname) {

	//printf("Element end\n");
	//printf("%s\n",fullname);

	switch (EtatXmlDyn) {

	//On sort du document vide
	case ETAT_DOCUMENT_IN: {
		EtatXmlDyn = ETAT_ERROR;
		trace_error("No graph read: %s", fullname);
	}
		break;

	case ETAT_GRAPH_IN: {
		EtatXmlDyn = ETAT_GRAPH_OUT;
	}
		break;

	case ETAT_NODE_IN: {
		EtatXmlDyn = ETAT_GRAPH_IN;
	}
		break;

	case ETAT_ATT_NODE_IN: {
		EtatXmlDyn = ETAT_NODE_IN;
	}
		break;

		//Tous les cas non connus
	default: {
		trace_error("end element: %s\n", fullname);

	}
	}
}

/**
 * 	typeVarFonction activ�e lors de la d�tection du d�but du fichier XML
 *
 */
void userEndDocumentdyn(void * /*ctx */) {

	//printf("Document end\n");
	if (EtatXmlDyn == ETAT_GRAPH_OUT) {
		EtatXmlDyn = ETAT_DEBUT;
	};

}

/**
 * 	typeVarFonction activ�e au cours du traitement d'un �l�ment
 *
 */
void userCharactersdyn(void * /*ctx */, const xmlChar * /*ch */, int /*len */) {

}

void userIgnorableWhitespacedyn(void * /*ctx */, const xmlChar * /*ch */, int /*len */) {

	//printf("WhiteSpace\n");

}

/**
 * 	Parsing the a XML file word which contain dynamic of different nodes
 *
 *	\param FichierXML : nom du fichier
 */
void Gfl::xmlDynReader(const char *FichierXML) {
	myhandlerdyn.startDocument = userStartDocumentdyn;
	myhandlerdyn.startElement = userStartElementdyn;
	myhandlerdyn.endDocument = userEndDocumentdyn;
	myhandlerdyn.endElement = userEndElementdyn;
	myhandlerdyn.characters = userCharactersdyn;
	myhandlerdyn.ignorableWhitespace = userIgnorableWhitespacedyn;

	// Validation  du fichier XML
	trace_output(STEP_XML_VALIDATED, FichierXML); //". XML file validation started"

	xmlSAXUserParseFile(&myhandlerdyn, NULL, FichierXML);

	trace_output(STEP_XML_CONVERT); //". XML file conversion started"

	//Transfert de la dynamique
	PtrXmlDyn->XmlDyn2GFL(this);

	trace_output(STEP_XML_END); //". SFG has been created from the XML description"

	delete PtrXmlDyn;
	xmlCleanupParser();
}
