/*!
 *  \author Daniel Menard, Mohammad DIAB, Jean-Charles Naud
 *  \date   28.06.2002
 *  \version 1.0
 */

// === Bibliothèques standard
//#include <stdlib>
#include <iostream>
#include <string>
#include <math.h>
#include <ctime>

#include "graph/dfg/datanode/NoeudSrcBruit.hh"
#include "graph/dfg/Gfd.hh"
#include "graph/lfg/GFL.hh"
#include "utils/Tool.hh"

// === Bibliothèques non standard (Forward Declaration)

// === Bibliothèques non standard (utiliser dans les Methodes et typeVarFonctions)


#ifndef OCTAVE
    #include <engine.h>
#endif

// === Namespaces
using namespace std;

namespace dynamic {
	void T2_DynamicRangeExpressionDetermination(Gfl* gfl){
		Chrono chrono;

		NodeGraphContainer::iterator it;
		string tempPathFichierParamFT;
		string pathMatlabFunctionSource;
		string commandMatlab;
		bool testFichierMatlabParamFTok;
		string nbImpulsePoint;
#ifndef OCTAVE
		Engine *ep;
#endif

		chrono.start();

		// Transfer function file generation
		trace_output(TOOL_STEP_3_2_1);
		tempPathFichierParamFT = ProgParameters::getOutputGenDirectory();
		tempPathFichierParamFT += PARAM_DYN_FT_FILENAME;
		Gft g(*gfl);
		testFichierMatlabParamFTok = g.genereMatFileParamFTDyn(tempPathFichierParamFT);
		if (!testFichierMatlabParamFTok) {
			trace_error("Problème génération fichier Matlab");
			exit(0);
		}

		// Engine processing
		trace_output(TOOL_STEP_3_2_2);
		if(ProgParameters::getDynProcess() == "interval"){
			pathMatlabFunctionSource = ProgParameters::getResourcesDirectory() + "/engine/l1_norm/";
			commandMatlab = "l1DynamicNormProcess";
			nbImpulsePoint = "100";
		}
		else if(ProgParameters::getDynProcess() == "overflow-proba"){
			pathMatlabFunctionSource = ProgParameters::getResourcesDirectory() + "/engine/kle_range/";
			commandMatlab = "overflowDynamicProcess";
			nbImpulsePoint = "199";
		}
		else{
			trace_error("Type d'évaluation de la dynamique incorrect");
			exit(-1);
		}

		commandMatlab += "('" + ProgParameters::getOutputGenDirectory() + "','" + DYNAMIC_ENGINE_FILE_NAME + "'," + nbImpulsePoint + ")";

#ifndef OCTAVE
		trace_info("Matlab processing...\n");
		if (!(ep = engOpen("\0"))) { // Test le moteur de matlab est operationnel, arret sinon
			trace_error("Can't start MATLAB engine");
			exit(0);
		}

		string pathMatlabToAdd = "addpath('" + pathMatlabFunctionSource + "')";
		trace_debug("matlab command: %s\n", pathMatlabToAdd.data());
		engEvalString(ep, pathMatlabToAdd.c_str());
		trace_debug("matlab command : %s\n", commandMatlab.data());
		engEvalString(ep, commandMatlab.c_str());
		engClose(ep);
#else
		trace_info("Octave processing...\n");
		trace_debug("octave command: %s\n", commandMatlab.data());
		runOctaveCommand(pathMatlabFunctionSource, commandMatlab);
#endif

		chrono.stop();
		RuntimeParameters::timing("T2 Engine processing", &chrono);
	}
}
