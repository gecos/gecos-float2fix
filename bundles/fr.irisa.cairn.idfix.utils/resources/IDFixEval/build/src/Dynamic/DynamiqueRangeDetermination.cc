/*!
 *  \author Daniel Menard
 *  \date   28.06.2002
 *  \version 1.0
 */

// === Bibliothèques standard
//#include <stdlib>
#include <iostream>
#include <string>

#include "dynamic/DynamicRangeDetermination.hh"
#include "graph/dfg/datanode/NoeudData.hh"
#include "graph/dfg/Gfd.hh"
#include "graph/dfg/operatornode/NoeudOps.hh"
#include "graph/lfg/GFL.hh"
#include "graph/toolkit/Types.hh"
#include "utils/Chrono.hh"
#include "utils/graph/cycledismantling/CycleDismantling.hh"
#include "utils/Tool.hh"


namespace dynamic {
	static void DynamicRangeDeterminationWithCycle(Gfd* gfd);
	static void DynamicRangeDeterminationWithoutCycle(Gfd* gfd);
	static void DynamicPropagation(Gfd* gfd);
	static DataDynamic DynamicPropagation(NoeudData* nData);
	static DataDynamic DynamicPropagation(NoeudOps* nOps);
	static void DynamicSavingToFile(Gfd* gfd);


	void DynamicRangeDetermination(Gfd* gfd){
		// Test si présence de circuit pour determiner la norme a appliquer
		if (RuntimeParameters::isRecursiveGraph() || ProgParameters::getDynProcess() == "overflow-proba") {
			trace_debug("Graphe avec cycle\n");
			DynamicRangeDeterminationWithCycle(gfd);
		}
		else {
			trace_debug("Graphe sans cycle\n");
			DynamicRangeDeterminationWithoutCycle(gfd);
		}
	}

	/**
	 * Appel les principal fonctions nécessaire pour la determination de la dynamique dans le cas d'un Gfd sans cycles
	 *
	 * \author Nicolas Simon
	 *
	 */
	static void DynamicRangeDeterminationWithoutCycle(Gfd* gfd) {
		Chrono chrono;
		chrono.start();

		// Doit être fait pour preparer la dynamique des noeuds autre que la source pour l'algo de propagation de la dynamique fonctionnant avec les cycles. Ne change pas la dynamique de l'entrée
		InitializeDataDynamicToNaN(gfd);

		// Algo de propagation qui accepte les cycles si l'initialisation est faite pour
		DynamicPropagation(gfd);

		gfd->saveGf("GFD.END");
		DynamicSavingToFile(gfd); // Sauvegarde la dynamique contenu dans les noeuds du graph dans un fichier txt
		GenerateDynamicFixedPointSpecification(gfd);

		chrono.stop();
		RuntimeParameters::timing("Dynamic without cycle" , &chrono);
	}


	/**
	 * Appel les principal fonctions nécessaire pour la determination de la dynamique dans le cas d'un Gfd avec cycles
	 * \return Rend un vecteur contenant les noeuds a démenteler
	 * \author Nicolas Simon
	 *
	 */

	static void DynamicRangeDeterminationWithCycle(Gfd* gfd) {
		Chrono chrono;

		if (!RuntimeParameters::isLTIGraph()) { //Verification que le Graph est LTI, arret si il ne l'est pas
			trace_fatal("Le Graph n'est pas LTI : Exécution interrompue");
			exit(0);
		}

		// Création du copie du GFD. Cette copie est indispensable pour pouvoir utiliser la propagation dynamique avec circuit.
		// L'algo de propagation utilise fonctionne sur le graph d'origine et non dementelle
		chrono.start();
		Gfd *copieGfd;
		copieGfd = gfd->copie();
		chrono.stop();
		RuntimeParameters::timing("GFD copying", &chrono);

		Gfl* GflResultat = T1_SystemPseudoFTDetermination(gfd);

		// Execution de Matlab et generation de DynNode.xml stockant la dynamique des noeud critique nécessaire pour appliquer la propagation
		T2_DynamicRangeExpressionDetermination(GflResultat);

		// Ajout de la dynamique des noeud critique calculé par MatLab au sein de la copie du GFD et propagation
		T3_DynamicTransferToGFD(GflResultat, gfd, copieGfd);

		// Algo recursif et parcours le graph en suivant l'operande 0 puis 1 des opérateurs (Important pour les circuits)
		chrono.start();
		DynamicPropagation(copieGfd);
		chrono.stop();
		RuntimeParameters::timing("Dynamic propagation", &chrono);
		copieGfd->saveGf("GFD.END");

		// Generation des fichiers contenant les résultats de la dynamique
		chrono.start();
		DynamicSavingToFile(copieGfd);
		GenerateDynamicFixedPointSpecification(copieGfd);
		chrono.stop();
		RuntimeParameters::timing("Output files generation", &chrono);


		delete GflResultat;
		delete copieGfd;
	}
	
	/*************************************************************************************/
	/*		PROPAGATION DYNAMIQUE  */
	/*************************************************************************************/
	/**
	 * Initialize all data node dynamic to NaN except source node, constant and tagged node
	 *
	 * \author Nicolas Simon
	 * \date 08/09/2008
	 */
	void InitializeDataDynamicToNaN(Gfd* gfd) {
		NoeudData *Noeud;

		DataDynamic val;
		val.valMax = NAN;
		val.valMin = NAN;
		NodeGraphContainer::iterator it;

		it = gfd->getBeginTabNode();
		for (; it != gfd->getEndTabNode(); ++it) {
			Noeud = dynamic_cast<NoeudData *> (*it);
			if(Noeud == NULL)
				continue;

			if(!Noeud->isNodeConst() && !Noeud->isNodeSource()){
				if(ProgParameters::getDynProcess() == "interval")
					Noeud->setDataDynamic(val);
				else if (ProgParameters::getDynProcess() == "overflow-proba" && Noeud->getDynProcess() != OVERFLOW_PROBA)
					Noeud->setDataDynamic(val);
			}
	
			/*		if (!Noeud->isNodeConst() && !Noeud->isNodeSource() && !(Noeud->getDynProcess() == OVERFLOW_PROBA && ProgParameters::getDynProcess() == "overflow-proba")){
					Noeud->setDataDynamic(Val);
					}*/
		}
	}


	/*
	 * Dynamic propagation for data node even if there are cycle in the graph
	 *
	 * \author Nicolas Simon
	 * \date 08/09/2010
	 */
	static void DynamicPropagation(Gfd* gfd) {
		NoeudData *NoeudVar;
		DataDynamic outputDynamic;
		int i;
		gfd->resetInfoVisite(); //reset info VISITE
		for (i = 0; i < gfd->getNbOutput(); i++) {
			NoeudVar = (NoeudData *) (gfd->getOutputGf(i));
			outputDynamic = DynamicPropagation(NoeudVar);
		}
		gfd->resetInfoVisite(); //reset info VISITE
	}

	/*
	 * Dynamic propagation for data node even if there are cycle in the graph
	 *
	 * \author Nicolas Simon
	 * \date 08/09/2010
	 */
	static DataDynamic DynamicPropagation(NoeudData* nData) {
		NoeudOps *NOps;
		DataDynamic outputDyn;

		if (nData->getEtat() == NONVISITE && !nData->isNodeSource()) {
			NOps = (NoeudOps *) (nData->getElementPred(0));
			nData->setEtat(ENTRAITEMENT);
			outputDyn = DynamicPropagation(NOps);
			if (nData->getDataDynamic().valMax != nData->getDataDynamic().valMax) { // Test si la valeur a deja une dynamique ou non. Utile car nous sommes dans un cas recursif avec des cycles
				nData->setDataDynamic(outputDyn);
			}
			nData->setEtat(VISITE);
			return nData->getDataDynamic();
		}
		else if(nData->getEtat() == ENTRAITEMENT && nData->getDataDynamic().valMax != nData->getDataDynamic().valMax){ // Cas pour des boucles imbriqué
			NOps = (NoeudOps*) (nData->getElementPred(0));
			return DynamicPropagation(NOps);
		}
		else {
			nData->setEtat(VISITE);
			return nData->getDataDynamic();
		}
	}

	/*
	 * Dynamic propagation for data node even if there are cycle in the graph
	 *
	 * \author Nicolas Simon
	 * \date 08/09/2010
	 */
	static DataDynamic DynamicPropagation(NoeudOps* nOps) {
		int i;
		int NbIn;
		DataDynamic *DynIn = NULL;
		DataDynamic DynOut;
		NoeudData **Op;

		NbIn = (nOps)->getNbPred();
		Op = new NoeudData *[NbIn]; /* Tableau de pointeur sur les noeuds Data representant les        operandes */
		DynIn = new DataDynamic[NbIn]; /* Tableau de la dynamique des entrees */
		for (i = 0; i < NbIn; i++) {
			Op[i] = (NoeudData *) ((nOps)->getElementPred(i));
			DynIn[i] = DynamicPropagation(Op[i]);
		}

        DynOut = nOps->dynamicRule(NbIn, DynIn);

		nOps->setOutputDynamic(DynOut.valMin, DynOut.valMax);

		delete[] DynIn;
		delete[] Op;

		return DynOut;
	}

	/**
	 * Save the dynamics of GFD node to DynSave.txt file
	 *
	 * \author Nicolas Simon
	 */
	static void DynamicSavingToFile(Gfd* gfd) {

		fstream FileDyn_pf;
		NoeudData *NData;
		NoeudGFD *NGfd;
		string pathFile;

		pathFile = ProgParameters::getOutputGenDirectory();

		string CurrentPath = pathFile;
		CurrentPath += DYN_SAVE_FILENAME;

		FileDyn_pf.open(CurrentPath.c_str(), fstream::out);
		FileDyn_pf << "---------------- " << endl;
		FileDyn_pf << "ENTREES" << endl;
		FileDyn_pf << "---------------- " << endl;
		for (int i = 0; i < gfd->getNbInput(); i++) {
			NData = (NoeudData *) gfd->getInputGf(i);
			FileDyn_pf << NData->getNumero() << " : " << NData->getName() << endl;
			FileDyn_pf << "    Dyn[ " << NData->getDataDynamic().valMin << " , " << NData->getDataDynamic().valMax << " ]" << endl << endl;
		}

		FileDyn_pf << "---------------- " << endl;
		FileDyn_pf << "CONSTANTS" << endl;
		FileDyn_pf << "---------------- " << endl;

		for (int i = 2; i < gfd->getNbNode(); i++) {
			NGfd = (NoeudGFD *) gfd->getElementGf(i);
			NData = (NoeudData *) NGfd;
			if (NGfd->getTypeNodeGFD() == DATA && NData->isNodeConst()) {
				FileDyn_pf << NGfd->getNumero() << " : " << NData-> getName() << endl;
				FileDyn_pf << "    Value[ " << NData-> getValeur() << " ]" << endl << endl;
			}
		}

		FileDyn_pf << "---------------- " << endl;
		FileDyn_pf << "VARINT" << endl;
		FileDyn_pf << "---------------- " << endl;

		for (int i = 0; i < gfd->getNbVarInt(); i++) {
			NData = (NoeudData *) gfd->getVarIntGf(i);
			FileDyn_pf << NData->getNumero() << " : " << NData-> getName() << endl;
			FileDyn_pf << "    Dyn[ " << NData->getDataDynamic().valMin << " , " << NData->getDataDynamic().valMax << " ]" << endl << endl;
		}

		FileDyn_pf << "---------------- " << endl;
		FileDyn_pf << "SORTIES" << endl;
		FileDyn_pf << "---------------- " << endl;
		for (int i = 0; i < gfd->getNbOutput(); i++) {
			NData = (NoeudData *) gfd->getOutputGf(i);
			FileDyn_pf << NData->getNumero() << " : " << NData->getName() << endl;
			FileDyn_pf << "    Dyn[ " << NData->getDataDynamic().valMin << " , " << NData->getDataDynamic().valMax << " ]" << endl << endl;
		}

		FileDyn_pf << "--------- FIN ----------";
		FileDyn_pf.close();
	}
}
