/**
 *  \file ModelFixPtSpecificationGeneration
 *  \author Loic Cloatre
 *  \date   17.11.2009
 *  \brief this file contains method used to create que model FixPtSpecification used in Gecos
 */
#include <stdio.h>
#include <stdlib.h>
#include <limits>
#include <map>
#include <iostream>

#include "dynamic/DynamicRangeDetermination.hh"
#include "graph/dfg/Gfd.hh"
#include "graph/dfg/datanode/NoeudData.hh"
#include "graph/dfg/operatornode/NoeudOps.hh"

#define BALISE_START_FIXPTSPECIF1  		"<fr.irisa.cairn.float2fix.model.FixPtSpecification:FixPtSpecif xmlns:fr.irisa.cairn.float2fix.model.FixPtSpecification="
#define BALISE_START_FIXPTSPECIF2       "fr.irisa.cairn.float2fix.model.FixPtSpecification"
#define BALISE_END_FIXPTSPECIF  		"</fr.irisa.cairn.float2fix.model.FixPtSpecification:FixPtSpecif>"


namespace dynamic {

	/*
	 * Methodes permettant de generer le fichier fp_specification.xml contenant les bornes des dynamiques pour des Noeud ops et data représenté par les mêmes numéro CDFG
	 *
	 * Nicolas Simon
	 * 21/03/11
	 */
	void GenerateDynamicFixedPointSpecification(Gfd* gfd) {
		fstream FileXML_pf;
		string pathFilemodeleSpecif = ProgParameters::getOutputDirectory() + FP_SPECIF_FILENAME;
		map<int, list<NoeudData *> > mapDataCDFG;
		map<int, list<NoeudOps *> > mapOpsCDFG;
		map<int, list<NoeudData *> >::iterator itData;
		map<int, list<NoeudOps *> >::iterator itOps;
		list<NoeudData *>::iterator itListData;
		list<NoeudOps *>::iterator itListOps;
		DataDynamic dynamicData, dynamicOps, dynamicOpsInput1, dynamicOpsInput2;
		NoeudGFD* NoeudTraite = NULL;
		NoeudOps* NOps = NULL;
		NoeudData* NData = NULL;

		FileXML_pf.open((pathFilemodeleSpecif).c_str(), fstream::out);
		if (!FileXML_pf.is_open()) {
			trace_error("erreur ouverture XML file FixPtSpecification : %s", pathFilemodeleSpecif.c_str());
			exit(-1);
		}

		FileXML_pf << "<?xml version=" << '"' << "1.0" << '"' << " encoding=" << '"' << "ASCII" << '"' << "?>" << endl;
		FileXML_pf << BALISE_START_FIXPTSPECIF1 << '"' << BALISE_START_FIXPTSPECIF2 << '"' << ">" << endl;

		/*
		 * Ajout aux differentes map les Noeud a traité par rapport a leur Numero CDFG coté opérateur et data
		 */
		for (int i = 2; i < gfd->getNbNode(); i++) {
			NoeudTraite = (NoeudGFD *) gfd->getElementGf(i);
			if (NoeudTraite->getTypeNodeGFD() == DATA && NoeudTraite->getNumCDFG() != -1) {
				itData = mapDataCDFG.find(NoeudTraite->getNumCDFG());
				if (itData != mapDataCDFG.end()) {// NumCDFG deja present dans la mapDataCDFG
					list<NoeudData *> tempList = mapDataCDFG[NoeudTraite->getNumCDFG()];
					tempList.push_back((NoeudData*) NoeudTraite);
					mapDataCDFG.erase(NoeudTraite->getNumCDFG());
					mapDataCDFG.insert(pair<int, list<NoeudData *> > (NoeudTraite->getNumCDFG(), tempList));
				}
				else {
					list<NoeudData*> tempList(1, (NoeudData *) NoeudTraite);
					mapDataCDFG.insert(pair<int, list<NoeudData *> > (NoeudTraite->getNumCDFG(), tempList));
				}
			}
			else if (NoeudTraite->getTypeNodeGFD() == OPS && NoeudTraite->getNumCDFG() != -1) {
				itOps = mapOpsCDFG.find(NoeudTraite->getNumCDFG());
				if (itOps != mapOpsCDFG.end()) { // NumCDFG deja present dans la mapOpsCDFG
					list<NoeudOps *> tempList = mapOpsCDFG[NoeudTraite->getNumCDFG()];
					tempList.push_back((NoeudOps *) NoeudTraite);
					mapOpsCDFG.erase(NoeudTraite->getNumCDFG());
					mapOpsCDFG.insert(pair<int, list<NoeudOps*> > (NoeudTraite->getNumCDFG(), tempList));
				}
				else {
					list<NoeudOps *> tempList(1, (NoeudOps*) NoeudTraite);
					mapOpsCDFG.insert(pair<int, list<NoeudOps *> > (NoeudTraite->getNumCDFG(), tempList));
				}
			}
		}

		/*
		 * Utilisation des maps pour determiner les bornes de la dynamique des noeuds représenté par le même numero CDFG puis ecriture dans fp_specification.xml
		 */
		for (itOps = mapOpsCDFG.begin(); itOps != mapOpsCDFG.end(); itOps++) {
			dynamicOps.valMin = std::numeric_limits<double>::max();
			dynamicOps.valMax = -std::numeric_limits<double>::max();
			dynamicOpsInput1.valMin = std::numeric_limits<double>::max();
			dynamicOpsInput1.valMax = -std::numeric_limits<double>::max();
			dynamicOpsInput2.valMin = std::numeric_limits<double>::max();
			dynamicOpsInput2.valMax = -std::numeric_limits<double>::max();

			for (itListOps = (*itOps).second.begin(); itListOps != (*itOps).second.end(); itListOps++) {
				/* Gestion de la dynamique de la sortie */
				NOps = (*itListOps);
				if (((NoeudData *) NOps->getElementSucc(0))->getDataDynamic().valMin < dynamicOps.valMin) {
					dynamicOps.valMin = ((NoeudData *) NOps->getElementSucc(0))->getDataDynamic().valMin;
				}
				if (((NoeudData *) NOps->getElementSucc(0))->getDataDynamic().valMax > dynamicOps.valMax) {
					dynamicOps.valMax = ((NoeudData *) NOps->getElementSucc(0))->getDataDynamic().valMax;
				}

				/* Le premier opérande est toujours défini */
				if (((NoeudData *) NOps->getElementPred(0))->getDataDynamic().valMin < dynamicOpsInput1.valMin) {
					dynamicOpsInput1.valMin = ((NoeudData *) NOps->getElementPred(0))->getDataDynamic().valMin;
				}
				if (((NoeudData*) NOps->getElementPred(0))->getDataDynamic().valMax > dynamicOpsInput1.valMax) {
					dynamicOpsInput1.valMax = ((NoeudData *) NOps->getElementPred(0))->getDataDynamic().valMax;
				}

				/* Test si opérateur à 2 opérandes */
				if (NOps->getNbPred() > 1) {
					if (((NoeudData *) NOps->getElementPred(1))->getDataDynamic().valMin < dynamicOpsInput2.valMin) {
						dynamicOpsInput2.valMin = ((NoeudData *) NOps->getElementPred(1))->getDataDynamic().valMin;
					}
					if (((NoeudData *) NOps->getElementPred(1))->getDataDynamic().valMax > dynamicOpsInput2.valMax) {
						dynamicOpsInput2.valMax = ((NoeudData *) NOps->getElementPred(1))->getDataDynamic().valMax;
					}
				}
			}

			FileXML_pf << "  <listFixPtOpsSpecif numOp=\"" << NOps->getNumCDFG() << "\" nameOp=\"" << NOps->toString().c_str() << "\">" << endl;
			FileXML_pf << "    <input1>" << endl;
			FileXML_pf << "      <typeAcFixed s=\"true\" />" << endl;
			FileXML_pf << "      <dyn lowerBound=\"" << dynamicOpsInput1.valMin << "\" upperBound=\"" << dynamicOpsInput1.valMax << "\"/>" << endl;
			FileXML_pf << "    </input1>" << endl;
			FileXML_pf << "    <input2>" << endl;
			FileXML_pf << "      <typeAcFixed s=\"true\" />" << endl;
			FileXML_pf << "      <dyn lowerBound=\"" << dynamicOpsInput2.valMin << "\" upperBound=\"" << dynamicOpsInput2.valMax << "\"/>" << endl;
			FileXML_pf << "    </input2>" << endl;
			FileXML_pf << "    <output>" << endl;
			FileXML_pf << "      <typeAcFixed s=\"true\" />" << endl;
			FileXML_pf << "      <dyn lowerBound=\"" << dynamicOps.valMin << "\" upperBound=\"" << dynamicOps.valMax << "\"/>" << endl;
			FileXML_pf << "    </output>" << endl;
			FileXML_pf << "  </listFixPtOpsSpecif>" << endl;

		}

		for (itData = mapDataCDFG.begin(); itData != mapDataCDFG.end(); itData++) {
			dynamicData.valMin = std::numeric_limits<double>::max();
			dynamicData.valMax = -std::numeric_limits<double>::max();
			for (itListData = (*itData).second.begin(); itListData != (*itData).second.end(); itListData++) {
				NData = (*itListData);
				if (NData->getDataDynamic().valMin < dynamicData.valMin) {
					dynamicData.valMin = NData->getDataDynamic().valMin;
				}
				if (NData->getDataDynamic().valMax > dynamicData.valMax) {
					dynamicData.valMax = NData->getDataDynamic().valMax;
				}
			}

			FileXML_pf << "  <listFixPtType>" << endl;
			FileXML_pf << "    <key Name=\"" << NData->getOriginalName().data() << "\" Num=\"" << NData->getNumCDFG() << "\" />" << endl;
			FileXML_pf << "    <value>" << endl;
			FileXML_pf << "      <typeAcFixed />" << endl;
			FileXML_pf << "      <dyn lowerBound=\"" << dynamicData.valMin << "\" upperBound=\"" << dynamicData.valMax << "\" />" << endl;
			FileXML_pf << "    </value>" << endl;
			FileXML_pf << "  </listFixPtType>" << endl;
		}

		FileXML_pf << BALISE_END_FIXPTSPECIF << endl;
	}
}

