/*!
 *  \author Daniel Menard, Mohammad DIAB, Jean-Charles Naud
 *  \date   28.06.2002
 *  \version 1.0
 */


#include "dynamic/DynamicRangeDetermination.hh"
#include "graph/dfg/datanode/NoeudData.hh"
#include "graph/dfg/Gfd.hh"
#include "graph/lfg/GFL.hh"
#include "utils/graph/cycledismantling/CycleDismantling.hh"
#include "utils/Tool.hh"


// === Namespaces
using namespace std;

namespace dynamic {
	static Gfl* ReccurentEquationDetermination(Gfd* gfd);
	static void PartialPseudoFTDetermination(Gfl* gfl);

	Gfl* T1_SystemPseudoFTDetermination(Gfd* gfd){
		Chrono chrono;

		trace_output(TOOL_STEP_3_1_1);
		chrono.start();
		CycleDismantling(gfd);
		gfd->initNumSrc(); // les noeuds source doivent avoir une numSrc différent dû a la création du GFL. //TODO A verifier si on ne peut pas l'enlever NS
		chrono.stop();
		gfd->saveGf("T11_GFD.Z");
		RuntimeParameters::timing("T11 GFD dismantling", &chrono);

		trace_output(TOOL_STEP_3_1_3);
		chrono.start();
		Gfl *GflResultat = ReccurentEquationDetermination(gfd);
		chrono.stop();
		RuntimeParameters::timing("T12 Reccurent Equation Determination", &chrono);

		trace_output(TOOL_STEP_3_1_4);
		chrono.start();
		PartialPseudoFTDetermination(GflResultat);
		chrono.stop();
		RuntimeParameters::timing("T13 Partial PseudoFT Determination", &chrono);

		return GflResultat;
	}

	/**
	 *  Determination of the set of pseudo-transfer functions which defines the system
	 *  	- Recurrent equation determination : calculFT()
	 *  	- partial pseudo-transfer function determination : mergeOfDifferentsGFL()
	 *  	- global pseudo-transfer function : executionOfVariableSubstitution()
	 *
	 *
	 *  \ingroup T1
	 *  \author Nicolas Simon
	 */
	static Gfl* ReccurentEquationDetermination(Gfd* gfd) {
		gfd->resetInfoVisite(); //reset fo linear fonction determination

		// Utilisation de la nouvelle structure des fonctions lineaire et des différents algoritme associé
		NoeudData* out;
		list<LinearFunction*> fctOut;
		for(int i = 0 ; i < gfd->getNbOutput() ; i++){
			out = dynamic_cast<NoeudData*> (gfd->getOutputGf(i));
			fctOut.push_back(out->calculLinearFoncLTI());
			fctOut.back()->setNode(out);
		}
		gfd->resetInfoVisite(); //on reset l'info visite du calcul des fonctions lineaires

		Gfl *GflGetFromT22 = new Gfl(fctOut, gfd->getNoiseEval()); //construction du GFL associe au GFD

	#if SAVE_GRAPH
		GflGetFromT22->saveGf("T12.GFL.A");
	#endif
		Gfl *GflRegroupe = GflGetFromT22->mergeOfGFL(); //fonction pour regrouper les petits GFL

	#if SAVE_GRAPH
		GflRegroupe->saveGf("T12.GFL.B");
	#endif

		delete GflGetFromT22;
		return GflRegroupe;
	}

	static void PartialPseudoFTDetermination(Gfl* gfl){
		CycleReduction(gfl);
	#if SAVE_GRAPH
		gfl->saveGf("T13.GFL.C");
	#endif
	}
}

