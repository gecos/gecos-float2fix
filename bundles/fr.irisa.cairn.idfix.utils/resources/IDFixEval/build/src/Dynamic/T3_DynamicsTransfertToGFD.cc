// === Bibliothèques standard
//#include <stdlib>

#include <string>

#include "dynamic/DynamicRangeDetermination.hh"
#include "graph/dfg/datanode/NoeudSrcBruit.hh"
#include "graph/dfg/Gfd.hh"
#include "graph/lfg/GFL.hh"
#include "graph/lfg/NoeudGFL.hh"
#include "utils/Tool.hh"


// === Namespaces
using namespace std;

namespace dynamic {
static void InitializationSourceDynamicToNaN(Gfd* gfd);

	/*
	 * Transfert the GFL node dynamics get by Matlab to GFD nodes linked
	 *
	 * \author Nicolas Simon
	 */
	void T3_DynamicTransferToGFD(Gfl* Gfl, Gfd* gfdDismantled, Gfd* gfd){
		Chrono chrono;
		chrono.start();

		trace_output(TOOL_STEP_3_3);
		trace_output(TOOL_STEP_3_3_1_rec);

		/* 		Etape 1
		 *
		 *  L'étape 1 consiste a recuperer les dynamiques fournies par Matlab via fichier xml et de les transferer au noeud du GFD dementellé correspondant
		 */

		InitializationSourceDynamicToNaN(gfdDismantled); // Init toutes les sources du graph démentellé a NaN, ceci va permettre de transferer les bonnes entrées dans le graph non démentellé
	#if SAVE_GRAPH
		gfdDismantled->saveGf("T3.GFD.A");
	#endif
		string pathFile;
		pathFile = ProgParameters::getOutputGenDirectory();
		string CurrentPath = pathFile;
		CurrentPath += DYNAMIC_ENGINE_FILE_NAME;
		Gfl->xmlDynReader(CurrentPath.c_str()); // Lis DynNode.xml et ajoute la dynamique calculé a travers les noeuds GFL au noeud GFD (gfdDismantled) correspondant

	#if SAVE_GRAPH
		gfdDismantled->saveGf("T3.GFD.B");
	#endif

	#if 0
		{
			for (int i = 0; i < gfdDismantled->GetNbEntrees(); i++) {
				NoeudSrc *temp = (NoeudSrc *) gfdDismantled->getInputGf(i);
				cout << "Nom du InputNoeud : " << temp->getNomData() << endl;
				temp->AffichageDynamique();
			}
			for (int i = 0; i < gfdDismantled->getNbOutput(); i++) {
				NoeudSrc *temp = (NoeudSrc *) gfdDismantled->getOutputGf(i);
				cout << "Nom du OutputNoeud : " << temp->getNomData() << endl;
				temp->AffichageDynamique();
			}
		}
	#endif

		trace_output(TOOL_STEP_3_3_2_rec);

		/*		Etape 2
		 *
		 *	L'étape 2 consiste a transferer la dynamique des noeud GFD dementellé a la copie du GFD d'origine
		 */

		NoeudData *NData;
		NoeudData *NDataCopie;

		InitializeDataDynamicToNaN(gfd);
	#if SAVE_GRAPH
		gfd->saveGf("T3.GFD.C");
	#endif

		// Transmission des information la dynamique pour toutes les entrées du GFD démentellé
		for(int i = 0; i< gfdDismantled->getNbInput(); i++){
			NData = dynamic_cast<NoeudData *> (gfdDismantled->getInputGf(i));
			assert(NData != NULL);
			if (NData->getDataDynamic().valMax == NData->getDataDynamic().valMax) { // Teste si la dynamique est deja attribué ou non
				NDataCopie = dynamic_cast<NoeudData*> (gfd->getElementGFD(NData->getName()));
				assert(NDataCopie != NULL);
				NDataCopie->setDataDynamic(NData->getDataDynamic());
			}

			// Traitement particulié pour la partie dynamique en mode proba
			// Les noeuds taggé ne sont pas des entrée du GFD démenteller mais nous devons tout de même transmettre la dynamiques
			// Ces Noeuds taggé sont des sortie du GFL.
			if (ProgParameters::getDynProcess() == "overflow-proba"){
				for(int i = 2; i<Gfl->getNbNode() ; i++){
					NData = dynamic_cast<NoeudData*> (((NoeudGFL*) Gfl->getElementGf(i))->getRefParamSrc());
					assert(NData != NULL);
					NDataCopie = dynamic_cast<NoeudData*> (gfd->getElementGFD(NData->getName()));
					assert(NDataCopie != NULL);
					if(NDataCopie->getDynProcess() == OVERFLOW_PROBA){ // Cas des sortie principale du graph qui ne sont pas forcement taggé, on doit les propager
						// Dynamique d'un noeud taggé non attribué dans le noeud démentellé
						assert(NData->getDataDynamic().valMax == NData->getDataDynamic().valMax);
						NDataCopie->setDataDynamic(NData->getDataDynamic());
					}
				}


			}
	#if SAVE_GRAPH
			gfd->saveGf("T3.GFD.D");
	#endif
		}

		chrono.stop();
		RuntimeParameters::timing("T3 Transfer dynamic to GFD", &chrono);
	}

	/**
	 * Initialize all dismantled source node to NaN
	 *
	 *  \author Nicolas Simon
	 *  \date 08/09/2008
	 */
	static void InitializationSourceDynamicToNaN(Gfd* gfd){
		NoeudData* nData;
		DataDynamic val;
		val.valMax = NAN;
		val.valMin = NAN;

		for(int i = 0 ; i<gfd->getNbInput() ; i++){
			nData = dynamic_cast<NoeudData*> (gfd->getInputGf(i));
			// Une entrée est forcement un noeud data
			assert(nData != NULL);
			nData->setDataDynamic(val);
		}
	}
}
