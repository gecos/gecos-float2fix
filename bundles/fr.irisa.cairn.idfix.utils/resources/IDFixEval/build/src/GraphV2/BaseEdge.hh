#ifndef __BASEEDGE_HH__
#define __BASEEDGE_HH__

#include "BaseNode.hh"

class BaseNode;

class BaseEdge {
    private:
        BaseNode *m_from, *m_to;

    protected:
        BaseEdge(BaseNode *from, BaseNode *to);
        BaseEdge(const BaseEdge &other){}

    public:
        ~BaseEdge(){};
        virtual BaseEdge* clone(BaseNode *from, BaseNode *to) = 0;

        BaseNode* getFrom(){return m_from;}
        BaseNode* getTo(){return m_to;}
};


#endif
