#include "XMLParserDFG.hh"

#include <list>
#include <map>
#include <iostream>
#include <sstream>
#include <cmath>
#include <libxml/SAX2.h>
#include <utils/Tool.hh>

#include "ADDNode2.hh"
#include "DELAYNode2.hh"
#include "DIVNode2.hh"
#include "MULNode2.hh"
#include "SUBNode2.hh"
#include "SETNode2.hh"
#include "PHINode2.hh"

#include "ConstantNode.hh"
#include "SourceNode.hh"
#include "ScalarNode.hh"

enum XML_STATE {
    IN_DOC,
    OUT_DOC,
    IN_GRAPH,
    OUT_GRAPH,
    IN_NODE,
    IN_EDGE,
    IN_ATTR_NODE,
    IN_ATTR_EDGE
};

enum NodeClass {
    OP_NODE,
    DATA_NODE,
    UNKNOWN 
};

enum DataProperty {
    OTHER,
    INPUT,
    OUTPUT
};

struct Attr {
    std::string key, value;
};

struct EdgeP {
    std::string id_pred, id_succ;
    std::list<Attr> list_attr;
};

struct Node {
    std::string id;
    std::list<Attr> list_attr;
};


static EdgeP current_edge;
static Node current_node;
static XML_STATE current_elem;

struct UserData {
    DFG *dfg;
    std::map<int, DFGNode*> nodes;
};

static void CreateNode(void *user_data, Node xmlNode);
static void CreateEdge(void *user_data, EdgeP xmlEdge);

void start_document(void *user_data){
    current_elem = IN_DOC;
}

void end_document(void *user_data){
    current_elem = OUT_DOC;
}

void start_element(void *user_data, const xmlChar *name, const xmlChar **attrs){
    switch(current_elem){
        case IN_DOC:
            if (xmlStrEqual(name, BAD_CAST("graph")) == 1 || xmlStrEqual(name, BAD_CAST("fr.irisa.cairn.float2fix:Graph")) == 1) 
                current_elem = IN_GRAPH;
            else 
                current_elem = OUT_GRAPH; // Absence de graphe donc sortie du document
            break;

        case IN_GRAPH:
            if (xmlStrEqual(name, BAD_CAST("node")) == 1) {
                current_elem = IN_NODE;
                current_node.id = (char*)attrs[3];
                current_node.list_attr.clear();
            }
            else if (xmlStrEqual(name, BAD_CAST("edge")) == 1) {
                current_elem = IN_EDGE;
                current_edge.id_pred = (char*)attrs[1];
                current_edge.id_succ = (char*)attrs[3];
                current_edge.list_attr.clear(); 
            }

            break;

        case IN_NODE:
            if (xmlStrEqual(name, BAD_CAST("attr")) == 1) {
                current_elem = IN_ATTR_NODE;
                Attr attr;
                attr.key = (char*) attrs[1];
                attr.value = (char*) attrs[3];
                current_node.list_attr.push_back(attr);
            }
            break;

        case IN_EDGE:
            if (xmlStrEqual(name, BAD_CAST("attr")) == 1) {
                current_elem = IN_ATTR_EDGE;
                Attr attr;
                attr.key = (char*) attrs[1];
                attr.value = (char*) attrs[3];
                current_edge.list_attr.push_back(attr);
            }
            break;

        default:
            trace_error("XML error parsing\n");
            exit(-1);
            break;
    }
}

void end_element(void *user_data, const xmlChar *name){
    switch(current_elem){
        case IN_ATTR_NODE:
            current_elem = IN_NODE;
            
            break;

        case IN_ATTR_EDGE:
            current_elem = IN_EDGE;

            break;

        case IN_NODE:
            CreateNode(user_data, current_node);
            current_elem = IN_GRAPH;

            break;
        
        case IN_EDGE:
            CreateEdge(user_data, current_edge);
            current_elem = IN_GRAPH;

            break;

        case IN_GRAPH:
            current_elem = IN_DOC;
        
            break;

        case IN_DOC:
            break;

        default:
            trace_error("XML error parsing\n");
            exit(-1);

            break;
    }
}

void user_characters(void * , const xmlChar *, int) {
}

DFG* parseInputXMLFile(std::string filepath){

    xmlSAXHandler handler = { 0 };; 
    handler.startDocument = start_document;
    handler.startElement = start_element;
    handler.endDocument = end_document;
    handler.endElement = end_element;
	handler.characters = user_characters;

    DFG* dfg = new DFG();
    UserData data;
    data.dfg = dfg;
    if(xmlSAXUserParseFile(&handler, &data, filepath.c_str()) != 0){

    }

	xmlCleanupParser();
    return dfg;
}

void CreateNode(void *user_data, Node xmlNode){
    UserData* data = (UserData*) user_data;
    std::list<Attr>::const_iterator it;
    std::string key, value;
    NodeClass NClass = UNKNOWN;
    std::stringstream ss;

    int num_data_cdfg = -1, num_op_cdfg = -1, num_op_sfg = -1, num_basicblock = -1; 
    float data_value = NAN;
    double source_valmin = NAN, source_valmax = NAN;
    std::string name, nature, property;
    
    int id = atoi(xmlNode.id.c_str());
    if(data->nodes.find(id) == data->nodes.end()){
        for(it = xmlNode.list_attr.begin() ; it != xmlNode.list_attr.end() ; it++){
            key = (*it).key;
            value = (*it).value;
        
            if(key.compare("CLASS") == 0){
                if(value.compare("OP") == 0){
                    NClass = OP_NODE; 
                }
                else if(value.compare("DATA") == 0){
                    NClass = DATA_NODE;
                }
                else{
                    trace_error("Node class unknown or incorrect %s\n", value.c_str());
                    exit(-1);
                }
            }
            else if(key.compare("NAME") == 0){
                name = value;
            }
            else if(key.compare("NATURE") == 0){
                nature = value;
            }
            else if(key.compare("NUM_DATA_CDFG") == 0){
                num_data_cdfg = atoi(value.c_str());         
            }
            else if(key.compare("NUM_OP_CDFG") == 0){
                num_op_cdfg = atoi(value.c_str());         
            }
            else if(key.compare("NUM_OP_SFG") == 0){
                num_op_sfg = atoi(value.c_str());         
            }
            else if(key.compare("NUM_BASIC_BLOCK") == 0){
                num_basicblock = atoi(value.c_str());         
            }
            else if(key.compare("NUM_AFFECT") == 0){
                trace_warn("XML attribute not managed yet %s.\n", key.c_str());
            }
            else if(key.compare("VALUE") == 0){
                ss << value;
                ss >> data_value;
            }
            else if(key.compare("PROPERTY") == 0){
                property = value; 
            }
            else if(key.compare("VAL_MIN") == 0){
                ss << value;
                ss >> source_valmin;
            }
            else if(key.compare("VAL_MAX") == 0){
                ss << value;
                ss >> source_valmax;
            }
            else{
                trace_warn("XML attribute not managed yet %s.\n", key.c_str());
            }
            ss.clear();
        }

        DFGNode * dfg_node = NULL;
        if(NClass == DATA_NODE){
            if(num_data_cdfg == -1){
                trace_error("Missing required information in the xml to create a operator node.\n");
                exit(-1);
            }

            if(nature.compare("CONSTANT") == 0){
                if(!std::isnan(data_value)){
                    dfg_node = new ConstantNode(data_value); 
                }
                else{
                    trace_error("Missing value information of the constant node in the xml file. ID:%i ", id);
                    exit(-1);
                }
            }
            else if(nature.compare("VARIABLE_VAL_KNOWN") == 0){
                if(!std::isnan(data_value)){
                    dfg_node = new ScalarNode(name, num_data_cdfg, data_value); 
                }
                else{
                    trace_error("Missing value information of the known value data node in the xml file. ID:%i ", id);
                    exit(-1);
                }
            }
            else if(nature.compare("VARIABLE_VAL_UNKNOWN") == 0){
                if(property.compare("INPUT") == 0){
                   if(!std::isnan(source_valmax) && !std::isnan(source_valmin)){
                       dfg_node = new SourceNode(name, num_data_cdfg, source_valmin, source_valmax);
                   }
                   else{
                        trace_error("Missing dynamic information of a input variable. ID:%i\n", id); 
                        exit(-1);
                   }
                }
                else{
                    dfg_node = new ScalarNode(name, num_data_cdfg); 
                }
            }
            else{
                trace_error("Wrong nature attribute detected in the xml for a node representing a data. ID:%i\n", id);
                exit(-1);
            }
        }
        else if(NClass == OP_NODE){
            if(num_op_cdfg == -1||num_op_sfg == -1||num_basicblock==-1){
                trace_error("Missing required information in the xml to create a operator node.\n");
                exit(-1);
            }

            Block block(num_basicblock);

            if(nature.compare("ADD") == 0){
                dfg_node = new ADDNode(num_op_cdfg, num_op_sfg, block); 
            }
            else if(nature.compare("SUB") == 0){
                dfg_node = new SUBNode(num_op_cdfg, num_op_sfg, block); 
            }
            else if(nature.compare("MUL") == 0){
                dfg_node = new MULNode(num_op_cdfg, num_op_sfg, block); 
            }
            else if(nature.compare("DIV") == 0){
                dfg_node = new DIVNode(num_op_cdfg, num_op_sfg, block); 
            }
            else if(nature.compare("DELAY") == 0){
                dfg_node = new DELAYNode(num_op_cdfg, num_op_sfg, block); 
            }
            else if(nature.compare("EQ") == 0){
                dfg_node = new SETNode(num_op_cdfg, num_op_sfg, block); 
            }
            else if(nature.compare("PHI") == 0){
                dfg_node = new PHINode(block); 
            }
        }
        else{
            trace_error("Class node unknown detected in the xml.");
            exit(-1);
        }

        if(dfg_node != NULL){
            data->dfg->addNode(dfg_node);
            data->nodes[id] = dfg_node;
        }
        else{
            trace_error("Problem during the building of the node of id %i.\n", id);
            exit(-1);
        }
    }
    else{
        trace_error("A node with the id %i have been already created. Error in the xml input file given.\n", id);
        exit(-1);
    }
}

void CreateEdge(void *user_data, EdgeP xmlEdge){
    UserData* data = (UserData*) user_data;
    std::list<Attr>::const_iterator it;
    std::string key, value;
    std::stringstream ss;
    unsigned int num_input = 0;

    int idPred = atoi(xmlEdge.id_pred.c_str());
    int idSucc = atoi(xmlEdge.id_succ.c_str());
    if(data->nodes.find(idPred) == data->nodes.end()){
        trace_error("Error during the creation of the edge from the node (id) %i to the node (id) %i. The node (id) have %i not been created yet.\n", idPred, idSucc, idPred);
        exit(-1);
    }
    else if(data->nodes.find(idSucc) == data->nodes.end()){
        trace_error("Error during the creation of the edge from the node (id) %i to the node (id) %i. The node (id) have %i not been created yet.\n", idPred, idSucc, idSucc);
        exit(-1);
    }
    else{
        for(it = xmlEdge.list_attr.begin() ; it != xmlEdge.list_attr.end() ; it++){
            key = (*it).key;
            value = (*it).value;

            if(key.compare("NUM_INPUT") == 0){
                ss << value;
                ss >> num_input;
            }
        }
        data->dfg->addEdge(data->nodes[idPred], data->nodes[idSucc], num_input);
    }
}
