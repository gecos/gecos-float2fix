#ifndef __XMLPARSERDFG_HH__
#define __XMLPARSERDFG_HH__

#include <string>

#include "DFG.hh"


DFG* parseInputXMLFile(std::string filepath);

#endif
