#ifndef __DFG_PHINODE_HH__
#define __DFG_PHINODE_HH__

#include "OperatorNode.hh"

class PHINode : public OperatorNode {
    protected:
      //  FonctionLineaire* computeRuleLinearFunction(unsigned int size, FonctionLineaire* operand[]); 

    public:
        PHINode(Block &block); 
        PHINode(const PHINode &other);
        PHINode* clone(){return new PHINode(*this);}

        // Global
/*        TypeLti isLTI(){return SIGNAL_LTI;}; ///< Return if the operator is LTI from operands

        // Dynamic determination
        DataDynamic dynamicRule(unsigned int size, DataDynamic operand[]); ///< Apply the dynamic rules of the operator from the dynamic of his operand

        // Noise model and noise system behaviour determination
        NoeudData* arithmeticOperationGeneration(fstream & matfile); ///< Generate the athmetic operation needed by the non-lti system for this operator
        void moveNoiseSource(NoeudSrcBruit* noiseNode){} ///< Move, if possible, through the operator the noise node given
        void noiseModelInsertion(Gfd *gfd); ///< Insert the noise model of the operator

        // Back End (noise function generation)
        string WFPEffDetermination(); ///< Return the number of bit effetive of this operator
        string KDetermination(); ///< Return the K bit eleminated of this operator*/

        std::string toString(){return "PHI";}
        std::string getDotColor(){return "forestgreen";}
        bool isPHINode(){return true;}
};

#endif

