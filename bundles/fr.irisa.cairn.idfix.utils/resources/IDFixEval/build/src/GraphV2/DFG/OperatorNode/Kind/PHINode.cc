#include "PHINode2.hh"
//#include "PseudoFonctionLineaire.hh"

PHINode::PHINode(Block &block):OperatorNode(-1, -1, 2, block, "PHI"){}
PHINode::PHINode(const PHINode &other):OperatorNode(other){}

/*std::string PHINode::WFPEffDetermination(){
    trace_error("Not yet implemented\n"); 
    exit(-1);
}

std::string PHINode::KDetermination(){
    trace_error("Not yet implemented\n");
    exit(-1);
}

DataDynamic PHINode::dynamicRule(unsigned int size, DataDynamic operand[]){
    DataDynamic dynOut;

    // Propagation de la dynamique au pire cas (Worst case)
    dynOut.valMin = operand[0].valMin;
    dynOut.valMax = operand[0].valMax;
    for (unsigned int i = 1; i < size; i++) {
        if (dynOut.valMin > operand[i].valMin) {
            dynOut.valMin = operand[i].valMin;
        }
        if (dynOut.valMax < operand[i].valMax) {
            dynOut.valMax = operand[i].valMax;
        }
    }

    return dynOut;
}

NoeudData*  PHINode::arithmeticOperationGeneration(fstream & matfile){
    trace_error("PHI node cannot generate non-LTI arithmetic operation. Error during graph transformation\n");
    exit(EXIT_FAILURE);
}

FonctionLineaire* PHINode::computeRuleLinearFunction(unsigned int size, FonctionLineaire* operand[]){
    FonctionLineaire *result;

    assert(getNbSucc() == 1);
    PseudoFonctionLineaire* pseudoFct = new PseudoFonctionLineaire(this);
    pseudoFct->addEltZ(0,1,"1");
    result = new FonctionLineaire(this); // Le nData ne sert a rien puisqu'il est écrasé
    result->addPseudoFctLineaire(pseudoFct);

    return result;
}

void PHINode::noiseModelInsertion(Gfd *gfd){
    int count = 0; // compteur du numéro de l'egde (puisque le noeud phy peut avoir plus de deux entrée)

    trace_debug("A CONTROLER\n");
    NoeudGraph *NG;
    NoeudGFD *NGFD;
    NoeudData *ND;
    TypeSignalBruit typeSignalBruit;
    TypeNodeGFD TypeNGFD;


    OperatorNode *OpsPhyBruit = this->clone();
    OpsPhyBruit->setTypeSignalBruit(BRUIT);
    gfd->addOPNode(OpsPhyBruit);
    int nbSucc = this->getNbSucc();
    int nbPred = this->getNbPred();

    // --- Redirection (Ajout et supression) des acrs succ  ---
    for (int i = 0; i < nbSucc; i++){ // Redirection des arcs succ
        NG = this->getElementSucc(i);
        NGFD = (NoeudGFD *) NG;
        TypeNGFD = NGFD->getTypeNodeGFD();
        if (TypeNGFD == DATA) {
            ND = (NoeudData *) NGFD;
            typeSignalBruit = ND->getTypeSignalBruit();
            if (typeSignalBruit == BRUIT) {
                this->deleteEdgeDirectedTo(ND); // On suprime les arcs (OPS_SIGNAL vers DATA_BRUIT)
                nbSucc--;
                i--;
                OpsPhyBruit->edgeDirectedTo(ND);
            }
        }
    }

    // --- Redirection (Ajout et supression) des acrs pred  ---
    for (int i = 0; i < nbPred; i++) // Redirection des arcs pred
    {
        NG = this->getEdgePredTransformation(i)->getNodePred();
        NGFD = (NoeudGFD *) NG;
        TypeNGFD = NGFD->getTypeNodeGFD();
        if (TypeNGFD == DATA) {
            ND = (NoeudData *) NGFD;
            typeSignalBruit = ND->getTypeSignalBruit();
            if (typeSignalBruit == BRUIT) {
                this->EnleveedgeFromOf(ND); // On suprime les arcs (DATA_BRUIT vers OPS_SIGNAL)
                OpsPhyBruit->edgeFromOf(ND,count); // On ajoute les arcs (OPS_BRUIT vers DATA_BRUIT)
                count++;
            }
        }
    }
}*/
