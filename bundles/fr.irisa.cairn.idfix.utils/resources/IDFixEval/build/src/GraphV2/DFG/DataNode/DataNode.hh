#ifndef _DATANODE_HH_
#define _DATANODE_HH_

#include "DFGNode.hh"
#include "DFGEdge.hh"

class DataNode: public DFGNode {
    protected:
        DataNode(std::string name, int cdfgNumber);
        DataNode(const DataNode &other);

    public:
        void checkAddingEdgeValidity(DFGEdge *edge);
};

#endif // _NOEUDDATA_HH_
