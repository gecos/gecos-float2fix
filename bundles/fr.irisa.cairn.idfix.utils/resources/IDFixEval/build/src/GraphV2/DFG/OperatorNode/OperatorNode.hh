/**
 *  \file NoeudOps.hh
 *  \author Daniel Menard, Jean-Charles Naud
 *  \date   31.01.2002
 *  \brief Classe definissant les noeuds representant une operation dans un GFD
 */

#ifndef _OPERATORNODE_HH_
#define _OPERATORNODE_HH_

#include <utils/graph/Block.hh>

#include "DFGNode.hh"

/**
 * \class NoeudOps
 * \brief Classe définisant les nœuds d'opération
 *
 *  Classe définisant les nœuds d'opération aritmétique ou non
 */
class OperatorNode: public DFGNode {
protected:
	unsigned int m_inputNumber; ///< Nombre d'entree
	int m_SFGNumber; ///< Field got with input XML file
	Block &m_block; ///< ID of the block this node is inside of

	OperatorNode(int cdfg_number, int sfg_number, int inputNumber, Block &block, std::string name); ///< Constructeur
	OperatorNode(const OperatorNode &other);

//    virtual FonctionLineaire* computeRuleLinearFunction(unsigned int size, FonctionLineaire* operand[]) = 0;

    std::string getDotLabel();
public:
	// ======================================= Constructor/Destructor
//	virtual ~OperatorNode(); ///< Destructeur
    //virtual OperatorNode* clone() = 0;
	
	// ======================================= Getter/Setter 
    Block &getBlock() const {
		return m_block;
	}///< Retourne l'ID du block

	int getNumSFG() const {
		return m_SFGNumber;
	} ///< Getter of NumSFG

    unsigned int getInputNumber(){return m_inputNumber;}
    
	// ======================================= Virtual methods
    // Global
/*	virtual TypeLti isLTI() = 0; ///< Return if the operator is LTI from operands
    
    // Dynamic determination
    virtual DataDynamic dynamicRule(unsigned int size, DataDynamic operand[]) = 0; ///< Apply the dynamic rules of the operator from the dynamic of his operand

    // Noise model and noise system behaviour determination
    virtual NoeudData* arithmeticOperationGeneration(fstream & matfile) = 0; ///< Generate the athmetic operation needed by the non-lti system for this operator
	virtual void moveNoiseSource(NoeudSrcBruit* noiseSourceNode) = 0;  ///< Insert the noise model of the operator
    virtual void noiseModelInsertion(Gfd *gfd) = 0; ///< Move, if possible, through the operator the noise node given
    
    // Back End (noise function generation)
    virtual string WFPEffDetermination() = 0; ///< Return the number of bit effetive of this operator
    virtual string KDetermination() = 0; ///< Return the K bit eleminated of this operator

    virtual string toString() = 0;

	// ======================================= Methods
	// T11
	list<NoeudData*> detectionBrCorrelated(list< list<NoeudData*> >* tabListBr);

    //T22
    FonctionLineaire* calculLinearFoncLTI();
	FonctionLineaire* calculLinearFoncNonLTI();*/

    void checkAddingEdgeValidity(DFGEdge *edge);

    virtual bool isPHINode(){return false;}
};

#endif // _NOEUDOPS_HH_
