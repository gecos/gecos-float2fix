#ifndef __DFG_HH__
#define __DFG_HH__

#include "BaseGraph.hh"
#include "DFGNode.hh"

class DFG : public BaseGraph {
    public:
        DFG(){};
        DFG(const DFG &other);

        void addNode(DFGNode *node){BaseGraph::addNode(node);}
        void addEdge(DFGNode *from, DFGNode *to, unsigned int input_number);
};


#endif
