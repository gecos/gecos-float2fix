#include "DataNode.hh"

#include <utils/Tool.hh>

#include "OperatorNode.hh"

DataNode::DataNode(std::string name, int cdfgNumber) :
	DFGNode(name, cdfgNumber) {
}

DataNode::DataNode(const DataNode &other):DFGNode(other){
    
}

void DataNode::checkAddingEdgeValidity(DFGEdge *edge){
    DFGNode *from, *to;
    OperatorNode *opNode;
    from = edge->getFrom();
    to = edge->getTo();
    

    if(from == this){
       if((opNode = dynamic_cast<OperatorNode*>(to)) == NULL){
            trace_error("Data node must be connected to a operator node. Impossible to connect the data node %s to the node %s\n", this->getName().c_str(), to->getName().c_str()); 
            exit(-1);
       } 
    }
    else if(to == this){
       if((opNode = dynamic_cast<OperatorNode*>(from)) == NULL){
            trace_error("Data node must be connected to a operator node. Impossible to connect the data node %s from the node %s\n", this->getName().c_str(), to->getName().c_str()); 
            exit(-1);
       } 
       if(this->getNbPredecessors() == 1){
            trace_error("Try to add an new egde to the data node %s while an another operator node %s is connected\n", this->getName().c_str(), from->getName().c_str());
            exit(-1);
        }
    }
    else{
        trace_error("The edge is not build to be connect to the data node %s", this->getName().c_str());
        exit(-1);
    }
}
