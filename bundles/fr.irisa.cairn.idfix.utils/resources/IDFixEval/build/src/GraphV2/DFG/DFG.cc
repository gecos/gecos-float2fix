#include "DFG.hh"

#include "DFGEdge.hh"

DFG::DFG(const DFG &other):BaseGraph(other){}

void DFG::addEdge(DFGNode *from, DFGNode* to, unsigned int input_number){
    DFGEdge *edge = new DFGEdge(from, to, input_number);
    from->checkAddingEdgeValidity(edge);
    to->checkAddingEdgeValidity(edge);
    BaseGraph::addEdge(from, to, edge);
}
