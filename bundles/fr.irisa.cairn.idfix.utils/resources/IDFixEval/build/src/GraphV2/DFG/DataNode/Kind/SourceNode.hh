#ifndef __SOURCENODE_HH__
#define __SOURCENODE_HH__

#include "DataNode.hh"

class SourceNode : public DataNode{
    protected:
        std::string getDotColor(){return "dodgerblue";}

    public:
        SourceNode(std::string name, int cdfgNumber, double val_min, double val_max);
        SourceNode(const SourceNode &other);
        SourceNode* clone(){return new SourceNode(*this);}
        void checkAddingEdgeValidity(DFGEdge *edge);
};

#endif
