#include "DFGEdge.hh"

#include <utils/Tool.hh>

DFGEdge::DFGEdge(DFGNode *from, DFGNode *to, unsigned int inputNumber):BaseEdge(from,to){
    this->m_inputNumber = inputNumber;
}

DFGEdge::DFGEdge(const DFGEdge &other):BaseEdge(other){
    m_inputNumber = other.m_inputNumber;
}

DFGEdge* DFGEdge::clone(BaseNode *from, BaseNode *to){
    DFGNode *dfg_from, *dfg_to;
    if((dfg_from = dynamic_cast<DFGNode*>(from)) == NULL){
        trace_error("Impossible to clone this DFG edge with this \"from\" node given %s. This node is not a DFG node.", from->getName().c_str());
        exit(-1);
    }
    else if((dfg_to = dynamic_cast<DFGNode*>(to)) == NULL){
        trace_error("Impossible to clone this DFG edge with this \"to\" node given %s. This node is not a DFG node.", to->getName().c_str());
        exit(-1);
    }
    else{
        return new DFGEdge(dfg_from, dfg_to, this->getInputNumber());
    }
}
