#ifndef __SCALARNODE_HH__
#define __SCALARNODE_HH__

#include "DataNode.hh"

class ScalarNode : public DataNode{
    private:
        float m_value;

    protected:
        std::string getDotLabel();

    public:
        ScalarNode(std::string name, int cdfgNumber);
        ScalarNode(std::string name, int cdfgNumber, float value);
        ScalarNode(const ScalarNode &other);
        ScalarNode* clone(){return new ScalarNode(*this);}

        float getValue(){return m_value;}
        void setValue(float value){m_value = value;}
};

#endif
