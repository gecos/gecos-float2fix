#ifndef __DFGNODE_HH__
#define __DFGNODE_HH__

#include "BaseNode.hh"
#include "DFGEdge.hh"

class DFGEdge;

class DFGNode : public BaseNode {
    private:
        int m_cdfgNumber;
        DataDynamic m_dynamic;

    protected:
        DFGNode(std::string name, int cdfgNumber);
        DFGNode(const DFGNode &other);
        std::string getDotLabel();

    public:

        virtual void checkAddingEdgeValidity(DFGEdge *edge) = 0;

        void printDOTEdge(FILE *file);

        int getCDFGNumber(){return m_cdfgNumber;}
        DataDynamic getDynamic() {return m_dynamic;}
        void setDynamic(double min, double max){m_dynamic.valMin = min;m_dynamic.valMax = max;}
};

#endif
