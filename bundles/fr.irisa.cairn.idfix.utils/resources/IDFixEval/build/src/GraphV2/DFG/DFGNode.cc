#include "DFGNode.hh"

#include <sstream>
#include "DFGEdge.hh"

DFGNode::DFGNode(std::string name, int cdfgNumber):BaseNode(name){
    m_cdfgNumber = cdfgNumber;
    m_dynamic.valMin = 0;
    m_dynamic.valMax = 0;
}

DFGNode::DFGNode(const DFGNode &other):BaseNode(other){
    m_cdfgNumber = other.m_cdfgNumber;
    m_dynamic.valMin = other.m_dynamic.valMin;
    m_dynamic.valMax = other.m_dynamic.valMax;
}

std::string DFGNode::getDotLabel(){
    std::ostringstream ss, valmin, valmax;
    ss << getCDFGNumber();
    valmin << this->getDynamic().valMin;
    valmax << this->getDynamic().valMax;
    return BaseNode::getDotLabel() + "\\nCDFG: " + ss.str() + "\\n[" + valmin.str() + ", " + valmax.str() +"]";
}

void DFGNode::printDOTEdge(FILE *file){
    EdgeContainer::const_iterator it;
    for(it = m_successors.begin() ; it != m_successors.end() ; it++) {
        fprintf(file, "n_%lu -> n_%lu [label=\"%u\"]; \n", this->getUniqueNumber(), (*it)->getTo()->getUniqueNumber(), ((DFGEdge*)(*it))->getInputNumber()) ;
    }
}
