#ifndef __DFGEDGE_HH__
#define __DFGEDGE_HH__

#include "BaseEdge.hh"
#include "DFGNode.hh"

class DFGNode;

class DFGEdge : public BaseEdge {
    private:
        unsigned int m_inputNumber;

    public:
        DFGEdge(DFGNode *from, DFGNode *to, unsigned int inputNumber);
        DFGEdge(const DFGEdge &other);
        DFGEdge* clone(BaseNode *from, BaseNode *to);

        unsigned int getInputNumber(){return m_inputNumber;}
        DFGNode* getFrom(){return (DFGNode*)BaseEdge::getFrom();}
        DFGNode* getTo(){return (DFGNode*)BaseEdge::getTo();}
};

#endif
