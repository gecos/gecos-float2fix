#include "ScalarNode.hh"
#include <cmath>
#include <sstream>

ScalarNode::ScalarNode(std::string name, int cdfgNumber):DataNode(name, cdfgNumber){
    m_value = NAN;
}

ScalarNode::ScalarNode(std::string name, int cdfgNumber, float value):DataNode(name, cdfgNumber){
    m_value = value;
}

ScalarNode::ScalarNode(const ScalarNode &other):DataNode(other){
    m_value = other.m_value;
}

std::string ScalarNode::getDotLabel(){
    std::stringstream ss;
    if(!std::isnan(getValue())){
        ss << std::scientific << getValue();
        return DataNode::getDotLabel() + "\\n"+ ss.str();
    }
    else{
        return DataNode::getDotLabel();
    }
}
