#include "ConstantNode.hh"

#include <utils/Tool.hh>
#include <sstream>
#include "DFGEdge.hh"

ConstantNode::ConstantNode(float value):DataNode("CONSTANT", -1){
    m_value = value;
}

ConstantNode::ConstantNode(const ConstantNode &other):DataNode(other){
    m_value = other.m_value;
}

void ConstantNode::checkAddingEdgeValidity(DFGEdge *edge){
    DataNode::checkAddingEdgeValidity(edge);
    
    if(edge->getTo() == this){
        trace_error("An constant node represent a constant of the system. It cannot have an predecessor. Impossible to add edge from %s to %s\n", edge->getFrom()->getName().c_str(), this->getName().c_str());
        exit(-1);
    }
}

std::string ConstantNode::getDotLabel(){
    std::stringstream ss;
    ss << std::scientific << getValue();
    return this->getName() + "\\n" + ss.str();
}
