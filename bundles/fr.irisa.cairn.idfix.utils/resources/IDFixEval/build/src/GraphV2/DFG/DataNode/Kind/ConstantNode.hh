#ifndef __CONSTANTNODE_HH__
#define __CONSTANTNODE_HH__

#include "DataNode.hh"

class ConstantNode : public DataNode{
    private:
        float m_value;

    protected:
        std::string getDotColor(){return "aquamarine";}
        std::string getDotLabel();

    public:
        ConstantNode(float value);
        ConstantNode(const ConstantNode &other);
        ConstantNode* clone(){return new ConstantNode(*this);}
        void checkAddingEdgeValidity(DFGEdge *edge);
        float getValue(){return m_value;}

};

#endif
