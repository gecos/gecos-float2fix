#include "SourceNode.hh"

#include <utils/Tool.hh>

#include "DFGEdge.hh"

SourceNode::SourceNode(std::string name, int cdfgNumber, double val_min, double val_max):DataNode(name, cdfgNumber){
    this->setDynamic(val_min, val_max);
}

SourceNode::SourceNode(const SourceNode &other):DataNode(other){}

void SourceNode::checkAddingEdgeValidity(DFGEdge *edge){
    DataNode::checkAddingEdgeValidity(edge);
    
    if(edge->getTo() == this){
        trace_error("An source node represent an input of the system. It cannot have an predecessor. Impossible to add edge from %s to %s\n", edge->getFrom()->getName().c_str(), this->getName().c_str());
        exit(-1);
    }
}
