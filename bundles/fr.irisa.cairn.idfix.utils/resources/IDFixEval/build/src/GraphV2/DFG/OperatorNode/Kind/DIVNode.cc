#include "DIVNode2.hh"
//#include "NoisePowerExpressionGeneration.hh"
//#include "MULNode.hh"
//#include "SUBNode.hh"

    DIVNode::DIVNode(int cdfg, int sfg, Block &block, unsigned int input_number):OperatorNode(cdfg, sfg, input_number, block,"DIV"){}
    DIVNode::DIVNode(const DIVNode &other):OperatorNode(other){}

    /*std::string DIVNode::WFPEffDetermination(){
        std::ostringstream oss;
        std::string str;

        oss << this->getNumSFG();
        // WFPeff[xOps][2] = WFP[xOps][2];
        str = "   " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+2]"; // Si 2 est la sortie
        str += " = " + __NOISEPOWER_WFPSFG__ + "[" + oss.str() + "*3+2];";

        return str;
    }

    std::string DIVNode::KDetermination(){
			// infini;
			return "999";
    }

    DataDynamic DIVNode::dynamicRule(unsigned int size, DataDynamic operand[]){
		DataDynamic dynOut;

        if (size != 2) {
            trace_fatal("Dynamic rules not implemented yet for an division operator with more of 2 operands\n");
            exit(-1);
        }

        if(operand[1].valMax != 0 && operand[1].valMin != 0) {
            // First part 1/[ymin, ymax]
            float tempMin, tempMax;
            DataDynamic firstPart;
            tempMin = 1 / operand[1].valMin;
            tempMax = 1 / operand[1].valMax;
            firstPart.valMin = min(tempMin, tempMax);
            firstPart.valMax = max(tempMin, tempMax);
            // Second part [xmin, xmax] * (1/[ymin, ymax])
            double tabPartialProduct[4] = { 0, 0, 0, 0 };
            tabPartialProduct[0] = operand[0].valMin * firstPart.valMin;
            tabPartialProduct[1] = operand[0].valMin * firstPart.valMax;
            tabPartialProduct[2] = operand[0].valMax * firstPart.valMin;
            tabPartialProduct[3] = operand[0].valMax * firstPart.valMax;
            dynOut.valMin = *min_element(tabPartialProduct, tabPartialProduct + 4); //+4 because read 4 data
            dynOut.valMax = *max_element(tabPartialProduct, tabPartialProduct + 4); //+4 because read 4 data
        }
        else {
            trace_fatal("Division by 0 detected. Impossible to continue the dynamic propagation\n");
            exit(-1);
        }

        return dynOut;
    }
    
    NoeudData* DIVNode::arithmeticOperationGeneration(fstream & matfile){
        NoeudData *nDataSucc, *nDataPred1, *nDataPred2;

        if(this->getNbInput() != 2) {
            trace_fatal("Arithmetic operation not implemented yet for an addition with more of 2 operands\n");
            exit(-1);
        }

        nDataSucc = dynamic_cast<NoeudData*> (this->getElementSucc(0));
        if(nDataSucc == NULL){
            trace_error("The successor node of a division operation is not a data node.\n");
            exit(0);
        }

        nDataPred1 = dynamic_cast<NoeudData*> (this->getElementPred(0));
        if(nDataPred1 == NULL){
            trace_error("The first predecessor node of a division operation is not a data node.\n");
            exit(0);
        }
        
        nDataPred2 = dynamic_cast<NoeudData*> (this->getElementPred(1));
        if(nDataPred2 == NULL){
            trace_error("The second predecessor node of a division operation is not a data node.\n");
            exit(0);
        }

        nDataPred1 = nDataPred1->genArithmeticOperation(matfile);
        nDataPred2 = nDataPred2->genArithmeticOperation(matfile);
        if(nDataPred1->getSrcSignal()){
            matfile << nDataSucc->getName().data() << " = " << nDataPred2->getName().data() << ";\n";
        }
        else if(nDataPred2->getSrcSignal()){
            matfile << nDataSucc->getName().data() << " = " << nDataPred1->getName().data() << ";\n";
        }
        else{
            matfile << nDataSucc->getName().data() << " = " << nDataPred1->getName().data() << " ./ " << nDataPred2->getName().data() << ";\n";
        }

        return nDataSucc;
    }
    
    void DIVNode::moveNoiseSource(NoeudSrcBruit* noiseNode){
        NoeudData *nodeDataPred, *nodeDataSuiv, *nodeConstante;
        int numInputOps;

        if(RuntimeParameters::isLTIGraph() && !RuntimeParameters::isRecursiveGraph()){
            assert(this->getNbSucc() == 1 && noiseNode->getNbPred() == 1);
            //Data(nodeDataPred)-->br(noiseNode)--->Ops(this)--->Data(nodeDataSuiv)
            nodeDataPred = dynamic_cast<NoeudData*> (noiseNode->getElementPred(0));
            nodeDataSuiv = dynamic_cast<NoeudData*> (this->getElementSucc(0));
            assert(nodeDataPred != NULL && nodeDataSuiv != NULL);
            assert(nodeDataPred->getTypeNodeGraph() != INIT && nodeDataSuiv->getTypeNodeGraph() != FIN);

            //test if nodeData has not more than one successor
            if (nodeDataSuiv->getNbSucc() == 1 && !nodeDataSuiv->isNodeOutput()) {

                // === Sauvegarde des numInput pour les futures modification sur les edges
                numInputOps = noiseNode->getEdgeSucc(0)->getNumInput();

                assert(this->getNbPred() == 2);
                if( this->getElementPred(0) == noiseNode){
                    nodeConstante = dynamic_cast<NoeudData*> (this->getElementPred(1));
                    assert(nodeConstante != NULL);
                    if(!nodeConstante->isNodeConst()){
                        trace_error("Division par variable alors que le systeme est LTI\n");
                        exit(0);
                    }
                }
                else if( this->getElementPred(1) == noiseNode){
                    nodeConstante = dynamic_cast<NoeudData*> (this->getElementPred(0));
                    assert(nodeConstante != NULL);
                    if(!nodeConstante->isNodeConst()){
                        trace_error("Division par variable alors que le systeme est LTI\n");
                        exit(0);
                    }
                }
                else{
                    trace_error("Aucun noeud predecesseur de l'opération est le noeud Br à aggréger\n");
                    exit(0);
                }
                noiseNode->divConstante(nodeConstante->getValeur());

                // === Redirection des arcs
                // Data(nodeDataPred)-->br(noiseNode)-->Ops(this)-->Data(nodeDataSuiv)
                nodeDataPred->deleteEdgeDirectedTo(noiseNode);
                // Data(nodeDataPred)  :  br(noiseNode)-->Ops(this)-->Data(nodeDataSuiv)
                noiseNode->deleteEdgeDirectedTo(this);
                // Data(nodeDataPred)  :  br(noiseNode)  :  Ops(this)-->Data(nodeDataSuiv)
                this->deleteEdgeDirectedTo(nodeDataSuiv);


                // Data(nodeDataPred)  :  br(noiseNode)  :  Ops(this)  :  Data(nodeDataSuiv)
                nodeDataPred->edgeDirectedTo(this, numInputOps);
                // Data(nodeDataPred)-->Ops(this)  :  br(noiseNode)  :  Data(nodeDataSuiv)
                noiseNode->edgeDirectedTo(nodeDataSuiv);
                // Data(nodeDataPred)-->Ops(this)  :  br(noiseNode)-->Data(nodeDataSuiv)
                this->edgeDirectedTo(noiseNode);
                // Data(nodeDataPred)-->Ops(this)-->br(noiseNode)-->Data(nodeDataSuiv)

                assert(noiseNode->getNbSucc() == 1);
                if (nodeDataSuiv->getTypeNodeData() == DATA_SRC_BRUIT) {
                    // The node just below us is a noise node, it is our only successor and it is in the same block as the other noise source we are handling
                    //we need to merge 2 noise sources: noiseNode and nodeGraphSuiv
                    //we add current noise source parameter to next noise source: nodeGraphSuiv

                    ((NoeudSrcBruit *) nodeDataSuiv)->calculSumParamNoise((NoeudSrcBruit *) (noiseNode)); //add parameter

                    //noeudPred-->OPS-->Br(noiseNode)-->Br(nodeGraphSuiv)-->OPS   before
                    this->deleteEdgeDirectedTo(noiseNode);
                    noiseNode->deleteEdgeDirectedTo(nodeDataSuiv);
                    this->edgeDirectedTo(nodeDataSuiv);
                    //noeudPred-->OPS-->Br(nodeGraphSuiv)-->OPS   after

                    //no recursion to continue move: this is end of process on noiseNode which is merged
                    //in nodeGraphSuiv noise source-->these noise source is processed after with the tabNoiseSourcesNode

                }
                else
                    ((NoeudGFD*)noiseNode->getElementSucc(0))->moveNoiseSource(noiseNode); // Appel recurcif
            }
        }
    }
            
    FonctionLineaire* DIVNode::computeRuleLinearFunction(unsigned int size, FonctionLineaire* operand[]){
        FonctionLineaire *result;
        if (size != 2) {
            trace_fatal("Linear function rules not implemented yet for an division with more of 2 operands\n");
            exit(-1);
        }
			
        result = *operand[0] / *operand[1];
        delete operand[0];
        delete operand[1];

        return result;
    }

    void DIVNode::noiseModelInsertion(Gfd *gfd){
        int nbSucc;
        int nbPred;

        char s[30];

        OperatorNode *OpsMultBruit1 = NULL;
        OperatorNode *OpsMultSignal2 = NULL;
        OperatorNode *OpsDivBruit1 = NULL;
        OperatorNode *OpsDivSignal2 = NULL;
        OperatorNode *OpsSousBruit = NULL;

        NoeudGraph *NG = NULL;
        NoeudGFD *NodeGFD = NULL;

        NoeudData *NDataVar1 = NULL;
        NoeudData *NDataVar2 = NULL;
        NoeudData *NDataVar3 = NULL;
        NoeudData *NDataVar4 = NULL;

        NoeudData *NodeData = NULL;

        NoeudData *NodeDataBruit[2] = { NULL, NULL };
        NoeudData *NodeDataSignal[2] = { NULL, NULL };

        int IndiceSignal;

        TypeNodeGFD typeNodeGFD;
        TypeSignalBruit typeSignalBruit;

        IndiceSignal = 0;

        // Le modele de bruit d'une division(OPS_SIGNAL) est obtenue avec deux multiplications une division et une soustraction (OPS_BRUIT)
        //
        //On a a la base, une division (OPS_SIGNAL) avec les entrees et sortie doublees(DATA_SIGNAL et DATA_BRUIT)
        OpsDivBruit1 = this->clone();
        OpsDivBruit1->setTypeSignalBruit(BRUIT);
        gfd->addOPNode(OpsDivBruit1);
        OpsDivSignal2 = this->clone();
        OpsDivSignal2->setTypeSignalBruit(SIGNAL);
        gfd->addOPNode(OpsDivSignal2);

        int temp = gfd->getNbNode() + 1;
        sprintf(s, "VarInt(%d)", temp);
        NDataVar1 = gfd->addNoeudData(s, new TypeVar(VAR), BRUIT);
        NDataVar1->getTypeVar()->setTypeVarFonct(VAR);

        temp = gfd->getNbNode() + 1;
        sprintf(s, "VarInt(%d)", temp);
        NDataVar2 = gfd->addNoeudData(s, new TypeVar(VAR), SIGNAL);
        NDataVar2->getTypeVar()->setTypeVarFonct(VAR);

        temp = gfd->getNbNode() + 1;
        sprintf(s, "VarInt(%d)", temp);
        NDataVar3 = gfd->addNoeudData(s, new TypeVar(VAR), BRUIT);
        NDataVar3->getTypeVar()->setTypeVarFonct(VAR);

        temp = gfd->getNbNode() + 1;
        sprintf(s, "VarInt(%d)", temp);
        NDataVar4 = gfd->addNoeudData(s, new TypeVar(VAR), SIGNAL);
        NDataVar4->getTypeVar()->setTypeVarFonct(VAR);

        OpsMultBruit1 = new gfd::MULNode(this->getBlock());
        OpsMultBruit1->setTypeSignalBruit(BRUIT);
        gfd->addOPNode(OpsMultBruit1);
        OpsMultSignal2 = new gfd::MULNode(this->getBlock());
        OpsMultSignal2->setTypeSignalBruit(SIGNAL);
        gfd->addOPNode(OpsMultSignal2);
        OpsSousBruit = new gfd::SUBNode(this->getBlock());
        OpsSousBruit->setTypeSignalBruit(BRUIT);
        gfd->addOPNode(OpsSousBruit);

        nbSucc = this->getNbSucc();
        for (int i = nbSucc - 1; i >= 0; i--) // Redirection des arcs succ
        {
            NG = this->getElementSucc(i);
            NodeGFD = (NoeudGFD *) NG;

            typeNodeGFD = NodeGFD->getTypeNodeGFD();

            if (typeNodeGFD == DATA) {
                NodeData = (NoeudData *) NodeGFD;

                typeSignalBruit = NodeData->getTypeSignalBruit();

                if (typeSignalBruit == BRUIT) {
                    this->deleteEdgeDirectedTo(NodeData); // On suprime les arcs (OPS_SIGNAL vers DATA_BRUIT)
                    OpsSousBruit->edgeDirectedTo(NodeData); // On ajoute les arcs (OPS_BRUIT vers DATA_BRUIT)
                }
            }
        }

        nbPred = this->getNbPred();
        for (int j = 0; j < nbPred; j++) // On repertorie les pred de la division (OPS_SIGNAL)
        {
            NG = this->getElementPred(j);
            NodeGFD = (NoeudGFD *) NG;

            typeNodeGFD = NodeGFD->getTypeNodeGFD();

            if (typeNodeGFD == DATA) {
                NodeData = (NoeudData *) NodeGFD;
                typeSignalBruit = NodeData->getTypeSignalBruit();

                if (typeSignalBruit == SIGNAL) {
                    NodeDataSignal[IndiceSignal] = NodeData;
                    IndiceSignal++;
                }

            }
        }

        for (int j = 0; j < nbPred; j++) // differenciation de la premiere et de la deuxieme entree
        {
            NG = this->getElementPred(j);
            NodeGFD = (NoeudGFD *) NG;

            typeNodeGFD = NodeGFD->getTypeNodeGFD();

            if (typeNodeGFD == DATA) {
                NodeData = (NoeudData *) NodeGFD;
                typeSignalBruit = NodeData->getTypeSignalBruit();

                if (typeSignalBruit == BRUIT) {
                    if (NodeGFD == NodeDataSignal[0]->getPtrclone()) {
                        NodeDataBruit[0] = NodeData; //1ere entree
                    }
                    else {
                        if (NodeGFD == NodeDataSignal[1]->getPtrclone()) {
                            NodeDataBruit[1] = NodeData; //2eme entree
                        }
                        else {
                            trace_error("Error during Signal and Noise binding");
                            exit(-1);
                        }

                    }

                }

            }
        }

        this->EnleveedgeFromOf(NodeDataBruit[0]);
        this->EnleveedgeFromOf(NodeDataBruit[1]);


        NodeDataBruit[0]->edgeDirectedTo(OpsDivBruit1, 0);
        NodeDataSignal[1]->edgeDirectedTo(OpsDivBruit1, 1); //

        NodeDataBruit[1]->edgeDirectedTo(OpsMultBruit1, 1); //
        NDataVar4->edgeDirectedTo(OpsMultBruit1, 0); //

        NodeDataSignal[1]->edgeDirectedTo(OpsMultSignal2, 0); //
        NodeDataSignal[1]->edgeDirectedToRapide(OpsMultSignal2, 1); // Rapide (permet de ne pas faire le test si il existe déja)



        OpsMultBruit1->edgeDirectedTo(NDataVar1); // Ajouts d'arcs de connexion entre les OPS_BRUIT.
        OpsMultSignal2->edgeDirectedTo(NDataVar2); //

        NodeDataSignal[0]->edgeDirectedTo(OpsDivSignal2, 0); // Ajouts d'arcs de connexion entre les OPS_BRUIT.
        NDataVar2->edgeDirectedTo(OpsDivSignal2, 1); //

        OpsDivBruit1->edgeDirectedTo(NDataVar3); //
        OpsDivSignal2->edgeDirectedTo(NDataVar4); //


        NDataVar3->edgeDirectedTo(OpsSousBruit, 0); //
        NDataVar1->edgeDirectedTo(OpsSousBruit, 1); //
    }
*/
    /**
     *  Return a TypeLti in terms of the operation node
     *  	\return : TypeLti
     *
     *  DIV =>  CONST/CONST -> CONST
     *  		SIGNAL/CONST -> SIGNAL
     *  		other -> NOT_LTI
     *
     *  Working only to operation node with 1 or 2 input
     *  \author Nicolas Simon
     *  \date 09/07/2010
     */
   /* TypeLti DIVNode::isLTI(){
        if(this->getNbInput() != 2){
            trace_fatal("LTI detection not implemented yet for an division with more of 2 operands\n");
            exit(-1);
        }

        NoeudData *NData1;
        NoeudData *NData2;
        TypeLti TypeOp1;
        TypeLti TypeOp2;
        TypeLti Res;

        NData1 = dynamic_cast<NoeudData *> (this->getElementPred(0));
        assert(NData1 != NULL);
        TypeOp1 = NData1->isLTI();
        NData2 = dynamic_cast<NoeudData *> (this->getElementPred(1));
        assert(NData2 != NULL);
        TypeOp2 = NData2->isLTI();

        if (TypeOp1 == NOT_LTI || TypeOp2 == NOT_LTI) {
            Res = NOT_LTI;
        }
        else if (TypeOp2 == SIGNAL_LTI) {
            Res = NOT_LTI;
        }
        else if (TypeOp1 == SIGNAL_LTI) {
            Res = SIGNAL_LTI;
        }
        else {
            Res = CONST_LTI;
        }

        return Res;
    }*/
