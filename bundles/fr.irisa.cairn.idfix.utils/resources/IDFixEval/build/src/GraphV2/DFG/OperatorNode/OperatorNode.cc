#include "OperatorNode.hh"

#include <utils/Tool.hh>
#include <sstream>
#include "DataNode.hh"
#include "DFGEdge.hh"

OperatorNode::OperatorNode(int cdfg_number, int sfg_number, int inputNumber, Block &block, std::string name) :
	DFGNode(name, cdfg_number), m_block(block) {
	m_inputNumber = inputNumber;
	m_SFGNumber = sfg_number; //only set during parse of input xml file
}

OperatorNode::OperatorNode(const OperatorNode &other):DFGNode(other), m_block(other.m_block){
	this->m_inputNumber = other.m_inputNumber;
	this->m_SFGNumber = other.m_SFGNumber;
}

/*OperatorNode::~OperatorNode() {

}*/

void OperatorNode::checkAddingEdgeValidity(DFGEdge *edge){
    BaseNode *from, *to;
    from = edge->getFrom();
    to = edge->getTo();
    if(from == this){
        if(this->getNbSuccessors() == 1){
            trace_error("The operator %s have already a successor. Impossible to add a new one\n", this->getName().c_str());    
            exit(-1);
        } 
    }
    else if(to == this){
        if(edge->getInputNumber() >= this->getInputNumber()){
            trace_error("Trying to add an new egde at the operator input %u while the maximum input number of the operator %s is %u\n", edge->getInputNumber(), this->getName().c_str(), this->getInputNumber());
            exit(-1);
        }
        else{
            //TODO add the cheking precence of edge in the input to add
        }
    }
    else{
        trace_error("The edge is not build to be connect to the operator node %s", this->getName().c_str());
        exit(-1);
    }
}

std::string OperatorNode::getDotLabel(){
    std::stringstream cdfg, sfg, block;
    cdfg << this->getCDFGNumber();
    sfg << this->getNumSFG();
    block << this->getBlock().getID();
    return DFGNode::getDotLabel() + " - SFG : " + sfg.str() + "\\n Block : " + block.str();
}
