#include "DELAYNode2.hh"
//#include "NoisePowerExpressionGeneration.hh"

    DELAYNode::DELAYNode(int cdfg, int sfg, Block &block):OperatorNode(cdfg, sfg, 1, block, "DELAY"){}
    DELAYNode::DELAYNode(const DELAYNode &other):OperatorNode(other){}

    /*std::string DELAYNode::WFPEffDetermination(){
        std::ostringstream oss;
        std::string str;

        oss << this->getNumSFG();
        // WFPeff[xOps][2] = min(WFP[xOps][2], WFPeff[xOps][0]);
        str = "   " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+2]"; // Si 2 est la sortie
        str += " = min(" + __NOISEPOWER_WFPSFG__ + "[" + oss.str() + "*3+2],";
        str += " " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+0]);";

        return str;

    }

    std::string DELAYNode::KDetermination(){
        std::ostringstream oss;
        std::string str;
    
        oss << this->getNumSFG();
        // WFP_sfg_eff[xOps][0] - WFP_sfg[xOps][2];
        str = __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+0]"; // Si 2 est la sortie
        str += " - " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+2]";
        return str;
    }

    DataDynamic DELAYNode::dynamicRule(unsigned int size, DataDynamic operand[]){
		DataDynamic dynOut;
        
        if(size != 1){
            trace_error("A delay operator cannot have more one operand\n");
            exit(-1);
        }

        dynOut.valMin = operand[0].valMin;
        dynOut.valMax = operand[0].valMax;

		return dynOut;
    }
    
    NoeudData* DELAYNode::arithmeticOperationGeneration(fstream & matfile){
        trace_error("Delay node cannot generate non-LTI arithmetic operation. Error during graph transformation\n");
        exit(EXIT_FAILURE);
    }
            
    FonctionLineaire* DELAYNode::computeRuleLinearFunction(unsigned int size, FonctionLineaire* operand[]){
        FonctionLineaire *result;
        if (size != 1) {
            trace_fatal("Linear function rules not implemented yet for an delay with more of 1 operands\n");
            exit(-1);
        }
        
        operand[0]->operatorDelay();
        result = operand[0];

        return result;
    }

    void DELAYNode::noiseModelInsertion(Gfd *gfd){
        int i;
        int j;
        int numInput;
        NoeudGraph *NG = NULL;
        NoeudGFD *NGFD = NULL;
        NoeudData *ND = NULL;
        OperatorNode *NO1 = NULL;
        TypeSignalBruit typeSignalBruit;
        TypeNodeGFD TypeNGFD;

        // Le modele de bruit d'un retard(OPS_SIGNAL) est obtenue avec un retard (OPS_BRUIT)
        //
        //On a � la base, un retard (OPS_SIGNAL) avec les entr�es et sortie doubl�es(DATA_SIGNAL et DATA_BRUIT)
        NO1 = this->clone();
        NO1->setTypeSignalBruit(BRUIT);
        gfd->addOPNode(NO1);


        for (i = 0; i < this->getNbSucc(); i++) // Redirection des arcs succ
        {
            NG = this->getElementSucc(i);
            NGFD = (NoeudGFD *) NG;
            TypeNGFD = NGFD->getTypeNodeGFD();
            if (TypeNGFD == DATA) {
                ND = (NoeudData *) NGFD;
                typeSignalBruit = ND->getTypeSignalBruit();
                if (typeSignalBruit == BRUIT) {
                    this->deleteEdgeDirectedTo(ND); // On suprime les arcs (OPS_SIGNAL vers DATA_BRUIT)
                    NO1->edgeDirectedTo(ND); // On ajoute les arcs (OPS_BRUIT vers DATA_BRUIT)
                    i--;
                }
            }
        }
        for (j = 0; j < this->getNbPred(); j++) // Redirection des arcs pred
        {
            NG = this->getElementPred(j);
            NGFD = (NoeudGFD *) NG;
            TypeNGFD = NGFD->getTypeNodeGFD();
            if (TypeNGFD == DATA) {
                ND = (NoeudData *) NGFD;
                typeSignalBruit = ND->getTypeSignalBruit();
                if (typeSignalBruit == BRUIT) {
                    // Sauvegarde du numInput avant modification des edges
                    numInput = -1;
                    vector<Edge *>::iterator itPredOps = this->getListePred()->begin();
                    while (itPredOps != this->getListePred()->end()) {
                        if ((*itPredOps)->getNodePred() == ND) {
                            numInput = (*itPredOps)->getNumInput();
                            break;
                        }
                        itPredOps++;
                    }
                    assert(numInput != -1); // numInput doit être forcement affecté au dessus
                    this->EnleveedgeFromOf(ND); // On suprime les arcs (DATA_BRUIT vers OPS_SIGNAL)
                    NO1->edgeFromOf(ND, numInput-this->getNbInput()); // On ajoute les arcs (DATA_BRUIT vers OPS_BRUIT)
                }
            }
        }

    }
*/
    /**
     *  Return a TypeLti in terms of the operation node
     *  	\return : TypeLti
     *  \author Nicolas Simon
     *  \date 09/07/2010
     */
  /*  TypeLti DELAYNode::isLTI(){
        NoeudData *NData1;

        NData1 = dynamic_cast<NoeudData *> (this->getElementPred(0));
        assert(NData1 != NULL);
        return NData1->isLTI();
   }*/
