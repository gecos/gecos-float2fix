#include "BaseNode.hh"

#include <utils/Tool.hh>

BaseNode::BaseNode(std::string name) {
    m_name = name;
    m_owner = NULL;
    m_state = NONVISITE;
}

BaseNode::BaseNode(const BaseNode &other){
    m_name = other.m_name;
    m_owner = NULL;
    m_state = NONVISITE; 
}

BaseNode::~BaseNode(){
    EdgeContainer::iterator it, next;
    for(next = it = this->m_predecessors.begin() ; it != this->m_predecessors.end() ; it = next ){
        ++next;
        (*it)->getFrom()->deleteAllEdgeTo(this);
        trace_debug("Delete edge from %s\n", (*it)->getFrom()->getName().c_str());
        delete (*it);
    }

    for(next = it = this->m_successors.begin() ; it != this->m_successors.end() ; it = next ){
        ++next;
        (*it)->getTo()->deleteAllEdgeFrom(this);
        trace_debug("Delete edge to %s\n", (*it)->getTo()->getName().c_str());
        delete (*it);
    }
}

void BaseNode::deleteAllEdgeFrom(BaseNode* node){
    EdgeContainer::iterator it, next;
    for(next = it = m_predecessors.begin() ; it != m_predecessors.end() ; it = next){
        ++next;
        if((*it)->getFrom() == node)
            m_predecessors.erase(it);
    }
}

void BaseNode::deleteAllEdgeTo(BaseNode* node){
    EdgeContainer::iterator it, next;
    for(next = it = m_successors.begin() ; it != m_successors.end() ; it = next){
        ++next;
        if((*it)->getTo() == node)
            m_successors.erase(it);
    }
}

std::string BaseNode::getDotLabel(){
    return this->getName();
}

std::string BaseNode::getDotForm(){
    return "style=\"filled\" fillcolor=\"" + getDotColor() + "\"";
}

void BaseNode::printDOTNode(FILE *file){
	fprintf(file,"n_%lu [label=\"%s\" %s];\n", this->getUniqueNumber(), this->getDotLabel().c_str(), this->getDotForm().c_str());
}

void BaseNode::printDOTEdge(FILE *file){
    EdgeContainer::const_iterator it;
    for(it = m_successors.begin() ; it != m_successors.end() ; it++) {
        fprintf(file, "n_%lu -> n_%lu ; \n", this->getUniqueNumber(), (*it)->getTo()->getUniqueNumber()) ;
    }
}
