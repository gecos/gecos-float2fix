#ifndef __BASEGRAPH_HH__
#define __BASEGRAPH_HH__

#include <list>
#include "BaseNode.hh"
#include "BaseEdge.hh"

class BaseNode;
class BaseEdge;

class BaseGraph{
    private:
        std::list<BaseNode*> m_vertex;
        unsigned long m_unique_number;
        
    protected:
        BaseGraph();
        BaseGraph(const BaseGraph &other);

        void addNode(BaseNode *node);
        void addEdge(BaseNode *from, BaseNode *to, BaseEdge *edge);
    public:
        const std::list<BaseNode*>* getNodes(){return &m_vertex;}
        unsigned int getNbNodes(){return m_vertex.size();}
        bool contain(BaseNode *node);
        void resetNodeStatus();

        void removeNode(BaseNode *node);

        void printDOT(std::string filename);
};


#endif

