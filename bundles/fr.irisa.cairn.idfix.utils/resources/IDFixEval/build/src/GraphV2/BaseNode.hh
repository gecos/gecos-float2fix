#ifndef __BASENODE_HH__
#define __BASENODE_HH__

#include <graph/toolkit/Types.hh>
#include <string>
#include <list>
#include <stdio.h>

#include "BaseGraph.hh"
#include "BaseEdge.hh"

class BaseGraph;
class BaseEdge;

typedef std::list<BaseEdge*> EdgeContainer;

class BaseNode {
    friend class BaseGraph;
    private:
        unsigned long m_unique_number;
        BaseGraph *m_owner;
        std::string m_name; // Name of the node 
        TypeEtat m_state;
    
    protected:
        BaseNode(std::string name);
        BaseNode(const BaseNode &other);

        EdgeContainer m_predecessors;
        EdgeContainer m_successors;

        void deleteAllEdgeFrom(BaseNode* node);
        void deleteAllEdgeTo(BaseNode* node);
    
        virtual std::string getDotColor(){return "";}
        virtual std::string getDotLabel();
        virtual std::string getDotForm();
    public:
        virtual BaseNode* clone() = 0;
        ~BaseNode();

        std::string getName(){return m_name;}
        TypeEtat getState(){return m_state;}
        void setState(TypeEtat state){m_state = state;}
        unsigned long getUniqueNumber(){return m_unique_number;}

        unsigned int getNbPredecessors(){return m_predecessors.size();}
        unsigned int getNbSuccessors(){return m_successors.size();}
        const EdgeContainer* getPredecessors() {return &m_predecessors;}
        const EdgeContainer* getSuccessors(){return &m_successors;}

        BaseGraph* getOwner(){return m_owner;}

        bool isRoot(){return getNbPredecessors() == 0;}
        bool isLeaf(){return getNbSuccessors() == 0;}

        virtual void printDOTNode(FILE *file);
        virtual void printDOTEdge(FILE *file);
};

#endif
