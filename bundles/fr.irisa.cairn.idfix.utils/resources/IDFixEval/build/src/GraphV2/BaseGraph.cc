#include "BaseGraph.hh"

#include <stdio.h>
#include <utils/Tool.hh>
#include <map>

BaseGraph::BaseGraph(){
    m_unique_number = 0;
}

BaseGraph::BaseGraph(const BaseGraph &other){
    this->m_unique_number = 0;

    std::map<BaseNode*, BaseNode*> copier;
    BaseNode *node, *nodeFrom, *nodeTo;
    BaseEdge *edge;
    std::list<BaseNode*>::const_iterator it;
    for(it = other.m_vertex.begin() ; it != other.m_vertex.end() ; it++){
        node = (*it)->clone(); 
        copier[(*it)] = node;
        this->addNode(node);
    }
    
    EdgeContainer::const_iterator itEdge;
    for(it = other.m_vertex.begin() ; it != other.m_vertex.end() ; it++){
        for(itEdge = (*it)->getSuccessors()->begin() ; itEdge != (*it)->getSuccessors()->end() ; itEdge++){
            nodeFrom = copier[(*itEdge)->getFrom()];
            nodeTo = copier[(*itEdge)->getTo()];
            edge = (*itEdge)->clone(nodeFrom, nodeTo);
            this->addEdge(nodeFrom, nodeTo, edge);
        }
    }
}

bool BaseGraph::contain(BaseNode *node){
    std::list<BaseNode*>::const_iterator it;
    for(it = m_vertex.begin() ; it != m_vertex.end() ; it++){
        if((*it) == node)
            return true;
    }

    return false;
}

void BaseGraph::resetNodeStatus(){
    std::list<BaseNode*>::const_iterator it;
    for(it = m_vertex.begin() ; it != m_vertex.end() ; it++){
        (*it)->setState(NONVISITE);
    }

}

void BaseGraph::addNode(BaseNode *node){ 

    if(this->contain(node)){
        trace_error("node %s already contained in the graph. Impossible to add it\n", node->getName().c_str());
        exit(-1);
    }
    else{
        node->m_owner = this;
        node->m_unique_number = m_unique_number;
        m_unique_number++;
        m_vertex.push_back(node);
    }
}

void BaseGraph::addEdge(BaseNode *from, BaseNode *to, BaseEdge *edge){
    if(from->getOwner() != this){
        trace_error("node %s is not contained in the graph. Impossible to add an edge from it\n", from->getName().c_str());
        exit(-1);
    }
    else if(to->getOwner() != this){
        trace_error("node %s is not contained in the graph. Impossible to add an edge to it\n", to->getName().c_str());
        exit(-1);
    }
    else if(edge->getFrom() != from){
        trace_error("the edge to add have not the right \"from\" node information. Impossible to add this edge\n");
        exit(-1);
    }
    else if(edge->getTo() != to){
        trace_error("the edge to add have not the right \"to\" node information. Impossible to add this edge\n");
        exit(-1);
    }
    else{
        from->m_successors.push_back(edge);
        to->m_predecessors.push_back(edge);
    }
}

void BaseGraph::removeNode(BaseNode *node){
    if(node->getOwner() == this){
        std::list<BaseNode*>::iterator it2;
        for(it2 = m_vertex.begin() ; it2 != m_vertex.end() ; it2++){
            if((*it2) == node){
                m_vertex.erase(it2);
                break;
            }
        }

        delete node;
    }
}

void BaseGraph::printDOT(std::string filename){
    std::list<BaseNode*>::const_iterator it;
    FILE *file;
    std::string full_filename = ProgParameters::getOutputDirectory() + filename + ".dot";

    file = fopen(full_filename.c_str() ,"w");
    if(file == NULL){
        trace_error("Error during the opening of the file dot for saving the graph");
        exit(-1);
    }
    else{
        fprintf(file, "digraph G{\n");

        for(it = m_vertex.begin() ; it != m_vertex.end() ; it++){
            (*it)->printDOTNode(file);        
        }

        for(it = m_vertex.begin() ; it != m_vertex.end() ; it++){
            (*it)->printDOTEdge(file);        
        }

        fprintf(file, "}");
        fclose(file);
    }
}
