/**
 *  \file TypeVar.cc
 *  \author Daniel Menard, Jean-Charles Naud
 *  \date   31.01.2002
 *  \brief Classe definissant les TypesVar
 */

// === Bibliothèques .hh associe
#include "graph/toolkit/TypeVar.hh"

// ======================================= Constructeurs - Destructeurs
/**
 *  Constructeur de la classe TypeVar
 *		\param typeVarFonct	Type de variable
 *		\param typeVarPortee	Portée de la variable dans le code source
 *		\param typeVarRef		typeVarReference de la variable
 *		\param typeVarStruct	typeVarStructure associee a la variable
 *		\param typeVarReb		Type de rebouclage
 */
TypeVar::TypeVar(TypeVarFonct typeVarFonct) {
//TypeVar::TypeVar(TypeVarFonct typeVarFonct, TypeVarPortee typeVarPortee, TypeVarRef typeVarRef, TypeVarStruct typeVarStruct, TypeVarReb typeVarReb) {
	this->typeVarFonct = typeVarFonct; // Type de variable ( VAR_ENTREE, VAR_SORTIE, VAR, VAR_INT, CONST, CONST_SORTIE)
//	this->typeVarPortee = typeVarPortee; // Porte de la variable dans le code source (INTER, GLOBAL, LOCAL, EXTERN)
//	this->typeVarRef = typeVarRef; // TypeVarReference de la variable (DIRECTE,POINTEUR)
//	this->typeVarStruct = typeVarStruct; // TypeVarStructure associee a la variable (SCALAIRE, TABLEAU)
//	this->typeVarReb = typeVarReb; // Type de rebouclage (AUCUN, INITIALISATION, REBOUCLAGE, VIEILLISSEMENT, CYCLE)
}

/**
 * Destructeur de la classe TypeVar
 */
TypeVar::~TypeVar() {

}

// ======================================= Methodes
/**
 * Clonage de la classe
 * \Return la classe cloné
 */
TypeVar *TypeVar::clone() {
	TypeVarFonct typeVarFonct = (this)->getTypeVarFonct();
//	TypeVarPortee typeVarPortee = (this)->getTypeVarPortee();
//	TypeVarRef typeVarRef = (this)->getTypeVarRef();
//	TypeVarStruct typeVarStruct = (this)->getTypeVarStruct();
//	TypeVarReb typeVarReb = (this)->getTypeVarReb();

	TypeVar *TypeVarclone = new TypeVar(typeVarFonct);

	return TypeVarclone;
}
