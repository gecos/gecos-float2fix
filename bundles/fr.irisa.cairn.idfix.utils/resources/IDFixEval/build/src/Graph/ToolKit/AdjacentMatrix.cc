/**
 * \file AdjacentMatrix.cc
 * \author Loic Cloatre
 * \date   09.12.2008
 * \brief Classe definissant la matrice adjacente
 * The adjacent matrix is another way of storing a graph.
 * The matrix is a 2d array where each line represents a node, and each element in the line
 * is a successor of this node. This compact representation allows fast exploration of the
 * graph, but isn't as convenient for usual graph algorithms. The matrix is best suited for random-access
 * operations in the graph, not directly linked to its structure. Each node can be accessed directly.
 *
 * This class is built around a C-style dynamic array, with lines of different length. This is because
 * C++ style objects (like vectors) are too slow.
 *
 * It's used for finding circuits in the GFD.
 */

// === Bibliothèques .hh associées
#include "graph/toolkit/AdjacentMatrix.hh"

// ======================================= Constructeurs - Destructeurs
/**
 *
 */
AdjacentMatrix::AdjacentMatrix(int Taille) {
	tab = new LigneMatrice *[Taille];
	this->Taille = Taille;

	for (int i = 0; i < this->Taille; i++) {
		tab[i] = NULL;
	}
}

/**
 *
 */
AdjacentMatrix::AdjacentMatrix(LigneMatrice * Ligne, int Taille) {

	this->Taille = Taille - 1;
	tab = new LigneMatrice *[Taille - 1];

	tab[0] = (LigneMatrice *) malloc(sizeof(int) * (Ligne->size + 1));
	tab[0]->size = Ligne->size;
	for (int i = 0; i < tab[0]->size; i++) {
		tab[0]->contenu[i] = Ligne->contenu[i];
	}

	for (int i = 1; i < this->Taille; i++) {
		tab[i] = NULL;
	}
}

/**
 *
 */
AdjacentMatrix::AdjacentMatrix(LigneMatrice * Init, LigneMatrice * Fin, int Taille) {

	this->Taille = Taille;
	tab = new LigneMatrice *[Taille];

	tab[0] = (LigneMatrice *) malloc(sizeof(int) * 2);
	tab[0]->size = 1;
	tab[0]->contenu[0] = 0;

	tab[Taille - 1] = (LigneMatrice *) malloc(sizeof(int) * 2);
	tab[Taille - 1]->size = 1;
	tab[Taille - 1]->contenu[0] = 0;

	for (int i = 1; i < Taille - 1; i++) {
		tab[i] = NULL;
	}
}

/**
 *
 */
AdjacentMatrix::~AdjacentMatrix() {

	for (int i = 0; i < this->Taille; i++) {
		free(tab[i]);
	}
	delete[] tab;
}

// ======================================= Methodes
/**
 * TypeVarFonction pour recuperer la valeur de la celulle d'une matrice
 * \param numLigne : Définie la ligne
 * \param numColonne : Définie la collonne
 * \return la valeur -1 si erreur
 */
int AdjacentMatrix::getCelulle(int numLigne, int numColonne) {
	if (tab[numLigne]->size < numColonne) {
		return -1;
	}
	else {
		return tab[numLigne]->contenu[numColonne];
	}
}

/**
 * TypeVarFonction pour recuperer la longueur d'une ligne
 * \param numLigne : Définie la numeroLigne
 * \return la ligne -1 si erreur
 */
int AdjacentMatrix::getLongueurLigne(int numLigne) {

	if (tab[numLigne] == NULL)
		return -1;
	else
		return tab[numLigne]->size;

}

/**
 * Retourne la valeur de la celulle d'une matrice
 * \param numLigne : Définie la numeroLigne
 * \param valeur : Valeur de la constante
 * \return 1 Création, 0 Écrasement
 */
int AdjacentMatrix::addCelulle(const int numLigne, const int valeur) {
	if (tab[numLigne] != NULL) {
		tab[numLigne] = (LigneMatrice *) realloc(tab[numLigne], (tab[numLigne]->size + 2) * sizeof(int));
		tab[numLigne]->contenu[tab[numLigne]->size] = valeur;
		tab[numLigne]->size++;
		return 1;
	}
	else {
		tab[numLigne] = (LigneMatrice *) malloc(sizeof(int) * 2);
		tab[numLigne]->size = 1;
		tab[numLigne]->contenu[0] = valeur;
		return 0;
	}
}

/**
 * Affecte une cellule
 * \param numLigne : numéro de la ligne
 * \param numColonne : numéro de la colonne
 * \param valeur : valeur à affecter à la cellule
 */
void AdjacentMatrix::setCellule(int numLigne, int numColonne, int valeur) {
	if (numLigne < this->Taille) {
		if (numColonne < this->tab[numLigne]->size) {
			this->tab[numLigne]->contenu[numColonne] = valeur;
		}
		else {
			trace_error("indice colonne trop grand: %i sur la ligne %i", numColonne, numLigne);
			exit(-1);
		}
	}
	else {
		trace_error("indice ligne trop grand: %i", numLigne);
		exit(-1);
	}
}

/**
 * Affiche la matrice
 */
void AdjacentMatrix::displayMatrice() {
	cout << "Taille de la matrice affichage : " << this->Taille << endl;
	//cout<<"affichage matrice adjacente: "<<endl;
	for (int i = 0; i < this->Taille; i++) {
		cout << "listSucc du noeud " << i + 1 << ": [";
		for (int j = 0; j < this->getLongueurLigne(i); j++) {
			cout << this->getCelulle(i, j);
			if (j + 1 < this->getLongueurLigne(i))
				cout << " , ";
		}
		cout << " ]" << endl;
	}
}

/**
 * Affiche la ligne de la matrice
 * \param numLigne : Numéro de la ligne
 */
void AdjacentMatrix::displayLigneMatrice(LigneMatrice * numLigne) {
	if (numLigne != NULL) {
		cout << "[";
		for (int i = 0; i < numLigne->size; i++) {
			cout << numLigne->contenu[i] << " ";
		}
		cout << " ]" << endl;
	}
}

/**
 *  Clear a line in the matrix (and free the memory)
 */
void AdjacentMatrix::resetLigneMatrice(int numero) {
	free(tab[numero]);
	tab[numero] = NULL;
}

/**
 *  typeVarFonction qui remet a 0 la matrice
 */
void AdjacentMatrix::resetMatrice() {
	for (int i = 0; i < Taille; i++) {
		this->resetLigneMatrice(i);
	}
	//this->mOrigine.clear();
	//this->CreerMatriceInt(nbLignes, nbColonnes);
}
