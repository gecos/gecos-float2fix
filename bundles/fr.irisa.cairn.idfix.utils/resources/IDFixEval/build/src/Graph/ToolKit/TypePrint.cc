/**
 * \file Typeprint.cc
 * \author  Jena-Charles Naud
 * \date   1.09.2008
 * \brief Sources definissant les fonction globale de Convertion en string de Type
 */

// === Bibliothèques .hh associe
#include <cstdio>

#include "graph/toolkit/TypePrint.hh"

// === Bibliothèques non standard

// === Namespaces
using namespace std;

/**
 * 	Convertion en string de TypeVarFonct
 */
string typetypeVarFonctConvToStr(TypeVarFonct typeVarFonct) {
	switch (typeVarFonct) {
	case VAR_ENTREE: {
		return string("VAR_ENTREE");
	}
		break;
	case VAR_SORTIE: {
		return string("VAR_SORTIE");
	}
		break;
	case VAR: {
		return string("VAR");
	}
		break;
	case VAR_INT: {
		return string("VAR_INT");
	}
		break;
	case CONST: {
		return string("CONST");
	}
		break;
	case CONST_SORTIE: {
		return string("CONST_SORTIE");
	}
		break;
	default: {
		return string("?");
	}
		break; // Symbole inconnue
	}
}

/**
 * 	Convertion en string de TypeSignalBruit
 */
string typeSignalBruitConvToStr(TypeSignalBruit SignalBruit) {
	switch (SignalBruit) {
	case SIGNAL: {
		return string("SIGNAL");
	}
		break;
	case BRUIT: {
		return string("BRUIT");
	}
		break;
	default: {
		return string("?");
	}
		break; // Symbole inconnue
	}
}

/**
 * 	Convertion en string de TypeNodeData
 */
string typeNodeDataConvToStr(TypeNodeData NodeData) {
	switch (NodeData) {
	case DATA_BASE: {
		return string("DATA_BASE");
	}
		break;
	case DATA_ARRAY: {
		return string("DATA_ARRAY");
	}
		break;
	case DATA_SRC_BRUIT: {
		return string("DATA_SRC_BRUIT");
	}
		break;
	case DATA_SRC: {
		return string("DATA_SRC");
	}
		break;
	default: {
		return string("?");
	}
		break; // Symbole inconnue
	}
}

/**
 * 	Convertion en string de TypeNodeGraph
 */
string typeNodeGraphConvToStr(TypeNodeGraph Graphe) {
	switch (Graphe) {
	case GFD: {
		return string("GFD");
	}
		break;
	case GFT: {
		return string("GFT");
	}
		break;
	case INIT: {
		return string("INIT");
	}
		break;
	case FIN: {
		return string("FIN");
	}
		break;
	default: {
		return string("?");
	}
		break; // Symbole inconnue
	}
}

/**
 * 	Convertion en string de TypeNodeGFD
 */
string typeNoeudGFDConvToStr(TypeNodeGFD NoeudGFD) {
	switch (NoeudGFD) {
	case OPS: {
		return string("OPS");
	}
		break;
	case DATA: {
		return string("DATA");
	}
		break;
	default: {
		return string("?");
	}
		break; // Symbole inconnue
	}
}

/**
 * 	Convertion en string de TypeEtat
 */
string typeEtatConvToStr(TypeEtat Etat) {
	switch (Etat) {
	case VISITE: {
		return string("VISITE");
	}
		break;
	case ENTRAITEMENT: {
		return string("ENTRAITEMENT");
	}
		break;
	case NONVISITE: {
		return string("NONVISITE");
	}
		break;
	default: {
		return string("?");
	}
		break; // Symbole inconnue
	}
}

/**
 * 	Convertion en string de TypeNodeXml
 */
string typeNodeXmlConvToStr(TypeNodeXml NodeXml) {
	switch (NodeXml) {
	case EDGE: {
		return string("EDGE");
	}
		break;
	case NODE: {
		return string("NODE");
	}
		break;
	default: {
		return string("?");
	}
		break; // Symbole inconnue
	}
}

/**
 * 	Convertion en string de TypeXmlOpsData
 */
string typeXmlOpsDataConvToStr(TypeXmlOpsData XmlOpsData) {
	switch (XmlOpsData) {
	case DATA_XML: {
		return string("DATA_XML");
	}
		break;
	case OPS_XML: {
		return string("OPS_XML");
	}
		break;
	default: {
		return string("?");
	}
		break; // Symbole inconnue
	}
}
