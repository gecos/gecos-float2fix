/*!
 *  \author Nicolas Simon
 *  \date   25.06.2010
 *  \version 1.0
 *  \class Edge
 *  \brief Class which mean edges (herited by EdgeGFD and EdgeGFL class)
 */

// === Bibliothèques .hh associe
#include "graph/Edge.hh"


// ======================================= Constructeurs - Destructeurs

/**
 * Constructeur de la classe Edge
 *	\param nodePred : pointeur sur le Noeud predecesseur cible
 *	\param nodeSucc : pointeur sur le Noeud successeur cible
 *	\param numInput : Numero d'opérande sur lequel l'edge arrive sur le noeud succ
 */
Edge::Edge(NoeudGraph* nodePred, NoeudGraph* nodeSucc, int numInput) {
	this->nodePred = nodePred;
	this->nodeSucc = nodeSucc;
	this->numEdge = -1; 
	this->numInput = numInput;
}
/**
 * Constructeur de copie 
 * \param other : Edge a copier
 *
 */
Edge::Edge(const Edge &other) :
	numInput(other.numInput), nodePred(other.nodePred), nodeSucc(other.nodeSucc) {

}
/**
 *	Fonction de copie
 *	\return un pointeur sur un nouveau objet Edge
 *
 */
Edge *Edge::clone() {
	return new Edge(*this);
}

Edge::~Edge() {
}

/*void Edge::print() {
	cout << "edge " << numEdge << " from " << this->nodePred->getNumero() << " to " << this->nodeSucc()->getNumero() << endl;
}*/

