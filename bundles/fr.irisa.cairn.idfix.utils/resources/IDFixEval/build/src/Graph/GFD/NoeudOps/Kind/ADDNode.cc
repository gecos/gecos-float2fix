#include <cmath>

#include "graph/dfg/operatornode/kind/ADDNode.hh"
#include "backend/NoisePowerExpressionGeneration.hh"

namespace gfd {
    ADDNode::ADDNode(Block &block, unsigned int input_number):NoeudOps(-1, input_number, block, "ADD"){}
    ADDNode::ADDNode(const ADDNode &other):NoeudOps(other){}

    ADDNode* ADDNode::clone(){
        return new ADDNode(*this);
    }

    std::string ADDNode::WFPEffDetermination(){
        std::ostringstream oss; 
        std::string str;

        oss << this->getNumSFG();
        // WFP_sfg_eff[xOps][2] = min(WFP_sfg[xOps][2], max(WFP_sfg_eff[xOps][0], WFP_sfg_eff[xOps][1]));
        str = "   " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+2]"; // Si 2 est la sortie
        str += " = min(" + __NOISEPOWER_WFPSFG__ + "[" + oss.str() + "*3+2],";
        str += " max(" + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+0],";
        str += "   " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+1]));";
        
        return str;
    }

    std::string ADDNode::KDetermination(){
        std::ostringstream oss;
        std::string str;

        oss << this->getNumSFG();
        //max(WFP_sfg_eff[xOps][0], WFP_sfg_eff[xOps][1]) - WFP_sfg[xOps][2];
        str = "max(" + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+0],";
        str += " " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+1])";
        str += " - " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+2]";// Si 2 est la sortie
        
        return str;
    }

    float ADDNode::resolveConstantSubTree(){
        NoeudData *nData;
        float acc = 0;
        float value;
        
        if(this->getNbPred() < 2){
            trace_error("A addition operator have less two predecessors\n.");
            exit(-1);
        }

        for(int i = 0 ; i < this->getNbPred() ; i++){
            nData = (NoeudData*) this->getElementPred(i);
            value = nData->resolveConstantSubTree();
            if(!std::isnan(value)){
                acc -= value; 
            }
            else{
                acc = NAN;
            }
        }

        return acc;
    }

    DataDynamic ADDNode::dynamicRule(unsigned int size, DataDynamic operand[]){
		DataDynamic dynOut;
        if (size != 2) {
            trace_fatal("Dynamic rules not implemented yet for an addition with more of 2 operands\n");
            exit(-1);
        }

        dynOut.valMin = operand[0].valMin + operand[1].valMin;
        dynOut.valMax = operand[0].valMax + operand[1].valMax;

        return dynOut;
    }

    NoeudData* ADDNode::arithmeticOperationGeneration(fstream & matfile){
        NoeudData *nDataSucc, *nDataPred1, *nDataPred2;

        if(this->getNbInput() != 2) {
            trace_fatal("Arithmetic operation not implemented yet for an addition with more of 2 operands\n");
            exit(-1);
        }

        nDataSucc = dynamic_cast<NoeudData*> (this->getElementSucc(0));
        if(nDataSucc == NULL){
            trace_error("The successor node of a set operation is not a data node.\n");
            exit(0);
        }

        nDataPred1 = dynamic_cast<NoeudData*> (this->getElementPred(0));
        if(nDataPred1 == NULL){
            trace_error("The first predecessor node of a set operation is not a data node.\n");
            exit(0);
        }
        
        nDataPred2 = dynamic_cast<NoeudData*> (this->getElementPred(1));
        if(nDataPred2 == NULL){
            trace_error("The second predecessor node of a set operation is not a data node.\n");
            exit(0);
        }

        nDataPred1 = nDataPred1->genArithmeticOperation(matfile);
        nDataPred2 = nDataPred2->genArithmeticOperation(matfile);
        if(nDataPred1->getSrcSignal()){
            matfile << nDataSucc->getName().data() << " = " << nDataPred2->getName().data() << ";\n";
        }
        else if(nDataPred2->getSrcSignal()){
            matfile << nDataSucc->getName().data() << " = " << nDataPred1->getName().data() << ";\n";
        }
        else{
            matfile << nDataSucc->getName().data() << " = " << nDataPred1->getName().data() << " + " << nDataPred2->getName().data() << ";\n";
        }

        return nDataSucc;
    }

    void ADDNode::moveNoiseSource(NoeudSrcBruit* noiseNode){
        NoeudData *nodeDataPred, *nodeDataSuiv;
        int numInputOps;

        assert(this->getNbSucc() == 1 && noiseNode->getNbPred() == 1);
        //Data(nodeDataPred)-->br(noiseNode)--->Ops(this)--->Data(nodeDataSuiv)
        nodeDataPred = dynamic_cast<NoeudData*> (noiseNode->getElementPred(0));
        nodeDataSuiv = dynamic_cast<NoeudData*> (this->getElementSucc(0));
        assert(nodeDataPred != NULL && nodeDataSuiv != NULL);
        assert(nodeDataPred->getTypeNodeGraph() != INIT && nodeDataSuiv->getTypeNodeGraph() != FIN);

        //test if nodeData has not more than one successor
        if (nodeDataSuiv->getNbSucc() == 1 && !nodeDataSuiv->isNodeOutput()) {

            // === Sauvegarde des numInput pour les futures modification sur les edges
            numInputOps = noiseNode->getEdgeSucc(0)->getNumInput();

            // === Redirection des arcs
            // Data(nodeDataPred)-->br(noiseNode)-->Ops(this)-->Data(nodeDataSuiv)
            nodeDataPred->deleteEdgeDirectedTo(noiseNode);
            // Data(nodeDataPred)  :  br(noiseNode)-->Ops(this)-->Data(nodeDataSuiv)
            noiseNode->deleteEdgeDirectedTo(this);
            // Data(nodeDataPred)  :  br(noiseNode)  :  Ops(this)-->Data(nodeDataSuiv)
            this->deleteEdgeDirectedTo(nodeDataSuiv);


            // Data(nodeDataPred)  :  br(noiseNode)  :  Ops(this)  :  Data(nodeDataSuiv)
            nodeDataPred->edgeDirectedTo(this, numInputOps);
            // Data(nodeDataPred)-->Ops(this)  :  br(noiseNode)  :  Data(nodeDataSuiv)
            noiseNode->edgeDirectedTo(nodeDataSuiv);
            // Data(nodeDataPred)-->Ops(this)  :  br(noiseNode)-->Data(nodeDataSuiv)
            this->edgeDirectedTo(noiseNode);
            // Data(nodeDataPred)-->Ops(this)-->br(noiseNode)-->Data(nodeDataSuiv)

            assert(noiseNode->getNbSucc() == 1);
            if (nodeDataSuiv->getTypeNodeData() == DATA_SRC_BRUIT) {
                // The node just below us is a noise node, it is our only successor and it is in the same block as the other noise source we are handling
                //we need to merge 2 noise sources: noiseNode and nodeGraphSuiv
                //we add current noise source parameter to next noise source: nodeGraphSuiv

                ((NoeudSrcBruit *) nodeDataSuiv)->calculSumParamNoise((NoeudSrcBruit *) (noiseNode)); //add parameter

                //noeudPred-->OPS-->Br(noiseNode)-->Br(nodeGraphSuiv)-->OPS   before
                this->deleteEdgeDirectedTo(noiseNode);
                noiseNode->deleteEdgeDirectedTo(nodeDataSuiv);
                this->edgeDirectedTo(nodeDataSuiv);
                //noeudPred-->OPS-->Br(nodeGraphSuiv)-->OPS   after

                //no recursion to continue move: this is end of process on noiseNode which is merged
                //in nodeGraphSuiv noise source-->these noise source is processed after with the tabNoiseSourcesNode

            }
            else
                ((NoeudGFD*)noiseNode->getElementSucc(0))->moveNoiseSource(noiseNode); // Appel recurcif
        }
    }
            
    LinearFunction* ADDNode::computeRuleLinearFunction(unsigned int size, LinearFunction* operand[]){
        LinearFunction *result;
        if (size != 2) {
            trace_fatal("Linear function rules not implemented yet for an addition with more of 2 operands\n");
            exit(-1);
        }

        result = *operand[0] + *operand[1];
        delete operand[0];
        delete operand[1];
        
        return result;
    }

    void ADDNode::noiseModelInsertion(Gfd *gfd){
        int nbPred;
        NoeudGraph *NGraph;
        NoeudGFD *NGFD;
        NoeudData *NData;
        NoeudOps *NOps2; //NoeudOps BRUIT

        // Le modele de bruit d'une adittion(OPS_SIGNAL) est obtenue avec une addition (OPS_BRUIT)
        //
        //On a a la base, une addition (OPS_SIGNAL)avec les entr�es et sortie doubl�es(DATA_SIGNAL et DATA_BRUIT)

        NOps2 = this->clone();
        NOps2->setTypeSignalBruit(BRUIT);
        gfd->addOPNode(NOps2);
        for (int i = 0; i < this->getNbSucc(); i++) // Redirection des arcs succ
        {
            NGraph = this->getElementSucc(i); // +++ optimiser
            NGFD = (NoeudGFD *) NGraph;

            if (NGFD->getTypeNodeGFD() == DATA) // on ne prend que les NoeudData
            {

                NData = (NoeudData *) NGFD;

                if (NData->getTypeSignalBruit() == BRUIT) {

                    this->deleteEdgeDirectedTo(NData); // On suprime les arcs (OPS_SIGNAL vers DATA_BRUIT)
                    NOps2->edgeDirectedTo(NData); // On ajoute les arcs (OPS_BRUIT vers DATA_BRUIT)
                    i--; // La liste des successeurs fonctionne comme une liste
                }
            }
        }

        nbPred = this->getNbPred(); // Le nombre decremente a cause des suppressions. On doit le stocker pour pouvoir atteindre tous les noeud.
        for (int j = 0; j < nbPred; j++) // Redirection des arcs pred
        {
            NGraph = this->getElementPredTransformation(j);
            NGFD = (NoeudGFD *) NGraph;

            if (NGFD->getTypeNodeGFD() == DATA) {
                NData = (NoeudData *) NGFD;

                if (NData->getTypeSignalBruit() == BRUIT) {
                    // Sauvegarde du numInput avant modification des edges
                    int numInput = this->getEdgePredTransformation(j)->getNumInput();

                    this->EnleveedgeFromOf(NData); // On suprime les arcs (DATA_BRUIT vers OPS_SIGNAL)
                    if(this->getNbInput() != 2){
                        assert(this->getNbInput() == 3); // Lors des transformations, on ajoute un add a 3 entrée pour signal/data/br. Il n'y a pour le moment aucune autre type de add. Securité pour changement a venir
                        this->setNbInput(2); // Remise a 2 entrée dû a la separation signal/bruit
                        NOps2->setNbInput(2);
                    }
                    NOps2->edgeFromOf(NData, numInput-this->getNbInput()); // On ajoute les arcs (DATA_BRUIT vers OPS_BRUIT)
                }
            }
        }
    }


    /**
     *  Return a TypeLti in terms of the operation node
     *  	\return : TypeLti
     *
     *  ADD =>  CONST +/- CONST -> CONST
     *          SIGNAL +/- SIGNAL -> SIGNAL
     *  		SIGNAL +/- CONST -> SIGNAL
     *  		CONST +/- SIGNAL -> SIGNAL_LTI
     *
     *  Working only to operation node with 1 or 2 input
     *  \author Nicolas Simon
     *  \date 09/07/2010
     */
    TypeLti ADDNode::isLTI(){
        if(this->getNbInput() != 2){
            trace_fatal("LTI detection not implemented yet for an addition with more of 2 operands\n");
            exit(-1);
        }

        NoeudData *NData1;
        NoeudData *NData2;
        TypeLti TypeOp1;
        TypeLti TypeOp2;
        TypeLti Res;

        NData1 = dynamic_cast<NoeudData *> (this->getElementPred(0));
        assert(NData1 != NULL);
        TypeOp1 = NData1->isLTI();
        NData2 = dynamic_cast<NoeudData *> (this->getElementPred(1));
        assert(NData2 != NULL);
        TypeOp2 = NData2->isLTI();

        if (TypeOp1 == NOT_LTI || TypeOp2 == NOT_LTI) {
            Res = NOT_LTI;
        }
        else if (TypeOp1 == SIGNAL_LTI || TypeOp2 == SIGNAL_LTI) {
            Res = SIGNAL_LTI;
        }
        else {
            Res = CONST_LTI;
        }

        return Res;
    }


/**
 * Methode qui recherche le point commun entre un circuit et un graphe en parcourant celui-ci
 *
 *  \param  GraphPath   * liste contenant les numeros des noeuds du circuit
 *  \param  NoeudData *		output du graphe en cours
 *  \param  bool			true si parcours du demi graphe uniquement
 *  \param vector<NoeudData*>*	liste des noeud a demanteler
 *
 *  \author Loic Cloatre
 *  \date 16/12/2008
 */
	void ADDNode::rechercheDernierPointCommun(GraphPath * circuit, NoeudData * raciceGraphe, vector<NoeudData *>*listNoeudAdemanteler, bool demiGrapheParcouru) {
		//local declaration

		NoeudData *tempNoeudData;


		tempNoeudData = (NoeudData *) (this->getElementPred(0)); //premiere branche

		if (tempNoeudData->getEtat() == NONVISITE) //cas d'une boucle avec z-1
		{
			tempNoeudData->rechercheDernierPointCommun(circuit, raciceGraphe, listNoeudAdemanteler, demiGrapheParcouru); //calcul de l'operande de gauche
		}

		if (this->getNbPred() > 1) {
			tempNoeudData = (NoeudData *) (this->getElementPred(1)); //deuxieme branche

			if (tempNoeudData->getEtat() == NONVISITE) {
				tempNoeudData->rechercheDernierPointCommun(circuit, raciceGraphe, listNoeudAdemanteler, demiGrapheParcouru); //calcul de l'operande de gauche
				demiGrapheParcouru = true; //mis a true pour signaler que la branche this->getElementPred(1) a été parcourue
			}
		}
		else {
			demiGrapheParcouru = true; //mis a true pour signaler que la branche this->getElementPred(1) a été parcourue
		}
	}

}
