/**
 *  \file NoeudSrcBruit.cc
 *  \author  Loic Cloatre, Quentin Meunier
 *  \date   11.02.2011
 *  \brief Classe definissant les noeuds representant une source bruit
 */

// === Bibliothèques .hh associées
#include <sstream>              // Pour convertir un int en string (atoi pas ANSI^^)
#include <cassert>

#include "graph/dfg/datanode/NoeudSrcBruit.hh"
#include "graph/toolkit/TypeVar.hh"
#include "utils/Tool.hh"

// === Bibliothèques non standard

// === Namespaces
using namespace std;

// ======================================= Constructeurs - Destructeurs
/**
 * Constructeur de la classe NoeudData
 *   \param NumNoeud :
 *   \param Nom :
 *   \param pasQuantif :
 *   \param KbitsElimine :
 *   \param TSB :
 *   \param blockI :
 */
NoeudSrcBruit::NoeudSrcBruit(int NumNoeud, string Nom, TypeSignalBruit TSB, Block &block, bool noiseOfSource) :
	NoeudData(DATA_SRC_BRUIT, NumNoeud, Nom.c_str(), new TypeVar(VAR_ENTREE), TSB, Nom.data()), m_block(block) {
	this->setNumSrc(NumNoeud);
	this->noiseOfSource = noiseOfSource;
}


NoeudSrcBruit::NoeudSrcBruit(const NoeudSrcBruit& other):NoeudData(other), m_block(other.m_block){
	this->noiseOfSource = other.noiseOfSource;

	for(int i = 0 ; i < (int) other.m_PasQuantif.size() ; i++){
		this->m_PasQuantif.push_back(other.m_PasQuantif[i]);
		this->m_ModeQuantif.push_back(other.m_ModeQuantif[i]);
		this->m_Constante.push_back(other.m_Constante[i]);
		this->m_KbitsElimine.push_back(other.m_KbitsElimine[i]);
		this->m_SigneMoyenne.push_back(other.m_SigneMoyenne[i]);
	}
}
	
NoeudSrcBruit* NoeudSrcBruit::clone(){
    return new NoeudSrcBruit(*this);
}


/**
 * \brief Methode qui renvoie le pas de quantification
 *
 * \param numero numero de la case
 * \return string  expression
 */
string NoeudSrcBruit::getNumModeQuantif(int numero) {
	if (numero >= (int) m_ModeQuantif.size()) {
		trace_error("indice plus grand que le conteneur");
		exit(0);
	}
	return m_ModeQuantif[numero];
}

/**
 * \brief Methode qui renvoie le pas de quantification
 *
 * \param numero numero de la case
 * \return string  expression
 */
string NoeudSrcBruit::getNumStepQuantif(int numero) {
	if (numero >= (int) m_PasQuantif.size()) {
		trace_error("indice plus grand que le conteneur");
		exit(0);
	}
	return m_PasQuantif[numero];
}

/**
 * \brief Methode qui renvoi le nombre de bits elimines
 *
 * \param numero : numero de la case
 * \return string : nombre de bits Éliminé
 */
string NoeudSrcBruit::getNumSuppressKBits(int numero) {
	if (numero >= (int) m_KbitsElimine.size()) {
		trace_error("indice plus grand que le conteneur");
		exit(0);
	}
	return m_KbitsElimine[numero];
}

/**
 * \brief Methode qui renvoi le signe de la moyenne
 *
 * \param numero : numero à commparé
 * \return int : +1 ou -1 
 */
int NoeudSrcBruit::getSigneMoyenne(int numero) {
	if (numero >= (int) m_SigneMoyenne.size()) {
		trace_error("indice plus grand que le conteneur");
		exit(0);
	}
	return m_SigneMoyenne[numero];
}

/**
 *	Rend la constante correspondant à l'indice i
 *
 *	\param i : indice de la constante a rendre
 *	\return constante associé à l'indice i
 *
 */
float NoeudSrcBruit::getConstante(int i){
	if(i >= (int) m_Constante.size()){
		trace_error("indice plus grand que le conteneur");
		exit(0);
	}
	return m_Constante[i];
}


/**
 *	Multiplie tout les valeurs du conteneur des constates par la valeur fourni 
 *
 *	\param cste : valeur par laquelle multiplier les valeurs du conteneur
 *
 */
void NoeudSrcBruit::multConstante(float cste){
	for(int i = 0 ; i < (int) m_Constante.size() ; i++){
		m_Constante[i] *= cste;
	}
}

/**
 * Divise toutes les valeurs du conteneur des constantes par la valeur founi
 *
 * \param cste : valeur par laquelle diviser les valeurs du conteneur
 *
 */
void NoeudSrcBruit::divConstante(float cste){
	for (int i = 0; i < (int) m_Constante.size() ; i++) {
		m_Constante[i] /= cste;
	}
}

/**
 * \brief Inverseur de signe
 *
 * Methode pour inverser tous les signes sur le vecteur de SigneMoyenne, lors de passage dans un operateur -
 */
void NoeudSrcBruit::inverseVectSigneMean() {
	for (int i = 0; i < (int) m_SigneMoyenne.size(); i++) {
		m_SigneMoyenne[i] = -m_SigneMoyenne[i];
	}
}

/**
 * \brief Somme de paramètre de bruit
 *
 * Methode pour effectuer la somme des parametres de bruit entre deux noeuds
 * utilise pour le regroupement de deux sources de bruit
 * operande de gauche modifiee
 *
 * \param noeudCible : Noeud cible
 */
void NoeudSrcBruit::calculSumParamNoise(NoeudSrcBruit * noeudCible) {
	for (int i = 0; i < (int) noeudCible->getNbSuppressKBits(); i++) {
		this->m_KbitsElimine.push_back(noeudCible->getNumSuppressKBits(i));
		this->m_PasQuantif.push_back(noeudCible->getNumStepQuantif(i));
		this->m_SigneMoyenne.push_back(noeudCible->getSigneMoyenne(i));
		this->m_ModeQuantif.push_back(noeudCible->getNumModeQuantif(i));
		this->m_Constante.push_back(noeudCible->getConstante(i));
	}
	// Lors du merge d'un NoeudSrcBruit à un autre, si un des noeud est associée à une source, nous devons conserver cette information
	this->noiseOfSource = this->noiseOfSource || noeudCible->getNoiseOfSource();

}

/**
 * \brief Soustraction de paramètre de bruit
 *
 * Methode pour effectuer la soustraction des parametres de bruit entre deux noeuds
 * utilise pour le regroupement de deux sources de bruit
 * operande de gauche modifiee
 *
 *  \param noeudCible : Noeud cible a soustraire
 */
void NoeudSrcBruit::calculSubtractionParamNoise(NoeudSrcBruit * noeudCible) {
	for (int i = 0; i < (int) noeudCible->gettypeVarRefVectKBits().size(); i++) {
		this->m_KbitsElimine.push_back(noeudCible->getNumSuppressKBits(i));
		this->m_PasQuantif.push_back(noeudCible->getNumStepQuantif(i));
		this->m_SigneMoyenne.push_back(-noeudCible->getSigneMoyenne(i)); // Inversé
		this->m_ModeQuantif.push_back(noeudCible->getNumModeQuantif(i));
		this->m_Constante.push_back(noeudCible->getConstante(i));
	}
	// Lors du merge d'un NoeudSrcBruit à un autre, si un des noeud est associée à une source, nous devons conserver cette information
	this->noiseOfSource = this->noiseOfSource || noeudCible->getNoiseOfSource();
}

/**
 * \brief Affichage des params du noeud
 *
 * Methode qui affiche les params du noeud (si c'est une source de bruit)
 */
void NoeudSrcBruit::displayParamNoise() {
	cout << "noeudSrcBruit:(" << this->getNumSrc() << "," << getName() << ")" << endl;
	if (this->isNodeSourceBruitBR()) {
		//pour eviter d'afficher le noeud SignalSrc
		for (int i = 0; i < (int) this->m_KbitsElimine.size(); i++) {
			//on fait un push des couples (K,Q)
			cout << "\t K " << i << "=" << m_KbitsElimine[i].data() << endl << "\t Q " << i << "=" << m_PasQuantif[i].data() << endl << "\t S " << i << "=" << m_SigneMoyenne[i] << endl << "\t C " << i << "=" <<m_Constante[i] <<endl;
		}
	}
}
