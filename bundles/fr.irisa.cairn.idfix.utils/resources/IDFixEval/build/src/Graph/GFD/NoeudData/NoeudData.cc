/**
 *  \file NoeudData.cc
 *  \author Daniel Menard, Jean-Charles Naud
 *  \date   31.01.2002
 *  \brief Classe definissant les noeuds representant une donnee dans un GFD
 */

#include <sstream>              //pour convertir un int en string (atoi pas ANSI^^)
#include <cmath>

#include "graph/dfg/datanode/NoeudData.hh"
#include "graph/dfg/operatornode/NoeudOps.hh"


// === Namespaces
using namespace std;

// ======================================= Constructeurs - Destructeurs
/**
 * Constructeur de la classe NoeudData
 *
 * \param typeNodeData : type de la variable
 * \param NumNoeud : numéro du noeud
 * \param nom : nom
 * \param TypeVariable :  type de la variable
 * \param &blockId :
 * \param originalName :
 */
NoeudData::NoeudData(TypeNodeData typeNodeData, int NumNoeud, string nom, TypeVar * TypeVariable, string originalName) :
	NoeudGFD(DATA, NumNoeud, NONVISITE, nom) {

	this->typeNodeData = typeNodeData; // Type de la variable (DATA_BASE, DATA_ARRAY, DATA_SRC_BRUIT)

	// Informations generales
	this->setOriginalName(originalName);
	typeVar = TypeVariable; // Defintion des differentes caracteristiques de la donnes
	noeudDemantele = false;
	numSrc = -1; // Par defaut un noeud data est a -1
	dataDynamic.valMax = 0;
	dataDynamic.valMin = 0;
	srcSignal = false;
	typeSimu = false;
	dynamicProcess = INTERVAL;
	probDeb = -1;
	numAffect = -1;
	indexArray = "";
	variableWidth = false;

	m_ptrFonctionLineaire = NULL;
	Valeur = 0;
}

NoeudData::NoeudData(TypeNodeData typeNodeData, int NumNoeud, string nom, TypeVar * TypeVariable, TypeSignalBruit typeSignalBruit, string originalName) :
	NoeudGFD(DATA, NumNoeud, NONVISITE, nom, typeSignalBruit) {

	this->typeNodeData = typeNodeData; // Type de la variable (DATA_BASE, DATA_ARRAY, DATA_SRC_BRUIT)

	// Informations generales
	this->setOriginalName(originalName);
	typeVar = TypeVariable; // Defintion des differentes caracteristiques de la donnes
	noeudDemantele = false;
	numSrc = -1; // Par defaut un noeud data est a -1
	dataDynamic.valMax = 0;
	dataDynamic.valMin = 0;
	srcSignal = false;
	typeSimu = false;
	dynamicProcess = INTERVAL;
	probDeb = -1;
	numAffect = -1;
	indexArray = "";
	variableWidth = false;

	m_ptrFonctionLineaire = NULL;
	Valeur = 0;
}

/**
 *  Constructeur de la classe NoeudData
 *
 *  \param typeNodeData : noeud de reférence
 */
NoeudData::NoeudData(const NoeudData & other) :
	NoeudGFD(other) {
	numSrc = other.numSrc;
	typeNodeData = other.typeNodeData;
	typeVar = new TypeVar(*other.typeVar);
	Valeur = other.Valeur;
	noeudDemantele = other.noeudDemantele;
	typeSimu = other.typeSimu;
	srcSignal = other.srcSignal;
	variableWidth = other.variableWidth;
	dynamicProcess = other.dynamicProcess;
	probDeb = other.probDeb;
	dataDynamic = other.dataDynamic;
	numAffect = other.numAffect;
	indexArray = other.indexArray;

	if(other.m_ptrFonctionLineaire == NULL)
		this->m_ptrFonctionLineaire = NULL;
	else	
	   m_ptrFonctionLineaire = new LinearFunction(*other.m_ptrFonctionLineaire);
}

/**
 * Destructeur de la classe NoeudData
 */
NoeudData::~NoeudData() {
	delete typeVar;
    delete m_ptrFonctionLineaire;
}


NoeudData* NoeudData::clone(){
    return new NoeudData(*this);
}

// =======================================  Methodes
/**
 *  \brief Test entrée
 *
 *  Indique si le NoeudData est une entree (VAR_ENTREE ?)
 *
 *	\return bool : Resultat du test
 */

bool NoeudData::isNodeInput() {
	if ((this)->getTypeVar()->getTypeVarFonct() == VAR_ENTREE) {
		return true;
	}
	else {
		return false;
	}
}

/**
 *  \brief Test sortie
 *
 *  Indique si le NoeudData est une sortie (VAR_SORTIE ou CONST_SORTIE ?)
 *
 *	\return bool : Resultat du test
 */
bool NoeudData::isNodeOutput() {
	if ((getTypeVar()->getTypeVarFonct() == VAR_SORTIE) || (getTypeVar()->getTypeVarFonct() == CONST_SORTIE)) {
		return true;
	}
	else {
		return false;
	}
}

/**
 * \brief Test variable de sortie
 *
 * Indique si le NoeudData est une sortie systeme(VAR_SORTIE et ..... yn ?)
 *
 * \return bool : Resultat du test
 */
bool NoeudData::isNodeSystemOutput() {

	string *NomSortie;

	if ((this)->getTypeVar()->getTypeVarFonct() == VAR_SORTIE) {
		NomSortie = new string("yn");

		if ((this)->getName().compare(*NomSortie) == 0) {
			return true;
		}
		else {
			return false;
		}
	}
	return false;
}

/**
 * \brief Test variable entière
 *
 * Indique si le NoeudData est une variable intermediaire (VAR_INT ?)
 *
 *\return bool : Resultat du test
 */
bool NoeudData::isNodeVarInt() {
	if ((this)->getTypeVar()->getTypeVarFonct() == VAR_INT) {
		return true;
	}
	else {
		return false;
	}
}

/**
 *  \brief Test variable
 *
 *  Indique si le NoeudData est une variable intermediaire (VAR ?)
 *
 *	\return bool : Resultat du test
 */
bool NoeudData::isNodeVar() {
	if ((this)->getTypeVar()->getTypeVarFonct() == VAR) {
		return true;
	}
	else {
		return false;
	}
}

/**
 * \brief Test constante
 *
 * Indique si le NoeudData est une constante (CONST ou CONST_SORTIE ?)
 *
 * \return bool : Resultat du test
 */
bool NoeudData::isNodeConst() {
	if (((this)->getTypeVar()->getTypeVarFonct() == CONST) || ((this)->getTypeVar()->getTypeVarFonct() == CONST_SORTIE)) {
		return true;
	}
	else {
		return false;
	}
}

/**
 *  \brief Test source de bruit BR
 *
 *  Renvoie true si le noeud est une source 'Br'
 *  on peut avoir par construction le noeud srcSignal DATA_SRC_BRUIT et SIGNAL: ret = false
 *
 *	\return bool : Resultat du test
 */
bool NoeudData::isNodeSourceBruitBR() {
	if (numSrc != -1) {
		if ((this->getTypeNodeData() == DATA_SRC_BRUIT) && (this->getTypeSignalBruit() == BRUIT)) {
			return true;
		}
	}
	return false;
}


float NoeudData::resolveConstantSubTree(){
    float value;
    if(this->isNodeConst()){
        return this->Valeur;
    }
    else if(this->isNodeInput()){
        return NAN;
    }
    else{
        value = ((NoeudOps*) this->getElementPred(0))->resolveConstantSubTree();
        if(!std::isnan(value)){
            this->Valeur = value;
            this->getTypeVar()->setTypeVarFonct(CONST);
        }
        return value;
    }
}
