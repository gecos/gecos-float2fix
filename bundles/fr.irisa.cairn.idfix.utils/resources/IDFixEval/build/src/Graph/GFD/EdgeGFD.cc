/**
 *  \file EdgeGFD.cc
 *  \author Daniel Menard, Nicolas Simon
 *  \date   25.06.2010
 *  \brief Class which mean edges of GFD
 */

// === Bibliothèques .hh associées
#include "graph/dfg/EdgeGFD.hh"

// ======================================= Constructeurs - Destructeurs
/**
 * Constructeur de la classe NoeudData
 *
 * \param nodePred : Numéro Pred
 * \param nodeSucc : Numéro Succ
 * \param format : format
 */
EdgeGFD::EdgeGFD(NoeudGraph * nodePred, NoeudGraph * nodeSucc, int numInput) :
	Edge(nodePred, nodeSucc, numInput) {
}

EdgeGFD::~EdgeGFD() {
}
