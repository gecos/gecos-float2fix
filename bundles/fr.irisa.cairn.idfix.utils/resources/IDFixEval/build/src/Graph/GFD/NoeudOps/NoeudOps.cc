/**
 *  \file NoeudOps.cc
 *  \author Daniel Menard, Jean-Charles Naud
 *  \date   31.01.2002
 *  \brief Classe definissant les noeuds representant une donnee dans un GFD
 */

#include <graph/dfg/operatornode/NoeudOps.hh>

#include <graph/toolkit/TypeVar.hh>

NoeudOps::NoeudOps(int NumNoeud, int m_inputNumber, Block &block, string name) :
	NoeudGFD(OPS, NumNoeud, NONVISITE, name), m_block(block) {
	this->m_inputNumber = m_inputNumber;
	if((int) this->listPred.size()<m_inputNumber)
		this->listPred.resize(m_inputNumber);

	m_dynamic.valMin = 0;
	m_dynamic.valMax = 0;
	m_SFGNumber = -1; //only set during parse of input xml file
}

NoeudOps::NoeudOps(int NumNoeud, TypeSignalBruit typeSignalBruit, int m_inputNumber, Block &block, string name) :
	NoeudGFD(OPS, NumNoeud, NONVISITE, name, typeSignalBruit), m_block(block) {
	this->m_inputNumber = m_inputNumber;
	if((int) this->listPred.size()<m_inputNumber)
		this->listPred.resize(m_inputNumber);

	m_dynamic.valMin = 0;
	m_dynamic.valMax = 0;

	m_SFGNumber = -1; //only set during parse of input xml file
}

NoeudOps::NoeudOps(const NoeudOps& other):NoeudGFD(other), m_block(other.m_block){
	this->m_inputNumber = other.m_inputNumber;
	this->m_dynamic = other.m_dynamic;
	this->m_SFGNumber = other.m_SFGNumber;
}

NoeudOps::~NoeudOps() {

}
