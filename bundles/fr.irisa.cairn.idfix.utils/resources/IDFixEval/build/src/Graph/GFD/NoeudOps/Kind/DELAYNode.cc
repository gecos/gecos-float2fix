#include "graph/dfg/operatornode/kind/DELAYNode.hh"
#include "backend/NoisePowerExpressionGeneration.hh"

namespace gfd {
    DELAYNode::DELAYNode(Block &block):NoeudOps(-1, 1, block, "DELAY"){}
    DELAYNode::DELAYNode(const DELAYNode &other):NoeudOps(other){}

    DELAYNode* DELAYNode::clone(){
        return new DELAYNode(*this);
    }
    
    std::string DELAYNode::WFPEffDetermination(){
        std::ostringstream oss;
        std::string str;

        oss << this->getNumSFG();
        // WFPeff[xOps][2] = min(WFP[xOps][2], WFPeff[xOps][0]);
        str = "   " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+2]"; // Si 2 est la sortie
        str += " = min(" + __NOISEPOWER_WFPSFG__ + "[" + oss.str() + "*3+2],";
        str += " " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+0]);";

        return str;

    }

    std::string DELAYNode::KDetermination(){
        std::ostringstream oss;
        std::string str;
    
        oss << this->getNumSFG();
        // WFP_sfg_eff[xOps][0] - WFP_sfg[xOps][2];
        str = __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+0]"; // Si 2 est la sortie
        str += " - " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+2]";
        return str;
    }

    float DELAYNode::resolveConstantSubTree(){
        NoeudData *nData;
        
        if(this->getNbPred() != 1){
            trace_error("A delay operator have not only one predecessor.\n");
            exit(-1);
        }
        else{
            nData = (NoeudData*) this->getElementPred(0);
            return nData->resolveConstantSubTree();
        }
    }

    DataDynamic DELAYNode::dynamicRule(unsigned int size, DataDynamic operand[]){
		DataDynamic dynOut;
        
        if(size != 1){
            trace_error("A delay operator cannot have more one operand\n");
            exit(-1);
        }

        dynOut.valMin = operand[0].valMin;
        dynOut.valMax = operand[0].valMax;

		return dynOut;
    }
    
    NoeudData* DELAYNode::arithmeticOperationGeneration(fstream & matfile){
        trace_error("Delay node cannot generate non-LTI arithmetic operation. Error during graph transformation\n");
        exit(EXIT_FAILURE);
    }
            
    LinearFunction* DELAYNode::computeRuleLinearFunction(unsigned int size, LinearFunction* operand[]){
        LinearFunction *result;
        if (size != 1) {
            trace_fatal("Linear function rules not implemented yet for an delay with more of 1 operands\n");
            exit(-1);
        }
        
        operand[0]->operatorDelay();
        result = operand[0];

        return result;
    }

    void DELAYNode::noiseModelInsertion(Gfd *gfd){
        int i;
        int j;
        int numInput;
        NoeudGraph *NG = NULL;
        NoeudGFD *NGFD = NULL;
        NoeudData *ND = NULL;
        NoeudOps *NO1 = NULL;
        TypeSignalBruit typeSignalBruit;
        TypeNodeGFD TypeNGFD;

        // Le modele de bruit d'un retard(OPS_SIGNAL) est obtenue avec un retard (OPS_BRUIT)
        //
        //On a � la base, un retard (OPS_SIGNAL) avec les entr�es et sortie doubl�es(DATA_SIGNAL et DATA_BRUIT)
        NO1 = this->clone();
        NO1->setTypeSignalBruit(BRUIT);
        gfd->addOPNode(NO1);


        for (i = 0; i < this->getNbSucc(); i++) // Redirection des arcs succ
        {
            NG = this->getElementSucc(i);
            NGFD = (NoeudGFD *) NG;
            TypeNGFD = NGFD->getTypeNodeGFD();
            if (TypeNGFD == DATA) {
                ND = (NoeudData *) NGFD;
                typeSignalBruit = ND->getTypeSignalBruit();
                if (typeSignalBruit == BRUIT) {
                    this->deleteEdgeDirectedTo(ND); // On suprime les arcs (OPS_SIGNAL vers DATA_BRUIT)
                    NO1->edgeDirectedTo(ND); // On ajoute les arcs (OPS_BRUIT vers DATA_BRUIT)
                    i--;
                }
            }
        }
        for (j = 0; j < this->getNbPred(); j++) // Redirection des arcs pred
        {
            NG = this->getElementPred(j);
            NGFD = (NoeudGFD *) NG;
            TypeNGFD = NGFD->getTypeNodeGFD();
            if (TypeNGFD == DATA) {
                ND = (NoeudData *) NGFD;
                typeSignalBruit = ND->getTypeSignalBruit();
                if (typeSignalBruit == BRUIT) {
                    // Sauvegarde du numInput avant modification des edges
                    numInput = -1;
                    vector<Edge *>::iterator itPredOps = this->getListePred()->begin();
                    while (itPredOps != this->getListePred()->end()) {
                        if ((*itPredOps)->getNodePred() == ND) {
                            numInput = (*itPredOps)->getNumInput();
                            break;
                        }
                        itPredOps++;
                    }
                    assert(numInput != -1); // numInput doit être forcement affecté au dessus
                    this->EnleveedgeFromOf(ND); // On suprime les arcs (DATA_BRUIT vers OPS_SIGNAL)
                    NO1->edgeFromOf(ND, numInput-this->getNbInput()); // On ajoute les arcs (DATA_BRUIT vers OPS_BRUIT)
                }
            }
        }

    }

    /**
     *  Return a TypeLti in terms of the operation node
     *  	\return : TypeLti
     *  \author Nicolas Simon
     *  \date 09/07/2010
     */
    TypeLti DELAYNode::isLTI(){
        NoeudData *NData1;

        NData1 = dynamic_cast<NoeudData *> (this->getElementPred(0));
        assert(NData1 != NULL);
        return NData1->isLTI();
    }

/**
 * Methode qui recherche le point commun entre un circuit et un graphe en parcourant celui-ci
 *
 *  \param  GraphPath   * liste contenant les numeros des noeuds du circuit
 *  \param  NoeudData *		output du graphe en cours
 *  \param  bool			true si parcours du demi graphe uniquement
 *  \param vector<NoeudData*>*	liste des noeud a demanteler
 *
 *  \author Loic Cloatre
 *  \date 16/12/2008
 */
	void DELAYNode::rechercheDernierPointCommun(GraphPath * circuit, NoeudData * raciceGraphe, vector<NoeudData *>*listNoeudAdemanteler, bool demiGrapheParcouru) {
		//local declaration

		NoeudData *tempNoeudData;
		tempNoeudData = (NoeudData *) (this->getElementPred(0));
		tempNoeudData->rechercheDernierPointCommun(circuit, raciceGraphe, listNoeudAdemanteler, demiGrapheParcouru); //calcul de l'operande
		demiGrapheParcouru = true; //mis a true pour signaler que la branche this->getElementPred(1) a été parcourue

	
	}
}
