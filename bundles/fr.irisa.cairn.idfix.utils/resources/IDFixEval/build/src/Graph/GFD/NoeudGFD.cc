/**
 *  \author Daniel Menard, Jean-Charles Naud
 *  \date   06.02.2002
 *  \class NoeudGFD
 *  \brief Classe generique definissant les noeuds  present dans un GFD
 */

// === Bibliothèques .hh associe
#include "graph/dfg/datanode/NoeudData.hh"
#include "graph/dfg/NoeudGFD.hh"
#include "graph/dfg/NoeudInitFin.hh"
#include "graph/dfg/EdgeGFD.hh"
#include "graph/dfg/operatornode/NoeudOps.hh"

// === Namespaces
using namespace std;


// ======================================= Constructeurs - Destructeurs

/**
 * Constructeur de la clase NoeudGFD
 *   \param typeNodeGFD : Type de Noeud du GFD
 *   \param Numero : Numero du noeud
 *
 */

NoeudGFD::NoeudGFD(TypeNodeGFD typeNodeGFD, int NumNoeud, TypeEtat Etat, string name) :
	NoeudGraph(GFD, NumNoeud, name) {
	this->typeNodeGFD = typeNodeGFD; // Type de Noeud du GFD ( OPS, DATA, INIT, FIN)
	Etat = NONVISITE;
	this->Etat = Etat; // Etat du noeud ( VISITE, ENTRAITEMENT, NONVISITE )

	cloned = false; /// Boolean indiquant si le noeud est deja clone */
	ptrclone = NULL; /// Pointeur sur le clone du noeud */
	numCDFG = -1; //numCDFG is only set during parsing of XML file
	typeSignalBruit = SIGNAL; // Par défaut
}

NoeudGFD::NoeudGFD(TypeNodeGFD typeNodeGFD, int NumNoeud, TypeEtat Etat, string name, TypeSignalBruit typeSignalBruit) :
	NoeudGraph(GFD, NumNoeud, name) {
	this->typeNodeGFD = typeNodeGFD; // Type de Noeud du GFD ( OPS, DATA, INIT, FIN)
	Etat = NONVISITE;
	this->Etat = Etat; // Etat du noeud ( VISITE, ENTRAITEMENT, NONVISITE )

	cloned = false; /// Boolean indiquant si le noeud est deja clone */
	ptrclone = NULL; /// Pointeur sur le clone du noeud */
	numCDFG = -1; //numCDFG is only set during parsing of XML file
	this->typeSignalBruit = typeSignalBruit; // Par défaut
}


NoeudGFD::NoeudGFD(const NoeudGFD& other):NoeudGraph(other){
	this->typeNodeGFD = other.typeNodeGFD;
	this->cloned = other.cloned;
	this->ptrclone = other.ptrclone;
	this->numCDFG = other.numCDFG;
	this->typeSignalBruit = other.typeSignalBruit;
}

/**
 * Destructeur de la clase NoeudGFD
 */

NoeudGFD::~NoeudGFD() {
}

// ======================================= Methodes de test pour savoir si le noued INIT ou FIN
// =================================================================
// =================================================================


/**
 *  Fonction determinant si un noeud est une Source du graphe GFD
 * 	\return booleen TRUE si le noeud est un Source, FALSE sinon
 *
 *	Un noeud est un Terminal si
 *		- il n'a pas de listPred
 *		- il a un seul predecesseur : le noeud INIT
 */

bool NoeudGFD::isNodeSource() {

	bool Test = false;
	int NblistPred;
	NoeudGraph *NGFD;

	NblistPred = (this)->getNbPred();

	if (NblistPred == 0)
		Test = true; // Ne devrait pas se produire (noeud orphelin)
	else {
		if (NblistPred == 1) {
			NGFD = ((this)->getElementPred(0));

			Test = (NGFD->getTypeNodeGraph() == INIT); // Notre pred est le noeus INIT donc on est une source
		}
		else
			Test = false;
	}
	return Test;
}

/**
 *  Fonction determinant si un noeud est une Racine du graphe GFD
 * 	\return booleen TRUE si le noeud est une Source, FALSE sinon
 *
 *	Un noeud est un Terminal si
 *		- il n'a pas de listSucc
 *		- il a un seul predecesseur : le noeud FIN
 */
bool NoeudGFD::isNodeSink() {

	bool Test = false;
	int NblistSucc;
	NoeudGraph *NGFD;

	NblistSucc = (this)->getNbSucc();

	if (NblistSucc == 0)
		Test = true;
	else {
		if (NblistSucc == 1) {
			NGFD = (this)->getElementSucc(0);

			Test = (NGFD->getTypeNodeGraph() == FIN);
		}
		else {
			Test = false;
		}
	}
	return Test;
}

/**
 * 	Creation of Edge between this and OPS Tete node (Used by GFD)
 *		\param Queue : Node pointer
 *		\param numInput : Operand number of Tete
 *
 *	\author Nicolas Simon 
 *	\date 28/06/2010
 */
void NoeudGFD::edgeDirectedTo(NoeudGraph * Tete, int numInput) {
	if (Tete->getTypeNodeGraph() == GFD) {
		if (((NoeudGFD *) Tete)->getTypeNodeGFD() != OPS)
			this->addEdge(Tete, 0);
		else if (numInput >= 0)
			this->addEdge(Tete, numInput);
		else {
			trace_error("Le numero d'entrée de l'edge vers un operateur doit être précisé et >= à 0");
			exit(-1);
		}
	}
	else if (Tete->getTypeNodeGraph() == FIN) {
		((NoeudFin*)Tete)->addEdge(this);
	}
	else {
		trace_error("Un edge GFD ne peut être ajoutée que seulement entre des NoeudGFD");
		exit(-1);
	}
}

/*
 *	Creation of Edge between queue and this node (Used by GFD)
 *		\param Queue : Node pointer
 *		\param numInput : Operand number of this
 *
 * 	\author Nicolas Simon
 * 	\date 28/06/2010
 */
void NoeudGFD::edgeFromOf(NoeudGraph * Queue, int numInput) {
	if (this->getTypeNodeGraph() == GFD) {
		if (((NoeudGFD *) this)->getTypeNodeGFD() != OPS)
			Queue->addEdge(this, 0);
		else if (numInput >= 0)
			Queue->addEdge(this, numInput);
		else {
			trace_error("Le numero d'entrée de l'edge vers un operateur doit être précisé et >= à 0");
			exit(-1);
		}
	}
	else if (this->getTypeNodeGraph() == FIN) {
		Queue->addEdge(this);
	}
	else {
		trace_error("Un edge GFD ne peut être ajoutée que seulement entre des NoeudGFD");
		exit(-1);
	}
}
/**
 *   Add a EdgeGFD element between this and NGraph, the Format is save too
 *		\param NGraph : Head of the edge
 *		\param numInput : Operand number of NGraph 
 *
 *	 \author Nicolas Simon
 *	 \date 26/06/2010
 */

void NoeudGFD::addEdge(NoeudGraph * NGraph, int numInput) {
	EdgeGFD* Elt;

	Elt = new EdgeGFD(this, NGraph, numInput);
	this->listSucc.push_back(Elt); // Ajout du successeur NGraph
	this->nbSucc++;
	if(numInput< NGraph->getNbPred() && NGraph->getEdgePred(numInput) != NULL){; // Empeche l'ecrasement d'un edge pred sans avoir fait les traitement necessaire de suppression.
		trace_error("Un arc est déjà présent à l'endroit où l'ajout doit ce faire");
		exit(0);
	}
	NGraph->setListePred(Elt, numInput); // Ajout du predecesseur this à la liste de NGraph
	NGraph->setNbPred(NGraph->getNbPred() + 1);
}

