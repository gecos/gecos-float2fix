/**
 *  \file NoeudSrc.cc
 *  \author Nicolas Simon
 *  \date  Jul 6, 2010
 *  \brief Classe definissant les noeuds representant une donnee dans un GFD
 */

// === Bibliothèques .hh associées
#include "graph/dfg/datanode/NoeudSrc.hh"
#include "graph/toolkit/TypeVar.hh"

// ======================================= Constructeurs - Destructeurs
/**
 * Constructeur de la classe NoeudData
 *  \param typeNodeData :
 *  \param NumNoeud :
 *  \param Nom :
 *  \param TypeVariable :
 *  \param &blockId :
 *  \param originalName :
 */
NoeudSrc::NoeudSrc(TypeNodeData typeNodeData, int NumNoeud, string Nom, TypeVar * TypeVariable, string originalName) :
	NoeudData(typeNodeData, NumNoeud, Nom, TypeVariable, originalName) {
}

NoeudSrc::NoeudSrc(const NoeudData &other):NoeudData(other){
    this->typeNodeData = DATA_SRC;
}


NoeudSrc::NoeudSrc(const NoeudSrc &other):NoeudData(other){
    
}

/**
 * Destructeur de la classe NoeudSrc
 */
NoeudSrc::~NoeudSrc() {
}

NoeudSrc* NoeudSrc::clone(){
    return new NoeudSrc(*this);
}
