#include <cmath>

#include "graph/dfg/operatornode/kind/SUBNode.hh"
#include "backend/NoisePowerExpressionGeneration.hh"

namespace gfd {
    SUBNode::SUBNode(Block &block, unsigned int input_number):NoeudOps(-1, input_number, block, "SUB"){}
    SUBNode::SUBNode(const SUBNode &other):NoeudOps(other){}

    SUBNode* SUBNode::clone(){
        return new SUBNode(*this);
    }
    
    std::string SUBNode::WFPEffDetermination(){
        std::ostringstream oss; 
        std::string str;

        oss << this->getNumSFG();
        // WFP_sfg_eff[xOps][2] = min(WFP_sfg[xOps][2], max(WFP_sfg_eff[xOps][0], WFP_sfg_eff[xOps][1]));
        str = "   " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+2]"; // Si 2 est la sortie
        str += " = min(" + __NOISEPOWER_WFPSFG__ + "[" + oss.str() + "*3+2],";
        str += " max(" + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+0],";
        str += "   " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+1]));";

        return str;
    }

    std::string SUBNode::KDetermination(){
        std::ostringstream oss;
        std::string str;

        oss << this->getNumSFG();
        //max(WFP_sfg_eff[xOps][0], WFP_sfg_eff[xOps][1]) - WFP_sfg[xOps][2];
        str = "max(" + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+0],";
        str += " " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+1])";
        str += " - " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+2]";// Si 2 est la sortie
        
        return str;
        
    }

    float SUBNode::resolveConstantSubTree(){
        NoeudData *nData;
        float acc;
        float value;

        if(this->getNbPred() < 2){
            trace_error("A subtraction operator have less two predecessors.\n");
            exit(-1);
        }

        nData = (NoeudData*) this->getElementPred(0);
        acc = nData->resolveConstantSubTree();
        if(std::isnan(acc))
            return NAN;

        for(int i = 1 ; i < this->getNbPred() ; i++){
            nData = (NoeudData*) this->getElementPred(i);
            value = nData->resolveConstantSubTree();
            if(!std::isnan(value)){
                acc -= value; 
            }
            else{
                acc = NAN;
            }
        }

        return acc;
    }

    DataDynamic SUBNode::dynamicRule(unsigned int size, DataDynamic operand[]){
		DataDynamic dynOut;

        if (size != 2) {
            trace_fatal("Dynamic rules not implemented yet for an subtraction operator with more of 2 operands\n");
            exit(-1);
        }

        dynOut.valMin = operand[0].valMin - operand[1].valMax;
        dynOut.valMax = operand[0].valMax - operand[1].valMin;

        return dynOut;
    }
    
    NoeudData* SUBNode::arithmeticOperationGeneration(fstream & matfile){
        NoeudData *nDataSucc, *nDataPred1, *nDataPred2;

        if(this->getNbInput() != 2) {
            trace_fatal("Arithmetic operation not implemented yet for an addition with more of 2 operands\n");
            exit(-1);
        }

        nDataSucc = dynamic_cast<NoeudData*> (this->getElementSucc(0));
        if(nDataSucc == NULL){
            trace_error("The successor node of a set operation is not a data node.\n");
            exit(0);
        }

        nDataPred1 = dynamic_cast<NoeudData*> (this->getElementPred(0));
        if(nDataPred1 == NULL){
            trace_error("The first predecessor node of a set operation is not a data node.\n");
            exit(0);
        }
        
        nDataPred2 = dynamic_cast<NoeudData*> (this->getElementPred(1));
        if(nDataPred2 == NULL){
            trace_error("The second predecessor node of a set operation is not a data node.\n");
            exit(0);
        }

        nDataPred1 = nDataPred1->genArithmeticOperation(matfile);
        nDataPred2 = nDataPred2->genArithmeticOperation(matfile);
        if(nDataPred1->getSrcSignal()){
            matfile << nDataSucc->getName().data() << " = -" << nDataPred2->getName().data() << ";\n";
        }
        else if(nDataPred2->getSrcSignal()){
            matfile << nDataSucc->getName().data() << " = " << nDataPred1->getName().data() << ";\n";
        }
        else{
            matfile << nDataSucc->getName().data() << " = " << nDataPred1->getName().data() << " - " << nDataPred2->getName().data() << ";\n";
        }

        return nDataSucc;
    
    }
    
    void SUBNode::moveNoiseSource(NoeudSrcBruit* noiseNode){
        NoeudData *nodeDataPred, *nodeDataSuiv;
        int numInputOps;

        assert(this->getNbSucc() == 1 && noiseNode->getNbPred() == 1);
        //Data(nodeDataPred)-->br(noiseNode)--->Ops(this)--->Data(nodeDataSuiv)
        nodeDataPred = dynamic_cast<NoeudData*> (noiseNode->getElementPred(0));
        nodeDataSuiv = dynamic_cast<NoeudData*> (this->getElementSucc(0));
        assert(nodeDataPred != NULL && nodeDataSuiv != NULL);
        assert(nodeDataPred->getTypeNodeGraph() != INIT && nodeDataSuiv->getTypeNodeGraph() != FIN);

        //test if nodeData has not more than one successor
        if (nodeDataSuiv->getNbSucc() == 1 && !nodeDataSuiv->isNodeOutput()) {

            // === Sauvegarde des numInput pour les futures modification sur les edges
            numInputOps = noiseNode->getEdgeSucc(0)->getNumInput();
            if(this->getElementPred(1) == noiseNode && noiseNode->getEdgeSucc(0)->getNumInput() == 1){
				((NoeudSrcBruit *) noiseNode)->inverseVectSigneMean();
            } 
            // === Redirection des arcs
            // Data(nodeDataPred)-->br(noiseNode)-->Ops(this)-->Data(nodeDataSuiv)
            nodeDataPred->deleteEdgeDirectedTo(noiseNode);
            // Data(nodeDataPred)  :  br(noiseNode)-->Ops(this)-->Data(nodeDataSuiv)
            noiseNode->deleteEdgeDirectedTo(this);
            // Data(nodeDataPred)  :  br(noiseNode)  :  Ops(this)-->Data(nodeDataSuiv)
            this->deleteEdgeDirectedTo(nodeDataSuiv);


            // Data(nodeDataPred)  :  br(noiseNode)  :  Ops(this)  :  Data(nodeDataSuiv)
            nodeDataPred->edgeDirectedTo(this, numInputOps);
            // Data(nodeDataPred)-->Ops(this)  :  br(noiseNode)  :  Data(nodeDataSuiv)
            noiseNode->edgeDirectedTo(nodeDataSuiv);
            // Data(nodeDataPred)-->Ops(this)  :  br(noiseNode)-->Data(nodeDataSuiv)
            this->edgeDirectedTo(noiseNode);
            // Data(nodeDataPred)-->Ops(this)-->br(noiseNode)-->Data(nodeDataSuiv)

            assert(noiseNode->getNbSucc() == 1);
            if (nodeDataSuiv->getTypeNodeData() == DATA_SRC_BRUIT) {
                // The node just below us is a noise node, it is our only successor and it is in the same block as the other noise source we are handling
                //we need to merge 2 noise sources: noiseNode and nodeGraphSuiv
                //we add current noise source parameter to next noise source: nodeGraphSuiv

                ((NoeudSrcBruit *) nodeDataSuiv)->calculSumParamNoise((NoeudSrcBruit *) (noiseNode)); //add parameter

                //noeudPred-->OPS-->Br(noiseNode)-->Br(nodeGraphSuiv)-->OPS   before
                this->deleteEdgeDirectedTo(noiseNode);
                noiseNode->deleteEdgeDirectedTo(nodeDataSuiv);
                this->edgeDirectedTo(nodeDataSuiv);
                //noeudPred-->OPS-->Br(nodeGraphSuiv)-->OPS   after

                //no recursion to continue move: this is end of process on noiseNode which is merged
                //in nodeGraphSuiv noise source-->these noise source is processed after with the tabNoiseSourcesNode

            }
            else
                ((NoeudGFD*)noiseNode->getElementSucc(0))->moveNoiseSource(noiseNode); // Appel recurcif
        }
    }
            
    LinearFunction* SUBNode::computeRuleLinearFunction(unsigned int size, LinearFunction* operand[]){
        LinearFunction *result;
        if (size != 2) {
            trace_fatal("Linear function rules not implemented yet for an subtraction with more of 2 operands\n");
            exit(-1);
        }
			
        result = *operand[0] - *operand[1];
        delete operand[0];
        delete operand[1];

        return result;
    }

    void SUBNode::noiseModelInsertion(Gfd *gfd){
        int nbPred;
        NoeudGraph *NG;
        NoeudGFD *NGFD;
        NoeudData *ND;
        NoeudOps *NO1;
        TypeSignalBruit typeSignalBruit;
        TypeNodeGFD TypeNGFD;

        // Le modele de bruit d'une soustraction (OPS_SIGNAL)est obtenue avec une soustraction (OPS_BRUIT)
        //
        //On a � la base, une soustraction(OPS_SIGNAL) avec les entr�es et sortie doubl�es(DATA_SIGNAL et DATA_BRUIT)

        NO1 = this->clone();
        NO1->setTypeSignalBruit(BRUIT);
        gfd->addOPNode(NO1);
        for (int i = 0; i < this->getNbSucc(); i++) // Redirection des arcs succ
        {
            NG = this->getElementSucc(i);
            NGFD = (NoeudGFD *) NG;
            TypeNGFD = NGFD->getTypeNodeGFD();
            if (TypeNGFD == DATA) {
                ND = (NoeudData *) NGFD;
                typeSignalBruit = ND->getTypeSignalBruit();
                if (typeSignalBruit == BRUIT) {
                    this->deleteEdgeDirectedTo(ND); // On suprime les arcs (OPS_SIGNAL vers DATA_BRUIT)
                    NO1->edgeDirectedTo(ND); // On ajoute les arcs (OPS_BRUIT vers DATA_BRUIT)
                }
            }
        }
        nbPred = this->getNbPred();
        for (int j = 0; j < nbPred; j++) // Redirection des arcs pred
        {
            NG = this->getElementPredTransformation(j);
            NGFD = (NoeudGFD *) NG;
            TypeNGFD = NGFD->getTypeNodeGFD();
            if (TypeNGFD == DATA) {
                ND = (NoeudData *) NGFD;
                typeSignalBruit = ND->getTypeSignalBruit();
                if (typeSignalBruit == BRUIT) {

                    // Sauvegarde du numInput avant modification des edges
                    int numInput = this->getEdgePredTransformation(j)->getNumInput();
                    this->EnleveedgeFromOf(ND); // On suprime les arcs (DATA_BRUIT vers OPS_SIGNAL)
                    NO1->edgeFromOf(ND, numInput-this->getNbInput()); // On ajoute les arcs (DATA_BRUIT vers OPS_BRUIT)
                }
            }
        }
    }

    /**
     *  Return a TypeLti in terms of the operation node
     *  	\return : TypeLti
     *
     *  SOUS => CONST +/- CONST -> CONST
     *  		SIGNAL +/- SIGNAL -> SIGNAL
     *  		SIGNAL +/- CONST -> SIGNAL
     *  		CONST +/- SIGNAL -> SIGNAL_LTI
     *
     *  Working only to operation node with 1 or 2 input
     * 
     *  \author Nicolas Simon
     *  \date 09/07/2010
     */
    TypeLti SUBNode::isLTI(){
        if(this->getNbInput() != 2){
            trace_fatal("LTI detection not implemented yet for an subtraction with more of 2 operands\n");
            exit(-1);
        }

        NoeudData *NData1;
        NoeudData *NData2;
        TypeLti TypeOp1;
        TypeLti TypeOp2;
        TypeLti Res;

        NData1 = dynamic_cast<NoeudData *> (this->getElementPred(0));
        assert(NData1 != NULL);
        TypeOp1 = NData1->isLTI();
        NData2 = dynamic_cast<NoeudData *> (this->getElementPred(1));
        assert(NData2 != NULL);
        TypeOp2 = NData2->isLTI();

        if (TypeOp1 == NOT_LTI || TypeOp2 == NOT_LTI) {
            Res = NOT_LTI;
        }
        else if (TypeOp1 == SIGNAL_LTI || TypeOp2 == SIGNAL_LTI) {
            Res = SIGNAL_LTI;
        }
        else {
            Res = CONST_LTI;
        }

        return Res;
    }

/**
 * Methode qui recherche le point commun entre un circuit et un graphe en parcourant celui-ci
 *
 *  \param  GraphPath   * liste contenant les numeros des noeuds du circuit
 *  \param  NoeudData *		output du graphe en cours
 *  \param  bool			true si parcours du demi graphe uniquement
 *  \param vector<NoeudData*>*	liste des noeud a demanteler
 *
 *  \author Loic Cloatre
 *  \date 16/12/2008
 */
	void SUBNode::rechercheDernierPointCommun(GraphPath * circuit, NoeudData * raciceGraphe, vector<NoeudData *>*listNoeudAdemanteler, bool demiGrapheParcouru) {
		//local declaration

		NoeudData *tempNoeudData;


		tempNoeudData = (NoeudData *) (this->getElementPred(0)); //premiere branche

		if (tempNoeudData->getEtat() == NONVISITE) //cas d'une boucle avec z-1
		{
			tempNoeudData->rechercheDernierPointCommun(circuit, raciceGraphe, listNoeudAdemanteler, demiGrapheParcouru); //calcul de l'operande de gauche
		}

		if (this->getNbPred() > 1) {
			tempNoeudData = (NoeudData *) (this->getElementPred(1)); //deuxieme branche

			if (tempNoeudData->getEtat() == NONVISITE) {
				tempNoeudData->rechercheDernierPointCommun(circuit, raciceGraphe, listNoeudAdemanteler, demiGrapheParcouru); //calcul de l'operande de gauche
				demiGrapheParcouru = true; //mis a true pour signaler que la branche this->getElementPred(1) a été parcourue
			}
		}
		else {
			demiGrapheParcouru = true; //mis a true pour signaler que la branche this->getElementPred(1) a été parcourue
		}
	}

}
