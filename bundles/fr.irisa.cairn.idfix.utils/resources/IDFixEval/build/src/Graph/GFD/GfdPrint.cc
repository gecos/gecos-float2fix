
/*!
 *  \author Daniel Menard, Jean-Charles Naud
 *  \date   20.02.2002
 *  \version 1.0
 */


// === Bibliothèques standard
#include	<iostream>
#include	<string>

#include "graph/dfg/datanode/NoeudSrc.hh"
#include "graph/dfg/EdgeGFD.hh"
#include "graph/dfg/Gfd.hh"
#include "graph/dfg/NoeudInitFin.hh"
#include "graph/dfg/operatornode/NoeudOps.hh"
#include "graph/toolkit/TypePrint.hh"
#include "graph/lfg/EdgeGFL.hh"
#include "graph/lfg/NoeudGFL.hh"
#include "graph/tfg/NodeGFT.hh"
#include "graph/toolkit/TypeVar.hh"
#include "utils/Tool.hh"


// === Bibliothèques non standard


// === Namespaces
using namespace std;

// ======================================= Methodes
/**
 * 	Ecriture de la liste des listSucc
 * 		\param fic : fichier de destination des informations si le parametre est stdout la destionation est l ecran
 */

void NoeudGraph::printListSucc(FILE * fic) {
   NoeudGraph *NGraph;

   ListSuccPred::iterator it;

   for (int i = 0 ; i<this->getNbSucc() ; i++)        // Parcours des listSucc
   {
      NGraph = this->getElementSucc(i);

      fprintf(fic, "      Suc %d: ", NGraph->getNumero());      // Numero du noeud
      NGraph->printInfo(fic);   // Information sur le noeud
   }
   fprintf(fic, "\n");
}

/**
 * 	Ecriture de la liste des listPred
 * 	\param fic : fichier de destination des informations si le parametre est stdout la destionation est l ecran
 */
void NoeudGraph::printListPred(FILE * fic) {
   NoeudGraph *NGraph;

   ListSuccPred::iterator it;
  // cout<<"Numero de this : "<<this->getNumero()<<endl;
//	cout<<"Nombre Pred this : "<<this->getNbPred()<<endl<<endl;
   for (int i = 0 ; i<this->getNbPred() ; i++)    // Parcours des listSucc
   {
      NGraph = this->getElementPred(i);

      fprintf(fic, "      Pred %d: ", NGraph->getNumero());     // Numero du noeud
      NGraph->printInfo(fic);   // Information sur le noeud
   }
   fprintf(fic, "\n");
}

/*
 * typeVarFonction d'affichage virtuelle du Noeud et de ces Suc Pred dans un ficher(fic), ou sur ecran
 */
void NoeudGraph::print(FILE * f) {
	printInfo(f);
	printListSucc(f);
	printListPred(f);
}
// --------------------------------------------- NoeudGFD ---------------------------------------------
/**
 * 	Ecriture du Noeud et de ces Suc Pred
 * 		\param fic : fichier de destination des informations si le parametre est stdout la destionation est l ecran
 */
void NoeudGFD::print(FILE * fic) {
   assert(fic != NULL);

   // Ecriture des informations du Noeud
   (this)->printInfo(fic);      //      NoeudData ou NoeudOps ou ...

   fprintf(fic, "\n nbSucc = %d ; nbPred = %d \n", this->getNbSucc(),     // Nombre de listSucc
           this->getNbPred());  // Numero de listPred

   // Ecriture de la liste des listSucc
   (this)->printListSucc(fic);   // NoeudGraph

   // Ecriture de la liste des listPred
   (this)->printListPred(fic);  // NoeudGraph
}
// --------------------------------------------- NoeudFin ---------------------------------------------

/**
 *	Ecriture des info du Noeud
 * 		\param fic : fichier de destination des informations si le parametre est stdout la destionation est l ecran
 */

void NoeudInit::printInfo(FILE * fic) {
   fprintf(fic, "Init");
}
void NoeudInit::print(FILE * fic) {
   fprintf(fic, "%d: INIT", this->getNumero()); // Numero du noeud

   // Ecriture des informations du Noeud
   // (this)->printInfo(fic);                   //      NoeudData ou NoeudOps ou ...

   fprintf(fic, "\n nbSucc = %d ; nbPred = %d \n", this->getNbSucc(), this->getNbPred());
   (this)->printListSucc(fic);
   (this)->printListPred(fic);
}
// --------------------------------------------- NoeudFin ---------------------------------------------
/**
 * Ecriture des info du Noeud
 * \param fic : fichier de destination des informations si le parametre est stdout la destionation est l ecran
 */
void NoeudFin::printInfo(FILE * fic) {
   fprintf(fic, "Fin");
}

void NoeudFin::print(FILE * fic) {
   fprintf(fic, "%d: FIN", this->getNumero());  // Numero du noeud

   // Ecriture des informations du Noeud
   // (this)->printInfo(fic);                      //  NoeudData ou NoeudOps ou ...

   fprintf(fic, "\n nbSucc = %d ; nbPred = %d \n", this->getNbSucc(), this->getNbPred());
   (this)->printListSucc(fic);
   (this)->printListPred(fic);
}
// --------------------------------------------- NoeudGFT ---------------------------------------------
/**
 * Ecriture des info du Noeud
 * \param fic : fichier de destination des informations si le parametre est stdout la destionation est l ecran
 */
void NoeudGFT::print(FILE * f) {
   NoeudGraph::print(f);

   switch (typeNoeudGFT) {
   case PHI_GFT:
      fprintf(f, "Type = PHI\n");
      break;
   case VAR_GFT:
      fprintf(f, "Type = VAR\n");
      break;
   default:
      fprintf(f, "Type inconnu!\n");
   }
}

// --------------------------------------------- NoeudData ---------------------------------------------
/**
 * Methode d'ecriture des infos du noeud
 * \param fic : fichier de destination des informations si le parametre est stdout la destionation est l ecran
 */
void NoeudData::printInfo(FILE * fic) {
   TypeVar *typeVar;
   //ParamStatBruit *      Param ;
   fprintf(fic, "Data = \"%s\" : TypeNode = %s", getName().data(),   // Numero du noeud
           typeNodeDataConvToStr(getTypeNodeData()).c_str()        // Type de DATA
      );

   // Information sur typeVar
   typeVar = this->getTypeVar();

   if (typeVar != NULL) {
      fprintf(fic, ", ");
      typeVar->printInfo(fic);
   }
   else {
      fprintf(fic, ", typeVar=NULL");
   }
}

// --------------------------------------------- TypeVar ---------------------------------------------
/**
 * 	Methode d'ecriture des infos du noeud
 * 		\param fic : fichier de destination des informations si le parametre est stdout la destionation est l ecran
 */
void TypeVar::printInfo(FILE * fic) {
   fprintf(fic, "TypeVar =[%s]", typetypeVarFonctConvToStr(this->getTypeVarFonct()).c_str());
}

// --------------------------------------------- NoeudOps ---------------------------------------------


/**
 * 	Methode d ecriture des infos du noeud
 * 		\param fic : fichier de destination des informations si le parametre est nul la destionation est l ecran
 */
void NoeudOps::printInfo(FILE * fic) {

   fprintf(fic,
           "OPS = \"%s\" nbInput=%d",
           this->toString().c_str(),
           this->getNbInput());
}


// --------------------------------------------- NoeudGFL --------------------------------------------
void NoeudGFL::printInfoGflVCG(FILE *fp)	///
{
	if(fp!=NULL)														// Si on a un fichier en entree
	{
		fprintf(fp, "node: { title:\"%d\"  color:cyan label:\"%s\" }\n",
			Numero, this->getName().data());
	}
}

// ======================================= Methodes de sauvegarde VCG
// =================================================================
// =================================================================


// --------------------------------------------- NoeudGraph --------------------------------------------

/**
 * typeVarFonction d ecriture de la liste des listSucc au format VCG
 * 	\param fic : fichier de destination des informations
 */

void NoeudGraph::printListSuccVCG(FILE * fp) {

   int NumeroSuc;
   ListSuccPred::iterator it;

   if (fp) {
      for (it = listSucc.begin(); it != listSucc.end(); ++it)     // Parcours des listSucc
      {
         NumeroSuc = (*it)->getNodeSucc()->getNumero();        // On prend le numero du noeud successeur

         if (NumeroSuc > 1)     // On ignore les numeros 0 et 1 (NoeudInit et NoeudFin)
         {
            string label = (*it)->getLabel();

            if (label.empty())
               fprintf(fp,
                       "edge: { color: %s thickness:3 sourcename:\"%d\" targetname:\"%d\"}\n",
                       it == listSucc.begin()? "black" : "pink",
                       this->Numero, NumeroSuc);
            else
               fprintf(fp,
                       "edge: { color:%s thickness:3 sourcename:\"%d\" targetname:\"%d\" label:\"%s\"}\n",
                       it == listSucc.begin()? "red" : "magenta",
                       this->Numero, NumeroSuc, label.c_str());
         }
      }
   }
}

// --------------------------------------------- NoeudData --------------------------------------------

/**
 * 	Methode global d'ecriture des infos du noeud Data au format VCG
 * 		\param fp : fichier de destination des informations
 */
void NoeudData::printDataVCG(FILE * fp) {

   /* Ecriture des informations specifiques au noeud */
   (this)->printInfoDataVCG(fp);

   /* Ecriture de la liste des listSucc */
   (this)->printListSuccVCG(fp); // NoeudGraph

}


/**
 * 	Methode d'ecriture des infos du noeud Data au format VCG
 * 		\param fp : fichier de destination des informations
 */

void NoeudData::printInfoDataVCG(FILE * fp) {
   //string  chaine;
   //char* chaine = "cyan";
   string chaine = "yellow";
   if (fp != NULL) {             // Si on a un fichier en entree
      if (this->getTypeSignalBruit() ==  BRUIT /*&&this->getsousArbreSignal()==true */ ) {
         //chaine="yellow";
         chaine = "green";
      }
      else if (this->getTypeSignalBruit() == SIGNAL && this->getTypeSimu() == true /*&& this->isNodeSource()==false */ ) {
         chaine = "cyan";
      }
      else
         if ((this->getTypeSignalBruit() == SIGNAL && this->getTypeSimu() == false) /*||(this->isNodeSource()==true && this->getTypeSignalBruit()==SIGNAL) */) {
         chaine = "red";
      }
      fprintf(fp,
              "node: { title:\"%d\"  color:%s label:\"%s\" info1:\"%2f\" info2:\"%2f\"}\n",
              Numero, chaine.c_str(), (this)->getName().c_str(),
              getDataDynamic().valMin, getDataDynamic().valMax);
   }
}

void NoeudSrc::printSrcVCG(FILE * fp) {
   // Ecriture des informations specifiques au noeud
   (this)->printInfoSrcVCG(fp);

   // Ecriture de la liste des listSucc
   (this)->printListSuccVCG(fp);
}

void NoeudSrc::printInfoSrcVCG(FILE * fp) {
   fprintf(fp,
           "node: { title:\"%d\"  color:%s label:\"%s\" info1:\"%2f\" info2:\"%2f\"}\n",
           Numero, "blue", (this)->getName().c_str(),
           getDataDynamic().valMin, getDataDynamic().valMax);
}

/**
 * Methode global d'ecriture des infos du noued Ops au format VCG
 *
 * \param fp : fichier de destination des informations
 */
void NoeudOps::printOpsVCG(FILE * fp) {
   // Ecriture des informations specifiques au noeud
   (this)->printInfoOpsVCG(fp);

   // Ecriture de la liste des listSucc
   (this)->printListSuccVCG(fp); // NoeudGraph
}

// --------------------------------------------- NoeudOps --------------------------------------------

/**
 * 	Methode d'ecriture des infos du noeud Data au format VCG
 * 		\param fp : fichier de destination des informations
 */

void NoeudOps::printInfoOpsVCG(FILE * fp) {
   string StrSymbolOps;
   StrSymbolOps = this->toString(); 

   if (fp != NULL) {              // Si on a un fichier en entree
      fprintf(fp,
              "node: { title:\"%d\" shape: box label:\"%s (%d),SFG(%d),CDFG(%d)\"   }\n",
              getNumero(), StrSymbolOps.c_str(), getNumero(), this->getNumSFG(),
              numCDFG);
   }
}

// --------------------------------------------- NoeudGFL --------------------------------------------

void NoeudGFL::printVCG(FILE *fp)	{
	this->printInfoGflVCG(fp); // Ecriture des informations specifiques au noeud
	this->printListSuccVCG(fp); // Ecriture de la liste des listSucc
}					// Methode pour ecrire le GFL au format VCG



// --------------------------------------------- NoeudGFT --------------------------------------------
void NoeudGFT::printVCG(FILE * f) {
	fprintf(f, "node: { title:\"%d\"  color:pink label:\"%s\"}\n",
			Numero, this->getName().data());
	printListSuccVCG(f);
}




/*
 * Functions to dislpay all type of graph to dot graph Format
 * 		
 *	Many information can be add into the node and edge representation via these Functions
 *
 *	16/03/11 - Nicolas Simon
 */



void NoeudGFL::printDOTEdge(FILE *f){
	for(int j = 0 ; j < this->getNbSucc() ; j++){
		if(this->getElementSucc(j)->getTypeNodeGraph() != FIN){
			fprintf(f, "n_%i -> n_%i [label=\"%s\"];\n", this->getNumero(), this->getElementSucc(j)->getNumero(), ((EdgeGFL*)this->getEdgeSucc(j))->getPseudoFonctionLineaire()->toString().data());
		}
	}
}

void NoeudGFL::printDOTNode(FILE *f, bool noiseEval){
	if(noiseEval)
		fprintf(f,"n_%i [label=\"%s\\n%s\\n%i\"];\n", this->getNumero(), this->getOriginalName().data(), this->getName().data(), this->getNumero());
	else
		fprintf(f,"n_%i [label=\"%s\\n%s\\n%i\"];\n", this->getNumero(), this->getRefParamSrc()->getOriginalName().data(), this->getName().data(), this->getNumero());
}

void NoeudData::printDOTEdge(FILE *f){
	for(int i = 0 ; i < this->getNbSucc() ; i++){
		if(this->getEdgeSucc(i)->getNodeSucc()->getTypeNodeGraph() != FIN){
			fprintf(f, "n_%i -> n_%i [label=\"%i\"];\n", this->getNumero(), this->getEdgeSucc(i)->getNodeSucc()->getNumero(), this->getEdgeSucc(i)->getNumInput());
		}
	}	
}

void NoeudData::printDOTNode(FILE *f, bool noiseEval){
	string color;
	string dynamicProcess;

	if (this->getTypeSignalBruit() ==  BRUIT)
		color = "green";
	else if (this->getTypeSignalBruit() == SIGNAL && this->getTypeSimu())
		color = "cyan";
	else if(this->isNodeOutput())
		color = "red";
	else
		color = "";

	if(noiseEval)
		fprintf(f,"n_%i [label=\"%s\\n%s\\nNum : %i - CDFG : %i - Affect : %i\" style=\"filled\" color=\"%s\" fillcolor=\"%s\"];\n", this->getNumero(), this->getOriginalName().data(), this->getName().data(), this->getNumero(), this->getNumCDFG(), this->getNumAffect(), color.data(), color.data());
	else {
		switch(this->getDynProcess()){
		case OVERFLOW_PROBA :
			dynamicProcess = "overflow_proba";
			break;
		case INTERVAL :
			dynamicProcess = "interval";
			break;
		default :
			trace_error("dynamicProcess non defini ou inconnu");
			exit(-1);
			break;
		}
        if(this->isNodeConst()){
            fprintf(f,"n_%i [label=\"%s\\n%s\\nNum : %i - CDFG : %i\\n%s\\n%e\" style=\"filled\" color=\"%s\" fillcolor=\"%s\"];\n", this->getNumero(), this->getOriginalName().data(), this->getName().data(),this->getNumero(), this->getNumCDFG(), dynamicProcess.data(), this->getValeur(), color.data(), color.data());
        }
        else{
            fprintf(f,"n_%i [label=\"%s\\n%s\\nNum : %i - CDFG : %i\\n%s\\n%e\\n%e   %e\" style=\"filled\" color=\"%s\" fillcolor=\"%s\"];\n", this->getNumero(), this->getOriginalName().data(), this->getName().data(),this->getNumero(), this->getNumCDFG(), dynamicProcess.data(), this->getProbDeb(), this->getDataDynamic().valMin, this->getDataDynamic().valMax, color.data(), color.data());
        }
        
	}
}

void NoeudOps::printDOTEdge(FILE *f){
	for(int i = 0 ; i < this->getNbSucc() ; i++){
		if(this->getEdgeSucc(i)->getNodeSucc()->getTypeNodeGraph() != FIN){
			fprintf(f, "n_%i -> n_%i [label=\"%i\"];\n", this->getNumero(), this->getEdgeSucc(i)->getNodeSucc()->getNumero(), this->getEdgeSucc(i)->getNumInput());
		}
	}	
}

void NoeudSrc::printDOTNode(FILE *f, bool noiseEval){
	string dynamicProcess;
	switch(this->getDynProcess()){
		case OVERFLOW_PROBA :
			dynamicProcess = "overflow_proba";
			break;
		case INTERVAL :
			dynamicProcess = "interval";
			break;
		default :
			trace_error("dynamicProcess non defini ou inconnu");
			exit(-1);
			break;
	}
	fprintf(f,"n_%i [label=\"%s\\n%s\\nNum : %i - CDFG : %i\\n%s\\n%e\\n%e   %e\" style=\"filled\"];\n", this->getNumero(), this->getOriginalName().data(), this->getName().data(), this->getNumero(), this->getNumCDFG(), dynamicProcess.data(), this->getProbDeb(), this->getDataDynamic().valMin, this->getDataDynamic().valMax);
}

void NoeudSrcBruit::printDOTNode(FILE *f, bool noiseEval){
	fprintf(f,"n_%i [label=\"%s\\n%s\\nBlock : %d\" style=\"filled\"];\n", this->getNumero(), this->getName().data(), this->getNoiseOfSource() ? "true" : "false", this->getBlock().getID());
}


void NoeudOps::printDOTNode(FILE *f, bool noiseEval){
	string color = this->getDotColor();
	fprintf(f,"n_%i [label=\"%s\\nCDFG : %i - SFG : %i - Num : %i\\n Block : %d\" style=\"filled\" fillcolor=\"%s\"];\n", this->getNumero(), this->getName().data(), this->getNumCDFG(), this->getNumSFG(), this->getNumero(), this->getBlock().getID(), color.data());
}



void NoeudGFT::printDOTEdge(FILE *f){
	for(int j = 0 ; j<this->getNbSucc() ; j++){
		if(this->getElementSucc(j)->getTypeNodeGraph() != FIN){
			fprintf(f, "n_%i -> n_%i [label=\"%s\\n%s\"]; \n", this->getNumero(), this->getElementSucc(j)->getNumero(), ((EdgeGFT*)this->getEdgeSucc(j))->getFT()->toStringNum().c_str(), ((EdgeGFT*)this->getEdgeSucc(j))->getFT()->toStringDen().c_str());
		}
	}
}

void NoeudGFT::printDOTNode(FILE *f, bool noiseEval){
	fprintf(f,"n_%i [label=\"%s\\n%i\"];\n", this->getNumero(), this->getName().data(), this->getNumero());
}
