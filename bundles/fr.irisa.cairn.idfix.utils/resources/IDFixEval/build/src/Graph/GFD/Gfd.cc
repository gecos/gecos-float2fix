/**
 *  \file Gfd.cc
 *  \author Daniel Menard, Jean-Charles Naud
 *  \date   31.01.2002
 *  \class Gfd
 *  \brief Classe definissant un Graphe Flot de Donnees (GFD)
 */

// === Bibliothèques .hh associe
#include "graph/dfg/Gfd.hh"

// === typeVarFonctions extern
extern NodeGraphContainer::iterator Recherche(NoeudGraph * Noeud, NodeGraphContainer * Liste);

// === Variables extern
extern FILE *LogFile;
extern char *BufferLog;
extern void printLogFile(char *Comment);

using namespace std;

// ======================================= Constructeurs - Destructeurs
// ===============================================================
// ===============================================================
/**
 *  Constructeur de la classe Gfd
 *  \param noiseEval : Vrai si le graphe est spécifique au traitement de l'évaluation de la précision, faux pour la détermination de la précision
 */
Gfd::Gfd(bool noiseEval) :
	Graph(noiseEval) {
	numSrcNode = 0;
}


/**
 *  Destructeur de la classe Gfd
 */
Gfd::~Gfd() {
	if (m_logDebug_pf.is_open()) {
		m_logDebug_pf.close();
	}
}


void Gfd::deleteNoiseNode(NoeudSrcBruit* node){
	vector<NoeudSrcBruit*>::iterator it;
	for(it = tabNoiseSourcesNode.begin() ; it != tabNoiseSourcesNode.end() ; it++){
		if(*it == node){
			tabNoiseSourcesNode.erase(it);
			break;
		}
	}
}


// ======================================= Methodes

/**
 * Acces au noeud de nom name, pas de test si plusieurs noeuds ont le meme nom
 *
 *	\param name :nom du noeud recherche dans tabNoeud
 *	\return Rend un pointer sur le 1er noeud ayant un nom identique
 */
NoeudGraph *Gfd::getElementGFD(const string & name) {
	NoeudGraph *NoeudRet = NULL;
	bool trouve = false;
	for (int i = 0; i < (this)->getNbNode(); i++) {
		if (getElementGf(i)->getName() == name) //si les noms sont identiques
		{
			NoeudRet = (this)->getElementGf(i);
			i = (this)->getNbNode(); //pour sortir de la boucle
			trouve = true;
		}
	}
	if (!trouve){
		trace_error("noeud non trouve: %s", name.c_str());
		exit(-1);
	}

	return NoeudRet;

}

// ======================================= Methodes d'ajout de NoeudData dans le GFD
// ===============================================================
// ===============================================================

/**
 * 	Fonction d'ajout d'un NoeudData dans le Gfd
 *   	\param Nom : nom de la variable
 *   	\param TypeVariable : pointeur sur l'objet representant le type de la variable (class TypeVar)
 *   	\param blockId : Block contenant le NoeudData
 *   	\return Rend un pointer sur le NoeudData nouvellement créé et ajouté
 *  	
 */

NoeudData *Gfd::addNoeudData(string originalName, TypeVar * typeVariable) {
	NoeudData *NData;

	// Creation du noeud Data (Type_DATA=DATA_BASE, NumNode = Numeros du prochain noeud)
	string varName = RuntimeParameters::generateVarName(originalName);
	NData = new NoeudData(DATA_BASE, NumNode, varName, typeVariable, originalName);

	// Ajout dans le GFD
	addNoeudGraph(NData); // Ajout dans le GFD (dans la liste des NOEUDS)

	if (NData->isNodeInput()) {
		addInputGf(NData); //      Ajout dans le GFD (dans la liste des ENTREES)
	}
	if (NData->isNodeOutput()) {
		addOutputGf(NData); //      Ajout dans le GFD (dans la liste des SORTIE)
	}
	if (NData->isNodeVar()) {
		addVarIntGf(NData);
	}

	return NData;

}

NoeudData *Gfd::addNoeudData(string originalName, TypeVar * typeVariable, TypeSignalBruit typeSignalBruit) {
	NoeudData *NData;

	// Creation du noeud Data (Type_DATA=DATA_BASE, NumNode = Numeros du prochain noeud)
	string varName = RuntimeParameters::generateVarName(originalName);
	NData = new NoeudData(DATA_BASE, NumNode, varName, typeVariable, typeSignalBruit, originalName);

	// Ajout dans le GFD
	addNoeudGraph(NData); // Ajout dans le GFD (dans la liste des NOEUDS)

	if (NData->isNodeInput()) {
		addInputGf(NData); //      Ajout dans le GFD (dans la liste des ENTREES)
	}
	if (NData->isNodeOutput()) {
		addOutputGf(NData); //      Ajout dans le GFD (dans la liste des SORTIE)
	}
	if (NData->isNodeVar()) {
		addVarIntGf(NData);
	}

	return NData;

}

/**
 * Fonction d'ajout d'un NoeudData dans le Gfd
 *   \param NData : pointeur sur l'objet data
 */

void Gfd::addNoeudData(NoeudData * NData) {

	// Ajout dans le GFD
	addNoeudGraph(NData); // Ajout dans le GFD (dans la liste des NOEUDS)

	if (NData->isNodeInput()) {
		addInputGf(NData); //      Ajout dans le GFD (dans la liste des ENTREES)
	}

	if (NData->isNodeOutput()) {
		addOutputGf(NData); //      Ajout dans le GFD (dans la liste des SORTIE)
	}

	if (NData->isNodeVar()) {
		addVarIntGf(NData);
	}
}

// ======================================= Methodes d'ajout de NoeudOps dans le GFD
// ===============================================================
// ===============================================================
/**
 * Fonction d'ajout d'un NoeudOps dans le Gfd
 *   \param NbIn : nombre d'entree de l'operateur
 *   \param TypeOp : fonction de l'operation
 *   \param blockId : Block contenant le NoeudOps
 *   \return Rend le pointer sur le NoeudOps crée et ajouté
 *
 */


void Gfd::addOPNode(NoeudOps * node){
    node->setNumero(NumNode);
    addNoeudGraph(node);
}

// ======================================= Methodes
// ===============================================================
// ===============================================================

/**
 * Fonction d'ajout d'un NoeudSrcBruit dans le Gfd
 *   \param Nom : nom de la variable
 *   \param pasQuantif : pas de quantification
 *   \param KbitsElimine : nombre de bits elimines
 *   \param TypeSignalBruit : Type de signal du NoeudSrcBruit
 *   \param blockId : Block contenant le NoeudSrcBruit
 *   \param noiseOfSource : Vrai si le bruit representé par le NoeudSrcBruit est associé à une source du système
 *   \return Rend le pointer sur le NoeudSrcBruit nouvellement créé et ajouté
 */
void Gfd::addNoeudSrcBruit(NoeudData* nodeData) {
	string nom;
	NoeudSrcBruit *noeudSrcBruit = NULL;
	NoeudOps* nodeOps;

	// === Création du nom du noeud
	numSrcNode++;
	ostringstream oss; // Int to String
	oss << numSrcNode;
	nom = NAME_NOISE_NODE; // "Br"
	nom += oss.str(); // "Br22"

	assert(nodeData->getNbPred() == 1);
	nodeOps = dynamic_cast<NoeudOps*> (nodeData->getElementPred(0));
	assert(nodeOps != NULL);

	// === Création du NoeudSrcNoise
	noeudSrcBruit = new NoeudSrcBruit(NumNode, nom, BRUIT, nodeOps->getBlock());
	this->addNoiseNode(noeudSrcBruit);
	this->addNoeudGraph(noeudSrcBruit); // Ajout dans le GFD
	noeudSrcBruit->getTypeVar()->setTypeVarFonct(VAR_INT); // Par defaut un Br est une entrée alors qu'ici, nous sommes que a l'insertion
	noeudSrcBruit->setNumSrc(numSrcNode);


	// === Redirection des arcs
	// Before : noeudOpsOut --> noeudData
	nodeOps->deleteEdgeDirectedTo(nodeData);
	nodeOps->edgeDirectedTo(noeudSrcBruit);
	noeudSrcBruit->edgeDirectedTo(nodeData);
}

void Gfd::addNoeudSrcBruit(NoeudOps* nodeOps){
	string nom;
	NoeudSrcBruit *noeudSrcBruit = NULL;
	NoeudData* nodeData;
	int numInput;

	for(int i = 0 ; i < nodeOps->getNbPred() ; i++){
		// === Création du nom du noeud
		numSrcNode++;
		ostringstream oss; // Int to String
		oss << numSrcNode;
		nom = NAME_NOISE_NODE; // "Br"
		nom += oss.str(); // "Br22"

		nodeData = dynamic_cast<NoeudData*> (nodeOps->getEdgePred(i)->getNodePred());
		numInput = nodeOps->getEdgePred(i)->getNumInput();
		assert(nodeData != NULL);

		if(!nodeData->isNodeConst()){ // On ajoute pas de Br entre une constante et un opérateur
			if(nodeData->isNodeInput())
				noeudSrcBruit = new NoeudSrcBruit(NumNode, nom, BRUIT, nodeOps->getBlock(), true); // Tag le Br en précisant que le Br a été crée au niveau d'une entrée
			else
				noeudSrcBruit = new NoeudSrcBruit(NumNode, nom, BRUIT, nodeOps->getBlock(), false);
			this->addNoiseNode(noeudSrcBruit);
			this->addNoeudGraph(noeudSrcBruit); // Ajout dans le GFD
			noeudSrcBruit->getTypeVar()->setTypeVarFonct(VAR_INT); // Par defaut un Br est une entrée alors qu'ici, nous sommes que a l'insertion
			noeudSrcBruit->setNumSrc(numSrcNode);

			nodeData->deleteEdgeDirectedTo(nodeOps);
			nodeData->edgeDirectedTo(noeudSrcBruit);
			noeudSrcBruit->edgeDirectedTo(nodeOps, numInput);
		}
	}
}



NoeudSrcBruit* Gfd::addNoeudSrcBruitCopie(NoeudSrcBruit* node){
	NoeudSrcBruit * nodeCopie = new NoeudSrcBruit(*node);
	nodeCopie->setNumero(this->NumNode);
	this->addNoiseNode(nodeCopie);
	this->addNoeudGraph(nodeCopie);

	return nodeCopie;


}

/**
 *  Function which update number of all NoeudSrc
 *
 *  \author Nicolas Simon
 *  \date 12/07/2010
 */
void Gfd::initNumSrc() {
	NoeudSrc *NSrc;
	numSrcNode = 0;

	for (int i = 0; i < this->getNbInput(); i++) {
		numSrcNode++;
		NSrc = (NoeudSrc *) this->getInputGf(i);
		NSrc->setNumSrc(numSrcNode);
	}

}

/**
 *  Function which display number of all NoeudSrc
 *
 *  \author Nicolas Simon
 *  \date 12/07/2010
 */
void Gfd::displayNumSrc() {
	int NbSrc;

	for (int i = 0; i < this->getNbInput(); i++) {
		NbSrc = ((NoeudData *) this->getInputGf(i))->getNumSrc();
		cout << "Num Noise " << i << " : " << NbSrc << endl;
	}

}

/*
 * Create a copy of a GFD
 *	\return Rend un pointer vers le nouveau GFD entièrement copié
 * \author Nicolas Simon
 * \date 06/09/2010
 */

Gfd *Gfd::copie() {

	NoeudGFD *NoeudVar;
	NoeudGFD *copieNoeudVar;
	NoeudGFD *NoeudVarSuc;
	NoeudGFD *copieNoeudVarSuc;

	Gfd *copieGfd = new Gfd(this->getNoiseEval());
	NoeudGFD **tab; // Tableau utile pour conserver l'ordre des Noeud dans les listes (important dans certain algo)
	tab = new NoeudGFD *[this->getNbNode()];

	for (int i = 0; i < this->getNbNode(); i++)
		tab[i] = NULL;

	for (int i = 2; i < this->getNbNode(); i++) {
		NoeudVar = (NoeudGFD *) this->getElementGf(i);

		if (tab[i] == NULL) { // Noeud principal non copie
			copieNoeudVar = (NoeudGFD *)NoeudVar->clone();
			tab[i] = copieNoeudVar; // copie et ajout du noeud
		}
		else { // Noeud principal deja copie
			copieNoeudVar = tab[i];
		}

		int nbSucc;
		nbSucc = NoeudVar->getNbSucc();

		for (int j = 0; j < nbSucc; j++) {
			NoeudVarSuc = (NoeudGFD *) NoeudVar->getEdgeSucc(j)->getNodeSucc();
			if (NoeudVarSuc->getNumero() != 1) {
				if (tab[NoeudVarSuc->getNumero()] == NULL) { //Noeud Suc non copie
					copieNoeudVarSuc = (NoeudGFD *)NoeudVarSuc->clone();
					tab[copieNoeudVarSuc->getNumero()] = copieNoeudVarSuc; // Ajout au tableau
				}
				else { // Noeud Suc deja copie
					copieNoeudVarSuc = tab[NoeudVarSuc->getNumero()];
				}
				// On recupere l'information sur le numInput de l'arc	
				copieNoeudVar->edgeDirectedTo(copieNoeudVarSuc, NoeudVar->getEdgeSucc(j)->getNumInput()); // Creation de l'arc en ajoutant le format
			}
		}
	}

	for (int i = 2; i < this->getNbNode(); i++) {

		if (tab[i]->getTypeNodeGFD() == OPS) {
			copieGfd->addNoeudGraph(tab[i]);
		}
		else {
			copieGfd->addNoeudData((NoeudData *) tab[i]);
		}
	}

	//copie des dernières information
	copieGfd->setNoiseEval(this->getNoiseEval());
	copieGfd->liaisonsSuccPredInitFin();

	delete[] tab;
	return copieGfd;

}

/**
 *	Supprime les noeud isolée et renumerote correctement les noeuds
 *
 */
void Gfd::deleteIsolatedNodeNoise() {
	Graph::deleteIsolatedNodeNoise();

	//on renumerote les sources de bruit
	int newNumeroSrcBruit = 1;
	for (int i = 0; i < this->getNbNode(); i++) //on regarde chaque noeud du graphe
	{
		if (dynamic_cast<NoeudGFD *> (getElementGf(i)) != NULL && ((NoeudGFD *) (this->getElementGf(i)))->getTypeNodeGFD() == DATA) {
			if (((NoeudData *) (this->getElementGf(i)))->isNodeSourceBruitBR() == true) {
				std::ostringstream out;
				out << newNumeroSrcBruit;
				string sNum = out.str();
				string s = "Br" + sNum;

				((NoeudSrcBruit *) (this->getElementGf(i)))->setName(s);
				((NoeudSrcBruit *) (this->getElementGf(i)))->setNumSrc(newNumeroSrcBruit);
				newNumeroSrcBruit++;
			}
		}
	}
}

/**
 * Rajoute des noeuds intermédiaires entre deux noeuds opérations sur le graphe en entrée
 * \author Quentin Meunier
 * \author Nicolas Simon
 */
void Gfd::addVarIntNodes() {
	int numInput;
	this->resetInfoVisite();

	NoeudGraph * node = getNoeudInit();

	list<NoeudGraph *> nodesToProcess;
	for (int i = 0; i < node->getNbSucc(); i++) {
		nodesToProcess.push_back(node->getElementSucc(i));
	}
	while (nodesToProcess.size() != 0) {
		node = nodesToProcess.front();
		nodesToProcess.pop_front();
		node->setEtat(VISITE);
		for (int j = 0; j < node->getNbSucc(); j++) {
			if (node->getElementSucc(j)->getTypeNodeGraph() != FIN && node->getElementSucc(j)->getEtat() == NONVISITE) {
				nodesToProcess.push_back(node->getElementSucc(j));
			}
		}
		if (((NoeudGFD *) node)->getTypeNodeGFD() == OPS) {
			/* On parcourt la liste des suivants pour voir s'il y a des noeuds opérations */
			for (int j = 0; j < node->getNbSucc(); j++) {
				NoeudGraph * succNode = node->getElementSucc(j);
				if (((NoeudGFD *) succNode)->getTypeNodeGFD() == OPS) {
					/* On rajoute un Noeud VAR entre les deux */
					//NoeudData * newNode = this->addNoeudData((char *) "Added Var Node", new TypeVar(VAR, LOCAL, DIRECTE, SCALAIRE, AUCUN));
					NoeudData * newNode = this->addNoeudData((char *) "Added Var Node", new TypeVar(VAR));
					node->edgeDirectedTo(newNode);

					ListSuccPred::iterator it2;
					numInput = -1;
					for (it2 = succNode->getListePred()->begin(); it2 != succNode->getListePred()->end(); it2++) {
						if ((*it2)->getNodePred() == node) {
							numInput = (*it2)->getNumInput();
							break;
						}
					}
					assert(numInput != -1); // numInput doît forcement être affecté au dessus
					node->deleteEdgeDirectedTo(succNode);
					newNode->edgeDirectedTo(succNode, numInput);
				}
			}
		}
	}
}

void Gfd::resolveConstantSubTree(){
    NodeGraphContainer::const_iterator it;
    for(it = this->getBeginTabOutput() ; it != this->getEndTabOutput() ; it++){
       ((NoeudGFD*) *it)->resolveConstantSubTree();
    }
}


//TODO a verifié si cela est utile, autrement a supprimé
void Gfd::sortListEdgePred() {
	NoeudGFD * NGfd;
	vector<Edge *>::iterator it;
	vector<Edge *> *listEdge;
	for (int i = 2; i < this->getNbNode(); i++) {
		NGfd = (NoeudGFD*) this->getElementGf(i);
		if (NGfd->getTypeNodeGFD() == OPS) {
			listEdge = NGfd->getListePred();
			it = listEdge->begin();
			if ((*it)->getNumInput() == 1) {
		//		listEdge->reverse();
			}
		}
	}
}
