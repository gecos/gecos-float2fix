#include <cmath>

#include "graph/dfg/operatornode/kind/ADDNode.hh"
#include "graph/dfg/operatornode/kind/MULNode.hh"
#include "backend/NoisePowerExpressionGeneration.hh"

namespace gfd {
    MULNode::MULNode(Block &block, unsigned int input_number):NoeudOps(-1, input_number, block, "MUL"){}
    MULNode::MULNode(const MULNode &other):NoeudOps(other){}

    MULNode* MULNode::clone(){
        return new MULNode(*this);
    }
    
    std::string MULNode::WFPEffDetermination(){
        std::ostringstream oss;
        std::string str;
	
        oss << this->getNumSFG(); //int to str
        // WFPeff[xOps][2] = min(WFP[xOps][2], WFPeff[xOps][0] + WFPeff[xOps][1]);
        str = "   " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+2]"; // Si 2 est la sortie
        str += " = min(" + __NOISEPOWER_WFPSFG__ + "[" + oss.str() + "*3+2],";
        str += "   " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+0] +";
        str += "   " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+1]);";

        return str;
    }

    std::string MULNode::KDetermination(){
        std::ostringstream oss;
        std::string str;

        oss << this->getNumSFG();
        // WFP_sfg_eff[xOps][0] + WFP_sfg_eff[xOps][1] - WFP_sfg[xOps][2];
        str = __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+0]";
        str += " + " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+1]";
        str += " - " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+2]";

        return str;
    }

    float MULNode::resolveConstantSubTree(){
        NoeudData *nData;
        float acc = 1;
        float value;

        if(this->getNbPred() < 2){
            trace_error("A multiplication operator have less two predecessors.\n");
            exit(-1);
        }

        for(int i = 0 ; i < this->getNbPred() ; i++){
            nData = (NoeudData*) this->getElementPred(i);
            value = nData->resolveConstantSubTree();
            if(!std::isnan(value)){
                acc *= value; 
            }
            else{
                acc = NAN;
            }
        }

        return acc;
    }

    DataDynamic MULNode::dynamicRule(unsigned int size, DataDynamic operand[]){
		DataDynamic dynOut;

        if (size != 2) {
            trace_fatal("Dynamic rules not implemented yet for an multiplication operator with more of 2 operands\n");
            exit(-1);
        }

        double tabPartialProduct[4] = { 0, 0, 0, 0 };
        tabPartialProduct[0] = operand[0].valMin * operand[1].valMin;
        tabPartialProduct[1] = operand[0].valMin * operand[1].valMax;
        tabPartialProduct[2] = operand[0].valMax * operand[1].valMin;
        tabPartialProduct[3] = operand[0].valMax * operand[1].valMax;
        dynOut.valMin = *min_element(tabPartialProduct, tabPartialProduct + 4); //+4 because read 4 data
        dynOut.valMax = *max_element(tabPartialProduct, tabPartialProduct + 4); //+4 because read 4 data

        return dynOut;
    }

    NoeudData* MULNode::arithmeticOperationGeneration(fstream & matfile){
        NoeudData *nDataSucc, *nDataPred1, *nDataPred2;

        if(this->getNbInput() != 2) {
            trace_fatal("Arithmetic operation not implemented yet for an addition with more of 2 operands\n");
            exit(-1);
        }

        nDataSucc = dynamic_cast<NoeudData*> (this->getElementSucc(0));
        if(nDataSucc == NULL){
            trace_error("The successor node of a multiplication operation is not a data node.\n");
            exit(0);
        }

        nDataPred1 = dynamic_cast<NoeudData*> (this->getElementPred(0));
        if(nDataPred1 == NULL){
            trace_error("The first predecessor node of a multiplication operation is not a data node.\n");
            exit(0);
        }

        nDataPred2 = dynamic_cast<NoeudData*> (this->getElementPred(1));
        if(nDataPred2 == NULL){
            trace_error("The second predecessor node of a multiplication set operation is not a data node.\n");
            exit(0);
        }

        nDataPred1 = nDataPred1->genArithmeticOperation(matfile);
        nDataPred2 = nDataPred2->genArithmeticOperation(matfile);
        if(nDataPred1->getSrcSignal()){
            matfile << nDataSucc->getName().data() << " = " << nDataPred2->getName().data() << ";\n";
        }
        else if(nDataPred2->getSrcSignal()){
            matfile << nDataSucc->getName().data() << " = " << nDataPred1->getName().data() << ";\n";
        }
        else{
            matfile << nDataSucc->getName().data() << " = " << nDataPred1->getName().data() << " .* " << nDataPred2->getName().data() << ";\n";
        }

        return nDataSucc;
    }
    
    void MULNode::moveNoiseSource(NoeudSrcBruit* noiseNode){
        NoeudData *nodeDataPred, *nodeDataSuiv, *nodeConstante;
        int numInputOps;

        if(RuntimeParameters::isLTIGraph() && !RuntimeParameters::isRecursiveGraph()){
            assert(this->getNbSucc() == 1 && noiseNode->getNbPred() == 1);
            //Data(nodeDataPred)-->br(noiseNode)--->Ops(this)--->Data(nodeDataSuiv)
            nodeDataPred = dynamic_cast<NoeudData*> (noiseNode->getElementPred(0));
            nodeDataSuiv = dynamic_cast<NoeudData*> (this->getElementSucc(0));
            assert(nodeDataPred != NULL && nodeDataSuiv != NULL);
            assert(nodeDataPred->getTypeNodeGraph() != INIT && nodeDataSuiv->getTypeNodeGraph() != FIN);

            //test if nodeData has not more than one successor
            if (nodeDataSuiv->getNbSucc() == 1 && !nodeDataSuiv->isNodeOutput()) {

                // === Sauvegarde des numInput pour les futures modification sur les edges
                numInputOps = noiseNode->getEdgeSucc(0)->getNumInput();

                // Dans le cas d'un systeme LTI, l'aggrégation des Br associé à une multiplication par une constante est effectué
                // Pour cela, nous devons garder l'information de la multiplication dans la noeud représentant le bruit
                assert(this->getNbPred() == 2);
                if( this->getElementPred(0) == noiseNode){
                    nodeConstante = dynamic_cast<NoeudData*> (this->getElementPred(1));
                    if(!nodeConstante->isNodeConst()){
                        trace_error("Multiplication par variable %s alors que le systeme est LTI\n", nodeConstante->getName().c_str());
                        exit(0);
                    }
                }
                else if( this->getElementPred(1) == noiseNode){
                    nodeConstante = dynamic_cast<NoeudData*> (this->getElementPred(0));
                    if(!nodeConstante->isNodeConst()){
                        trace_error("Multiplication par variable %s alors que le systeme est LTI\n", nodeConstante->getName().c_str());
                        exit(0);
                    }
                }
                else{
                    trace_error("Aucun noeud predecesseur de l'opération est le noeud Br à aggréger\n");
                    exit(0);
                }
                noiseNode->multConstante(nodeConstante->getValeur());

                // === Redirection des arcs
                // Data(nodeDataPred)-->br(noiseNode)-->Ops(this)-->Data(nodeDataSuiv)
                nodeDataPred->deleteEdgeDirectedTo(noiseNode);
                // Data(nodeDataPred)  :  br(noiseNode)-->Ops(this)-->Data(nodeDataSuiv)
                noiseNode->deleteEdgeDirectedTo(this);
                // Data(nodeDataPred)  :  br(noiseNode)  :  Ops(this)-->Data(nodeDataSuiv)
                this->deleteEdgeDirectedTo(nodeDataSuiv);


                // Data(nodeDataPred)  :  br(noiseNode)  :  Ops(this)  :  Data(nodeDataSuiv)
                nodeDataPred->edgeDirectedTo(this, numInputOps);
                // Data(nodeDataPred)-->Ops(this)  :  br(noiseNode)  :  Data(nodeDataSuiv)
                noiseNode->edgeDirectedTo(nodeDataSuiv);
                // Data(nodeDataPred)-->Ops(this)  :  br(noiseNode)-->Data(nodeDataSuiv)
                this->edgeDirectedTo(noiseNode);
                // Data(nodeDataPred)-->Ops(this)-->br(noiseNode)-->Data(nodeDataSuiv)

                assert(noiseNode->getNbSucc() == 1);
                if (nodeDataSuiv->getTypeNodeData() == DATA_SRC_BRUIT) {
                    // The node just below us is a noise node, it is our only successor and it is in the same block as the other noise source we are handling
                    //we need to merge 2 noise sources: noiseNode and nodeGraphSuiv
                    //we add current noise source parameter to next noise source: nodeGraphSuiv

                    ((NoeudSrcBruit *) nodeDataSuiv)->calculSumParamNoise((NoeudSrcBruit *) (noiseNode)); //add parameter

                    //noeudPred-->OPS-->Br(noiseNode)-->Br(nodeGraphSuiv)-->OPS   before
                    this->deleteEdgeDirectedTo(noiseNode);
                    noiseNode->deleteEdgeDirectedTo(nodeDataSuiv);
                    this->edgeDirectedTo(nodeDataSuiv);
                    //noeudPred-->OPS-->Br(nodeGraphSuiv)-->OPS   after

                    //no recursion to continue move: this is end of process on noiseNode which is merged
                    //in nodeGraphSuiv noise source-->these noise source is processed after with the tabNoiseSourcesNode

                }
                else
                    ((NoeudGFD*)noiseNode->getElementSucc(0))->moveNoiseSource(noiseNode); // Appel recurcif
            }
        }
    }
            
    LinearFunction* MULNode::computeRuleLinearFunction(unsigned int size, LinearFunction* operand[]){
        LinearFunction *result;
        if (size != 2) {
            trace_fatal("Linear function rules not implemented yet for an multiplication with more of 2 operands\n");
            exit(-1);
        }
        //trace_info("op 0 : %s\nop 1 : %s\n", operand[0]->toString().c_str(), operand[1]->toString().c_str());
        result = *operand[0] * *operand[1];
        delete operand[0];
        delete operand[1];

        return result;
    }

    void MULNode::noiseModelInsertion(Gfd *gfd){
        int nbPred;

        NoeudOps *OpsMultBruit1 = NULL;
        NoeudOps *OpsMultBruit2 = NULL;
        NoeudData *NDataVar1 = NULL;
        NoeudData *NDataVar2 = NULL;
        NoeudOps *OpsAddBruit = NULL;
        NoeudGraph *NG = NULL;
        NoeudGFD *NodeGFD = NULL;

        char s[30];

        NoeudData *NodeData;

        NoeudData *NodeDataBruit[2] = { NULL, NULL };
        NoeudData *NodeDataSignal[2] = { NULL, NULL };

        int IndiceSignal;


        IndiceSignal = 0;

        // Le modele de bruit d'une multiplication(OPS_SIGNAL) est obtenue avec deux multiplication et une addition (OPS_BRUIT)
        //
        //On a a la base, une multiplication(OPS_SIGNAL) avec les entrees et sortie doublees(DATA_SIGNAL et DATA_BRUIT)
        OpsMultBruit1 = this->clone();
        OpsMultBruit1->setTypeSignalBruit(BRUIT);
        gfd->addOPNode(OpsMultBruit1);
        OpsMultBruit2 = this->clone();
        OpsMultBruit2->setTypeSignalBruit(BRUIT);
        gfd->addOPNode(OpsMultBruit2);

        int temp = gfd->getNbNode() + 1;
        sprintf(s, "VarInt(%d)", temp);

        NDataVar1 = gfd->addNoeudData(s, new TypeVar(VAR), BRUIT);
        NDataVar1->getTypeVar()->setTypeVarFonct(VAR);

        temp = gfd->getNbNode() + 1;
        sprintf(s, "VarInt(%d)", temp);
        NDataVar2 = gfd->addNoeudData(s, new TypeVar(VAR), BRUIT);
        NDataVar2->getTypeVar()->setTypeVarFonct(VAR);

        OpsAddBruit = new gfd::ADDNode(this->getBlock());
        gfd->addOPNode(OpsAddBruit);

        OpsMultBruit1->edgeDirectedTo(NDataVar1); // Ajouts d'arcs de connextion entre les OPS_BRUIT.
        OpsMultBruit2->edgeDirectedTo(NDataVar2); //

        // Specif arbitraire des numInput
        NDataVar1->edgeDirectedTo(OpsAddBruit, 0);
        NDataVar2->edgeDirectedTo(OpsAddBruit, 1);


        //for (i = nbSucc - 1; i >= 0; i--) // Redirection des arcs succ
        for(int i = 0 ; i<this->getNbSucc() ; i++ )
        {
            NG = this->getElementSucc(i);
            NodeGFD = dynamic_cast<NoeudGFD *> (NG);

            if (NodeGFD->getTypeNodeGFD() == DATA) {
                NodeData = dynamic_cast<NoeudData *> (NodeGFD);

                if (NodeData->getTypeSignalBruit() == BRUIT) {
                    this->deleteEdgeDirectedTo(NodeData); // On suprime les arcs (OPS_SIGNAL vers DATA_BRUIT)
                    OpsAddBruit->edgeDirectedTo(NodeData); // On ajoute les arcs (OPS_BRUIT vers DATA_BRUIT)
                }
            }
        }

        nbPred = this->getNbPred();
        for (int j = 0; j < nbPred; j++) { // On repertorie les pred de la multiplication(OPS_SIGNAL)
            NG = this->getElementPred(j);
            NodeGFD = dynamic_cast<NoeudGFD *> (NG);

            if (NodeGFD->getTypeNodeGFD() == DATA) {
                NodeData = dynamic_cast<NoeudData *> (NodeGFD);

                if (NodeData->getTypeSignalBruit() == SIGNAL) {
                    NodeDataSignal[IndiceSignal] = NodeData;
                    IndiceSignal++;
                }

            }
        }

        for (int j = 0; j < nbPred; j++) { // differenciation de la premiere et de la deuxieme entree

            NG = this->getElementPred(j);
            NodeGFD = dynamic_cast<NoeudGFD *> (NG);

            if (NodeGFD->getTypeNodeGFD() == DATA) {
                NodeData = dynamic_cast<NoeudData *> (NodeGFD);

                if (NodeData->getTypeSignalBruit() == BRUIT) {
                    if (NodeGFD == NodeDataSignal[0]->getPtrclone()) {
                        NodeDataBruit[0] = NodeData; //1ere entree
                    }
                    else {
                        if (NodeGFD == NodeDataSignal[1]->getPtrclone()) {
                            NodeDataBruit[1] = NodeData; //2eme entree
                        }
                        else {
                            trace_error("Error during Signal and Noise binding");
                            exit(EXIT_FAILURE);
                        }

                    }

                }

            }
        }
        this->EnleveedgeFromOf(NodeDataBruit[0]); // Redirection des arcs pred
        this->EnleveedgeFromOf(NodeDataBruit[1]); //

        // Specif des numInput par rapport au entrée
        NodeDataBruit[1]->edgeDirectedTo(OpsMultBruit1, 1); //
        NodeDataSignal[0]->edgeDirectedTo(OpsMultBruit1, 0); //

        NodeDataBruit[0]->edgeDirectedTo(OpsMultBruit2, 0); //
        NodeDataSignal[1]->edgeDirectedTo(OpsMultBruit2, 1); //
    }

    /**
     *  Return a TypeLti in terms of the operation node
     *  	\return : TypeLti
     *
     *  MULT => CONST * CONST -> CONST
     *  		SIGNAL * CONST -> SIGNAL
     *  		other -> NOT_LTI
     *
     *  Working only to operation node with 1 or 2 input
     *  \author Nicolas Simon
     *  \date 09/07/2010
     */
    TypeLti MULNode::isLTI(){
        if(this->getNbInput() != 2){
            trace_fatal("LTI detection not implemented yet for an multiplication with more of 2 operands\n");
            exit(-1);
        }

        NoeudData *NData1;
        NoeudData *NData2;
        TypeLti TypeOp1;
        TypeLti TypeOp2;
        TypeLti Res;

        NData1 = dynamic_cast<NoeudData *> (this->getElementPred(0));
        assert(NData1 != NULL);
        TypeOp1 = NData1->isLTI();
        NData2 = dynamic_cast<NoeudData *> (this->getElementPred(1));
        assert(NData2 != NULL);
        TypeOp2 = NData2->isLTI();

        if (TypeOp1 == NOT_LTI || TypeOp2 == NOT_LTI) {
            Res = NOT_LTI;
        }
        else if (TypeOp1 == SIGNAL_LTI && TypeOp2 == SIGNAL_LTI) {
            Res = NOT_LTI;
        }
        else if (TypeOp1 == SIGNAL_LTI || TypeOp2 == SIGNAL_LTI) {
            Res = SIGNAL_LTI;
        }
        else {
            Res = CONST_LTI;
        }

        return Res;
    }

/**
 * Methode qui recherche le point commun entre un circuit et un graphe en parcourant celui-ci
 *
 *  \param  GraphPath   * liste contenant les numeros des noeuds du circuit
 *  \param  NoeudData *		output du graphe en cours
 *  \param  bool			true si parcours du demi graphe uniquement
 *  \param vector<NoeudData*>*	liste des noeud a demanteler
 *
 *  \author Loic Cloatre
 *  \date 16/12/2008
 */
	void MULNode::rechercheDernierPointCommun(GraphPath * circuit, NoeudData * raciceGraphe, vector<NoeudData *>*listNoeudAdemanteler, bool demiGrapheParcouru) {
		//local declaration

		NoeudData *tempNoeudData;


		tempNoeudData = (NoeudData *) (this->getElementPred(0)); //premiere branche

		if (tempNoeudData->getEtat() == NONVISITE) //cas d'une boucle avec z-1
		{
			tempNoeudData->rechercheDernierPointCommun(circuit, raciceGraphe, listNoeudAdemanteler, demiGrapheParcouru); //calcul de l'operande de gauche
		}

		if (this->getNbPred() > 1) {
			tempNoeudData = (NoeudData *) (this->getElementPred(1)); //deuxieme branche

			if (tempNoeudData->getEtat() == NONVISITE) {
				tempNoeudData->rechercheDernierPointCommun(circuit, raciceGraphe, listNoeudAdemanteler, demiGrapheParcouru); //calcul de l'operande de gauche
				demiGrapheParcouru = true; //mis a true pour signaler que la branche this->getElementPred(1) a été parcourue
			}
		}
		else {
			demiGrapheParcouru = true; //mis a true pour signaler que la branche this->getElementPred(1) a été parcourue
		}
	}

}
