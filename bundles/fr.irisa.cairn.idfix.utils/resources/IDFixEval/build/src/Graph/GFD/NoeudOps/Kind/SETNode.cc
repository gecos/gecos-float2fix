#include "graph/dfg/operatornode/kind/SETNode.hh"
#include "graph/dfg/datanode/NoeudData.hh"

#include "backend/NoisePowerExpressionGeneration.hh"

namespace gfd {
    SETNode::SETNode(Block &block):NoeudOps(-1, 1, block, "SET"){}
    SETNode::SETNode(const SETNode &other):NoeudOps(other){}

    SETNode* SETNode::clone(){
        return new SETNode(*this);
    }
    
    std::string SETNode::WFPEffDetermination(){
        std::ostringstream oss;
        std::string str;

        oss << this->getNumSFG();
        // WFPeff[xOps][2] = min(WFP[xOps][2], WFPeff[xOps][0]);
        str = "   " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+2]"; // Si 2 est la sortie
        str += " = min(" + __NOISEPOWER_WFPSFG__ + "[" + oss.str() + "*3+2],";
        str += " " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+0]);";

        return str;
    }

    std::string SETNode::KDetermination(){
        std::ostringstream oss;
        std::string str;
    
        oss << this->getNumSFG();
        // WFP_sfg_eff[xOps][0] - WFP_sfg[xOps][2];
        str = __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+0]"; // Si 2 est la sortie
        str += " - " + __NOISEPOWER_WFPSFGEff__ + "[" + oss.str() + "*3+2]";
        return str;
    }

    float SETNode::resolveConstantSubTree(){
        NoeudData *nData;
        
        if(this->getNbPred() != 1){
            trace_error("A delay operator have not only one predecessor.\n");
            exit(-1);
        }
        else{
            nData = (NoeudData*) this->getElementPred(0);
            return nData->resolveConstantSubTree();
        }
    }

    DataDynamic SETNode::dynamicRule(unsigned int size, DataDynamic operand[]){
        DataDynamic dynOut;

        if(size != 1){
            trace_error("A set operator cannot have more one operand\n");
            exit(0);
        }

        dynOut.valMin = operand[0].valMin;
        dynOut.valMax = operand[0].valMax;

        return dynOut;
    }
    
    NoeudData* SETNode::arithmeticOperationGeneration(fstream & matfile){
        NoeudData *nDataSucc, *nDataPred;
        nDataSucc = dynamic_cast<NoeudData*> (this->getElementSucc(0));
        if(nDataSucc == NULL){
            trace_error("The successor node of a set operation is not a data node.\n");
            exit(0);
        }

        nDataPred = dynamic_cast<NoeudData*> (this->getElementPred(0));
        if(nDataPred == NULL){
            trace_error("The predecessor node of a set operation is not a data node.\n");
            exit(0);
        }

        nDataPred = nDataPred->genArithmeticOperation(matfile);
        matfile << nDataSucc->getName().data() << " = " << nDataPred->getName().data() << ";\n";
        return nDataSucc;
    }
    
    void SETNode::moveNoiseSource(NoeudSrcBruit* noiseNode){
        NoeudData *nodeDataPred, *nodeDataSuiv;
        int numInputOps;

        assert(this->getNbSucc() == 1 && noiseNode->getNbPred() == 1);
        //Data(nodeDataPred)-->br(noiseNode)--->Ops(this)--->Data(nodeDataSuiv)
        nodeDataPred = dynamic_cast<NoeudData*> (noiseNode->getElementPred(0));
        nodeDataSuiv = dynamic_cast<NoeudData*> (this->getElementSucc(0));
        assert(nodeDataPred != NULL && nodeDataSuiv != NULL);
        assert(nodeDataPred->getTypeNodeGraph() != INIT && nodeDataSuiv->getTypeNodeGraph() != FIN);

        //test if nodeData has not more than one successor
        if (nodeDataSuiv->getNbSucc() == 1 && !nodeDataSuiv->isNodeOutput()) {

            // === Sauvegarde des numInput pour les futures modification sur les edges
            numInputOps = noiseNode->getEdgeSucc(0)->getNumInput();

            // === Redirection des arcs
            // Data(nodeDataPred)-->br(noiseNode)-->Ops(this)-->Data(nodeDataSuiv)
            nodeDataPred->deleteEdgeDirectedTo(noiseNode);
            // Data(nodeDataPred)  :  br(noiseNode)-->Ops(this)-->Data(nodeDataSuiv)
            noiseNode->deleteEdgeDirectedTo(this);
            // Data(nodeDataPred)  :  br(noiseNode)  :  Ops(this)-->Data(nodeDataSuiv)
            this->deleteEdgeDirectedTo(nodeDataSuiv);


            // Data(nodeDataPred)  :  br(noiseNode)  :  Ops(this)  :  Data(nodeDataSuiv)
            nodeDataPred->edgeDirectedTo(this, numInputOps);
            // Data(nodeDataPred)-->Ops(this)  :  br(noiseNode)  :  Data(nodeDataSuiv)
            noiseNode->edgeDirectedTo(nodeDataSuiv);
            // Data(nodeDataPred)-->Ops(this)  :  br(noiseNode)-->Data(nodeDataSuiv)
            this->edgeDirectedTo(noiseNode);
            // Data(nodeDataPred)-->Ops(this)-->br(noiseNode)-->Data(nodeDataSuiv)

            assert(noiseNode->getNbSucc() == 1);
            if (nodeDataSuiv->getTypeNodeData() == DATA_SRC_BRUIT) {
                // The node just below us is a noise node, it is our only successor and it is in the same block as the other noise source we are handling
                //we need to merge 2 noise sources: noiseNode and nodeGraphSuiv
                //we add current noise source parameter to next noise source: nodeGraphSuiv

                ((NoeudSrcBruit *) nodeDataSuiv)->calculSumParamNoise((NoeudSrcBruit *) (noiseNode)); //add parameter

                //noeudPred-->OPS-->Br(noiseNode)-->Br(nodeGraphSuiv)-->OPS   before
                this->deleteEdgeDirectedTo(noiseNode);
                noiseNode->deleteEdgeDirectedTo(nodeDataSuiv);
                this->edgeDirectedTo(nodeDataSuiv);
                //noeudPred-->OPS-->Br(nodeGraphSuiv)-->OPS   after

                //no recursion to continue move: this is end of process on noiseNode which is merged
                //in nodeGraphSuiv noise source-->these noise source is processed after with the tabNoiseSourcesNode

            }
            else
                ((NoeudGFD*)noiseNode->getElementSucc(0))->moveNoiseSource(noiseNode); // Appel recurcif
        }
    }
            
    LinearFunction* SETNode::computeRuleLinearFunction(unsigned int size, LinearFunction* operand[]){
        if (size != 1) {
            trace_fatal("Linear function rules not implemented yet for an affectation with more of 1 operands\n");
            exit(-1);
        }
            
        return operand[0];
    }

    void SETNode::noiseModelInsertion(Gfd *gfd){
        int i;
        int j;
        int numInput;
        NoeudGraph *NG = NULL;
        NoeudGFD *NGFD = NULL;
        NoeudData *ND = NULL;
        NoeudOps *NO1 = NULL;
        TypeSignalBruit typeSignalBruit;
        TypeNodeGFD TypeNGFD;

        // Le modele de bruit d'un retard(OPS_SIGNAL) est obtenue avec un retard (OPS_BRUIT)
        //
        //On a � la base, un retard (OPS_SIGNAL) avec les entr�es et sortie doubl�es(DATA_SIGNAL et DATA_BRUIT)
        NO1 = this->clone();
        NO1->setTypeSignalBruit(BRUIT);
        gfd->addOPNode(NO1);


        for (i = 0; i < this->getNbSucc(); i++) // Redirection des arcs succ
        {
            NG = this->getElementSucc(i);
            NGFD = (NoeudGFD *) NG;
            TypeNGFD = NGFD->getTypeNodeGFD();
            if (TypeNGFD == DATA) {
                ND = (NoeudData *) NGFD;
                typeSignalBruit = ND->getTypeSignalBruit();
                if (typeSignalBruit == BRUIT) {
                    this->deleteEdgeDirectedTo(ND); // On suprime les arcs (OPS_SIGNAL vers DATA_BRUIT)
                    NO1->edgeDirectedTo(ND); // On ajoute les arcs (OPS_BRUIT vers DATA_BRUIT)
                    i--;
                }
            }
        }
        for (j = 0; j < this->getNbPred(); j++) // Redirection des arcs pred
        {
            NG = this->getElementPred(j);
            NGFD = (NoeudGFD *) NG;
            TypeNGFD = NGFD->getTypeNodeGFD();
            if (TypeNGFD == DATA) {
                ND = (NoeudData *) NGFD;
                typeSignalBruit = ND->getTypeSignalBruit();
                if (typeSignalBruit == BRUIT) {
                    // Sauvegarde du numInput avant modification des edges
                    numInput = -1;
                    vector<Edge *>::iterator itPredOps = this->getListePred()->begin();
                    while (itPredOps != this->getListePred()->end()) {
                        if ((*itPredOps)->getNodePred() == ND) {
                            numInput = (*itPredOps)->getNumInput();
                            break;
                        }
                        itPredOps++;
                    }
                    assert(numInput != -1); // numInput doit être forcement affecté au dessus
                    this->EnleveedgeFromOf(ND); // On suprime les arcs (DATA_BRUIT vers OPS_SIGNAL)
                    NO1->edgeFromOf(ND, numInput-this->getNbInput()); // On ajoute les arcs (DATA_BRUIT vers OPS_BRUIT)
                }
            }
        }
    }

    /**
     *  Return a TypeLti in terms of the operation node
     *  	\return : TypeLti
     *
     *  \author Nicolas Simon
     *  \date 09/07/2010
     */
    TypeLti SETNode::isLTI(){
        NoeudData *NData1;

        NData1 = dynamic_cast<NoeudData *> (this->getElementPred(0));
        assert(NData1 != NULL);
        return NData1->isLTI();
    }

/**
 * Methode qui recherche le point commun entre un circuit et un graphe en parcourant celui-ci
 *
 *  \param  GraphPath   * liste contenant les numeros des noeuds du circuit
 *  \param  NoeudData *		output du graphe en cours
 *  \param  bool			true si parcours du demi graphe uniquement
 *  \param vector<NoeudData*>*	liste des noeud a demanteler
 *
 *  \author Loic Cloatre
 *  \date 16/12/2008
 */
	void SETNode::rechercheDernierPointCommun(GraphPath * circuit, NoeudData * raciceGraphe, vector<NoeudData *>*listNoeudAdemanteler, bool demiGrapheParcouru) {
		//local declaration

		NoeudData *tempNoeudData;


		tempNoeudData = (NoeudData *) (this->getElementPred(0)); //premiere branche

		if (tempNoeudData->getEtat() == NONVISITE) //cas d'une boucle avec z-1
		{
			tempNoeudData->rechercheDernierPointCommun(circuit, raciceGraphe, listNoeudAdemanteler, demiGrapheParcouru); //calcul de l'operande de gauche
		}

		if (this->getNbPred() > 1) {
			tempNoeudData = (NoeudData *) (this->getElementPred(1)); //deuxieme branche

			if (tempNoeudData->getEtat() == NONVISITE) {
				tempNoeudData->rechercheDernierPointCommun(circuit, raciceGraphe, listNoeudAdemanteler, demiGrapheParcouru); //calcul de l'operande de gauche
				demiGrapheParcouru = true; //mis a true pour signaler que la branche this->getElementPred(1) a été parcourue
			}
		}
		else {
			demiGrapheParcouru = true; //mis a true pour signaler que la branche this->getElementPred(1) a été parcourue
		}
	}

}
