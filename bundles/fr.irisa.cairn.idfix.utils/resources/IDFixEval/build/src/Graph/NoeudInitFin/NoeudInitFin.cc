/**
 *  \file NoeudInitFin.cc
 *  \author Daniel Menard, Jean-Charles Naud
 *  \date   31.01.2002S
 *  \brief Classe definissant le noeud DEBUT d'un GFD et FIN
 */

// === Bibliothèques .hh associe
#include "graph/dfg/EdgeGFD.hh"
#include "graph/dfg/NoeudInitFin.hh"


// ----------------------------------------------- NoeudInit ----------------------------------------------

// ======================================= Constructeur - Destructeur

/**
 * Constructeurs de la classe NoeudInit
 */
NoeudInit::NoeudInit(int NumNoeud) :
	NoeudGraph(INIT, NumNoeud, "NODE_INIT") {
}

NoeudInit::~NoeudInit() {

}

// ======================================= Methodes
void NoeudInit::addEdge(NoeudGraph * NGraph) {
	EdgeGFD *Elt;

	if(!this->isSucc(NGraph)){
		Elt = new EdgeGFD(this, NGraph, 0);
		this->setListeSucc(Elt , this->getNbSucc());       // Ajout du successeur NGraph à la liste de this
		nbSucc++;
		NGraph->setListePred(Elt , 0);   // Ajout du predecesseur this à la liste de NGraph
		NGraph->setNbPred(NGraph->getNbPred() + 1);
	}
}

// ----------------------------------------------- NoeudFin ----------------------------------------------


// ======================================= Constructeur - Destructeur

/**
 * Constructeurs de la classe NoeudFin
 */
NoeudFin::NoeudFin(int NumNoeud) :
	NoeudGraph(FIN, NumNoeud, "NODE_FIN") {
	}

NoeudFin::~NoeudFin() {

}

// ======================================= Methodes
void NoeudFin::addEdge(NoeudGraph * NGraph) {
	EdgeGFD *Elt;
	if(!this->isPred(NGraph)){
		Elt = new EdgeGFD(NGraph, this, 0);
		NGraph->setListeSucc(Elt , 0);
		NGraph->setNbSucc(NGraph->getNbSucc() + 1);
		this->setListePred(Elt , this->getNbPred());   // Ajout du predecesseur this à la liste de NGraph
		nbPred++;
	}
}

