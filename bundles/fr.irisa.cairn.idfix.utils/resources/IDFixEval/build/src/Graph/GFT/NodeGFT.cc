/**
 * \file NodeGFT.cc
 * \author Adrien Destugues
 * \date    19.11.2010
 * \brief Class for NodeGFT
 *
 */

// === Bibliothèques .hh associées
#include "graph/dfg/NoeudInitFin.hh"
#include "graph/tfg/EdgeGFT.hh"
#include "graph/tfg/NodeGFT.hh"
#include "utils/graph/cycledismantling/GraphPath.hh"

// === Bibliothèques non standard


NoeudGFT::NoeudGFT(const NoeudGFL & n) :
	NoeudGraph(GFT, n.getNumero(), n.getName()) {

	switch (n.getTypeNoeudGFL()) {
	case PHI_GFL:
		typeNoeudGFT = PHI_GFT;
		break;
	default:
	case VAR_GFL:
		typeNoeudGFT = VAR_GFT;
		break;
	}

	refParamSrc.push_back(n.getRefParamSrc(0));
}

NoeudGFT::NoeudGFT(const NoeudGFT & other) :
	NoeudGraph(other) {
	refParamSrc = other.refParamSrc;
	typeNoeudGFT = other.typeNoeudGFT;
}

NoeudGraph *NoeudGFT::clone() {
	return new NoeudGFT(*this);
}

void NoeudGFT::addEdge(NoeudGFT * NGraph, TransferFunction* Mfct) {
	EdgeGFT *e = new EdgeGFT(this, NGraph, Mfct);

	listSucc.push_back(e); // Ajout du successeur NGraph à la liste de this
	nbSucc++;

	NGraph->getListePred()->push_back(e); // Ajout du predecesseur this à la liste de NGraph
	NGraph->setNbPred(NGraph->getNbPred() + 1);
}

void NoeudGFT::Swap(NoeudGFT * other) {
	// On trouve d'abord l'edge qui va être swappé (other>this)
	ListSuccPred::iterator it = other->getListeSucc()->begin();
	while (it != other->getListeSucc()->end()) {
		if ((*it)->getNodeSucc() == this)
			break; // On a trouvé
		it++;
	}

	if (it == other->getListeSucc()->end()) {
		trace_error("Other et this ne sont pas connectés!");
		assert(false);
	}

	EdgeGFT *edgeToSwap = dynamic_cast<EdgeGFT *> (*it);

	assert(edgeToSwap != NULL);

	// On parcours nos listSucc
	it = getListeSucc()->begin();
	while (it != getListeSucc()->end()) {
		EdgeGFT *newEdge = dynamic_cast<EdgeGFT *> (edgeToSwap->clone());
		assert(newEdge != NULL);
		newEdge->Mult(*dynamic_cast<EdgeGFT *> (*it)->getFT());

		//newEdge->AddBlocks(edgeToSwap->GetBlocks());

		(*it)->getNodeSucc()->addPredEdge(newEdge);
		other->addSuccEdge(newEdge);
		it++;
	}

	other->deleteEdgeDirectedTo(this);
}

/**
 *  Indique si le NoeudGFL est une sortie
 *		Return bool : Resultat du test
 */
bool NoeudGFT::isNodeOutput() {
	return dynamic_cast<NoeudFin *> (this->getElementSucc(0)) != NULL;
}


NoeudGFD *NoeudGFT::getRefParamSrc(unsigned int Numero) const {
	if (Numero < refParamSrc.size()) {
		list<NoeudGFD *>::const_iterator it = refParamSrc.begin();
		for (unsigned int i = 0; i < Numero; i++) {
			it++;
		}
		return (*it);
	}
	else {
		return NULL;
	}
}

/*NoeudData *NoeudGFT::getRefParamSrc() {

	if (!refParamSrc.empty()) {
		return refParamSrc.front();
	}
	else
		return NULL;
}*/

/**
 *  Methode qui parcourt tous les chemins à partir d'une sortie et renvoie la liste des chemins possible vers une source
 *
 * 		\return : CheminGraph liste des chemins possibles
 *
 *  by Loic Cloatre creation le 09/02/2010
 */
GraphPath *NoeudGFT::enumerationCheminVersSource(NoeudGFT * sourcebruit, EdgeGFT* origine) {

	GraphPath *retChemin = NULL;
	GraphPath *cheminTrouve = NULL;
	GraphPath *retCheminQueue = NULL;
	GraphPath *retCheminParcours = NULL;

	if (this->isNodeSource()) { //si on est au sommet du graphe
		if (this->Numero == sourcebruit->getNumero()) { //si c'est la source de bruit recherchee
			//source de bruit source atteinte
			//on cree un GraphPath et on l'ajoute à la liste
			retChemin = new GraphPath();
			retChemin->addElemPath(this, origine);
		}
	}
	else {
		//sinon on parcourt les listPred
		for (int i = 0; i < this->nbPred; i++) {
			if (this->Numero != this->getElementPred(i)->getNumero()) { //pour eviter les boucles
				cheminTrouve = ((NoeudGFT *) getElementPred(i))->enumerationCheminVersSource(sourcebruit, dynamic_cast<EdgeGFT*> (getEdgePred(i)));

				//if(Gfd::getModeDebug())cout<<"noeud en cours apres enum "<<this->Numero<<endl;
				if (cheminTrouve != NULL) {
					//on ajoute le numero courant à tous les circuits precedents
					if (retChemin == NULL) {
						retChemin = cheminTrouve;
					}
					else {
						//cas ou on a deja trouve un chemin, on ajoute celui qu'on vient de trouver
						retCheminQueue = retChemin;
						while (retCheminQueue->Suiv != NULL) {
							retCheminQueue = retCheminQueue->Suiv;
						}
						//queue trouvee: on ajoute le chemintrouve a la suite
						retCheminQueue->Suiv = cheminTrouve;
					}
				}
			}
		}
		//tous les chemins partant de ce noeud vers la source de bruit superieure sont enumeres, on ajoute le numero courant à tous les chemins
		if (retChemin != NULL) {
			retCheminParcours = retChemin;

			retCheminParcours->addElemPath(this, origine); //on ajoute le premier maillon

			while (retCheminParcours->Suiv != NULL) { //on ajoute chaque suivant si necessaire
				retCheminParcours->Suiv->addElemPath(this, origine);
				retCheminParcours = retCheminParcours->Suiv;
			}
		}
	}

	return retChemin;
}

bool NoeudGFT::isNodeSource() {
	int NblistPred = (this)->getNbPred();
	return (NblistPred == 0) || (NblistPred == 1 && getElementPred(0)->getTypeNodeGraph() == INIT);
}
