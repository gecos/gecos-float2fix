/**
 *  \file EdgeGFT.cc
 *  \author Adrien Destugues
 *  \date   12.2010
 *  \brief Class for edges in the Gft (transfert function graph)
 *	The edges of the Gft have only one attribute, which is the transfert functions between
 * two nodes. This transfert function may be a regular linear one, a pseudo-FT or something else.
 */

// === Bibliothèques .hh associées
#include "graph/tfg/EdgeGFT.hh"
#include "graph/tfg/GFT.hh"
#include "graph/tfg/NodeGFT.hh"

// ======================================= Constructeurs - Destructeurs
EdgeGFT::EdgeGFT(NoeudGFT * s, NoeudGFT * d, TransferFunction* f) :
	Edge(s, d, -1) {
	m_FT = f;
}

EdgeGFT::~EdgeGFT(){
	delete m_FT;
}

// =======================================  Methodes
string EdgeGFT::getLabel() const {
	std::string label;
	label = m_FT->toString();

	// Add the block list
	/*label += "\n{";
	ostringstream o;
	for (set<Block*>::iterator it = m_Blocks.begin(); it != m_Blocks.end(); it++)
		o << (*it)->getID().first << "." << (*it)->getID().second << ", ";

	label += o.str() + "}";*/
	return label;
}

string EdgeGFT::getMatlabExpression() const {
	/*ostringstream probablocs;
	probablocs << "H(" << numEdge << ").Blocks = [";
	set<Block*>::const_iterator it;
	for (it = m_Blocks.begin(); it != m_Blocks.end(); it++) {
		probablocs << (*it)->getID().first << " " << (*it)->getID().second << "; ";
	}
	probablocs << "];\n";*/
	//return probablocs.str() + m_FT->getMatlabExpression(numEdge);
	return m_FT->getMatlabExpression(numEdge);
}
