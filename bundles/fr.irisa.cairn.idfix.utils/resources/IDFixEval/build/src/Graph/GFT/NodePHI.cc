/**
 * \file NodePHI.cc
 * \author Adrien Destugues
 * \date    19.11.2010
 *  \brief Classe pour représenter un noeud phi dans un GFT
 */

// === Bibliothèques .hh associées
#include "graph/tfg/EdgeGFT.hh"
#include "graph/tfg/NodePHI.hh"

// ======================================= Methodes
/**
 * Clone la classe NoeudPHI
 * \return le classe clonée
 */
NoeudGraph *NoeudPHI::clone() {
	return new NoeudPHI(*this);
}
/**
 * Clone la classe NoeudPHI
 * \param NoeudPHI à merger
 */
void NoeudPHI::Merge(NoeudPHI * other) {
	ListSuccPred::iterator it = other->getListePred()->begin();

	// Récupration de la FT en sortie du NoeudPHI other
	EdgeGFT* edgeInter = dynamic_cast<EdgeGFT*> (other->getEdgeSucc(0));
	//TransfertFunction f = edgeInter->getFT();
	TransferFunction* f = edgeInter->getFT();

	while (it != other->getListePred()->end()) {
		EdgeGFT* edgeCourant = dynamic_cast<EdgeGFT*> (*it);
		edgeCourant->Mult(*f);
//		edgeCourant->AddBlocks(edgeInter->GetBlocks()); // Union des enssembles des blocs traversés

		this->addPredEdge(*it);
		it++;
	}

	other->removeEdge(this);
}
