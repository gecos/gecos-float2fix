/**
 * \file GFT.cc
 * \author Adrien Destugues
 * \date    19.11.2010
 * \brief Class for graph of Transfer Functions
 * This graph is similar to the GFL, but instad of allowing only linear non-recursive functions on the edges, it allows all kinds of transfert functions and
 * even pseudo-transfert functions for some non-linear cases. These cases include the phi operators and the ones obtained by simulation only.
 * The GFT makes it possible to make all these kinds of transfert functions working together.
 *
 */

// === Bibliothèques .hh associées
#include "graph/dfg/Gfd.hh"
#include "graph/lfg/EdgeGFL.hh"
#include "graph/lfg/NoeudGFL.hh"

// === Bibliothèques non standard
#include "graph/tfg/EdgeGFT.hh"
#include "graph/tfg/GFT.hh"
#include "graph/tfg/NodeGFT.hh"
#include "graph/tfg/NodePHI.hh"

/**
 Builds a GFT from an existing GFL. The denominator loops in the GFL are converted in denominators for transfert functions.
 */
Gft::Gft(const Gfl & linearGraph) :
	Graph(linearGraph.getNoiseEval()) {
	// Copy all nodes from the GFL
	// INIT and END nodes are created by Graph() constructor
	for (int i = 2; i < linearGraph.getNbNode(); i++)
		addNoeudGraph(linearGraph.getElementGf(i));

	// Copy all edges from the GFL, but convert the LinearFoncs to TransferFuncs
	// We do this in another loop because we want to be sure that the destination node exists when we create an edge.
	for (int i = 0; i < linearGraph.getNbNode(); i++) {
		for (int j = 0; j < linearGraph.getElementGf(i)->getNbSucc(); j++) {
			addEdge(linearGraph.getElementGf(i)->getEdgeSucc(j));
		}
	}
}

/**
 Adds a node to a GFT. You can either pass a GFT node which will be added directly, or a GFL node, that will be converted.
 */
void Gft::addNoeudGraph(NoeudGraph * n) {
	if (n == NULL)
		return;

	if (n->getTypeNodeGraph() == GFL) {
		NoeudGFT *nt;

		// Also add the node to the input/output/intermediates
		switch (((NoeudGFL *) n)->getTypeNoeudGFL()) {
		case VAR_ENTREE_GFL:
			nt = new NoeudGFT(*dynamic_cast<NoeudGFL *> (n));
			Graph::addNoeudGraph(nt);
			addInputGf(nt);
			break;
		case VAR_SORTIE_GFL:
			nt = new NoeudGFT(*dynamic_cast<NoeudGFL *> (n));
			Graph::addNoeudGraph(nt);
			addOutputGf(nt);
			break;
		case VAR_GFL:
			nt = new NoeudGFT(*dynamic_cast<NoeudGFL *> (n));
			Graph::addNoeudGraph(nt);
			addVarIntGf(nt);
			break;
		case PHI_GFL:
			nt = new NoeudPHI(*dynamic_cast<NoeudGFL *> (n));
			Graph::addNoeudGraph(nt);
			// Pas d'ajout particulier à faire, la liste globale suffit
			break;
		default:
			trace_warn("type de noeud inconnu dans le GFL...")
			;
			break;
		}
	}
	else
		Graph::addNoeudGraph(n);
}

/**
 Adds an edge to a GFT. You can either pass a GFT edge (or Graph edge for init and end nodes),
 or a GFL edge that will be converted.

 This function does not take ownership of the edge you pass it, so delete it yourself.
 */
void Gft::addEdge(Edge * e) {
	NoeudGraph* nPred = e->getNodePred();
	NoeudGraph* nSucc = e->getNodeSucc();
	
	EdgeGFL *el = dynamic_cast<EdgeGFL *> (e);
//	TransfertFunction* tempTF;
	TransferFunction* ft;
	
	if (el == NULL || nPred->getTypeNodeGraph() == INIT || nSucc->getTypeNodeGraph() == FIN ) {
		getElementGf(e->getNodePred()->getNumero())-> edgeDirectedTo(getElementGf(e->getNodeSucc()->getNumero()));
	}
	else if (el->getNodePred() != el->getNodeSucc()) {
		// Convert the GFL edge to a GFT one
		//tempTF = static_cast<NoeudGFL*> (el->getNodePred())->calculTypeVarFonctionTransfertAvecUnSuccesseur(el->getNodeSucc()->getNumero());
		ft = static_cast<NoeudGFL*> (nPred)->calculFonctionTransfertAvecSuccesseur(el);
		static_cast<NoeudGFT*> (getElementGf(nPred->getNumero()))->addEdge(static_cast<NoeudGFT*> (getElementGf(nSucc->getNumero())), ft);//*static_cast<NoeudGFL*> (el->getNodePred())->calculTypeVarFonctionTransfertAvecUnSuccesseur(el->getNodeSucc()->getNumero()));
	}
}

/**
 Performs phi-propagation through the graph.
 All the phi operators needs to be moved down the graph to be just above the output node.
 When a phi node is moved down, the transfert functions above and below need to be combined.
 */
void Gft::moveAllPhisDown() {
	// D'abord on se fabrique une liste de phi, à partir de la liste de tous les noeuds du graphe
	std::list<NoeudPHI *> listePhi;
	NodeGraphContainer::iterator it = tabNode->begin();

	while (it != tabNode->end()) {
		if ((*it)->getTypeNodeGraph() == GFT) {
			NoeudGFT *g = dynamic_cast<NoeudGFT *> (*it);
			if (g && g->getTypeNodeGraphGFT() == PHI_GFT) {
				NoeudPHI *h = dynamic_cast<NoeudPHI *> (*it);
				if (h)
					listePhi.push_back(h);
				else{
					trace_error("Fake PHI node !");
					exit(-1);
				}
					
			}
		}

		it++;
	}

	trace_debug("Found %i phi nodes\n", (int) listePhi.size());

	// listePhi est remplie avec des pointeurs vers tous les noeuds phi
	// maintenant on va prendre chaque noeud et le descendre le plus bas possible.
	// Plusieurs cas peuvent se produire :
	// * Phi avec un seul suc : on les échange
	// * Phi avec deux suc : on dédouble le phi et on le met après chacun des deux suc (l'un des deux dédoublés est rangé dans la liste et on continue à descendre
	//      l'autre)
	// * Phi avec un suc qui est lui-même un autre phi : on les fusionne, le résultat est un phi avec 3 entrées (attention au calcul des probas dans ce cas)
	// * Phi avec un suc qui est la sortie : on a fini, on enlève ce phi de notre liste
	std::list<NoeudPHI *>::iterator current = listePhi.begin();
	while (!listePhi.empty()) {
		//DEBUG("CURRENT PHI NODE\n");
		//(*current)->print();
		// On ne regarde que le 1er suc du noeud phi en cours de traitement. S'il y a d'autres suc on crée un nouveau phi pour eux, et on verra plus tard
		if ((*current)->getNbSucc() > 1) {
			//DEBUG("Splitting node\n");
			// Découpage du noeud courant en 2:
			// D'abord on le clone
			NoeudPHI *clone = static_cast<NoeudPHI*> ((*current)->clone());
			addNoeudGraph(clone);

			// Le clone est le "même" noeud que l'autre, mais il est à une adresse différente en mémoire.
			// Donc on doit rediriger tous les edges (suc et pred)
			ListSuccPred::iterator it = clone->getListeSucc()->begin();
			while (it != clone->getListeSucc()->end()) {
				(*it)->setNodePred(clone);
				it++;
			}

			it = clone->getListePred()->begin();
			while (it != clone->getListePred()->end()) {
				(*it)->setNodeSucc(clone);
				it++;
			}

			// On enlève tous les suc de l'original sauf le premier
			for (int i = 1; i < (*current)->getNbSucc(); i++)
				(*current)->deleteEdgeDirectedTo((*current)->getElementSucc(i));

			// Ensuite on enlève le 1er suc du clone
			clone->deleteEdgeDirectedTo(clone->getElementSucc(0));

			// On ajoute le clone à notre liste pour s'en occuper plus tard
			listePhi.push_back(clone);
		}

		// Maintenant on est surs que le phi courant n'a qu'un suc.
		NoeudGFT *succ = dynamic_cast<NoeudGFT *> ((*current)->getElementSucc(0));

		// Cas 1 : un seul suc qui est la sortie (fin de traitement de ce phi)
		if (succ->isNodeOutput()) {
			trace_debug("Output Node\n");
			listePhi.erase(current++);
		}
		// Cas 2 : un seul suc qui est un phi (fusion, donc fin de traitement de ce phi et on continuera quand on traitera l'autre si c'est pas déjà fait)
		else if (succ->getTypeNodeGraphGFT() == PHI_GFT) {
			trace_debug("Merging node\n");
			static_cast<NoeudPHI*> (succ)->Merge(*current);
			deleteGraphNode(*current);
			listePhi.erase(current++);
		}
		// Cas 3 : un seul suc, autre cas. Pas de fusion mais un échange de noeuds et on descend
		else {
			//DEBUG("Swapping Node\n");
			succ->Swap(*current);
			//(*current)->print();
		}
	}
}
