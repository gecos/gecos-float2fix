/*!
 *  \author Daniel Menard, Jean-Charles Naud
 *  \date   31.01.2002
 *  \version 1.0
 */

// === Bibliothèques .hh associe
#include "graph/dfg/datanode/NoeudData.hh"
#include "graph/dfg/EdgeGFD.hh"
#include "graph/NoeudGraph.hh"
#include "graph/dfg/operatornode/NoeudOps.hh"

#include "graph/lfg/EdgeGFL.hh"

// === Namespaces
using namespace std;

/**
 * Constructeur de la classe NoeudGraph
 * 	\param TypeNodeGraph : Type de Noeud
 *	\param Numero :Numero du noeud a creer
 *	\param b : ID du block auquel appartient le NoeudGraph 
 */
NoeudGraph::NoeudGraph(TypeNodeGraph typeNodeGraph, int Numero, string name) {
	// Initialisation des elements du noeud
	this->typeNodeGraph = typeNodeGraph; //      (GFD, GFC, ELT_ES, OPS_ASS, OPR)
	this->Numero = Numero; //  (0 -> oo)
    this->Name = name;
	// Initialisation gestion des listes listSucc et listPred
	nbSucc = 0;
	nbPred = 0;
	Etat = NONVISITE;
}
/*
 *	Constructeur par copie
 *	\param other : Noeud a copier
 *
 */

NoeudGraph::NoeudGraph(const NoeudGraph & other) :
	Name(other.Name), typeNodeGraph(other.typeNodeGraph), Numero(other.Numero), // warning!
			nbSucc(0), nbPred(0) {
	Etat = other.Etat;
	originalName = other.originalName;
}

/**
 * Destructeur de la classe NoeudGraph
 */
NoeudGraph::~NoeudGraph() {
	for(int i = 0 ; i<nbPred ; i++){
		delete listPred[i];
	}
}

/**
 * typeVarFonction de recherche d'un noeud dans une liste de NoeudGraph.
 *	\return element de la liste associe au noeud
 *	\param NGraph : pointeur sur l element a rechercher
 *	\param Liste : pointeur sur la liste
 *
 */
NodeGraphContainer::iterator Recherche(NoeudGraph * NGraph, NodeGraphContainer * Liste) {

	NodeGraphContainer::iterator it;

	for (it = Liste->begin(); it != Liste->end(); ++it) // parcours de la liste
	{
		if ((*it) == NGraph) {
			return it; // On renvoie le pointeur (version iterateur) sur l'element rechercher( a changer)
		}
	}

	return Liste->end(); // On renvoie NULL (version iterateur) si l'on a rien trouve
}

/**
 * typeVarFonction de recherche d'un noeud dans une liste de NoeudGraph.
 *	\return position de l'element de la liste associe au noeud
 *	\param NGraph : pointeur sur l element a rechercher
 *	\param Liste : pointeur sur la liste
 *
 */

int RecherchePosition(NoeudGraph * NGraph, NodeGraphContainer * Liste) {

	int i = 0;
	NodeGraphContainer::iterator it;

	for (it = Liste->begin(); it != Liste->end(); ++it) // parcours de la liste
	{
		if ((*it) == NGraph) {
			return (i); // On renvoie la position de l'element trouve
		}
		i++;
	}

	return -1; // On renvoie -1 si l'on a rien trouve
}

/**
 *  Fonction de recherche d'un noeud dans une liste de listSucc ou de listPred
 *	\return Element de la liste associe au noeud
 *	\param NGraph : pointeur sur l element a rechercher
 *	\param Liste : pointeur sur la liste
 *
 */

ListSuccPred::iterator searchSuccPred(NoeudGraph * NGraph, ListSuccPred * Liste) {

	NoeudGraph *NoeudCourantPred;
	NoeudGraph *NoeudCourantSucc;
	ListSuccPred::iterator it;

	for (it = Liste->begin(); it != Liste->end(); ++it) // parcours de la liste
	{
		NoeudCourantSucc = (*it)->getNodeSucc();
		NoeudCourantPred = (*it)->getNodePred();
		if (NGraph == NoeudCourantSucc || NoeudCourantPred) {
			return it; // On renvoie le pointeur (version iterator) sur l'element rechercher( a changer)
		}
	}
	return Liste->end(); // On renvoie NULL (version iterator) si l'on a rien trouve

}

/**
 *  Fonction de recherche d'un noeud dans une liste de listSucc ou de listPred
 *	\return Position de l'element de la liste associe au noeud
 *	\param NG : pointeur sur l element a rechercher
 *	\param Liste : pointeur sur la liste
 *
 */
int RecherchePositionSucPred(NoeudGraph * NG, ListSuccPred * Liste) {

	int i = 0;
	NoeudGraph *NoeudCourantPred;
	NoeudGraph *NoeudCourantSucc;
	ListSuccPred::iterator it;

	for (it = Liste->begin(); it != Liste->end(); ++it) // parcours de la liste
	{
		NoeudCourantSucc = (*it)->getNodeSucc();
		NoeudCourantPred = (*it)->getNodePred();
		if (NG == NoeudCourantSucc || NoeudCourantPred) {
			return (i); // On renvoie la position de l'element trouve
		}
		i++;
	}

	return -1; // On renvoie -1 si l'on a rien trouve
}

// ======================================= Methodes permettant d'enlever un element dans les listes Suc et Pred
// =================================================================
// =================================================================


/**
 * Suppression d'un successeur-predecesseur (noeud isolé)
 *	\param Init : Noeud Init du Graph où il faut supprimer les noeuds isolés
 *	\param Fin : Noeud Fin du Grpah où il faut supprimer les noeuds isolés  
 */

void NoeudGraph::removeSuccPred(NoeudGraph * Init, NoeudGraph * Fin) {

	int	iInit = Init->searchSucc(this);
	if (iInit != -1) {
		delete Init->getEdgeSucc(iInit);
		Init->getListeSucc()->erase(Init->getListeSucc()->begin()+iInit);
		Init->setNbSucc(Init->getNbSucc() - 1);
	}

	int iFin = Fin->searchPred(this);
	if (iFin != -1) {
		delete Fin->getEdgePred(iFin);
		Fin->getListePred()->erase(Fin->getListePred()->begin()+iFin);
		Fin->setNbPred(Fin->getNbPred() - 1);
	}

	// Noeud isolé : il n'est donc lié à rien d'autre
	// On vide ses listes de suc/pred complètement
	this->getListePred()->clear();
	this->getListeSucc()->clear();
	this->setNbPred(0);
	this->setNbSucc(0);
	
}

// ======================================= Methodes permettant d'obtenir un NoeudGraph de la liste
// =================================================================
// =================================================================

/**
 *	Acces a l element i de la liste des listSucc du NoeudGraph
 * 		\return Pointeur sur le ieme element
 *		\param i : Indice de l element de la liste
 */

NoeudGraph *NoeudGraph::getElementSucc(int i) {

	if (i < nbSucc) // On verifie si l'element i existe
	{
		return this->listSucc[i]->getNodeSucc();
	}
	else {
		return NULL; // Return NULL si l'element i n'existe pas

	}
}

/**
 * Acces a l element i de la liste des listPred du NoeudGraph
 *  \return Pointeur sur le ieme element
 *	\param i : Indice de l element de la liste
 */

NoeudGraph *NoeudGraph::getElementPred(int i) {

	if (i < nbPred) // On verifie si l'element i existe
	{
		return this->listPred[i]->getNodePred();
	}
	else {
		return NULL; // Return NULL si l'element n'existe pas
	}
}


/**
 * Acces a l element i de la liste des listPred du NoeudGraph
 * A utiliser uniquement dans des transformations pour pouvoir gérer ce genre de cas :
 *  	Listpred => 0 1 2 3
 *  	suppression de 2 => 0 1 NULL 3
 *  	suppression de 3 => 0 1 NULL NULL
 *  Ceci ne marche pas avec getElementPred dû a la contrainte de continuité des vecteur Pred
 *   \return Pointeur sur le ieme element
 *	\param i : Indice de l element de la liste
 */

NoeudGraph *NoeudGraph::getElementPredTransformation(int i) {

	if (i < (int) this->listPred.size()) // On verifie si l'element i existe
	{
		return this->listPred[i]->getNodePred();
	}
	else {
		return NULL; // Return NULL si l'element n'existe pas
	}
}

// ======================================= Methodes permettant d'obtenir ou de changer le numero des elements Pred
// =================================================================
// =================================================================

/**
 * 	Permet de recuperer le numero de l'element predecesseur i
 *   	\return Numero du iemme predecesseur
 *		\param i : Indice de l element de la liste
 */

int NoeudGraph::getNumInputEdgePred(int i) {

	ListSuccPred::iterator it;

	if (i < nbPred) // On verifie si l'element i existe
	{
		return this->listPred[i]->getNumInput(); // Return le pointeur sur le noeud de position i
	}
	else {
		trace_error("The index is greater then the number of elements of the Pred List of node %i", (this)->getNumero());
		return -1; // Return -1 si l'element n'existe pas
	}
}

/**
 * Permet d'initialiser le numero de l'element predecesseur i
 *	\param i : Indice de l element de la liste
 */

void NoeudGraph::setNumInputEdgePred(int i, int Val) {
	ListSuccPred::iterator it;
	if (i < nbPred) // On verifie si l'element i existe
	{
		this->listPred[i]->setNumInput(Val); // On change le numero
	}
	else {
		trace_error("The index is greater then the number of elements of the Pred List of node %i", (this)->getNumero());
		exit(-1);
	}
}

// ======================================= Methodes de creation, d'insertion et de supression des arcs au milieu d'un graph
// =================================================================
// =================================================================

/**
 *	Ajoute e dans la liste des edges successeurs  
 *	\param e : Edge a ajouter dans la liste des successeurs
 */
void NoeudGraph::addSuccEdge(Edge * e) {
	e->setNodePred(this);
	listSucc.push_back(e);
	nbSucc++;
}

/**
 *	Ajoute e dans la liste des edges prédecesseur  
 *	\param e : Edge a ajouter dans la liste des prédecesseur
 */	
void NoeudGraph::addPredEdge(Edge * e) {
	e->setNodeSucc(this);
	listPred[e->getNumInput()] = e;
	nbPred++;
}
/**
 * Add a new Edge element between this and NGraph
 * \param NGraph : NoeudGraph auqel relier this
 *
 */
void NoeudGraph::addEdge(NoeudGraph * NGraph) {
	Edge *Elt;
	
	if (!this->isSucc(NGraph)) {
		Elt = new Edge(this, NGraph, NGraph->getNbPred());
		this->listSucc.push_back(Elt);
		this->nbSucc++;
		NGraph->setListePred(Elt , NGraph->getNbPred());
		NGraph->setNbPred(NGraph->getNbPred() + 1);

	}
	//Sécurité non utile
	/*else {
		cout << "Le noeud " << NGraph->getName() << " est déjà en suc ou pred de " << this->getName() << endl;
		exit(-1);
	}*/
}

/**
 * 	Creation of Edge between tete and this node
 *		\param Tete : Node pointer
 *		\param numInput : numéro d'opérande de l'edge sur le neoud tête 
 *
 *	\author Nicolas Simon
 *	\date 28/06/2010
 */

void NoeudGraph::edgeDirectedTo(NoeudGraph * Tete, int numInput) {
	//this->edgeDirectedTo(Tete,NULL);
	if (this->getTypeNodeGraph() == GFD) {
		((NoeudGFD*)this)->edgeDirectedTo(Tete, numInput);
	}
	else if (this->getTypeNodeGraph() == GFL) { // Les edge GFL n'ont pas besoin de connaitre l'entrée sur lequel ils arrivent
		this->edgeDirectedTo(Tete, (PseudoLinearFunction *) NULL);
	}
	else {
		this->addEdge(Tete);
	}
}


/**
 * 	Creation of Edge between this and Tete node (Used by GFL)
 *		\param Queue : Node pointer
 *		\param Mfct : Linear function linked with the EdgeGFL
 *
 *	\author Nicolas Simon
 *	\date 28/06/2010
 */
void NoeudGraph::edgeDirectedTo(NoeudGraph * Tete, PseudoLinearFunction * Mfct) {
	if (Tete->getTypeNodeGraph() != GFL && Tete->getTypeNodeGraph() != FIN) {
		trace_error("Un edge GFL ne peut être ajoutée que seulement entre des NoeudGFL");
		exit(-1);
	}

	this->addEdge(Tete, Mfct);
}



/**
 * 	Creation of Edge between this and Tete node (Used by GFL) without verification
 *		\param Queue : Node pointer
 *		\param Mfct : Linear function linked with the EdgeGFL
 *
 *	\author Nicolas Simon
 *	\date 28/06/2010
 */
void NoeudGraph::edgeDirectedToRapide(NoeudGraph * Tete, PseudoLinearFunction * Mfct) {
	if (Tete->getTypeNodeGraph() != GFL && Tete->getTypeNodeGraph() != FIN) {
		trace_error("Un edge GFL ne peut être ajoutée que seulement entre des NoeudGFL");
		exit(-1);
	}

	this->addEdgeRapide(Tete, Mfct);
}



/**
 *	Creation of Edge between queue and this node
 *		\param Queue : Node pointer
 *		\param numInput : numero d'opérande de l'edge sur le noeud this
 *
 *	\author Nicolas Simon
 *	\date 28/06/2010
 */
void NoeudGraph::edgeFromOf(NoeudGraph * Queue, int numInput) {
	//this->edgeFromOf(Queue, NULL);
	if (Queue->getTypeNodeGraph() == GFD)
		((NoeudGFD*)this)->edgeFromOf(Queue, numInput);
	else if (Queue->getTypeNodeGraph() == GFL) // Les Edge GFL n'ont pas besoin de savoir sur quelles entrées ils vont
		this->edgeFromOf(Queue, (PseudoLinearFunction *) NULL);
	else
		Queue->addEdge(this);

}

/**
 *	Creation of Edge between Queue and this node (Used by GFL)
 *		\param Queue : Node pointer
 *		\param Mfct : Linear function linked with the EdgeGFL
 *
 *	\author Nicolas Simon
 *	\date 28/06/2010
 */
void NoeudGraph::edgeFromOf(NoeudGraph * Queue, PseudoLinearFunction * Mfct) {
	if (this->getTypeNodeGraph() != GFL && this->getTypeNodeGraph() != FIN) {
		trace_error("Un edge GFL ne peut être ajoutée que seulement entre des NoeudGFL");
		exit(-1);
	}

	Queue->addEdge(this, Mfct);
}

/**
 * 	Creation d un arc dirige entre le noeud Courant et un noeud Tete ( ne test oas si il existe deja)
 *		\param Tete : pointeur sur le noeud correspondant a la tete de l'arc
 *		\param numInput : numero d'opérande de l'edge sur le noeud Tete
 *
 */
void NoeudGraph::edgeDirectedToRapide(NoeudGraph * Tete, int numInput) {
	this->addEdgeRapide(Tete, numInput);
}

/**
 * Creation d un arc dirige entre un noeud Queue et le noeud Courant(ne teste pas si il existe deja)
 *		\param Queue : pointeur sur le noeud correspondant a la Queue de l arc
 *		\param numInput : numero d'opérande de l'edge sur le noeud this 
 *
 */
void NoeudGraph::edgeFromOfRapide(NoeudGraph * Queue, int numInput) {
	Queue->addEdgeRapide(this, numInput);
}

/**
 *	Methode de suppression d'un arc
 *		\param Queue : pointeur sur la queue de l'arc
 */
void NoeudGraph::EnleveedgeFromOf(NoeudGraph * Queue) {
	Queue->removeEdge(this);
}

/**
 *	Methode de suppression d'un arc
 *		\param Tete: pointeur sur la tete de l'arc
 */

void NoeudGraph::deleteEdgeDirectedTo(NoeudGraph * Tete) {
	this->removeEdge(Tete);
}

// ======================================= Methodes de recherche des operation de delay
// =================================================================
// =================================================================
/**
 *	Test si le noeud est un noeud isolé ou non 
 *	\return Vrai si le noeud est isole, faux sinon
 *  \author Loic Cloatre
 *  \date 18/05/2009
 */
bool NoeudGraph::isNodeIsolate() {
	bool ret = false;
	int nbPred = -1;
	int nbSuccc = -1;

	if (this->getNbPred() == 0) {
		nbPred = 0;
	}
	else if ((this->getNbPred() == 1) && (this->getElementPred(0)->getNumero() == 0)) //un pred: init
	{
		nbPred = 0;
	}

	if (this->getNbSucc() == 0) {
		nbSuccc = 0;
	}
	else if ((this->getNbSucc() == 1) && (this->getElementSucc(0)->getNumero() == 1)) //un pred: fin
	{
		nbSuccc = 0;
	}

	if ((nbSuccc == 0) && (nbPred == 0)) //si le noeud n'a ni succ ni pred il est isole
	{
		ret = true;
		//cout<<"noeud isole: "<<this->getName()->c_str()<<endl;
	}
	return ret;
}

/**
 * Return Edge element pointer of this successor list with i like index
 *		\return Edge element pointer
 *		\param i : index in the successor list of this
 *
 *  \author Nicolas Simon
 *  \date 28/06/2010
 */
Edge *NoeudGraph::getEdgeSucc(int i) {
	if (i < nbSucc) // On verifie si l'element i existe
	{
		return this->listSucc[i]; // Return le pointeur sur l'Edge Successeur de position i
	}
	else {
		return NULL; // Return NULL si l'element n'existe pas
	}
}
/**
 *	Return Edge element pointer of this predecessor list with i like index
 *		\return Edge : Edge element pointer
 *		\param i : index in the predecessor list of this
 *
 *  \author Nicolas Simon
 *  \date 28/06/2010
 */
Edge *NoeudGraph::getEdgePred(int i) {
	if (i < nbPred) // On verifie si l'element i existe
	{
		return this->listPred[i]; // Return le pointeur sur l'Edge Predecesseur de position i
	}
	else {
		return NULL; // Return NULL si l'element n'existe pas
	}
}

/**
 *	Return Edge element pointer of this predecessor list with i like index
 *	A utiliser uniquement dans des transformations pour pouvoir gérer ce genre de cas :
 *  	Listpred => 0 1 2 3
 *  	suppression de 2 => 0 1 NULL 3
 *  	on veut recuperer le numInput contenu dans l'egde de 3 avant sa suppression
 *	Ceci ne marche pas avec getEdgePred dû a la continuité de list Pred
 *      \return Edge : Edge element pointer
 *		\param i : index in the predecessor list of this
 *
 *  \author Nicolas Simon
 *  \date 28/06/2010
 */
Edge *NoeudGraph::getEdgePredTransformation(int i) {
	if (i < (int) this->listPred.size()) // On verifie si l'element i existe
	{
		return this->listPred[i]; // Return le pointeur sur l'Edge Predecesseur de position i
	}
	else {
		return NULL; // Return NULL si l'element n'existe pas
	}
}

/*
 *	Return Edge pointer from this to nGraph if nGraph is successor of this, else NULL
 *	\param nGraph : noeud tete de l'edge 
 *	\return Rend l'edge entre this et nGraph, NULL sinon
 *
 *	\author Nicolas Simon
 *	\date 18/05/11
 */

Edge* NoeudGraph::getEdgeSucc(NoeudGraph* nGraph){
	for(int i = 0; i<this->nbSucc ; i++){
		if(this->listSucc[i]->getNodeSucc() == nGraph)
			return this->listSucc[i];
	}
	return NULL;
}

/*
 *	Return Edge pointer from nGraph to this if nGraph is predecessor of this, else NULL
 *  \param nGraph : noeud queue de l'Edge
 *  \return Rend l'edge entre nGraph et this, NULL sinon
 *
 *	 \author Nicolas Simon
 *	 \date 18/05/11
 */
Edge* NoeudGraph::getEdgePred(NoeudGraph* nGraph){
	for(int i = 0 ; i<this->nbPred ; i++){
		if(this->listPred[i]->getNodePred() == nGraph)
			return this->listPred[i];
	}
	return NULL;
}


/**
 *	Add a new Edge element between this and Tete (no verification if a edge is already in the numInput slot)
 *		\param Tete : Head of the edge
 *		\param numInput : operand number of the head
 *
 *  \author Nicolas Simon
 *  \date 28/06/2010
 */
void NoeudGraph::addEdgeRapide(NoeudGraph * Tete, int numInput) {
	Edge * Elt;

	if (this->getTypeNodeGraph() != GFL) // Le Noeud n'est pas un GFL, ajout d'un EdgeGFD
	{
		if (((NoeudGFD *) Tete)->getTypeNodeGFD() != OPS)
			Elt = new EdgeGFD(this, Tete, 0);
		else if (numInput >= 0)
			Elt = new EdgeGFD(this, Tete, numInput);
		else {
			trace_error("Le numero d'entrée de l'edge vers un operateur doit être précisé et >= à 0");
			exit(-1);
		}
	}
	else // Le Noeud est un GFL, ajout d'un EdgeGFL
	{
		Elt = new EdgeGFL(this, Tete, (PseudoLinearFunction*) NULL);
	}

	this->listSucc.push_back(Elt);
	Tete->setListePred(Elt, Elt->getNumInput());
	this->nbSucc++; // Incrementation du nombre de listSucc
	Tete->setNbPred(Tete->getNbPred() + 1);
}

/**
 * Removes the Edge which has a way like this->NGraph
 *		\param NGraph : Node wanted
 *
 *  \author Nicolas Simon
 *  \date 28/06/2010
 */
void NoeudGraph::removeEdge(NoeudGraph * NGraph) {

	if(this->isSucc(NGraph)){

		int iPred = NGraph->searchPred(this);
		int iSucc = this->searchSucc(NGraph);


		if (iPred == -1){
			trace_error("itPred pas trouvé");
			exit(-1);
		}

		if (iSucc == -1){
			trace_error("itSuc pas trouvé");
			exit(-1);
		}

		delete (this->getEdgeSucc(iSucc)); // Suppression de l'Edge
		if(NGraph->getTypeNodeGraph() == FIN || NGraph->getTypeNodeGraph() == GFL)	// On supprime facon liste pour supprimer en même temp la case.
			NGraph->getListePred()->erase(NGraph->getListePred()->begin()+iPred);
		else // Pas d'erase dans la list de pred car ceci enleve une case. Il est directement remplacé par un nouveau edge dans la suite de l'algo.
			NGraph->setListePred(NULL, iPred);

		NGraph->setNbPred(NGraph->getNbPred() - 1);

		listSucc.erase(listSucc.begin()+iSucc); // Suppression de l'Edge la liste des listSucc de this
		this->nbSucc--;
	}
}


/**
 *  True if NGraph is a successor of this, false otherwise
 *		\return boolean : True if NGraph is a predecessor of this, false otherwise
 *		\param NGraph : Node wanted
 *
 *  \author Nicolas Simon
 *  \date 28/06/2010
 */
bool NoeudGraph::isSucc(NoeudGraph * NGraph) {
	for (int i = 0; i<this->getNbSucc() ; i++) {
		if (NGraph == this->getElementSucc(i)) {
			return true;
		}
	}
	return false;
}

/**
 *  True if NGraph is a predecessor of this, false otherwise
 *		\return boolean : True if NGraph is a predecessor of this, false otherwise
 *		\param NGraph : Node wanted
 *
 *  \author Nicolas Simon
 *  \date 28/06/2010
 */
bool NoeudGraph::isPred(NoeudGraph * NGraph) {
	for (int i = 0 ; i<this->getNbPred() ; i++) {
		if (NGraph == this->getElementPred(i)) {
			return true;
		}
	}
	return false;
}

/**
 *  Determine if NGraph is a successor of this and return version iterator pointer of successor list element
 *		\return iterator : Iterator pointer of successor list element
 *		\param NGraph : Node wanted
 *
 *  \author Nicolas Simon 
 *  \date 28/06/2010
 */
int NoeudGraph::searchSucc(NoeudGraph * NGraph) {
	for(int i = 0 ; i<this->getNbSucc() ; i++){
		if(NGraph == this->getElementSucc(i))
			return i;
	}
	return -1;
}

/**
 *  Determine if NGraph is a predecessor of this and return version iterator pointer of predecessor list element
 *		\return iterator : Iterator pointer of predecessor list element
 *		\param NGraph : Node wanted
 *
 *  \author Nicolas Simon
 *  \date 28/06/2010
 */
int NoeudGraph::searchPred(NoeudGraph * NGraph) {
	for(int i = 0 ; i<(int)this->getListePred()->size() ; i++){
		if(this->getListePred()->at(i) != NULL && NGraph == this->getListePred()->at(i)->getNodePred())
			return i;
	}
	return -1;
}

void NoeudGraph::printInfo(FILE * f) {
	fprintf(f, "%d:%s\n", getNumero(), Name.c_str());
}

/**
 *  typeVarFonction determinant si un noeud est un Sink du graphe
 * 	\return booleen TRUE si le noeud est un Sink
 *
 *	Un noeud est un Sink si
 *		- il n'a pas de listSucc (normalement ça n'arrive pas...)
 *		- il a un seul successeur : le noeud FIN
 */
bool NoeudGraph::isNodeSink() {
	int NblistSucc = (this)->getNbSucc();

	if (NblistSucc == 0) {
		return true;
	}
	else if (NblistSucc == 1) {
		NoeudGraph *NGFL = getElementSucc(0);

		if (NGFL->getTypeNodeGraph() == FIN)
			return true;
	}

	return false;
}


void NoeudGraph::setListePred(Edge * element, int i) {
	if((int)listPred.size()<i+1){
		listPred.resize(i+1);
	}
	
	listPred[i] = element;
}

void NoeudGraph::setListeSucc(Edge * element, int i) {
	if((int)listSucc.size()<i+1){
		listSucc.resize(i+1);
	}
	
	listSucc[i] = element;
}
