/*
 * GF.cc
 *
 *  Created on: 28 janv. 2009
 *      Author: cloatre
 */

// === Bibliothèques .hh associe
#include <sstream>
#include <queue>
#include <stdio.h>
#include <stdlib.h>

#include "graph/Graph.hh"

#include "graph/dfg/datanode/NoeudSrcBruit.hh"
#include "graph/dfg/EdgeGFD.hh"
#include "graph/dfg/NoeudInitFin.hh"
#include "graph/lfg/EdgeGFL.hh"
#include "graph/lfg/NoeudGFL.hh"


// === Fonctions extern
extern NodeGraphContainer::iterator Recherche(NoeudGraph * Noeud, NodeGraphContainer * Liste);


/**
 *  Definition du predicat pour pouvoir trier la liste des noeuds par ordre de numero
 *
 *  \return false si les elements sont inverses
 *
 *  by Loic Cloatre creation le 02/06/2009
 */
bool predicatOrdreNoeud(NoeudGraph * elt1, NoeudGraph * elt2) {
	return elt1->getNumero() < elt2->getNumero();
}

// ======================================= Constructeurs - Destructeurs
// ===============================================================
// ===============================================================

/**
 * \brief Constructeur
 *
 *  Constructeur par defaut de la classe Graph
 *
 *  \param noiseEval : Vrai si traitement en calcul de la précision
 */
Graph::Graph(bool noiseEval) {
	tabNode = new NodeGraphContainer; /* Liste des noeuds du graphes */
	nbNode = 0; /* Nombre de noeuds dans le graphes */

	tabInput = new NodeGraphContainer; /* Liste des noeuds DATA correspondant a l'entree du Gf */
	nbInput = 0; /* Initialisation du nombre de noeuds entree */

	tabOutput = new NodeGraphContainer; /* Liste des noeuds DATA correspondant � la sortie du Gf */
	nbOutput = 0; /* Initialisation du nombre de noeuds sortie */

	tabVarInt = new NodeGraphContainer; /* Liste des noeuds DATA correspondant � la sortie du Gf */
	nbVarInt = 0; /* Initialisation du nombre de noeuds variable intermediaire  */
	noeudInit = new NoeudInit(0); /* Noeud INIT */
	tabNode->push_back(noeudInit); //append
	noeudFin = new NoeudFin(1); /* Noeud FIN  */
	tabNode->push_back(noeudFin); //append
	nbNode = 2; /* Nombre de noeuds dans le graphes */// Il y a 2 noeud (INIT et FIN)
	NumNode = nbNode; //Numeros du prochain noeud

	this->noiseEval = noiseEval; // Permet de connaitre dans quel traitement va être applique au graph
}

/**
 *	\brief Destructeur
 *
 *  Destructeur de la classe Graph
 */
Graph::~Graph() {
	//cout << "DEBUT SUPPR GRAPH\n";
	NodeGraphContainer::iterator it;
	it = tabNode->begin();
	while (it != tabNode->end()) {
		delete (*it++);
	}

	tabNode->clear();
	tabVarInt->clear();
	tabInput->clear();
	tabOutput->clear();

	delete tabNode;
	delete tabVarInt;
	delete tabInput;
	delete tabOutput;

	//delete noeudInit;                                                        //destructeur
	//delete noeudFin;                                                         //destructeur

	if (m_logDebug_pf.is_open()) {
		m_logDebug_pf.close();
	}

	//cout << "FIN SUPPR GRAPH\n";
}

// ======================================= Methode renvoyant l'element i
// ===============================================================
// ===============================================================

/**
 * Acces a l'élement i du Graph
 * Le numero du noeud doit etre inferieur au nombre de noeud
 *	\param i : Indice de l element de la liste tabNode
 *	\return Le noeud d'indice i
 */
NoeudGraph* Graph::getElementGf(int i) const {
	// Indice superieur au nombre d'elements dans le conteneur
	assert(i < (int) tabNode->size());
	return tabNode->at(i);
}


/**
 * Fonctions d'obtention de l'element i la liste des entrees
 * Le numero du noeud doit etre inferieur au nombre de noeud
 *      \param i : indice de l'element dans la liste
 *		\return Le noeud d'indice i
 */
NoeudGraph *Graph::getInputGf(int i) {
	// Indice superieur au nombre d'elements dans le conteneur
	assert(i < nbInput && nbInput == (int) tabInput->size());
	return tabInput->at(i);
}


/**
 * Fonctions d'obtention de l'element i la liste des sorties
 * Le numero du noeud doit etre inferieur au nombre de noeud
 *      \param i : indice de l'element dans la liste
 *		\return Le noeud d'indice i
 */
NoeudGraph *Graph::getOutputGf(int i) {
	// Indice superieur au nombre d'elements dans le conteneur
	assert(i < nbOutput && nbOutput == (int) tabOutput->size());
	return tabOutput->at(i);
}

/**
 * Fonctions d'obtention de l'element i la liste des VarInts
 * Le numero du noeud doit etre inferieur au nombre de noeud
 *      \param i : indice de l'element dans la liste
 *		\return Le noeud d'indice i
 */
NoeudGraph *Graph::getVarIntGf(int i) {
	// Indice superieur au nombre d'elements dans le conteneur
	assert(i < nbVarInt && nbVarInt == (int) tabVarInt->size());
	return tabVarInt->at(i);
}

// ======================================= Methodes d'ajout et de supression de NoeudGraph dans le Gf
// ===============================================================
// ===============================================================

/**
 * Fonction d'ajout d'un noeud NoeudGraph dans le Gf en testant si il existe deja
 *       \param Noeud : noeud à ajouter
 */
void Graph::addNoeudGraph(NoeudGraph * Noeud) {
	assert(Noeud != NULL);
	if (Recherche(Noeud, tabNode) != tabNode->end()) // Si l'element existe
	//if (this->rechercheNoeud(Noeud) != tabNode->end()) // Si l'element existe
	{
		trace_error("The element node %i is already in the TabNode list of the graph", Noeud->getNumero());
		exit(1);
	}
	else // Si l'element n'existe pas
	{
		tabNode->push_back(Noeud); // On l'ajoute a la fin de la liste
		nbNode++; //on incremente le nombre de noeud
		NumNode++; //on incremente le numero du prochain noeud
	}
}

/**
 * Fonction d'ajout d'un noeud NoeudGraph dans le Gf sans tester si il existe deja
 *       \param Noeud : noeud a ajouter
 */
void Graph::addNoeudGraphRapide(NoeudGraph * Noeud) {
	tabNode->push_back(Noeud); // On l'ajoute a la fin de la liste
	nbNode++; //on incremente le nombre de noeud
	NumNode++; //on incremente le numero du prochain noeud
}


/**
 * Fonctions d'ajout d'un noeud dans la liste des entrees
 *       \param Noeud : noeud à ajouter
 */
void Graph::addInputGf(NoeudGraph * Noeud) {
	NodeGraphContainer::iterator it;
	it = Recherche(Noeud, tabInput);
	//it = this->rechercheNoeudInput(Noeud);
	if (it != tabInput->end()) {
		trace_error("The element node %i is already in the input list of the graph", Noeud->getNumero());
		exit(1);
	}
	else {
		tabInput->push_back(Noeud);
		nbInput++;
		//test si lien avec noeud init
		if(!Noeud->isPred(getNoeudInit())){
			getNoeudInit()->edgeDirectedTo(Noeud);
		}
	}


}

/**
 * Fonctions d'ajout d'un noeud dans la liste des sorties
 *       \param Noeud : noeud à ajouter
 */
void Graph::addOutputGf(NoeudGraph * Noeud) {
	NodeGraphContainer::iterator it;
	it = Recherche(Noeud, tabOutput);
	//it = this->rechercheNoeudOutput(Noeud);
	if (it != tabOutput->end()) {
		trace_error("The element node %i is already in the outpur list of node of the graph", Noeud->getNumero());
		exit(1);
	}
	else {
		tabOutput->push_back(Noeud);
		nbOutput++;
		//test si lien avec noeud init
		if(!Noeud->isPred(getNoeudFin())){
			Noeud->edgeDirectedTo(getNoeudFin());
		}
	}
}

/**
 * Fonctions d'ajout d'un noeud dans la liste des VarInts
 *       \param Noeud : noeud à ajouter
 */
void Graph::addVarIntGf(NoeudGraph * Noeud) {
	NodeGraphContainer::iterator it;

	it = Recherche(Noeud, tabVarInt);
	//it = this->rechercheNoeudVarInt(Noeud);
	if (it != tabVarInt->end()) {
		trace_error("The element node %i is already in he input list of the graph", Noeud->getNumero());
		exit(1);
	}
	else {
		tabVarInt->push_back(Noeud);
		nbVarInt++;
	}
}

/**
 * Fonction de suppression d'un noeud NoeudGraph dans le Gf
 *       \param Noeud : noeud a enlever
 *       \return Rend l'iterator sur le noeud suivant du noeud supprimé
 */
NodeGraphContainer::iterator Graph::deleteGraphNode(NoeudGraph * Noeud) {
	NodeGraphContainer::iterator it;
	it = Recherche(Noeud, tabNode); // typeVarFonction recherche definie dans NoeudGraph.cc
	//it = this->rechercheNoeud(Noeud); // typeVarFonction recherche definie dans NoeudGraph.cc
	if (it == tabNode->end()) // Si n'element pas existe (tabNode->end() == NULL version iterateur)
	{
		trace_error("The element node %i is not in the TabNode list of the graph", Noeud->getNumero());
		exit(1);
	}

	it = tabNode->erase(it); // On suprime le noeud
	nbNode--; // On decremente le nombre de noeud
	if(Noeud->getTypeNodeGraph() == GFD && ((NoeudGFD*)Noeud)->getTypeNodeGFD() == DATA/* && ((NoeudData * )Noeud)->getTypeNodeData() != DATA_SRC_BRUIT*/){
		if(((NoeudData*)Noeud)->isNodeInput())
			this->deleteInputGf(Noeud);
		if(((NoeudData*)Noeud)->isNodeOutput())
			this->deleteOutputGf(Noeud);
		if(((NoeudData*)Noeud)->isNodeVar())
			this->deleteVarIntGf(Noeud);
	}
	delete Noeud;
	return it;
}
/**
 *	Fonction qui supprime le neoud d'indice i dans le Graph
 *	\param i : indice du noeud à supprimer dans le graph
 *
 */
void Graph::deleteGraphNode(int i){
//	NodeGraphContainer::iterator it = tabNode->begin();
	assert(i < (int) tabNode->size());

	if(tabNode->at(i)->getTypeNodeGraph() == GFD && ((NoeudGFD*)tabNode->at(i))->getTypeNodeGFD() == DATA /*&& ((NoeudData * )(*it))->getTypeNodeData() != DATA_SRC_BRUIT*/){
		if(((NoeudData*)tabNode->at(i))->isNodeInput())
			this->deleteInputGf(tabNode->at(i));
		if(((NoeudData*)tabNode->at(i))->isNodeOutput())
			this->deleteOutputGf(tabNode->at(i));
		if(((NoeudData*)tabNode->at(i))->isNodeVar())
			this->deleteVarIntGf(tabNode->at(i));
	}
	delete tabNode->at(i);
	tabNode->erase(tabNode->begin()+i);
	nbNode--;

}

/**
 * Fonctions de suppression d'un noeud dans la liste des entrees
 *       \param Noeud : noeud a enlever
 */
void Graph::deleteInputGf(NoeudGraph * Noeud) {
	NodeGraphContainer::iterator it;

	it = Recherche(Noeud, tabInput);
	//it = this->rechercheNoeudInput(Noeud);
	if (it != tabInput->end()) {
		tabInput->erase(it);
		nbInput--;
	}
	else {
		trace_error("The element node %i is not contains in the input list of the graph", Noeud->getNumero());
		exit(-1);
	}
}

/**
 * Fonctions de suppression d'un noeud dans la liste des sorties
 *       \param Noeud : noeud à enlever
 */
void Graph::deleteOutputGf(NoeudGraph * Noeud) {
	NodeGraphContainer::iterator it;
	it = Recherche(Noeud, tabOutput);
	//it = this->rechercheNoeudOutput(Noeud);
	if (it != tabOutput->end()) {
		tabOutput->erase(it);
		nbOutput--;
	}
	else {
		trace_error("The element node %i is not contains in the output list of the graph", Noeud->getNumero());
		exit(-1);
	}
}

/**
 * Fonctions de suppression d'un noeud dans la liste des VarInts
 *      \param Noeud : noeud à enlever
 */
void Graph::deleteVarIntGf(NoeudGraph * Noeud) {
	NodeGraphContainer::iterator it;

	it = Recherche(Noeud, tabVarInt);
	//it = this->rechercheNoeudVarInt(Noeud);
	if (it != tabVarInt->end()) {
		tabVarInt->erase(it);
		nbVarInt--;
	}
	else {
		trace_error("The element node %i is not contains in the input list of the graph", Noeud->getNumero());
		exit(-1);
	}
}




// ======================================= Methodes de recherche dans le graph
// ===============================================================
// ===============================================================

void Graph::resetInfoVisite() {
	NodeGraphContainer::iterator it = tabNode->begin();
	it++;
	it++;
	for (; it != tabNode->end(); it++)
	{
		(*it)->setEtat(NONVISITE);
	}
}

/**
 * Fonctions de detection d'un NoeudGraph ou bien de detection si les noms sont identiques
 *		\param NoeudCible : Noeud recherche
 *		\return bool : Rend vrai si trouvé, faux sinon
 */
bool Graph::isGfContientNoeud(NoeudGraph * NoeudCible) {
	NoeudGraph *Noeud;
	bool ret = false;

	for (int i = 0; i < (this)->getNbNode(); i++) {
		Noeud = (this)->getElementGf(i);
		if (Noeud == NoeudCible) {
			ret = true;
			i = (this)->getNbNode(); //pour sortir de la boucle
		}
		else if (strcmp(Noeud->getName().c_str(), NoeudCible->getName().c_str()) == false) //si condition ok, les noms sont identiques
		{
			ret = true;
			i = (this)->getNbNode(); //pour sortir de la boucle
		}
	}
	return ret;
}

/**
 * Fonctions de recherche d'un NoeudGraph a partir de son nom
 *		\param nameNoeudCible : nom du Noeud recherche
 *		\return Rend un pointeur sur le noeud si trouvé, NULL sinon
 */
NoeudGraph *Graph::GfRechercheNoeud(string * nameNoeudCible) {
	NoeudGraph *retNoeud;

	for (int i = 0; i < (this)->getNbNode(); i++) {
		retNoeud = (NoeudGraph *) (this)->getElementGf(i);

		if (strcmp(retNoeud->getName().c_str(), nameNoeudCible->c_str()) == 0) //si condition ok, les noms sont identiques
		{	
			return retNoeud;
		}
	}
	return NULL;
}

/**
 * Fonctions de recherche d'un NoeudGraph a partir de son numero
 *		\param numeroNoeudCible : numero du Noeud recherche
 *		\return Rend un pointeur sur le numero si trouvé, NULL sinon
 */
NoeudGraph *Graph::GfRechercheNoeud(int numeroNoeudCible) {
	NoeudGraph *retNoeud;

	for (int i = 0; i < (this)->getNbNode(); i++) {
		retNoeud = (NoeudGraph *) (this)->getElementGf(i);

		if (retNoeud->getNumero() == numeroNoeudCible) //si condition ok, les numeros sont identiques
		{
			return retNoeud; //pour sortir de la boucle
		}
	}
	return NULL;
}


/**
 * Fonction indiquant si la liste des entrées est vide
 * 		\return Vrai si la la liste des entrées est vide, faux sinon
 */
bool Graph::isListInputGfEmpty() {
	if (nbInput > 0) 
		return false;
	else 
		return true;
}


/**
 * Fonction indiquant si la liste des Sorties est vide
 * 		\return Vrai si la la liste des sorties est vide, faux sinon
 * 		
 */
bool Graph::isListOutputGfEmpty() {
	if (nbOutput > 0) 
		return false;
	else 	
		return true;
}

/**
 * Fonction indiquant si la liste des varint est vide
 * 		\return Vrai si la la liste des VarInts est vide, faux sinon
 */
bool Graph::isListVarIntGfEmpty() {
	if (nbVarInt > 0) 
		return false;
	else	
		return true;
}

/**
 * Indique si node est contenu dans la table des entrées
 * 		\return Vrai si node est contenu dans la table des entrées
 */
bool Graph::isInputGfContain(const NoeudGraph* node){
	return nodeIsIn(this->tabInput, node);
}

/**
 * Indique si node est contenu dans la table des sorties
 * 		\return Vrai si node est contenu dans la table des sorties
 */
bool Graph::isOutputGfContain(const NoeudGraph* node){
	return nodeIsIn(this->tabOutput, node);
}

/**
 * Indique si node est contenu dans la table des variables
 * 		\return Vrai si node est contenu dans la table des variables
 */
bool Graph::isVarGfContain(const NoeudGraph* node){
	return nodeIsIn(this->tabVarInt, node);
}

/**
 * Indique si node est contenu dans la table des noeuds
 * 		\return Vrai si node est contenu dans la table des noeuds
 */
bool Graph::isGraphContain(const NoeudGraph* node){
	return nodeIsIn(this->tabNode, node);
}

/**
 *	Indique si la node est contenu dans tab
 *		\return Vrai si node est contenu dans tab, faux sinon
 *
 */
bool Graph::nodeIsIn(const vector<NoeudGraph*> *tab, const NoeudGraph* node){
	for(int i = 0 ; i < (int)tab->size() ; i++){
		if(tab->at(i) == node)
			return true;
	}
	return false;	
}


// ======================================= Methodes d affichage et de sauvegarde dans un fichier
// =================================================================
// =================================================================
// --------------------------------------------- Gf --------------------------------------------

/**
 *  Ecriture au formet dfg dans un ficher(fic)
 *   	\param fic : fichier de destination des informations si le parametre est nul ou stdout la destionation est l ecran
 */
void Graph::print(FILE * fic) const {
	int TpsCalcul = 0, Tclk = 0;
	NodeGraphContainer::iterator it;

	if (fic == NULL) //      Si fic == NULL
	{
		fic = stdout; // On redirige l'affichage sur la sortie standard
	}
	fprintf(fic, "-------------------------\n");
	fprintf(fic, "GRAPHE  \n");
	fprintf(fic, "-------------------------\n");
	fprintf(fic, "TpsCalcul = %d ; Tclk = %d\n\n", TpsCalcul, Tclk);

	for (it = tabNode->begin(); it != tabNode->end(); it++) // parcours de la liste des noeuds
	{
		(*it)->print(fic);
		fprintf(fic, "\n");
	}
	// Impression des differents tableaux du Gf
	printListEntrees(fic);
	printListSorties(fic);
	printListVarInts(fic);
}

/**
 *	Ecriture de la liste des Entrees au format dfg (appeler par print)
 * 		\param fic : fichier de destination des informations si le parametre est stdout la destionation est l ecran
 */
void Graph::printListEntrees(FILE * fic) const {
	NoeudGraph *NGf;
	NodeGraphContainer::iterator it;

	fprintf(fic, " \n Liste Entrees  (%d elts): ", nbInput);

	for (it = tabInput->begin(); it != tabInput->end(); ++it) // Parcours de la liste des Entrees
	{
		NGf = (NoeudGraph *) (*it);
		fprintf(fic, "%d:%s  ", NGf->getNumero(), NGf->getName().data());
	}
}

/**
 *	Ecriture de la liste des Sorties au formet dfg (appeler par print)
 * 		\param fic : fichier de destination des informations si le parametre est stdout la destionation est l ecran
 */
void Graph::printListSorties(FILE * fic) const {
	NoeudGraph *NGf;
	NodeGraphContainer::iterator it;

	fprintf(fic, " \n Liste Sorties  (%d elts): ", nbOutput);

	for (it = tabOutput->begin(); it != tabOutput->end(); ++it) // Parcours de la liste des Sorties
	{
		NGf = (NoeudGraph *) (*it);
		fprintf(fic, "%d:%s  ", NGf->getNumero(), NGf->getName().data());
	}

}

/**
 *	Ecriture de la liste des VarInts au format dfg(appeler par print)
 *   	\param fic : fichier de destination des informations si le parametre est stdout la destionation est l ecran
 */
void Graph::printListVarInts(FILE * fic) const {
	NoeudGraph *NGf;
	NodeGraphContainer::iterator it;

	fprintf(fic, " \n Liste VarInts  (%d elts): ", nbVarInt);

	for (it = tabVarInt->begin(); it != tabVarInt->end(); ++it) // Parcours de la liste des VarInts
	{
		NGf = (NoeudGraph *) (*it);
		fprintf(fic, "%d:%s  ", NGf->getNumero(), NGf->getName().data());
	}
}

// ======================================= Methodes de sauvegarde VCG ou DOT
// =================================================================
// =================================================================

/**
 * Fonction de sauvegarde du Gf dans un fichier au format DOT et DFG
 *
 *  \param Extension : Extension pour le nom du fichier : Gf_xxx.dot
 *  \return bool : true si pas d'erreur
 *	\author Loic Cloatre
 *	\author Quentin Meunier
 *	\author Nicolas Simon 
 *	\date 12/01/2011
 */
void Graph::saveGf(const char *Extension) const {
	FILE *fic;
	string ext = Extension;
	string vcg_filename;
	string dfg_filename;
	if (this->getNoiseEval()) {
		vcg_filename = ProgParameters::getOutputGraphNoiseDirectory() + "GF_" + ext + ".dot";
		dfg_filename = ProgParameters::getOutputGraphNoiseDirectory() + "GF_" + ext + ".dfg";
	}
	else {
		vcg_filename = ProgParameters::getOutputGraphDynDirectory() + "GF_" + ext + ".dot";
		dfg_filename = ProgParameters::getOutputGraphDynDirectory() + "GF_" + ext + ".dfg";
	}

	fic = fopen(vcg_filename.data(), "w");
	if (fic == NULL) {
		trace_error("Error while opening vcg file for writing : %s\n", vcg_filename.data());
		exit(-1);
	}
	else {
		// A remplacer par printVCG(fic) pour un affichage en format vcg
		this->printDOT(fic);
		fclose(fic);
	}

	fic = fopen(dfg_filename.data(), "w");
	if (fic == NULL) {
		trace_error("Error while opening dfg file for writing: %s\n", dfg_filename.data());
		exit(-1);
	}
	else {
		print(fic);
		fclose(fic);
	}

}

/**
 *	Fonction d'ecriture du Graph au format VCG
 *   	\param fp : Fichier de destination des informations
 */
void Graph::printVCG(FILE * fp) const {
	NodeGraphContainer::iterator it;

	// Initialisation du fichier VCG
	fprintf(fp, "graph: {"
		"title: \"FloatToFix graph \"\n"
		"x: 30 \ny: 30 \nxspace: 40 \nyspace: 40 \n"
		"stretch: 60 \nshrink: 100 \n"
		"finetuning: yes \nlayoutalgorithm: minbackward \nport_sharing: no \n"
		"node.borderwidth: 3 \nnode.color: white\nnode.textcolor: black\nnode.bordercolor: black \nnode.shape: ellipse \n"
		"edge.color: black\ndisplay_edge_labels:yes\n");

	it = tabNode->begin();
	++it;

	for (++it; it != tabNode->end(); it++) // parcours de la liste du 3eme au dernier (on ignore INIT et FIN)
	{
		(*it)->printVCG(fp);
	}
	fprintf(fp, "\n}\n");
}

/**
 *	Fonction d'ecriture du Graph au format DOT
 *   	\param fp : fichier de destination des informations
 */
void Graph::printDOT(FILE *fp) const {
	fprintf(fp, "digraph G{\n");
	bool noiseEval = this->getNoiseEval();
	NodeGraphContainer::iterator it;

	it = tabNode->begin();
	it++;
	for (++it; it != tabNode->end(); it++) {
		(*it)->printDOTNode(fp, noiseEval);
	}
	it = tabNode->begin();
	it++;
	for (++it; it != tabNode->end(); it++) {
		(*it)->printDOTEdge(fp);
	}
	fprintf(fp, "}\n");
}

/**
 *  Methodes permetant de lier les Init et les Fin avec le reste du graph 
 *
 */
void Graph::liaisonsSuccPredInitFin(void) {
	NodeGraphContainer::iterator it;
	it = tabNode->begin();
	++it;
	for (++it; it != tabNode->end(); ++it) // parcours la liste des noeud a partie du 3eme (on ne passe pas part Init et Fin)
	{
		if ((*it)->getNbPred() == 0) {
			this->getNoeudInit()->addEdge(*it);
		}
		if ((*it)->getNbSucc() == 0) {
			(*it)->addEdge(this->getNoeudFin());
		}
	}
}

//===========================================detection/enumeration/dementellement circuit

///**
// * Function which creates adjacent matrix then run search of circuit in the graph
// *
// *  \return GraphPath *	  list of path got
// *	\author Loic Cloatre
// *	\date 16/12/2008
// */
//GraphPath *Graph::enumerationOfCircuitPath() {
//	GraphPath *Path = NULL;
//	AdjacentMatrix *matriceEnCours = NULL;
//	GraphPath *testPath = new GraphPath;
//	int numeroSortie = (*this->tabOutput->begin())->getNumero();
//
//	//cout<<"creation matrice adjacente"<<endl;
//	matriceEnCours = this->createAdjacentMatrix(); //on cree la matrice adjacente
//
//	matriceEnCours->displayMatrice();
//
//	//cout<<endl<<"recherche circuit: "<<endl;
//
//	if (this->cycleIsPresent() == true) //on controle pour savoir s'il existe des circuit afin de ne pas lancer l'enumeration qui prend du temps
//	{
//		testPath = Path->searchPathCircuits(matriceEnCours, matriceEnCours->getHauteurMatrice(), PathCircuit, numeroSortie); //on recherche les chemins
//		testPath->displayPath();
//	}
//
//	return testPath;
//
//}

/**
 *  function which call deleteIsolatedNodeNoise for the Noise evaluation or deleteIsolatedNodeDyn for the Dynamic evaluation
 *
 *	\ingroup T11
 *  \author Loic Cloatre
 *  \author Nicolas Simon
 *  \date 08/07/2009
 */
void Graph::deleteIsolatedNode() {
	if (this->noiseEval) {
		this->deleteIsolatedNodeNoise();
	}
	else {
		this->deleteIsolatedNodeDyn();
	}

}

/**
 * function which delete separated node got after grouping or some processing, and new numbering
 *
 *	\ingroup T11
 *  \author Loic Cloatre
 *  \author Nicolas Simon TODO a re travailler sur cet algo
 *  \date 08/07/2009
 */

void Graph::deleteIsolatedNodeNoise() {
	NoeudGraph *noeudIsoleEnCours = NULL;
	NoeudGraph *noeudInit = this->getElementGf(0);
	NoeudGraph *noeudFin = this->getElementGf(1);
	NodeGraphContainer::iterator it;

	for (int i = 2; i < this->getNbNode(); i++) //on regarde chaque noeud du graphe
	{

			/*cout<<"Numero de noeud courant : "<<this->getElementGf(i)->getNumero()<<endl;
			cout<<"Nombre Pred : "<<this->getElementGf(i)->getNbPred()<<endl;
			cout<<"Nombre Succ : "<<this->getElementGf(i)->getNbSucc()<<endl<<endl;
*/

		if (this->getElementGf(i)->isNodeIsolate()) //on teste l'element isole
		{
			noeudIsoleEnCours = this->getElementGf(i);
			//on supprime le noeud
			if (noeudIsoleEnCours->getTypeNodeGraph() == GFD) {
				noeudIsoleEnCours->removeEdge(noeudFin);
				noeudInit->removeEdge(noeudIsoleEnCours);
				this->deleteGraphNode(noeudIsoleEnCours);
				this->NumNode--; //on decremente le numero du prochain noeud
			}
			else if (noeudIsoleEnCours->getTypeNodeGraph() == GFL) {

				if (((NoeudGFL *) noeudIsoleEnCours)->getTypeNoeudGFL() == VAR_ENTREE_GFL) {
					this->deleteInputGf(noeudIsoleEnCours);
				}
				else if (((NoeudGFL *) noeudIsoleEnCours)->getTypeNoeudGFL() == VAR_SORTIE_GFL) {
					this->deleteOutputGf(noeudIsoleEnCours);
				}
				else if (((NoeudGFL *) noeudIsoleEnCours)->getTypeNoeudGFL() == VAR_GFL) {
					this->deleteVarIntGf(noeudIsoleEnCours);
				}
				else {
					//DEBUG("Other node\n");
				}

				noeudIsoleEnCours->removeEdge(noeudFin);
				noeudInit->removeEdge(noeudIsoleEnCours);
				this->deleteGraphNode(noeudIsoleEnCours);
				this->NumNode--; //on decremente le numero du prochain noeud

			}
			else {
				//DEBUG("Other Graph\n");
			}
			//on decremente
			i--;
		} //endof if(this->getElementGf(i)->isNodeIsolate())
	} //endof for(int i=0; i< this->getNbNode(); i++)

	//il faut maintenant renumeroter les noeuds et mettre a jour le numero des sources de bruit
	for (int i = 0; i < this->getNbNode(); i++) //on regarde chaque noeud du graphe
	{
		this->getElementGf(i)->setNumero(i);
	}
}

/**
 * function which delete separated node got after grouping or some processing, and new numbering
 *
 *	\ingroup T11
 *  \author Loic Cloatre 
 *  \author Nicolas Simon TODO a re travailler sur cet algo
 *  \date 08/07/2009
 */

void Graph::deleteIsolatedNodeDyn() {
	NoeudGraph *noeudIsoleEnCours = NULL;
	NoeudGraph *noeudInit = this->getElementGf(0);
	NoeudGraph *noeudFin = this->getElementGf(1);
	NodeGraphContainer::iterator it;
	bool TypeNodeGraphGFL = false;

	if (this->nbNode > 2) {
		if (this->getElementGf(2)->getTypeNodeGraph() == GFL) {
			TypeNodeGraphGFL = true; //on recupere le type de graphe
		}
	}

	for (int i = 2; i < this->getNbNode(); i++) //on regarde chaque noeud du graphe
	{
		if (this->getElementGf(i)->isNodeIsolate()) //on teste l'element isole
		{
			noeudIsoleEnCours = this->getElementGf(i);
			//on supprime le noeud
			if (noeudIsoleEnCours->getTypeNodeGraph() == GFD) {
				if (((NoeudGFD *) noeudIsoleEnCours)->getTypeNodeGFD() == DATA) {
					if (((NoeudData *) noeudIsoleEnCours)->isNodeInput() == true) {
						it = Recherche((NoeudData *) noeudIsoleEnCours, tabInput); //on vérifie qu'il appartient a la table
						//it = this->rechercheNoeudInput((NoeudData *) noeudIsoleEnCours); //on vérifie qu'il appartient a la table
						if (it != tabInput->end()) {
							this->deleteInputGf(noeudIsoleEnCours); //si oui on supprime
						}
					}
					if (((NoeudData *) noeudIsoleEnCours)->isNodeOutput() == true) {
						it = Recherche((NoeudData *) noeudIsoleEnCours, tabOutput); //on vérifie qu'il appartient a la table
						//it = this->rechercheNoeudOutput((NoeudData *) noeudIsoleEnCours); //on vérifie qu'il appartient a la table
						if (it != tabOutput->end()) {
							this->deleteOutputGf(noeudIsoleEnCours); //si oui on supprime
						}
					}
					// if(((NoeudData*)noeudIsoleEnCours)->isNodeVarInt()==true)
					if (((NoeudData *) noeudIsoleEnCours)->isNodeVar() == true) {
						it = Recherche((NoeudData *) noeudIsoleEnCours, tabVarInt); //on vérifie qu'il appartient a la table
						//it = this->rechercheNoeudVarInt((NoeudData *) noeudIsoleEnCours); //on vérifie qu'il appartient a la table
						if (it != tabVarInt->end()) {
							this->deleteVarIntGf(noeudIsoleEnCours); //si oui on supprime
						}
					}
				}
				noeudIsoleEnCours->removeSuccPred(noeudInit, noeudFin);
				this->deleteGraphNode(noeudIsoleEnCours);
				this->NumNode--; //on decremente le numero du prochain noeud

			}
			else if (noeudIsoleEnCours->getTypeNodeGraph() == GFL) {

				if (((NoeudGFL *) noeudIsoleEnCours)->getTypeNoeudGFL() == VAR_ENTREE_GFL) {
					this->deleteInputGf(noeudIsoleEnCours);
				}
				else if (((NoeudGFL *) noeudIsoleEnCours)->getTypeNoeudGFL() == VAR_SORTIE_GFL) {
					this->deleteOutputGf(noeudIsoleEnCours);
				}

				noeudIsoleEnCours->removeSuccPred(noeudInit, noeudFin);
				this->deleteGraphNode(noeudIsoleEnCours);
				this->NumNode--; //on decremente le numero du prochain noeud

			}
			//on decremente
			i--;
		} //endof if(this->getElementGf(i)->isNodeIsolate())
	} //endof for(int i=0; i< this->getNbNode(); i++)

	//il faut maintenant renumeroter les noeuds et mettre a jour le numero des sources de bruit
	for (int i = 0; i < this->getNbNode(); i++) //on regarde chaque noeud du graphe
	{
		this->getElementGf(i)->setNumero(i);
	}

	if (TypeNodeGraphGFL == true) {
		//cette methode est appelee une fois que le GFL a ete cree et les substitution realisee
		//il faut que les numeros des sources de bruits correspondent entre les fichiers generes
		//on reaffecte donc: init(0) fin(1) br1(2) br2(3) ...bri(1+i)
		int nbSrc = 0;
		for (int i = 0; i < this->getNbNode(); i++) //on regarde chaque noeud du graphe
		{
			if (this->m_logDebug_pf.is_open())
				m_logDebug_pf << TOOL_FILE_DEBUG << getElementGf(i)-> getName() << "/" << getElementGf(i)->getNumero() << endl;

			if (getElementGf(i)->getTypeNodeGraph() == GFL && ((NoeudGFL *) getElementGf(i))->getTypeNoeudGFL() == VAR_ENTREE_GFL) {
				nbSrc++;
			}
		}
		int numeroNoeudNonSrc = 2 + nbSrc; //si on a 10 src, on aura init(0) fin(1) out(2) br1(3)...br10(12) autre(13)
		int numeroNoeudSrc = 2;
		for (int i = 2; i < this->getNbNode(); i++) //on regarde chaque noeud du graphe
		{
			if (this->m_logDebug_pf.is_open())
				m_logDebug_pf << TOOL_FILE_DEBUG << getElementGf(i)-> getName() << "/" << getElementGf(i)->getNumero() << endl;

			if (((NoeudGFL *) (this->getElementGf(i)))->getTypeNoeudGFL() == VAR_ENTREE_GFL) {
				NoeudGFL *NGfl = dynamic_cast<NoeudGFL *> (getElementGf(i));
				if (NGfl) {
					this->getElementGf(i)->setNumero(numeroNoeudSrc); //on set le numero de la source
					//NGfl->print();
					if (NGfl->getRefParamSrc() && NGfl->getRefParamSrc()->getTypeNodeGFD() == DATA)
						NGfl->setNumSrc(((NoeudData*)NGfl->getRefParamSrc())->getNumSrc()); //on set le numero src
					numeroNoeudSrc++;
				}
			}
			else {
				this->getElementGf(i)->setNumero(numeroNoeudNonSrc); //on set le numero de la
				((NoeudGFL *) this->getElementGf(i))->setNumSrc(-1); //on set le numero du bruit a -1(non valide)
				numeroNoeudNonSrc++;
			}
		}

		// Tri important pour certains algo de LOIC
		// TODO a enlever ce tri qui non forcement utile et qui peut faire baisser la perfomance
		sort(tabNode->begin(), tabNode->end(), predicatOrdreNoeud);
		sort(tabInput->begin(), tabInput->end(), predicatOrdreNoeud);
		sort(tabOutput->begin(), tabOutput->end(), predicatOrdreNoeud);
		sort(tabVarInt->begin(), tabVarInt->end(), predicatOrdreNoeud);

		if (this->m_logDebug_pf.is_open()) {
			for (int i = 0; i < this->getNbNode(); i++) //on regarde chaque noeud du graphe
			{
				m_logDebug_pf << TOOL_FILE_DEBUG << getElementGf(i)-> getName() << "/" << getElementGf(i)->getNumero() << endl;
			}
		}
	}

}

/** Performs a prefix exploration of the graph.
 Starting from END node, each node is passed to fonctionTraitement,
 then its predecessors are passed too. The nodes are marked with 'visited'
 info if they were already explored, but you have to handle this in the fonctionTraitement
 (you may either ignore these nodes or do something special for them.)

 \param fonctionTraitement function to run on the nodes. This function is passed a reference to the node to handle
 \param noeudInitial the node to start with. Usually this is the END node of the graph, unless you are exploring only a subgraph.
 */
void Graph::parcoursPrefixe(TraitementNoeud & fonctionTraitement, NoeudGraph & noeudInitial) {
	if (noeudInitial.getEtat() != VISITE)
		noeudInitial.setEtat(ENTRAITEMENT);

	fonctionTraitement(noeudInitial);

	for (int i = 0; i < noeudInitial.getNbPred(); i++) {
		parcoursPrefixe(fonctionTraitement, *noeudInitial.getElementPred(i));
	}
	noeudInitial.setEtat(VISITE);
}



/**
 *	Algorithme de Dijstra qui permet de connaitre les plus court chemins entre un noeud précis et tout les autres
 *
 *
 *	\param input : Noeud avec lequel on veut connaitre les plus court chemins
 *	\return En sortie, nous obtenons un tableau de pair<NoeudGraph*, int>. l'indice du tableau représente le numero des noeuds. Le premier élément de la pair représente l'élement précedent du noeud sur le plus court chemin et le second réprésente la longeur totale du chemin. Si il n'y a pas de chemin, il n'y a pas de noeud précédent donc celui sera a NULL
 *	\author Nicolas Simon
 *	\date 10/06/11
 *
 */

pair<NoeudGraph*, int>* Graph::algoDeDijkstra(NoeudGraph* input){
	priority_queue< int, vector<int> , greater<int> > mypq; // Queue de priorité implémentant un min heap
	NoeudGraph* noeudSuccCourant;
	NoeudGraph* noeudCourant;
	int tailleCheminCourant;
	pair<NoeudGraph*, int>* tab = new pair<NoeudGraph*, int>[this->getNbNode()]; // Tableau contenant les informations du predecesseur et de la longeur acutelle du chemin le plus court pour chaque noeud du graph

	this->resetInfoVisite();
	input->setEtat(ENTRAITEMENT);
	mypq.push(input->getNumero()) ;
	tab[input->getNumero()].second = 0;
	
	while(!mypq.empty()){
		tailleCheminCourant = tab[mypq.top()].second;
		noeudCourant = this->getElementGf(mypq.top());
		mypq.pop();
		noeudCourant->setEtat(VISITE);

		for(int i = 0; i<noeudCourant->getNbSucc() ; i++){
			noeudSuccCourant = noeudCourant->getElementSucc(i);
			if(noeudSuccCourant->getTypeNodeGraph() == FIN )
				continue;

			if(noeudSuccCourant->getEtat() == NONVISITE){
				noeudSuccCourant->setEtat(ENTRAITEMENT);
				mypq.push(noeudSuccCourant->getNumero());
				tab[noeudSuccCourant->getNumero()] = pair<NoeudGraph* , int> (noeudCourant, tailleCheminCourant + 1);
			}
			else if(noeudSuccCourant->getEtat() == ENTRAITEMENT){
				if(tailleCheminCourant + 1 < tab[noeudSuccCourant->getNumero()].second){
					tab[noeudSuccCourant->getNumero()] = pair<NoeudGraph* , int> (noeudCourant, tailleCheminCourant + 1);
				}
			}
		}
	}

	// Affichage pour vérification
	/*cout<<"nbNode : "<<this->getNbNode()<<endl<<endl;
	for(int i = 3 ; i<this->getNbNode() ; i++){
		cout<<"Numero du noeud : "<<this->getElementGf(i)->getNumero()<<endl;
		if(tab[i].first != NULL){
			cout<<"Numero du noeud pred : "<<tab[i].first->getNumero()<<endl;
			cout<<"Longueur du chemun : "<<tab[i].second<<endl<<endl;
		}
		else 
			cout<<"Pas de chemin"<<endl;
	}*/


	return tab;
}

/**
 *	Rend le plus court chemin entre deux noeuds précis sous la forme d'une liste, vide si il n'y a pas de chemin. Utilise l'algorithme de Dijkstra.
 *
 * 	\param noeudOrigine : Noeud d'origine pour la detection du chemin le plus court
 * 	\param noeudArrivee : Noeud d'arrivé pour la detection du chemun le plus court
 * 	\return Rend une liste de noeud représentant le plus court chemin, vide sinon
 *	\author Nicolas Simon
 *	\date 10/06/11
 *
 */

list<NoeudGraph*> Graph::cheminPlusCourtEntreDeuxNoeud(NoeudGraph* noeudOrigine, NoeudGraph* noeudArrivee){
	pair<NoeudGraph*, int>* cheminOrigineVersAutre;
	list<NoeudGraph*> cheminOrigineArrivee;
	NoeudGraph* noeudCourant;

	cheminOrigineVersAutre = this->algoDeDijkstra(noeudOrigine);


	if(cheminOrigineVersAutre[noeudArrivee->getNumero()].first == NULL) // Aucun chemin entre ces noeuds, on rend une liste vide
		return cheminOrigineArrivee;

	noeudCourant = noeudArrivee;
	while(noeudCourant != noeudOrigine && noeudCourant){
		noeudCourant = cheminOrigineVersAutre[noeudCourant->getNumero()].first;
		cheminOrigineArrivee.push_front(noeudCourant);
	}

	// Affichage pour vérification
	cout<<"Chemin entre "<<noeudOrigine->getNumero()<<" et "<<noeudArrivee->getNumero()<<endl;
	list<NoeudGraph*>::iterator it;
	for(it = cheminOrigineArrivee.begin() ; it != cheminOrigineArrivee.end() ; it++){
		cout<<(*it)->getNumero()<<" ";
	}
	cout<<endl;

	delete[] cheminOrigineVersAutre;
	return cheminOrigineArrivee;
}

/**
 *	Rend le plus court chemin entre un noeud et une liste de noeud sous la forme d'une map avec comme clé le noeud d'arrivée et la liste des noeuds du chemin
 *	Si il n'y a pas de chemin entre deux, noeud la liste sera vide
 * 	\param noeudOrigine : Noeud d'origine pour la detection du plus court chemin
 * 	\param listArrivee : Liste de noeud avec lequel on veut determiner un plus court chemin
 * 	\return map avec comme clé le pointeur vers le noeud d'arrivé et comme valeur la liste contenant le chemin le plus court avec le noeud d'origine. Liste vide si pas de chemin
 *	\author Nicolas Simon
 *	\date 10/06/11
 *
 */

map<NoeudGraph*, list<NoeudGraph*> > Graph::cheminPlusCourtEntreDeuxNoeud(NoeudGraph* noeudOrigine, list<NoeudGraph*> listArrivee){
	pair<NoeudGraph*, int>* cheminOrigineVersAutre;
	map<NoeudGraph*, list<NoeudGraph*> > cheminOrigineListArrivee;
	list<NoeudGraph*>::iterator itListArrivee;
	list<NoeudGraph*> temp;
	NoeudGraph* noeudCourant;
	NoeudGraph* noeudRef;

	cheminOrigineVersAutre = this->algoDeDijkstra(noeudOrigine);

	for(itListArrivee = listArrivee.begin() ; itListArrivee != listArrivee.end() ; itListArrivee++){
		noeudCourant = noeudRef = *itListArrivee;

		if(cheminOrigineVersAutre[noeudCourant->getNumero()].first == NULL){ // Aucun chemin entre ces Noeud, on ajoute une liste vide
			cheminOrigineListArrivee[noeudCourant] = temp;
		}
		else{
			while(noeudCourant != noeudOrigine){
				noeudCourant = cheminOrigineVersAutre[noeudCourant->getNumero()].first;
				temp.push_front(noeudCourant);
			}
			cheminOrigineListArrivee[noeudRef] = temp;
			temp.clear();
		}
	}


	// Affichage pour vérification
	/*map<NoeudGraph*, list<NoeudGraph*> >::iterator itMap;
	cout<<"Affichage des chemins"<<endl;
	for(itMap = cheminOrigineListArrivee.begin() ; itMap != cheminOrigineListArrivee.end() ; itMap++){
		cout<<"Chemin entre "<<noeudOrigine->getNumero()<<" et "<<(*itMap).first->getNumero()<<endl;
		list<NoeudGraph*>::iterator it;
		for(it = (*itMap).second.begin() ; it != (*itMap).second.end() ; it++){
			cout<<(*it)->getNumero()<<" ";
		}
		cout<<endl<<endl;
	}*/


	delete[] cheminOrigineVersAutre;
	return cheminOrigineListArrivee;
}

/**
 *	Determine les noeuds les plus jeune dans un ensemble de noeud commun de circuits non indépendant. Prend en parametre les chemins les plus court entre les sources vers chaque noeud commun et les noeuds communs detectés pour chaque cycle en fonction des paths
 *	
 *	\param mapChemin : Chemins les plus court entre chaque noeuds communs et les noeud sources
 *	\param vectorDesNoeudsCommun : Noeud communs detecté en fonction des circuits
 *	\return Rend les noeuds les plus jeune de chaque circuits traité en fonction des chemins les plus court fournis
 * \author Nicolas Simon
 * \date 14/06/11
 *
 */

list<NoeudGraph*> Graph::noeudPlusJeuneDansMultipleNoeudCommun(map<NoeudGraph*, list<NoeudGraph*> > mapChemin, vector<list<NoeudGraph*> > vectorDesNoeudsCommun){
	list<NoeudGraph*> noeudPlusJeune;
	list<NoeudGraph*>::iterator itListVecCourant, itListVec;
	list<NoeudGraph*>::iterator itListMap;
	NoeudGraph* noeudCourant;
	bool courantPlusJeune = true;

	for(int i = 0 ; i<(int)vectorDesNoeudsCommun.size() ; i++){ // Pour tout les paths
		itListVecCourant = vectorDesNoeudsCommun[i].begin();
		noeudCourant = *itListVecCourant;
		itListVec = vectorDesNoeudsCommun[i].begin(); // pour tout les elements commun dans le vecteur des noeuds commun
		while(itListVec != vectorDesNoeudsCommun[i].end()){
			if(noeudCourant == *itListVec){
				itListVec++;
				continue;
			}

			for(itListMap = mapChemin[noeudCourant].begin() ; itListMap != mapChemin[noeudCourant].end() ; itListMap++){ // Pour tout les éléments du chemin
				//si un autre noeud commun est présent sur le chemun entre la source et le noeud courant, il est plus jeune que le noeud courant
				if(*itListMap == *itListVec){
					noeudCourant = *itListMap;
					courantPlusJeune = false;
					break;
				}
				else{
					courantPlusJeune = true;
				}
			}
			if(!courantPlusJeune)
				itListVec = vectorDesNoeudsCommun[i].begin();
			else
				itListVec++;
		}
		noeudPlusJeune.push_back(noeudCourant);
	}
	return noeudPlusJeune;
}

/**
 *	Recherche dichotomique du noeud nodeGraph dans le tableau des noeuds tabNode du graph et rend l'iterator, si non trouvé rend end
 *	
 *	\author Nicolas Simon
 *	\date 09/01/12
 *
 */
vector<NoeudGraph*>::iterator Graph::rechercheDichotomique(NoeudGraph* nodeGraph, vector<NoeudGraph*> * tabNode){
	int idMin, idMax, idMid;
	bool trouve;
	vector<NoeudGraph*>::iterator it;

	idMin = 0;
	idMax = (int) tabNode->size()-1;
	trouve = false;

	while(!trouve && (idMin <= idMax)){
		idMid = idMin + (idMax - idMin)/2;
		if(tabNode->at(idMid)->getNumero() == nodeGraph->getNumero()){
			trouve = true;
		}
		else if(tabNode->at(idMid)->getNumero() < nodeGraph->getNumero()){
			idMin = idMid + 1;
		}
		else{
			idMax = idMid - 1;
		}	
	}

	if(trouve){
		it = tabNode->begin();
		for(int i = 0; i<idMid ; i++){
			it++;
		}
	}
	else{
		it = tabNode->end();
	}

	return it;
}

vector<NoeudGraph*>::iterator Graph::rechercheNoeud(NoeudGraph* nodeGraph){
 return rechercheDichotomique(nodeGraph, this->tabNode);
}
vector<NoeudGraph*>::iterator Graph::rechercheNoeudInput(NoeudGraph* nodeGraph){
 return rechercheDichotomique(nodeGraph, this->tabInput);
}
vector<NoeudGraph*>::iterator Graph::rechercheNoeudOutput(NoeudGraph* nodeGraph){
 return rechercheDichotomique(nodeGraph, this->tabOutput);
}
vector<NoeudGraph*>::iterator Graph::rechercheNoeudVarInt(NoeudGraph* nodeGraph){
 return rechercheDichotomique(nodeGraph, this->tabVarInt);
}


