/*
 * NoeudGFL.cc
 *
 *  Created on: 5 janv. 2009
 *      Author: cloatre
 */

#include "graph/dfg/datanode/NoeudSrcBruit.hh"
#include "graph/lfg/NoeudGFL.hh"

// === Namespaces
using namespace std;

extern ListSuccPred::iterator searchSuccPred(NoeudGraph * NGraph, ListSuccPred * Liste); /** fonction definie dans le fichier NoeudGraph.cc*/
void traitementSuppressionEdgeDynProcessInterval(NoeudGFL* noeudCible, NoeudGFL* noeudSuppr);
void traitementSuppressionEdgeDynProcessOverflow(NoeudGFL* noeudCible, NoeudGFL* noeudSuppr);

// ======================================= Constructeurs - Destructeurs
// =================================================================
// =================================================================

/**
 * Constructeur de la clase NoeudGFL
 *   \param TypeNodeGFL : 	Type de Noeud du GFL
 *   \param Numero : 		Numero du noeud
 *   \param TypeEtat: 		etat du noeud VISITE, ENTRAITEMENT ou NONVISITE
 */
NoeudGFL::NoeudGFL(TypeNoeudGFL TypeNodeGFL, int NumNoeud, TypeEtat Etat, const string & nom, string originalName) :
	NoeudGraph(GFL, NumNoeud, nom) {
	this->TypeNodeGFL = TypeNodeGFL; // Type de Noeud du GFL ( OPS, DATA, INIT, FIN)
	this->Etat = Etat; // Etat du noeud ( VISITE, ENTRAITEMENT, NONVISITE )
	this->setOriginalName(originalName);
	numSrc = 0;
}

NoeudGFL::NoeudGFL(const NoeudGFL& other):NoeudGraph(other){
	this->TypeNodeGFL = other.TypeNodeGFL;
	this->numSrc = other.numSrc;

    list<NoeudGFD*>::const_iterator it;
	for(it = other.refParamSrc.begin() ; it != other.refParamSrc.end() ; it++){
		this->refParamSrc.push_back(*it);
	}
}




/**
 * Destructeur de la clase NoeudGFL
 */
NoeudGFL::~NoeudGFL() {
	list<NoeudGFD *>::iterator it;
	for (it = refParamSrc.begin(); it != refParamSrc.end();) {
		if (*it && (*it)->isNodecloned() == true)
			delete *it++;
		else
			it++;
	}
}

NoeudGraph *NoeudGFL::clone() {
	return new NoeudGFL(*this);
}

/**
 * Renvoi le successeur de numero donne, NULL si pas trouve
 *   \param int : 	numero du noeud recherche
 *   \return NoeudGFL : reference sur le noeud trouve, NULL si pas trouve
 *  by Loic Cloatre creation le 13/02/2009
 */
NoeudGFL *NoeudGFL::getSuccFromNumber(int numeroDuNoeud) {
	NoeudGFL *noeudSuccRet = NULL;
	for (int i = 0; i < this->nbSucc; i++) {
		if (this->getElementSucc(i)->getNumero() == numeroDuNoeud) {
			noeudSuccRet = (NoeudGFL *) (this->getElementSucc(i));
			i = this->nbSucc;
		}
	}
	if (noeudSuccRet == NULL) {
		trace_error("Noeud non retrouve dans la liste des listSucc du noeud : %i", this->Numero);
		exit(1);
	}
	return noeudSuccRet;
}

/**
 * Renvoi le predecesseur de numero donne, NULL si pas trouve
 *   \param int : 	numero du noeud recherche
 *   \return NoeudGFL : reference sur le noeud trouve, NULL si pas trouve
 *  by Loic Cloatre creation le 13/02/2009
 */
NoeudGFL *NoeudGFL::getPredFromNumber(int numeroDuNoeud) {
	NoeudGFL *noeudPredRet = NULL;
	for (int i = 0; i < this->nbPred; i++) {
		if (this->getElementPred(i)->getNumero() == numeroDuNoeud) {
			noeudPredRet = (NoeudGFL *) (this->getElementPred(i));
			i = this->nbPred;
		}
	}
	if (noeudPredRet == NULL) {
		trace_error("Noeud non retrouve dans la liste des listPred du noeud : %i", this->Numero);
		exit(1);
	}
	return noeudPredRet;
}

// ======================================= Methodes de test pour savoir si le noued INIT ou FIN
// =================================================================
// =================================================================


/**
 *  typeVarFonction determinant si un noeud est une Source du graphe GFL
 * 	return booleen TRUE si le noeud est un Source
 *
 *	Un noeud est un Terminal si
 *		- il n'a pas de listPred
 *		- il a un seul predecesseur : le noeud INIT
 */
bool NoeudGFL::isNodeSource() {
	bool ret = false;
	int NblistPred = (this)->getNbPred();

	if (NblistPred == 0) {
		ret = true;
	}
	else {
		if (NblistPred == 1) {
			NoeudGFL *NGFL = (NoeudGFL *) ((this)->getElementPred(0));

			if (NGFL->getTypeNodeGraph() == INIT) // Si le noued des un noeud initiale (INIT)
			{
				ret = true;
			}
			else if (NGFL->getNumero() == 0) {
				ret = true;
				trace_warn("Noeud INIT corrompu!");
				// NGFL->setTypeNoeudGFL(INIT_GFL);
			}
		}
	}
	return ret;
}

/**
 *  Indique si le NoeudGFL est une entree (VAR_ENTREE_GFL ?)
 *		Return bool : Resultat du test
 */
bool NoeudGFL::isNodeInput() {
	if ((this)->getTypeNoeudGFL() == VAR_ENTREE_GFL)
		return true;

	else
		return false;

}

/**
 *  Indique si le NoeudGFL est une entree (VAR_ENTREE_GFL ?)
 *		Return bool : Resultat du test
 */
bool NoeudGFL::isNodeOutput() {
	if ((this)->getTypeNoeudGFL() == VAR_SORTIE_GFL)
		return true;

	else
		return false;

}

/**
 * 	Ecriture du Noeud et de ces Suc Pred
 * 		\param fic : fichier de destination des informations si le parametre est stdout la destionation est l ecran
 */
void NoeudGFL::print(FILE * fic) {
	// Ecriture des informations du Noeud
	(this)->printInfo(fic); //      NoeudData ou NoeudOps ou ...

	fprintf(fic, "\n   nbSucc = %d ; nbPred = %d \n", this->getNbSucc(), // Nombre de listSucc
	this->getNbPred()); // Numero de listPred

	// Ecriture de la liste des listSucc
	(this)->printListSucc(fic); // NoeudGraph

	// Ecriture de la liste des listPred
	(this)->printListPred(fic); // NoeudGraph
}

void NoeudGFL::printInfo(FILE * fic) {
	fprintf(fic, "Nom = \"%s\" ,numero: %d type: ", getName().data(), this->Numero);
	switch (TypeNodeGFL) {
	case PHI_GFL:
		fprintf(fic, "PHI");
		break;
	case VAR_GFL:
		fprintf(fic, "VAR");
		break;
	case VAR_ENTREE_GFL:
		fprintf(fic, "IN");
		break;
	case VAR_SORTIE_GFL:
		fprintf(fic, "OUT");
		break;
	}
}

/**
 * Methode qui regarde si le noeud courant fait partie des listPred immediats du noeud cible
 *
 *  \param NoeudGfl*			  	noeud cible
 *  \return bool				  	true si trouve
 *
 *  by Loic Cloatre creation le 21/01/2009
 */
bool NoeudGFL::noeudGflEstDansPredImmediat(NoeudGFL * noeudCible) {
	NoeudGraph *tempNoeudGraph = (NoeudGraph *) noeudCible;
	for (int i = 0; i < tempNoeudGraph->getNbPred(); i++) {
		if (getName() == noeudCible->getElementPred(i)->getName())
			return true;
	}
	return false;
}

/**
 * Methode qui regarde si le noeud cible fait partie des listPred du noeud courant
 *
 *  \param NoeudGfl*			  	noeud cibke
 *  \return bool				  	true si trouve
 *
 *  by Loic Cloatre creation le 09/06/2009
 */
bool NoeudGFL::noeudSortieEstDansPredThis(NoeudGFL * noeudCible) {
	bool ret = false;

	for (int i = 0; i < this->getNbPred(); i++) {
		if (strcmp(noeudCible->getName().c_str(), this->getElementPred(i)->getName().c_str()) == 0) {
			i = this->getNbPred(); //pour sortir de la boucle
			ret = true;
		}
	}
	return ret;
}

/**
 * typeVarFonction determinant si il y a un circuit unitaire revenant vers lui meme et le numero du successeur correspondant
 *
 *  \param   int*			indice du successeur correspondant
 *  \return  bool			true si circuit unitaire trouve
 *  by Loic Cloatre creation le 12/02/2009
 */
bool NoeudGFL::detectionCircuitUnitaire(int *succ) {
	bool ret = false;
	for (int i = 0; i < this->nbSucc; i++) {
		if (getName() == getElementSucc(i)->getName()) {
			ret = true; //on a trouve un successeur qui a le meme nom donc forcement on a un circuit unitaire
			*succ = i; //on donne l'indice
			i = this->nbSucc; //pour sortir de la boucle
		}
	}
	return ret;
}

/**
 * Rend l'edge représentant le circuit unitaire si il y a, NULL sinon
 *	
 *
 */
EdgeGFL* NoeudGFL::detectionCircuitUnitaire() {
	EdgeGFL* ret;
	for (int i = 0; i < this->nbSucc; i++) {
		if (this == this->getEdgeSucc(i)->getNodeSucc()) {
			ret = dynamic_cast<EdgeGFL*> (this->getEdgeSucc(i));
			assert(ret != NULL);
			return ret;
		}
	}
	return NULL;
}

/**
 * Methode qui regarde si le Noeud appartient au circuit
 *
 *  \param  GraphPath * liste contenant les numeros des noeuds du circuit
 *  \return bool
 *
 *  by Loic Cloatre creation le 03/02/2009
 */
bool NoeudGFL::belongsToTheCircuit(GraphPath * circuit) {
	bool ret = false;

	for (int i = 0; i < (int) circuit->pathSize(); i++) {
		if (this->getNumero() == circuit->pathItem(i)) {
			ret = true;
			i = circuit->pathSize(); //pour sortir de la boucle for
		}
	}
	return ret;
}

//==========================================================methode pour fonction de transfert


TransferFunction* NoeudGFL::calculFonctionTransfertAvecSuccesseur(EdgeGFL* edge){
	TransferFunction* res;
	EdgeGFL* edgeBoucle;	
	NoeudGFL* succ;
   
	succ = dynamic_cast<NoeudGFL*> (edge->getNodeSucc());
	assert(succ != NULL);
	edgeBoucle = succ->detectionCircuitUnitaire();

	
	
	if(edgeBoucle != NULL) // Boucle unitaire présente sur succ
		res = new TransferFunction(edge->getPseudoFonctionLineaire(), edgeBoucle->getPseudoFonctionLineaire());
	else
		res = new TransferFunction(edge->getPseudoFonctionLineaire());

	return res;
}


/**
 *   Add a EdgeGFL element between this and NGraph, the VarLinearFonc is save too
 *		\param NGraph : Head of the edge
 *		\param Mfct : VarLinearFonc link with the edge
 *
 *	by Nicolas Simon create 26/06/2010
 */
void NoeudGFL::addEdge(NoeudGraph * NGraph, PseudoLinearFunction * Mfct) {
	assert(!this->isSucc(NGraph));
	addEdgeRapide(NGraph, Mfct);
}

void NoeudGFL::addEdgeRapide(NoeudGraph * NGraph, PseudoLinearFunction * Mfct) {
	EdgeGFL *Elt = new EdgeGFL(this, NGraph, Mfct);

	this->listSucc.push_back(Elt); // Ajout du successeur NGraph à la liste de this
	this->nbSucc++;
	NGraph->getListePred()->push_back(Elt); // Ajout du predecesseur this à la liste de NGraph
	NGraph->setNbPred(NGraph->getNbPred() + 1);
}

void NoeudGFL::displayParamSrc() {
	list<NoeudGFD*>::iterator it = refParamSrc.begin();
	for (unsigned int i = 0; i < refParamSrc.size(); i++) {
		if (*it == NULL)
			cout << "Noeud ref NULL" << endl;
		cout << "Numero Param " << i << " : " << (*it)->getName().data() << endl;
		it++;
	}
}

NoeudGFD *NoeudGFL::getRefParamSrc(unsigned int Numero) const {
	if (Numero < refParamSrc.size()) {
		list<NoeudGFD *>::const_iterator it = refParamSrc.begin();
		for (unsigned int i = 0; i < Numero; i++) {
			it++;
		}
		return (*it);
	}
	else {
		return NULL;
	}
}

void NoeudGFL::setRefParamSrc(NoeudGFD* NoeudParamSrc){
		this->refParamSrc.push_back(NoeudParamSrc);
}

bool NoeudGFL::isContainRefParamSrc(NoeudGFD* nodeData){
	list<NoeudGFD*>::iterator it;
	for(it = refParamSrc.begin() ; it != refParamSrc.end() ; it++){
		if(*it == nodeData)
			return true;
	}
	return false;
}



