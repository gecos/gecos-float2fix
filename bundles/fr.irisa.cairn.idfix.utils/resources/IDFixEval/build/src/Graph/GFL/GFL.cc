/*
 * GFL.cc
 *
 *  Created on: 19 déc. 2008
 *      Author: cloatre
 */

#include <queue>
#include <utility>
#include <typeinfo>

#include "graph/dfg/datanode/NoeudSrcBruit.hh"
#include "graph/dfg/NoeudInitFin.hh"
#include "graph/dfg/operatornode/NoeudOps.hh"
#include "graph/lfg/EdgeGFL.hh"
#include "graph/lfg/GFL.hh"
#include "utils/Tool.hh"


// === typeVarFonctions extern
extern NodeGraphContainer::iterator Recherche(NoeudGraph * Noeud, NodeGraphContainer * Liste);

// ======================================= Constructeurs - Destructeurs
// ===============================================================
// ===============================================================

/**
 *  Constructeur par defaut de la classe Gfl
 */
Gfl::Gfl(bool noiseEval) :
	Graph(noiseEval) //constructeur classe Gf mere
{
}

Gfl::Gfl(list<LinearFunction *> listefonctionlineaire, bool noiseEval) :
	Graph(noiseEval) {
	list<LinearFunction *>::iterator it_list = listefonctionlineaire.begin();
	while (it_list != listefonctionlineaire.end()) {
		//(*it_list)->TriVLFParBruitCroissant(); //on trie dans l'ordre les Br
		this->addNodeFromLinearFonction(*it_list);
		it_list++;
	}
}

/**
 *  ce constructeur cree un GFL avec comme sortie le parametre donne et avec tous les listPred immediats
 *
 *  \param NoeudGFL*				sortie du Gfl a partir duquel on cree le GFL
 *
 *  by Loic Cloatre creation le 21/01/2009
 */
Gfl::Gfl(NoeudGFL * noeudDeSortie, bool noiseEval) :
	Graph(noiseEval) {
	NoeudGFL *NodeGFL;

	NoeudGFL *NodeArrivee = new NoeudGFL(noeudDeSortie->getTypeNoeudGFL(), this->NumNode, NONVISITE, noeudDeSortie->getName(), noeudDeSortie->getOriginalName());

	if (noeudDeSortie->getRefParamSrc() != NULL)
		NodeArrivee->setRefParamSrc(noeudDeSortie->getRefParamSrc());

	this->addNoeudGFL(NodeArrivee); //ajout du noeud sortie

	for (int i = 0; i < noeudDeSortie->getNbPred(); i++) {
		if (noeudDeSortie->getElementPred(i)->getNumero() > 1) //pour eviter Init/fin
		{
			NodeGFL = new NoeudGFL(dynamic_cast<NoeudGFL*> (noeudDeSortie->getElementPred(i))->getTypeNoeudGFL(), this->NumNode, NONVISITE, noeudDeSortie->getElementPred(i)->getName(), noeudDeSortie->getElementPred(i)->getOriginalName()); //on cree le noeud

			if (dynamic_cast<NoeudGFL*> (noeudDeSortie->getElementPred(i))->getRefParamSrc() != NULL)
				NodeGFL->setRefParamSrc(dynamic_cast<NoeudGFL*> (noeudDeSortie->getElementPred(i))->getRefParamSrc());

			this->addNoeudGFL(NodeGFL); //on ajoute le nouveau noeud

			Edge *tempEltSucPred = *(noeudDeSortie->getElementPred(i)->getListeSucc()->begin()); //le noeud n'a qu'un seul successeur donc le premier
			EdgeGFL *temp = (EdgeGFL *) tempEltSucPred;
		//	NodeGFL->edgeDirectedTo((NoeudGraph *) NodeArrivee, temp->getTypeVarFonctionLineaire()); //on cree l'arc bidirectionel avec le format
			NodeGFL->edgeDirectedTo((NoeudGraph *) NodeArrivee, temp->getPseudoFonctionLineaire()); //on cree l'arc bidirectionel avec le format
		} //endof if
	} //endof for

	this->liaisonsSuccPredInitFin(); // Liaisons des NoeudInit et NoeudFin avec le reste du graph GFL
}

Gfl::~Gfl() {
	if (m_logDebug_pf.is_open()) {
		m_logDebug_pf.close();
	}
}

// ======================================= Methodes d'ajout et de supression de NoeudGFL dans le Gfl


/**
 * typeVarFonction d'ajout d'un noeud NoeudGFL dans le GFL
 *
 *  \param  NoeudGFL*: noeud a ajouter
 *
 *  by Loic Cloatre 		creation le 07/01/2009
 */
void Gfl::addNoeudGFL(NoeudGFL * Noeud) {
	addNoeudGraph(Noeud); // Ajout dans le GFL (dans la liste des NOEUDS)
	if (Noeud->getTypeNoeudGFL() == VAR_ENTREE_GFL) {
		addInputGf(Noeud); //      Ajout dans le GFL (dans la liste des ENTREES)
	}
	else if (Noeud->getTypeNoeudGFL() == VAR_SORTIE_GFL) {
		addOutputGf(Noeud); //      Ajout dans le GFL (dans la liste des SORTIE)
	}
	else if (Noeud->getTypeNoeudGFL() == VAR_GFL) {
		//cout<<"liste variable intermediaire non geree dans l'ajout NoeudGFL dans le graphe"<<endl;
	}
}

/**
 * typeVarFonction d'enlevement d'un noeud NoeudGFL dans le GFL
 *
 *  \param  NoeudGFL*: noeud a enlever
 *
 *  by Loic Cloatre 		creation le 07/01/2009
 */
void Gfl::deleteNoeudGFL(NoeudGFL * Noeud) {
	trace_warn("Fonction nan implantee encore::deleteNoeudGFL");
}



void Gfl::addNodeFromLinearFonction(LinearFunction* fct){
	NoeudGFL* output;
	NoeudGFL* input;
	NoeudGFD* tempGFD;
	
	output = new NoeudGFL(VAR_SORTIE_GFL, this->getNumNode(), NONVISITE, fct->getNode()->getName(), fct->getNode()->getOriginalName()); 
	output->setRefParamSrc(fct->getNode()); // Mise en place de la liaison entre les noeudGFL et noeudGFD
	this->addNoeudGFL(output);

	for(int i = 0 ; i < fct->getNbPseudoFctLineaire() ; i++){
		tempGFD = fct->getPseudoFctLineaire(i)->getStartNode();
		input = new NoeudGFL(VAR_ENTREE_GFL, this->getNumNode(), NONVISITE, tempGFD->getName(), tempGFD->getOriginalName());
		input->setRefParamSrc(tempGFD);
		this->addNoeudGFL(input);
		input->edgeDirectedTo(output, fct->getPseudoFctLineaire(i));
	}

	this->liaisonsSuccPredInitFin(); // Liaisons des NoeudInit et NoeudFin avec le reste du graph GFL
}

/**
 *  Methodes permetant mettre a jour les types: entree/sortie ou variable
 *  by Loic Cloatre creation le 21/01/2009
 *	\TODO A refaire cet algo NS
 */
/*void Gfl::updateTypeNoeudGf() {
	NodeGraphContainer::iterator it;
	NodeGraphContainer::iterator it_test;

	it = tabNode->begin();
	++it; //pour ne pas prendre les noeuds init et fin

	for (++it; it != tabNode->end(); ++it) // parcours la liste des noeud a partie du 3eme (on ne passe pas part Init et Fin)
	{
		//cout<<"noeud teste: "<<((NoeudGFL*)(*it))->getNumero()<<endl;
		if (((NoeudGFL *) *it)->getTypeNoeudGFL() == PHI_GFL)
			continue; // Un phi reste toujours un phi

		((NoeudGFL *) (*it))->setTypeNoeudGFL(VAR_GFL); //on commence par tous les mettre en Var par defaut

		if (((NoeudGFL *) (*it))->isNodeSource()) {
			((NoeudGFL *) (*it))->setTypeNoeudGFL(VAR_ENTREE_GFL);

			it_test = Recherche((*it), tabInput); //on verifie que le noeud n'existe pas deja dans la table
			//it_test = this->rechercheNoeudInput((*it)); //on verifie que le noeud n'existe pas deja dans la table
			if (it_test == tabInput->end()) {
				this->addInputGf(((NoeudGFL *) (*it)));
			}
		}
		else if (((NoeudGFL *) (*it))->isNodeSink()) {

			((NoeudGFL *) (*it))->setTypeNoeudGFL(VAR_SORTIE_GFL);

			it_test = Recherche((*it), tabOutput); //on verifie que le noeud n'existe pas deja dans la table
			//it_test = this->rechercheNoeudOutput((*it)); //on verifie que le noeud n'existe pas deja dans la table
			if (it_test == tabOutput->end()) {
				this->addOutputGf(((NoeudGFL *) (*it)));
			}

		}
		else {
			//verification que le noeud n'est pas dans les tabEntree et sortie
			it_test = Recherche((*it), tabOutput);
			//it_test = this->rechercheNoeudOutput((*it));
			if (it_test == tabOutput->end()) {
			} //noeud pas trouve dans la liste
			else {
				it_test = tabNode->begin(); //on se place sur init
				it_test++; //on se place sur fin
				((NoeudGFL *) (*it))-> deleteEdgeDirectedTo(((NoeudGFL *) (*it_test)));
				this->deleteOutputGf(((NoeudGFL *) (*it)));
			}
			it_test = Recherche((*it), tabInput);
			//it_test = this->rechercheNoeudInput((*it));
			if (it_test == tabInput->end()) {
			} //noeud pas trouve dans la liste
			else {
				it_test = tabNode->begin(); //on se place sur init
				((NoeudGFL *) (*it))-> EnleveedgeFromOf(((NoeudGFL *) (*it_test)));
				this->deleteInputGf(((NoeudGFL *) (*it)));
			}
		}
	} //endof for(++it; it!=tabNode->end(); ++it)
}*/

/**
 * Met à jour les typesGFL de tous les noeuds contenu dans le GFL courant
 *
 * Nicolas Simon
 * 26/07/12
 */
void Gfl::updateTypeNoeudGf(){
	NoeudGFL* nGFL;

	for(int i = 2 ; i < this->getNbNode() ; i++){
		nGFL = dynamic_cast<NoeudGFL*> (this->getElementGf(i));
		if(nGFL->isNodeSource()){
			if(nGFL->getTypeNoeudGFL() != VAR_ENTREE_GFL){
				if(nGFL->getTypeNoeudGFL() == VAR_SORTIE_GFL){
					assert(this->isOutputGfContain(nGFL));
					this->deleteOutputGf(nGFL);
				}
				nGFL->setTypeNoeudGFL(VAR_ENTREE_GFL);
				assert(this->isInputGfContain(nGFL));
				this->addInputGf(nGFL);
			}
		}
		else if(nGFL->isNodeSink()){
			if(nGFL->getTypeNoeudGFL() != VAR_SORTIE_GFL){
				if(nGFL->getTypeNoeudGFL() == VAR_ENTREE_GFL){
					assert(this->isInputGfContain(nGFL));
					this->deleteInputGf(nGFL);
				}

				nGFL->setTypeNoeudGFL(VAR_SORTIE_GFL);
				assert(this->isOutputGfContain(nGFL));
				this->addOutputGf(nGFL);
			}
		}
		else{
			if(nGFL->getTypeNoeudGFL() == VAR_SORTIE_GFL){
				assert(this->isOutputGfContain(nGFL));
				this->deleteOutputGf(nGFL);
			}
			if(nGFL->getTypeNoeudGFL() == VAR_ENTREE_GFL){
				assert(this->isInputGfContain(nGFL));
				this->deleteInputGf(nGFL);
			}

			assert(nGFL->getRefParamSrc() != NULL); // Un NoeudGFL n'a aucun lien vers un NoeudGFD
			if(nGFL->getRefParamSrc()->getTypeNodeGFD() == OPS && ((NoeudOps*) nGFL->getRefParamSrc())->isPHINode()){
				assert(nGFL->getNbRefParamSrc() == 1); // Un NoeudGFL phi correspond à un seul opérateur dans le GFD
				if(nGFL->getTypeNoeudGFL() != PHI_GFL){
					nGFL->setTypeNoeudGFL(PHI_GFL);
				}
			}
			else{
				nGFL->setTypeNoeudGFL(VAR_GFL);
			}
		}
	}
}


//===========================================detection/enumeration/dementellement circuit
/**
 *  Methode qui va choisir un noeud a substituer en fonction de l'histogramme et de sa non dependance avec d'autre circuits
 *
 *  \param  GraphPath *			la liste des circuits a regarder
 *  \param  vector<int>*			numero des noeuds
 *  \param  vector<int> *			nombre de fois qu'apparait le noeud
 *  \return int						noeud choisi
 *
 *  by Loic Cloatre creation le 26/03/2009
 */
int Gfl::choixNoeudASubstituerIndependant(GraphPath * listeDesCircuit, vector<int>*numeroNoeud, vector<int>*nbOccurence) {
	//un circuit est ici dit independant si en scrutant tous les listPred des noeuds de ce circuit,
	//aucun ne fait partie d'un autre circuit different(cad circuit different n'ont aucuns noeuds en commun

	int ret = 0;
	int nbMaxOccurence = 0;
	bool noeudChoisi = false;
	bool dependanceTrouvee = false;
	vector<int>::iterator itVectNumeroNoeud; //iterateur utilise pour determiner le max
	vector<int>::iterator itVectNbOccurence; //iterateur utilise pour determiner le max
	GraphPath *cheminCircuitTemp = NULL;
	GraphPath *cheminCircuitSansNoeudCommun = NULL;
	GraphPath *circuitAnalyseEnCours = NULL;

	//recherche du max du vecteur nbOccurence associe au numero de ce noeud
	itVectNumeroNoeud = numeroNoeud->begin(); //on se place eu debut du vecteur
	itVectNbOccurence = nbOccurence->begin(); //on se place eu debut du vecteur
	ret = -1; //initialisation, le numero max est le premier
	nbMaxOccurence = -1; //initialisation, le nombre d'occurance max est celui du premier


	while (itVectNbOccurence != nbOccurence->end()) //boucle pour tester toutes les valeurs, les tailles des deux vecteurs sont identiques
	{
		//===================on va regarder l'independance de CHAQUE circuit contenant le numero===========//

		cheminCircuitTemp = new GraphPath(*listeDesCircuit);
		GraphPath *debutTemp = cheminCircuitTemp;

		while (cheminCircuitTemp != NULL) {
			if (cheminCircuitTemp->pathSize() > 1) //on ne regarde pas les circuits unitaires
			{
				if (cheminCircuitTemp->Appartient((unsigned int) *itVectNumeroNoeud) == true) {
					circuitAnalyseEnCours = new GraphPath(*cheminCircuitTemp); //on n'utilise que le premier path
					cheminCircuitSansNoeudCommun = new GraphPath(*listeDesCircuit);
					GraphPath *debut = cheminCircuitSansNoeudCommun;

					while ((cheminCircuitSansNoeudCommun != NULL) && (noeudChoisi == false)) //on va regarder tous les circuits sans points communs
					{
						if (((circuitAnalyseEnCours->CircuitSansPointCommunAvec(cheminCircuitSansNoeudCommun) == true) //les circuits sont completements differents
								&& (cheminCircuitSansNoeudCommun->pathSize() > 1)) //on ne regarde pas les circuits unitaires
						) {
							//on a deux circuits sans points communs
							//on va vérifier que le circuitSansNoeudCommun ne fait pas
							//parti des listPred du circuit en cours
							//pour chaque noeud du circuit en cours on teste ses pred avec le circuitSansNoeudCommun

							for (int ii = 0; ii < (int) circuitAnalyseEnCours->pathSize(); ii++) {
								NoeudGFL *noeudTeste = (NoeudGFL *) this-> getElementGf(circuitAnalyseEnCours->pathItem(ii));

								for (int jj = 0; jj < noeudTeste->getNbPred(); jj++) {
									int numeroPred = noeudTeste->getElementPred(jj)->getNumero();
									//cout<<","<<numeroPred;

									if ((numeroPred > 1) //pour eviter init et fin
											&& cheminCircuitSansNoeudCommun->Appartient(numeroPred)) {
										dependanceTrouvee = true;
										jj = noeudTeste->getNbPred(); //pour sortir de la boucle
										ii = circuitAnalyseEnCours->pathSize(); //pour sortir de la boucle
									}
								}
							} //endof for(int ii=0; ii<circuitAnalyseEnCours
						}
						else {
							//le numero est contenu dans les deux circuits
						} //endof if(circuitAnalyseEnCours->CircuitSansPointCommunAvec

						cheminCircuitSansNoeudCommun = cheminCircuitSansNoeudCommun->Suiv;
					} //endof while((cheminCircuitSansNoeudCommun->Suiv!=NULL

					delete debut;
					delete circuitAnalyseEnCours;
				} //endof if(cheminCircuitTemp->Appartient
			} //endof if(cheminCircuitTemp->Path.size()>2)

			cheminCircuitTemp = cheminCircuitTemp->Suiv;

		} //endof while((cheminCircuitTemp->Suiv!=NULL)

		delete debutTemp;

		if (dependanceTrouvee == false) //on a trouve un bon noeud a substituer
		{
			//cout<<"debug lcl: ret temporaire"<<nbMaxOccurence<<endl;
			if (*itVectNbOccurence > nbMaxOccurence) //
			{
				ret = *itVectNumeroNoeud; //on sauvegarde le numero du noeud correspondant
				nbMaxOccurence = *itVectNbOccurence; //mise a jour du max
			}
		}
		else {
			//ca veut dire que un des noeuds du cheminCircuitSansNoeudCommun fait partie des listPred du circuit en cours
		}
		dependanceTrouvee = false;

		itVectNumeroNoeud++; //incrementation des iterateurs
		itVectNbOccurence++;
	} //endof while(itVectNbOccurence!=nbOccurence->end())
	ret = listeDesCircuit->searchFirstNodeCommun(ret); //on verifie que le noeud propose est le premier noeud commun des circuits
	delete cheminCircuitTemp;

	return ret;
}

bool predicatOrdreNoeud(NoeudGraph * elt1, NoeudGraph * elt2);
// définie dans Graph.cc


void Gfl::deleteIsolatedNodeNoise() {
	Graph::deleteIsolatedNodeNoise();

	//cette methode est appelee une fois que le GFL a ete cree et les substitution realisee
	//il faut que les numeros des sources de bruits correspondent entre les fichiers generes
	//on reaffecte donc: init(0) fin(1) br1(2) br2(3) ...bri(1+i) autre(2+i) ...

	int nbSrcBruit = 0;
	for (int i = 2; i < this->getNbNode(); i++) //on regarde chaque noeud du graphe
	{
		if (typeid(*getElementGf(i)) == typeid(NoeudInit) || typeid(*getElementGf(i)) == typeid(NoeudFin)) {
			// Pour init et fin on fait rien (normalement)
			// Il semblerait que parfois un noeud se retrouve AVANt init et fin dans le graph ?
			trace_error("WRONG NODE NUMBERING!");
			print();
			exit(-1);
		}

		if ((dynamic_cast<NoeudGFL *> (this->getElementGf(i)))->getTypeNoeudGFL() == VAR_ENTREE_GFL) {
			nbSrcBruit++;
		}
	}

	if (nbSrcBruit != nbInput){
		trace_error("Graph structure is wrong : count of inputs does not match (%d in list but found %d in graph)!", nbSrcBruit, nbInput);
		exit(-1);
	}

	int numeroNoeudNonBruit = 2 + nbSrcBruit;
	// si on a 10 src de bruit,on aura init(0) fin(1) br1(2)...br10(11) autre(12)
	// on calcule le numéro de ce premier 'autre'.
	for (int i = 2; i < this->getNbNode(); i++) //on regarde chaque noeud du graphe
	{
		string tempName = this->getElementGf(i)->getName();
		if (tempName.find(NAME_NOISE_NODE) == 0 && ((NoeudGFL *) (this->getElementGf(i)))->getTypeNoeudGFL() == VAR_ENTREE_GFL) {
			// On récupère le numéro à partir du nom en enlevant "Br".
			tempName.erase(0, strlen(NAME_NOISE_NODE));

			int numeroDuBruitEntier = -1;
			stringstream numeroBruitString;
			numeroBruitString << tempName;
			numeroBruitString >> numeroDuBruitEntier;

			if (!numeroBruitString.goodbit) {
				// Si la conversion marche pas, on ne change pas le numéro du noeud.
				this->getElementGf(i)->setNumero(numeroDuBruitEntier + 1); //on set le numero de la source
				((NoeudGFL *) this->getElementGf(i))->setNumSrc(numeroDuBruitEntier); //on set le numero du bruit
			}
			else {
				trace_error("Erreur de conversion");
				assert(false);
			}
		}
		else {
			this->getElementGf(i)->setNumero(numeroNoeudNonBruit); //on set le numero de la
			((NoeudGFL *) this->getElementGf(i))->setNumSrc(-1); //on set le numero du bruit a -1(non valide)
			numeroNoeudNonBruit++;
		}
	}

	
	// Tri important pour certains algo de LOIC
	// TODO a enlever ce tri qui non forcement utile et qui peut faire baisser la perfomance
	sort(tabNode->begin(), tabNode->end(), predicatOrdreNoeud);
	sort(tabInput->begin(), tabInput->end(), predicatOrdreNoeud);
	sort(tabOutput->begin(), tabOutput->end(), predicatOrdreNoeud);
	sort(tabVarInt->begin(), tabVarInt->end(), predicatOrdreNoeud);
}

/*
 *	A partir de la structure de la liste de noeud par rapport aux path dépendant (qui ont au moins un noeud commun), determine la liste des noeud a substituer grâce a la caractèristique du noeud le plus jeune
 *		- Determination des chemins les plus court entre l'ensemble des noeuds communs
 *		- Determination du noeud le plus jeune via les chemins 
 *	
 *	Rend la liste des noeud a substituer
 *
 * Nicolas Simon
 * 15/06/11
 */

list<NoeudGraph*> Gfl::determinationNoeudsASubstituer(vector<list<NoeudGraph*> > vecListNode){
	list<NoeudGraph*> listTempNGraph;
	list<NoeudGraph*> noeudSansChemin;
	list<NoeudGraph*>::iterator itVecListNode;
	map<NoeudGraph*, list<NoeudGraph*> > mapRef;
	map<NoeudGraph*, list<NoeudGraph*> > mapChemin;
	map<NoeudGraph*, list<NoeudGraph*> >::iterator itMap;
	NoeudGFL* input = NULL;
	NoeudSrcBruit* brInput;

	// On doit changer la stucture de donnée pour avant de determiner les chemins
	for(int i = 0; i<(int)vecListNode.size() ; i++){
		for(itVecListNode = vecListNode[i].begin() ; itVecListNode != vecListNode[i].end() ; itVecListNode++)
			listTempNGraph.push_back(*itVecListNode);
	}
	listTempNGraph.sort();
	listTempNGraph.unique();

	// Affichage
/*	list<NoeudGraph*>::iterator itListTempNGraph;
	for(itListTempNGraph = listTempNGraph.begin() ; itListTempNGraph != listTempNGraph.end() ; itListTempNGraph++){
	  cout<<(*itListTempNGraph)->getNumero()<<" ";
	  }
	  cout<<endl;*/

	
	// Choix de l'entrée coté Bruit car toutes les sources de bruit ne sont pas associé à une véritable source
	if(this->getNoiseEval()){
		for(int i = 0 ; i<this->getNbInput() ; i++){
			input = (NoeudGFL*) this->getInputGf(i);
			brInput = dynamic_cast<NoeudSrcBruit*> (input->getRefParamSrc());
			assert(brInput != NULL);
			if(brInput->getNoiseOfSource())
				break;
		}	
	}
	else
		input = (NoeudGFL*) this->getInputGf(0);

	// Init de la map de reférence
	mapRef = this->cheminPlusCourtEntreDeuxNoeud(input, listTempNGraph);
	for(itMap = mapRef.begin()  ; itMap != mapRef.end() ; itMap++){
		if((*itMap).second.empty()){
			noeudSansChemin.push_back((*itMap).first);
		}
	}
	
	// Si il manque des chemins, il faut completer la mapRef en determinant les chemins manquant via les autres entrées
	// Cette partie permet de gerer les chemins entre les nids de boucle ayant des chemins inaccessible via certaines entrées
	// Avant de detecter les noeuds les plus jeunes, nous devons avoir un chemin pour chaque noeud commun
	for(int i = 1; i<this->getNbInput() && !noeudSansChemin.empty() ; i++){
		if(this->getNoiseEval() ){
			input = (NoeudGFL*) this->getInputGf(i);
			brInput = dynamic_cast<NoeudSrcBruit*> (input->getRefParamSrc());
			assert(brInput != NULL);
			if(!brInput->getNoiseOfSource())
				continue;
		}	

		mapChemin = this->cheminPlusCourtEntreDeuxNoeud(this->getInputGf(i), noeudSansChemin);
		for(itMap = mapChemin.begin() ; itMap != mapChemin.end() ; itMap++){
			mapRef[(*itMap).first] = (*itMap).second;
		}

		noeudSansChemin.clear();
		for(itMap = mapRef.begin()  ; itMap != mapRef.end() ; itMap++){
			if((*itMap).second.empty()){
				noeudSansChemin.push_back((*itMap).first);
			}
		}
	}
	assert(noeudSansChemin.empty()); // Au moins un chemin est vide et donc un point commun n'est pas relié a une source alors qu'on les a toutes faites 

	listTempNGraph = this->noeudPlusJeuneDansMultipleNoeudCommun(mapRef, vecListNode);
	listTempNGraph.sort();
	listTempNGraph.unique();	

	return listTempNGraph;
}

//=======================================//=======================================//
//                               Regroupement des GFL                             //
//=======================================//=======================================//
/**
 *
 * Fonction permettant d'obtenir à partir d'un GFL ayant de multiple sous graph dû à sa création à partir des fonctions linéaire obtenu du GFD un nouveau GFL n'ayant aucun sous graph.
 *
 *	\author Nicolas Simon
 *	\date 27/01/12
 *	\return un nouveau graph sans sous graph créé à partir d'un GFL ayant des sous graph
/ *
 */
Gfl* Gfl::mergeOfGFL(){
	NoeudGFL *noeudSortieGflEnCours = NULL;
	NoeudGFL *nodeOuput;

	bool noeudRetrouveDansAutreGraphe = false;
	Gfl *gflRetRegroupe, *petitGflAFusionner;

	//le Gfl en cours est compose de differents petits Gfl, on va chercher celui qui contient la ou une des sortie globale à partir de laquelle nous allons reconstruire un graph
	for (int i = 0; i < this->nbOutput; i++) {
		noeudSortieGflEnCours = (NoeudGFL *) this->getOutputGf(i);

		for (int j = i + 1; j < this->nbOutput && !noeudRetrouveDansAutreGraphe; j++) {
			noeudRetrouveDansAutreGraphe = noeudSortieGflEnCours->noeudGflEstDansPredImmediat((NoeudGFL *) this->getOutputGf(j));
		}
		if (!noeudRetrouveDansAutreGraphe) { // le noeudSortieGflEnCours n'est dans aucun autre sous graphe, il est donc la ou une des sortie globale du système
			break;
		}
	}

	gflRetRegroupe = new Gfl(noeudSortieGflEnCours, this->getNoiseEval());
	if(noeudSortieGflEnCours == NULL){
	   trace_error("Aucun noeud output présent dans la graph");
	   exit(0);
	}

	for(int i = 0 ; i < this->getNbOutput() ; i++){
		nodeOuput = dynamic_cast<NoeudGFL*> (this->getOutputGf(i));
		if(noeudSortieGflEnCours == nodeOuput)
			continue;

        petitGflAFusionner = new Gfl(nodeOuput, this->getNoiseEval());
        gflRetRegroupe->mergeBetween2GFL(petitGflAFusionner);
    }

#if SAVE_GRAPH
	if (this->getNoiseEval())
		gflRetRegroupe->saveGf("T23.GFL.A_merge");
	else
		gflRetRegroupe->saveGf("T14.GFL.A_merge");
#endif


	gflRetRegroupe->liaisonsSuccPredInitFin();
#if SAVE_GRAPH
	if(this->getNoiseEval())
		gflRetRegroupe->saveGf("T23.GFL.B_merge");
	else
		gflRetRegroupe->saveGf("T14.GFL.B_merge");
#endif

	gflRetRegroupe->updateTypeNoeudGf(); //on remet bien les parametres en ENTREE/SORTIE ou VAR
#if SAVE_GRAPH
	if (this->getNoiseEval())
		gflRetRegroupe->saveGf("T23.GFL.C_merge");
	else
		gflRetRegroupe->saveGf("T14.GFL.C_merge");
#endif

	gflRetRegroupe->deleteIsolatedNode();
#if SAVE_GRAPH
	if (this->getNoiseEval())
		gflRetRegroupe->saveGf("T23.GFL.D_merge");
	else
		gflRetRegroupe->saveGf("T14.GFL.D_merge");
#endif

	gflRetRegroupe->liaisonsSuccPredInitFin();
#if SAVE_GRAPH
	if (this->getNoiseEval())
		gflRetRegroupe->saveGf("T23.GFL.E_merge");
	else
		gflRetRegroupe->saveGf("T14.GFL.E_merge");
#endif

	return gflRetRegroupe;
}


/**
 *	Permet de merge 2 graph de type GFL ensemble. petitGflAFusionner est merge dans this
 *
 *	\author Nicolas Simon
 *	\date 27/01/12
 *
 */
void Gfl::mergeBetween2GFL(Gfl* petitGflAFusionner){
	NoeudGFL *noeudArrivee, *noeudArriveePetitGfl, *noeudDepart, *noeudDepartPetitGfl;
	NoeudGFD* nodeGFDRef;
	PseudoLinearFunction *newPsdFct, *psdFctPetitGfl, *psdFctThis;
	EdgeGFL *edgeDepartArrive, *edgeDepartArrivePetitGfl;
	string temp;
	int nbSucc;

	// Nous commencons par copier l'unique sortie du petitGflAFusionner dans le this
	assert(petitGflAFusionner->getNbOutput() == 1);
	noeudArriveePetitGfl = dynamic_cast<NoeudGFL*> (petitGflAFusionner->getOutputGf(0));
	assert(noeudArriveePetitGfl != NULL);
	if(!this->isGfContientNoeud(noeudArriveePetitGfl)){
		noeudArrivee = new NoeudGFL(*noeudArriveePetitGfl);
		noeudArrivee->setNumero(this->NumNode);
		this->addNoeudGFL(noeudArrivee);
	}
	else{ // Meme si nous créons pas de nouveau noeud vu que celui ci est déjà présent, nous devons merger les informations représentant le lien entre GFL/GFD (nécessaire pour transmettre la dynamique)
		temp = noeudArriveePetitGfl->getName();
		noeudArrivee = dynamic_cast<NoeudGFL*> (this->GfRechercheNoeud( &temp));
		if(noeudArrivee->getTypeNoeudGFL() == VAR_ENTREE_GFL){
			noeudArrivee->setTypeNoeudGFL(VAR_GFL);
			NoeudGraph *node;
			for(int i = 0 ; i < noeudArrivee->getNbPred() ; i++){
				node = noeudArrivee->getElementPred(i);
				if(node->getTypeNodeGraph() == INIT){
					node->deleteEdgeDirectedTo(noeudArrivee);
					this->deleteInputGf(noeudArrivee);
					break;
				}
			}
		}

		for(int j = 0 ; j < noeudArriveePetitGfl->getNbRefParamSrc() ; j++){
			nodeGFDRef = noeudArriveePetitGfl->getRefParamSrc(j);
			if(!noeudArrivee->isContainRefParamSrc(nodeGFDRef)){ // Ceci permet de ne pas produire des doublons de liens
				noeudArrivee->setRefParamSrc(nodeGFDRef);
			}
		}
	}

	// Cette partie est l'ajout des autre noeuds du petitGflAFusionner qui sont forcement des entrée du graph (il n'y pas de noeud intermédiaire dans un sous GFL) et les liens nécessaire
	for( int i = 0 ; i < petitGflAFusionner->getNbInput() ; i++){
		noeudDepartPetitGfl = dynamic_cast<NoeudGFL*> (petitGflAFusionner->getInputGf(i));
		assert(noeudDepartPetitGfl != NULL);

		if( !(noeudDepartPetitGfl->getNbSucc() == 1 && noeudDepartPetitGfl->getElementSucc(0) == noeudArriveePetitGfl)){
				trace_error("Problème de construction des petits Gfl\n");
				exit(0);
		}

		if(!this->isGfContientNoeud(noeudDepartPetitGfl)){
			noeudDepart = new NoeudGFL(*noeudDepartPetitGfl);
			noeudDepart->setNumero(this->NumNode);
			this->addNoeudGFL(noeudDepart);
		}
		else{ // Meme si nous créons pas de nouveau noeud vu que celui ci est déjà présent, nous devons merger les informations représentant le lien entre GFL/GFD (nécessaire pour transmettre la dynamique)
			temp = noeudDepartPetitGfl->getName();
			noeudDepart = dynamic_cast<NoeudGFL*> (this->GfRechercheNoeud( &temp));
			for( int j = 0 ; j < noeudDepartPetitGfl->getNbRefParamSrc() ; j++){
				nodeGFDRef = noeudDepartPetitGfl->getRefParamSrc(j);
				if(!noeudDepart->isContainRefParamSrc(nodeGFDRef)){
					noeudDepart->setRefParamSrc(nodeGFDRef);
				}
			}
		}

		// Après l'ajout du noeud, nous nous occupons des arcs et de leur PseudoFonctionLineaire
		edgeDepartArrivePetitGfl = dynamic_cast<EdgeGFL*> (noeudDepartPetitGfl->getEdgeSucc(0));
		assert(edgeDepartArrivePetitGfl != NULL);

		if(noeudDepart->isSucc(noeudArrivee)){ // il y a deja un arc de créer, il faut donc fusionner la PseudoFonctionLineaire obtenu via l'arc du petitGflAFusionner à this sans créer de un nouvel arc
			psdFctPetitGfl = edgeDepartArrivePetitGfl->getPseudoFonctionLineaire();
			nbSucc = noeudDepart->searchSucc(noeudArrivee);
			if(nbSucc<0){
				trace_error("L'arc cherché n'a pas été trouvé\n");
				exit(0);
			}

			edgeDepartArrive = dynamic_cast<EdgeGFL*> (noeudDepart->getEdgeSucc(nbSucc));
			psdFctThis = edgeDepartArrive->getPseudoFonctionLineaire();
			*psdFctThis += *psdFctPetitGfl;
		}
		else{ // Il n'y a pas déjà d'arc, nous créons donc un nouvel arc avec une copie de la PseudoFonctionLineaire
			newPsdFct = new PseudoLinearFunction(*(edgeDepartArrivePetitGfl->getPseudoFonctionLineaire()));
			noeudDepart->edgeDirectedTo(noeudArrivee, newPsdFct);
		}
	}
}

