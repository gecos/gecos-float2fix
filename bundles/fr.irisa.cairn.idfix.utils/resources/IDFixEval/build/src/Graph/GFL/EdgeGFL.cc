/**
 *  \file EdgeGFD.cc
 *  \author Nicolas Simon
 *  \date   25.06.2010
 *  \brief Class which mean edges of GFL
 */

// === Bibliothèques .hh associées
#include "graph/lfg/EdgeGFL.hh"

// ======================================= Constructeurs - Destructeurs
EdgeGFL::EdgeGFL(NoeudGraph * nodePred, NoeudGraph * nodeSucc, PseudoLinearFunction * Mfct) :
	Edge(nodePred, nodeSucc, -1) {
	if (Mfct == NULL)
		this->m_fct = new PseudoLinearFunction(NULL);
	else
		this->m_fct = new PseudoLinearFunction(*Mfct);
}

EdgeGFL::~EdgeGFL() {
	delete m_fct;
}
