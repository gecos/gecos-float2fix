/**
 *  \file main.cc
 *  \author Daniel Menard
 *  \date   31.01.2002
 *  \brief Classe principal
 */

#include "noise/NoiseFunctionGeneration.hh"
#include "noise/T1_Group.hh"
#include "noise/T2_Group.hh"
#include "noise/T3_Group.hh"
#include "utils/graph/cycledismantling/CycleDismantling.hh"
#include "utils/Tool.hh"
#include "backend/NoisePowerExpressionGeneration.hh"
#include "dynamic/DynamicRangeDetermination.hh"

void usage(char *name);
void optionsExecution(int argc, char **argv);
void DynamicEvaluationPart(Gfd *gfd);
void NoiseEvaluationPart(Gfd *gfd);
void CycleAndLtiDetectionAndInitialization(Gfd *gfd);

/**
 *  TODO Modify the xml reader and just read one time the graph. Copy use copy system to copy GFD for run the "all" eval type
 */

int main(int argc, char **argv) {
	optionsExecution(argc, argv);
	Chrono chrono;

	if(ProgParameters::getEvalType() == "dynamic"){
        chrono.start();
        
        Gfd *gfd = new Gfd(false);
        gfd->lectureFluxXML(ProgParameters::getXMLInputFile().data(), ProgParameters::getDTDInputFile().data()); // TODO remake this
        gfd->deleteIsolatedNode(); // TODO put at the end of the xml reading
        gfd->resolveConstantSubTree();

        CycleAndLtiDetectionAndInitialization(gfd);
        DynamicEvaluationPart(gfd);
        delete gfd;

        chrono.stop();
        RuntimeParameters::timing("Total dynamic part timing", &chrono);
        RuntimeParameters::timing("End of dynamic part");
    }
    else if (ProgParameters::getEvalType() == "noise") {
        chrono.start();
        
        Gfd *gfd = new Gfd(true);
        gfd->lectureFluxXML(ProgParameters::getXMLInputFile().data(), ProgParameters::getDTDInputFile().data()); // TODO remake this
        gfd->deleteIsolatedNode(); // TODO put at the end of the xml reading
        gfd->resolveConstantSubTree();
        
        CycleAndLtiDetectionAndInitialization(gfd);
        NoiseEvaluationPart(gfd);
        delete gfd;
        
        chrono.stop();
        RuntimeParameters::timing("Total noise evaluation part timing", &chrono);
        RuntimeParameters::timing("End of evaluation part");
    }
    else if (ProgParameters::getEvalType() == "all"){
        chrono.start();
        Gfd *dynamicGfd = new Gfd(false);
        dynamicGfd->lectureFluxXML(ProgParameters::getXMLInputFile().data(), ProgParameters::getDTDInputFile().data()); // TODO remake this
        dynamicGfd->deleteIsolatedNode(); // TODO put at the end of the xml reading
        dynamicGfd->resolveConstantSubTree();

        CycleAndLtiDetectionAndInitialization(dynamicGfd);
        DynamicEvaluationPart(dynamicGfd);
        
        delete dynamicGfd;
        chrono.stop();
        RuntimeParameters::timing("Total dynamic part timing", &chrono);
        RuntimeParameters::timing("End of dynamic part");
        
        chrono.start();
        Gfd *noiseGfd = new Gfd(true);
        noiseGfd->lectureFluxXML(ProgParameters::getXMLInputFile().data(), ProgParameters::getDTDInputFile().data()); // TODO remake this
        noiseGfd->deleteIsolatedNode(); // TODO put at the end of the xml reading
        noiseGfd->resolveConstantSubTree();
        
        NoiseEvaluationPart(noiseGfd);
        delete noiseGfd;
        chrono.stop();
        RuntimeParameters::timing("Total noise evaluation part timing", &chrono);
        RuntimeParameters::timing("End of evaluation part");
    }

	if(ProgParameters::getTimingMode()){
		// Ecriture de la balise de fermeture du xml
		*ProgParameters::getLogTiming() << "</fr.irisa.cairn.float2fix.model.logTiming:LogTiming>";
		ProgParameters::getLogTiming()->close();
		delete ProgParameters::getLogTiming();
	}
	trace_info("Fin du programme\n");
	return 0;
}

/**
 *  Detect if the GFD give as parameter contained an cycle and if it represent a LTI system or not
 *  Initialize the runtime parameters used into the flow
 *
 *  \author Nicolas Simon
 *  \date 04/06/2014
 */
void CycleAndLtiDetectionAndInitialization(Gfd *gfd){
    Chrono chrono;
    chrono.start();
    
    Gfd *copyToLtiAnalysis = gfd->copie();
    RuntimeParameters::setIsRecursiveGraph(CycleDetection(copyToLtiAnalysis));
    if(RuntimeParameters::isRecursiveGraph()){
        CycleDismantling(copyToLtiAnalysis);
    }
    RuntimeParameters::setIsLTIGraph(copyToLtiAnalysis->isLTI());
    delete copyToLtiAnalysis;

    chrono.stop();
    RuntimeParameters::timing("Cycle and Lti detection", &chrono);
}

/**
 *  Run the complete dynamic flow
 */
void DynamicEvaluationPart(Gfd *gfd){
    RuntimeParameters::setProcessingType(false);
    trace_info("Dynamic evaluation part processing\n");
    gfd->saveGf((char *) "gfd_origine");

    gfd->generateConstantVariable(); // TODO Check what is it but it is important...
    dynamic::DynamicRangeDetermination(gfd);

    trace_info("Dynamic evaluation part end\n\n");
}

/**
 *  Run the complete noise evaluation flow
 */
void NoiseEvaluationPart(Gfd *gfd){
    RuntimeParameters::setProcessingType(true);
    Gft *GftResultat;
    Chrono chronoPrecision;
    trace_info("Noise evaluation part processing\n");
    gfd->saveGf((char *) "gfd_origine");

    // Write SFG <- CDFG affectation used in the noise function
    chronoPrecision.start();
    WFPCDFGToWFPSFG(gfd);
    chronoPrecision.stop();
    RuntimeParameters::timing("ComputePb Pre Processing", &chronoPrecision);

    gfd->generateConstantVariable(); // TODO Check what is it but it is important...

    T1_SystemNoiseModelDetermination(gfd);
    GftResultat = T2_SystemPseudoTransferFunctionDetermination(gfd);
    T3_NoisePowerExpressionDetermination(GftResultat);
    NoiseFunctionGeneration(GftResultat, gfd);

    // Création de la librairie dynamique
    chronoPrecision.start();
    NoisePowerExpressionLibraryCompilation();
    chronoPrecision.stop();
    RuntimeParameters::timing("ComputePb compilation", &chronoPrecision);

    delete GftResultat;
    trace_info("Noise evaluation part end\n\n");
}


// ================================================ Fonctions
/**
 * Affichage des options d'utilisation du programme ID.Fix
 * 		\param nameExe : Nom de l'éxécutable
 */
void usage(char *name) {
	printf("\n");
	printf("usage: %s [options] [--timing] [--fixed-point-specification] [--debug]\n", name);
	printf("Required options are:\n");
	printf("\t--xml-file <filename>           : Name of the xml input file\n");
	printf("\t--output-dir <dirname>          : Name of the directory containing all Results\n");
	printf("\t                                  It contains a subdirectory Graphs/ with vcg files, and a directory Generated/ containing other files.\n");
	printf("\t                                  The root directory only contains four files: the main output file for noise evaluation (e.g. ComputePb.c),\n");
	printf("\t                                  the corresponding library (libcomputepb.so), the main output file for dynamic evaluation (e.g. fp_specification.xml\n");
	printf("\t                                  and the log file (output.log).\n");
	printf("\t--output-format [C|CJni]        : Format of the generated file output\n");
	printf("Optional option are:\n");
	printf("\t--eval-type [noise|dynamic|all] : Type of evaluation. Default: all (noise and dynamic)\n");
	printf("\t--output-file <filename>        : Name of the main output file containing the results without extension nor location. Default : ComputePb\n");
	printf("\t--resources-dir <dirname>       : Name of the directory containing Matlab functions and the sfg.dtd file to use. Default to the Resources/ directory in the repository\n");
	printf("\t--dtd-file <filename>           : Absolute name of the dtd file to use (eg: ~/sfg.dtd). Default to Resources/sfg.dtd\n");
	printf("\t--debug                         : Activate debug traces\n");
	printf("\t--timing                        : Activate timing measurement\n");
}

/**
 * \brief Gestion des arguments
 * \param argc : Nombre d'argument fournit au programme principale
 * \param argv : Tableau de chaine de caractère contenant les argument du programme principale
 * \param xml_file : Fichier XML d'entrée définissant le programme traité
 * \param dtd_file : Fichier de norme hortographique XML. Permet de vérfier si le XML est bien construit
 * \param output_file: Fichier de sortie "ComputePb"
 * \param output_dir : Répertoire de sortie du fichier de sortie
 * \param resources_dir : Répertoire Resources
 * \param eval_type : dynamic, noise ou all
 * \param output_format : Format C ou CJni
 */
void optionsExecution(int argc, char **argv) {
	bool error = false;
	int32_t arg_count = 1;
	bool option_ok;
	char *xml_file = NULL;
	char *dtd_file = NULL;
	char *output_file = NULL;// ComputePb.c par défaut
	char *output_dir = NULL; // Par défaut Output, contenant Generated (avec ComputePb.c, et Graphs contenant les fichiers vcg)
	char *resources_dir = NULL;
	char *overflow_file = NULL;
	char *eval_type = NULL;
	char *output_format = NULL;
	char *dyn_process = NULL;
	char *simulated_values_xml = NULL;
	char *thread_number = NULL;

	// Debut : Gestion des arguments (argv)
	while (arg_count < argc && !error) {
		option_ok = false;

		if (strcmp(argv[arg_count], "--xml-file") == 0 || strcmp(argv[arg_count], "-i") == 0) {
			option_ok = true;
			arg_count++;
			xml_file = argv[arg_count];
		}
		else if (strcmp(argv[arg_count], "--dtd-file") == 0) {
			option_ok = true;
			arg_count++;
			dtd_file = argv[arg_count];
		}
		else if (strcmp(argv[arg_count], "--debug") == 0) {
			option_ok = true;
			ProgParameters::setDebugMode(true);
		}
		else if (strcmp(argv[arg_count], "--output-file") == 0) {
			option_ok = true;
			arg_count++;
			output_file = argv[arg_count];
		}
		else if (strcmp(argv[arg_count], "--output-dir") == 0) {
			option_ok = true;
			arg_count++;
			output_dir = argv[arg_count];
		}
		else if (strcmp(argv[arg_count], "--eval-type") == 0) {
			option_ok = true;
			arg_count++;
			if (strcmp(argv[arg_count], "noise") != 0 && strcmp(argv[arg_count], "dynamic") != 0 && strcmp(argv[arg_count], "all") != 0) {
				fprintf(stderr, "*** Error: evaluation type is incorrect.\n");
				error = true;
			}
			else {
				eval_type = argv[arg_count];
			}
		}
		else if (strcmp(argv[arg_count], "--output-format") == 0) {
			option_ok = true;
			arg_count++;
			if (strcmp(argv[arg_count], "C") != 0 && strcmp(argv[arg_count], "CJni") != 0) {
				fprintf(stderr, "*** Error: Output format is incorrect.\n");
				error = true;
			}
			else {
				output_format = argv[arg_count];
			}
		}
		else if (strcmp(argv[arg_count], "--timing") == 0) {
			option_ok = true;
			ProgParameters::setTimingMode(true);
		}
		else if (strcmp(argv[arg_count], "--dyn-process") == 0) {
			option_ok = true;
			arg_count++;
			if(strcmp(argv[arg_count], "overflow-proba") != 0 && strcmp(argv[arg_count], "interval") != 0) {
				fprintf(stderr, "*** Error : Dynamic process is incorrect.\n");
				error = true;
			}
			else {
				dyn_process = argv[arg_count];
			}
		}
		else if (strcmp(argv[arg_count], "--overflow-file") == 0) {
			option_ok = true;
			arg_count++;
			overflow_file = argv[arg_count];
		}
		else if(strcmp(argv[arg_count], "--simulated-values-xml") == 0) {
			option_ok = true;
			arg_count++;
			simulated_values_xml = argv[arg_count];
		}
		else if(strcmp(argv[arg_count], "--correlation") == 0) {
			option_ok = true;
			ProgParameters::setCorrelationMode(true);
		}
		else if(strcmp(argv[arg_count], "--resources-dir") == 0){
			option_ok = true;
			arg_count++;
			resources_dir = argv[arg_count];
		}
		else if(strcmp(argv[arg_count], "--thread-numbers") == 0){
			option_ok = true;
			arg_count++;
			thread_number = argv[arg_count];
		}


		if (!option_ok && strcmp(argv[arg_count], "") != 0) {
			// 2e test car on peut avoir un argument correspondant à une chaine vide
			fprintf(stderr, "*** Errror: Unrecognized option %s\n", argv[arg_count]);
			usage(argv[0]);
			exit(1);
		}
		arg_count++;
	}



	if (resources_dir == NULL) {
		ProgParameters::setResourcesDirectory((char *) (get_idfix_root() + RESOURCES_DIR).data());
	}
	else {
		ProgParameters::setResourcesDirectory(resources_dir);
	}

	if (xml_file == NULL) {
		fprintf(stderr, "*** Error: .xml input filename hasn't been provided.\n");
		error = true;
	}
	else {
		ProgParameters::setXMLInputFile(xml_file);
	}

	if (dtd_file == NULL) {
		ProgParameters::setDTDInputFile((char *) (ProgParameters::getResourcesDirectory() + DTD_FILENAME).data());
	}
	else {
		ProgParameters::setDTDInputFile(dtd_file);
	}

	if (output_dir == NULL) {
		fprintf(stderr, "*** Error: output directory hasn't been provided.\n");
		error = true;
	}
	else {
		ProgParameters::setOutputDirectory(output_dir);
	}

	if (eval_type == NULL) {
		eval_type = (char *) "all";
	}
	ProgParameters::setEvalType(eval_type);

	if (output_format == NULL) {
		fprintf(stderr, "*** ERROR: Output format hasn't been specified (C or CJni)");
		exit(-1);
	}
	else {
		ProgParameters::setOutputFormat(output_format);
	}

	if(dyn_process == NULL) {
		dyn_process = (char*) "interval";
	}
	ProgParameters::setDynProcess(dyn_process);

	if(overflow_file != NULL){
		ProgParameters::setOverflowPathFile(overflow_file);
	}
	else if(ProgParameters::getDynProcess() == "overflow-proba"){
		fprintf(stderr, "*** ERROR: Fichier contenant les dynamiques nécessaire pour le mode overflow non spécifié");
		exit(-1);
	}

	if(thread_number != NULL){
		int nbThread = strtol(thread_number, NULL, 10);
		if(nbThread < 1){
			fprintf(stderr, "*** ERROR: Le nombre de thread spécifié par la commande --thread-numbers doit être >= 1");
			exit(-1);
		}
		ProgParameters::setNbThread(strtol(thread_number, NULL, 10));
	}


	if (error) {
		usage(argv[0]);
		exit(1);
	}

	string logfile = ProgParameters::getOutputDirectory() + "output.log";

	FILE *plogfile = fopen(logfile.data(), "w");
	if (!plogfile) {
		exit(1);
	}
	ProgParameters::setPLogFile(plogfile);

	if (output_file == NULL) {
		output_file = (char*) "ComputePb.c";
		trace_notice("No output filename specified : default filename is ComputePb");
	}
	else if(strcmp(output_format, "C") == 0 || strcmp(output_format, "CJni") == 0) {
		strcat(output_file,".c");
	}
	else{
		trace_error("Type de format de sortie non pris en charge");
		exit(0);
	}

	ProgParameters::setOutputFilename(output_file);
	ProgParameters::setLogFile(logfile);




	// Log du timing;
	if(ProgParameters::getTimingMode()){
		string logTimingName = ProgParameters::getOutputDirectory() + LOG_TIMING_FILENAME;
		fstream* logTiming = new fstream(logTimingName.data(), fstream::out);
		if(!logTiming->is_open()){
			trace_error("Erreur d'ouverture du fichier log du timing");
			exit(-1);
		}

		// Ecriture de l'entête du xml
		*logTiming << "<?xml version=\"1.0\" encoding=\"ASCII\"?>" <<endl;
		*logTiming << "<fr.irisa.cairn.float2fix.model.logTiming:LogTiming xmi:version=\"2.0\" xmlns:xmi=\"http://www.omg.org/XMI\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:fr.irisa.cairn.float2fix.model.logTiming=\"fr.irisa.cairn.float2fix.model.logTiming\" xsi:schemaLocation=\"fr.irisa.cairn.float2fix.model.logTiming \">" << endl;

		ProgParameters::setLogTiming(logTiming);
		ProgParameters::setPathLogTiming(logTimingName);
	}

	if(simulated_values_xml != NULL){
		ProgParameters::setSimulatedValuesPathFile(simulated_values_xml);
	}
	else{
		trace_notice("Aucun xml contenant les échantillons simulés des variables du code C");
	}

	// Début d'affichage des options
	trace_output("*** Execution options:\n");
	trace_output("   - XML input file: %s\n",xml_file);
	trace_output("   - DTD input file: %s\n",ProgParameters::getDTDInputFile().data());
	trace_output("   - Output directory: %s\n",output_dir);
	trace_output("   - Output file: %s\n",output_file);
	trace_output("   - Log file: %s\n",logfile.data());
	trace_output("   - Resources directory: %s\n",ProgParameters::getResourcesDirectory().data());
	trace_output("   - evaluation type: ");
	if (strcmp(eval_type, "all") == 0) {
		trace_output("noise & dynamic\n");
	}
	else if (strcmp(eval_type, "noise") == 0) {
		trace_output("noise\n");
	}
	else if (strcmp(eval_type, "dynamic") == 0) {
		trace_output("dynamic\n");
	}
	else {
		assert(false);
	}
	trace_output("   - Output format: ");
	if (strcmp(output_format, "C") == 0) {
		trace_output("C\n");
	}
	else if (strcmp(output_format, "CJni") == 0) {
		trace_output("CJni\n");
	}
	else if (strcmp(output_format, "C++") == 0) {
        trace_output("C++\n");
    }
	else {
		assert(false);
	}

	if (ProgParameters::getTimingMode()) {
		trace_output("   - timing Mode activated\n");
	}
	if (ProgParameters::getDebugMode()) {
		trace_output("   - Debug Mode activated\n");
	}
	if (ProgParameters::isCorrelationMode()){
		trace_output("   - Correlation Mode activated\n");
	}
}

