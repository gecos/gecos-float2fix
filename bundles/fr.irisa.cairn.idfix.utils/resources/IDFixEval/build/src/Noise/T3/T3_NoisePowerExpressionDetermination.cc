/*!
 *  \author Daniel Menard, Mohammad DIAB, Jean-Charles Naud
 *  \date   28.06.2002
 *  \version 1.0
 */
#include <string>

#include "graph/tfg/GFT.hh"
#include "noise/T3_Group.hh"
#include "utils/Tool.hh"

#ifndef OCTAVE
    #include <engine.h>
#endif

using namespace std;

static void T3_NonLTISystem(Gft* gft);
static void T3_LTISystem(Gft* gft);

void T3_NoisePowerExpressionDetermination(Gft* gft){
	if(RuntimeParameters::isLTIGraph()){
		T3_LTISystem(gft);
	}
	else{
		T3_NonLTISystem(gft);
	}
}

static void T3_LTISystem(Gft* gft){
	string pathMatFile = ProgParameters::getResourcesDirectory();
	string tempPathFichierParamFT = pathMatFile + PARAMFT_FILENAME;
	Chrono chrono;
	if(!RuntimeParameters::isRecursiveGraph()){
		chrono.start();
		trace_debug("Lancement du computeKLM\n");
		gft->computeKLM();
		trace_debug("Fin du computeKLM\n");
		chrono.stop();
		RuntimeParameters::timing("ComputeKLM processing", &chrono);

		// Pour generer lire les binaires et generer en txt la lecture
		//GftResultat->computeKLMTest();
	}
	else{
		// Génération du fichier ParamFT puis éxécution de matlab pour obtenir les gains utilisé par la suite dans la génération du ComputePb
		chrono.start();
		trace_debug("Chemin de ParamFT : %s\n", tempPathFichierParamFT.data());
		gft->genereMatFileParamFT(tempPathFichierParamFT);
		chrono.stop();
		RuntimeParameters::timing("ParamFT generation", &chrono);
		chrono.start();

		string pathMatlabFunctionSource = ProgParameters::getResourcesDirectory() + "/engine/recursive/";
		string commandMatlab = "KLM_process_binary('" + ProgParameters::getOutputGenDirectory() + "','" + NOISE_KLM_FILEMANE + "', 100)";


#ifndef OCTAVE
		trace_info("Matlab processing...\n");
		Engine *ep;
		if (!(ep = engOpen(NULL))) {
			trace_error("Can't start MATLAB engine");
			exit(EXIT_FAILURE);
		}

		/* Add path to allow access to matlab files when IDFix is run from another repository */
		/* The directory added must contain the file KLM_process.m */
		string addpathMatlabWorkspace = "addpath('" + pathMatlabFunctionSource + "')";
		trace_debug("matlab workspace path : %s\n", pathMatlabFunctionSource.data());
		engEvalString(ep, addpathMatlabWorkspace.data());
		trace_debug("matlab command: %s\n", commandMatlab.data());
		engEvalString(ep, commandMatlab.data());
		engClose(ep);

#else
		trace_info("Octave processing...\n");
		trace_debug("octave command: %s\n", commandMatlab.data());
		runOctaveCommand(pathMatlabFunctionSource, commandMatlab);
#endif

		chrono.stop();
#ifndef OCTAVE
		RuntimeParameters::timing("Matlab processing", &chrono);
#else
		RuntimeParameters::timing("Octave processing", &chrono);
#endif
	}
}

static void T3_NonLTISystem(Gft* gft){
	Chrono chrono;
	chrono.start();
	string PathParamFTNonLti;
	PathParamFTNonLti = ProgParameters::getOutputGenDirectory() + "ParamFTNonLti.m";

	trace_debug("Chemin de ParamFTNonLti : %s\n", PathParamFTNonLti.data());
	gft->genereMatFileParamFTNonLti(PathParamFTNonLti);
	chrono.stop();
	RuntimeParameters::timing("ParamFTNonLti generation", &chrono);

	//Execution de matlab
	chrono.start();
	string pathMatFile = ProgParameters::getResourcesDirectory();

	string pathMatlabFunctionSource = ProgParameters::getResourcesDirectory() + "/engine/nonLTI/";
	string commandMatlab = "noisePowerNonLTI_binary('" + ProgParameters::getOutputGenDirectory() + "','" + NOISE_KLM_FILEMANE + "')";

#ifndef OCTAVE
	trace_info("Matlab processing...\n");
	// Lancement de matlab
	Engine *ep;
	if (!(ep = engOpen(NULL))) {
		trace_error("Can't start MATLAB engine");
		exit(EXIT_FAILURE);
	}
	else {
		/* Add path to allow access to matlab files when IDFix is run from another repository */
		/* The directory added must contain the file CaculH.m */
		string pathMatlabWorkspace = "addpath('" + pathMatlabFunctionSource + "')";
		trace_debug("Matlab workspace path : %s\n", pathMatlabFunctionSource.data());
		engEvalString(ep, pathMatlabWorkspace.data());

		trace_debug("commande matlab: %s\n", commandMatlab.data());
		engEvalString(ep, commandMatlab.data());

		engClose(ep);

	}
#else
	trace_info("Octave processing...\n");
	trace_debug("octave command: %s\n", commandMatlab.data());
	runOctaveCommand(pathMatlabFunctionSource, commandMatlab);
#endif

	chrono.stop();
#ifndef OCTAVE
	RuntimeParameters::timing("Matlab processing", &chrono);
#else
	RuntimeParameters::timing("Octave processing", &chrono);
#endif
}


