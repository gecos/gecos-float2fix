/*!
 *  \author Daniel Menard, Mohammad DIAB, Jean-Charles Naud
 *  \date   28.06.2002
 *  \version 1.0
 */

#include "noise/T2_Group.hh"

static Gft* T2_NonLTISystem(Gfd* gfd);
static Gft* T2_LTISystem(Gfd* gfd);

Gft* T2_SystemPseudoTransferFunctionDetermination(Gfd* gfd){
	Gft* GftResultat;
	if(RuntimeParameters::isLTIGraph()){
		trace_info("Graph LTI - LTI mode processing\n");
		GftResultat = T2_LTISystem(gfd);
	}
	else{
		trace_info("Graph non LTI - Non LTI mode processing\n");
		GftResultat = T2_NonLTISystem(gfd);
	}

	return GftResultat;
}

static Gft* T2_LTISystem(Gfd* gfd){
	trace_output(TOOL_STEP_2_2);
	Chrono chrono;

	trace_output(TOOL_STEP_2_2_1);
	chrono.start();
	T21_CycleDismantling(gfd);
	chrono.stop();
	RuntimeParameters::timing("T21 Cycle dismantling", &chrono);

	trace_output(TOOL_STEP_2_2_2);
	chrono.start();
	Gfl* GflRegroupe = T22_ReccurentEquationDetermination(gfd);
	chrono.stop();
	RuntimeParameters::timing("T22 Recurrent Equation Determination", &chrono);

	trace_output(TOOL_STEP_2_2_3);
	chrono.start();
	T23_PartialPseudoTransferFunctionDetermination(GflRegroupe);
	chrono.stop();
	RuntimeParameters::timing("T23 PartialPseudoTransferFunctionDetermination", &chrono);

	trace_output(TOOL_STEP_2_2_4);
	chrono.start();
	Gft* GftResultat = T24_GlobalPseudoTransferFunctionDetermination(GflRegroupe);
	chrono.stop();
	RuntimeParameters::timing("T23 GlobalPseudoTransferFunctionDetermination", &chrono);

#if SAVE_GRAPH
	gfd->saveGf("T2.GFD.Z");
	GflRegroupe->saveGf("T2.GFL.Z");
	GftResultat->saveGf("T2.GFT.Z");
#endif

	delete GflRegroupe;
	return GftResultat;
}


static Gft* T2_NonLTISystem(Gfd* gfd){
	// Création des path pour les fichiers nécessaire pour la simulation
	string PathSimulatedValues, PathMatlabArithmeticOpFile;
	Chrono chrono;

	PathSimulatedValues = ProgParameters::getOutputGenDirectory() + "simulatedValues.m";

	PathMatlabArithmeticOpFile = ProgParameters::getOutputGenDirectory() + "MatlabArithmericOp.m";
	//-----------------------------------------------------------------------------

	chrono.start();
	gfd->addTypeSimuNode(); // Determination des noeuds typeSimu

#if SAVE_GRAPH
	gfd->saveGf("T21.GFD.A");
#endif

	gfd->genArithmeticOperation(PathMatlabArithmeticOpFile);

	// Test puis generation du fichier simulatedValues.txt a partir du xml donné en entrée
	if(ProgParameters::getSimulatedValuesPathFile().empty()){
		trace_error("Fichier d'échantillons des variables simulées du code C non présent pour le traitement non LTI\n");
		exit(1);
	}
	trace_debug("Création du fichier contenant les échantillons\n");
	gfd->readSimulatedNode(ProgParameters::getSimulatedValuesPathFile().data(), PathSimulatedValues);

	chrono.stop();
	RuntimeParameters::timing("Pre-computing non-lti information", &chrono);

	trace_output(TOOL_STEP_2_2_1);
	chrono.start();
	T21_CycleDismantling(gfd);
	chrono.stop();
	RuntimeParameters::timing("T21 Cycle dismantling", &chrono);

	trace_output(TOOL_STEP_2_2_2);
	chrono.start();
	Gfl* GflRegroupe = T22_ReccurentEquationDetermination(gfd);
	chrono.stop();
	RuntimeParameters::timing("T22 Recurrent Equation Determination", &chrono);

	trace_output(TOOL_STEP_2_2_3);
	chrono.start();
	T23_PartialPseudoTransferFunctionDetermination(GflRegroupe);
	chrono.stop();
	RuntimeParameters::timing("T23 PartialPseudoTransferFunctionDetermination", &chrono);

	trace_output(TOOL_STEP_2_2_4);
	chrono.start();
	Gft* GftResultat = T24_GlobalPseudoTransferFunctionDetermination(GflRegroupe);
	chrono.stop();
	RuntimeParameters::timing("T23 GlobalPseudoTransferFunctionDetermination", &chrono);

#if SAVE_GRAPH
	gfd->saveGf("T2.GFD.Z");
	GflRegroupe->saveGf("T2.GFL.Z");
	GftResultat->saveGf("T2.GFT.Z");
#endif

	delete GflRegroupe;
	return GftResultat;
}
