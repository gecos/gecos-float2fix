/*
 * GFL.cc
 *
 *  Created on: 19 déc. 2008
 *      Author: cloatre
 */

// === Bibliothèques .hh associe
#include "graph/lfg/EdgeGFL.hh"
#include "graph/lfg/GFL.hh"
#include "graph/lfg/NoeudGFL.hh"
#include "graph/tfg/GFT.hh"

#include "utils/graph/cycledismantling/GraphPath.hh"

/*
 * Methode qui permet de réduire les cycles non unitaire d'un GFL à unitaire
 *
 * - Enumeration des circuits
 * - Determination des noeuds communs a subsituer dans les cas des cycles avec un seul points commun ou des cycles avec imbriqué avec plusieurs points communs
 *
 *   Nicolas Simon 15/06/11
 *
 */

Gft* T24_GlobalPseudoTransferFunctionDetermination(Gfl* gfl){
	Gft *GrapheFoncTrans = new Gft(*gfl);
	return GrapheFoncTrans;
}
