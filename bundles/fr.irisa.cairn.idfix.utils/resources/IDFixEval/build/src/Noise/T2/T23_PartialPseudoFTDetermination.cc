/*
 * GFL.cc
 *
 *  Created on: 19 déc. 2008
 *      Author: cloatre
 */

#include "graph/lfg/EdgeGFL.hh"
#include "graph/lfg/GFL.hh"
#include "graph/lfg/NoeudGFL.hh"

#include "utils/graph/cycledismantling/CycleDismantling.hh"
#include "utils/graph/cycledismantling/GraphPath.hh"

void T23_PartialPseudoTransferFunctionDetermination(Gfl* gfl){
	// Substitution des variables qui reprend l'algorithme utilisé dans la thèse de Daniel pour la suppression des cycles non unitaires au sein d'un GFL
	CycleReduction(gfl);
#if SAVE_GRAPH
	gfl->saveGf("T23.GFL.A");
#endif
}
