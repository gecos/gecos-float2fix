/*!
 *  \author Mohammad Diab, Jean-Charles Naud
 *  \date   08.01.2008
 *  \version 1.0
 */

#include "graph/dfg/datanode/NoeudSrcBruit.hh"
#include "graph/dfg/Gfd.hh"
#include "graph/dfg/operatornode/NoeudOps.hh"
#include "graph/lfg/GFL.hh"
#include "graph/lfg/NoeudGFL.hh"
#include "graph/toolkit/TypeVar.hh"
#include "utils/Tool.hh"

#if DEBUG
#include <fstream>
#endif 

// === Namespaces
using namespace std;

Gfl* T22_ReccurentEquationDetermination(Gfd* gfd){
	Chrono chrono;
	chrono.start();
	gfd->resetInfoVisite(); //reset fo linear fonction determination

	NoeudData* out;
	list<LinearFunction*> fctOut;
	if(RuntimeParameters::isLTIGraph()){
		for(int i = 0 ; i < gfd->getNbOutput() ; i++){
			out = dynamic_cast<NoeudData*> (gfd->getOutputGf(i));
			if(out->getTypeSignalBruit() == BRUIT){
				fctOut.push_back(out->calculLinearFoncLTI());
				fctOut.back()->setNode(out);
			}
		}
	}
	else{
		for(int i = 0 ; i < gfd->getNbOutput() ; i++){
			out = dynamic_cast<NoeudData*> (gfd->getOutputGf(i));
			if(out->getTypeSignalBruit() == BRUIT){
				fctOut.push_back(out->calculLinearFoncNonLTI());
				fctOut.back()->setNode(out);
			}
		}
	}


#if DEBUG
    ofstream file(string(ProgParameters::getOutputGenDirectory() + "linear_function_debug.log").c_str(), ofstream::out);
    if(!file.is_open()){
        trace_debug("Error during opening the debug file for saving extracted linear function\n");
        exit(-1);
    }

	file <<"Affichage FonctionLineaire"<<endl;
	list<LinearFunction*>::iterator itListAffichage;
	for(itListAffichage = fctOut.begin() ; itListAffichage != fctOut.end() ; itListAffichage++){
		file << (*itListAffichage)->toString();
	}

    file.close();
#endif

	// Traitement des noeuds PHI
	NoeudGFD* nGFD;
	NoeudData* nData;
	for(int i = 2 ; i < gfd->getNbNode() ; i++){
		nGFD = dynamic_cast<NoeudGFD*> (gfd->getElementGf(i));
		assert(nGFD != NULL);
		if(nGFD->getTypeSignalBruit() == BRUIT && nGFD->getTypeNodeGFD() == OPS && ((NoeudOps*) nGFD)->isPHINode()){
			cout<<"ici"<<endl;
			assert(nGFD->getNbPred() >= 2);
			for(int j = 0 ; j < nGFD->getNbPred() ; j++){
				nData = dynamic_cast<NoeudData*> (nGFD->getElementPred(j));
				assert(nData != NULL);
				fctOut.push_back(nData->getPtrFonctionLineaire());
				fctOut.back()->setNode(nGFD);
			}
		}
	}

#if DEBUG
    file.open(string(ProgParameters::getOutputGenDirectory() + "linear_function_phi_debug.log").c_str(), ofstream::out);
    if(!file.is_open()){
        trace_debug("Error during opening the debug file for saving extracted linear function\n");
        exit(-1);
    }

	file <<"Affichage FonctionLineaire"<<endl;
	for(itListAffichage = fctOut.begin() ; itListAffichage != fctOut.end() ; itListAffichage++){
		file << (*itListAffichage)->toString();
	}

    file.close();
#endif

	chrono.stop();
	RuntimeParameters::timing("T22 Linear functions determination", &chrono);

	chrono.start();
	Gfl *GflGetFromT22 = new Gfl(fctOut, gfd->getNoiseEval()); //construction du GFL associe au GFD

	chrono.stop();
	RuntimeParameters::timing("T22 GFL creation", &chrono);
	/*list<FonctionLineaire*>::iterator itList;
	for(itList = fctOut.begin() ; itList != fctOut.end() ; itList++){
		delete *itList;
	}*/
#if SAVE_GRAPH
	GflGetFromT22->saveGf("T22.GFL.A");
#endif


	chrono.start();
	Gfl *GflRegroupe = GflGetFromT22->mergeOfGFL(); //fonction pour regrouper les petits GFL
#if SAVE_GRAPH
	GflRegroupe->saveGf("T22.GFL.B");
#endif

	chrono.stop();
	RuntimeParameters::timing("T22 Merging of all small GFL", &chrono);

	delete GflGetFromT22;
	return GflRegroupe;
}


/**
 *	Fonction récursive permettant le calcul des fonction lineaire du systeme (LTI)
 *
 *	\return Fonction Lineaire associé au noeudData
 *	\date 18\11\11
 *	\author Nicolas Simon
 */
LinearFunction* NoeudData::calculLinearFoncLTI(){
    NoeudGraph* NG;
    NoeudOps* nOps;
    //NoeudSrc* nSrc;
    PseudoLinearFunction* pseudoFct;
    LinearFunction* out;


    // Rend la sauvegarde de la fonction lineaire
    if(this->getPtrFonctionLineaire() != NULL){
        return new LinearFunction(*this->getPtrFonctionLineaire());
    }

    if(!this->isNodeSource()){
        assert(this->getNbPred() == 1);
        nOps = dynamic_cast<NoeudOps*> (this->getElementPred(0));
        assert(nOps != NULL);
        out = nOps->calculLinearFoncLTI();
    }
    else if(this->isNodeInput()){
        if(this->getTypeNodeData() == DATA_SRC || this->getTypeNodeData() == DATA_SRC_BRUIT || (this->isNodeNoeudDemantele() && this->getTypeSignalBruit() == BRUIT)){
            pseudoFct = new PseudoLinearFunction(this);
            pseudoFct->addEltZ(0,1,"1");
            out = new LinearFunction(this);
            out->addPseudoFctLineaire(pseudoFct);

        }else{
            out = new LinearFunction(this);
        }
    }
    else {// Constante
        out = new LinearFunction(this->getValeur(), this);
    }

    // Créé une sauvegarde de la fonction lineaire pour eviter de la recalculer au prochaine passage
    if(this->getNbSucc() > 1){
        this->setPtrFonctionLineaire(new LinearFunction(*out));
    }else{
        NG = this->getElementSucc(0);
        assert(NG != NULL);
        if(!NG->isNodeSink()){
            nOps = dynamic_cast<NoeudOps*>(NG);
            assert(nOps != NULL);
            if(nOps->isPHINode()){
                this->setPtrFonctionLineaire(new LinearFunction(*out));
                this->getPtrFonctionLineaire()->setNode(this);
                this->getPtrFonctionLineaire()->print();
            }
        }
    }


    //out->print();
    return out;
}


LinearFunction* NoeudOps::calculLinearFoncLTI(){
    LinearFunction* tabFct[this->getNbInput()];
    NoeudData* nData;

    for(int i = 0 ; i < this->getNbInput() ; i++){
        nData = dynamic_cast<NoeudData*> (this->getElementPred(i));
        assert(nData != NULL);
        tabFct[i] = nData->calculLinearFoncLTI();
    }

	return this->computeRuleLinearFunction(this->getNbInput(), tabFct);
}

/**
 *	Fonction récursive permettant le calcul des fonction lineaire du systeme (NON LTI)
 *
 *	\return Fonction Lineaire associé au noeudData
 *	\date 18\11\11
 *	\author Nicolas Simon
 */
LinearFunction* NoeudData::calculLinearFoncNonLTI(){
	NoeudGraph* NG;
	NoeudOps* nOps;
	PseudoLinearFunction* pseudoFct;
	LinearFunction* out;

	// Rend la sauvegarde de la fonction lineaire
	if(this->getPtrFonctionLineaire() != NULL){
		return new LinearFunction(*this->getPtrFonctionLineaire());
	}

	if(!this->getTypeSimu() && !this->isNodeSource()){
		assert(this->getNbPred() == 1);
		nOps = dynamic_cast<NoeudOps*> (this->getElementPred(0));
		assert(nOps != NULL);
		out = nOps->calculLinearFoncNonLTI();
	}
	else if(this->isNodeInput()){
		if(this->getTypeNodeData() == DATA_SRC || this->getTypeNodeData() == DATA_SRC_BRUIT || (this->isNodeNoeudDemantele() && this->getTypeSignalBruit() == BRUIT)){
			pseudoFct = new PseudoLinearFunction(this);
			pseudoFct->addEltZ(0,1,"1");
			out = new LinearFunction(this);
			out->addPseudoFctLineaire(pseudoFct);
		}
		else
		{
		 out = new LinearFunction(this);
		 }
	}
//	else if(this->getTypeSignalBruit() == BRUIT)
//		out = new FonctionLineaire(this);
	else
		out = new LinearFunction(this->getValeur(), this);

	// Créé une sauvegarde de la fonction lineaire pour eviter de la recalculer au prochaine passage
	if(this->getNbSucc() > 1){
		this->setPtrFonctionLineaire(new LinearFunction(*out));
	}else{
		NG = this->getElementSucc(0);
		assert(NG != NULL);
		if(!NG->isNodeSink()){
			nOps = dynamic_cast<NoeudOps*>(NG);
			assert(nOps != NULL);
			if(nOps->isPHINode()){
				this->setPtrFonctionLineaire(new LinearFunction(*out));
			}
		}
	}


	return out;
}

LinearFunction* NoeudOps::calculLinearFoncNonLTI(){
	LinearFunction* tabFct[this->getNbInput()];
	NoeudData* nData;

	for(int i = 0 ; i < this->getNbInput() ; i++){
		nData = dynamic_cast<NoeudData*> (this->getElementPred(i));
		assert(nData != NULL);
		tabFct[i] = nData->calculLinearFoncNonLTI();
	}

	return this->computeRuleLinearFunction(this->getNbInput(), tabFct);
}


/**
 *	Fonction récursive permettant l:e calcul des fonction lineaire du systeme (LTI)
 *
 *	\return Fonction Lineaire associé au noeudData
 *	\date 18\11\11
 *	\author Nicolas Simon
 */
/*FonctionLineaire* NoeudData::calculLinearFoncLTI(){
	NoeudGraph* NG;
	NoeudOps* nOps;
	//NoeudSrc* nSrc;
	PseudoFonctionLineaire* pseudoFct;
	FonctionLineaire* out;


	// Rend la sauvegarde de la fonction lineaire
	if(this->getPtrFonctionLineaire() != NULL){
		return new FonctionLineaire(*this->getPtrFonctionLineaire());
	}

	if(!this->isNodeSource()){
		assert(this->getNbPred() == 1);
		nOps = dynamic_cast<NoeudOps*> (this->getElementPred(0));
		assert(nOps != NULL);
		out = nOps->calculLinearFoncLTI();
	}
	else if(this->isNodeInput()){
		if(this->getTypeNodeData() == DATA_SRC || this->getTypeNodeData() == DATA_SRC_BRUIT || (this->isNodeNoeudDemantele() && this->getTypeSignalBruit() == BRUIT)){
			pseudoFct = new PseudoFonctionLineaire(this);
			pseudoFct->addEltZ(0,1);
			out = new FonctionLineaire(this);
			out->addPseudoFctLineaire(pseudoFct);
		}
		else
		{
		 out = new FonctionLineaire(this);
		 }
	}
	else {// Constante
		out = new FonctionLineaire(this->getValeur(), this);
	}

	// Créé une sauvegarde de la fonction lineaire pour eviter de la recalculer au prochaine passage
	if(this->getNbSucc() > 1){
		this->setPtrFonctionLineaire(new FonctionLineaire(*out));
	}else{
		NG = this->getElementSucc(0);
		assert(NG != NULL);
		if(!NG->isNodeSink()){
			nOps = dynamic_cast<NoeudOps*>(NG);
			assert(nOps != NULL);
			if(nOps->isPHINode()){
				this->setPtrFonctionLineaire(new FonctionLineaire(*out));
			}
		}
	}
	//cout<<"NoeudData : "<<this->getName()<<endl;
	//out->print();
	return out;
}

FonctionLineaire* NoeudOps::calculLinearFoncLTI(){
	FonctionLineaire* tabFct[this->getNbInput()];
	FonctionLineaire* result;
	NoeudData* nData;
	PseudoFonctionLineaire* pseudoFct;


	for(int i = 0 ; i < this->getNbInput() ; i++){
		nData = dynamic_cast<NoeudData*> (this->getElementPred(i));
		assert(nData != NULL);
		tabFct[i] = nData->calculLinearFoncLTI();
	}


	switch(this->getTypeOps()){
		case OPS_EQ :
			result = tabFct[0];
			break;

		case OPS_ADD :
			result = *tabFct[0] + *tabFct[1];
			delete tabFct[0];
		   	delete tabFct[1];
			break;

		case OPS_SUB :
			result = *tabFct[0] - *tabFct[1];
			delete tabFct[0];
			delete tabFct[1];
			break;

		case OPS_MULT :
			result = *tabFct[0] * *tabFct[1];
			delete tabFct[0];
			delete tabFct[1];
			break;

		case OPS_DIV :
			result = *tabFct[0] / *tabFct[1];
			delete tabFct[0];
			delete tabFct[1];
			break;

		case OPS_DELAY :
			tabFct[0]->operatorDelay();
			result = tabFct[0];
			break;

		case OPS_PHI :
			assert(getNbSucc() == 1);
			pseudoFct = new PseudoFonctionLineaire(this);
			pseudoFct->addEltZ(0,1);
			result = new FonctionLineaire(this); // Le nData ne sert a rien puisqu'il est écrasé
			result->addPseudoFctLineaire(pseudoFct);
			break;

		default :
			trace_error("Opérateur non connu au niveau des traitements des fonctions linéaires");
			exit(-1);

	}
	//cout<<"Num Ops : "<<this->getNumero()<<endl;
	//result->print();
	return result;
}*/

