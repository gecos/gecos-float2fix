/*!
 *  \author Daniel Menard, Jean-Charles Naud
 *  \date   10.06.2002
 *  \version 1.0
 */

#include "graph/dfg/datanode/NoeudSrc.hh"
#include "graph/dfg/Gfd.hh"
#include "graph/dfg/operatornode/NoeudOps.hh"
#include "graph/toolkit/AdjacentMatrix.hh"
#include "graph/toolkit/TypeVar.hh"
#include "noise/T2_Group.hh"
#include "utils/graph/cycledismantling/CycleDismantling.hh"
#include "utils/graph/cycledismantling/GraphPath.hh"
#include "graph/Edge.hh"


void T21_CycleDismantling(Gfd* gfd){
	CycleDismantling(gfd);
#if SAVE_GRAPH
	gfd->saveGf("T21.GFD.Z");
#endif
}
