/*!
 *  \author Daniel Menard, Mohammad DIAB, Jean-Charles Naud
 *  \date   10.06.2002
 *  \version 1.0
 */
// === Bibliothèques standard
#include <string>
#include <sstream>

#include "graph/dfg/datanode/NoeudData.hh"
#include "graph/dfg/datanode/NoeudSrcBruit.hh"
#include "graph/dfg/Gfd.hh"
#include "graph/dfg/operatornode/kind/ADDNode.hh"
#include "graph/dfg/operatornode/kind/DELAYNode.hh"
#include "graph/dfg/operatornode/kind/DIVNode.hh"
#include "graph/dfg/operatornode/kind/MULNode.hh"
#include "graph/dfg/operatornode/kind/PHINode.hh"
#include "graph/dfg/operatornode/kind/SETNode.hh"
#include "graph/dfg/operatornode/kind/SUBNode.hh"
#include "graph/toolkit/TypeVar.hh"
#include "utils/Tool.hh"
#include "graph/Edge.hh"

using namespace std;

static void InsertionNoiseSourceModel(Gfd* gfd);
static void InsertionDataNoiseModel(Gfd* gfd);
static void InsertionOperatorNoiseModel(Gfd* gfd);

void T12_NoiseDataAndOperatorModelInsertion(Gfd* gfd){
	InsertionNoiseSourceModel(gfd);
	gfd->deleteIsolatedNode();

	InsertionDataNoiseModel(gfd);
#if SAVE_GRAPH
	gfd->saveGf("T12.GFD.A");
#endif

	InsertionOperatorNoiseModel(gfd);

#if SAVE_GRAPH
	gfd->saveGf("T12.GFD.Z");
#endif
}

/**
 *  Function which for each noise source Br add an addition and 2 data nodes
 *  before:
 *  Ops1-->Data-->Br-->Ops2
 *  after:
 *  Ops1-->Data    -->(    )
 *        Br       -->( add)-->VarInt-->Ops2
 *		  srcSignal-->(	   )
 *	\ingroup T11
 *  \auhtor Loic Cloatre
 *  \author Nicolas Simon
 *  \date 01/04/11
 */
static void InsertionNoiseSourceModel(Gfd* gfd){
	vector<NoeudSrcBruit*>::iterator it;
	NoeudSrcBruit * nodeSrcNoise;
	NoeudData *nodeData;
	NoeudOps *nodeOp;
	NoeudGraph * nodeGraphPred, *nodeGraphSuccc;
	NoeudGFD * nodeGFD;
	for(int i = 0 ; i<gfd->getNbNoiseNode() ; i++){
		nodeSrcNoise = gfd->getNoiseNode(i);

		// Un Br ne peut avoir un seul Pred et un Succ a ce niveau la du a son ajout entre une data et un op
		assert(nodeSrcNoise->getNbSucc() == 1 && nodeSrcNoise->getNbPred() == 1);

		nodeGraphPred = nodeSrcNoise->getElementPred(0);
		nodeGraphSuccc = nodeSrcNoise->getElementSucc(0);

		//cout<<nodeSrcNoise->getName()<<endl;
		//cout<<nodeGraphSuccc->getName()<<endl;

		//cout<<nodeGraphPred->getName()<<endl;
		nodeGFD=dynamic_cast<NoeudGFD *>(nodeGraphSuccc);
		//if (((NoeudGFD *) nodeGraphPred)->getTypeNodeGFD() == DATA ) {
		if (!(nodeGraphSuccc->getTypeNodeGraph()== FIN) && nodeGFD->getTypeNodeGFD() == OPS ) {
			//cout<<"Noeud non sortie et Successeur est un OPS"<<endl;
			nodeOp = dynamic_cast<NoeudOps*> (nodeGraphSuccc);
			nodeData = dynamic_cast<NoeudData*> (nodeGraphPred);

			assert(nodeOp != NULL && nodeData != NULL);

			// Un Br ne peut être une entrée, une sortie ou un noeud dit isolée
			assert(nodeOp->getTypeNodeGraph() != FIN && nodeData->getTypeNodeGraph() != INIT);

			// === Création de d'un noeud intermédiaire pour garder la cohérence DATA -> OPS -> DATA
			string nomVarInt = "VarInt_";
			ostringstream numNoeud;
			numNoeud << gfd->getNumNode();
			nomVarInt += numNoeud.str();
			TypeVar *TypeV = new TypeVar(VAR);
			NoeudData *VarIntNode = gfd->addNoeudData((char *) nomVarInt.c_str(), TypeV);
			VarIntNode->getTypeVar()->setTypeVarFonct(VAR_INT);
            gfd::ADDNode *addNode = new gfd::ADDNode(nodeOp->getBlock(), 3);
            gfd->addOPNode(addNode);

			// === Création de d'un noeud srcSignal (même endroit que le br, mais restera du coté signal)
			string nomsrcSignal = "srcSignal_";
			nomsrcSignal += numNoeud.str(); //gfd->NumNode;
			NoeudData *srcSignalNode = gfd->addNoeudData((char *) nomsrcSignal.c_str(), new TypeVar(VAR_ENTREE));
			srcSignalNode->setTypeSignalBruit(SIGNAL);
			srcSignalNode->setTypeNodeData(DATA_BASE);
			srcSignalNode->getTypeVar()->setTypeVarFonct(VAR);
			srcSignalNode->setSrcSignal(true);

			// Sauvegarde du numInput avant modification des edges
			int numInput = nodeSrcNoise->getEdgeSucc(0)->getNumInput();

			// === Redirection des arcs
			nodeData->edgeDirectedTo(addNode, 0);
			srcSignalNode->edgeDirectedTo(addNode, 1);
			nodeSrcNoise->edgeDirectedTo(addNode, 2);
			nodeData->deleteEdgeDirectedTo(nodeSrcNoise);
			addNode->edgeDirectedTo(VarIntNode);
			nodeSrcNoise->deleteEdgeDirectedTo(nodeOp);
			VarIntNode->edgeDirectedTo(nodeOp, numInput);

			nodeSrcNoise->getTypeVar()->setTypeVarFonct(VAR_ENTREE);

			gfd->getNoeudInit()->edgeDirectedTo(srcSignalNode);
			gfd->getNoeudInit()->edgeDirectedTo(nodeSrcNoise);

		}
		else {
			// === Traitement des br (spéciaux) situé juste avant les sorties.
			//cout<<"Noeud sortie ou Successeur est une DATA"<<endl;
			nodeOp = dynamic_cast<NoeudOps*> (nodeGraphPred);
			nodeData = dynamic_cast<NoeudData*> (nodeGraphSuccc);
			//cout<<nodeData->getName()<<endl;
			assert(nodeOp != NULL && nodeData != NULL);
			//assert(nodeData->isNodeOutput());
			// nodeOp-->br--->nodeData-->(FIN)

			// === Création de d'un noeud intermédiaire pour garder la cohérence DATA -> OPS -> DATA
			string nomVarInt = "VarInt_";
			ostringstream numNoeud;
			numNoeud << gfd->getNumNode();
			nomVarInt += numNoeud.str();
			TypeVar *TypeV = new TypeVar(VAR);
			NoeudData *VarIntNode = gfd->addNoeudData((char *) nomVarInt.c_str(), TypeV);
			VarIntNode->getTypeVar()->setTypeVarFonct(VAR_INT);
            gfd::ADDNode *addNode = new gfd::ADDNode(nodeOp->getBlock(), 3);
            gfd->addOPNode(addNode);

			// === Création de d'un noeud srcSignal (même endroit que le br, mais restera du coté signal)
			string nomsrcSignal = "srcSignal_";
			nomsrcSignal += numNoeud.str(); // gfd->NumNode;
			NoeudData *nodeSrcSignal = gfd->addNoeudData((char *) nomsrcSignal.c_str(), new TypeVar(VAR_ENTREE));
			nodeSrcSignal->setTypeSignalBruit(SIGNAL);
			nodeSrcSignal->setTypeNodeData(DATA_BASE);
			nodeSrcSignal->getTypeVar()->setTypeVarFonct(VAR);
			nodeSrcSignal->setSrcSignal(true);

			// === Redirection des arcs
			// nodeOp-->br(nodeSrcNoise)-->nodeData : addNode : VarIntNode : nodeSrcSignal
			nodeOp->deleteEdgeDirectedTo(nodeSrcNoise);
			nodeSrcNoise->deleteEdgeDirectedTo(nodeData);
			// nodeOp  :  br(nodeSrcNoise)  :  nodeData : addNode : VarIntNode : nodeSrcSignal
			nodeOp->edgeDirectedTo(VarIntNode);
			VarIntNode->edgeDirectedTo(addNode, 0);
			nodeSrcSignal->edgeDirectedTo(addNode, 1);
			nodeSrcNoise->edgeDirectedTo(addNode, 2);
			addNode->edgeDirectedTo(nodeData);
			// nodeOp-->VarIntNode-->addNode-->nodeData
			// br(nodeSrcNoise)-------^ ^----nodeSrcSignal

			nodeSrcNoise->getTypeVar()->setTypeVarFonct(VAR_ENTREE);
			gfd->getNoeudInit()->edgeDirectedTo(nodeSrcSignal);
			gfd->getNoeudInit()->edgeDirectedTo(nodeSrcNoise);
		}
	}
}

/**
 *  Insertion of the noise model for data
 * 		- A data node x is split into a data signal node x  and a data noise node x.b
 *
 *	\ingroup T12
 *  \author Mohammad DIAB, Loic Cloatre
 *  \date 02/05/2009
 *  \todo REPONDRE AUX QUESTIONS SUIVANTES :
 *  	- pourquoi ceci : NDataBruit->setTypeNodeData(DATA_SRC_BRUIT); il faut il me semble DATA_BASE
 *  	- il faut traiter le cas ou ce n'est pas une entree ou une sortie
 *  	- A voir entre  loic et Daniel car pas clair
 *
 */
static void InsertionDataNoiseModel(Gfd* gfd){
	int j, k, nbSucc, nbPred;
	NoeudGFD *NGFD = NULL;
	NoeudData *NDataSignal = NULL;
	NoeudData *NDataBruit = NULL;
	TypeVar *TVarSignal = NULL;
	TypeVar *TVarBruit = NULL;
	NoeudGraph *NGraph2 = NULL;
	NoeudOps* NOps = NULL;

	NodeGraphContainer::iterator it;
	char s[30];

	int nbNodeSave;

	nbNodeSave = gfd->getNbNode();
	for (int i = 2 ; i < nbNodeSave; i++) // parcours de la liste (On ne peut pas utiliser ->end() car cette methode modifie la taille de la liste)
	{
		NGFD = dynamic_cast<NoeudGFD *> (gfd->getElementGf(i));
		assert(NGFD != NULL);
		if (NGFD->getTypeNodeGFD() == DATA) { // On ne regarde que les DATA
			NDataSignal = (NoeudData *) NGFD;

			if (NDataSignal->getTypeNodeData() != DATA_SRC_BRUIT && !NDataSignal->getSrcSignal()) { // On ne regarde que les DATA SIGNAL

				sprintf(s, "%s.b", NDataSignal->getOriginalName().data());

				if (NDataSignal->isNodeOutput() == true) {
					NDataBruit = gfd->addNoeudData(s, new TypeVar(VAR_SORTIE)); //une sortie est modelisee par une sortie.b aussi
					NDataBruit->setTypeNodeData(DATA_BASE); //old(DATA_SRC_BRUIT);
				}
				else if (NDataSignal->isNodeInput() == true) {
					NDataBruit = gfd->addNoeudData(s, new TypeVar(VAR_ENTREE));
					NDataBruit->setTypeNodeData(DATA_BASE); //old(DATA_SRC_BRUIT);
				}
				else {
					NDataBruit = gfd->addNoeudData(s, new TypeVar(VAR));
					NDataBruit->setTypeNodeData(DATA_BASE); //old(DATA_SRC_BRUIT);
				}

//				numSrcNode++; //on incremente le numero// pas de set pour maintenir le numero a -1
				gfd->incNumSrcNode();
				NDataBruit->setTypeSignalBruit(BRUIT);
				//				NDataBruit->getTypeVar()->setTypeVarFonct(VAR);

				TVarSignal = NDataSignal->getTypeVar();
				TVarBruit = NDataBruit->getTypeVar();

				TVarBruit->setTypeVarFonct(TVarSignal->getTypeVarFonct());

				NDataSignal->setPtrclone(NDataBruit);

				// Securite
				NDataBruit->setTypeSignalBruit(BRUIT);
				NDataSignal->setTypeSignalBruit(SIGNAL);

				// Redirection des Suc Pred des Pred Suc
				nbSucc = NDataSignal->getNbSucc();
				nbPred = NDataSignal->getNbPred();

				vector<Edge *>::iterator itPredOps;
				for (j = 0; j < nbSucc; ++j) {
					NGraph2 = NDataSignal->getElementSucc(j);
					if(NGraph2->getTypeNodeGraph() == FIN){
						NDataBruit->edgeDirectedTo(NGraph2);
					}
					else {
						NOps = dynamic_cast<NoeudOps*> (NGraph2);
						assert(NOps != NULL);
						itPredOps = NOps->getListePred()->begin();
						while (itPredOps != NOps->getListePred()->end()) {
							if ((*itPredOps)->getNodePred() == NDataSignal)
								break;

							itPredOps++;
						}
						//if(NGraph2->getNbPred()%NGraph2->getNbInput() == 0) // cas sur les noeud ops sans Br
						NDataBruit->edgeDirectedTo(NOps, (*itPredOps)->getNumInput()+ NOps->getNbInput());
						//else
						//	NDataBruit->edgeDirectedTo(NGraph2, (*itPredOps)->getNumInput() + Graph2->getNbPred());

					}
				}

				for (k = 0; k < nbPred; k++) {
					NGraph2 = NDataSignal->getElementPred(k);
					NDataBruit->edgeFromOf(NGraph2);
				}
			}
		}
	}
}

/**
 *  Insertion of the Operation noise model
 *  	- Each operation is replaced by its noise model according to its function
 *
 *  \ingroup T12
 *  \author Mohammad DIAB, Loic Cloatre
 *  \date  12/02/2009
 */
static void InsertionOperatorNoiseModel(Gfd* gfd){
	NoeudGFD *NGFD;
	NoeudOps *NOps;

	NodeGraphContainer::iterator it;
	int nbNodeSave;

	nbNodeSave = gfd->getNbNode();

	for (int i = 2; i < nbNodeSave; i++) { // parcours de la liste (On ne peut pas utiliser ->end() car cette fonction modifie la taille de la liste)
		NGFD = dynamic_cast<NoeudGFD *> (gfd->getElementGf(i));
		assert(NGFD != NULL);

		if (NGFD->getTypeNodeGFD() == OPS) // On parcours tous les Noeuds de type OPS du GFD
		{
			NOps = (NoeudOps *) NGFD;
            NOps->noiseModelInsertion(gfd);
		}
	}
}
