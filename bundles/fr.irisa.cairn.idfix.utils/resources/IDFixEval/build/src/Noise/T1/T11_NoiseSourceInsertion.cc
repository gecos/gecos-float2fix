/*!
 *  \author Daniel Menard, Mohammad DIAB, Jean-Charles Naud
 *  \date   10.06.2002
 *  \version 1.0
 *
 *  Regroup all the T11 transformation functions
 *
 */
// === Bibliothèques standard
#include "graph/dfg/datanode/NoeudData.hh"
#include "graph/dfg/datanode/NoeudSrcBruit.hh"
#include "graph/dfg/Gfd.hh"
#include "graph/dfg/operatornode/NoeudOps.hh"
#include "graph/toolkit/TypeVar.hh"
#include "utils/Tool.hh"

#include "backend/NoisePowerExpressionGeneration.hh"

using namespace std;


static void NoiseSourceInsertion(Gfd* gfd);
static void NoiseSourceAggregation(Gfd* gfd);
static void NoiseSourceNotCorrelatedAggregation(Gfd* gfd);
static void NoiseSourceNotCorrelatedForcedAggregation(Gfd* gfd);


/**
 * 1 - Insertion of the noise source node
 * 2 - Add information of the number of bit eliminated, quantification information, step quantification for each source noise
 * 3 - Apply simple aggregation
 * 4 - Apply advanced aggregation due to correlation model
 */
void T11_NoiseSourceInsertion(Gfd* gfd){
	NoiseSourceInsertion(gfd);
#if SAVE_GRAPH
	gfd->saveGf("T11.GFD.A");
#endif

	WFPSFGToWFPSFGEff(gfd);

	NoiseSourceAggregation(gfd);
#if SAVE_GRAPH
	gfd->saveGf("T11.GFD.B");
#endif


	if(!RuntimeParameters::isRecursiveGraph()){ // Si il n'y a que une seule sortie, cela ne sert a rien de faire ce traitement. Il faut au moins 2 sortie pour la non corrélation et que que le graph ne soit pas récursif
		if(ProgParameters::isCorrelationMode() && gfd->getNbOutput() > 1){
			NoiseSourceNotCorrelatedAggregation(gfd);
		}
		else if(!ProgParameters::isCorrelationMode()){
			NoiseSourceNotCorrelatedForcedAggregation(gfd);
		}
		else{
			trace_warn("Aucune aggregation supplémentaire effectué alors que le graph est non récursif\n");
		}
	}

	gfd->saveGf("T11.GFD.Z");
}

/**
 *  Insertion of the noise sources in the graph
 *  	- Insertion on each input of operator
 *		- Insertion after each input and before each output
 *		- Insertion before a data node if it have more of one successor
 *  \author Nicolas Simon
 *  \date 13/12/11
 */
static void NoiseSourceInsertion(Gfd* gfd){
	int nbNodeSave;
	NoeudGFD* nodeGFD;
	NoeudOps* nodeOps;
	NoeudData* nodeData;

	assert(gfd->getNbNode() > 2); // Verification si le graph est non vide
	nbNodeSave = gfd->getNbNode(); // Sauvegarde du nombre de noeud pour effectuer l'itération vu que nous ajoutons des nouveaux noeuds au graph et donc modifions le nombre de noeuds au sein du graph

	for(int i = 2 ; i < nbNodeSave ; i++){ // On commence à 2 pour eviter le noeud INIT ou FIN
		nodeGFD = dynamic_cast<NoeudGFD*> (gfd->getElementGf(i));
		assert(nodeGFD != NULL);
		if(nodeGFD->getTypeNodeGFD() == DATA){
			nodeData = (NoeudData*) nodeGFD;
			// Un Br doit être présent juste avant chaque sortie du système.
			// Un noeud Br doit être présent devant chaque noeud data ayant plus d'un successeur pour gérer la corrélation
			if((nodeData->isNodeOutput() || nodeData->getNbSucc() > 1) && !nodeData->isNodeConst() && !nodeData->isNodeInput()){
				gfd->addNoeudSrcBruit(nodeData);
			}
		}
		else if(nodeGFD->getTypeNodeGFD() == OPS){ // Un noeud Br doit être présent à chaque entrée d'un noeud opérateur
			nodeOps = (NoeudOps*) nodeGFD;
            if(!nodeOps->isPHINode()){
                gfd->addNoeudSrcBruit(nodeOps);
            }
		}
		else{
			trace_error("Type de noeud inconnue au sein du GFD");
			exit(0);
		}
	}
}

/**
 *  Function which detects and realise aggregate of Noise sources
 *  ex: going up a tree:
 *  e = c + d(source)   et c = a + b(source)
 *  if conditions are ok, it becomes
 *  e = c + d --->with b integrated into d and the a disapears
 *
 *	\ingroup T11
 *  \author Loic Cloatre
 *  \date 02/03/2009
 */
static void NoiseSourceAggregation(Gfd* gfd){
	vector<NoeudSrcBruit*>::iterator it;
	NoeudSrcBruit *noiseSourceNode;

	gfd->resetInfoVisite(); // un noeud est marque visite si on ne peut plus le faire traverser d'addition

	for(int i = 0 ; i< gfd->getNbNoiseNode() ; i++){
		noiseSourceNode = gfd->getNoiseNode(i);
		assert(noiseSourceNode != NULL);
		((NoeudGFD*)noiseSourceNode->getElementSucc(0))->moveNoiseSource(noiseSourceNode);
		//test if noise source has been merged
		if (noiseSourceNode->isNodeIsolate() == true) {
			gfd->deleteNoiseNode(noiseSourceNode);
			i--;
		}
	}
	gfd->deleteIsolatedNode();
	gfd->resetInfoVisite();
}

/**
 *	Detecte tout les liens entre chaque sortie et chaque Br, determine si le Br ayant une possibilité d'être corrélé l'est ou non. Si il n'est pas corrélé, il aggrege ce Br.
 *
 *	\author Nicolas Simon
 *	\date 04/01/12
 *
 */
static void NoiseSourceNotCorrelatedAggregation(Gfd* gfd){
	list< list<NoeudData*> > *tabListBr = new list< list<NoeudData*> >[gfd->getNbNoiseNode()];
	list< list<NoeudData*> >::iterator itList, itListSucc;
	list<NoeudData*>::iterator itList2, itList2Succ;
	bool isCorrelated;
	int numInput, nbNoiseNodeSave;
	NoeudData *input, *nodeData;
	NoeudGFD *nodeGFDSucc, *nodeGFDPred, *nodeGFD;
	NoeudSrcBruit *nodeSrcNoise, *nodeSrcNoiseCopie;

	for(int i = 0 ; i < gfd->getNbInput() ; i++){
		input = (NoeudData*) gfd->getInputGf(i);
		input->detectionBrCorrelated(tabListBr);
	}

	// Affichage
/*	for(int i = 0 ; i < gfd->getNbNoiseNode() ; i++){
		cout<<"Br : "<<gfd->getNoiseNode(i)->getName()<<endl;
		int numBranche = 0;
		for(itList = tabListBr[i].begin() ; itList != tabListBr[i].end() ; itList++){
			cout<<"Branche "<<numBranche<<endl;
			for(itList2 = (*itList).begin() ; itList2 != (*itList).end() ; itList2++){
				cout<<(*itList2)->getName()<<endl;
			}
			numBranche++;
		}
		cout<<endl;
	}
*/
	nbNoiseNodeSave = gfd->getNbNoiseNode();
	for(int i = 0 ; i < nbNoiseNodeSave ; i++){
		nodeSrcNoise = gfd->getNoiseNode(i);
		assert(nodeSrcNoise->getNbSucc() == 1);

		nodeGFD = dynamic_cast<NoeudGFD*> (nodeSrcNoise->getElementSucc(0));
		assert(nodeGFD != NULL);

		if(nodeGFD->getTypeNodeGFD() == DATA){ // Nous devons traiter que les Br qui sont dans le possible cas de la corrélation. c-a-d, que le successeur est une data et non un ops
			nodeData = (NoeudData*) nodeGFD;

			if(nodeData->getNbSucc() > 1){

				isCorrelated = false;

				for(itList = tabListBr[nodeSrcNoise->getNumSrc() - 1].begin() ; itList != tabListBr[nodeSrcNoise->getNumSrc() - 1].end() && !isCorrelated ; itList++){
					for(itList2 = (*itList).begin() ; itList2 != (*itList).end() && !isCorrelated ; itList2++){
						itListSucc = itList;
						itListSucc++;
						for(itList2Succ = (*itListSucc).begin() ; itList2Succ != (*itListSucc).end() && !isCorrelated ; itList2Succ++){
							isCorrelated = ((*itList2) == (*itList2Succ));
						}
					}
				}

				if(!isCorrelated && !nodeData->isNodeOutput()){ // création de nouveaux bruit ayant pour caractéristique les informations du noeud bruit avant la noeud data. Ces noeuds sont ensuite aggrégé si possible.
					for(int j = 0 ; j < nodeData->getNbSucc() ; j++){
						nodeSrcNoiseCopie = gfd->addNoeudSrcBruitCopie(nodeSrcNoise);
						nodeGFDSucc = dynamic_cast<NoeudGFD*> (nodeData->getEdgeSucc(0)->getNodeSucc()); // EdgeSucc(0) car nous supprimons l'arc plus tard, le suivant passe donc en indice 0
						assert(nodeGFDSucc != NULL);
						numInput = nodeData->getEdgeSucc(0)->getNumInput();

						nodeData->deleteEdgeDirectedTo(nodeGFDSucc);
						nodeData->edgeDirectedTo(nodeSrcNoiseCopie);
						nodeSrcNoiseCopie->edgeDirectedTo(nodeGFDSucc, numInput);

						// TODO A verifier si la transformation est correcte NS 21/02/12
						if(nodeGFDSucc->getTypeNodeGFD() == DATA && ((NoeudData*) nodeGFDSucc)->getTypeNodeData() == DATA_SRC_BRUIT){
							// The node just below us is a noise node, it is our only successor and it is in the same block as the other noise source we are handling
							//we need to merge 2 noise sources: noiseSourceNode and nodeGraphSuiv
							//we add current noise source parameter to next noise source: nodeGraphSuiv

							((NoeudSrcBruit *) nodeGFDSucc)->calculSumParamNoise((NoeudSrcBruit *) (nodeSrcNoiseCopie)); //add parameter

							//noeudPred-->Data-->Br(noiseSourceNode)-->Br(nodeGraphSuiv)-->OPS   before
							nodeData->deleteEdgeDirectedTo(nodeSrcNoiseCopie);
							nodeSrcNoiseCopie->deleteEdgeDirectedTo(nodeGFDSucc);
							nodeData->edgeDirectedTo(nodeGFDSucc);
							//noeudPred-->Data-->Br(nodeGraphSuiv)-->OPS   after

							//no recursion to continue move: gfd is end of process on noiseSourceNode which is merged
						}
						else{
							nodeGFDSucc->moveNoiseSource(nodeSrcNoiseCopie);
						}

						if (nodeSrcNoiseCopie->isNodeIsolate())
							gfd->deleteNoiseNode(nodeSrcNoiseCopie);
					}

					// Suppression du noeud Br non corrélé
					nodeGFDPred = (NoeudGFD*)nodeSrcNoise->getElementPred(0);
					nodeGFDPred->deleteEdgeDirectedTo(nodeSrcNoise);
					nodeSrcNoise->deleteEdgeDirectedTo(nodeData);
					nodeGFDPred->edgeDirectedTo(nodeData);
					gfd->deleteNoiseNode(nodeSrcNoise);
					nbNoiseNodeSave--;
					i--;

				}
			}
		}
	}

	gfd->deleteIsolatedNode();
	delete [] tabListBr;
}

/**
 *	Force l'aggrégation des Br ayant comme successeur une data avec plus de 1 successeur
 *
 *	\author Nicolas Simon
 *	\date 24/01/11
 *
 *
 */
static void NoiseSourceNotCorrelatedForcedAggregation(Gfd* gfd){
	int numInput, nbNoiseNodeSave;
	NoeudData* nodeData;
	NoeudGFD *nodeGFDSucc, *nodeGFDPred, *nodeGFD;
	NoeudSrcBruit *nodeSrcNoise, *nodeSrcNoiseCopie;

	nbNoiseNodeSave = gfd->getNbNoiseNode();
	for(int i = 0 ; i < nbNoiseNodeSave ; i++){
		nodeSrcNoise = gfd->getNoiseNode(i);
		assert(nodeSrcNoise->getNbSucc() == 1);

		nodeGFD = dynamic_cast<NoeudGFD*> (nodeSrcNoise->getElementSucc(0));
		assert(nodeGFD != NULL);

		if(nodeGFD->getTypeNodeGFD() == DATA){ // Nous devons traiter que les Br qui sont dans le possible cas de la corrélation. c-a-d, que le successeur est une data et non un ops
			nodeData = (NoeudData*) nodeGFD;

			if(nodeData->getNbSucc() > 1 && !nodeData->isNodeOutput()){
				for(int j = 0 ; j < nodeData->getNbSucc() ; j++){
					nodeSrcNoiseCopie = gfd->addNoeudSrcBruitCopie(nodeSrcNoise);
					nodeGFDSucc = dynamic_cast<NoeudGFD*> (nodeData->getEdgeSucc(0)->getNodeSucc()); // EdgeSucc(0) car nous supprimons l'arc plus tard, le suivant passe donc en indice 0
					assert(nodeGFDSucc != NULL);

					numInput = nodeData->getEdgeSucc(0)->getNumInput();

					nodeData->deleteEdgeDirectedTo(nodeGFDSucc);
					nodeData->edgeDirectedTo(nodeSrcNoiseCopie);
					nodeSrcNoiseCopie->edgeDirectedTo(nodeGFDSucc, numInput);

					if (nodeGFDSucc->getTypeNodeGFD() == DATA && ((NoeudData *) nodeGFDSucc)->getTypeNodeData() == DATA_SRC_BRUIT) {
						// The node just below us is a noise node, it is our only successor and it is in the same block as the other noise source we are handling
						//we need to merge 2 noise sources: noiseSourceNode and nodeGraphSuiv
						//we add current noise source parameter to next noise source: nodeGraphSuiv

						((NoeudSrcBruit *) nodeGFDSucc)->calculSumParamNoise(nodeSrcNoiseCopie); //add parameter

						//noeudPred-->Data-->Br(nodeSrcNoiseCopie)-->Br(nodeGFDSucc)-->OPS   before
						nodeData->deleteEdgeDirectedTo(nodeSrcNoiseCopie);
						nodeSrcNoiseCopie->deleteEdgeDirectedTo(nodeGFDSucc);
						nodeData->edgeDirectedTo(nodeGFDSucc);
						//noeudPred-->Data-->Br(nodeGFDSucc)-->OPS   after

						//no recursion to continue move: gfd is end of process on noiseSourceNode which is merged
						//in nodeGraphSuiv noise source-->these noise source is processed after with the tabNoiseSourcesNode
					}
					else{
						nodeGFDSucc->moveNoiseSource(nodeSrcNoiseCopie);
					}

					if (nodeSrcNoiseCopie->isNodeIsolate())
						gfd->deleteNoiseNode(nodeSrcNoiseCopie);
				}

				// Suppression du noeud Br non corrélé
				nodeGFDPred = (NoeudGFD*)nodeSrcNoise->getElementPred(0);
				nodeGFDPred->deleteEdgeDirectedTo(nodeSrcNoise);
				nodeSrcNoise->deleteEdgeDirectedTo(nodeData);
				nodeGFDPred->edgeDirectedTo(nodeData);
				gfd->deleteNoiseNode(nodeSrcNoise);
				nbNoiseNodeSave--;
				i--;
			}
		}
	}
	gfd->deleteIsolatedNode();
}

/**
 *  function which moves noise sources if possible
 *  	\param NoeudGraph : sources to move
 *
 *  \author Loic Cloatre
 *  \author Nicolas Simon
 *  \date 28/01/2010
 *
 */
void NoeudData::moveNoiseSource(NoeudSrcBruit * noiseSourceNode) {
	//3 cases:
	//noeudPred-->Br-->Data-->OPS           (one path of data flow)
	//noeudPred-->Br-->Data-->Br-->OPS      (one path of data flow)
	//noeudPred-->Br-->Data --> n?          (and more than one path of data flow, we don't move noise source)
	int numInput;
	assert(noiseSourceNode->getNbPred() == 1 && this->getTypeNodeGraph() != FIN);

	NoeudGraph *nodePred = noiseSourceNode->getElementPred(0);
	NoeudGraph *nodeGraphSuiv = this->getElementSucc(0);
	//test to see if node are not init
	assert(nodePred->getTypeNodeGraph() != INIT);

	//test if nodeData has not more than one successor
	if (this->getNbSucc() == 1 && !this->isNodeOutput()) {
		// Sauvegarde le numInput entre la data et le nodeGraphSuiv
		numInput = this->getEdgeSucc(0)->getNumInput();

		// nodePred --> noiseSourceNode --> Data(this) --> nodeGraphSuiv
		nodePred->deleteEdgeDirectedTo(noiseSourceNode);
		// nodePred : noiseSourceNode --> Data(this) --> nodeGraphSuiv
		noiseSourceNode->deleteEdgeDirectedTo(this);
		// nodePred : noiseSourceNode : Data(this) --> nodeGraphSuiv
		this->deleteEdgeDirectedTo(nodeGraphSuiv);
		// nodePred : noiseSourceNode : Data(this) : nodeGraphSuiv

		nodePred->edgeDirectedTo(this);
		// nodePred --> Data(this) : noiseSourceNode : nodeGraphSuiv
		this->edgeDirectedTo(noiseSourceNode);
		// nodePred --> Data(this) --> noiseSourceNode : nodeGraphSuiv
		noiseSourceNode->edgeDirectedTo(nodeGraphSuiv, numInput);
		// nodePred --> Data(this) --> noiseSourceNode --> nodeGraphSuiv


		if (((NoeudGFD *) nodeGraphSuiv)->getTypeNodeGFD() == DATA && ((NoeudData *) nodeGraphSuiv)->getTypeNodeData() == DATA_SRC_BRUIT) {
			// The node just below us is a noise node, it is our only successor and it is in the same block as the other noise source we are handling
			//we need to merge 2 noise sources: noiseSourceNode and nodeGraphSuiv
			//we add current noise source parameter to next noise source: nodeGraphSuiv

			((NoeudSrcBruit *) nodeGraphSuiv)->calculSumParamNoise((NoeudSrcBruit *) (noiseSourceNode)); //add parameter

			//noeudPred-->Data-->Br(noiseSourceNode)-->Br(nodeGraphSuiv)-->OPS   before
			this->deleteEdgeDirectedTo(noiseSourceNode);
			noiseSourceNode->deleteEdgeDirectedTo(nodeGraphSuiv);
			this->edgeDirectedTo(nodeGraphSuiv);
			//noeudPred-->Data-->Br(nodeGraphSuiv)-->OPS   after

			//no recursion to continue move: this is end of process on noiseSourceNode which is merged
			//in nodeGraphSuiv noise source-->these noise source is processed after with the tabNoiseSourcesNode

		}
		else {
			//recursion to continue move
			for (int i = 0; i < noiseSourceNode->getNbSucc(); i++) {
				((NoeudGFD*)noiseSourceNode->getElementSucc(i))->moveNoiseSource(noiseSourceNode);
			}
		}
	}
}

list<NoeudData*> NoeudData::detectionBrCorrelated(list< list<NoeudData*> >* tabListBr){
	list<NoeudData*> listMerge, temp;
	list<NoeudData*>::iterator itList;
	list< list<NoeudData*> >::iterator itList2;

	NoeudSrcBruit* brPred;
	assert(this->getNbPred() == 1);


	if(!this->isNodeOutput()){
		if(this->getNbSucc() > 1 && this->getElementPred(0)->getTypeNodeGraph() == GFD && ((NoeudGFD*) this->getElementPred(0))->getTypeNodeGFD() == DATA && ((NoeudData*) this->getElementPred(0))->getTypeNodeData() == DATA_SRC_BRUIT){ // Cas ou il y a un Br devant la donnée ayant plus d'une sortie. Nous devons sauvegarder chaque branche et returner la liste des sorties pour la suite
			brPred = dynamic_cast<NoeudSrcBruit*> (this->getElementPred(0));
			assert(brPred != NULL);


			// Si nous sommes deja passé par ce noeud et que nous avons sauvegardé les données, il suffit juste de les fusionner et de les rendre
			if(tabListBr[brPred->getNumSrc() - 1].size() != 0){
				for(itList2 = tabListBr[brPred->getNumSrc() - 1].begin() ; itList2 != tabListBr[brPred->getNumSrc() - 1].end() ; itList2++){
					for(itList = (*itList2).begin() ; itList != (*itList2).end() ; itList++){
						listMerge.push_back(*itList);
					}
				}
				return listMerge;
			}


			listMerge = ((NoeudGFD*)this->getElementSucc(0))->detectionBrCorrelated(tabListBr);
			tabListBr[brPred->getNumSrc() - 1].push_back(listMerge); // Sauvegarde de la branche

			for(int i = 1 ; i < this->getNbSucc() ; i++){
				temp = ((NoeudGFD*)this->getElementSucc(i))->detectionBrCorrelated(tabListBr);
				tabListBr[brPred->getNumSrc() - 1].push_back(temp); // Sauvegarde de la branche
				for(itList = temp.begin() ; itList != temp.end() ; itList++){
					listMerge.push_back(*itList);
				}
			}
		}
		else{ // Cas ou il n'y a pas de Br devant une donnée ayant la possibilité d'avoir plus de 1 successeur
			listMerge = ((NoeudGFD*)this->getElementSucc(0))->detectionBrCorrelated(tabListBr);
			for(int i = 1 ; i < this->getNbSucc() ; i++){
				temp = ((NoeudGFD*)this->getElementSucc(i))->detectionBrCorrelated(tabListBr);
				for(itList = temp.begin() ; itList != temp.end() ; itList++){
					listMerge.push_back(*itList);
				}
			}
		}
	}
	else{
		listMerge.push_back(this);
	}

	return listMerge;
}


list<NoeudData*> NoeudOps::detectionBrCorrelated(list< list<NoeudData*> >* tabListBr){
	assert(this->getNbSucc() == 1);
	return ((NoeudGFD*) this->getElementSucc(0))->detectionBrCorrelated(tabListBr);
}
