/*!
 *  \author Daniel Menard, Mohammad DIAB, Jean-Charles Naud
 *  \date   28.06.2002
 *  \version 1.0
 */

#include "graph/dfg/Gfd.hh"
#include "noise/T1_Group.hh"

#include "utils/Tool.hh"

void T1_SystemNoiseModelDetermination(Gfd* gfd) {
	Chrono chrono;

	trace_output(TOOL_STEP_2_1_1);
	chrono.start();
	T11_NoiseSourceInsertion(gfd);
	chrono.stop();
	RuntimeParameters::timing("T11 Noise source insertion", &chrono);

	trace_output(TOOL_STEP_2_1_2);
	chrono.start();
	T12_NoiseDataAndOperatorModelInsertion(gfd);
	chrono.stop();
	RuntimeParameters::timing("T12 Noise data&operator model insertion", &chrono);
}
