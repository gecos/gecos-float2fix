#include "noise/NoiseFunctionGeneration.hh"


/**
 *  This part describes the transformation T_3  corresponding to the determination of the noise power expression
 *  	- Generation of matlab file : genereMatFileParamFT()
 *  	-
 *
 *
 *  \ingroup T3
 *  \author Cloatre loic
 *  \date  25/05/2009
 */
void NoiseFunctionGeneration(Gft* gft, Gfd* gfd) {
	Chrono chrono;
	chrono.start();

#if SAVE_GRAPH
	gft->saveGf("T30.GFT.A");
#endif

	gft->moveAllPhisDown();
	gft->saveGf("T30.GFT.Z");

    gft->generateNoiseFunction(gfd);

	chrono.stop();
	RuntimeParameters::timing("ComputePb generation", &chrono);
}
