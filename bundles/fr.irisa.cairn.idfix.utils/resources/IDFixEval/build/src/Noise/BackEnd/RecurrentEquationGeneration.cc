/*
 * RecurrentEquationGeneration.cc
 *
 *  Created on: Mar 18, 2015
 *      Author: Nicolas Simon
 */

#include "noise/RecurrentEquationGeneration.hh"

#include <fstream>
#include <list>

#include "utils/Tool.hh"

//void IncludeWriter(ofstream &file);
void BeginMainFunctionWriter(ofstream &file);
void EndMainFunctionWriter(ofstream &file);
void RecurrentEquationWriter(ofstream &file, LinearFunction *linearFunction);

void RecurrentEquationGeneration(list<LinearFunction*> linearFunctions){
	std::ofstream outfile(string(ProgParameters::getOutputGenDirectory() + "recurrent_equation.log").c_str(), ofstream::out);

	list<LinearFunction*>::const_iterator itLinearFunction;
	for(itLinearFunction = linearFunctions.begin() ; itLinearFunction != linearFunctions.end() ; itLinearFunction++){

	}
}

void BeginMainFunctionWriter(ofstream &file){
	file << "int main(){" << std::endl;
}

void EndMainFunctionWriter(ofstream &file){
	file << "}";
}

void RecurrentEquationWriter(ofstream &file, LinearFunction *linearFunction){

}
