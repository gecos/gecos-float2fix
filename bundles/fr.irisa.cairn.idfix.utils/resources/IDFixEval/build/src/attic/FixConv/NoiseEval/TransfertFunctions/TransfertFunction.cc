/*
 * TransfertFunction.cc
 *
 *  Created on: 12 févr. 2009
 *      Author: cloatre
 */

#include <TransfertFunction.hh>

#include <graph/lfg/EdgeGFL.hh>
#include <graph/lfg/NoeudGFL.hh>
#include <typeinfo>

#include "LinearFonc.hh"
#include "LinearTransfertFunction.hh"

//=================definition constructeur/destructeur
/**
 *  constructeur de recopie
 *
 *  \param copie pointeur sur le noeud a recopier
 *  by Loic Cloatre creation le 12/02/2009
 */
TransfertFunction::TransfertFunction(const TransfertFunction & other) {
	m_realTF = other.m_realTF->copie();
}

TransfertFunction::TransfertFunction(TFImpl * letter) {
	m_realTF = letter;
}

TransfertFunction::TransfertFunction(const VarLinearFonc & num, const VarLinearFonc & den, const string Name) {
	m_realTF = new LinearTransfertFunction(num, den, Name);
}

/**
 *  set du nom (avec allocation new string)
 *
 *  \param  nom nom qu'on va recopier dans un nouvel element
 *
 *  by Loic Cloatre creation le 12/02/2009
 */
void TransfertFunction::setName(string nom) {
	m_realTF->m_Name = nom;
}

/**
 *  calcul fonction de transfert entre deux points d'un GFL
 *
 *  \param  depart	noeud depart
 *  \param  arrivee	noeud arrivee
 *  \return TransfertFunction *	nouvelle fonction de transfert resultat
 *
 *  by Loic Cloatre creation le 12/02/2009
 */
TransfertFunction *TransfertFunction::calculFTEntreDeuxNoeuds(NoeudGFL * depart, NoeudGFL * arrivee) {
	//si le noeud arrivee contient un cycle unitaire sur lui meme de fonction lineaire fct(ww): il y aura un denominateur
	//exemple: U--------->W
	//Huw = fct(wu)  / 1 - fct(ww)

	TransfertFunction *ret = NULL;
	VarLinearFonc *numRet = NULL;
	VarLinearFonc denRet;
	VarLinearFonc *tempVarLinearFonc = NULL;
	string nameRet;
	int numeroSuccesseur = 0; // le numero du successeur pour le circuit unitaire

	if ((depart == NULL) || (arrivee == NULL)) {
		trace_error("calculFTEntreDeuxNoeuds (depart==NULL) || (arrivee==NULL)");
		exit(-1);
	}
	else {
		//recherche de l'arc pour obtenir le numerateur de la FT
		for (int i = 0; i < depart->getNbSucc(); i++) {
			if (((NoeudGFL *) depart->getElementSucc(i))->getName() == arrivee->getName().c_str()) {
				//numRet= depart->getFormatEdgeSucc(i)->GetMfct();
				numRet = ((EdgeGFL *) depart->getEdgeSucc(i))->getTypeVarFonctionLineaire(); //on recupere la fonction lineaire du format
				break;
			}
		}

		//calcul du denominateur de la FT( 1 si pas de boucle)
		if (arrivee->detectionCircuitUnitaire(&numeroSuccesseur) == true) //recherche si circuit
		{
			tempVarLinearFonc = new VarLinearFonc("+1");
			tempVarLinearFonc->AjoutEltZ(0, "1");
			//denRet = tempVarLinearFonc->operator -(((NoeudGFL*)arrivee)->getFormatEdgeSucc(*numeroSuccesseur)->GetMfct());
			EdgeGFL *temp = (EdgeGFL *) arrivee->getEdgeSucc(numeroSuccesseur);
			VarLinearFonc *temp1 = temp->getTypeVarFonctionLineaire();
			denRet = tempVarLinearFonc->operator -(*temp1);

			delete tempVarLinearFonc;
		}
		else {
			denRet = VarLinearFonc("1"); //il n'y a pas de boucle
			denRet.AjoutEltZ(0, "0"); //le den vaut donc 1
		}

		//calcul du nom de la fonction de transfert
		string temp1(depart->getName());
		string temp2(arrivee->getName());
		string temp("H(");
		temp = temp + temp2 + temp1 + ")";

		if (numRet != NULL) {
			if (numRet->empty())
				numRet->AjoutEltZ(0, "0"); // Sinon on ne génère pas un script matlab correct
			ret = new TransfertFunction(*numRet, denRet, temp);
		}
		else {
			VarLinearFonc num("empty");
			num.AjoutEltZ(0, "1");
			ret = new TransfertFunction(num, denRet, temp);
		}
	}

	return ret;
}

/**
 * mise a NULL des 3 pointeurs attributs
 *
 *  by Loic Cloatre creation le 23/03/2009
 */
/*
 void  	TransfertFunction::RAZ()
 {
 delete denominateur;
 delete numerateur;

 this->denominateur=NULL;
 this->numerateur=NULL;
 this->name="";
 }
 */

TransfertFunction TransfertFunction::operator*(const TransfertFunction & op) const {
	return m_realTF->multiply(*op.m_realTF);
}

TransfertFunction TransfertFunction::operator+(const TransfertFunction & op) const {
	return m_realTF->add(*op.m_realTF);
}

TransfertFunction & TransfertFunction::operator=(const TransfertFunction & other) {
	delete m_realTF;
	m_realTF = other.m_realTF->copie();
	return *this;
}

TFImpl::~TFImpl() {
}
