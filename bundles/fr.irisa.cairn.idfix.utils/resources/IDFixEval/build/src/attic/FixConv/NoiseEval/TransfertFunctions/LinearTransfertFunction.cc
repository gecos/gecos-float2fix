#include "LinearTransfertFunction.hh"

#include "LinearFonc.hh"
#include "TransfertFunction.hh"

/*
 LinearTransfertFunction::LinearTransfertFunction()
 {
 this->denominateur = new VarLinearFonc("numerateur");
 this->numerateur = new VarLinearFonc("denominateur");
 }*/

LinearTransfertFunction::LinearTransfertFunction(const LinearTransfertFunction & other) :
	TFImpl(other) {
	this->denominateur = other.GetDenominateur();
	this->numerateur = other.getNumerateur();
}

/**
 *  constructeur a partir du numerateur et denominateur
 *
 *  \param num numerateur de la FT
 *  \param den denominateur de la FT
 *  \param name nom de la FT
 *  by Loic Cloatre creation le 12/02/2009
 */
LinearTransfertFunction::LinearTransfertFunction(const VarLinearFonc & num, const VarLinearFonc & den, string name) :
	TFImpl(name) {
	this->denominateur = den;
	this->numerateur = num;
}

/**
 *  destructeur
 *
 *  by Loic Cloatre creation le 12/02/2009
 */
LinearTransfertFunction::~LinearTransfertFunction() {
}

LinearTransfertFunction &
LinearTransfertFunction::operator=(const LinearTransfertFunction & other) {
	if (&other == this)
		return *this;

	denominateur = other.denominateur;
	numerateur = other.numerateur;
	m_Name = other.m_Name;

	return *this;
}

/**
 *  set du numerateur (avec allocation new VarLinearFonc)
 *
 *  \param  num	VarLinearFonc qu'on va recopier dans un nouvel element
 *
 *  by Loic Cloatre creation le 12/02/2009
 */
void LinearTransfertFunction::SetNumerateur(const VarLinearFonc & num) {
	this->numerateur = num;
}

/**
 *   set du denominateur (avec allocation new VarLinearFonc)
 *
 *  \param  den	VarLinearFonc qu'on va recopier dans un nouvel element
 *
 *  by Loic Cloatre creation le 12/02/2009
 */
void LinearTransfertFunction::SetDenominateur(const VarLinearFonc & den) {
	this->denominateur = den;
}

/**
 Compute the addition with another TFImpl. The other may be a LinearTransfertFunction
 or something else. This function decides what to do from the actual type.

 The returned object will be either a LinearTransfertFunction or some other kind, depending
 on what's added.

 This function is called from TransfertFunction::operator+ .
 
 \param  op2	fonction de transfert a ajouter
 \return TransfertFunction nouvelle fonction de transfert resultat
 by Loic Cloatre creation le 12/02/2009
 */
TransfertFunction LinearTransfertFunction::add(const TFImpl & op2) const {
	const LinearTransfertFunction & op = dynamic_cast<const LinearTransfertFunction &> (op2);
	VarLinearFonc temp;
	VarLinearFonc temp2;
	VarLinearFonc numRet; //numerateur du retour
	VarLinearFonc denRet; //denominateur du retour
	string nomSomme; //nom du retour
	bool deuxOperandeOK = false;

	if (this == NULL) //verification que les fonctions de transferts existent
	{
		numRet = op.getNumerateur(); //l'operande de gauche n'existe pas
		denRet = op.GetDenominateur();
		nomSomme = op.getName();
	} /*
	 else if(op==NULL && this!=NULL)
	 {
	 numRet = new VarLinearFonc(this->getNumerateur());                                                                          //l'operande de droite n'existe pas
	 denRet = new VarLinearFonc(this->GetDenominateur());
	 nomSomme = this->name;
	 } */
	else if (this->denominateur == op.GetDenominateur()) //si les denominateurs sont les memes
	{
		numRet = this->numerateur + op.getNumerateur(); //appel de la methode de calcul de somme de VarLinearFonc
		denRet = this->denominateur; //denominateur commun
		deuxOperandeOK = true;
	}
	else //mise sur denominateur commun:  A/B   +   C/D  = (AD + CB)/BD
	{
		temp = this->numerateur * op.GetDenominateur(); //appel de la methode de calcul de produit de VarLinearFonc
		temp2 = this->denominateur * op.getNumerateur(); //appel de la methode de calcul de produit de VarLinearFonc
		numRet = temp + temp2;

		denRet = this->denominateur * op.GetDenominateur(); //appel de la methode de calcul de produit de VarLinearFonc
		deuxOperandeOK = true;
	}

	if (deuxOperandeOK == true) {
		nomSomme = getName();
		if (!op.getName().empty()) {
			nomSomme += "+";
			nomSomme += op.getName();
		}
	}

	return TransfertFunction(numRet, denRet, nomSomme);
}

/**
 *  redefinition operateur*
 *
 *  \param  op	fonction de transfert a multiplier
 *  \return TransfertFunction *	nouvelle fonction de transfert resultat
 *
 *  by Loic Cloatre creation le 12/02/2009
 */
TransfertFunction LinearTransfertFunction::multiply(const TFImpl & op2) const {
	const LinearTransfertFunction & op = dynamic_cast<const LinearTransfertFunction &> (op2);
	VarLinearFonc numRet; //numerateur du retour
	VarLinearFonc denRet; //denominateur du retour
	string nomSomme; //nom du retour

	if (this != NULL) //dans le cas ou: A = A*B et que A n'existe pas encore
	{
		numRet = this->numerateur * op.getNumerateur(); //appel de la methode de calcul de produit de VarLinearFonc
		denRet = this->denominateur * op.GetDenominateur(); //appel de la methode de calcul de produit de VarLinearFonc
		nomSomme = getName();
		nomSomme += ".";
		nomSomme += op.getName();
		return TransfertFunction(numRet, denRet, nomSomme);
	}
	else {
		return TransfertFunction(new LinearTransfertFunction(op));
	}
}

/**
 Generate the matlab expression of this TF. This expression is used in matlab to
 actually compute the noise power in the GFT in T3 step.

 \param entree Start node for the function
 \param sortie End node for the function
 */
string LinearTransfertFunction::getMatlabExpression(int numEdge) const {
	ostringstream expression;

	if(RuntimeParameters::isLTIGraph()){
		expression << "H(" << numEdge << ").Num = " << getNumerPourMatlab() << ";\n";
		expression << "H(" << numEdge << ").Den = " << GetDenomPourMatlab() << ";\n";
		expression << "H(" << numEdge << ").tf  = tf(H(" << numEdge << ").Num, H(" << numEdge << ").Den, 1, 'variable', 'z^-1');\n\n";
	}
	else{
		expression << "H(" << numEdge << ").Num(1).g = " << getNumerPourMatlab() << ";\n";
		expression << "H(" << numEdge << ").Den(1).f = " << GetDenomPourMatlab() << ";\n";
		expression << "H(" << numEdge << ").tf = ft(H(" << numEdge << "));\n\n";
	}
	return expression.str();
}
/**
 *  get du format du numerateur [1 A0 A1 A2]
 *
 *  \return string* du numerateur au format  [1 A0 A1 A2]
 *
 *  by Loic Cloatre creation le 23/02/2009
 */
string LinearTransfertFunction::getNumerPourMatlab() const {
	return this->numerateur.GetExpressionPourMatlab();
}

/**
 *  get du format du denominateur [1 B0 B1 B2]
 *
 *  \return string* du numerateur au format  [1 B0 B1 B2]
 *
 *  by Loic Cloatre creation le 23/02/2009
 */
string LinearTransfertFunction::GetDenomPourMatlab() const {
	string ret;
	ret = this->denominateur.GetExpressionPourMatlab();
	//cout<<"debug lcl: denominateur: "<<ret->c_str()<<endl;
	if (ret == "[0  ]") {
		//cout<<"div par zero"<<endl;
		ret = "[1 ]";
	}
	return ret;
}

