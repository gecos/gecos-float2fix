/**
 \class LinearTransfertFunction
 \date 25.11.2010
 \author Adrien Destugues
 \brief Class for representing a linear transfert function

 This is a regular linear transfert function with a numerator and denominator.
 It is not meant to be used directly, but inside a TransfertFunction wrapper.
 */

#ifndef __LINEARTF_H__
#define __LINEARTF_H__

#include <utils/Tool.hh>

#include "TransfertFunction.hh"
#include "LinearFonc.hh"

class LinearTransfertFunction: public TFImpl {
private:
	VarLinearFonc numerateur;
	VarLinearFonc denominateur;

	TFImpl* copie() {
		return new LinearTransfertFunction(*this);
	}
public:
	LinearTransfertFunction(const VarLinearFonc& num, const VarLinearFonc& den, string name);
	//constructeur a partir du numerateur et denominateur
	LinearTransfertFunction(const LinearTransfertFunction& other);
	~LinearTransfertFunction();
	LinearTransfertFunction& operator=(const LinearTransfertFunction& other);

	void SetNumerateur(const VarLinearFonc& num);
	/// set du numerateur (avec allocation new VarLinearFonc) */
	void SetDenominateur(const VarLinearFonc& den);
	/// set du denominateur (avec allocation new VarLinearFonc)  */
	VarLinearFonc getNumerateur() const {
		return numerateur;
	}
	/// get du numerateur  */
	VarLinearFonc GetDenominateur() const {
		return denominateur;
	}
	/// get du denominateur   */

	string getNumerPourMatlab() const;
	/// get du format du numerateur [1 A0 A1 A2] */
	string GetDenomPourMatlab() const;
	/// get du format du denominateur [1 B0 B1 B2] */

	TransfertFunction add(const TFImpl& op) const; /// redefinition operateur+  */
	//TransfertFunction operator-(const LinearTransfertFunction& op) const;						/// redefinition operateur-  */
	TransfertFunction multiply(const TFImpl& op) const; /// redefinition operateur-  */

	string getMatlabExpression(int numEdge) const;
	string ToString() const {
		string result;
		result = getNumerPourMatlab();
		if (!GetDenomPourMatlab().empty())
			result += "/\n" + GetDenomPourMatlab();
		return result;
	}
};

#endif
