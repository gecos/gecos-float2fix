///*!
// *  \author Berthau Florian
// *  \date   2.06.2010
// *  \version 1.0
// */
//
//#include <ctime>
//#include <iostream>
//#include <math.h>
//#include <string>
//
//#include "TypeVar.hh"
//#include "Gfd.hh"
//#include "GraphPath.hh"
//#include "NoeudOps.hh"
//#include "NoeudGFL.hh"
//#include "NoeudSrcBruit.hh"
//#include "Tool.hh"
//
//#include "T2_Group.hh"
//
//using namespace std;
//
///**
// *  Determination of the set of pseudo-transfer functions which defines the system nonLinear
// *  	- Cycle dismantling : cycleDismantlingJohnson()
// *  			- Cycle Detection : cycleIsPresent()
// *  			- Cycle Enumeration : Enumeration()
// *  			- Directed Acyclic Graphs Generation
// *  	- Recurrent equation determination : calculFT()
// *  	- partial pseudo-transfer function determination : mergeOfDifferentsGFL()
// *  	- global pseudo-transfer function : executionOfVariableSubstitution()
// *
// *
// *  \ingroup T2
// *  \author Nicolas Simon
// *  \date  15/06/2010
// */
//Gft *Gfd::systemNonLTIExpressionDetermination() {
//	ListNoeudGraph::iterator itNG;
//	ListSuccPred::iterator itPred;
//	Chrono* chrono = new Chrono();
//
//	trace_output(TOOL_STEP_2_2);
//	trace_output(TOOL_STEP_2_2_1);
//	trace_output(TOOL_STEP_2_2_1_1);
//
//	chrono->start();
//	T21_CycleDismantling(this);
//
//	chrono->stop();
//	RuntimeParameters::timing("T21 GFD dismantling", chrono);
//
//	this->liaisonsSuccPredInitFin();
//
//	/* Transformation T22 : Recurrent equation determination */
//	trace_output(TOOL_STEP_2_2_2);
//
//	chrono->start();
//	this->resetInfoVisite(); //on reset l'info pour le parcours recursif du calcul des fonctions lineaires
//
//	// Utilisation de la nouvelle structure des fonctions lineaire et des différents algoritme associé
//	NoeudData* out;
//	list<FonctionLineaire*> fctOut;
//	for(int i = 0 ; i < this->getNbOutput() ; i++){
//		out = dynamic_cast<NoeudData*> (this->getOutputGf(i));
//		if(out->getTypeSignalBruit() == BRUIT){
//			fctOut.push_back(out->calculLinearFoncNonLTI());
//			fctOut.back()->setNode(out);
//		}
//	}
//
//	this->resetInfoVisite(); //on reset l'info visite du calcul des fonctions lineaires
//
//#if DEBUG
//    cout << endl << "Linear function display" << endl;
//	list<FonctionLineaire*>::iterator itListAffichage;
//	for(itListAffichage = fctOut.begin() ; itListAffichage != fctOut.end() ; itListAffichage++){
//		(*itListAffichage)->print();
//	}
//
//    cout << "Nb noise node : " << this->getNbNoiseNode() << endl;
//    for(int i = 0 ; i < this->getNbNoiseNode() ; i++){
//        cout << this->getNoiseNode(i)->getName() << " ";
//    }
//    cout << endl << endl;
//
//
//    cout << "Nb output node : " << this->getNbOutput() << endl;
//    for(int i = 0 ; i < this->getNbOutput() ; i++){
//        cout << this->getOutputGf(i)->getName() << " ";
//    }
//    cout << endl << endl;
//
//
//    cout << "Nb linear function : " << fctOut.size() << endl;
//	for(itListAffichage = fctOut.begin() ; itListAffichage != fctOut.end() ; itListAffichage++){
//		cout << (*itListAffichage)->getNbPseudoFctLineaire() << " ";
//	}
//    cout << endl;
//#endif
//
//	Gfl *GflGetFromT22 = new Gfl(fctOut, getOutputBlock(), this->getNoiseEval()); //construction du GFL associe au GFD
//
//
//#if DEBUG
//    cout << "Nb input node : " << GflGetFromT22->getNbInput() << endl;
//    for(int i = 0 ; i < GflGetFromT22->getNbInput() ; i++){
//        cout << GflGetFromT22->getInputGf(i)->getName() << " ";
//    }
//    cout << endl << endl;
//
//    cout << "Nb output node : " << GflGetFromT22->getNbOutput() << endl;
//    for(int i = 0 ; i < GflGetFromT22->getNbOutput() ; i++){
//        cout << GflGetFromT22->getOutputGf(i)->getName() << " ";
//    }
//    cout << endl << endl;
//#endif
//
//	Gfl *GflRegroupe = GflGetFromT22->mergeOfDifferentsGFL2(); //fonction pour regrouper les petits GFL
//
//#if DEBUG
//    cout << "Nb input node : " << GflRegroupe->getNbInput() << endl;
//    for(int i = 0 ; i < GflRegroupe->getNbInput() ; i++){
//        cout << GflRegroupe->getInputGf(i)->getName() << " ";
//    }
//    cout << endl << endl;
//
//    cout << "Nb output node : " << GflRegroupe->getNbOutput() << endl;
//    for(int i = 0 ; i < GflRegroupe->getNbOutput() ; i++){
//        cout << GflRegroupe->getOutputGf(i)->getName() << " ";
//    }
//    cout << endl << endl;
//#endif
//
//	GflRegroupe->cycleReduction();
//#if SAVE_GRAPH
//	GflRegroupe->saveGf("T24.GFL.A");
//#endif
//
//	GflRegroupe->mergeGflNode();
//#if SAVE_GRAPH
//	GflRegroupe->saveGf("T24.GFL.B");
//#endif
//
//	Gft *GrapheFoncTrans = new Gft(*GflRegroupe);
//#if SAVE_GRAPH
//	GrapheFoncTrans->saveGf("T24.GFT.A");
//#endif
//
//	chrono->stop();
//	RuntimeParameters::timing("T22/23/24 GFL creation", chrono);
//
//	delete GflRegroupe;
//	return GrapheFoncTrans;
//}
//
///**
// *	Fonction récursive permettant le calcul des fonction lineaire du systeme (NON LTI)
// *
// *	\return Fonction Lineaire associé au noeudData
// *	\date 18\11\11
// *	\author Nicolas Simon
// */
//FonctionLineaire* NoeudData::calculLinearFoncNonLTI(){
//	NoeudGraph* NG;
//	NoeudOps* nOps;
//	PseudoFonctionLineaire* pseudoFct;
//	FonctionLineaire* out;
//
//	// Rend la sauvegarde de la fonction lineaire
//	if(this->getPtrFonctionLineaire() != NULL){
//		return new FonctionLineaire(*this->getPtrFonctionLineaire());
//	}
//
//	if(!this->getTypeSimu() && !this->isNodeSource()){
//		assert(this->getNbPred() == 1);
//		nOps = dynamic_cast<NoeudOps*> (this->getElementPred(0));
//		assert(nOps != NULL);
//		out = nOps->calculLinearFoncNonLTI();
//	}
//	else if(this->isNodeInput()){
//		if(this->getTypeNodeData() == DATA_SRC || this->getTypeNodeData() == DATA_SRC_BRUIT || (this->isNodeNoeudDemantele() && this->getTypeSignalBruit() == BRUIT)){
//			pseudoFct = new PseudoFonctionLineaire(this);
//			pseudoFct->addEltZ(0,1,"1");
//			out = new FonctionLineaire(this);
//			out->addPseudoFctLineaire(pseudoFct);
//		}
//		else
//		{
//		 out = new FonctionLineaire(this);
//		 }
//	}
//	else if(this->getTypeSignalBruit() == BRUIT)
//		out = new FonctionLineaire(this);
//	else
//		out = new FonctionLineaire(this->getValeur(), this);
//
//	// Créé une sauvegarde de la fonction lineaire pour eviter de la recalculer au prochaine passage
//	if(this->getNbSucc() > 1){
//		this->setPtrFonctionLineaire(new FonctionLineaire(*out));
//	}else{
//		NG = this->getElementSucc(0);
//		assert(NG != NULL);
//		if(!NG->isNodeSink()){
//			nOps = dynamic_cast<NoeudOps*>(NG);
//			assert(nOps != NULL);
//			if(nOps->getTypeOps() == OPS_PHI){
//				this->setPtrFonctionLineaire(new FonctionLineaire(*out));
//			}
//		}
//	}
//
//
//	return out;
//}
//
//FonctionLineaire* NoeudOps::calculLinearFoncNonLTI(){
//	FonctionLineaire* tabFct[this->getNbInput()];
//	FonctionLineaire* result;
//	NoeudData* nData;
//	PseudoFonctionLineaire* pseudoFct;
//
//
//
//	for(int i = 0 ; i < this->getNbInput() ; i++){
//		nData = dynamic_cast<NoeudData*> (this->getElementPred(i));
//		assert(nData != NULL);
//		tabFct[i] = nData->calculLinearFoncNonLTI();
//	}
//
//
//	switch(this->getTypeOps()){
//		case OPS_EQ :
//			result = tabFct[0];
//			break;
//
//		case OPS_ADD :
//			result = *tabFct[0] + *tabFct[1];
//			delete tabFct[0];
//		   	delete tabFct[1];
//			break;
//
//		case OPS_SUB :
//		    result = *tabFct[0] - *tabFct[1];
//			delete tabFct[0];
//		  	delete tabFct[1];
//			break;
//
//		case OPS_MULT :
//			result = *tabFct[0] * *tabFct[1];
//			delete tabFct[0];
//			delete tabFct[1];
//			break;
//
//		case OPS_DIV :
//			result = *tabFct[0] / *tabFct[1];
//			delete tabFct[0];
//			delete tabFct[1];
//			break;
//
//        case OPS_NEG:
//            result = -*tabFct[0];
//            delete tabFct[0];
//            break;
//
//		case OPS_DELAY :
//			tabFct[0]->operatorDelay();
//			result = tabFct[0];
//			break;
//
//		case OPS_PHI :
//			assert(getNbSucc() == 1);
//			pseudoFct = new PseudoFonctionLineaire(this);
//			pseudoFct->addEltZ(0,1, "1");
//			result = new FonctionLineaire(this); // Le nData ne sert a rien puisqu'il est écrasé
//			result->addPseudoFctLineaire(pseudoFct);
//			break;
//
//		default : {
//			trace_error("Opérateur non connu au niveau des traitements des fonctions linéaires");
//			exit(-1);
//				  }
//	}
//	return result;
//}
//
///**
// *	Fonction récursive permettant le calcul des fonction lineaire du systeme (NON LTI)
// *
// *	\return Fonction Lineaire associé au noeudData
// *	\date 18\11\11
// *	\author Nicolas Simon
// */
///*FonctionLineaire* NoeudData::calculLinearFoncNonLTI(){
//	NoeudGraph* NG;
//	NoeudOps* nOps;
//	PseudoFonctionLineaire* pseudoFct;
//	FonctionLineaire* out;
//
//	// Rend la sauvegarde de la fonction lineaire
//	if(this->getPtrFonctionLineaire() != NULL){
//		return new FonctionLineaire(*this->getPtrFonctionLineaire());
//	}
//
//	if(!this->getTypeSimu() && !this->isNodeSource()){
//		assert(this->getNbPred() == 1);
//		nOps = dynamic_cast<NoeudOps*> (this->getElementPred(0));
//		assert(nOps != NULL);
//		out = nOps->calculLinearFoncNonLTI();
//	}
//	else if(this->isNodeInput()){
//		if(this->getTypeNodeData() == DATA_SRC || this->getTypeNodeData() == DATA_SRC_BRUIT || (this->isNodeNoeudDemantele() && this->getTypeSignalBruit() == BRUIT)){
//			pseudoFct = new PseudoFonctionLineaire(this);
//			pseudoFct->addEltZ(0,1);
//			out = new FonctionLineaire(this);
//			out->addPseudoFctLineaire(pseudoFct);
//		}
//		else
//		{
//		 out = new FonctionLineaire(this);
//		 }
//	}
//	else if(this->getTypeSignalBruit() == BRUIT)
//		out = new FonctionLineaire(this);
//	else
//		out = new FonctionLineaire(this->getValeur(), this);
//
//	// Créé une sauvegarde de la fonction lineaire pour eviter de la recalculer au prochaine passage
//	if(this->getNbSucc() > 1){
//		this->setPtrFonctionLineaire(new FonctionLineaire(*out));
//	}else{
//		NG = this->getElementSucc(0);
//		assert(NG != NULL);
//		if(!NG->isNodeSink()){
//			nOps = dynamic_cast<NoeudOps*>(NG);
//			assert(nOps != NULL);
//			if(nOps->getTypeOps() == OPS_PHI){
//				this->setPtrFonctionLineaire(new FonctionLineaire(*out));
//			}
//		}
//	}
//
//
//	return out;
//}
//
//FonctionLineaire* NoeudOps::calculLinearFoncNonLTI(){
//	FonctionLineaire* tabFct[this->getNbInput()];
//	FonctionLineaire* result;
//	NoeudData* nData;
//	PseudoFonctionLineaire* pseudoFct;
//
//
//
//	for(int i = 0 ; i < this->getNbInput() ; i++){
//		nData = dynamic_cast<NoeudData*> (this->getElementPred(i));
//		assert(nData != NULL);
//		tabFct[i] = nData->calculLinearFoncNonLTI();
//	}
//
//
//	switch(this->getTypeOps()){
//		case OPS_EQ :
//			result = tabFct[0];
//			break;
//
//		case OPS_ADD :
//			result = *tabFct[0] + *tabFct[1];
//			delete tabFct[0];
//		   	delete tabFct[1];
//			break;
//
//		case OPS_SUB :
//			result = *tabFct[0] - *tabFct[1];
//			delete tabFct[0];
//		  	delete tabFct[1];
//			break;
//
//		case OPS_MULT :
//			result = *tabFct[0] * *tabFct[1];
//			delete tabFct[0];
//			delete tabFct[1];
//			break;
//
//		case OPS_DIV :
//			result = *tabFct[0] / *tabFct[1];
//			delete tabFct[0];
//			delete tabFct[1];
//			break;
//
//		case OPS_DELAY :
//			tabFct[0]->operatorDelay();
//			result = tabFct[0];
//			break;
//
//		case OPS_PHI :
//			assert(getNbSucc() == 1);
//			pseudoFct = new PseudoFonctionLineaire(this);
//			pseudoFct->addEltZ(0,1);
//			result = new FonctionLineaire(this); // Le nData ne sert a rien puisqu'il est écrasé
//			result->addPseudoFctLineaire(pseudoFct);
//			break;
//
//		default : {
//			trace_error("Opérateur non connu au niveau des traitements des fonctions linéaires");
//			exit(-1);
//				  }
//	}
//	return result;
//}*/
