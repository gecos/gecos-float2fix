/**
 \author: cloatre
 \class TransfertFunction
 \brief Black-box container for different types of transfert functions.

 This class is a wrapper around the different types of transfert functions. It allows using operators (* and +)
 on different types of transfert functions (linear, non linear, ...). The actual transfert function is derived
 from TFImpl. Heach subclass of TFImpl must handle operations with all the others on a case by case basis.
 TransfertFunction then selects the right operation depending on the type of the two operands.
 */

#ifndef TRANSFERTFUNCTION_HH_
#define TRANSFERTFUNCTION_HH_

#include <iostream>
#include <string>
using namespace std;

class VarLinearFonc;
class NoeudGFL;

/*
 Un petit mot sur le fonctionnement particuler de cette classe : on utilise le pattern "lettre/enveloppe".

 On a plusieurs types de fonctions de transferts (linéaires ou non, analysées ou simulées, ...).
 On souhaite utiliser les opérateurs + et *.

 Donc on veut avoir une classe abstraite de base et surcharger les opérateurs en fonction du type des objets.
 Cependant, en C++ un opérateur retourne un objet par valeur, donc il ne peut pas retourner un type abstrait.

 La solution est donc d'avoir une classe d'interface, l'enveloppe (ici TransfertFunction). Cette classe ne
 fait rien elle-même mais a un pointeur vers une implémentation (la lettre). Quand on appelle une méthode de
 l'enveloppe, le message est transmis à la lettre. On peut donc utiliser la lettre comme un objet normal
 sans s'inquiéter et le polymorphisme chiant est assuré à l'intèrieur.
 */

class TransfertFunction;

class TFImpl /* La lettre (classe abstraite, a surcharger par les différents types de FT) */
{
	friend class TransfertFunction; // TransfertFunction peut accéder aux trucs internes de cette classe.
	// Et personne d'autre ne peut, parce que tout va être protected.
	// De cette façon on est sur que tout le monde utilise toujours une enveloppe.
public:
	virtual ~TFImpl();
protected:
	TFImpl(const TFImpl& other) {
		m_Name = other.getName();
	}
	TFImpl(const string name) {
		m_Name = name;
	}
	string getName() const {
		return m_Name;
	}

	string m_Name;

private:
	virtual TransfertFunction multiply(const TFImpl&) const = 0;
	virtual TransfertFunction add(const TFImpl&) const = 0;
	virtual TFImpl* copie() = 0;
	virtual string getMatlabExpression(int numEdge) const = 0;

	virtual string ToString() const = 0;
};

class TransfertFunction /* L'enveloppe (classe pas abstraite, et la même pour tout le monde) */
{
private:
	TFImpl* m_realTF; /* la lettre (cachée dans l'enveloppe) */

public:
	//==========================================constructeurs:
	TransfertFunction(const VarLinearFonc& num, const VarLinearFonc& den, const string Name); // Construit une enveloppe avec une TF linéaire dedans
	TransfertFunction(TFImpl* letter); //constructeur d'enveloppage
	TransfertFunction(const TransfertFunction& other); //constructeur de recopie

	~TransfertFunction() {
		delete m_realTF;
	} //destructeur

	//==========================================accesseurs
	void setName(string nom);
	/// set du name (avec allocation) */
	//void  	RAZ();
	/// mise a NULL des 3 pointeurs attributs, sans desallocation memoire */

	string getName() const {
		return m_realTF->m_Name;
	}
	/// get du name */

	//Methodes utilisees pour generer le fichier matlab ParamFT.m
	string getMatlabExpression(int numEdge) const {
		return m_realTF->getMatlabExpression(numEdge);
	}

	//Methodes utilisees pour generer le fichier matlab ParamFTNonLti.m
	string getNumerPourMatlabNonLti(); /// get du format du numerateur [1 A0 A1 A2] */
	string GetDenomPourMatlabNonLti(); /// get du format du denominateur [1 B0 B1 B2] */


	// redefinition operateur: a faire dans les classes dérivées, pour gérer tous les cas. On ne peut pas retourner une 'TransfertFunction' pure virtual,
	// donc chaque classe doit gérer ses propres calculs. Par exemple lin+phi retourne phi, lin+lin retourne lin, ...
	TransfertFunction operator+(const TransfertFunction& op) const; /// redefinition operateur+  */
	//TransfertFunction& operator- (const TransfertFunction& op) const;						/// redefinition operateur-  */
	TransfertFunction operator*(const TransfertFunction& op) const; /// redefinition operateur*  */
	TransfertFunction& operator=(const TransfertFunction& other);

	void AffichetypeVarFonctionTransfert() const {
		cout << ToString();
	} /// affichage de la fonction de transfert  */
	string ToString() const {
		return m_realTF->ToString();
	}

	// Méthodes statiques
	static TransfertFunction* calculFTEntreDeuxNoeuds(NoeudGFL* depart, NoeudGFL* arrivee); /// calcul fonction de transfert entre deux points d'un GFL  */
};

#endif /* TRANSFERTFUNCTION_HH_ */
