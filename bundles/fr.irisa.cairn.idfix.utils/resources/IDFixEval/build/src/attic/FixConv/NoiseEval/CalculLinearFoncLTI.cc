/*!
 *  \author Daniel Menard, Mohammad DIAB, Jean-Charles Naud
 *  \date   28.06.2002
 *  \version 1.0
 */

// === Bibliothèques standard
#include <graph/dfg/datanode/NoeudSrcBruit.hh>
#include <graph/dfg/Gfd.hh>
#include <graph/lfg/GFL.hh>
#include <graph/lfg/NoeudGFL.hh>
#include <graph/tfg/GFT.hh>
#include <algorithm>
#include <ctime>
#include <iostream>
#include <math.h>
#include <noise/T2_Group.hh>
#include <utils/graph/cycledismantling/GraphPath.hh>
#include <utils/Tool.hh>
#include <string>

// === Bibliothèques non standard (utiliser dans les Methodes et typeVarFonctions)

/**
 *  Determination of the set of pseudo-transfer functions which defines the system
 *  	- Cycle dismantling : cycleDismantlingJohnson()
 *  			- Cycle Detection : cycleIsPresent()
 *  			- Cycle Enumeration : Enumeration()
 *  			- Directed Acyclic Graphs Generation
 *  	- Recurrent equation determination : calculFT()
 *  	- partial pseudo-transfer function determination : mergeOfDifferentsGFL()
 *  	- global pseudo-transfer function : executionOfVariableSubstitution()
 *
 *
 *  \ingroup T2
 *  \author Mohammad DIAB, Cloatre loic
 *  \date  10/02/2008
 */

//Gft *Gfd::systemExpressionDetermination() {
//	Chrono* chrono = new Chrono();
//
//
//	// Dementellement du des circuits si présent
//	chrono->start();
//	T21_CycleDismantling(this);
//	this->liaisonsSuccPredInitFin();
//	saveGf("T21.GFD.Z");
//	chrono->stop();
//	RuntimeParameters::timing("T21 GFD dismantling", chrono);
//
//	Gfl* GflRegroupe = T22_ReccurentEquationDetermination(this);
//
//	// Transformation T24 : global pseudo-transfer function
//	T23_PartialPseudoTransferFunctionDetermination(GflRegroupe);
//	Gft* GrapheFoncTrans = T24_GlobalPseudoTransferFunctionDetermination(GflRegroupe);
//	// CONSTRUCTION DU GFT (Graphe de typeVarFonctions de Transfert)
//	// Remplace T24 ?
//
//	delete GflRegroupe;
//	delete chrono;
//	return GrapheFoncTrans;
//}
