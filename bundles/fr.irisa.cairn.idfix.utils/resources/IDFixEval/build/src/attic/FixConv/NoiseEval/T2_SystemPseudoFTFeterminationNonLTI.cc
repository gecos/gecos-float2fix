/*
 * T2_SystemPseudoFTFeterminationNonLTI.cc
 *
 *  Created on: Aug 27, 2010
 *      Author: nicolas
 */

// === Bibliothèques standard
#include <ctime>
#include <iostream>
#include <math.h>
#ifndef OCTAVE
    #include <engine.h>
#endif
#include <string>

// === Bibliothèques non standard (utiliser dans les Methodes et typeVarFonctions)
#include <graph/dfg/Gfd.hh>
#include <graph/tfg/GFT.hh>
#include <utils/Tool.hh>

/*
 *	Methode regroupant la determination des typeSimu, la partie simulation, la génération du fichier ParamFTNonLti et l'éxécution de matlab
 *	
 *
 *	\author Nicolas Simon
 *	\date 01/07/11
 */

//Gft *Gfd::systemDeterminationNonLTI() {
//	// Création des path pour les fichiers nécessaire pour la simulation
//	string PathParamFTNonLti, PathSimulatedValues, PathMatlabArithmeticOpFile;
//	Chrono* chrono = new Chrono();
//
//	PathSimulatedValues = ProgParameters::getOutputGenDirectory() + "simulatedValues.m";
//
//	PathParamFTNonLti = ProgParameters::getOutputGenDirectory() + "ParamFTNonLti.m";
//
//	PathMatlabArithmeticOpFile = ProgParameters::getOutputGenDirectory() + "MatlabArithmericOp.m";
//	//-----------------------------------------------------------------------------
//
//	chrono->start();
//	this->addTypeSimuNode(); // Determination des noeuds typeSimu
//
//#if SAVE_GRAPH
//	this->saveGf("T21.GFD.A");
//#endif
//
//	this->genArithmeticOperation(PathMatlabArithmeticOpFile);
//
//	// Test puis generation du fichier simulatedValues.txt a partir du xml donné en entrée
//	if(ProgParameters::getSimulatedValuesPathFile().empty()){
//		trace_error("Fichier d'échantillons des variables simulées du code C non présent pour le traitement non LTI\n");
//		exit(1);
//	}
//	trace_debug("Création du fichier contenant les échantillons\n");
//	this->readSimulatedNode(ProgParameters::getSimulatedValuesPathFile().data(), PathSimulatedValues);
//
//	chrono->stop();
//	RuntimeParameters::timing("Pre-computing non-lti information", chrono);
//
//	// Determination des pseudo fonctions linéaire et création du GFL
//	Gft *gftResultat = this->systemNonLTIExpressionDetermination();
//
//	chrono->start();
//	trace_debug("Chemin de ParamFTNonLti : %s\n", PathParamFTNonLti.data());
//	gftResultat->genereMatFileParamFTNonLti(PathParamFTNonLti);
//	chrono->stop();
//	RuntimeParameters::timing("ParamFTNonLti generation", chrono);
//
//	//Execution de matlab
//	chrono->start();
//	string pathMatFile = ProgParameters::getResourcesDirectory();
//
//#ifndef OCTAVE
//	trace_debug("Lancement de Matlab...\n");
//	// Lancement de matlab
//	Engine *ep;
//	if (!(ep = engOpen(NULL))) {
//		trace_error("Can't start MATLAB engine");
//		exit(EXIT_FAILURE);
//	}
//	else {
//		/* Add path to allow access to matlab files when IDFix is run from another repository */
//		/* The directory added must contain the file CaculH.m */
//		string pathMatlabWorkspace = "addpath('" + ProgParameters::getResourcesDirectory() + "/NoisePower/')";
//		trace_debug("Matlab workspace path : %s\n", pathMatlabWorkspace.data());
//		engEvalString(ep, pathMatlabWorkspace.data());
//
//		string commandMatlab = "noisePowerNonLTI('" + pathMatFile + "','" + ProgParameters::getOutputGenDirectory() + "')";
//		trace_debug("commande matlab: %s\n", commandMatlab.data());
//		engEvalString(ep, commandMatlab.data());
//
//		//le fichier CalculRSBQ est genere et les resultats des gains aussi: on concatène les fichiers
//		//ainsi que la formule dispo a la fin du fichier floatTofix.cc
//
//		engClose(ep);
//
//	}
//#else
//	trace_debug("Lancement de Octave...\n");
//    string commandMatlab = "noisePowerNonLTI('" + pathMatFile + "','" + ProgParameters::getOutputGenDirectory() + "')";
//    trace_debug("octave command: %s\n", commandMatlab.data());
//    runOctaveCommand(ProgParameters::getResourcesDirectory()+ "NoisePower/", commandMatlab);
//#endif
//
//	chrono->stop();
//#ifndef OCTAVE
//	RuntimeParameters::timing("Matlab processing", chrono);
//#else
//	RuntimeParameters::timing("Octave processing", chrono);
//#endif
//
//	return gftResultat;
//}
