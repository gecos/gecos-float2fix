/**
 \author Adrien Destugues
 \date 14/10/2010

 Lecture du fichier probas.xml indiquant les probabilités de chaque bloc.
 */

#include <graph/dfg/Gfd.hh>
#include <map>
#include <stack>

#include <string.h>

#include <libxml/SAX2.h>
#include <utils/graph/Block.hh>
#include <utils/Tool.hh>


class t_probactx {
public:
	std::stack<std::pair<int, int> > blockStack;
	std::map<std::pair<int, int>, Block> &blockMap;

	t_probactx(map<std::pair<int, int>, Block> &bm) :
		blockMap(bm) {
	}
};

// Callback function for libxml - start of an XML element (a block)
void probaElementStart(void *ctx, const xmlChar * fullName, const xmlChar ** atts) {
	// Get the context
	t_probactx *context = (t_probactx *) ctx;

	if (!xmlStrEqual(fullName, BAD_CAST("block"))) {
		trace_warn("Element %s is not a block\n", fullName);
	}

	// Read the attributes (id and proba)
	int id = atoi((const char *) atts[1]);
	int alt = atoi((const char *) atts[3]);
	double proba = atof((const char *) atts[5]);

	std::pair<int, int> idalt(id, alt);

	// Set the probability
	context->blockMap.at(std::pair<int, int>(idalt)).setProbability(proba);

	// Set the parent block
	if (context->blockStack.empty()) {
		// This is the root block
		context->blockMap.at(idalt).setParent(NULL);
	}
	else {
		// This block parent's is the stack top
		context->blockMap.at(idalt).setParent(&context->blockMap. at(context->blockStack.top()));
	}

	// Put this block in the blockStack
	context->blockStack.push(idalt);
}

// Callback function for libxml - end of an XML element (a block)
void probaElementEnd(void *ctx, const xmlChar * fullName) {
	// Get the context
	t_probactx *context = (t_probactx *) ctx;

	// TODO : check the block matches the stack top ?
	// (not sure if we can get the attributes from here and we need the id)

	// TODO : check fullName == "Block"

	// Remove the block from the context
	context->blockStack.pop();
}

/**
 * Read the path file and fill in the data of blocks in the blockmap.
 *
 * Blocks must have been already created by reading the xml Graph.
 *
 * This function will add the probability and the nesting hierarchy between blocks.
 *
 *	\param  path : chemin du fichier xml où récuperer les probabilités
 *  \returns false on error
 */
bool Gfd::readProbas(const char *path) {
	// Setup XML parser
	// We use a SAX parser, not a DOM one, as it is better for this data
	// structure (we don't want a tree).
	// See http://www.jamesh.id.au/articles/libxml-sax/libxml-sax.html for
	// more information about XML parsing.
	xmlSAXHandler xmlHandler;

	// Set all pointers in the handler to NULL
	memset(&xmlHandler, 0, sizeof(xmlHandler));

	// Set callbacks we will use
	// xmlHandler.startDocument = probaDocumentStart;
	// Called when the document starts
	// xmlHandler.endDocument = probaDocumentEnd;
	// Called when the document ends
	xmlHandler.startElement = probaElementStart;
	// Called when an element (a block) starts
	xmlHandler.endElement = probaElementEnd;
	// Called when an element (a block) ends
	// xmlHandler.characters = probaCharacters;
	// Called when chars are found out of the xml syntax
	// xmlHandler.ignorableWhitespace = probaWhiteSpace;
	// Called for whitespace
	// xmlHandler.warning;
	// xmlHandler.error;
	// xmlHandler.fatalError;

	// Browse the file and examine the blocks
	t_probactx user_data(this->blocks);

	if (xmlSAXUserParseFile(&xmlHandler, &user_data, path) < 0) {
		trace_error("Parsing probabilities failed\n");
		return false;
	}
	return true;
}
