/**
 * \file GenerateCalculRSBQ.cc
 * \author QLM
 * \date  11 févr. 2011
 *
 * Fusion des anciens trois fichiers de génération vers un fichier unique (+ réécriture)
 * Génère le fichier ComputePb.c permettant de calculer la puissance du bruit
 */

// === Bibliothèques standard
#include <graph/dfg/datanode/NoeudSrcBruit.hh>
#include <graph/dfg/Gfd.hh>
#include <graph/dfg/operatornode/NoeudOps.hh>
#include <graph/dfg/TypePrint.hh>
#include <graph/tfg/GFT.hh>
#include <graph/toolkit/TypeVar.hh>
#include <fstream>
#include <iostream>
#include <math.h>
#include <utils/Tool.hh>
#include <sstream>

// === Bibliothèques non standard

// === Namespaces
using namespace std;

// Prototype des fonctions
void computePb_ecritureEntete(Gft * gft, Gfd * grapheDepart, fstream & fileCpp_pf, fstream & fileHpp_pf);
void computePb_declarationVariable(fstream & fileCpp_pf);
void computePb_ecritureWFPeff(Gfd * grapheDepart, fstream & fileCpp_pf);

/**
 * Main function to generate ComputePb file
 *
 *  \param  Gfl* gflRegroupe: GFL composed of one or more GFL
 *  \author Jean-Charles Naud
 */
void Gft::genereFileCalculRSQB(Gfd * grapheDepart) {
	fstream fileCpp_pf; // .c principale
	fstream fileCpp_func;
	fstream fileHpp_pf;
	fstream fileTool_pf;
	ListNoeudGraph::iterator it;
	string tempPathFile = ProgParameters::getOutputGenDirectory() + ProgParameters::getOutputFilename(); // Chemin du fichier ComputePb.cc
	string tempPathFileH = ProgParameters::getOutputGenDirectory() + "ComputePb.h";



	// Fichiers en entrée :
	//   - ValeurW_FP.m placé dans $(ResourcesDir)/ : largeur des données (copié dans ComputePB)
	//   - ValeurHgDb.m placé dans $(GeneratedDir)/ : gain (copié dans ComputePB)
	// Fichiers en sortie :
	//  - ComputePb.c placé dans $(OutputDirectory)/ (sortie principale)
	//  - ParamFT.m placé dans $(GeneratedDir)/

	assert((ProgParameters::getOutputFormat() == "C") || (ProgParameters::getOutputFormat() == "CJni"));
	assert(this->nbNode > 2); // Y a-t-il des Noeud dans le grahpe

	fileCpp_pf.open(tempPathFile.c_str(), fstream::out); // Ouverture du fichier en mode écriture
	assert(fileCpp_pf.is_open());

	fileHpp_pf.open(tempPathFileH.c_str(), fstream::out);
	assert(fileHpp_pf.is_open());

	trace_debug("Ouverture du fichier %s en écriture\n", tempPathFile.data());

	// === Écriture de l'entête du fichier .cc"
	computePb_ecritureEntete(this, grapheDepart, fileCpp_pf, fileHpp_pf);

	if(!RuntimeParameters::isLTIGraph() || RuntimeParameters::isRecursiveGraph()){
		fileCpp_pf << endl << "   // === Ecriture des gains entre les bruit et les sorties" << endl;
		this->computePb_ecritureGainBrVersSortie(grapheDepart, fileCpp_pf);
	}

	if (ProgParameters::getOutputFormat() == "C") {
		//fileCpp_pf << endl << "float ComputePb(int WFP[][3], int WFPquantMode[][3], int tabInputWd[], int tabInputWdquantMode[]){" << endl << endl;
		fileCpp_pf << endl << "float ComputePb(int *WFP, int *WFPquantMode, int *tabInputWd, int *tabInputWdquantMode){" << endl << endl;
	}
	if (ProgParameters::getOutputFormat() == "CJni") {
		fileCpp_pf << "/**" << endl;
		fileCpp_pf << " * Class:     fr_irisa_cairn_float2fix_noiseAndCost_NEvalLibNatives" << endl;
		fileCpp_pf << " * Method:    computePbCC" << endl;
		fileCpp_pf << " * Signature: ([LSolutionSpaceModel/Triplet;[Ljava/lang/String;)F" << endl;
		fileCpp_pf << " */" << endl;
		fileCpp_pf << "JNIEXPORT jfloat JNICALL Java_fr_irisa_cairn_idfix_optimization_utils_NEvalLibNatives_computePbCC" << endl;
		//fileCpp_pf << "(JNIEnv * env, jclass class, jobjectArray arrayObject, jobjectArray tabInputWd){" << endl;
		fileCpp_pf << "(JNIEnv * env, jclass class, jintArray jWFP, jintArray jWFPquantMode, jintArray jtabInputWd, jintArray jtabInputWdquantMode){" << endl;
	}
	// === Coeur du traitement
	fileCpp_pf << endl << "   // === Declaration des variables locales" << endl;
	computePb_declarationVariable(fileCpp_pf);

	fileCpp_pf << endl << "   // === Initialisation des Tableaux" << endl;
	grapheDepart->computePb_initialisationTableaux(fileCpp_pf);

	grapheDepart->computePb_WFP_cdfg_to_WFP_sfg(fileCpp_pf, fileHpp_pf);

	fileCpp_pf << endl << "   // === Ecriture des WFP_sfg_eff" << endl;
	grapheDepart->computePb_ecritureWFP_sfg_eff(fileCpp_pf, fileHpp_pf);

	fileCpp_pf << endl << "   // === Ecriture des K,Q et Mean et Var de chaque Bruit \"br\" " << endl;
	this->computePb_ecritureMeanVarBr(grapheDepart, fileCpp_pf, fileHpp_pf);

	fileCpp_pf << endl << "   // === Ecriture de l'expression du bruit Pby en sortie" << endl;
	this->computePb_ecritureExpressionPby(grapheDepart, fileCpp_pf);

	fileCpp_pf << endl << "}" << endl;

	fileCpp_pf.close();
	fileHpp_pf.close();
}

/**
 * Écriture de l'entête du fichier .cc
 * \author Jean-Charles Naud
 */
void computePb_ecritureEntete(Gft * gft, Gfd * grapheDepart, fstream & fileCpp_pf, fstream & fileHpp_pf) {

	// === Écriture de l'entête du fichier .cc


	fileHpp_pf << "#include <stdio.h>" << endl;
	fileHpp_pf << "#include <math.h>" << endl;
	fileHpp_pf << "#include <assert.h>" << endl;
	fileHpp_pf << "#include <stdlib.h>" << endl;

	if (ProgParameters::getOutputFormat() == "CJni") {
		fileHpp_pf << "#include <jni.h>" << endl << endl;
		fileHpp_pf << "#include \"natives.h\"" << endl;
	}
	else {
		fileHpp_pf << endl;
	}

	fileCpp_pf << "/**\n * \\file CalculRSQB.cc" << endl;
	fileCpp_pf << " * \\author Generated by ID.Fix" << endl;
	fileCpp_pf << " * \\brief Fichier généré automatiquement" << endl;
	fileCpp_pf << " */" << endl;
	fileCpp_pf << "#include \"ComputePb.h\"" << endl;

	fileCpp_pf << "#define NB_SRCE_NOISE " << grapheDepart->calculNombreSrcBruit() << endl;
	fileCpp_pf << "#define NB_OUTPUT " << gft->getNbOutput() << endl;
	fileCpp_pf << "#define NB_K_Q_ASSOCIATE " << grapheDepart->calculNombreKQAssocieSrcBruit() << endl;
	fileCpp_pf << "#define NB_CDFG " << grapheDepart->calculNombreCDFG() << endl;
	fileCpp_pf << "#define NB_SFG " << grapheDepart->calculNombreSFG() << endl << endl;
}

/**
 * Declaration des variables locales
 * \author Jean-Charles Naud
 */
void computePb_declarationVariable(fstream & fileCpp_pf) {
	/* Comment prendre en compte le type de bruit ? Est-ce à l'utilisateur de le spécifier ? */
	/* Pour l'instant le changer ici */
	/* Les variables suivantes sont déclarées en static, car sinon elles font péter la taille de la pile dans l'exécution native java... */

	fileCpp_pf << "   static float Xb_Mean[NB_SRCE_NOISE];" << endl;
	fileCpp_pf << "   static float Xb_Var[NB_SRCE_NOISE];" << endl << endl;

	fileCpp_pf << "   static int K[NB_K_Q_ASSOCIATE];" << endl;
	fileCpp_pf << "   static float Q[NB_K_Q_ASSOCIATE];" << endl<<endl;

	fileCpp_pf << "   float ret_Pby = 0 ;" << endl;
	fileCpp_pf << "   int WFP_sfg[NB_SFG*3];" << endl;
	fileCpp_pf << "   int WFP_sfg_eff[NB_SFG*3];" << endl;
	fileCpp_pf << "   int WFP_sfg_ModeQuantif[NB_SFG*3];" << endl << endl;
	fileCpp_pf << "   int i, j, itOut;" << endl;
#if DEBUG
	fileCpp_pf << "   FILE* pfile;" << endl;
#endif
	if(RuntimeParameters::isLTIGraph() && !RuntimeParameters::isRecursiveGraph()){
		if(RuntimeParameters::isBinaryWriteWithoutZero()){
			fileCpp_pf << "   unsigned short int nbBrDependant;"<< endl;
			fileCpp_pf << "   unsigned short int* brDependant = NULL;"<<endl;
			fileCpp_pf << "   int temp;"<<endl;
		}
		fileCpp_pf << "   long lSize;"<<endl;
		fileCpp_pf << "   size_t result;"<<endl;
		fileCpp_pf << "   float hSumCarre, hSumCroise;"<<endl;
		fileCpp_pf << "   char* pathFileGainBin = \""<<ProgParameters::getOutputGenDirectory()<<VALEUR_HG_DB_BIN<<"\";"<<endl;
		fileCpp_pf << "   FILE * fileGainBin;" << endl;
		fileCpp_pf << "   fileGainBin = fopen(pathFileGainBin, \"rb\");"<<endl;
		fileCpp_pf << "   if(fileGainBin == NULL){"<<endl;
		fileCpp_pf << "      printf(\"Erreur d'ouverture du fichier binaire des gains\");" << endl;
		fileCpp_pf << "      exit(0);" << endl;
		fileCpp_pf << "   }" << endl;


	}
#if DEBUG
		fileCpp_pf << "   pfile = fopen(\"noisefunction_debug.log\", \"w\");"<<endl;
		fileCpp_pf << "   if(pfile == NULL){"<<endl;
		fileCpp_pf << "      printf(\"Impossible to open the log debug file \");" << endl;
		fileCpp_pf << "      exit(0);" << endl;
		fileCpp_pf << "   }" << endl;
#endif
}

/**
 * Initialisation des Tableaux
 * \author Jean-Charles Naud
 */
void Gfd::computePb_initialisationTableaux(fstream & fileCpp_pf) {
	fileCpp_pf << "   for (j = 0; j < NB_SRCE_NOISE; j++) {" << endl;
	fileCpp_pf << "      Xb_Mean[j] = 0;" << endl;
	fileCpp_pf << "      Xb_Var[j] = 0;" << endl;
	fileCpp_pf << "   }" << endl;
	if (ProgParameters::getOutputFormat() == "CJni") {
		fileCpp_pf << "   // Note : en CJni, le tableau WFP est linéarisé, i.e. WFP[i][j] est transformé en WFP[i*3 + j] (WFP a toujours une deuxième dimension de 3)" << endl;
		fileCpp_pf << "   // Pour retrouver le numéro de l'opérateur à partir de l'indice unique, il faut donc diviser l'indice par 3" << endl;
		fileCpp_pf << "   // et pour savoir le numéro de l'entrée/sortie, il faut prendre le modulo 3" << endl;
		fileCpp_pf << "   jint * WFP = (*env)->GetIntArrayElements(env, jWFP, NULL);" << endl;
		fileCpp_pf << "   jint * WFPquantMode = (*env)->GetIntArrayElements(env, jWFPquantMode, NULL);" <<endl;
		fileCpp_pf << "   jint * tabInputWd = (*env)->GetIntArrayElements(env,jtabInputWd,NULL);" << endl;
		fileCpp_pf << "   jint * tabInputWdquantMode = (*env)->GetIntArrayElements(env, jtabInputWdquantMode, NULL);" <<endl;
	}

	fileCpp_pf << "   // Init WFP_sfg[NB_SFG]" << endl;
}

void Gfd::computePb_WFP_cdfg_to_WFP_sfg(fstream & fileCpp_pf, fstream & fileHpp_pf) {
	list<string>::iterator itStr;
	fstream fileTempCpp;
	int numeroIndexFileC = 0;
	int counter_Nbr_WFP = 0;
	ostringstream oss;
	string pathTempCpp;

	oss << numeroIndexFileC;
	pathTempCpp = ProgParameters::getOutputGenComputePbDirectory() + "computePb_WFP_CDFG_to_WFP" + oss.str() + ".c";
	fileTempCpp.open(pathTempCpp.c_str(), fstream::out);
	assert(fileTempCpp.is_open());

	fileTempCpp << "#include \"Tool.h\"" << endl;

#if DEBUG
	fileTempCpp << "void computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(int *WFP_sfg, int *WFP_sfg_ModeQuantif, int *WFP, int *WFPquantMode, FILE *pfile) {" << endl;
	fileCpp_pf << "computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(WFP_sfg, WFP_sfg_ModeQuantif, WFP, WFPquantMode, pfile);" << endl;
	fileHpp_pf << "void computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(int *WFP_sfg, int *WFP_sfg_ModeQuantif, int *WFP, int *WFPquantMode, FILE *pfile );" << endl;
#else
	fileTempCpp << "void computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(int *WFP_sfg, int *WFP_sfg_ModeQuantif, int *WFP, int *WFPquantMode) {" << endl;
	fileCpp_pf << "computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(WFP_sfg, WFP_sfg_ModeQuantif, WFP, WFPquantMode);" << endl;
	fileHpp_pf << "void computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(int *WFP_sfg, int *WFP_sfg_ModeQuantif, int *WFP, int *WFPquantMode);" << endl;
#endif

	for (itStr = this->listeStrWFP_sfg.begin(); itStr != this->listeStrWFP_sfg.end(); ++itStr, counter_Nbr_WFP++) {
		// 23500 = 1 MO
		// 2350 = 100 ko
		// 1175 = 50 ko
		if(counter_Nbr_WFP > 2350){
			ostringstream oss1;
			counter_Nbr_WFP = 0;
			numeroIndexFileC++;

			fileTempCpp << "}" << endl;
			fileTempCpp.close();
			assert(!fileTempCpp.is_open());

			oss1 << numeroIndexFileC;
			pathTempCpp = ProgParameters::getOutputGenComputePbDirectory() + "computePb_WFP_CDFG_to_WFP" + oss1.str() + ".c";
			fileTempCpp.open(pathTempCpp.c_str(), fstream::out);
			assert(fileTempCpp.is_open());
			fileTempCpp << "#include \"Tool.h\"" << endl;

#if DEBUG
			fileTempCpp << "void computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(int *WFP_sfg, int *WFP_sfg_ModeQuantif, int *WFP, int *WFPquantMode, FILE *pfile) {" << endl;
			fileCpp_pf << "computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(WFP_sfg, WFP_sfg_ModeQuantif, WFP, WFPquantMode, pfile);" << endl;
			fileHpp_pf << "void computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(int *WFP_sfg, int *WFP_sfg_ModeQuantif, int *WFP, int *WFPquantMode, FILE *pfile);" << endl;
#else
			fileTempCpp << "void computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(int *WFP_sfg, int *WFP_sfg_ModeQuantif, int *WFP, int *WFPquantMode) {" << endl;
			fileCpp_pf << "computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(WFP_sfg, WFP_sfg_ModeQuantif, WFP, WFPquantMode);" << endl;
			fileHpp_pf << "void computePb_WFP_CDFG_to_WFP" << numeroIndexFileC << "(int *WFP_sfg, int *WFP_sfg_ModeQuantif, int *WFP, int *WFPquantMode);" << endl;
#endif
		}
		fileTempCpp << *itStr << endl;
	}

	fileTempCpp << "}" << endl;
	fileTempCpp.close();

}

/**
 * Ecriture des WFPeff
 * \author Jean-Charles Naud
 */
void Gfd::computePb_ecritureWFP_sfg_eff(fstream & fileCpp_pf, fstream & fileHpp_pf) {
	// Ecriture des expressions des WFP effectif
	list<string>::iterator itStr;
	fstream fileTempCpp;
	int numeroIndexFileC = 0;
	int counter_Nbr_WFP = 0;
	ostringstream oss;
	string pathTempCpp;

	oss << numeroIndexFileC;
	cout<<ProgParameters::getOutputGenComputePbDirectory()<<endl;
	pathTempCpp = ProgParameters::getOutputGenComputePbDirectory() + "computePb_WFP_SFG_eff" + oss.str() + ".c";
	fileTempCpp.open(pathTempCpp.c_str(), fstream::out);
	assert(fileTempCpp.is_open());

	fileTempCpp << "#include \"Tool.h\"" << endl;

#if DEBUG
	fileTempCpp << "void computePb_WFP_SFG_eff" << numeroIndexFileC << "(int *WFP_sfg, int *WFP_sfg_eff, int *tabInputWd, FILE *pfile) {" << endl;
	fileCpp_pf << "computePb_WFP_SFG_eff" << numeroIndexFileC << "(WFP_sfg, WFP_sfg_eff, tabInputWd, pfile);" << endl;
	fileHpp_pf << "void computePb_WFP_SFG_eff" << numeroIndexFileC << "(int *WFP_sfg, int *WFP_sfg_eff, int *tabInputWd, FILE *pfile);" << endl;
#else
	fileTempCpp << "void computePb_WFP_SFG_eff" << numeroIndexFileC << "(int *WFP_sfg, int *WFP_sfg_eff, int *tabInputWd) {" << endl;
	fileCpp_pf << "computePb_WFP_SFG_eff" << numeroIndexFileC << "(WFP_sfg, WFP_sfg_eff, tabInputWd);" << endl;
	fileHpp_pf << "void computePb_WFP_SFG_eff" << numeroIndexFileC << "(int *WFP_sfg, int *WFP_sfg_eff, int *tabInputWd);" << endl;
#endif

	for (itStr = this->listeStrWFP_sfg_eff.begin(); itStr != this->listeStrWFP_sfg_eff.end(); ++itStr, counter_Nbr_WFP++) {
		// 23500 = 1 MO
		// 2350 = 100 ko
		// 1175 = 50 ko
		if(counter_Nbr_WFP > 2350){
			ostringstream oss1;
			counter_Nbr_WFP = 0;
			numeroIndexFileC++;

			fileTempCpp << "}" << endl;
			fileTempCpp.close();
			assert(!fileTempCpp.is_open());

			oss1 << numeroIndexFileC;
			pathTempCpp = ProgParameters::getOutputGenComputePbDirectory() + "computePb_WFP_SFG_eff" + oss1.str() + ".c";
			fileTempCpp.open(pathTempCpp.c_str(), fstream::out);
			assert(fileTempCpp.is_open());
			fileTempCpp << "#include \"Tool.h\"" << endl;
#if DEBUG
            fileTempCpp << "void computePb_WFP_SFG_eff" << numeroIndexFileC << "(int *WFP_sfg, int *WFP_sfg_eff, int *tabInputWd, FILE *pfile) {" << endl;
            fileCpp_pf << "computePb_WFP_SFG_eff" << numeroIndexFileC << "(WFP_sfg, WFP_sfg_eff, tabInputWd, pfile);" << endl;
            fileHpp_pf << "void computePb_WFP_SFG_eff" << numeroIndexFileC << "(int *WFP_sfg, int *WFP_sfg_eff, int *tabInputWd, FILE *pfile);" << endl;
#else
			fileTempCpp << "void computePb_WFP_SFG_eff" << numeroIndexFileC << "(int *WFP_sfg, int *WFP_sfg_eff, int *tabInputWd) {" << endl;
			fileCpp_pf << "computePb_WFP_SFG_eff" << numeroIndexFileC << "(WFP_sfg, WFP_sfg_eff, tabInputWd);" << endl;
			fileHpp_pf << "void computePb_WFP_SFG_eff" << numeroIndexFileC << "(int *WFP_sfg, int *WFP_sfg_eff, int *tabInputWd);" << endl;
#endif
		}
		fileTempCpp << *itStr << endl;
	}

	fileTempCpp << "}" << endl;
	fileTempCpp.close();
}

/**
 * Ecriture des Means et Var de chaque Bruit "br"
 * \author Jean-Charles Naud
 */
void Gft::computePb_ecritureMeanVarBr(Gfd * grapheDepart, fstream & fileCpp_pf, fstream & fileHpp_pf) {
	// Parcours des NoeudSrcBruit pour écrire les expression des moments
	fstream fileTempCpp;
	int numeroIndexFileC = 0;
	int counter_Nbr_WFP = 0;
	ostringstream oss;
	string pathTempCpp;

	oss << numeroIndexFileC;
	pathTempCpp = ProgParameters::getOutputGenComputePbDirectory() + "computePb_Mean_Var_Br" + oss.str() + ".c";
	fileTempCpp.open(pathTempCpp.c_str(), fstream::out);
	assert(fileTempCpp.is_open());

	fileTempCpp << "#include \"Tool.h\"" << endl;

#if DEBUG
	fileTempCpp << "void computePb_Mean_Var_Br" << numeroIndexFileC << "(int *K, float *Q, int *WFP_sfg_eff, int *WFP_sfg_ModeQuantif, int *tabInputWd, int *tabInputWdquantMode, float *Xb_Mean, float *Xb_Var, FILE *pfile) {" << endl;
	fileCpp_pf << "computePb_Mean_Var_Br" << numeroIndexFileC << "(K, Q, WFP_sfg_eff, WFP_sfg_ModeQuantif, tabInputWd, tabInputWdquantMode, Xb_Mean, Xb_Var, pfile);" << endl;
	fileHpp_pf << "void computePb_Mean_Var_Br" << numeroIndexFileC << "(int *K, float *Q, int *WFP_sfg_eff, int *WFP_sfg_ModeQuantif, int *tabInputWd, int *tabInputWdquantMode, float *Xb_Mean, float *Xb_Var, FILE *pfile);" << endl;
#else
	fileTempCpp << "void computePb_Mean_Var_Br" << numeroIndexFileC << "(int *K, float *Q, int *WFP_sfg_eff, int *WFP_sfg_ModeQuantif, int *tabInputWd, int *tabInputWdquantMode, float *Xb_Mean, float *Xb_Var) {" << endl;
	fileCpp_pf << "computePb_Mean_Var_Br" << numeroIndexFileC << "(K, Q, WFP_sfg_eff, WFP_sfg_ModeQuantif, tabInputWd, tabInputWdquantMode, Xb_Mean, Xb_Var);" << endl;
	fileHpp_pf << "void computePb_Mean_Var_Br" << numeroIndexFileC << "(int *K, float *Q, int *WFP_sfg_eff, int *WFP_sfg_ModeQuantif, int *tabInputWd, int *tabInputWdquantMode, float *Xb_Mean, float *Xb_Var);" << endl;
#endif

	for(int i = 0 ; i<grapheDepart->getNbNoiseNode() ; i++){
		// 3250 = 1 MO
		// 325 = 100 ko
		// 162 = 50 ko
		if(counter_Nbr_WFP > 325){
			ostringstream oss1;
			counter_Nbr_WFP = 0;
			numeroIndexFileC++;

			fileTempCpp << "}" << endl;
			fileTempCpp.close();
			assert(!fileTempCpp.is_open());

			oss1 << numeroIndexFileC;
			pathTempCpp = ProgParameters::getOutputGenComputePbDirectory() + "computePb_Mean_Var_Br" + oss1.str() + ".c";
			fileTempCpp.open(pathTempCpp.c_str(), fstream::out);
			assert(fileTempCpp.is_open());
			fileTempCpp << "#include \"Tool.h\"" << endl;
#if DEBUG
            fileTempCpp << "void computePb_Mean_Var_Br" << numeroIndexFileC << "(int *K, float *Q, int *WFP_sfg_eff, int *WFP_sfg_ModeQuantif, int *tabInputWd, int *tabInputWdquantMode, float *Xb_Mean, float *Xb_Var, FILE *pfile) {" << endl;
            fileCpp_pf << "computePb_Mean_Var_Br" << numeroIndexFileC << "(K, Q, WFP_sfg_eff, WFP_sfg_ModeQuantif, tabInputWd, tabInputWdquantMode, Xb_Mean, Xb_Var, pfile);" << endl;
            fileHpp_pf << "void computePb_Mean_Var_Br" << numeroIndexFileC << "(int *K, float *Q, int *WFP_sfg_eff, int *WFP_sfg_ModeQuantif, int *tabInputWd, int *tabInputWdquantMode, float *Xb_Mean, float *Xb_Var, FILE *pfile);" << endl;
#else
			fileTempCpp << "void computePb_Mean_Var_Br" << numeroIndexFileC << "(int *K, float *Q, int *WFP_sfg_eff, int *WFP_sfg_ModeQuantif, int *tabInputWd, int *tabInputWdquantMode, float *Xb_Mean, float *Xb_Var) {" << endl;
			fileCpp_pf << "computePb_Mean_Var_Br" << numeroIndexFileC << "(K, Q, WFP_sfg_eff, WFP_sfg_ModeQuantif, tabInputWd, tabInputWdquantMode, Xb_Mean, Xb_Var);" << endl;
			fileHpp_pf << "void computePb_Mean_Var_Br" << numeroIndexFileC << "(int *K, float *Q, int *WFP_sfg_eff, int *WFP_sfg_ModeQuantif, int *tabInputWd, int *tabInputWdquantMode, float *Xb_Mean, float *Xb_Var);" << endl;
#endif
		}
		counter_Nbr_WFP += grapheDepart->getNoiseNode(i)->getNbSuppressKBits();
		grapheDepart->getNoiseNode(i)->writeCalculMoments(fileTempCpp);
	}

	fileTempCpp << "}" << endl;
	fileTempCpp.close();
}

/**
 * Ecriture des gains en tre les bruit et les sorties
 * \author Jean-Charles Naud
 */
void Gft::computePb_ecritureGainBrVersSortie(Gfd * grapheDepart, fstream & fileCpp_pf) {

	string tempPathFichierGain = ProgParameters::getOutputGenDirectory() + VALEUR_HG_DB_FILENAME;
	string ligne;
	fstream fichierGain;

	trace_debug ("Ouverture du fichier gain %s généré et copie dans calculRSBQ\n", tempPathFichierGain.data());
	fichierGain.open(tempPathFichierGain.data(), fstream::in); //ouverture fichier en mode lecture
	if (!fichierGain.is_open()) {
		trace_error("Impossible d'ouvrir le fichier de gain ValeurHg.m (%s)\n",tempPathFichierGain.data());
		exit(EXIT_FAILURE);
	}
	else {
		while (getline(fichierGain, ligne)) {
			fileCpp_pf << ligne << endl;
		}
	}
	fichierGain.close();
}

/**
 * Ecriture de l'expression du bruit Pby en sortie
 * \author Jean-Charles Naud
 */
void Gft::computePb_ecritureExpressionPby(Gfd * grapheDepart, fstream & fileCpp_pf) {


	if(RuntimeParameters::isLTIGraph() && !RuntimeParameters::isRecursiveGraph()){ // Version KLM

		fileCpp_pf << "   ret_Pby = 0;"<< endl;

		fileCpp_pf << "   fseek(fileGainBin , 0 , SEEK_END);"<<endl;
		fileCpp_pf << "   lSize = ftell(fileGainBin);"<<endl;
		fileCpp_pf << "   rewind (fileGainBin);"<<endl;

		fileCpp_pf << "   for ( itOut = 0 ; itOut < NB_OUTPUT ; itOut++){" << endl;
		fileCpp_pf << "      for( i = 0 ; i < NB_SRCE_NOISE ; i++){" << endl;
		fileCpp_pf << "         result = fread(&hSumCarre, sizeof(float), 1, fileGainBin);"<<endl;
		fileCpp_pf << "         if(result > lSize) {fputs (\"Reading error\",stderr); exit (3);}" <<endl;
		fileCpp_pf << "         ret_Pby += Xb_Var[i]*hSumCarre;" <<endl;
#if DEBUG
        fileCpp_pf << "         fprintf(pfile, \"Xb_Var[%i]: %e\\n\",i ,Xb_Var[i]);" << endl;
		fileCpp_pf << "         fprintf(pfile, \"hSumCarre: %e\\n\", hSumCarre);" << endl;
		fileCpp_pf << "         fprintf(pfile, \"ret_Pby: %e\\n\\n\", ret_Pby);" << endl;
#endif
		fileCpp_pf << "      }"<<endl;
		fileCpp_pf << "   }"<<endl<<endl;

		if(RuntimeParameters::isBinaryWriteWithoutZero()){
			fileCpp_pf << "   for( itOut = 0 ; itOut < NB_OUTPUT ; itOut++){"<<endl;
			fileCpp_pf << "      result = fread(&nbBrDependant, sizeof(unsigned short int), 1, fileGainBin);"<<endl;
			fileCpp_pf << "      if(result > lSize) {fputs (\"Reading error\",stderr); exit (3);}" <<endl;
			fileCpp_pf << "      brDependant = malloc(nbBrDependant * sizeof(unsigned short int));"<<endl;
			fileCpp_pf << "      if(brDependant == NULL){"<<endl;
			fileCpp_pf << "         printf(\"Allocation tableau brDependant echoué\\n\");"<<endl;
			fileCpp_pf << "         exit(0);"<<endl;
			fileCpp_pf << "      }"<<endl;
			fileCpp_pf << "      for( temp = 0 ; temp < nbBrDependant ; temp++){"<<endl;
			fileCpp_pf << "         result = fread(&brDependant[temp], sizeof(unsigned short int), 1, fileGainBin);"<<endl;
			fileCpp_pf << "         if(result > lSize) {fputs (\"Reading error\",stderr); exit (3);}" <<endl;
			fileCpp_pf << "      }"<<endl;
			fileCpp_pf << "      for( i = 0 ; i < nbBrDependant ; i++){"<<endl;
			fileCpp_pf << "         for( j = i ; j < nbBrDependant ; j++){"<<endl;
			fileCpp_pf << "            result = fread(&hSumCroise, sizeof(float), 1, fileGainBin);"<<endl;
			fileCpp_pf << "            if(result > lSize) {fputs (\"Reading error\",stderr); exit (3);}" <<endl;
			fileCpp_pf << "            if( i == j){"<<endl;
			fileCpp_pf << "               ret_Pby += Xb_Mean[brDependant[i]] * Xb_Mean[brDependant[j]] * hSumCroise;"<<endl;
			fileCpp_pf << "            }"<<endl;
			fileCpp_pf << "            else{"<<endl;
			fileCpp_pf << "               ret_Pby += 2*Xb_Mean[brDependant[i]] * Xb_Mean[brDependant[j]] * hSumCroise;"<<endl;
			fileCpp_pf << "            }"<<endl;
#if DEBUG
            fileCpp_pf << "         fprintf(pfile, \"Xb_Mean[%i]: %e\\n\", brDependant[i], Xb_Mean[brDependant[i]]);" << endl;
            fileCpp_pf << "         fprintf(pfile, \"Xb_Mean[%i]: %e\\n\", brDependant[j], Xb_Mean[brDependant[j]]);" << endl;
            fileCpp_pf << "         fprintf(pfile, \"hSumCroise: %e\\n\", hSumCroise);" << endl;
            fileCpp_pf << "         fprintf(pfile, \"ret_Pby: %e\\n\\n\", ret_Pby);" << endl;
#endif
			fileCpp_pf << "         }"<<endl;
			fileCpp_pf << "      }"<<endl;
			fileCpp_pf << "      free(brDependant);"<<endl<<endl;
			fileCpp_pf << "   }"<<endl<<endl;
		}
		else{
			fileCpp_pf << "   for( itOut = 0 ; itOut < NB_OUTPUT ; itOut++){"<<endl;
			fileCpp_pf << "      for( i = 0 ; i < NB_SRCE_NOISE ; i++){"<<endl;
			fileCpp_pf << "         for( j = i ; j < NB_SRCE_NOISE ; j++){"<<endl;
			fileCpp_pf << "            result = fread(&hSumCroise, sizeof(float), 1, fileGainBin);"<<endl;
			fileCpp_pf << "            if(result > lSize) {fputs (\"Reading error\",stderr); exit (3);}" <<endl;
			fileCpp_pf << "            if(i == j){"<<endl;
			fileCpp_pf << "               ret_Pby += Xb_Mean[i]*Xb_Mean[j]*hSumCroise;"<<endl;
			fileCpp_pf << "            }"<<endl;
			fileCpp_pf << "            else{"<<endl;
			fileCpp_pf << "               ret_Pby += 2*Xb_Mean[i]*Xb_Mean[j]*hSumCroise;"<<endl;
			fileCpp_pf << "            }"<<endl;
#if DEBUG
            fileCpp_pf << "         fprintf(pfile, \"Xb_Mean[%i]: %e\\n\", i, Xb_Mean[i]);" << endl;
            fileCpp_pf << "         fprintf(pfile, \"Xb_Mean[%i]: %e\\n\", j, Xb_Mean[j]);" << endl;
            fileCpp_pf << "         fprintf(pfile, \"hSumCroise: %e\\n\", hSumCroise);" << endl;
            fileCpp_pf << "         fprintf(pfile, \"ret_Pby: %e\\n\\n\", ret_Pby);" << endl;
#endif
			fileCpp_pf << "         }"<<endl;
			fileCpp_pf << "      }"<<endl;
			fileCpp_pf << "   }"<<endl<<endl;
		}
		fileCpp_pf << "   fclose(fileGainBin);" << endl;
	}
	else{ // Version Matlab
		if(RuntimeParameters::isLTIGraph()){
			fileCpp_pf << "   ret_Pby = 0;" << endl;
			fileCpp_pf << "   for (itOut = 0; itOut < NB_OUTPUT; itOut++) {" << endl;
			fileCpp_pf << "      for (i = 0; i < NB_SRCE_NOISE; i++) {" << endl;
			fileCpp_pf << "   		ret_Pby += Xb_Var[i]*HSumCarree[itOut*NB_SRCE_NOISE+i];" << endl;
#if DEBUG
            fileCpp_pf << "         fprintf(pfile, \"Xb_Var[%i]: %e\\n\",i ,Xb_Var[i]);" << endl;
            fileCpp_pf << "         fprintf(pfile, \"HSumCarree[%i]: %e\\n\",itOut*NB_SRCE_NOISE+i, HSumCarree[itOut*NB_SRCE_NOISE+i]);" << endl;
            fileCpp_pf << "         fprintf(pfile, \"ret_Pby: %e\\n\\n\", ret_Pby);" << endl;
#endif
			fileCpp_pf << "   	 }" << endl  << endl;

			fileCpp_pf << "      for (i = 0; i < NB_SRCE_NOISE; i++) {" << endl;
			fileCpp_pf << "         for (j = i; j < NB_SRCE_NOISE; j++) {" << endl;
			fileCpp_pf << "				if( i == j){"<<endl;
			fileCpp_pf << "   		      ret_Pby += Xb_Mean[i]*Xb_Mean[j]*HSumCroise[itOut*NB_SRCE_NOISE*NB_SRCE_NOISE+i*NB_SRCE_NOISE+j];" << endl;
			fileCpp_pf << "				}"<<endl;
			fileCpp_pf << "				else{"<<endl;
			fileCpp_pf << "   		      ret_Pby += 2*Xb_Mean[i]*Xb_Mean[j]*HSumCroise[itOut*NB_SRCE_NOISE*NB_SRCE_NOISE+i*NB_SRCE_NOISE+j];" << endl;
			fileCpp_pf << "				}"<<endl;
#if DEBUG
            fileCpp_pf << "         fprintf(pfile, \"Xb_Mean[%i]: %e\\n\", i, Xb_Mean[i]);" << endl;
            fileCpp_pf << "         fprintf(pfile, \"Xb_Mean[%i]: %e\\n\", j, Xb_Mean[j]);" << endl;
            fileCpp_pf << "         fprintf(pfile, \"hSumCroise[%i]: %e\\n\",itOut*NB_SRCE_NOISE*NB_SRCE_NOISE+i*NB_SRCE_NOISE+j, HSumCroise[itOut*NB_SRCE_NOISE*NB_SRCE_NOISE+i*NB_SRCE_NOISE+j]);" << endl;
            fileCpp_pf << "         fprintf(pfile, \"ret_Pby: %e\\n\\n\", ret_Pby);" << endl;
#endif
			fileCpp_pf << "         }" << endl;
			fileCpp_pf << "      }" << endl;
			fileCpp_pf << "   }" << endl << endl;
		}
		else if(!RuntimeParameters::isLTIGraph()){
			fileCpp_pf << "   ret_Pby = 0;" << endl;
			fileCpp_pf << "   for (itOut = 0; itOut < NB_OUTPUT; itOut++) {" << endl;
			fileCpp_pf << "      for (i = 0; i < NB_SRCE_NOISE; i++) {" << endl;
			fileCpp_pf << "   		ret_Pby += Xb_Var[i]*HSumCarree[itOut*NB_SRCE_NOISE+i];" << endl;
			fileCpp_pf << "   	 }" << endl  << endl;

			fileCpp_pf << "      for (i = 0; i < NB_SRCE_NOISE; i++) {" << endl;
			fileCpp_pf << "         for (j = i; j < NB_SRCE_NOISE; j++) {" << endl;
			fileCpp_pf << "				if( i == j){"<<endl;
			fileCpp_pf << "   		      ret_Pby += Xb_Mean[i]*Xb_Mean[j]*HSumCroise[itOut*NB_SRCE_NOISE*NB_SRCE_NOISE+i*NB_SRCE_NOISE+j];" << endl;
			fileCpp_pf << "				}"<<endl;
			fileCpp_pf << "				else{"<<endl;
			fileCpp_pf << "   		      ret_Pby += 2*Xb_Mean[i]*Xb_Mean[j]*HSumCroise[itOut*NB_SRCE_NOISE*NB_SRCE_NOISE+i*NB_SRCE_NOISE+j];" << endl;
			fileCpp_pf << "				}"<<endl;
#if DEBUG
            fileCpp_pf << "         if(Xb_Mean[i] !=0){ " << endl;
            fileCpp_pf << "         if(Xb_Mean[j] != 0){ " << endl;
            fileCpp_pf << "         if(HSumCroise[itOut*NB_SRCE_NOISE*NB_SRCE_NOISE+i*NB_SRCE_NOISE+j] != 0){" << endl;
            fileCpp_pf << "         fprintf(pfile, \"Xb_Mean[%i]: %e\\n\", i, Xb_Mean[i]);" << endl;
            fileCpp_pf << "         fprintf(pfile, \"Xb_Mean[%i]: %e\\n\", j, Xb_Mean[j]);" << endl;
            fileCpp_pf << "         fprintf(pfile, \"hSumCroise[%i]: %e\\n\",itOut*NB_SRCE_NOISE*NB_SRCE_NOISE+i*NB_SRCE_NOISE+j, HSumCroise[itOut*NB_SRCE_NOISE*NB_SRCE_NOISE+i*NB_SRCE_NOISE+j]);" << endl;
            fileCpp_pf << "         fprintf(pfile, \"ret_Pby: %e\\n\\n\", ret_Pby);" << endl;
            fileCpp_pf << "         }" << endl;
            fileCpp_pf << "         }" << endl;
            fileCpp_pf << "         }" << endl;
#endif
			fileCpp_pf << "         }" << endl;
			fileCpp_pf << "      }" << endl;
			fileCpp_pf << "   }" << endl << endl;
		}
	}


	if (ProgParameters::getOutputFormat() == "CJni") {
		fileCpp_pf << "   (*env)->ReleaseIntArrayElements(env,jWFP,WFP,0);" << endl;
		fileCpp_pf << "   (*env)->ReleaseIntArrayElements(env,jWFPquantMode ,WFPquantMode,0);" << endl;
		fileCpp_pf << "   (*env)->ReleaseIntArrayElements(env,jtabInputWd,tabInputWd,0);" << endl;
		fileCpp_pf << "   (*env)->ReleaseIntArrayElements(env,jtabInputWdquantMode,tabInputWdquantMode,0);" << endl;
	}

#if DEBUG
    fileCpp_pf << "   fprintf(pfile, \"final ret_Pby: %e\\n\\n\", ret_Pby);" << endl;
    fileCpp_pf << "   fclose(pfile);" << endl;
#endif
	fileCpp_pf << "   return 10*log10(ret_Pby);" << endl;
}

/**
 *  Methode ecrit dans le fichier le calcul des moments d'ordre 1 et 2 de la source de bruit
 *  \param 	fichier ou ecrire
 * \author Jean-Charles Naud
 */
void NoeudSrcBruit::writeCalculMoments(fstream & fileCpp_pf) {
	int i = 0;

	fileCpp_pf << "   // " << this->getName().data() << " ;nbK = " << this->m_KbitsElimine.size() << endl;
	if (this->m_KbitsElimine.size() != 0) {
		for (i = 0; i < (int) this->m_KbitsElimine.size(); i++) {
			//cout<<this->getName()<<" "<<m_PasQuantif[i]<<endl;
			fileCpp_pf << "   K[" << i << "] = " << m_KbitsElimine[i] << ";" << endl;
			fileCpp_pf << "   Q[" << i << "] = " << m_PasQuantif[i] << ";" << endl;
#if DEBUG
            fileCpp_pf << "   fprintf(pfile, \"K[" << i << "]: %i\\n\", K[" << i << "]);" << endl;
            fileCpp_pf << "   fprintf(pfile, \"Q[" << i << "]: %f\\n\", Q[" << i << "]);" << endl;
#endif
		}
		/*Debug: cout<<"Pas Quantif: "<<this->getName()<<" "<<this->getModeQuantif()<<endl;*/
		fileCpp_pf << "   Xb_Mean[" << this->getNumSrc() - 1 << "] = ";

		//cout<<" "<<this->m_KbitsElimine.size()<<endl;

		for (i = 0; i < (int) this->m_KbitsElimine.size(); i++) {
			if (this->m_SigneMoyenne[i] == 1) { // Positif: +1
				fileCpp_pf << "+CalculBruitMean(Q[" << i << "],K[" << i << "],"<< m_ModeQuantif[i]<<")*" << m_Constante[i]; // Xb_Mean[0] = CalculBruitMean(Q0,K0)+
			}
			else { // Negatif: -1
				fileCpp_pf << "-CalculBruitMean(Q[" << i << "],K[" << i << "],"<< m_ModeQuantif[i]<<")*" << m_Constante[i]; // Xb_Mean[0] = CalculBruitMean(Q0,K0)+
			}
		}
		fileCpp_pf << ";"<<endl;
		fileCpp_pf << "   Xb_Var[" << this->getNumSrc() - 1 << "] = "; // Xb_Var[0] =
		// On enleve 1 car Br1 est represente par l'indice 0
		//xxx changer le mode d'arrondi de manière dynamique.
		for (i = 0; i < (int) this->m_KbitsElimine.size() - 1; i++) {
			fileCpp_pf << "CalculBruitVar(Q[" << i << "],K[" << i << "],"<< m_ModeQuantif[i]<<")*" << m_Constante[i]*m_Constante[i] << " + "; // Xb_Var[0] = CalculBruitVar(Q0,K0)+
		}
		fileCpp_pf << " CalculBruitVar(Q[" << i << "],K[" << i << "],"<< m_ModeQuantif[i]<<")*" << m_Constante[i]*m_Constante[i]; // Xb_Var[0] = CalculBruitVar(Q0,K0)+ CalculBruitVar(Q1,K1);
		fileCpp_pf << ";"<<endl;

		ostringstream oss117;
		oss117 << this->getNumSrc()-1;
#if DEBUG
		fileCpp_pf << "   fprintf(pfile, \"Xb_Mean[%d] : %e - Xb_Var[%d] : %e\"," + oss117.str() + ",Xb_Mean[" << this->getNumSrc() - 1 << "]," + oss117.str() + ",Xb_Var[" << this->getNumSrc() - 1 << "]);  " << endl;
		fileCpp_pf << "   fprintf(pfile, \"  K = %d\\n\\n\",K["<<i<<"]);" << endl;
#endif
		fileCpp_pf << endl;
	}
}


/**
 * Write calculation WFP efficace
 * \author Jean-Charles Naud
 */
void Gfd::writeWFPeffOps(fstream & fileCpp_pf) {

	// === Définition de int WFPeff[maxGeneral][X]
	//     Recherche du NumCDFG Maximal
	ListNoeudGraph::iterator it;
	int maxGeneral = -1, maxCourant;
	it = this->tabNode->begin();
	++it;
	for (++it; it != this->tabNode->end(); ++it) {
		if (((NoeudGFD *) (*it))->getTypeNodeGFD() == OPS) {
			maxCourant = ((NoeudOps *) (*it))->getNumCDFG();
			if (maxCourant > maxGeneral) {
				maxGeneral = maxCourant;
			}
		}
	}

	/*	// === Ecriture des WFPeff (en fonction des WFP et tabInputWd précedement calculer dans preGenereFileCalculRSQB
		list<string>::iterator itStr;
		for (itStr = this->listeStrWFPeff.begin(); itStr != this->listeStrWFPeff.end(); ++itStr) {
		fileCpp_pf << *itStr << endl;
		}*/
}
/**
 * Pre calculation function to generate ComputePb file
 * \author Jean-Charles Naud
 */
void Gfd::preCalculeWFPcdfgToWFPsfg() {

	NoeudGFD * noeudGFD;
	NoeudOps * noeudOps;
	ListNoeudGraph::iterator it;
	int NumSFG, NumCDFG;
	string str;
	std::ostringstream ossSFG, ossCDFG, ossInput;

	// === Expression des liens entre WFP_sfg et WFP_cdgf
	for (it = this->tabNode->begin(); it != this->tabNode->end(); ++it) {
		if ((*it)->getTypeNodeGraph() == GFD) {
			noeudGFD = (NoeudGFD *) *it;
			if (noeudGFD->getTypeNodeGFD() == OPS) {
				noeudOps = (NoeudOps *) noeudGFD;
				NumSFG = noeudOps->getNumSFG();
				if (NumSFG >= 0) {
                    ossSFG.str("");
                    ossSFG << NumSFG;
                    if(noeudOps->getTypeOps() != OPS_PHI){
                        NumCDFG = noeudOps->getNumCDFG();
                        if(NumCDFG >= 0){
                            ossCDFG.str("");
                            ossCDFG << NumCDFG;
                            str = "   WFP_sfg[" + ossSFG.str() + "*3+0] = WFP[" + ossCDFG.str() + "*3+0];";
                            this->listeStrWFP_sfg.push_back(str);
                            str = "   WFP_sfg[" + ossSFG.str() + "*3+1] = WFP[" + ossCDFG.str() + "*3+1];";
                            this->listeStrWFP_sfg.push_back(str);
                            str = "   WFP_sfg[" + ossSFG.str() + "*3+2] = WFP[" + ossCDFG.str() + "*3+2];";
                            this->listeStrWFP_sfg.push_back(str);
                            str = "   WFP_sfg_ModeQuantif[" + ossSFG.str() + "*3+0] = WFPquantMode[" + ossCDFG.str() + "*3+0];";
                            this->listeStrWFP_sfg.push_back(str);
                            str = "   WFP_sfg_ModeQuantif[" + ossSFG.str() + "*3+1] = WFPquantMode[" + ossCDFG.str() + "*3+1];";
                            this->listeStrWFP_sfg.push_back(str);
                            str = "   WFP_sfg_ModeQuantif[" + ossSFG.str() + "*3+2] = WFPquantMode[" + ossCDFG.str() + "*3+2];";
                            this->listeStrWFP_sfg.push_back(str);
#if DEBUG
                            str = "fprintf(pfile, \"WFP_sfg[" + ossSFG.str() + "*3+0]: %i\\n\", WFP_sfg[" + ossSFG.str() + "*3+0]);";
                            str += "\nfprintf(pfile, \"WFP_sfg[" + ossSFG.str() + "*3+1]: %i\\n\", WFP_sfg[" + ossSFG.str() + "*3+1]);";
                            str += "\nfprintf(pfile, \"WFP_sfg[" + ossSFG.str() + "*3+2]: %i\\n\", WFP_sfg[" + ossSFG.str() + "*3+2]);";
                            str += "\nfprintf(pfile, \"WFP_sfg_ModeQuantif[" + ossSFG.str() + "*3+0]: %i\\n\", WFP_sfg_ModeQuantif[" + ossSFG.str() + "*3+0]);";
                            str += "\nfprintf(pfile, \"WFP_sfg_ModeQuantif[" + ossSFG.str() + "*3+1]: %i\\n\", WFP_sfg_ModeQuantif[" + ossSFG.str() + "*3+1]);";
                            str += "\nfprintf(pfile, \"WFP_sfg_ModeQuantif[" + ossSFG.str() + "*3+2]: %i\\n\", WFP_sfg_ModeQuantif[" + ossSFG.str() + "*3+2]);";
                            this->listeStrWFP_sfg.push_back(str);
#endif
                        }
                        else{
                            trace_error("An operator have not a CDFG number");
                        }
                    }
                    else{
                        trace_warn("PHI node not managed yet in the generation of the compute pb");
                    }
                }
                else{
                    trace_error("An operator have not a SFG number"); 
                    exit(-1);
                }
            }
        }
    }
}

/**
 *	Pré calcul de la propagation des Wfp effectif
 * \author Jean-Charles Naud
 *
 */

void Gfd::preCalculWFPsfgToWFPsfgeff() {
    list<NoeudGraph *> * listTriBr = new list<NoeudGraph *> ;
    ListNoeudGraph::iterator it;
    NoeudSrcBruit * nodeSrcBruit;
    NoeudGFD * nodeGFD, * nodeGFD2;
    NoeudData* nodeData;
    NoeudOps* nodeOpSucc;
    string str;
    std::ostringstream ossBr, ossOpSucc, ossData, ossNumIn, ss;
    bool hasVariableWidth;

    // === Construction de listTriBr et détection des cycles avec (Etat = R2 et R3)
    this->resetInfoVisite();
    for (it = this->tabOutput->begin(); it != this->tabOutput->end(); ++it) {
        (*it)->preCalculWFPsfgToWFPsfgeff_TriBrGen(listTriBr);
    }

    // === Affichage listTriBr (debug)
    /*cout << "------ listTriBr  -------" << endl;
      for (it = listTriBr->begin(); it != listTriBr->end(); ++it) {
      cout << (*it)->getName().data() << endl;
      }

    // === Affichage des cycles détectés (debug)
    cout << "------ cycles détectés --------" << endl;
    it = this->tabNode->begin();
    ++it;
    for (++it; it != this->tabNode->end(); ++it) {

    if ((*it)->getEtat() == R1) {
    cout << "R1 = " << (*it)->getName().data() << endl;
    }
    if ((*it)->getEtat() == R2) {
    cout << "R2 = " << (*it)->getName().data() << endl;
    }
    if ((*it)->getEtat() == R3) {
    cout << "R3 = " << (*it)->getName().data() << endl;
    }
    }
    cout << "--------------------------" << endl;*/
    // === Écriture : 1) WFP_sfg_eff des entrées constantes
    str = "   // WFP_sfg_eff par rapport aux entrées constantes";
    this->listeStrWFP_sfg_eff.push_back(str);
    it = this->tabNode->begin();
    ++it;
    for (++it; it != this->tabNode->end(); ++it) {
        nodeGFD = dynamic_cast<NoeudGFD *> (*it);
        assert(nodeGFD!=NULL);
        if (nodeGFD->getTypeNodeGFD() == DATA) {
            nodeData = (NoeudData *) *it;
            if (nodeData->getTypeVar()->getTypeVarFonct() == CONST) {
                for(int i = 0 ; i < nodeData->getNbSucc() ; i++){
                    nodeOpSucc = dynamic_cast<NoeudOps *> (nodeData->getElementSucc(i));
                    assert(nodeOpSucc!=NULL);
                    // === Save Nums
                    hasVariableWidth = nodeData->getVariableWidth();
                    ossData.str("");
					ossNumIn.str("");
					ossOpSucc.str("");
                    ss.str("");
					ossData << nodeData->getNumCDFG();
					ossNumIn << nodeData->getEdgeSucc(i)->getNumInput();
					ossOpSucc << nodeOpSucc->getNumSFG();
					// Ecriture WFP_sfg_eff par rapport aux entrées et constantes
					if (!hasVariableWidth) {
						// WFP_sfg_eff[P1][j] = WFP[P1][j];
						str = "   WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]";
						//str += " = WFP_sfg[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "];";
                        ss << scientific << nodeData->getValeur();
						str += " = nbBitSignif("+ ss.str() + ", WFP_sfg[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
                        #if DEBUG
						str += "\nfprintf(pfile, \"nbBitSignif(%f, %i) = %i\\n\", " + ss.str() + ", WFP_sfg[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "], nbBitSignif("+ ss.str() + ", WFP_sfg[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]));";
						str += "\nfprintf(pfile, \"WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]: %i\\n\", WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
						#endif
						this->listeStrWFP_sfg_eff.push_back(str);
					}
					else {
						// WFP_sfg_eff[P1][j] = min(WFP[P1][j], tabInputWd[xDataP"]);
						str = "   WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]";
						str += " = min(WFP_sfg[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "],";
						//str += " tabInputWd[" + ossData.str() + "]);";

                        ss << scientific << nodeData->getValeur();
						str += " nbBitSignif("+ ss.str() + ", tabInputWd[" + ossData.str() + "]));";
                        #if DEBUG
						str += "\nfprintf(pfile, \"nbBitSignif(%f, %i) = %i\\n\", " + ss.str() + ", tabInputWd[" + ossData.str() + "], nbBitSignif("+ ss.str() + ", tabInputWd[" + ossData.str() + "]));";
						str += "\nfprintf(pfile, \"WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]: %i\\n\", WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
						#endif
						this->listeStrWFP_sfg_eff.push_back(str);
					}
				}
			}
		}
	}

	// === Écriture : 2) WFP_sfg_eff a l'aide des br
	list<NoeudGraph*>::iterator itList;
	for (itList = listTriBr->begin(); itList != listTriBr->end(); ++itList) {
		nodeSrcBruit = dynamic_cast<NoeudSrcBruit *> (*itList);
		assert(nodeSrcBruit != NULL);
		str = "   // " + nodeSrcBruit->getName();  // Affichage de commentaire dans le computePb a travers la liste correspondant aux expressions de bruit dans les noeud src bruit
		this->listeStrWFP_sfg_eff.push_back(str);

		nodeGFD = dynamic_cast<NoeudGFD *> (nodeSrcBruit->getElementPred(0));
		nodeGFD2 = dynamic_cast<NoeudGFD *> (nodeSrcBruit->getElementSucc(0));
		assert(nodeGFD != NULL && nodeGFD2 != NULL);

		if (nodeGFD->getTypeNodeGFD() == DATA) { // Cas GENERAL ou INPUT
			nodeData = (NoeudData *) nodeGFD;
			if (nodeData->isNodeSource()) { // Cas INPUT
				preCalculWFPsfgToWFPsfgeff_CaseInput(nodeSrcBruit);
			}
			else {
				nodeGFD=dynamic_cast<NoeudGFD *> (nodeGFD->getElementPred(0));
				if(nodeGFD->getTypeNodeGFD()==OPS) { // Cas GENERAL
					this->preCalculWFPsfgToWFPsfgeff_CaseGeneral(nodeSrcBruit); // Cas général 1
				}
				else{
					this->preCalculWFPsfgToWFPsfgeff_CaseGeneral3(nodeSrcBruit); // Cas général 3
				}
			}
		}
		else if (nodeGFD->getTypeNodeGFD() == OPS){
			nodeData = dynamic_cast<NoeudData *> (nodeGFD2);
			assert(nodeData != NULL);

			if(!nodeData->isNodeOutput()){// Cas OUTPUT probleme ici
				this->preCalculWFPsfgToWFPsfgeff_CaseGeneral2(nodeSrcBruit); //Cas general 2
			}
			else{
				this->preCalculWFPsfgToWFPsfgeff_CaseOutput(nodeSrcBruit);
			}

		}
		else { // Cas OUTPUT
			trace_error("Problème de construction au niveau des Br");
			exit(0);
		}
	}

	// Affichage pour verifier le contenu des différents Br
	/*for (itList = listTriBr->begin(); itList != listTriBr->end(); ++itList) {
	  nodeSrcBruit = dynamic_cast<NoeudSrcBruit *> (*itList);
	  cout<<nodeSrcBruit->getName()<<endl;
	  for (int i = 0; i < nodeSrcBruit->getNbSuppressKBits(); i++) {
	  cout << "   K[" << i << "] = " << nodeSrcBruit->getNumSuppressKBits(i) << ";" << endl;
	  cout << "   Q[" << i << "] = " << nodeSrcBruit->getNumStepQuantif(i) << ";" << endl;
	  }
	  cout<<endl;
	  }*/
	delete (listTriBr);
}

/**
 *	Pré calcul de la propagation des Wfp effectif cas par défaut
 * \author Jean-Charles Naud
 *
 */
void Gfd::preCalculWFPsfgToWFPsfgeff_CaseGeneral(NoeudSrcBruit* nodeSrcBruit) {
	NoeudOps * nodeOpPred, *nodeOpSucc;
	NoeudData* nodeData;
	bool hasVariableWidth;
	string str;
	std::ostringstream ossBr, ossOpSucc, ossOpPred, ossData, ossNumIn;
	ossBr.str("");
	nodeOpSucc = dynamic_cast<NoeudOps *> (nodeSrcBruit->getElementSucc(0));
	nodeData = dynamic_cast<NoeudData *> (nodeSrcBruit->getElementPred(0));
	nodeOpPred = dynamic_cast<NoeudOps *> (nodeData->getElementPred(0));

	// === Save Nums
	hasVariableWidth = nodeData->getVariableWidth();
	ossOpSucc << nodeOpSucc->getNumSFG();
	ossNumIn << nodeSrcBruit->getEdgeSucc(0)->getNumInput();
	ossOpPred << nodeOpPred->getNumSFG();
	ossData << nodeData->getNumCDFG();

	if (!hasVariableWidth) { // Data sans pragma
		// = Ecriture WFP_sfg_eff
		this->calculWFPeffOps(nodeOpPred);
		// WFP_sfg_effs[P2][in] = min(WFP_sfg_eff[P1][2], WFP_sfg[P2][in] )
		str = "   WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "] = ";
		str += "min(WFP_sfg_eff[" + ossOpPred.str() + "*3+2], "; // si 2 est la sortie
		str += "   WFP_sfg[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
		#if DEBUG
		str += "\nfprintf(pfile, \"WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]: %i\\n\", WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
        #endif
		this->listeStrWFP_sfg_eff.push_back(str);

		// = Écriture K I
		str = this->calculKOps(nodeSrcBruit, nodeOpPred);
		nodeSrcBruit->addSuppressKBits(str);

		// = Écriture Q I
		str = "(pow(2, (-(WFP_sfg_eff[" + ossOpPred.str() + "*3+2]))))";
		nodeSrcBruit->addStepQuantif(str);

		// = Ecriture Mode Quantif I
		str = "WFP_sfg_ModeQuantif[" + ossOpPred.str() + "*3+2]";
		nodeSrcBruit->addModeQuantif(str);

		nodeSrcBruit->addSigneMean(1);
		nodeSrcBruit->addConstante(1);

		// = Ecriture K IV
		str  = "   WFP_sfg_eff[" + ossOpPred.str() + "*3+2]"; // Si 2 est la sortie
		str += " - WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]";
		nodeSrcBruit->addSuppressKBits(str);

		// = Écriture Q IV
		str = "(pow(2, (-(WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]))))";
		nodeSrcBruit->addStepQuantif(str);

		// = Ecriture Mode Quantif IV
		str = "WFP_sfg_ModeQuantif[" + ossOpSucc.str() + "*3+"+ ossNumIn.str() +"]";
		nodeSrcBruit->addModeQuantif(str);

		nodeSrcBruit->addSigneMean(1);
		nodeSrcBruit->addConstante(1);
	}
	else { // Data avec pragma
		// = Ecriture WFP_sfg_eff
		this->calculWFPeffOps(nodeOpPred);
		str = "   WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "] = ";
		str += "min(min (WFP_sfg_eff[" + ossOpPred.str() + "*3+2], "; // si 2 est la sortie
		str += "tabInputWd[" + ossData.str() + "]), ";
		str += "   WFP_sfg[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
        #if DEBUG
		str += "\nfprintf(pfile, \"WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]: %i\\n\", WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
        #endif
		this->listeStrWFP_sfg_eff.push_back(str);

		// = Ecriture K I
		str = this->calculKOps(nodeSrcBruit, nodeOpPred);
		nodeSrcBruit->addSuppressKBits(str);

		// = Écriture Q I
		str = "(pow(2, (-(WFP_sfg_eff[" + ossOpPred.str() + "*3+2]))))";
		nodeSrcBruit->addStepQuantif(str);

		// = Ecriture Mode Quantif I
		str = "WFP_sfg_ModeQuantif[" + ossOpPred.str() + "*3+2]";
		nodeSrcBruit->addModeQuantif(str);

		nodeSrcBruit->addSigneMean(1);
		nodeSrcBruit->addConstante(1);

		// = Ecriture K II
		str = " fK(WFP_sfg_eff[" + ossOpPred.str() + "*3+2]"; // Si 2 est la sortie
		str += " - tabInputWd[" + ossData.str() + "])";
		nodeSrcBruit->addSuppressKBits(str);

		// = Écriture Q II
		str = "(pow(2, (-(min(tabInputWd["+ossData.str()+"], WFP_sfg_eff[" + ossOpPred.str() + "*3+2])))))";
		nodeSrcBruit->addStepQuantif(str);

		// = Ecriture Mode Quantif II
		str = "tabInputWdquantMode[" + ossData.str() + "]";
		nodeSrcBruit->addModeQuantif(str);

		nodeSrcBruit->addSigneMean(1);
		nodeSrcBruit->addConstante(1);

		// = Ecriture K III
		str = " min(WFP_sfg_eff[" + ossOpPred.str() + "*3+2],";
		str += " tabInputWd[" + ossData.str() + "])";
		str += " - WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]";
		nodeSrcBruit->addSuppressKBits(str);

		// = Ecriture Q III
		str = "(pow(2, (-(WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]))))";
		nodeSrcBruit->addStepQuantif(str);


		//  = Ecriture Mode Quantif III
		str = "WFP_sfg_ModeQuantif[" + ossOpSucc.str() + "*3+"+ ossNumIn.str() +"]";
		nodeSrcBruit->addModeQuantif(str);

		nodeSrcBruit->addSigneMean(1);
		nodeSrcBruit->addConstante(1);
	}
}

/**
 * Pré calcul de la propagation des Wfp effectif cas général2 (modèle à 3 sources de bruit)
 * \author Jean-Charles Naud
 *
 */
void Gfd::preCalculWFPsfgToWFPsfgeff_CaseGeneral2(NoeudSrcBruit* nodeSrcBruit) {

	NoeudOps * nodeOpPred;
	NoeudData* nodeData;
	bool hasVariableWidth;
	string str;
	std::ostringstream ossBr, ossOpSucc, ossOpPred, ossData, ossNumIn;
	ossBr.str("");
	nodeData = dynamic_cast<NoeudData *> (nodeSrcBruit->getElementSucc(0));
	nodeOpPred = dynamic_cast<NoeudOps *> (nodeSrcBruit->getElementPred(0));

	// === Save Nums
	hasVariableWidth = nodeData->getVariableWidth();
	ossOpPred << nodeOpPred->getNumSFG();
	ossData << nodeData->getNumCDFG();
	ossNumIn << nodeSrcBruit->getEdgePred(0)->getNumInput();

	if (!hasVariableWidth) { // Data sans pragma
		// = Ecriture WFP_sfg_eff
		this->calculWFPeffOps(nodeOpPred);

		// = Écriture K I
		str = this->calculKOps(nodeSrcBruit, nodeOpPred);
		nodeSrcBruit->addSuppressKBits(str);

		// = Ecriture Q I
		str = "(pow(2, (-(WFP_sfg_eff[" + ossOpPred.str() + "*3+2]))))";
		nodeSrcBruit->addStepQuantif(str);

		// = Ecriture Mode Quantif I
		str = "WFP_sfg_ModeQuantif[" + ossOpPred.str() + "*3+2]";
		nodeSrcBruit->addModeQuantif(str);

		nodeSrcBruit->addSigneMean(1);
		nodeSrcBruit->addConstante(1);

	}
	else { // Data avec pragma
		// = Ecriture WFP_sfg_eff
		this->calculWFPeffOps(nodeOpPred);

		// = Écriture K I
		str = this->calculKOps(nodeSrcBruit, nodeOpPred);
		nodeSrcBruit->addSuppressKBits(str);

		// = Ecriture Q I
		str = "(pow(2, (-(WFP_sfg_eff[" + ossOpPred.str() + "*3+2]))))";
		//}
		nodeSrcBruit->addStepQuantif(str);

		// = Ecriture Mode Quantif I
		str = "WFP_sfg_ModeQuantif[" + ossOpPred.str() + "*3+2]";
		nodeSrcBruit->addModeQuantif(str);

		nodeSrcBruit->addSigneMean(1);
		nodeSrcBruit->addConstante(1);

		// = Ecriture K II
		// WFP_sfg_eff[P1][2] - tabInputWd[D1]
		str = " fK(WFP_sfg_eff[" + ossOpPred.str() + "*3+2]"; // Si 2 est la sortie
		str += " - tabInputWd[" + ossData.str() + "])";
		nodeSrcBruit->addSuppressKBits(str);

		// = Ecriture Q II
		str = "(pow(2, (-(min(tabInputWd[" + ossData.str() + "], WFP_sfg_eff[" + ossOpPred.str() + "*3+2])))))";
		nodeSrcBruit->addStepQuantif(str);

		// = Ecriture Mode Quantif II
		str = "tabInputWdquantMode[" + ossData.str() + "]";
		nodeSrcBruit->addModeQuantif(str);

		nodeSrcBruit->addSigneMean(1);
		nodeSrcBruit->addConstante(1);
}

}

/**
 * Pré calcul de la propagation des Wfp effectif cas général3 (modèle des sources corrélé + gestion rebouclage)
 * \author Jean-Charles Naud
 *
 */
void Gfd::preCalculWFPsfgToWFPsfgeff_CaseGeneral3(NoeudSrcBruit* nodeSrcBruit) {

	NoeudOps * nodeOpPred, *nodeOpSucc;
	NoeudSrcBruit* nodeSrcBrPred;
	NoeudData* nodeData;
	bool hasVariableWidth;
	string str;
	std::ostringstream ossBr, ossOpSucc, ossOpPred, ossData, ossNumIn;
	ossBr.str("");
	nodeOpSucc = dynamic_cast<NoeudOps *> (nodeSrcBruit->getElementSucc(0));
	nodeData = dynamic_cast<NoeudData *> (nodeSrcBruit->getElementPred(0));
	nodeSrcBrPred= dynamic_cast<NoeudSrcBruit *> (nodeData->getElementPred(0));
	nodeOpPred = dynamic_cast<NoeudOps *> (nodeSrcBrPred->getElementPred(0));

	// === Save Nums
	hasVariableWidth = nodeData->getVariableWidth();
	ossOpSucc << nodeOpSucc->getNumSFG();
	ossNumIn << nodeSrcBruit->getEdgeSucc(0)->getNumInput();
	ossOpPred << nodeOpPred->getNumSFG();
	ossData << nodeData->getNumCDFG();

	if (nodeSrcBruit->getEtat() == R2) { // Rebouclage : Les WFP_sfg_effs des noeuds precedants existent (on prend le pire cas)
		if (!hasVariableWidth) { // Data sans pragma

			// = Écriture WFP_sfg_eff
			// WFP_sfg_effs[P2][in] = min(WFP_sfg[P1][2], WFP_sfg[P2][in] )
			str = "   WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "] = ";
			str += "min(WFP_sfg[" + ossOpPred.str() + "*3+2], "; // si 2 est la sortie (on prend le pire cas)
			str += "WFP_sfg[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
            #if DEBUG
            str += "\nfprintf(pfile, \"WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]: %i\\n\", WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
            #endif
			this->listeStrWFP_sfg_eff.push_back(str);

			// = Écriture K IV
			// WFP_sfg = WFP_sfg_eff normalement
			str = " WFP_sfg_eff[" + ossOpPred.str() + "*3+2]"; // Si 2 est la sortie
			str += " - WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]";
			nodeSrcBruit->addSuppressKBits(str);

			// = Écriture Q IV
			str = "(pow(2, (-(WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]))))";
			nodeSrcBruit->addStepQuantif(str);

			str = "WFP_sfg_ModeQuantif[" + ossOpSucc.str() + "*3+"+ ossNumIn.str() +"]";
			nodeSrcBruit->addModeQuantif(str);

			nodeSrcBruit->addSigneMean(1);
			nodeSrcBruit->addConstante(1);
		}
		else { // Data avec pragma
			// = Ecriture WFP_sfg_eff
			str = "   WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "] = ";
			str += "min(min (WFP_sfg[" + ossOpPred.str() + "*3+2], "; // si 2 est la sortie (on prend le pire cas)
			str += "tabInputWd[" + ossData.str() + "]), ";
			str += "WFP_sfg[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
            #if DEBUG
            str += "\nfprintf(pfile, \"WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]: %i\\n\", WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
            #endif
			this->listeStrWFP_sfg_eff.push_back(str);
			// == Ecriture K III
			str = " min(WFP_sfg_eff[" + ossOpPred.str() + "*3+2],";
			str += " tabInputWd[" + ossData.str() + "])";
			str += " - WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]";
			nodeSrcBruit->addSuppressKBits(str);

			// = Ecriture Q III
			str = "(pow(2, (-(WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]))))";
			nodeSrcBruit->addStepQuantif(str);

			// = Ecriture Mode Quantif III
			str = "WFP_sfg_ModeQuantif[" + ossOpSucc.str() + "*3+"+ ossNumIn.str() +"]";
			nodeSrcBruit->addModeQuantif(str);

			nodeSrcBruit->addSigneMean(1);
			nodeSrcBruit->addConstante(1);

		}
	}
	else { // Pas de rebouclage, on fait tout et (on prend pas le pire cas)
		if (!hasVariableWidth) { // Data sans pragma
			// = Ecriture WFP_sfg_eff
			str = "   WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "] = ";
			str += "min(WFP_sfg_eff[" + ossOpPred.str() + "*3+2], "; // si 2 est la sortie
			str += "WFP_sfg[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
            #if DEBUG
            str += "\nfprintf(pfile, \"WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]: %i\\n\", WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
            #endif
			this->listeStrWFP_sfg_eff.push_back(str);

			// = Ecriture K IV
			str = "WFP_sfg_eff[" + ossOpPred.str() + "*3+2]"; // Si 2 est la sortie
			str += " - WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]";
			nodeSrcBruit->addSuppressKBits(str);

			// = Écriture Q IV
			str = "(pow(2, (-(WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]))))";
			nodeSrcBruit->addStepQuantif(str);

			// = Ecriture Mode Quantif IV
			str = "WFP_sfg_ModeQuantif[" + ossOpSucc.str() + "*3+"+ ossNumIn.str() +"]";
			nodeSrcBruit->addModeQuantif(str);

			nodeSrcBruit->addSigneMean(1);
			nodeSrcBruit->addConstante(1);
		}
		else { // Data avec pragma
			// = Ecriture WFP_sfg_eff
			str = "   WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "] = ";
			str += "min(min (WFP_sfg_eff[" + ossOpPred.str() + "*3+2], "; // si 2 est la sortie
			str += "tabInputWd[" + ossData.str() + "]), ";
			str += "WFP_sfg[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
            #if DEBUG
            str += "\nfprintf(pfile, \"WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]: %i\\n\", WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
            #endif
			this->listeStrWFP_sfg_eff.push_back(str);

			// == Ecriture K III
			str = " min(WFP_sfg_eff[" + ossOpPred.str() + "*3+2],";
			str += " tabInputWd[" + ossData.str() + "])";
			str += " - WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]";
			nodeSrcBruit->addSuppressKBits(str);

			// = Écriture Q III
			str = "(pow(2, (-(WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]))))";
			nodeSrcBruit->addStepQuantif(str);

			// = Ecriture Mode Quantif II
			str = "WFP_sfg_ModeQuantif[" + ossOpSucc.str() + "*3+"+ ossNumIn.str() +"]";
			nodeSrcBruit->addModeQuantif(str);

			nodeSrcBruit->addSigneMean(1);
			nodeSrcBruit->addConstante(1);
		}
	}
}
/**
 * Pré calcul de la propagation des Wfp effectif cas des entrées
 * \author Jean-Charles Naud
 *
 */
void Gfd::preCalculWFPsfgToWFPsfgeff_CaseInput(NoeudSrcBruit * nodeSrcBruit) {
	NoeudOps *nodeOpSucc;
	NoeudData* nodeData;
	bool hasVariableWidth;
	string str;
	std::ostringstream ossBr, ossOpSucc, ossData, ossNumIn;

	nodeOpSucc = dynamic_cast<NoeudOps *> (nodeSrcBruit->getElementSucc(0));
	nodeData = dynamic_cast<NoeudData *> (nodeSrcBruit->getElementPred(0));
	//cout<<"cas Input pour le noeud "<<nodeSrcBruit->getName()<<endl;
	// === Save Num
	hasVariableWidth = nodeData->getVariableWidth();
	ossOpSucc << nodeOpSucc->getNumSFG();
	ossNumIn << nodeSrcBruit->getEdgeSucc(0)->getNumInput();
	ossData << nodeData->getNumCDFG();
	if (!hasVariableWidth) {
		// WFP_sfg_eff[P2][j] = WFP[P2][j];
		//  = Ecriture WFP effectif
		str = "WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]";
		str += " = WFP_sfg[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "];";
        #if DEBUG
        str += "\nfprintf(pfile, \"WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]: %i\\n\", WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
        #endif
		this->listeStrWFP_sfg_eff.push_back(str);
		// = Écriture K III
		str = "1000";
		nodeSrcBruit->addSuppressKBits(str);

		// = Écriture Q III
		str = "(pow(2, (-(WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]))))";
		nodeSrcBruit->addStepQuantif(str);

		// = Ecriture Mode Quantif III
		str = "WFP_sfg_ModeQuantif[" + ossOpSucc.str() + "*3+"+ ossNumIn.str() +"]";
		nodeSrcBruit->addModeQuantif(str);

		nodeSrcBruit->addSigneMean(1);
		nodeSrcBruit->addConstante(1);
	}
	else {
		// WFP_sfg_eff[P2][j] = min(WFP[P2][j], tabInputWd[xDataP"]);
		//  = Ecriture WFP effectif
		str = "   WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]";
		str += " = min(WFP_sfg[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "],";
		str += " tabInputWd[" + ossData.str() + "]);";
        #if DEBUG
        str += "\nfprintf(pfile, \"WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]: %i\\n\", WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]);";
        #endif
		this->listeStrWFP_sfg_eff.push_back(str);

		// = Écriture K IV
		str = "fK(tabInputWd[" + ossData.str() + "]";
		str += " - WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "])";
		nodeSrcBruit->addSuppressKBits(str);


		// = Écriture Q IV
		str = "(pow(2, (-(WFP_sfg_eff[" + ossOpSucc.str() + "*3+" + ossNumIn.str() + "]))))";
		nodeSrcBruit->addStepQuantif(str);

		// = Ecriture Mode Quantif IV
		str = "WFP_sfg_ModeQuantif[" + ossOpSucc.str() + "*3+"+ ossNumIn.str() +"]";
		nodeSrcBruit->addModeQuantif(str);

		nodeSrcBruit->addSigneMean(1);
		nodeSrcBruit->addConstante(1);
	}

}

/**
 * Pré calcul de la propagation des Wfp effectif cas des sorties
 * \author Jean-Charles Naud
 *
 */
void Gfd::preCalculWFPsfgToWFPsfgeff_CaseOutput(NoeudSrcBruit * nodeSrcBruit) {
	NoeudOps *nodeOpPred;
	NoeudData* nodeData;
	bool hasVariableWidth;
	string str;
	std::ostringstream ossBr, ossOpPred, ossData, ossNumIn;
	//cout<<"cas output pour le noeud "<<nodeSrcBruit->getName()<<endl;
	nodeData = dynamic_cast<NoeudData *> (nodeSrcBruit->getElementSucc(0));
	nodeOpPred = dynamic_cast<NoeudOps *> (nodeSrcBruit->getElementPred(0));

	// === Save Nums
	hasVariableWidth = nodeData->getVariableWidth();
	ossOpPred << nodeOpPred->getNumSFG();
	ossData << nodeData->getNumCDFG();

	if (!hasVariableWidth) { // Data sans pragma
		// = Ecriture WFP_sfg_eff
		this->calculWFPeffOps(nodeOpPred);

		// = Écriture K I
		str = calculKOps(nodeSrcBruit, nodeOpPred);
		nodeSrcBruit->addSuppressKBits(str);

		// = Écriture Q I
		str = "(pow(2, (-(WFP_sfg_eff[" + ossOpPred.str() + "*3+2]))))";
		nodeSrcBruit->addStepQuantif(str);

		// = Ecriture mode quantif I
		str = "WFP_sfg_ModeQuantif[" + ossOpPred.str() + "*3+2]";
		nodeSrcBruit->addModeQuantif(str);

		nodeSrcBruit->addSigneMean(1);
		nodeSrcBruit->addConstante(1);
	}
	else { // Data avec pragma

		// = Ecriture WFP_sfg_eff
		this->calculWFPeffOps(nodeOpPred);

		// = Écriture K I
		str = calculKOps(nodeSrcBruit, nodeOpPred);
		nodeSrcBruit->addSuppressKBits(str);

		// = Écriture Q I
		str = "(pow(2, (-(WFP_sfg_eff[" + ossOpPred.str() + "*3+2]))))";
		nodeSrcBruit->addStepQuantif(str);

		// = Ecriture mode quantif I
		str = "WFP_sfg_ModeQuantif[" + ossOpPred.str() + "*3+2]";
		nodeSrcBruit->addModeQuantif(str);

		nodeSrcBruit->addSigneMean(1);
		nodeSrcBruit->addConstante(1);

		// = Écriture K II
		str = " fK(WFP_sfg_eff[" + ossOpPred.str() + "*3+2]";
		str += " - tabInputWd[" + ossData.str() + "])";
		nodeSrcBruit->addSuppressKBits(str);

		// = Écriture Q II
		str = "(pow(2, (-(min(tabInputWd[" + ossData.str() + "], WFP_sfg_eff[" + ossOpPred.str() + "*3+2])))))";
		nodeSrcBruit->addStepQuantif(str);

		// = Ecriture Mode Quantif II
		str = "tabInputWdquantMode[" + ossData.str() + "]";
		nodeSrcBruit->addModeQuantif(str);

		nodeSrcBruit->addSigneMean(1);
		nodeSrcBruit->addConstante(1);
	}
}


/**
 *	Création de la liste d'amont en aval des sources bruit
 *	\author Jean-Charles Naud
 *
 */
int NoeudGraph::preCalculWFPsfgToWFPsfgeff_TriBrGen(list<NoeudGraph *> * listTriBr) {

	int count;
	int test, testTemp;
	NoeudGFD * nodeGFD;
	NoeudData *nodeData;
	//this->(ENTRAITEMENT);
	nodeGFD = dynamic_cast<NoeudGFD *> (this);
	assert(nodeGFD != NULL);

	switch (nodeGFD->getTypeNodeGFD()) {
		case DATA:
			nodeData = (NoeudData *) nodeGFD;

			if (nodeData->isNodeSource()) {
				this->setEtat(VISITE);
				return 0; // return: fin et pas de cycle detecté.
			}
			else {
				switch (this->getEtat()) {
					case NONVISITE:
						this->setEtat(ENTRAITEMENT);
						test = nodeData->getElementPred(0)->preCalculWFPsfgToWFPsfgeff_TriBrGen(listTriBr);
						// Decente
						if (this->getEtat() == R1) {
							this->setEtat(R3);
						}
						else { // xxx enlever pour la version 2
							if (test == 1) {
								this->setEtat(R2);
								if (nodeData->getTypeNodeData() == DATA_SRC_BRUIT) { // "br"
									listTriBr->push_back(this);
								}
							}
							else {
								this->setEtat(VISITE); // xxx enlever pour la version 2
								if (nodeData->getTypeNodeData() == DATA_SRC_BRUIT) { // "br"
									listTriBr->push_back(this);
								}
							}
						}
						return 0;
						break;
					case VISITE: // Déjà traité
					case R2: // Déjà traité
					case R3: // Déjà traité
						return 0;// return: fin et pas de cycle detecté.
						break;
					case ENTRAITEMENT: // Déctection d'un cycle
					case R1: // Déctection d'un cycle
						this->setEtat(R1);
						return 1; // return: cycle detecté.
						break;
					default:
						trace_error("Type non pris en compte (DATA)");
						exit(EXIT_FAILURE);
				}
			}
		case OPS:
			switch (this->getEtat()) {
				case NONVISITE:
					this->setEtat(ENTRAITEMENT);
					test = 0;
					for (count = 0; count < this->getNbPred(); count++) {
						nodeGFD = dynamic_cast<NoeudGFD*> (this->getElementPred(count));
						assert(nodeGFD != NULL);
						testTemp = nodeGFD->preCalculWFPsfgToWFPsfgeff_TriBrGen(listTriBr);
						if (testTemp == 1) {
							test = 1;
						}
					}
					if (test == 1) { // au moin un des pred est un noeud data de rebouclage.
						this->setEtat(R2);
					}
					else {
						this->setEtat(VISITE);
					}
					return 0;
					break;
				case VISITE: // Déjà traité
				case R2: // Déjà traité
					return 0; // Logiquement, jamais utilisé
					break;
				case ENTRAITEMENT:
					trace_error("OP ENTRAITEMENT: Impossible");
					exit(EXIT_FAILURE);
					break;
				default:
					trace_error("Type non pris en compte (OP)");
					exit(EXIT_FAILURE);
			}
			break;
		default:
			trace_error("Type non pris en compte (OP)");
			exit(EXIT_FAILURE);
	}
	return 0;
}



/**
 * Calcul du nombres bit éliminé pour une opération
 * \author Jean-Charles Naud
 */
string Gfd::calculKOps(NoeudSrcBruit* nodeSrcBruit, NoeudOps * noeudOps) {
	string str;
	std::ostringstream ossOps, ossj;

	ossOps << noeudOps->getNumSFG(); //int to str

	// === Expression des K présent aux sorties des opérations
	switch (noeudOps->getTypeOps()) {
		case OPS_ADD:
		case OPS_SUB:
			//max(WFP_sfg_eff[xOps][0], WFP_sfg_eff[xOps][1]) - WFP_sfg[xOps][2];
			str = "max(WFP_sfg_eff[" + ossOps.str() + "*3+0],";
			str += " WFP_sfg_eff[" + ossOps.str() + "*3+1])";
			str += " - WFP_sfg_eff[" + ossOps.str() + "*3+2]";// Si 2 est la sortie
			break;
		case OPS_MULT:
			// WFP_sfg_eff[xOps][0] + WFP_sfg_eff[xOps][1] - WFP_sfg[xOps][2];
			str = "WFP_sfg_eff[" + ossOps.str() + "*3+0]";
			str += " + WFP_sfg_eff[" + ossOps.str() + "*3+1]";
			str += " - WFP_sfg_eff[" + ossOps.str() + "*3+2]";
			break;
		case OPS_SQRT:
		case OPS_DIV: // Cas spécial
			// infini;
			str = "999";
			break;
		case OPS_DELAY:
		case OPS_ABS:
        case OPS_NEG:
		case OPS_EQ:
			// WFP_sfg_eff[xOps][0] - WFP_sfg[xOps][2];
			str = "WFP_sfg_eff[" + ossOps.str() + "*3+0]"; // Si 2 est la sortie
			str += " - WFP_sfg_eff[" + ossOps.str() + "*3+2]";
			break;
		case OPS_SHL:
			// WFP_sfg_eff[xOps][0]-1 - WFP_sfg[xOps][2];
			str += "WFP_sfg_eff[" + ossOps.str() + "*3+0]-1";
			str += " - WFP_sfg_eff[" + ossOps.str() + "*3+2]"; // Si 2 est la sortie
			break;
		case OPS_SHR:
			// WFP_sfg_eff[xOps][0]+1 - WFP_sfg[xOps][2];
			str += "WFP_sfg_eff[" + ossOps.str() + "*3+0]+1";
			str += " - WFP_sfg_eff[" + ossOps.str() + "*3+2]"; // Si 2 est la sortie
			break;

		case OPS_PHI:
			// max(WFPeff[xOps][0], WFPeff[xOps][1], ...) - WFP_sfg[xOps][2]
			if (noeudOps->getNbPred() == 1) {
				str = "WFP_sfg_eff[" + ossOps.str() + "*3+0]";// WFPeff[xOps][2] = min(WFP[xOps][2], WFPeff[xOps][0]);
                str += " - WFP_sfg_eff[" + ossOps.str() + "*3+2]";
			}
			else {
				str = " max(";
                for (int j = 0; j < (noeudOps->getNbPred() - 1); j++) {
                    ossj.str("");
                    ossj << j;
                    str += "WFP_sfg_eff[" + ossOps.str() + "*3+" + ossj.str() + "], ";
                }
                str += "   WFP_sfg_eff[" + ossOps.str() + "*3+" + ossj.str() + "])";
                str += " - WFP_sfg_eff[" + ossOps.str() + "*3+2]";// Si 2 est la sortie
			}
			break;
		default:
			trace_error("OPS no know for K expression calculation");
			exit(EXIT_FAILURE);
	}
	return str;
}

/**
 * Calcul du Wfp effectif pour une opération
 * \author Jean-Charles Naud
 */

void Gfd::calculWFPeffOps(NoeudOps * noeudOps) {
	string str;
	std::ostringstream ossOps, ossj;

	ossOps << noeudOps->getNumSFG(); //int to str
	str = "   // " + typeEtatConvToStr(noeudOps->getEtat());
	this->listeStrWFP_sfg_eff.push_back(str);
	if (noeudOps->getEtat() == VISITE || noeudOps->getEtat() == R4) {
		// === Expression des WFPeff présent aux sorties des opérations
		switch (noeudOps->getTypeOps()) {
			case OPS_ADD:
			case OPS_SUB:
				// WFP_sfg_eff[xOps][2] = min(WFP_sfg[xOps][2], max(WFP_sfg_eff[xOps][0], WFP_sfg_eff[xOps][1]));
				str = "   WFP_sfg_eff[" + ossOps.str() + "*3+2]"; // Si 2 est la sortie
				str += " = min(WFP_sfg[" + ossOps.str() + "*3+2],";
				str += " max(WFP_sfg_eff[" + ossOps.str() + "*3+0],";
				str += "   WFP_sfg_eff[" + ossOps.str() + "*3+1]));";
				break;
			case OPS_MULT:
				// WFPeff[xOps][2] = min(WFP[xOps][2], WFPeff[xOps][0] + WFPeff[xOps][1]);
				str = "   WFP_sfg_eff[" + ossOps.str() + "*3+2]"; // Si 2 est la sortie
				str += " = min(WFP_sfg[" + ossOps.str() + "*3+2],";
				str += "   WFP_sfg_eff[" + ossOps.str() + "*3+0] +";
				str += "   WFP_sfg_eff[" + ossOps.str() + "*3+1]);";
				break;
			case OPS_SQRT:
			case OPS_DIV:
				// WFPeff[xOps][2] = WFP[xOps][2];
				str = "   WFP_sfg_eff[" + ossOps.str() + "*3+2]"; // Si 2 est la sortie
				str += " = WFP_sfg[" + ossOps.str() + "*3+2];";
				break;
			case OPS_DELAY:
			case OPS_ABS:
            case OPS_NEG:
			case OPS_EQ:
				// WFPeff[xOps][2] = min(WFP[xOps][2], WFPeff[xOps][0]);
				str = "   WFP_sfg_eff[" + ossOps.str() + "*3+2]"; // Si 2 est la sortie
				str += " = min(WFP_sfg[" + ossOps.str() + "*3+2],";
				str += " WFP_sfg_eff[" + ossOps.str() + "*3+0]);";
				break;
			case OPS_SHL:
				// WFPeff[xOps][2] = min(WFP[xOps][2], WFPeff[xOps][0]-1);
				str = "   WFP_sfg_eff[" + ossOps.str() + "*3+2]"; // Si 2 est la sortie
				str += " = min(WFP_sfg[" + ossOps.str() + "*3+2],";
				str += " WFP_sfg_eff[" + ossOps.str() + "*3+0]-1);";
				break;
			case OPS_SHR:
				// WFPeff[xOps][2] = min(WFP[xOps][2], WFPeff[xOps][0]+1);
				str = "   WFP_sfg_eff[" + ossOps.str() + "*3+2]"; // Si 2 est la sortie
				str += " = min(WFP_sfg[" + ossOps.str() + "*3+2],";
				str += " WFP_sfg_eff[" + ossOps.str() + "*3+0]+1);";
				break;

			case OPS_PHI:
				// WFPeff[xOps][2] = min(WFP[xOps][2], max(WFPeff[xOps][0], WFPeff[xOps][1], ...));
				str = "   WFP_sfg_eff[" + ossOps.str() + "*3+2]"; // Si 2 est la sortie
				str += " = min(WFP_sfg[" + ossOps.str() + "*3+2],";
				if (noeudOps->getNbPred() == 1) {
					str += " WFP_sfg_eff[" + ossOps.str() + "*3+0]);";// WFPeff[xOps][2] = min(WFP[xOps][2], WFPeff[xOps][0]);
				}
				else {
					str += " max(";
				}
				for (int j = 0; j < (noeudOps->getNbPred() - 1); j++) {
					ossj.str("");
					ossj << j;
					str += "WFP_sfg_eff[" + ossOps.str() + "*3+" + ossj.str() + "], ";
				}
				str += "WFP_sfg_eff[" + ossOps.str() + "*3+" + ossj.str() + "]));";
				break;
			default:
				trace_error("OPS no know for WFPeff expression calculation");
				exit(EXIT_FAILURE);
		}
        #if DEBUG
        str += "\nfprintf(pfile, \"WFP_sfg_eff[" + ossOps.str() + "*3+2]: %i\\n\", WFP_sfg_eff[" + ossOps.str() + "*3+2]);";
        #endif
		this->listeStrWFP_sfg_eff.push_back(str);
		if (noeudOps->getEtat() == VISITE) {
			noeudOps->setEtat(NONVISITE);
		}
		if (noeudOps->getEtat() == R2) {
			noeudOps->setEtat(R4);
		}
	}

}
