#if 1

/**
 *  \author Nicolas Simon
 *  \date   17/11/11
 *  \version 1.0
 *  \class FonctionTransfert
 */

#ifndef FONCTION_TRANSFERT_NON_MATLAB
#define FONCTION_TRANSFERT_NON_MATLAB

#include <cstring>
#include <iostream>
#include <set>
#include <string>
#include <sstream>
#include <vector>
#include <cassert>

#include "PseudoFonctionLineaire.hh"
#include <utils/Tool.hh>

class FonctionTransfert{

private:
	PseudoFonctionLineaire* m_denominateur; ///< Représente le denominateur de la fonction de transfert
	PseudoFonctionLineaire* m_numerateur; ///< Représente le numerateur de la fonction de transfert

public:

	FonctionTransfert(); 
	FonctionTransfert(PseudoFonctionLineaire* num, PseudoFonctionLineaire* den); 
	FonctionTransfert(PseudoFonctionLineaire* num);
	FonctionTransfert(const FonctionTransfert& other);
	~FonctionTransfert();
	
	void setDenominateur(PseudoFonctionLineaire* fct){
		this->m_denominateur = fct;
	} ///< Setteur du dénominateur
	PseudoFonctionLineaire* getDenominateur(){
		return m_denominateur;
	} ///< Getteur du dénominateur

	void setNumerateur(PseudoFonctionLineaire* fct){
		this->m_numerateur = fct;
	} ///< Setteur du numerateur
	PseudoFonctionLineaire* getNumerateur(){
		return m_numerateur;
	} ///< Getteur du numerateur

	string toString() const; ///< Rend en format string la fonction de transfert
	string toStringNum() const; ///< Rend le numerateur en format string
	string toStringDen() const; ///< Rend le denominateur en format string
	string getMatlabExpression(int numEdge) const {return "";}; ///< Rend la fonction de transfert dans le format adéquat pour les traitements Matlab


};

#endif
#endif
