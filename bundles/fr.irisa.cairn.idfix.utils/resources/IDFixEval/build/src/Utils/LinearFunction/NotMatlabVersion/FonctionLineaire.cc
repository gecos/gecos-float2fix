#include "FonctionLineaire.hh"

#include <graph/dfg/datanode/NoeudData.hh>


/**
 *	Constructeur
 *
 *	\param node : Noeud à partir duquel la FonctionLineaire a été crée
 *
 *	\author Nicolas Simon
 *	\date 17/11/11
 *
 */
FonctionLineaire::FonctionLineaire(NoeudGFD* node){
	this->m_node = node;
	this->m_isCste = false;
	this->m_csteValue = 0;
}

/**
 *	Constructeur
 *
 * 	\param value : Defini la valeur de la constante
 *	\param node : Noeud à partir duquel la FonctionLineaire a été crée
 *	
 *	\author Nicolas Simon
 *	\date 17/11/11
 *
 */
FonctionLineaire::FonctionLineaire(float value, NoeudGFD* node){
	this->m_node = node;
	this->m_isCste = true;
	this->m_csteValue = value;
}

/**
 *	Constructeur par copie
 *
 *	\param other : FonctionLineaire a copier 
 *
 *	\author Nicolas Simon
 *	\date 17/11/11
 *
 */
FonctionLineaire::FonctionLineaire(const FonctionLineaire& other){
	this->m_node = other.m_node;
	this->m_isCste = other.m_isCste;
	this->m_csteValue = other.m_csteValue;

	for(int i = 0 ; i< (int) other.m_vectorPseudoFctLineaire.size() ; i++)
		this->m_vectorPseudoFctLineaire.push_back(new PseudoFonctionLineaire(*other.m_vectorPseudoFctLineaire[i]));
}
/**
 *	Destructeur
 *
 *	\author Nicolas Simon
 *	\date 17/11/11
 *
 */
FonctionLineaire::~FonctionLineaire(){
	for(int i = 0 ; i< this->getNbPseudoFctLineaire() ; i++)
		delete m_vectorPseudoFctLineaire[i];
}
/**
 *	Fonction d'affichage	
 *
 *	\author Nicolas Simon
 *	\date 17/11/11
 *
 */
void FonctionLineaire::print(){
	if(this->m_isCste){
		cout<<"Constante : "<<this->m_node->getName()<<endl;
		cout<<"Valeur : "<<this->m_csteValue<<endl;
	}
	else {
		cout<<"Nom de la fonction : "<<this->m_node->getName()<<endl;
		for(int i = 0 ; i < this->getNbPseudoFctLineaire() ; i++){
			this->m_vectorPseudoFctLineaire[i]->print();
			cout<<endl;
		}
	}
	cout<<endl;
}

/**
 *	Redifinition de l'operateur +
 *
 *	\param other : FonctionLineaire a additionner 
 *
 *	\author Nicolas Simon
 *	\date 17/11/11
 *
 */
// Fusion si venant de la meme source autrement ajout au vector en fonction de l'operateur
FonctionLineaire* FonctionLineaire::operator+(const FonctionLineaire& other){
	FonctionLineaire* result = new FonctionLineaire(*this);
	PseudoFonctionLineaire* temp;
	bool fusion = false;

	for(int i = 0 ; i < (int) other.m_vectorPseudoFctLineaire.size() ; i++){
		for(int j = 0 ; j < (int) this->m_vectorPseudoFctLineaire.size() ; j++){
			if(this->getPseudoFctLineaire(j)->getStartNode() == other.m_vectorPseudoFctLineaire[i]->getStartNode()){ // Fusion
				*result->getPseudoFctLineaire(j) += *other.m_vectorPseudoFctLineaire[i];
				fusion = true;
				break;
			}
		}

		if(!fusion){
			temp = new PseudoFonctionLineaire(*other.m_vectorPseudoFctLineaire[i]);
			result->addPseudoFctLineaire(temp);
		}
		fusion = false;	

	}
	return result;
}
/**
 *	Redifinition de l'operateur -
 *
 *	\param other : FonctionLineaire a soustraire 
 *
 *	\author Nicolas Simon
 *	\date 17/11/11
 *
 */
// Fusion venant si de la meme source autrement ajout au vector en fonction de l'operateur
FonctionLineaire* FonctionLineaire::operator-(const FonctionLineaire& other){
	FonctionLineaire* result = new FonctionLineaire(*this);
	PseudoFonctionLineaire *temp, *addFct;
	bool fusion = false;

	for(int i = 0 ; i < (int) other.m_vectorPseudoFctLineaire.size() ; i++){
		for(int j = 0 ; j < this->getNbPseudoFctLineaire() ; j++){
			if(this->getPseudoFctLineaire(j)->getStartNode() == other.m_vectorPseudoFctLineaire[i]->getStartNode()){ // Fusion
				*result->getPseudoFctLineaire(j) -= *other.m_vectorPseudoFctLineaire[i];
				fusion = true;
				break;
			}
		}
		if(!fusion){
			temp = new PseudoFonctionLineaire(other.m_vectorPseudoFctLineaire[i]->getStartNode()); // Pseudo fonction lineare a 0 pour obtenir par la suite l'opposé
			addFct = *temp - *other.m_vectorPseudoFctLineaire[i];
			delete temp;
			result->addPseudoFctLineaire(addFct);
			fusion = false;

		}
	}
	return result;
}

/**
 *	Redifinition de l'operateur *
 *
 *	\param other : FonctionLineaire a multiplier
 *
 *	\author Nicolas Simon
 *	\date 17/11/11
 *
 */
FonctionLineaire* FonctionLineaire::operator*(const FonctionLineaire& other){
	if(this->isCste() && other.isCste()){
		return new FonctionLineaire(this->getCsteValue()*other.getCsteValue(), this->getNode());		
	}
	else if(this->isCste())
		return other.FTMult(this->getCsteValue());
	else if(other.isCste())
		return this->FTMult(other.getCsteValue());
	else{
		trace_error("Multiplication de fonction lineaire autre que par une constante non implémenté");
		exit(0);
	}
}


/**
 *	Permet la multiplication d'une FonctionLineaire par une constante
 *
 * 	\param value : Valeur de la constante
 *	\param name : Nom de la constante 
 *
 *	\author Nicolas Simon
 *	\date 17/11/11
 *
 */
// Uniquement en LTI dont uniquement multiplication par constante. 
FonctionLineaire* FonctionLineaire::FTMult(float value) const{
	PseudoFonctionLineaire* temp = new PseudoFonctionLineaire(NULL);
	FonctionLineaire* result = new FonctionLineaire(this->getNode());

	temp->addEltZ(0, value);
	for(int i = 0 ; i < this->getNbPseudoFctLineaire() ; i++){
		result->addPseudoFctLineaire(*this->getPseudoFctLineaire(i) * (*temp));
	}
	delete temp;
	return result;
}

/**
 *	Definition de l'operateur delay
 *
 *	\author Nicolas Simon
 *	\date 17/11/11
 *
 */
// Ajout de 1 a chaque exposant
void FonctionLineaire::operatorDelay(){
	for(int i = 0 ; i < this->getNbPseudoFctLineaire() ; i++){
		this->getPseudoFctLineaire(i)->operatorDelay();	
	}
}


FonctionLineaire* FonctionLineaire::operator/(const FonctionLineaire& other){
	if(other.isCste()){
		return this->FTDiv(other.getCsteValue());
	}
	else {
		trace_error("Division de fonction lineaire autre que par une constante non implémenté");
		exit(0);
	}
}

/**
 *	Permet la division d'une FonctionLineaire par une constante
 *
 * 	\param value : Valeur de la constante
 *	\param name : Nom de la constante 
 *
 *	\author Nicolas Simon
 *	\date 17/11/11
 *
 */
// Uniquement en LTI dont uniquement multiplication par constante. 
FonctionLineaire* FonctionLineaire::FTDiv(float value) const{
	PseudoFonctionLineaire* temp = new PseudoFonctionLineaire(NULL);
	FonctionLineaire* result = new FonctionLineaire(this->getNode());

	temp->addEltZ(0, value);
	for(int i = 0 ; i < this->getNbPseudoFctLineaire() ; i++){
		result->addPseudoFctLineaire(*this->getPseudoFctLineaire(i) / (*temp));
	}
	delete temp;
	return result;
}
