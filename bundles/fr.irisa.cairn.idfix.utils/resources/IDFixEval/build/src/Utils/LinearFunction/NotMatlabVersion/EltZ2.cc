/**
 *  \file EltZ.cc 
 *  \author Nicolas Simon 
 *  \date 09/11/11 
 *  \class EltZ 
 */

#include "EltZ2.hh"


/**
 * Constructeur
 * \param exposant : Exposant de Z
 * \param coeff : valeur du coefficient de Z
 * \param coeff : valeur ou nom attribué au coefficient de Z (utile pour affichage et matlab)
 *	
 *	\author Nicolas Simon
 *	\date 09/11/11
 */
EltZ2::EltZ2(int exposant, float coeff){
	this->m_exposant = exposant;
	this->m_coeff = coeff;
}

/*
 *	Constructeur par copie
 *	\param other : Element à copier
 *
 *	\author Nicolas Simon
 *	\date 09/11/11
 */
EltZ2::EltZ2(const EltZ2& other) : m_exposant(other.m_exposant), m_coeff(other.m_coeff){}

/**
 *	Destructeur
 *
 *	\auhtor Nicolas Simon
 *	\date 09/11/11
 */
EltZ2::~EltZ2(){}

/**
 *	Affichage de EltZ sous les forme :
 *		- m_coeff*Z^m_exposant 
 *		- (m_coeff_str)*Z^m_exposant
 *	
 *	\author Nicolas Simon
 *	\date 09/11/11
 */
void EltZ2::print(){
	cout<< m_coeff << "*Z^-" << m_exposant;
}

string EltZ2::toString(){
	string result;
	std::ostringstream oss;
	
	oss << m_coeff;
	oss << "*Z^";
	oss << m_exposant;
	
	result = oss.str();
	return result;
}

/**
 *	Refinition de l'operateur += 
 *	Précondition : Les exposant des EltZ doivent être égale
 *	
 *	\param other : EltZ qui va être ajouté
 *
 *	\author Nicolas Simon
 *	\date 09/11/11
 */
EltZ2& EltZ2::operator+=(const EltZ2& other){
	assert(this->m_exposant == other.m_exposant);
	this->m_coeff = this->m_coeff + other.m_coeff;
	
	return *this;
}

/**
 *	Refinition de l'operateur -= 
 *	Précondition : Les exposant des EltZ doivent être égale
 *	
 *	\param other : EltZ qui va être soustrai
 *
 *	\author Nicolas Simon
 *	\date 09/11/11
 */
EltZ2& EltZ2::operator-=(const EltZ2& other){
	assert(this->m_exposant == other.m_exposant);
	this->m_coeff = this->m_coeff - other.m_coeff;

	return *this;
}

/**
 *	Refinition de l'operateur += 
 *	
 *	\param other : EltZ qui va être multiplié 
 *
 *	\author Nicolas Simon
 *	\date 09/11/11
 */
EltZ2& EltZ2::operator*=(const EltZ2& other){
	this->m_coeff = this->m_coeff * other.m_coeff;
	this->m_exposant = this->m_exposant + other.m_exposant;

	return *this;
}

/**
 *	Refinition de l'operateur *
 *	
 *	\param other : EltZ qui va être multiplié 
 *
 *	\author Nicolas Simon
 *	\date 09/11/11
 */
EltZ2* EltZ2::operator*(const EltZ2& other){
	EltZ2* result = new EltZ2(*this);
	return &(*result*=other);
}

/**
 *	Refinition de l'operateur /=
 *	
 *	\param other : EltZ qui va être multiplié 
 *
 *	\author Nicolas Simon
 *	\date 09/11/11
 */
EltZ2& EltZ2::operator/=(const EltZ2& other){
	this->m_coeff = this->m_coeff / other.m_coeff;
	this->m_exposant = this->m_exposant - other.m_exposant;

	return *this;
}

/**
 *	Refinition de l'operateur /
 *	
 *	\param other : EltZ qui va être multiplié 
 *
 *	\author Nicolas Simon
 *	\date 09/11/11
 */
EltZ2* EltZ2::operator/(const EltZ2& other){
	EltZ2* result = new EltZ2(*this);
	return &(*result/=other);
}


/**
 *	Refinition de l'operateur +
 *	
 *	\param other : EltZ qui va être multiplié 
 *
 *	\author Nicolas Simon
 *	\date 09/11/11
 */
EltZ2* EltZ2::operator+(const EltZ2& other){
	EltZ2* result = new EltZ2(*this);
	return &(*result+=other);
}

/**
 *	Refinition de l'operateur - 
 *	
 *	\param other : EltZ qui va être multiplié 
 *
 *	\author Nicolas Simon
 *	\date 09/11/11
 */
EltZ2* EltZ2::operator-(const EltZ2& other){
	EltZ2* result = new EltZ2(*this);
	return &(*result-=other);
}

/** 
 *	Méthode de comparaison
 *	Comparaison sur les 2 coeff
 * 
 *	\param other : EltZ a comparé avec this
 *
 *	\author Nicolas Simon
 *	\date 09/11/11
 */
bool EltZ2::isEqual(const EltZ2& other) const{
	return (this->m_exposant == other.m_exposant && this->m_coeff == other.m_coeff);
}

/**
 *	Méthode de comparaison des exposants
 *
 * 	\param other : EltZ a comparé avec this
 *
 * 	\author Nicolas Simon
 * 	\date 09/11/11
 */
bool EltZ2::isEqualExposant(const EltZ2& other) const{
	return (this->m_exposant == other.m_exposant);
}

/**
 *	Refinition de l'operateur == 
 *	Verification sur les deux coeff
 *	
 *	\param other : 
 *
 *	\author Nicolas Simon
 *	\date 09/11/11
 */
bool operator==(const EltZ2& eltZ1, const EltZ2& eltZ2){
	return eltZ1.isEqual(eltZ2);
}

