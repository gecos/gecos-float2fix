/**
 *  \file PseudoFonctionLinaire.cc 
 *  \author Nicolas Simon 
 *  \date 09/11/11 
 *  \class FonctionLinaire
 */

#include <utils/linearfunction/LinearFunction.hh>
#include "graph/dfg/datanode/NoeudSrc.hh"

/**
 *	Constructeur
 *	
 *	\param name : Nom représentant la fonction lineaire
 *	
 *	\author Nicolas Simon
 *	\date 09/11/11
 */
PseudoLinearFunction::PseudoLinearFunction(NoeudGFD* srcNode){
	this->m_startNode = srcNode;
}

/**
 *	Constructeur par copie
 *	
 *	\param other : Fonction lineaire copié
 *
 *	\author Nicolas Simon
 *	\date 09/11/11
 */
PseudoLinearFunction::PseudoLinearFunction(const PseudoLinearFunction& other){
	this->m_startNode = other.m_startNode;

	this->m_vecEltZ.resize((int)other.m_vecEltZ.size());
	for(int i = 0 ; i < (int) other.m_vecEltZ.size() ; i++){
		if(other.m_vecEltZ[i] != NULL)
			this->m_vecEltZ[i] = new EltZ2(*other.m_vecEltZ[i]);
	}
}


PseudoLinearFunction::PseudoLinearFunction(NoeudGFD* srcNode, EltZ2* elt){
	this->m_startNode = srcNode;

	this->m_vecEltZ.resize(elt->getExposant()+1);
	this->m_vecEltZ[elt->getExposant()] = elt;
}

/**
 *	Destructeur
 *
 * 	\author Nicolas Simon
 * 	\date 09/11/11
 */
PseudoLinearFunction::~PseudoLinearFunction(){
	vector<EltZ2*>::iterator it;
	it = m_vecEltZ.begin();
	for( ; it != m_vecEltZ.end() ; it++)
		delete (*it);
}

/**
 *	Méthode d'affichage de la fonction Lineaire
 *	
 *	\author Nicolas Simon
 *	\date 09/11/11
 */
void PseudoLinearFunction::print(){
	cout << "Nom : " << this->m_startNode->getName()<< " : " ;
	
	/*for(int i = 0 ; i < (int) this->m_vecEltZ.size() ; i++){
		if(this->m_vecEltZ[i] != NULL){
			cout<< " "; 
			this->m_vecEltZ[i]->printCoeff();
		}
	}
	cout<<endl;*/
	for(int i = 0 ; i < (int) this->m_vecEltZ.size() ; i++){
		if(this->m_vecEltZ[i] != NULL){
			cout<< " "; 
			this->m_vecEltZ[i]->printCoeffStr();
		}
	}
	cout<<endl;
}

string PseudoLinearFunction::toString(){
    string result;
    if(this->m_startNode != NULL)
        result = "Nom : " + this->m_startNode->getName() + " : " ;
    else
        result = "";
	for(int i = 0 ; i < (int) this->m_vecEltZ.size() ; i++){
		if(this->m_vecEltZ[i] != NULL){
            result += this->m_vecEltZ[i]->toString();
		}
	}
	return result;
}

string PseudoLinearFunction::getNumerPourMatlab(){
	string result ="[ ";

	for(int i = 0 ; i < (int) this->m_vecEltZ.size() ; i++){
		if(	this->m_vecEltZ[i] == NULL){
			result += "0 ";
		}
		else{
			result += this->m_vecEltZ[i]->getCoeffStr();
			result += " ";
		}
	}
	result +="]";

	return result;
}

string PseudoLinearFunction::getDenomPourMatlab(){
	string result = "[ ";

	if((int) this->m_vecEltZ.size() == 0){
		result = "1 ]";
	}
	else {
		for(int i = 0 ; i < (int) this->m_vecEltZ.size() ; i++){
			if(this->m_vecEltZ[i] == NULL){
				result += "1 ";
			}
			else {
				result += this->m_vecEltZ[i]->getCoeffStr();
				result += " ";
			}
		}
		result += "]";
	}

	return result;
}

/**
 *	Retourne l'EltZ de la case i
 *
 *	\param i : Indice de la case
 *	\return Rend le pointeur vers l'EltZ de la case i. NULL si la case est vide ou si i est supérieur au nombre de EltZ contenu dans la fonction
 *
 *	\author Nicolas Simon
 *	\date 09/11/
 *
 */
EltZ2* PseudoLinearFunction::getEltZ(int i){
	if(i < (int) this->m_vecEltZ.size())
		return this->m_vecEltZ[i];
	else
		return NULL;
}

/**
 *	Ajoute un EltZ. Si un EltZ ayant le même exposant est présent, il y a addition entre ces deux EltZ. Ajout EltZ si non présent 
 *	
 *	\param elt : EltZ a ajouté
 *
 *	\author Nicolas Simon
 *	\date 09/11/11
 *
 */
void PseudoLinearFunction::addEltZ(int exposant, float coeff, string coeffStr ){
	EltZ2* elt = new EltZ2(exposant, coeff, coeffStr);
	if(elt->getExposant()+1 > (int) this->m_vecEltZ.size()){
		this->m_vecEltZ.resize(elt->getExposant()+1);
		this->m_vecEltZ[elt->getExposant()] = elt;
	}
	else if(this->m_vecEltZ[elt->getExposant()] != NULL){
		(*this->m_vecEltZ[elt->getExposant()])+=(*elt);
		delete elt;
	}
	else{
		this->m_vecEltZ[elt->getExposant()] = elt;
	}
}

/**
 *	Supprime l'elt de la case i
 *
 *	\param i : Indice de la case
 *	\return Vrai si l'élement a bien été supprimé, faux sinon
 *
 * 	\author Nicolas Simon
 *	\date 09/11/11
 */
bool PseudoLinearFunction::removeEltZ(int i){
	if(i < (int) this->m_vecEltZ.size()){
		this->m_vecEltZ[i] = NULL;
		return true;
	}
	else 
		return false;
}

/**
 *	Redifinition de l'opérateur +
 *
 *	\param other : FonctionLinaire a additioné
 *	\return FonctionLinaire additioné
 *
 *	\auhtor Nicolas Simon
 *	\date 09/11/11
 *
 */
PseudoLinearFunction* PseudoLinearFunction::operator+(const PseudoLinearFunction& other){
	PseudoLinearFunction* result = new PseudoLinearFunction(*this);
	for(int i = 0 ; i < (int) other.m_vecEltZ.size() ; i++){
		if(other.m_vecEltZ[i] != NULL){
			result->addEltZ(other.m_vecEltZ[i]->getExposant(), other.m_vecEltZ[i]->getCoeff(), other.m_vecEltZ[i]->getCoeffStr());
		}
	}

	return result;
}

/**
 *	Redifinition de l'opérateur +=
 *
 *	\param other : FonctionLinaire a additioné
 *	\return FonctionLinaire additioné
 *
 *	\auhtor Nicolas Simon
 *	\date 09/11/11
 *
 */
PseudoLinearFunction* PseudoLinearFunction::operator+=(const PseudoLinearFunction& other){
	for(int i = 0 ; i < (int) other.m_vecEltZ.size() ; i++){
		if(other.m_vecEltZ[i] != NULL){
			this->addEltZ(other.m_vecEltZ[i]->getExposant(), other.m_vecEltZ[i]->getCoeff(), other.m_vecEltZ[i]->getCoeffStr());
		}
	}

	return this;
}
/**
 *	Redifinition de l'opérateur - 
 *
 *	\param other : FonctionLinaire a soustraire
 *	\return FonctionLinaire Soustrait 
 *
 *	\auhtor Nicolas Simon
 *	\date 09/11/11
 *
 */
PseudoLinearFunction* PseudoLinearFunction::operator-(const PseudoLinearFunction& other){
	PseudoLinearFunction* result = new PseudoLinearFunction(*this);
	string tempStr;
	for(int i = 0 ; i < (int) other.m_vecEltZ.size() ; i++){
		if(other.m_vecEltZ[i] != NULL){
			tempStr = "-(" + other.m_vecEltZ[i]->getCoeffStr() + ")";
			result->addEltZ((other.m_vecEltZ[i]->getExposant()), -(other.m_vecEltZ[i]->getCoeff()), tempStr);
		}
	}

	return result;
}

/**
 *	Redifinition de l'opérateur -= 
 *
 *	\param other : FonctionLinaire a soustraire
 *	\return FonctionLinaire Soustrait 
 *
 *	\auhtor Nicolas Simon
 *	\date 09/11/11
 *
 */
PseudoLinearFunction* PseudoLinearFunction::operator-=(const PseudoLinearFunction& other){
	string tempStr;
	for(int i = 0 ; i < (int) other.m_vecEltZ.size() ; i++){
		if(other.m_vecEltZ[i] != NULL){
			tempStr = "-(" + other.m_vecEltZ[i]->getCoeffStr() + ")";
			this->addEltZ((other.m_vecEltZ[i]->getExposant()), -(other.m_vecEltZ[i]->getCoeff()), tempStr);
		}
	}

	return this;
}

/**
 *	Redifinition de l'opérateur * 
 *
 *	\param other : FonctionLinaire a multiplié
 *	\return FonctionLinaire multiplié
 *
 *	\auhtor Nicolas Simon
 *	\date 09/11/11
 *
 */
PseudoLinearFunction* PseudoLinearFunction::operator*(const PseudoLinearFunction& other){
	PseudoLinearFunction* result = new PseudoLinearFunction(this->m_startNode);
	EltZ2* temp;
	
	for(int i = 0 ; i < (int) this->m_vecEltZ.size() ; i++){
		for(int j = 0 ; j < (int) other.m_vecEltZ.size() ; j ++){
			if(this->m_vecEltZ[i] != NULL && other.m_vecEltZ[j] != NULL){
				temp = *this->m_vecEltZ[i] * *other.m_vecEltZ[j];
				result->addEltZ(temp->getExposant(), temp->getCoeff(), temp->getCoeffStr());
				delete temp;
			}
		}
	}
	return result;
}

/**
 *	Redifinition de l'opérateur *= 
 *
 *	\param other : FonctionLinaire a multiplié
 *	\return FonctionLinaire multiplié
 *
 *	\auhtor Nicolas Simon
 *	\date 09/11/11
 *
 */
PseudoLinearFunction* PseudoLinearFunction::operator*=(const PseudoLinearFunction& other){
	EltZ2* temp;
	PseudoLinearFunction* tempFct = new PseudoLinearFunction(*this);
	
	for(int i = 0 ; i < (int) tempFct->m_vecEltZ.size() ; i++){
		this->removeEltZ(i);
		for(int j = 0 ; j < (int) other.m_vecEltZ.size() ; j ++){
			if(tempFct->m_vecEltZ[i] != NULL && other.m_vecEltZ[j] != NULL){
				temp = *tempFct->m_vecEltZ[i] * *other.m_vecEltZ[j];
				this->addEltZ(temp->getExposant(), temp->getCoeff(), temp->getCoeffStr());
				delete temp;
			}
		}
	}
	return this;
}

/**
 *	Redifinition de l'opérateur / 
 *
 *	\param other : FonctionLinaire diviseur 
 *	\return FonctionLinaire divisé
 *
 *	\auhtor Nicolas Simon
 *	\date 02/07/12
 */
PseudoLinearFunction* PseudoLinearFunction::operator/(const PseudoLinearFunction& other){
	EltZ2* temp;
	PseudoLinearFunction* result = new PseudoLinearFunction(this->m_startNode);

	if((int) other.m_vecEltZ.size() != 1 && other.m_vecEltZ[0] == NULL){
		trace_error("Le dénominateur de la division n'est pas une constante");
	}

	for (int i = 0; i < (int) this->m_vecEltZ.size() ; i++) {
		if(this->m_vecEltZ[i] != NULL){
			temp = *this->m_vecEltZ[i] / *other.m_vecEltZ[0];
			result->addEltZ(temp->getExposant(), temp->getCoeff(), temp->getCoeffStr());
			delete temp;
		}
	}

	return result;
}

PseudoLinearFunction* PseudoLinearFunction::operator-(){
	PseudoLinearFunction* result = new PseudoLinearFunction(this->m_startNode);
	EltZ2* temp;
	
	for(int i = 0 ; i < (int) this->m_vecEltZ.size() ; i++){
        temp = -(*this->m_vecEltZ[i]);
        result->addEltZ(temp->getExposant(), temp->getCoeff(), temp->getCoeffStr());
        delete temp;
    }
	return result;
}
/**
 *	Definition de l'operateur delay. Incrémente l'exposant de la PseudoFonctionLineaire 
 *
 * 	\author Nicolas Simon
 * 	\date 09/11/11
 *
 */
void PseudoLinearFunction::operatorDelay(){
	for(int i = 0 ; i < (int) this->m_vecEltZ.size() ; i++){
		if(this->m_vecEltZ[i] != NULL){
			this->getEltZ(i)->setExposant(this->getEltZ(i)->getExposant()+1);
		}
	}
}


double PseudoLinearFunction::computeK(){
	double result = 0;

	for(int i = 0 ; i < (int) this->m_vecEltZ.size() ; i++){
		if(this->m_vecEltZ[i] != NULL){
			result += this->m_vecEltZ[i]->getCoeff() * this->m_vecEltZ[i]->getCoeff();
		}
	}

	return result;
}

double PseudoLinearFunction::computeL(){
	double result = 0;

	for( int i = 0 ; i < (int) this->m_vecEltZ.size() ; i++){
		if(this->m_vecEltZ[i] != NULL){
			result += this->m_vecEltZ[i]->getCoeff();
		}
	}
	
	return result;
}
