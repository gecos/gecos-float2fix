/**
 *  \file LMKComputer.cc
 *  \author nicolas
 *  \date  03/11/11 
 */

#include <limits>
#include <pthread.h>
#include <stdio.h>

#include "graph/tfg/GFT.hh"
#include "graph/tfg/NodeGFT.hh"


/**
 *	Appelle les fonctions calculant et écrivant dans un fichier binaire les matrices KLM
 *	
 *	\author Nicolas Simon
 *	\date 28/11/11
 *
 */
void Gft::computeKLM(){
    string tempPathFichierGain = ProgParameters::getOutputGenDirectory() + NOISE_KLM_FILEMANE;
    FILE* pfile;
    pfile = fopen(tempPathFichierGain.c_str(), "wb");
    //	ofstream fichierGain(tempPathFichierGain.data(), ofstream::binary);
    map< NoeudGFT* , list<NoeudGFT*> > mapInterdependance;

    trace_debug("Nombre Noeud Br : %i\n", this->getNbInput());
    trace_debug("Nombre Sortie : %i\n", this->getNbOutput());

    // Calcul et écriture de la matrice K
    this->computeK(pfile);

    // Choix du type d'écriture de la matrice L
    RuntimeParameters::setIsBinaryWriteWithoutZero(this->isComputeLWithoutZero()); // Sauvegarde utile pour determiner quel type de lecture doit être utilisé dans le computePb
    if(RuntimeParameters::isBinaryWriteWithoutZero()){
        trace_debug("Compute sans zero \n");
        mapInterdependance = this->determinationInterdependanceOutputInput();
        this->computeLWithoutZero(pfile, mapInterdependance);
    }
    else{
        trace_debug("Compute avec zero \n");
        this->computeL(pfile);
    }
    //	fichierGain.close();
    fclose(pfile);
}


/**
 *	Fonction calculant et écrivant dans un fichier binaire la matrice K
 *
 *  \param file : fichier binaire cible
 *	\author Nicolas Simon
 *	\date 28/11/11
 */
void Gft::computeK(FILE* file){
    NoeudGFT* nInput;
    PseudoLinearFunction* num;
    EdgeGFT* tempEdge;
    double zero = 0;
    double temp;

    for(int i = 0 ; i < this->getNbOutput() ; i++){
        for(int j = 0 ; j < this->getNbInput(); j++){
            nInput = dynamic_cast<NoeudGFT*> (this->getInputGf(j));	
            assert(nInput != NULL);
            tempEdge = dynamic_cast<EdgeGFT*> (nInput->getEdgeSucc(this->getOutputGf(i)));
            if(tempEdge != NULL){
                num = tempEdge->getFT()->getNumerateur();
                temp = num->computeK();
                fwrite(&temp, sizeof(double), 1, file);
            }
            else{
                fwrite(&zero, sizeof(double), 1, file);
            }
        }
    }
}


/**
 *	Fonction calculant et écrivant dans un fichier binaire la matrice K
 *
 *  \param file : fichier binaire cible
 *	\author Nicolas Simon
 *	\date 28/11/11
 */
void Gft::computeL(FILE* file){
    double result;
    double temp;

    EdgeGFT *edgePred1, *edgePred2;

    for( int i = 0 ; i < this->getNbOutput() ; i++){
        for ( int j = 0 ; j < this->getNbInput() ; j++){
            edgePred1 = dynamic_cast<EdgeGFT*> (this->getInputGf(j)->getEdgeSucc(this->getOutputGf(i)));
            if(edgePred1 != NULL)
                result = edgePred1->getFT()->getNumerateur()->computeL();
            else 
                result = 0;

            for( int k = j ; k < this->getNbInput() ; k++){
                temp = 0;
                if(result != 0){
                    edgePred2 = dynamic_cast<EdgeGFT*> (this->getInputGf(k)->getEdgeSucc(this->getOutputGf(i)));
                    if(edgePred2 != NULL){
                        temp = result*edgePred2->getFT()->getNumerateur()->computeL();
                    }
                }
                fwrite(&temp, sizeof(double), 1, file);
            }
        }
    }
}
/**
 *	Fonction calculant et écrivant dans un fichier binaire la matrice L sans écrire les 0
 *
 *	\param file : fichier binaire cible
 *	\param mapInterdependance : map contenant la relation entre chaque entrée et les sorties avec qui elles sont connectés
 *	\author Nicolas Simon
 *	\date 28/11/11
 *
 */
void Gft::computeLWithoutZero(FILE* file, map<NoeudGFT*, list<NoeudGFT*> > mapInterdependance){
    double temp;
    unsigned short int nombreBrDependant;
    unsigned short int numNode;
    NoeudGFT* nodeOutput;
    list<NoeudGFT*>::iterator itList, itElem1, itElem2;

    EdgeGFT *edgePred1, *edgePred2;
    /*for( int i = 0 ; i < this->getNbOutput() ; i++){
        nodeOutput = (NoeudGFT*) this->getOutputGf(i);
        nombreBrDependant = (unsigned short int) mapInterdependance[nodeOutput].size();
        fwrite(&nombreBrDependant, sizeof(unsigned short int), 1, file);
        for(itList = mapInterdependance[nodeOutput].begin() ; itList != mapInterdependance[nodeOutput].end() ; itList++){
            assert(((*itList)->getNumero() - 2) < pow(2,8*sizeof(unsigned short int)));
            numNode = (*itList)->getNumero() - 2; //Int => unsigned short int (Débordement possible si trop de source de bruit le nombre ne sera normalement jamais atteint)
            assert(numNode >= 0); // Le numero du premier non a part INIT et END commence a 2
            fwrite(&numNode, sizeof(unsigned short int), 1, file);
        }

        for ( int j = 0 ; j < this->getNbInput() ; j++){
            edgePred1 = dynamic_cast<EdgeGFT*> (this->getInputGf(j)->getEdgeSucc(this->getOutputGf(i)));
            if(edgePred1 != NULL){
                result = edgePred1->getFT()->getNumerateur()->computeL();
                for( int k = j ; k < this->getNbInput() ; k++){
                    edgePred2 = dynamic_cast<EdgeGFT*> (this->getInputGf(k)->getEdgeSucc(this->getOutputGf(i)));
                    if(edgePred2 != NULL){
                        temp = result*edgePred2->getFT()->getNumerateur()->computeL();
                        fwrite(&temp, sizeof(float), 1, file);
                    }
                }
            }
        }
    }*/

    //  for(itMap = mapInterdependance.begin() ; itMap != mapInterdependance.end() ; itMap++){
    for( int i = 0 ; i < this->getNbOutput() ; i++){
        nodeOutput = (NoeudGFT*) this->getOutputGf(i);
//      nodeOutput = itMap->first;
        nombreBrDependant = (unsigned short int) mapInterdependance[nodeOutput].size();
        fwrite(&nombreBrDependant, sizeof(unsigned short int), 1, file);
        for(itList = mapInterdependance[nodeOutput].begin() ; itList != mapInterdependance[nodeOutput].end() ; itList++){
            assert(((*itList)->getNumero() - 2) < pow(2,8*sizeof(unsigned short int)));
            numNode = (*itList)->getNumero() - 2; //Int => unsigned short int (Débordement possible si trop de source de bruit le nombre ne sera normalement jamais atteint)
            assert(numNode >= 0); // Le numero du premier non a part INIT et END commence a 2
            fwrite(&numNode, sizeof(unsigned short int), 1, file);
        }
        
        for(itElem1 = mapInterdependance[nodeOutput].begin() ; itElem1 != mapInterdependance[nodeOutput].end() ; itElem1++){
            edgePred1 = dynamic_cast<EdgeGFT*> ((*itElem1)->getEdgeSucc(nodeOutput));
            assert(edgePred1 != NULL);
            for(itElem2 = itElem1 ; itElem2 != mapInterdependance[nodeOutput].end() ; itElem2++){
                assert(edgePred2 != NULL);
                edgePred2 = dynamic_cast<EdgeGFT*> ((*itElem2)->getEdgeSucc(nodeOutput));
                temp = edgePred1->getFT()->getNumerateur()->computeL() * edgePred2->getFT()->getNumerateur()->computeL();
                fwrite(&temp, sizeof(double), 1, file);
            }
        }
    }


}

/**
 *	Appelle les fonctions calculant et écrivant dans un fichier binaire les matrices KLM
 *	
 *	\author Nicolas Simon
 *	\date 28/11/11
 *
 */
void Gft::computeKLMTest(){
    string tempPathFichierGain = ProgParameters::getOutputGenDirectory() + "KLM_Test.bin";
    FILE* pfile;
    pfile = fopen(tempPathFichierGain.c_str(), "wb");
    ofstream fichierGain(tempPathFichierGain.data(), ofstream::binary);
    map< NoeudGFT* , list<NoeudGFT*> > mapInterdependance;

    trace_debug("Nombre Noeud Br : %i\n", this->getNbInput());
    trace_debug("Nombre Sortie : %i\n", this->getNbOutput());

    this->computeK(pfile);
    this->computeL(pfile);
    mapInterdependance = this->determinationInterdependanceOutputInput();
    this->computeLWithoutZero(pfile, mapInterdependance);
    //	fichierGain.close();
    fclose(pfile);

    //Fonction permettant de tester le binaire écrit ( a utiliser lorsque le nombre de br est faible)
    this->readKLMBinariesFile();

}
/**
 *	Fonction permettant la lecture du fichier binaire généré et de l'écrire dans un fichier. Genere aussi un comparatif a partir d'un nouveau calcul des matrices
 *
 *	\author Nicolas Simon
 *	\date 28/11/11
 */
void Gft::readKLMBinariesFile(){
    string tempPathFichierGain = ProgParameters::getOutputGenDirectory() + "CompareWithBin.m";
    string tempPathFichierGainBin = ProgParameters::getOutputGenDirectory() + "KLM_Test.bin";
    string tempPathFichierResult = ProgParameters::getOutputGenDirectory() + "ReadFromBin.m";

	fstream fichierGain;
	fichierGain.open(tempPathFichierGain.data(), fstream::out);
	assert(fichierGain.is_open());
	ofstream fichierGainResult(tempPathFichierResult.data());
	ifstream fichierGainBin(tempPathFichierGainBin.data(), ifstream::binary);

	double temp = 0;
	unsigned short int nombreBrDependant, numNode;
	//unsigned short int tempIndiceJ;
	//unsigned short int tempIndiceK;

	this->computeKCompareBin(fichierGain);
	fichierGain<<"\n\n";
	this->computeLCompareBin(fichierGain);
	fichierGainResult.precision(30);
	// Lecture de la matrice K dans le fichier binaire
	for(int i = 0 ; i<this->getNbOutput() ; i++){
		for(int j = 0 ; j<this->getNbInput() ; j++){
			fichierGainBin.read((char *)&temp, sizeof(double));		
			fichierGainResult<<fixed<<temp<<" ";
		}
		fichierGainResult<<"\n";
	}

	fichierGainResult<<"\n\n";
	
	// Lecture de la matrice L obtenu via computeL dans le fichier binaire
	for(int i = 0 ; i<this->getNbOutput() ; i++){
		for(int j = 0 ; j<this->getNbInput() ; j++){
			for(int k = j ; k<this->getNbInput() ; k++){	   
				fichierGainBin.read((char *)&temp, sizeof(double));
				fichierGainResult<<fixed<<temp<<" ";
			}
			fichierGainResult<<"\n";
		}
	}

	// Lecture de la matrice L obtenu via computeLWithoutZero dans le fichier binaire
	for(int i = 0 ; i<this->getNbOutput() ; i++){
		fichierGainBin.read((char *)&nombreBrDependant, sizeof(unsigned short int));
		fichierGainResult<<nombreBrDependant<<" ";
		for(int j = 0 ; j<nombreBrDependant ; j++){
			fichierGainBin.read((char *)&numNode, sizeof(unsigned short int));
			fichierGainResult<<numNode<<" ";
		}
		for(int j = 0 ; j<(nombreBrDependant*(nombreBrDependant+1))/2; j++){
			fichierGainBin.read((char *)&temp, sizeof(double));
			fichierGainResult<<fixed<<temp<<" ";
		}
		fichierGainResult<<endl;
	}

	fichierGainResult.close();
	fichierGainBin.close();
	fichierGain.close();
}

/**
 *	Generation dans le fichier file du la matrice K servant au comparatif
 *
 *	\param file : fichier cible
 *	\author Nicolas Simon
 *	\date 28/11/11
 */
void Gft::computeKCompareBin(fstream& file){
	NoeudGFT* nInput;
	PseudoLinearFunction* num;
	EdgeGFT* tempEdge;
	double zero = 0;
	file.precision(30);
	for(int i = 0 ; i < this->getNbOutput() ; i++){
		for(int j = 0 ; j < this->getNbInput(); j++){
			nInput = dynamic_cast<NoeudGFT*> (this->getInputGf(j));	
			assert(nInput != NULL);
			tempEdge = dynamic_cast<EdgeGFT*> (nInput->getEdgeSucc(this->getOutputGf(i)));
			if(tempEdge != NULL){
				num = tempEdge->getFT()->getNumerateur();
				file<<fixed<<num->computeK()<<" ";
			}
			else{
				file<<fixed<<zero<<" ";
			}
		}
		file<<"\n";
	}
}

/**
 *	Generation dans le fichier file du la matrice L servant au comparatif
 *
 *	\param file : fichier cible
 *	\author Nicolas Simon
 *	\date 28/11/11
 */
void Gft::computeLCompareBin(fstream& file){
	double zero = 0;
	double result;

	EdgeGFT *edgePred1, *edgePred2;

	for( int i = 0 ; i < this->getNbOutput() ; i++){
		for ( int j = 0 ; j < this->getNbInput() ; j++){
			edgePred1 = dynamic_cast<EdgeGFT*> (this->getInputGf(j)->getEdgeSucc(this->getOutputGf(i)));
			if(edgePred1 != NULL)
				result = edgePred1->getFT()->getNumerateur()->computeL();
			else 
				result = 0;

			for( int k = j ; k < this->getNbInput() ; k++){
				edgePred2 = dynamic_cast<EdgeGFT*> (this->getInputGf(k)->getEdgeSucc(this->getOutputGf(i)));
				if(edgePred1 != NULL && edgePred2 != NULL){
					file<<fixed<<result*edgePred2->getFT()->getNumerateur()->computeL()<<" ";
				}
				else{
					file<<fixed<<zero<<" ";
				}
			}
			file<<"\n";
		}

	}
}

/**
 *	Determine quelles entrées du GFT sont dépendantes avec tel ou tel sorties
 *
 * 	\return map avec comme clé les sorties et comme valeur une liste des noeuds Br connecté en fonction des sorties
 *	\author Nicolas Simon
 *	\date 02/12/11
 *
 */
map<NoeudGFT*, list<NoeudGFT*> > Gft::determinationInterdependanceOutputInput(){
	map< NoeudGFT*, list<NoeudGFT*> > mapInterdependance;
	NoeudGFT *nodeBr, *nodeOutput;

	for(int i = 0 ; i<this->getNbInput() ; i++){
		nodeBr = (NoeudGFT*) (this->getInputGf(i));
		for(int j = 0 ; j < nodeBr->getNbSucc() ; j++){
			nodeOutput = (NoeudGFT*) nodeBr->getElementSucc(j);
			mapInterdependance[nodeOutput].push_back(nodeBr);
		}
	}

	// Affichage pour verification
	/*map< NoeudGFT*, list<NoeudGFT*> >::iterator itMap;
	  list<NoeudGFT*>::iterator itList;
	  for(itMap = mapInterdependance.begin() ; itMap != mapInterdependance.end() ; itMap++){
	  cout<<(*itMap).first->getNumero()<<" : ";
	  for(itList = (*itMap).second.begin() ; itList != (*itMap).second.end() ; itList++){
	  cout<<(*itList)->getNumero()<<" ";
	  }
	  cout<<endl;
	  }*/

	return mapInterdependance;
}

/**
 *	Permet de determiner si l'écriture du binaire avec les zéros prend moins de place mémoire que l'écriture sans les zéros
 *	
 *	\date 05/12/11
 *	\author Nicolas Simon
 *
 *
 */
bool Gft::isComputeLWithoutZero(){
	double sizeWithZero;
	double sizeWithoutZero = 0;
	NoeudGFT* nodeOutput;

	sizeWithZero = ((this->getNbInput() * (this->getNbInput() + 1))/2)*this->getNbOutput()*sizeof(double);

	for(int i = 0 ; i < this->getNbOutput() ; i++){
		nodeOutput = (NoeudGFT*) this->getOutputGf(i);
		sizeWithoutZero += ((nodeOutput->getNbPred() * (nodeOutput->getNbPred() + 1 ))/2)*sizeof(double) + (nodeOutput->getNbPred() + 1)*sizeof(unsigned short int);
	}
	return sizeWithoutZero < sizeWithZero;
}


/**
 *	Appelle les fonctions calculant et écrivant dans un fichier binaire les matrices KLM
 *	
 *	\author Nicolas Simon
 *	\date 28/11/11
 *
 */
/*void Gft::computeKLMMultiThreading(){
	string tempPathFichierGain = ProgParameters::getOutputGenDirectory() + VALEUR_HG_DB_BIN;
	ofstream fichierGain(tempPathFichierGain.data(), ofstream::binary);
	map< NoeudGFT* , list<NoeudGFT*> > mapInterdependance;
	float **vectorK;
   	vectorK	= (float**)malloc(sizeof(*vectorK)*this->getNbOutput());
	for(int i = 0 ; i<this->getNbOutput() ; i++){
		vectorK[i] = (float*)malloc(sizeof(**vectorK)*this->getNbInput());
	}
	
	trace_debug("test\n");
	trace_debug("Nombre Noeud Br : %i\n", this->getNbInput());
	trace_debug("Nombre Sortie : %i\n", this->getNbOutput());


	// Calcul et écriture de la matrice K
	this->computeKMultiThreading(vectorK);
	for(int i = 0 ; i < this->getNbOutput() ; i++){
		for(int j = 0 ; j < this->getNbInput() ; j++){
			fichierGain.write((char *) &vectorK[i][j], sizeof(float));
		}
	}
	
	for(int i = 0 ; i < this->getNbOutput() ; i++){
		free(vectorK[i]);
	}	
	free(vectorK);
	

	float ***vectorL;
	vectorL = (float***)malloc(sizeof(*vectorL)*this->getNbOutput());
	for(int i = 0 ; i < this->getNbOutput() ; i++){
		vectorL[i] = (float**)malloc(sizeof(**vectorL)*this->getNbInput());
		for(int j = 0 ; j < this->getNbInput() ; j++){
			vectorL[i][j] = (float*)malloc(sizeof(***vectorL)*(this->getNbInput()-j));
		}
	}

	this->computeLMultiThreading(vectorL);
	for( int i = 0 ; i < this->getNbOutput() ; i++){
		for( int j = 0 ; j < this->getNbInput() ; j++){
			for(int k = j ;  k < this->getNbInput() ; k++){
				fichierGain.write((char *) &vectorL[i][j][k], sizeof(float));
			}
		}
	}


	for(int i = 0 ; i < this->getNbOutput() ; i++){
		for(int j = 0 ; j < this->getNbInput() ; j++){
			free(vectorL[i][j]);
		}
		free(vectorL[i]);
	}
	free(vectorL);

	fichierGain.close();	
}*/

/**
 *	Fonction calculant et écrivant dans un fichier binaire la matrice K
 *
 *  \param file : fichier binaire cible
 *	\author Nicolas Simon
 *	\date 28/11/11
 */
/*void Gft::computeKMultiThreading(float **vectorK){
	NoeudGFT* nInput;
	PseudoFonctionLineaire* num;
	EdgeGFT* tempEdge;
	float zero = 0;
	int i;
	int j;

#pragma omp parallel for private(i, j, num, tempEdge) shared(vectorK) 
	for(i = 0 ; i < this->getNbOutput(); i++){
		for(j = 0 ; j < this->getNbInput(); j++){
			nInput = dynamic_cast<NoeudGFT*> (this->getInputGf(j));	
			assert(nInput != NULL);
			tempEdge = dynamic_cast<EdgeGFT*> (nInput->getEdgeSucc(this->getOutputGf(i)));
			if(tempEdge != NULL){
				num = tempEdge->getFT()->getNumerateur();
				vectorK[i][j] = num->computeK();
			}
			else{
				vectorK[i][j] = zero;
			}
		}
	}

}*/


/**
 *	Fonction calculant et écrivant dans un fichier binaire la matrice K
 *
 *  \param file : fichier binaire cible
 *	\author Nicolas Simon
 *	\date 28/11/11
 */
/*void Gft::computeLMultiThreading(float*** vectorL){
	float zero = 0;
	float result;
	float temp;
	int i; 
	int j;
	int k;

	EdgeGFT *edgePred1, *edgePred2;
#pragma omp parallel for private(i, j, k, edgePred1, edgePred2, result, temp) shared(vectorL)
	for( i = 0 ; i < this->getNbOutput() ; i++){
		for ( j = 0 ; j < this->getNbInput() ; j++){
			edgePred1 = dynamic_cast<EdgeGFT*> (this->getInputGf(j)->getEdgeSucc(this->getOutputGf(i)));
			if(edgePred1 != NULL)
				result = edgePred1->getFT()->getNumerateur()->computeL();
			else 
				result = 0;

			for( k = j ; k < this->getNbInput() ; k++){
				edgePred2 = dynamic_cast<EdgeGFT*> (this->getInputGf(k)->getEdgeSucc(this->getOutputGf(i)));
				if(edgePred1 != NULL && edgePred2 != NULL){
					temp = result*edgePred2->getFT()->getNumerateur()->computeL();
					vectorL[i][j][k] = temp;
				}
				else{
					vectorL[i][j][k] = zero;
				}
			}
		}
	}
}*/
/**
 *	Fonction calculant et écrivant dans un fichier binaire la matrice L sans écrire les 0
 *
 *	\param file : fichier binaire cible
 *	\param mapInterdependance : map contenant la relation entre chaque entrée et les sorties avec qui elles sont connectés
 *	\author Nicolas Simon
 *	\date 28/11/11
 *
 */
/*void Gft::computeLWithoutZeroMultiThreading(float*** vectorL, map<NoeudGFT*, list<NoeudGFT*> > mapInterdependance){
	float result;
	float temp;
	unsigned short int nombreBrDependant;
	unsigned short int numNode;
	NoeudGFT* nodeOutput;
	list<NoeudGFT*>::iterator itList;

	EdgeGFT *edgePred1, *edgePred2;

	for( int i = 0 ; i < this->getNbOutput() ; i++){
		nodeOutput = (NoeudGFT*) this->getOutputGf(i);
		nombreBrDependant = (unsigned short int) mapInterdependance[nodeOutput].size();
		file.write((char *)&nombreBrDependant, sizeof(unsigned short int));
		for(itList = mapInterdependance[nodeOutput].begin() ; itList != mapInterdependance[nodeOutput].end() ; itList++){
			numNode = (*itList)->getNumero() - 2; //Int => unsigned short int (Débordement possible si trop de source de bruit le nombre ne sera normalement jamais atteint)
			assert(numNode >= 0); // Le numero du premier non a part INIT et END commence a 2
			file.write((char *)&numNode, sizeof(unsigned short int));
		}


		for ( int j = 0 ; j < this->getNbInput() ; j++){
			edgePred1 = dynamic_cast<EdgeGFT*> (this->getInputGf(j)->getEdgeSucc(this->getOutputGf(i)));
			if(edgePred1 != NULL){
				result = edgePred1->getFT()->getNumerateur()->computeL();
			}
			else{
				result = 0;
			}


			for( int k = j ; k < this->getNbInput() ; k++){
				edgePred2 = dynamic_cast<EdgeGFT*> (this->getInputGf(k)->getEdgeSucc(this->getOutputGf(i)));
				if(edgePred1 != NULL && edgePred2 != NULL){
					temp = result*edgePred2->getFT()->getNumerateur()->computeL();
					file.write((char *) &temp, sizeof(float));
				}
			}
		}
	}
}
*/

