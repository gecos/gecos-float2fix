#if 1

/**
 *  \author Nicolas Simon
 *  \date   09/11/11
 *  \version 1.0
 *  \class FonctionLinaiere
 *	\breif Classe correspondant representant chaque element Z d'une fonction linéaire (exposant, coefficient)
 */

#ifndef FONCTION_LINEAIRE_NON_MATLAB
#define FONCTION_LINEAIRE_NON_MATLAB

#include <cstring>
#include <iostream>
#include <set>
#include <string>
#include <sstream>
#include <vector>
#include <cassert>

#include "PseudoFonctionLineaire.hh"

class NoeudGFD;

class FonctionLineaire{
private:
	vector<PseudoFonctionLineaire*> m_vectorPseudoFctLineaire; ///< Conteneur représentant les sous parties de la fonction lineaire
	NoeudGFD* m_node;	///< Noeud sortie associé a la fonction lineaire
	bool m_isCste; ///< Vrai si la FonctionLineaire représente en faites une constante
	float m_csteValue; ///< Valeur de la constante si la FonctionLineaire en représente une

public:
	FonctionLineaire(NoeudGFD* node);
	FonctionLineaire(float value, NoeudGFD* node);
	FonctionLineaire(const FonctionLineaire& other);
	~FonctionLineaire();

	void setNode(NoeudGFD* node){
		this->m_node = node;
	} ///< Setteur de m_node
	NoeudGFD* getNode() const{
		return m_node;
	} ///< Getteur de m_node

	void addPseudoFctLineaire(PseudoFonctionLineaire* pseudoFct){
		this->m_vectorPseudoFctLineaire.push_back(pseudoFct);
	} ///< Ajoute une PseudoFonctionLineaire a la fin du conteneur

	PseudoFonctionLineaire* getPseudoFctLineaire(int i) const{
		assert(i<(int) m_vectorPseudoFctLineaire.size());
		return m_vectorPseudoFctLineaire[i];
	} ///< Renvoi la ième PseudoFonctionLineaire du conteneur 

	int getNbPseudoFctLineaire() const{
		return (int) m_vectorPseudoFctLineaire.size();
	} ///< Renvoi le nombre de PseudoFonctionLineaire de la fonction lineaire

	bool isCste() const{
		return m_isCste;
	}

	void setCsteValue(float value){
		this->m_csteValue = value;
	} ///< Setteur de m_csteValue
	float getCsteValue() const{
		return m_csteValue;
	} /// Getteur de m_csteValue

	FonctionLineaire* operator+(const FonctionLineaire& other); ///< Redefinition de l'operateur +
	FonctionLineaire* operator-(const FonctionLineaire& other); ///< Redefinition de l'operateur -
	FonctionLineaire* operator*(const FonctionLineaire& other); ///< Redefinition de l'operateur *
	FonctionLineaire* operator/(const FonctionLineaire& other); ///< Redefinition de l'operateur /
	FonctionLineaire* FTMult(float value) const; ///< Permet la multiplication d'une fonction lineaire avec une constante
	FonctionLineaire* FTDiv(float value) const; ///< Permet la multiplication d'une fonction lineaire avec une constante
	void operatorDelay(); ///< Défini l'operateur delay

	void print(); /// Defini la fonction d'affichage pour les FonctionLineaire
};

#endif
#endif
