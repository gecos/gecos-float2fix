/**
 *  \author Nicolas Simon
 *  \date   09/11/11
 *  \version 1.0
 *  \class EltZ
 *	\breif Classe correspondant representant chaque element Z d'une fonction linéaire (exposant, coefficient)
 */

#ifndef ELT_Z_NON_MATLAB
#define ELT_Z_NON_MATLAB

#include <cstring>
#include <iostream>
#include <set>
#include <string>
#include <sstream>
#include <vector>
#include <cassert>

using namespace std;

class EltZ2 {
private:
	int m_exposant; ///< Exposant du Z
	float m_coeff; ///< Valeur du coefficient du Z

public:
	EltZ2(int exposant, float coeff); ///< Constructeur
	EltZ2(const EltZ2& other); ///< Constructeur par copie
	~EltZ2(); ///< Destructeur

	void setExposant(int exposant){
		this->m_exposant = exposant;
	}///< Attribut m_exposant
	int getExposant(){
		return m_exposant;
	}///< Retourne m_exposant
	
	void setCoeff(float coeff){
		this->m_coeff = coeff;
	}///< Attribut m_coeff
	float getCoeff(){
		return m_coeff;
	}///< Retourne m_coeff

	void print(); ///< Affiche l'élement
	string toString(); ///< Rend une chaine de caractère de la forme m_coeff*Z-m_exposant
	bool isEqual(const EltZ2& other) const; ///< Méthode de comparaison
	bool isEqualExposant(const EltZ2& other) const; ///< Méthode de comparaison d'exposant

	EltZ2& operator+=(const EltZ2& other); ///< Redefinition de l'opérateur +=
	EltZ2& operator-=(const EltZ2& other); ///< Redefinition de l'opérateur -=
	EltZ2& operator*=(const EltZ2& other); ///< Redefinition de l'opérateur *=
	EltZ2& operator/=(const EltZ2& other); ///< Redefinition de l'opérateur *=
	EltZ2* operator*(const EltZ2& other); ///< Redefinition de l'opérateur *
	EltZ2* operator-(const EltZ2& other); ///< Redefinition de l'opérateur -
	EltZ2* operator+(const EltZ2& other); ///< Redefinition de l'opérateur +
	EltZ2* operator/(const EltZ2& other); ///< Redefinition de l'opérateur /


};

bool operator==(const EltZ2& eltZ1, const EltZ2& eltZ2); ///< Redefinition de l'opérateur ==

#endif


