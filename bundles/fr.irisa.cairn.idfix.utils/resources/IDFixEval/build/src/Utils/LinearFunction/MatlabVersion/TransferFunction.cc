#include <utils/linearfunction/TransferFunction.hh>

/**
 *	Constructeur par défaut
 *
 *	\date 17/11/11
 *	\author Nicolas Simon
 *
 */
TransferFunction::TransferFunction(){
	this->m_denominateur = NULL;
	this->m_numerateur = NULL;
}

/**
 *	Constructeur
 *	\param num : Numerateur de la fonction de transfert
 *	\param den : Denominateur de la fonction de transfert
 *
 *	\date 17/11/11
 *	\author Nicolas Simon
 *
 */
TransferFunction::TransferFunction(PseudoLinearFunction* num, PseudoLinearFunction* den){
	this->m_numerateur = new PseudoLinearFunction(*num);

	PseudoLinearFunction* temp = new PseudoLinearFunction(den->getStartNode());
	*temp -= *den;
	this->m_denominateur = temp;
}

/**
 *	Constructeur qui set a 1Z^0 le denominateur de la fonction de transfert
 *	\param num : Numerateur de la fonction de transfert
 *
 *	\date 17/11/11
 *	\author Nicolas Simon
 *
 */
TransferFunction::TransferFunction(PseudoLinearFunction* num){
	this->m_numerateur = new PseudoLinearFunction(*num);

	EltZ2* temp = new EltZ2(0, 1, "1");
	this->m_denominateur = new PseudoLinearFunction(NULL, temp);
}

/**
 *	Constructeur par copie
 *
 *	\date 17/11/11
 *	\author Nicolas Simon
 *
 */
TransferFunction::TransferFunction(const TransferFunction& other){
	this->m_denominateur = new PseudoLinearFunction(*other.m_denominateur);
	this->m_numerateur = new PseudoLinearFunction(*other.m_numerateur);
}

/**
 *	Destructeur
 *
 *	\date 17/11/11
 *	\author Nicolas Simon
 *
 */
TransferFunction::~TransferFunction(){
	delete m_numerateur;
	delete m_denominateur;
}

/**
 *	\return Rend la fonction de transfert en format string	
 *
 *	\date 17/11/11
 *	\author Nicolas Simon
 *
 */
string TransferFunction::toString() const{
	string result ="";
	result += "Num : ";
	result += m_numerateur->toString();
	result += "\nDen : ";
	result += m_denominateur->toString();

	return result;
}

/**
 *	\return Rend le numerateur de la fonction de transfert en format string	
 *
 *	\date 17/11/11
 *	\author Nicolas Simon
 *
 */
string TransferFunction::toStringNum() const{
	string result ="";
	result += "Num : ";
	result += m_numerateur->toString();

	return result;
}

/**
 *	\return Rend le denominateur de la fonction de transfert en format string	
 *
 *	\date 17/11/11
 *	\author Nicolas Simon
 *
 */
string TransferFunction::toStringDen() const{
	string result ="";
	result += "Den : ";
	result += m_denominateur->toString();

	return result;
}

/**
 *	\return Rend la fonction de transfert en format adéquat pour les traitements Matlab	
 *
 *	\date 17/11/11
 *	\author Nicolas Simon
 *
 */
string TransferFunction::getMatlabExpression(int numEdge) const {
	ostringstream expression;

	if(RuntimeParameters::isLTIGraph()){
		expression << "H(" << numEdge << ").Num = " << this->m_numerateur->getNumerPourMatlab() << ";\n";
		expression << "H(" << numEdge << ").Den = " << this->m_denominateur->getDenomPourMatlab() << ";\n";
		expression << "H(" << numEdge << ").tf  = tf(H(" << numEdge << ").Num, H(" << numEdge << ").Den, 1, 'variable', 'z^-1');\n\n";
	}
	else{
		expression << "H(" << numEdge << ").Num(1).g = " << this->m_numerateur->getNumerPourMatlab() << ";\n";
		expression << "H(" << numEdge << ").Den(1).f = " << this->m_denominateur->getDenomPourMatlab() << ";\n";
		expression << "H(" << numEdge << ").tf = ft(H(" << numEdge << "));\n\n";
	}
	return expression.str();
}

