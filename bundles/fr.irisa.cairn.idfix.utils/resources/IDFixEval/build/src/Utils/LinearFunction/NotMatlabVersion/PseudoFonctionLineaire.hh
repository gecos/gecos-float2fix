/**
 *  \author Nicolas Simon
 *  \date   09/11/11
 *  \version 1.0
 *  \class FonctionLinaire
 *	\breif Classe correspondant representant chaque element Z d'une fonction linéaire (exposant, coefficient)
 */

#ifndef PSEUDO_FONCTION_LINEAIRE_NON_MATLAB
#define PSEUDO_FONCTION_LINEAIRE_NON_MATLAB

#include <cstring>
#include <iostream>
#include <set>
#include <string>
#include <sstream>
#include <vector>
#include <cassert>
#include "EltZ2.hh"

class NoeudSrc;
class NoeudGFD;

class PseudoFonctionLineaire {
	//friend FonctionLineaire2* operator+(const FonctionLineaire2& elt1, const FonctionLineaire2& elt2);


private:
	vector<EltZ2*> m_vecEltZ; ///< Element Z de la fonction lineaire
	NoeudGFD* m_startNode; ///< Noeud de départ de la fonction linéaire

public:
	PseudoFonctionLineaire(NoeudGFD* srcNode); ///< Constructeur
	PseudoFonctionLineaire(NoeudGFD* srcNode, EltZ2* elt);
	PseudoFonctionLineaire(const PseudoFonctionLineaire& other); ///< Constructeur par copie
	~PseudoFonctionLineaire(); ///< Destructeur

	void setStartNode(NoeudGFD* startNode){
		this->m_startNode = startNode;
	}///< Attribue m_startNode
	NoeudGFD* getStartNode(){
		return m_startNode;
	}///< Retourne m_startNode

	int getMaxExposant(){
		return (int) m_vecEltZ.size();
	}///< Retourne la valeur de l'exposant max

	void print(); ///< Methode d'affichage de la fonction lineaire
	string toString(); 
	//string getNumerPourMatlab();
	//string getDenomPourMatlab();

	void addEltZ(int exposant, float coeff); ///< Créé et ajoute un EltZ. Additionne les EltZ2 si un est deja présent ils ont le même exposant 
	EltZ2* getEltZ(int i); ///< Rend le ieme EltZ de la fonction lineaire 
	bool removeEltZ(int i); ///< Supprime l'EltZ a la case i. Diminue la size du vecteur de 1
	PseudoFonctionLineaire* operator+(const PseudoFonctionLineaire& other); ///< Redefinition de l'opérateur +
	PseudoFonctionLineaire* operator+=(const PseudoFonctionLineaire& other); ///< Redefinition de l'opérateur +
	PseudoFonctionLineaire* operator*(const PseudoFonctionLineaire& other); ///< Redefinition de l'opérateur * 
	PseudoFonctionLineaire* operator/(const PseudoFonctionLineaire& other); ///< Redefinition de l'opérateur / 
	PseudoFonctionLineaire* operator*=(const PseudoFonctionLineaire& other); ///< Redefinition de l'opérateur -
	PseudoFonctionLineaire* operator-(const PseudoFonctionLineaire& other); ///< Redefinition de l'opérateur -
	PseudoFonctionLineaire* operator-=(const PseudoFonctionLineaire& other); ///< Redefinition de l'opérateur -
	void operatorDelay(); ///< Defini l'operateur delay

	float computeK();
	float computeL();
	

};


#endif	
