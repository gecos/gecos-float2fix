/**
 *  \file PseudoFonctionLinaire.cc 
 *  \author Nicolas Simon 
 *  \date 09/11/11 
 *  \class FonctionLinaire
 */

#include <graph/dfg/datanode/NoeudSrc.hh>

#include "FonctionLineaire.hh"

/**
 *	Constructeur
 *	
 *	\param name : Nom représentant la fonction lineaire
 *	
 *	\author Nicolas Simon
 *	\date 09/11/11
 */
PseudoFonctionLineaire::PseudoFonctionLineaire(NoeudGFD* srcNode){
	this->m_startNode = srcNode;
}

/**
 *	Constructeur par copie
 *	
 *	\param other : Fonction lineaire copié
 *
 *	\author Nicolas Simon
 *	\date 09/11/11
 */
PseudoFonctionLineaire::PseudoFonctionLineaire(const PseudoFonctionLineaire& other){
	this->m_startNode = other.m_startNode;

	this->m_vecEltZ.resize((int)other.m_vecEltZ.size());
	for(int i = 0 ; i < (int) other.m_vecEltZ.size() ; i++){
		if(other.m_vecEltZ[i] != NULL)
			this->m_vecEltZ[i] = new EltZ2(*other.m_vecEltZ[i]);
	}
}


PseudoFonctionLineaire::PseudoFonctionLineaire(NoeudGFD* srcNode, EltZ2* elt){
	this->m_startNode = srcNode;

	this->m_vecEltZ.resize(elt->getExposant()+1);
	this->m_vecEltZ[elt->getExposant()] = elt;
}

/**
 *	Destructeur
 *
 * 	\author Nicolas Simon
 * 	\date 09/11/11
 */
PseudoFonctionLineaire::~PseudoFonctionLineaire(){
	vector<EltZ2*>::iterator it;
	it = m_vecEltZ.begin();
	for( ; it != m_vecEltZ.end() ; it++)
		delete (*it);
}

/**
 *	Méthode d'affichage de la fonction Lineaire
 *	
 *	\author Nicolas Simon
 *	\date 09/11/11
 */
void PseudoFonctionLineaire::print(){
	cout << "Nom : " << this->m_startNode->getName()<< " : " ;
	
	for(int i = 0 ; i < (int) this->m_vecEltZ.size() ; i++){
		if(this->m_vecEltZ[i] != NULL){
			this->m_vecEltZ[i]->print();
			if(i<(int) this->m_vecEltZ.size()-1)
				cout<<" + ";
		}
	}
}

string PseudoFonctionLineaire::toString(){
	string result = "";
	for(int i = 0 ; i < (int) this->m_vecEltZ.size() ; i++){
		if(this->m_vecEltZ[i] != NULL){
			result += this->m_vecEltZ[i]->toString();
			if(i+1 != (int) this->m_vecEltZ.size())
				result += " + ";
		}
	}
	return result;


}

/*string PseudoFonctionLineaire::getNumerPourMatlab(){
	string result ="[ ";

	for(int i = 0 ; i < (int) this->m_vecEltZ.size() ; i++){
		if(	this->m_vecEltZ[i] == NULL){
			result += "0 ";
		}
		else{
			result += this->m_vecEltZ[i]->getCoeffStr();
			result += " ";
		}
	}
	result +="]";

	return result;
}

string PseudoFonctionLineaire::getDenomPourMatlab(){
	string result = "[ ";

	if((int) this->m_vecEltZ.size() == 0){
		result = "1 ]";
	}
	else {
		for(int i = 0 ; i < (int) this->m_vecEltZ.size() ; i++){
			if(this->m_vecEltZ[i] == NULL){
				result += "1 ";
			}
			else {
				result += this->m_vecEltZ[i]->getCoeffStr();
				result += " ";
			}
		}
		result += "]";
	}

	return result;
}*/

/**
 *	Retourne l'EltZ de la case i
 *
 *	\param i : Indice de la case
 *	\return Rend le pointeur vers l'EltZ de la case i. NULL si la case est vide ou si i est supérieur au nombre de EltZ contenu dans la fonction
 *
 *	\author Nicolas Simon
 *	\date 09/11/
 *
 */
EltZ2* PseudoFonctionLineaire::getEltZ(int i){
	if(i < (int) this->m_vecEltZ.size())
		return this->m_vecEltZ[i];
	else
		return NULL;
}

/**
 *	Ajoute un EltZ. Si un EltZ ayant le même exposant est présent, il y a addition entre ces deux EltZ. Ajout EltZ si non présent 
 *	
 *	\param elt : EltZ a ajouté
 *
 *	\author Nicolas Simon
 *	\date 09/11/11
 *
 */
void PseudoFonctionLineaire::addEltZ(int exposant, float coeff){
	EltZ2* elt = new EltZ2(exposant, coeff);
	if(elt->getExposant()+1 > (int) this->m_vecEltZ.size()){
		this->m_vecEltZ.resize(elt->getExposant()+1);
		this->m_vecEltZ[elt->getExposant()] = elt;
	}
	else if(this->m_vecEltZ[elt->getExposant()] != NULL){
		(*this->m_vecEltZ[elt->getExposant()])+=(*elt);
		delete elt;
	}
	else{
		this->m_vecEltZ[elt->getExposant()] = elt;
	}
}

/**
 *	Supprime l'elt de la case i
 *
 *	\param i : Indice de la case
 *	\return Vrai si l'élement a bien été supprimé, faux sinon
 *
 * 	\author Nicolas Simon
 *	\date 09/11/11
 */
bool PseudoFonctionLineaire::removeEltZ(int i){
	if(i < (int) this->m_vecEltZ.size()){
		this->m_vecEltZ[i] = NULL;
		return true;
	}
	else 
		return false;
}

/**
 *	Redifinition de l'opérateur +
 *
 *	\param other : FonctionLinaire a additioné
 *	\return FonctionLinaire additioné
 *
 *	\auhtor Nicolas Simon
 *	\date 09/11/11
 *
 */
PseudoFonctionLineaire* PseudoFonctionLineaire::operator+(const PseudoFonctionLineaire& other){
	PseudoFonctionLineaire* result = new PseudoFonctionLineaire(*this);
	for(int i = 0 ; i < (int) other.m_vecEltZ.size() ; i++){
		if(other.m_vecEltZ[i] != NULL){
			result->addEltZ(other.m_vecEltZ[i]->getExposant(), other.m_vecEltZ[i]->getCoeff());
		}
	}

	return result;
}

/**
 *	Redifinition de l'opérateur +=
 *
 *	\param other : FonctionLinaire a additioné
 *	\return FonctionLinaire additioné
 *
 *	\auhtor Nicolas Simon
 *	\date 09/11/11
 *
 */
PseudoFonctionLineaire* PseudoFonctionLineaire::operator+=(const PseudoFonctionLineaire& other){
	for(int i = 0 ; i < (int) other.m_vecEltZ.size() ; i++){
		if(other.m_vecEltZ[i] != NULL){
			this->addEltZ(other.m_vecEltZ[i]->getExposant(), other.m_vecEltZ[i]->getCoeff());
		}
	}

	return this;
}
/**
 *	Redifinition de l'opérateur - 
 *
 *	\param other : FonctionLinaire a soustraire
 *	\return FonctionLinaire Soustrait 
 *
 *	\auhtor Nicolas Simon
 *	\date 09/11/11
 *
 */
PseudoFonctionLineaire* PseudoFonctionLineaire::operator-(const PseudoFonctionLineaire& other){
	PseudoFonctionLineaire* result = new PseudoFonctionLineaire(*this);
	for(int i = 0 ; i < (int) other.m_vecEltZ.size() ; i++){
		if(other.m_vecEltZ[i] != NULL){
			result->addEltZ((other.m_vecEltZ[i]->getExposant()), -(other.m_vecEltZ[i]->getCoeff()));
		}
	}

	return result;
}

/**
 *	Redifinition de l'opérateur -= 
 *
 *	\param other : FonctionLinaire a soustraire
 *	\return FonctionLinaire Soustrait 
 *
 *	\auhtor Nicolas Simon
 *	\date 09/11/11
 *
 */
PseudoFonctionLineaire* PseudoFonctionLineaire::operator-=(const PseudoFonctionLineaire& other){
	for(int i = 0 ; i < (int) other.m_vecEltZ.size() ; i++){
		if(other.m_vecEltZ[i] != NULL){
			this->addEltZ((other.m_vecEltZ[i]->getExposant()), -(other.m_vecEltZ[i]->getCoeff()));
		}
	}

	return this;
}

/**
 *	Redifinition de l'opérateur * 
 *
 *	\param other : FonctionLinaire a multiplié
 *	\return FonctionLinaire multiplié
 *
 *	\auhtor Nicolas Simon
 *	\date 09/11/11
 *
 */
PseudoFonctionLineaire* PseudoFonctionLineaire::operator*(const PseudoFonctionLineaire& other){
	PseudoFonctionLineaire* result = new PseudoFonctionLineaire(this->m_startNode);
	EltZ2* temp;
	
	for(int i = 0 ; i < (int) this->m_vecEltZ.size() ; i++){
		for(int j = 0 ; j < (int) other.m_vecEltZ.size() ; j ++){
			if(this->m_vecEltZ[i] != NULL && other.m_vecEltZ[j] != NULL){
				temp = *this->m_vecEltZ[i] * *other.m_vecEltZ[j];
				result->addEltZ(temp->getExposant(), temp->getCoeff());
				delete temp;
			}
		}
	}
	return result;
}

/**
 *	Redifinition de l'opérateur *= 
 *
 *	\param other : FonctionLinaire a multiplié
 *	\return FonctionLinaire multiplié
 *
 *	\auhtor Nicolas Simon
 *	\date 09/11/11
 *
 */
PseudoFonctionLineaire* PseudoFonctionLineaire::operator*=(const PseudoFonctionLineaire& other){
	EltZ2* temp;
	PseudoFonctionLineaire* tempFct = new PseudoFonctionLineaire(*this);
	
	for(int i = 0 ; i < (int) tempFct->m_vecEltZ.size() ; i++){
		this->removeEltZ(i);
		for(int j = 0 ; j < (int) other.m_vecEltZ.size() ; j ++){
			if(tempFct->m_vecEltZ[i] != NULL && other.m_vecEltZ[j] != NULL){
				temp = *tempFct->m_vecEltZ[i] * *other.m_vecEltZ[j];
				this->addEltZ(temp->getExposant(), temp->getCoeff());
				delete temp;
			}
		}
	}
	return this;
}

/**
 *	Redifinition de l'opérateur / 
 *
 *	\param other : FonctionLinaire diviseur 
 *	\return FonctionLinaire divisé
 *
 *	\auhtor Nicolas Simon
 *	\date 02/07/12
 */
PseudoFonctionLineaire* PseudoFonctionLineaire::operator/(const PseudoFonctionLineaire& other){
	EltZ2* temp;
	PseudoFonctionLineaire* result = new PseudoFonctionLineaire(this->m_startNode);

	if((int) other.m_vecEltZ.size() != 1 && other.m_vecEltZ[0] == NULL){
		trace_error("Le dénominateur de la division n'est pas une constante");
	}

	for (int i = 0; i < (int) this->m_vecEltZ.size() ; i++) {
		if(this->m_vecEltZ[i] != NULL){
			temp = *this->m_vecEltZ[i] / *other.m_vecEltZ[0];
			result->addEltZ(temp->getExposant(), temp->getCoeff());
			delete temp;
		}
	}

	return result;
}

/**
 *	Definition de l'operateur delay. Incrémente l'exposant de la PseudoFonctionLineaire 
 *
 * 	\author Nicolas Simon
 * 	\date 09/11/11
 *
 */
void PseudoFonctionLineaire::operatorDelay(){
	for(int i = 0 ; i < (int) this->m_vecEltZ.size() ; i++){
		if(this->m_vecEltZ[i] != NULL){
			this->getEltZ(i)->setExposant(this->getEltZ(i)->getExposant()+1);
		}
	}
}


float PseudoFonctionLineaire::computeK(){
	float result = 0;

	for(int i = 0 ; i < (int) this->m_vecEltZ.size() ; i++){
		if(this->m_vecEltZ[i] != NULL){
			result += this->m_vecEltZ[i]->getCoeff() * this->m_vecEltZ[i]->getCoeff();
		}
	}

	return result;
}

float PseudoFonctionLineaire::computeL(){
	float result = 0;

	for( int i = 0 ; i < (int) this->m_vecEltZ.size() ; i++){
		if(this->m_vecEltZ[i] != NULL){
			result += this->m_vecEltZ[i]->getCoeff();
		}
	}
	
	return result;
}
