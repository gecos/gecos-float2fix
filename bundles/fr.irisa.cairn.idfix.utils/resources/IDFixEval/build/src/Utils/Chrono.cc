#include <iostream>

#include "utils/Chrono.hh"

#ifdef __APPLE__
# include <mach/clock.h>
# include <mach/mach.h>

# define clock_gettime clock_get_time
# define CLOCK_MONOTONIC 				SYSTEM_CLOCK
# define CLOCK_THREAD_CPUTIME_ID		SYSTEM_CLOCK
# define CLOCK_PROCESS_CPUTIME_ID		SYSTEM_CLOCK
# define CLOCK_REALTIME					REALTIME_CLOCK
# undef clock_gettime

int clock_gettime(int clk_id, timespec *tm) {
	clock_serv_t cclock;

	host_get_clock_service(mach_host_self(), clk_id, &cclock);
	clock_get_time(cclock, (mach_timespec_t*)(tm));
	mach_port_deallocate(mach_task_self(), cclock);

	return 0;
}
#endif

// The tv_sec member corresponds to a number of seconds while tv_nsec
// member corresponds to the number of nanoseconds expired in the
// current second.
timespec timespec_delta(const timespec& t1, const timespec& t2) {
	timespec result;

	if ((t2.tv_nsec - t1.tv_nsec) < 0 /* ns */) {
		result.tv_sec = t2.tv_sec - t1.tv_sec - 1;
		result.tv_nsec = 1000000000 /* ns */ + t2.tv_nsec - t1.tv_nsec;
	} else {
		result.tv_sec = t2.tv_sec - t1.tv_sec;
		result.tv_nsec = t2.tv_nsec - t1.tv_nsec;
	}

	return result;
}

timespec timespec_add(const timespec& t1, const timespec& t2) {
	timespec result;

	if ((t1.tv_nsec + t2.tv_nsec) > 1000000000 /* ns */) {
		result.tv_sec = t1.tv_sec + t2.tv_sec + 1;
		result.tv_nsec = t1.tv_nsec + t2.tv_nsec - 1000000000 /* ns */;
	} else {
		result.tv_sec = t1.tv_sec + t2.tv_sec;
		result.tv_nsec = t1.tv_nsec + t2.tv_nsec;
	}

	return result;
}

Chrono::Chrono(): stopped(true),clock_id(CLOCK_MONOTONIC) {
		total_time.tv_sec = 0;
		total_time.tv_nsec = 0;
}

Chrono::Chrono(Clock::Type clock): stopped(true) {
	if (clock == Clock::Thread) {
		clock_id = CLOCK_THREAD_CPUTIME_ID;
	}
	else if(clock == Clock::Process) {
		clock_id = CLOCK_PROCESS_CPUTIME_ID;
	}
	else if(clock == Clock::Real){
		clock_id = CLOCK_REALTIME;
	}
	else if(clock == Clock::Mono){
		clock_id = CLOCK_MONOTONIC;
	}
	else{
		std::cerr<<"Type d'horloge incorrect"<<std::endl;
		exit(-1);
	}

	total_time.tv_sec = 0;
	total_time.tv_nsec = 0;
}

void Chrono::start() throw(std::runtime_error) {
	if (stopped) {
		total_time.tv_sec = 0;
		total_time.tv_nsec = 0;
		if (clock_gettime(clock_id, &start_time) != 0) {
			throw std::runtime_error("Error retrieving clock value.");
		}
		stopped = false;
	}
}

void Chrono::stop() throw(std::runtime_error) {
	if (!stopped) {
		timespec _stop;
		if (clock_gettime(clock_id, &_stop) != 0) {
			throw std::runtime_error("Error retrieving clock value.");
		}
		timespec diff = timespec_delta(start_time, _stop);
		total_time = timespec_add(total_time, diff);
		stopped = true;
	}
}

void Chrono::reset() {
	stopped = true;
	total_time.tv_nsec = 0;
	total_time.tv_sec = 0;
}

	double Chrono::nsec() const throw (std::logic_error) {
		if (!stopped)
			throw std::logic_error("Can't retrieve the "
					"nanosecond counter while the chronometer is running.");

		return total_time.tv_nsec;
	}

	double Chrono::sec() const throw (std::logic_error) {
		if (!stopped)
			throw std::logic_error("Can't retrieve the "
					"second counter while the chronometer is running.");

		return total_time.tv_sec;
	}

