#include "graph/dfg/datanode/NoeudData.hh"
#include "graph/dfg/datanode/NoeudSrc.hh"
#include "graph/dfg/NoeudGFD.hh"
#include "graph/lfg/NoeudGFL.hh"
#include "graph/toolkit/AdjacentMatrix.hh"

#include "utils/graph/cycledismantling/CycleDismantling.hh"
#include "utils/graph/cycledismantling/GraphPath.hh"

static bool CycleDetection(NoeudGraph* nodeGraph);
static GraphPath* CycleEnumeration(Graph* graph);
static AdjacentMatrix* CreateAdjacentMatrix(Graph* graph);

static void CycleDismantlingProcessing(Gfd* gfd, Gfd* simpleGfd, GraphPath* graphPath);
static void CycleNodeDismantling(Gfd* gfd, vector<NoeudData*> * ListNoeudsCommuns);
extern Gfd* GFDSimplificationToCycleEnumeration(Gfd* gfd);

static int PathExtension(AdjacentMatrix * MatriceG, LigneMatrice * VecteurP, AdjacentMatrix * MatriceH, int k, int NbElements);

static void VariableSubstitution(Gfl* gfl, int numeroNoeudCommun, GraphPath* listeDesCircuits);
static void MergeNodeWithOneSuccessor(Gfl* gfl);
static vector<int>* CycleSharedNodeSearch(GraphPath * listeDesCircuits);

/**
 *  Function determining if there is a circuit
 *
 * 		\return : booleen TRUE if there is one
 *
 *  \ingroup T21
 *  by Mohammad Diab creation le 07/01/2008
 */
bool CycleDetection(Graph* graph){
	trace_output(TOOL_STEP_2_2_1_1);
	NoeudGraph *node;
	bool cycleIsPresent = false;
	graph->resetInfoVisite(); //on RAZ les infos visites des noeuds
	for (int i = 0; i < graph->getNbOutput() && cycleIsPresent == false; i++) {
		node = graph->getOutputGf(i);
		cycleIsPresent = CycleDetection(node);
	}

	return cycleIsPresent;
}

/**
 *  Recursive function to see if there is a cycle
 *
 * 	\return booleen TRUE if there is one
 *
 */
static bool CycleDetection(NoeudGraph* node){
	NoeudGraph *nodePred;
	bool cycleIsPresent = false;

	if (node->getEtat() == ENTRAITEMENT) {
		// On vient de nous demander de traiter un noeud déjà en traitement
		// c'est donc qu'on est en train de tourner en rond
		cycleIsPresent = true;
	}
	else if (node->getEtat() == NONVISITE) {
		// Ici on traite un nouveau noeud, on le marque donc en traitement
		node->setEtat(ENTRAITEMENT);

		for(int i = 0 ; i < node->getNbPred() && !cycleIsPresent ; i++){
			nodePred = node->getElementPred(i);
			cycleIsPresent = CycleDetection(nodePred);
		}

		node->setEtat(VISITE);
	}

	return cycleIsPresent;
}


void CycleDismantling(Gfd* gfd){
	Gfd* simpleGFD;
	GraphPath* listDesCircuits;
	while(CycleDetection(gfd)){
		simpleGFD = GFDSimplificationToCycleEnumeration(gfd);
		listDesCircuits = CycleEnumeration(simpleGFD);
//		listDesCircuits = CycleEnumeration(gfd);

		CycleDismantlingProcessing(gfd, simpleGFD, listDesCircuits);

		delete simpleGFD;
		delete listDesCircuits;
	}
	gfd->liaisonsSuccPredInitFin();
}

/**
 * Function which create adjacent matrix and then search circuit in the GFD
 *
 *  \return GraphPath *	  list of path got
 *
 *  \ingroup T21
 *  by Loic Cloatre creation le 16/12/2008
 */
static GraphPath* CycleEnumeration(Graph* graph){
	trace_output(TOOL_STEP_2_2_1_2);
	GraphPath *Path = NULL;
	GraphPath *testPath = NULL;
	int numeroSortie = (*graph->getBeginTabOutput())->getNumero();
	AdjacentMatrix* matriceEnCours = CreateAdjacentMatrix(graph);
	if (CycleDetection(graph)) {
		testPath = Path->searchPathCircuits(matriceEnCours, matriceEnCours->getHauteurMatrice(), numeroSortie); //on recherche les cheminsa
	}

	delete matriceEnCours;
	return testPath;
}


static LigneMatrice *AjoutCel(LigneMatrice * ligne, int slot, int cel) {
	if (slot < ligne->size) {
		ligne->contenu[slot] = cel;
	}
	else {
		ligne = (LigneMatrice *) realloc(ligne, (ligne->size + 2) * sizeof(int));
		ligne->contenu[ligne->size] = cel;
		ligne->size++;
	}
	return ligne;
}

/**
 *  function to create adjacent matrix
 *
 *  We scan list of node one by one: for each we fill the number of predecessor
 *  example of result:
 *  mat =		[0]
 *  			[5 2]
 *  			[1]
 *  that means: "node number 1(end) has no successor, the number 2 has 5 and 2 as successor, the number 3 has 1 as successor"
 *  each line of matrix gives the list of successor
 *
 *  \ingroup T211
 */
static AdjacentMatrix* CreateAdjacentMatrix(Graph* graph){
	int i, j;
	int k = 0;
	int Num;
	AdjacentMatrix *MatriceG;
	LigneMatrice *premierInit = (LigneMatrice *) malloc(sizeof(int) * 2);
	premierInit->size = 1;
	premierInit->contenu[0] = 0;

	//cout<<"Taille de la matrice : "<<this->nbNode<<endl;
	MatriceG = new AdjacentMatrix(premierInit, graph->getNbNode()); //on rentre dans la matrice le premier noeud qui correspond au "init", on ne lui met pas de successeur, ce sera pareil pour le "end"
	NoeudGraph *NGraph;
	NoeudGraph *nodePred1, *nodePred2;
	bool NoeudValide;
	NodeGraphContainer::iterator it;

	it = graph->getBeginTabNode();
	++it;
	i = 1;
	for (++it; it != graph->getEndTabNode(); ++it) { // On parcourt la liste des Noeuds (préfix = +rapide)

		i++; // On commence a 2
		NGraph = *it;
		NoeudValide = true;
		if (NGraph->getNbPred() <= 1) //on teste pour voir si le noeud a des listSucc
		{
			nodePred1 = NGraph->getElementPred(0);

			if (nodePred1 != NULL) {
				Num = nodePred1->getNumero(); //on teste le numero du predecesseur: le 1 correspond au noeud INIT non valide donc
				if (Num <= 1) {
					NoeudValide = false;
				}
			}
		} //endof if (NGraph->getNbPred() <= 1)

		if ((NGraph->getNbSucc() > 0) && (NoeudValide = true)) //on regarde le/les listSucc du Noeud
		{
			nodePred2 = NGraph->getElementSucc(0);
			j = 0;
			k = 0; //ajout lcl car dans un exemple, un noeud graphe qui avait plusieurs successeur n'est pas vu car K non RAZ

			while (nodePred2 != NULL) {
				Num = nodePred2->getNumero(); //on recupere le numero du noeud successeur nodePred2
				if (Num > 1) {
					//cout<<"Numero Noeud ajouté cellule num>1: "<<i<<endl;
					MatriceG->addCelulle(i - 1, Num); //on ajoute le numero du noeud a la suite de la ligne i-1
					j = j + 1;
				}
				else {//ajout lcl pour traiter le cas du noeud de fin qui n'a pas de successeur mais doit etre present dans la matrice
					//NoeudData *sortieGraph = (NoeudData *) NGraph;
					if (NGraph->isNodeSink()) {
						//cout<<"Numero Noeud ajouté cellule output: "<<i<<endl;
						MatriceG->addCelulle(i - 1, 0); //on ajoute une ligne dans la matrice avec 0 comme successeur
					}
					else if ((NGraph->getNbSucc() == 1) && (nodePred2->getTypeNodeGraph() == FIN)) {
						//cout<<"Numero Noeud ajouté cellule fin: "<<i<<endl;
						MatriceG->addCelulle(i - 1, 0); //on ajoute une ligne dans la matrice avec 0 comme successeur
					}
				} //endof if (Num>1)

				k++; //on incremente pour aller tester le successeur suivant si existe
				if (k < NGraph->getNbSucc()) {
					nodePred2 = NGraph->getElementSucc(k);
				}
				else {
					nodePred2 = NULL; //condition pour sortir de la boucle while
				}
			} //endof while (nodePred2!=NULL)
		} //endof if ((NGraph->getNbSucc() > 0) && (NoeudValide = true))
	} //endof for (++it; it != tabNode->end();++it)

	//cout<<"matrice adjacente cree: "<<endl;

	return (MatriceG);
}

GraphPath* GraphPath::searchPathCircuits(AdjacentMatrix * MatriceG, int NbLignes, int DernierElement){
	int j; // indice de l'élément à ajouter dans le chemin
	AdjacentMatrix *MatriceH = new AdjacentMatrix(NbLignes);
	// Matrice d'adjacence contenant les noeuds déjà traités
	LigneMatrice *VecteurP;
	// Contient les noeuds du chemin élémentaire en cours d'élaboration
	unsigned int Pk;
	// Numéro du noeud traité
	bool AlgoTermine;
	GraphPath *ListeCircuitsLocal = NULL;

	int k = 0; // Indice du noeud courant dans le chemin 
	// objectif voir si des noeuds sont ajoutes ou enleves 

	VecteurP = (LigneMatrice *) malloc(sizeof(int) * 2); //  VecteurP: vecteur contenant les noeuds du chemin elementaire
	VecteurP->size = 1;
	VecteurP->contenu[0] = 1;
	// On initialise VecteurP avec le noeud 1, c'est à dire le noeud FIN du graph.

	AlgoTermine = false;
	while (AlgoTermine == 0) {
		//  PathGraph Extension ---------------- 

		j = PathExtension(MatriceG, VecteurP, MatriceH, k, NbLignes);
		// On regarde si on  peut ajouter un noeud dans le chemin (si oui j=numéro du noeud, sinon j=-1)
		Pk = VecteurP->contenu[k];

		if (j != -1) {
			// VecteurP->contenu[k + 1] = MatriceG->getCelulle(Pk-1,j);
			VecteurP = AjoutCel(VecteurP, k + 1, MatriceG->getCelulle(Pk - 1, j));
			// On a réussi à trouver un noeud, on l'ajoute dans le VecteurP.

			k++; // passage au noeud suivant 
		}
		else {
			// Aucun element ne peut etre ajoute, notre chemin est le plus long possible, donc on va regarder si c'est un Circuit

			// Circuit confirmation ----------------
			if (MatriceG->getLigne(Pk - 1) != NULL && MatriceG->getLigne(Pk - 1)->Appartient(VecteurP->contenu[0])) {
				// Le premier élément étudié est un successeur du dernier, c'est donc un circuit !

				ListeCircuitsLocal = addPath(VecteurP, ListeCircuitsLocal); // Sauvegarde du circuit  
			}
			//endof if( Appartient

			// Vertex Closure ---------------- 
			if (k == 0) {
				// Advance initial vertex 
				//if( VecteurP->at(0) == DernierElement )                       // le dernier element a ete traite 
				if (VecteurP->contenu[0] == NbLignes) { // modif lcl, si la sortie se retrouve sur la 2eme ligne de la matrice on ne traite pas les suivantes, on met donc la taille pour tout traiter
					// on a fini d'explorer tous les noeuds
					AlgoTermine = true;
				}
				else {
					// on recommence l'algo avec le prochain noeud en 'racine' de circuit
					VecteurP->contenu[0] = VecteurP->contenu[0] + 1;
					k = 0;
					MatriceH->resetMatrice(); // Iniatisation a 0 de H 
				}
			}
			else {
				// on a pas trouvé de circuit commençant par A>B>C, on essaie avec A>B>D

				if (Pk > 0) {
					MatriceH->resetLigneMatrice(Pk - 1);
				}
				// Recherche du premier element vide de MatriceH[VecteurP[k-1]-1][.....] 

				MatriceH->addCelulle(VecteurP->contenu[k - 1] - 1, Pk);

				VecteurP = AjoutCel(VecteurP, k, 0);

				//m = 0;                                                                                         //Indice de l element
				//if(VecteurP->contenu[k-1]>0)
				//{
				//      cout<<"Contenu vecteur : "<<VecteurP->contenu[k-1]-1<<endl;
				//      cout<<"m : "<<m<<endl;
				//      while( MatriceH->getCelulle( VecteurP->contenu[k-1]-1 ,m) != 0)
				//      {
				//              m ++;
				//      }
				//}

				VecteurP = (LigneMatrice *) realloc(VecteurP, sizeof(int) * (k + 1));
				VecteurP->size = k;
				// On retire le dernier élément du vecteur P
				// Il serait plus rapide de ne pas le réallouer ...
				k--; // Retour en arriere 
			} //endof if(k == 0)
		} //endof if(j != -1)
	} // endof while (AlgoTermine == 0)

	delete MatriceH;
	free(VecteurP);

    return (ListeCircuitsLocal);
}

static int PathExtension(AdjacentMatrix * MatriceG, LigneMatrice * VecteurP, AdjacentMatrix * MatriceH, int k, int NbElements){
	int j = -1;
	int x;
	int y;

	if (VecteurP == NULL) {
		trace_error("VecteurP est NULL");
		return -1;
	}

	if (k >= VecteurP->size) {
		trace_error("Accès à l'élément %d dans un vecteur de taille %d", k,
				VecteurP->size);
		return -1;
	}

	x = VecteurP->contenu[k]; /* x : Numero du noeud traite */

	if (x > NbElements) {
		trace_error("Accès au noeud %d dans une matrice en comportant %d",x,NbElements);
		return -1;
	}

	// On parcourt les listSucc de x
	while (j < MatriceG->getLongueurLigne(x - 1) - 1) {
		j++;
		y = MatriceG->getCelulle(x - 1, j); // y : numéro du successeur courant

		// On regarde si y ne fait pas partie des noeuds déja traités
		if (y > VecteurP->contenu[0] && VecteurP->Appartient(y) == false && (MatriceH->getLigne(x - 1) == NULL || MatriceH->getLigne(x - 1)->Appartient(y) == false)) {
			return j;
		}
	}

	return -1;
}

static void CycleDismantlingProcessing(Gfd* gfd, Gfd* simpleGfd, GraphPath* graphPath){
	trace_output(TOOL_STEP_2_2_1_3);
	NoeudData* OutputTraitee;
	vector<NoeudData *> mesNoeudADemantelerTotal;
	bool conditionSortie = false;
	GraphPath *circuitEnCours = graphPath;
	if (!graphPath->isEmpty()) // Si aucun circuit, aucune raison de démenteler
	{
		simpleGfd->resetInfoVisite();
		//gfd->resetInfoVisite();

		for (int i = 0; i < gfd->getNbOutput(); i++) {
			OutputTraitee = dynamic_cast<NoeudData*> (simpleGfd->getOutputGf(i)); //on recupere le NoeudGraph de sortie caste en data
			assert(OutputTraitee != NULL); // Une sortie doit être une donnée
			while (!conditionSortie) {
				OutputTraitee->rechercheDernierPointCommunFromSimpleGraph(circuitEnCours, OutputTraitee, &mesNoeudADemantelerTotal, false);
				//OutputTraitee->rechercheDernierPointCommun(circuitEnCours, OutputTraitee, &mesNoeudADemantelerTotal, false);
				if (circuitEnCours->Suiv != NULL) //on va regarder tous les circuits afin de recuperer tous les numeros des noeuds a demanteler
				{
					circuitEnCours = circuitEnCours->Suiv; //on prend le circuit suivant
					simpleGfd->resetInfoVisite();
					//gfd->resetInfoVisite();
				}
				else {
					conditionSortie = true;
				}
			}
			circuitEnCours = graphPath; //pour chaque sortie on remet la liste des circuits
			conditionSortie = false; //dans le cas ou il y a plusieurs sorties, pour rentrer dans le while
		}
	}

    /*vector<NoeudData*>::const_iterator itconst;
    for(itconst = mesNoeudADemantelerTotal.begin() ; itconst != mesNoeudADemantelerTotal.end() ; itconst++){
        trace_info("%s\n", (*itconst)->getName().c_str());
    }*/

	CycleNodeDismantling(gfd, &mesNoeudADemantelerTotal);
}

static void CycleNodeDismantling(Gfd* gfd, vector<NoeudData*> * ListNoeudsCommuns){
	NoeudOps *NoeudOpsSucc;
	NoeudData *NoeudCommun;
	TypeVar *monType; //utilise pour editer le type de noeud en 'input' ou 'output'

	//pre-traitement des noeuds, on vérifie que chaque noeud n'est présent qu'une fois dans le vecteur
	vector<NoeudData *>::iterator it1 = ListNoeudsCommuns->begin();
	vector<NoeudData *>::iterator it2 = ListNoeudsCommuns->begin();

	while (it1 != ListNoeudsCommuns->end()) {
		while (it2 != ListNoeudsCommuns->end()) {
			if (((*it1)->getNumero() == (*it2)->getNumero()) && (it1 != it2)) {
				ListNoeudsCommuns->erase(it2); //on enleve les dupliquas
				it2 = ListNoeudsCommuns->begin(); //on se place sur le suivant
			}
			else {
				it2++; //on pointe sur le suivant
			}
		}
		it1++; //on se place sur le suivant
		it2 = ListNoeudsCommuns->begin(); //on se place sur le suivant
	} //fin pretraitement


	for (int j = 0; j < (int) ListNoeudsCommuns->size(); j++) {
		NoeudCommun = ListNoeudsCommuns->at(j); //on recupere le premier noeud
		//for (int i = 0; i < NoeudCommun->getNbSucc(); i++) //on va commencer par creer un noeud clone pour chaque successeur en noeud "input"
		while(NoeudCommun->getNbSucc())
		{
			NoeudData *cloneNoeudCommun;
			//creation d'un noeud clone
			if (gfd->getNoiseEval()) {
				cloneNoeudCommun = new NoeudData(*NoeudCommun);
			}
			else {
                cloneNoeudCommun = new NoeudSrc(*NoeudCommun); 
			}

			//il faut casser les liens de predecesseur, changer le numero et le type en input
			monType = cloneNoeudCommun->getTypeVar();
			monType->setTypeVarFonct(VAR_ENTREE);
			cloneNoeudCommun->setTypeVar(monType); //on change le type
			cloneNoeudCommun->setNumero(gfd->getNumNode()); //on commence a partir de 2 les noeud dans le xml
			cloneNoeudCommun->setNoeudDemantele(true); //on indique que le noeud a ete cree lors d'un demantelement
			cloneNoeudCommun->setTypeSignalBruit(NoeudCommun->getTypeSignalBruit()); //on set le type signal ou bruit

			//on travaille sur les successeur: le clone a recopie tous les listSucc: il n'en faut qu'un seul
			NoeudOpsSucc = (NoeudOps *) NoeudCommun->getElementSucc(0); // 0 car on supprime des succ l'edge après (listSucc fonctionne comme une liste et pas un vector dû a la non importance des numero des sortie)

			// Sauvegarde du numInput et du format avant la modif sur les edges
			int numInput = NoeudCommun->getEdgeSucc(0)->getNumInput();

			gfd->addNoeudGraph((NoeudGFD *) cloneNoeudCommun); //on ajoute le noeud au graphe
			gfd->addInputGf((NoeudGFD *) cloneNoeudCommun); //on ajoute le noeud dans la liste d'entree
			NoeudCommun->deleteEdgeDirectedTo((NoeudGraph *) NoeudCommun-> getElementSucc(0));
			cloneNoeudCommun->edgeDirectedTo(NoeudOpsSucc, numInput);

		} //endof for(int i=0; i< NoeudCommun->getNbSucc(); i++)


		//pour finir on met le noeudCommun comme "output"
		monType = NoeudCommun->getTypeVar();
		monType->setTypeVarFonct(VAR_SORTIE);
		NoeudCommun->setTypeVar(monType);
		gfd->addOutputGf((NoeudGFD *) NoeudCommun); //on ajoute le noeud a la liste de sortie

		/*int temp_noeud_succ = NoeudCommun->getNbSucc();
		for (int i = 0; i < temp_noeud_succ; i++) {
			NoeudCommun->deleteEdgeDirectedTo((NoeudGraph *) NoeudCommun-> getElementSucc(0));
		}
		NoeudCommun->setNbSucc(0);
		NoeudCommun->getListeSucc()->clear();*/

	} //endof for(int j=0; j<(int) ListNoeudsCommuns->size(); j++)

	gfd->liaisonsSuccPredInitFin();
}


void CycleReduction(Gfl* gfl){
	// nouvelle méthode de recherche des points commun qui permet de detecter tous les points communs les plus jeune si il y en a plusieurs + substitution de la thése de daniel
	/*if(this->cycleIsPresent()){
		GraphPath* listeDesCircuits;
		map<int ,list<int> > pathAvecDependanceNoeudCommun; // Pour chaque path, on informe avec quels autres paths il a des noeuds communs
		vector<list<NoeudGraph*> >  noeudCommunPathDependant; // Fourni pour chaque path les noeuds communs
		list<NoeudGraph*> noeudsCommunsASubstituer;
		list<NoeudGraph*>::iterator itNoeudCommunsASubstituer;

		// Enumeration des circuits
		listeDesCircuits = this->enumerationOfCircuitPath();
		//listeDesCircuits->displayPath();

		// Permet de determiner quels paths ont des noeuds commun et de les classé
		pathAvecDependanceNoeudCommun = listeDesCircuits->recherchePathAvecNoeudCommun();

		// Permet de fournir quels noeud sont les noeuds communs entre les paths dependant (ayant au moins un noeud commun)
		noeudCommunPathDependant = listeDesCircuits->rechercheNoeudCommunDansPath(pathAvecDependanceNoeudCommun, this);

		// Determine les noeuds commun a substituer
		noeudsCommunsASubstituer = this->determinationNoeudsASubstituer(noeudCommunPathDependant);

		// Execution de la substitution sur les noeuds determinés
		for(itNoeudCommunsASubstituer = noeudsCommunsASubstituer.begin() ; itNoeudCommunsASubstituer != noeudsCommunsASubstituer.end() ; itNoeudCommunsASubstituer++){
			//cout<<(*itNoeudCommunsASubstituer)->getNumero()<<" "; // Affichage
			this->variableSubstitution((*itNoeudCommunsASubstituer)->getNumero(), listeDesCircuits);
			}
			}*/

	// Version avec la méthode de loic de pour determiner les noeud communs dans les circuits + substitution de la thèse de Daniel
	vector<int>* listeDesNoeudCommuns;
	int numeroNoeudCommunASubstituer;
	GraphPath* listeDesCircuits;
	GraphPath* listeDesCircuitsTemp;
	vector<int>*numeroNoeud = new vector<int> ;
	vector<int>*nbOccurence = new vector<int> ;
	if(CycleDetection(gfl)){

		listeDesCircuits = CycleEnumeration(gfl);
		listeDesNoeudCommuns = CycleSharedNodeSearch(listeDesCircuits);
		//listeDesCircuits->displayPath();
		// cas plusieurs cycle imbriqué
		if(listeDesNoeudCommuns->size() != 0 ){
			VariableSubstitution(gfl, listeDesNoeudCommuns->at(0), listeDesCircuits);
		}
		// Méthode a Loic avec la construction de l'histogramme d'occurences mais avec l'algorithme de substitutions qui apparait dans la thèse de Daniel
		else if (listeDesCircuits->Suiv != NULL){// Cas plusieurs cycle en série
			do{
				listeDesCircuitsTemp = listeDesCircuits;
				listeDesCircuitsTemp->histogrammeOccurenceNoeudDansCircuits(numeroNoeud, nbOccurence);
				//on recherche les occurences des noeuds dans des circuits
				numeroNoeudCommunASubstituer = gfl->choixNoeudASubstituerIndependant(listeDesCircuits, numeroNoeud, nbOccurence);

				if (numeroNoeudCommunASubstituer != -1) // On a trouvé un noeud à substituer
				{
					VariableSubstitution(gfl, numeroNoeudCommunASubstituer, listeDesCircuits);
					numeroNoeud->clear(); //on vide le vecteur pour le passage suivant
					nbOccurence->clear(); //on vide le vecteur pour le passage suivant
				}
				else
					break;

				delete listeDesCircuits;
				listeDesCircuits = CycleEnumeration(gfl);
				//listeDesCircuits->displayPath();
			}
			while(!listeDesCircuits->isEmpty());
		}
		delete listeDesCircuits;
	}
	delete numeroNoeud;
	delete nbOccurence;

	MergeNodeWithOneSuccessor(gfl);
}

/**
 *  Fonction qui recherche un noeud commun dans les circuits trouvés
 *
 *  \param  GraphPath *	la liste des circuits a regarder
 *  \return vector<int> *	les numero des noeuds communs
 *
 *  by Loic Cloatre creation le 06/02/2009
 */
static vector<int>* CycleSharedNodeSearch(GraphPath * listeDesCircuits) {
	vector<int>*LigneBase;
	GraphPath *cheminSuivant;
	vector<int> LigneSuivante;
	vector<int>::iterator itBase;
	vector<int>::iterator itBasedebuglcl;
	bool Trouve = false;
	bool resteCircuit = true;

	cheminSuivant = listeDesCircuits;

	if (listeDesCircuits->isEmpty()) {
		LigneBase = new vector<int> (); //il n'y a pas de circuit
	}
	else if (listeDesCircuits->Suiv == NULL) {

		if ((int) listeDesCircuits->pathSize() > 1) //on ne prend pas les circuits unitaires
		{
			LigneBase = new vector<int> (0); //il y a un seul circuit
			for (int i = 0; i < (int) listeDesCircuits->pathSize(); i++)
				LigneBase->push_back(listeDesCircuits->pathItem(i));
		}
		else {
			LigneBase = new vector<int> (); //il n'y a pas de circuit superieur a l'unite
		}
	}
	else {
		LigneBase = new vector<int> (listeDesCircuits->pathSize()); //on copie la premiere ligne
		for (int w = 0; w < listeDesCircuits->pathSize(); w++) {
			LigneBase->at(w) = listeDesCircuits->pathItem(w);
		}

		cheminSuivant = listeDesCircuits->Suiv; //on initialise le premier chemin
		LigneSuivante.clear();
		for (int i = 0; i < (int) cheminSuivant->pathSize(); i++)
			LigneSuivante.push_back(cheminSuivant->pathItem(i));

		while (resteCircuit == true) //tant que le chemin suivant n'est pas vide
		{
			//cout<<"debug lcl: "<<debugLCL<<endl;
			//debugLCL++;

			itBase = LigneBase->begin();

			while (itBase != LigneBase->end()) //on va tester chaque element de ligneSuivante
			{
				for (int j = 0; j < (int) LigneSuivante.size(); j++) {
					if ((int) LigneSuivante[j] == *itBase) {
						Trouve = true;
						j = LigneSuivante.size(); //pour sortir de la boucle
					}
				}

				if (Trouve == false) {
					if (LigneBase->size() != 0) //securite
					{
						LigneBase->erase(itBase); //on enleve le numero non trouve
						itBase = LigneBase->begin();
					}
					else {
						itBase = LigneBase->end(); //la securite ci dessous evite d'incrementer et louper la condition de sortie
					}
				}

				if ((LigneBase->size() != 0) && (Trouve == true)) //securite
				{
					itBase++;
				}

				Trouve = false;
			} //endof while(itBase!=LigneBase->end())

			if (cheminSuivant->Suiv != NULL) //s'il y a encore des circuits a explorer
			{
				cheminSuivant = cheminSuivant->Suiv;
				// LigneSuivante = cheminSuivant->Path;
				LigneSuivante.clear();
				for (int i = 0; i < (int) cheminSuivant->pathSize(); i++)
					LigneSuivante.push_back(cheminSuivant->pathItem(i));
			}
			else {
				resteCircuit = false;
			}
		}

#if 0
		{
			cout << "Liste des numeros communs aux circuits: " << endl;
			itBase = LigneBase->begin();
			while (itBase != LigneBase->end()) {
				cout << (int) *itBase << " ";
				itBase++;
			}
			cout << endl;
		}
#endif
	}
	return LigneBase;
}

/*
 *	Fonction qui permet la suppression des cycles non unitaire via substitutions de variable.
 *
 *	Nous utilisons les cycles detecté pour créér un arbre fictif qui nous permet de substituer les neouds. Nous parcourons en profondeur les noeuds des circuits en effectuant les substitutions.
 *	Chaque niveau de profondeur du parcours doit être effectué à chaque cicrcuit "en même temps" jusqu'a que tout les noeuds faisant partie des circuits detecté soit traité.
 *
 *	Exemple :
 *
 *	3 circuit A B et C :
 *	Step 1 sur A B et C
 *	Step 2 sur A B et C
 *
 *	Nicolas Simon
 *	20/05/11
 */
static void VariableSubstitution(Gfl* gfl, int numeroNoeudCommun, GraphPath* listeDesCircuits){
	GraphPath* listeDesCircuitsTemp;
	bool gflModif = false;
	int indiceNCommunDansPath;
	int indicePred; // Niveau de traitement au niveau des cycles
	int indiceNPredDansPath;
	int indiceNDepartDansPath;
	EdgeGFL* edgeTempNDepartNPred;
	EdgeGFL* edgeTempNPredNCommum;
	EdgeGFL* edgeTemp;
	NoeudGFL* nCommun;
	NoeudGFL* nPred;
	NoeudGFL* nDepart;
	PseudoLinearFunction* foncTemp;
	PseudoLinearFunction* fonctionLineaireNCommunNPred;
	PseudoLinearFunction* fonctionLineaireNCommunNDepart;
	list<NoeudGFL*> listNPred; // Liste qui permet de supprimer les edges entre le nPred et le nCommun apres chaque step car plusieurs substitution de la meme step peuvent utiliser les informations contenue dans le même arc
	list<NoeudGFL*>::iterator it;
	map<int, int> mapNoeud; // Map qui permet de garder une mémoire sur les précédents circuits deja traité et evite de faire plusieurs fois la même substitution

	indicePred = 1;
	nCommun = dynamic_cast<NoeudGFL*> (gfl->getElementGf(numeroNoeudCommun));
	// Tant que nous avons pas fini de traiter tous les noeuds des différents cycle
	do{
		gflModif = false;
		listeDesCircuitsTemp = listeDesCircuits;
		// Nous devons traiter tous les cycles aux mêmes niveaux
		while(listeDesCircuitsTemp != NULL){
			if(indicePred<listeDesCircuitsTemp->pathSize()){ // Traitement a effectuer que si nous n'avons pas encore fini tous les noeud du cycle courant
				// Recherche de la position du nCommun dans le path courant
				indiceNCommunDansPath = -1;
				for(int j = 0 ; j < listeDesCircuitsTemp->pathSize() ; j++){
					if(listeDesCircuitsTemp->pathItem(j) == nCommun->getNumero()){
						indiceNCommunDansPath = j;
						break;
					}
				}

				// Le noeud n'est pas dans ce cycle
				if(indiceNCommunDansPath == -1){
					listeDesCircuitsTemp = listeDesCircuitsTemp->Suiv;
					continue;
				}

				// Les indices représentent l'emplacement des noeud dans le path par rapport au niveau de profondeur du parcours où nous sommes rendu
				indiceNPredDansPath = (listeDesCircuitsTemp->pathSize()+indiceNCommunDansPath-indicePred)%listeDesCircuitsTemp->pathSize();
				indiceNDepartDansPath = (listeDesCircuitsTemp->pathSize()+indiceNCommunDansPath-indicePred-1)%listeDesCircuitsTemp->pathSize();
				nPred = dynamic_cast<NoeudGFL*> (gfl->getElementGf(listeDesCircuitsTemp->pathItem(indiceNPredDansPath)));
				nDepart = dynamic_cast<NoeudGFL*> (gfl->getElementGf(listeDesCircuitsTemp->pathItem(indiceNDepartDansPath)));
				assert(nPred != NULL && nDepart != NULL);
				edgeTempNPredNCommum = (EdgeGFL*) nPred->getEdgeSucc(nCommun);
				edgeTempNDepartNPred = (EdgeGFL*) nDepart->getEdgeSucc(nPred);

				// Il est possible que dans chaque step, il y a plusieurs circuit qui ont des chemins communs
				// Test pour savoir si il a déja été traiter ou non ou si nous sommes sur une branche divergeante de l'arbre fictif
				if(mapNoeud[nPred->getNumero()] == nDepart->getNumero() || edgeTempNPredNCommum == NULL){
					listeDesCircuitsTemp = listeDesCircuitsTemp->Suiv;
					continue;
				}

				assert(edgeTempNDepartNPred != NULL && edgeTempNPredNCommum != NULL);

				// Merge des fonction lineaire en série
				fonctionLineaireNCommunNPred = edgeTempNPredNCommum->getPseudoFonctionLineaire();
				fonctionLineaireNCommunNDepart = *edgeTempNDepartNPred->getPseudoFonctionLineaire() * *fonctionLineaireNCommunNPred;
				listNPred.push_front(nPred);

				// La substitution peut entraîner la possibilité d'avoir 2 arc en parallèle, nous les mergons donc en additionant les fonction lineaires
				// Sinon on ajoute juste l'arc
				if(nCommun->isPred(nDepart)){
					edgeTemp = (EdgeGFL*) nDepart->getEdgeSucc(nCommun);
					foncTemp = *edgeTemp->getPseudoFonctionLineaire() + *fonctionLineaireNCommunNDepart;
					edgeTemp->setPseudoFonctionLineaire(foncTemp);
				}
				else{
					nDepart->edgeDirectedTo(nCommun, fonctionLineaireNCommunNDepart);
				}
				gflModif = true;

				mapNoeud[nPred->getNumero()] = nDepart->getNumero();

				// Cas particulier pour le noeud Commun soit relier aux entrée du graph
				for(int i = 0 ; i<nPred->getNbPred() ; i++){
					nDepart = dynamic_cast<NoeudGFL*> (nPred->getElementPred(i));
					if(nDepart == NULL) // nPred est une source
						break;

					if(nDepart->isNodeSource() && ((NoeudData*) nDepart->getRefParamSrc())->getTypeNodeData() != DATA_SRC_BRUIT && !nCommun->isPred(nDepart)){
						edgeTemp = (EdgeGFL*)nDepart->getEdgeSucc(nPred);
						foncTemp = *edgeTemp->getPseudoFonctionLineaire() * *fonctionLineaireNCommunNPred;
						nDepart->edgeDirectedTo(nCommun, foncTemp);
					}
				}

			}


			listeDesCircuitsTemp = listeDesCircuitsTemp->Suiv;
		} // Fin pour tout les circuits

		for(it = listNPred.begin() ; it != listNPred.end() ; it++){
			(*it)->deleteEdgeDirectedTo(nCommun);
		}
		listNPred.clear();
		mapNoeud.clear();

		indicePred++;
		//this->saveGf(RuntimeParameters::generateVarName("variableSubstitution").data());
	}
	while(gflModif);
}

/*
 *	Supprime les noeuds inutile du Gfl (ayant un seul successeur) tout en mergeant les fonctions linéaire pour obtenir le GFL le plus simple possible
 *
 *	Nicolas Simon
 *	24/05/11
 * */

static void MergeNodeWithOneSuccessor(Gfl* gfl){
	NoeudGFL* nGFL;
	NoeudGFL* nArrivee;
	NoeudGFL* nDepart;
	EdgeGFL* edgeTemp;
	PseudoLinearFunction* foncNDepartNArrivee;
	PseudoLinearFunction* foncNGFLNArrive;
	PseudoLinearFunction* foncTemp;
	PseudoLinearFunction* foncNDepartNGFL;
	bool graphModif = true;

	// Nous devons effectuer ce traitement tant que le graph est modifié car a chaque passage, une modification apporté peut en permettre une nouvelle
	while(graphModif){
		graphModif = false;

		for(int i = 2 ; i<gfl->getNbNode() ; i++){
			nGFL = dynamic_cast<NoeudGFL*> (gfl->getElementGf(i));
			assert(nGFL != NULL);
			// Ne doit ppas merger les noeuds qui sont tagger dans le cas du calcul de la dynamique avec la méthode d'Andrei
			if(nGFL->getNbSucc() == 1 && !nGFL->isNodeInput() && !nGFL->isNodeOutput() && nGFL->getTypeNoeudGFL() != PHI_GFL && (gfl->getNoiseEval() || ((NoeudData*) nGFL->getRefParamSrc())->getDynProcess() != OVERFLOW_PROBA)){
				nArrivee = dynamic_cast<NoeudGFL*> (nGFL->getElementSucc(0));
				edgeTemp = dynamic_cast<EdgeGFL*> (nGFL->getEdgeSucc(0));
				assert(nArrivee != NULL && edgeTemp != NULL);
				foncNGFLNArrive = edgeTemp->getPseudoFonctionLineaire();

				int nPredGFL = nGFL->getNbPred();
				for(int j = 0 ; j<nPredGFL ; j++){
					nDepart = dynamic_cast<NoeudGFL*> (nGFL->getElementPred(0));
					foncNDepartNGFL = ((EdgeGFL*) nGFL->getEdgePred(0))->getPseudoFonctionLineaire();
					assert(nDepart != NULL);
					foncTemp = *foncNDepartNGFL * *foncNGFLNArrive;

					if(nDepart->isSucc(nArrivee)){
						foncNDepartNArrivee = ((EdgeGFL*) nDepart->getEdgeSucc(nArrivee))->getPseudoFonctionLineaire();
						foncTemp = *foncTemp + *foncNDepartNArrivee;
						nDepart->deleteEdgeDirectedTo(nArrivee);
					}

					nDepart->deleteEdgeDirectedTo(nGFL);
					nDepart->edgeDirectedTo(nArrivee, new PseudoLinearFunction(*foncTemp));
				}

				nGFL->deleteEdgeDirectedTo(nArrivee);
				gfl->deleteGraphNode(nGFL);
				// Modification effectué, nous devons refaire une autre passe pour voir si il y a encore des simplification a faire
				graphModif = true;
			}
		}
	}

	// Renumérotaion des noeuds pour ne pas avoir de "trou" dans celle-ci du a la suppression de noeuds
	for(int i = 2 ; i<gfl->getNbNode() ; i++)
		gfl->getElementGf(i)->setNumero(i);

}
