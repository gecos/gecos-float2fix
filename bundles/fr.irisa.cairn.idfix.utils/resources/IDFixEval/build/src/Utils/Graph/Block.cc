/*!
 * \author Adrien Destugues
 * \date 08.10.2010
 * \class Block
 * \brief Class representing a code block.
 */

// === Bibliothèques .hh associe
#include "utils/graph/Block.hh"

// ======================================= Constructeurs - Destructeurs

/**
 * Constructeur de la classe Block
 */
Block::Block(int idBlock, Block * parent) {
	m_id = idBlock;
	m_parent = parent;
	m_proba = 1.0; // Default value : always execute this block
}

void Block::setProbability(float m_proba) {
	if (m_proba <= 1.0 && m_proba >= 0.0) {
		this->m_proba = m_proba;
	}
}
std::queue<int> Block::getFQID() {
	if (m_parent == NULL) {
		std::queue<int> q;
		q.push(m_id);
		return q;
	}
	else {
		std::queue<int> q = m_parent->getFQID();
		q.push(m_id);
		return q;
	}
}
