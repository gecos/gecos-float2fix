/**
 * \file GraphSimplificationToCycleDetection.cc
 * \date 13/04/11
 * \author Nicolas Simon	
 * \brief Regroupe les méthodes permettant la simplification de Gfd avant l'énumeration des circuits
 *
 */
#include <map>

#include "graph/dfg/datanode/NoeudSrc.hh"
#include "graph/dfg/EdgeGFD.hh"
#include "graph/dfg/Gfd.hh"
#include "graph/toolkit/TypeVar.hh"
#include "utils/graph/cycledismantling/GraphPath.hh"
#include "graph/Edge.hh"

static Gfd* GFDSimplification(Gfd* gfd);
static void RemoveSourceAndAssociedNode(Gfd* gfd);
static bool RemoveSourceAndAssociedNode(NoeudData* nData, map<int, NoeudData*> *mapNoeud, Gfd* gfd);
static void RemoveOneInputOutputNode(Gfd* gfd);

// Fonction permettant d'ajouter un edgeGFD entre deux noeud. Elle ne fait pas exactement la même chose que le méthode addEdge contenu dans les noeud
static void addEdge(NoeudGraph* queue, NoeudGraph* tete, int numInput);

/**
 *	Simplification of Gfd and determine nodes which must be dismantling
 *	\return return node to must be dismantling
 *
 * \author Nicolas Simon	
 * \date 13/04/11
 */

Gfd* GFDSimplificationToCycleEnumeration(Gfd* gfd){
	Gfd* gfdSimple;

	gfdSimple = GFDSimplification(gfd);

#if SAVE_GRAPH
	if(gfd->getNoiseEval())
		gfdSimple->saveGf("T21.GFD.A");
	if(!gfd->getNoiseEval())
		gfdSimple->saveGf("T11.GFD.A");
#endif

	RemoveSourceAndAssociedNode(gfdSimple);

#if SAVE_GRAPH
	if(gfd->getNoiseEval())
		gfdSimple->saveGf("T21.GFD.B");
	if(!gfd->getNoiseEval())
		gfdSimple->saveGf("T11.GFD.B");
#endif

	RemoveOneInputOutputNode(gfdSimple);

	// Pour l'énumeration des circuit via création d'une matrice adjacente, il ne doit pas avoir de trou dans la numérotation des noeuds
	for(int i = 2 ; i<gfdSimple->getNbNode() ; i++)
		gfdSimple->getElementGf(i)->setNumero(i);

#if SAVE_GRAPH
	if(gfd->getNoiseEval())
		gfdSimple->saveGf("T21.GFD.C");
	if(!gfd->getNoiseEval())
		gfdSimple->saveGf("T11.GFD.C");
#endif

	return gfdSimple;
}

/**
 *	Remove all the Op node of a GFD and create Edge from predecessor of operation to successor 
 *	\return Return new Gfd without operation
 *	\date 08/04/11
 *	\author Nicolas Simon
 */

static Gfd* GFDSimplification(Gfd* gfd){
	NoeudGFD* noeudGfd;
	NoeudData * noeudDataPred;
	NoeudData * noeudDataSucc;
	NoeudData * noeudDataSuccCopie;
	NoeudData * noeudDataPredCopie;
	// Tableau necessaire pour savoir si le noeud a deja été créé et ajouté dans le nouveau graph
	NoeudData * tabNodeCreate[gfd->getNbNode()];
	for(int i = 0 ; i < gfd->getNbNode() ; i++)
		tabNodeCreate[i] = NULL;

	Gfd* gfdSimple = new Gfd(gfd->getNoiseEval());

	for(int i = 0 ; i < gfd->getNbNode() ; i ++) {
		noeudGfd = dynamic_cast<NoeudGFD *> (gfd->getElementGf(i));

		if(noeudGfd != NULL && noeudGfd->getTypeNodeGFD() == OPS){
			// Un noeud Ops ne peut avoir que un seul successeur 
			assert(noeudGfd->getNbSucc() == 1);

			noeudDataSucc = dynamic_cast <NoeudData * > (noeudGfd->getElementSucc(0));
			// Un noeud Succ d'un op ne peut être qu'un noeudData
			assert(noeudDataSucc != NULL);
			if(tabNodeCreate[noeudDataSucc->getNumero()] == NULL){
				noeudDataSuccCopie = noeudDataSucc->clone();
				noeudDataSuccCopie->setPtrclone(noeudDataSucc);
				gfdSimple->addNoeudData(noeudDataSuccCopie);

				tabNodeCreate[noeudDataSucc->getNumero()] = noeudDataSuccCopie;
			}

			for(int i = 0 ; i<noeudGfd->getNbPred() ; i++){
				noeudDataPred = dynamic_cast<NoeudData * > (noeudGfd->getElementPred(i));
				assert(noeudDataPred != NULL); // Un pred d'un noeud Op est obligatoirement un noeudData
				if(tabNodeCreate[noeudDataPred->getNumero()] == NULL){
					noeudDataPredCopie = noeudDataPred->clone();
					noeudDataPredCopie->setPtrclone(noeudDataPred);
					gfdSimple->addNoeudData(noeudDataPredCopie);

					tabNodeCreate[noeudDataPred->getNumero()] = noeudDataPredCopie;
				}

				tabNodeCreate[noeudDataPred->getNumero()]->addEdge(tabNodeCreate[noeudDataSucc->getNumero()], i);
			}
		}
	}

	return gfdSimple;
}

/*
 *	Remove node which have only predecessor coming from source node
 *
 *  \date 11/04/11
 *  \author Nicolas Simon
 *
 */
static void RemoveSourceAndAssociedNode(Gfd* gfd){
	map<int, NoeudData*> *mapNoeud = new map<int , NoeudData*>;
	int jPred, jSucc;
	gfd->resetInfoVisite();
	NoeudData* nData;

	for(int i = 0 ; i< gfd->getNbOutput() ; i++){
		nData = dynamic_cast<NoeudData *> (gfd->getOutputGf(i));
		assert(nData != NULL);
		RemoveSourceAndAssociedNode(nData, mapNoeud, gfd);
	}

	map<int, NoeudData*>::iterator it = mapNoeud->begin();
	for( ; it != mapNoeud->end() ; it++){
		for(int j = 0 ; j<(*it).second->getNbSucc() ; j++){
			jSucc = (*it).second->getElementSucc(j)->searchPred((*it).second);
			(*it).second->getElementSucc(j)->getListePred()->erase((*it).second->getElementSucc(j)->getListePred()->begin()+jSucc);
			(*it).second->getElementSucc(j)->setNbPred((*it).second->getElementSucc(j)->getNbPred()-1);
			delete (*it).second->getEdgeSucc(j);
		}
		// Important de modifié le nombre de succ pour le destructeur du noeud
		(*it).second->setNbSucc(0);

		for(int j = 0 ; j<(*it).second->getNbPred() ; j++){
			jPred = (*it).second->getElementPred(j)->searchSucc((*it).second);
			(*it).second->getElementPred(j)->getListeSucc()->erase((*it).second->getElementPred(j)->getListeSucc()->begin()+jPred);
			(*it).second->getElementPred(j)->setNbSucc((*it).second->getElementPred(j)->getNbSucc()-1);
			delete (*it).second->getEdgePred(j);
		}
		// Important de modifié le nombre de pred pour le destructeur du noeud
		(*it).second->setNbPred(0);

		gfd->deleteGraphNode((*it).second);
	}
	gfd->liaisonsSuccPredInitFin();
	delete mapNoeud;
}


static bool RemoveSourceAndAssociedNode(NoeudData* data, map<int, NoeudData*> *mapNoeud, Gfd* gfd){
	NoeudData* nData;
	bool isSource = true;

	if(data->isNodeInput() || data->isNodeConst()){
		mapNoeud->insert(pair<int, NoeudData*>(data->getNumero(), data));
		data->setEtat(VISITE);
		return true;
	}
	else if(mapNoeud->find(data->getNumero()) != mapNoeud->end())
		return true;

	else if(data->getEtat() != NONVISITE)
		return false;

	else {
		data->setEtat(ENTRAITEMENT);
		for(int i = 0 ; i<data->getNbPred() ; i++){
			nData = dynamic_cast<NoeudData *> (data->getElementPred(i));
			assert(nData != NULL);
			isSource = isSource & RemoveSourceAndAssociedNode(nData, mapNoeud, gfd);
		}
		if(isSource && !data->isNodeOutput()){
			mapNoeud->insert(pair<int, NoeudData*>(data->getNumero(), data));
			data->setEtat(VISITE);
			return true;
		}
		else
			return false;
	}
}
/**
 *	Remove all node which have only one predecessor and successor and add a edge between this pred and this succ
 *
 *  \date 11/04/11
 *  \author Nicolas Simon
 */
static void RemoveOneInputOutputNode(Gfd* gfd){
	NoeudData* nData;
	NoeudGraph* nGraph;
	NoeudGraph* nPred;
	NoeudGraph* nSucc;
	int i = 0 ;
	int numInput;
	while(i<gfd->getNbNode()){
		nGraph = gfd->getElementGf(i);
		if(nGraph->getTypeNodeGraph() != INIT && nGraph->getTypeNodeGraph() != FIN && nGraph->getNbPred() == 1 && nGraph->getNbSucc() == 1){
			nData = dynamic_cast<NoeudData*> (nGraph);
			assert(nData != NULL);
			if(nData->isNodeOutput())
				i++;
			else{
				nPred = nData->getElementPred(0);
				nSucc =nData->getElementSucc(0);
				if(nSucc->getNbPred() <=1)
					numInput = 0;
				else if(nData->getEdgeSucc(0)->getNumInput()>nSucc->getNbPred())
					numInput = nSucc->getNbPred();
				else
					numInput = nData->getEdgeSucc(0)->getNumInput();

				nData->deleteEdgeDirectedTo(nSucc);
				nData->EnleveedgeFromOf(nPred);
				addEdge(nPred, nSucc, numInput);
				gfd->deleteGraphNode(i);
			}
		}
		else
			i++;
	}
}
/**
 *	Fonction permettant d'ajouter un edgeGFD entre deux noeud. Elle ne fait pas exactement la même chose que le méthode addEdge contenu dans les noeud
 *	\author Nicolas Simon
 */
static void addEdge(NoeudGraph * queue, NoeudGraph* tete, int numInput) {
	EdgeGFD* Elt;

	Elt = new EdgeGFD(queue, tete, numInput);
	queue->getListeSucc()->push_back(Elt); // Ajout du successeur NGraph
	queue->setNbSucc(queue->getNbSucc() + 1);
	if(numInput< tete->getNbPred())
		assert(tete->getEdgePred(numInput) == NULL); // Empeche l'ecrasement d'un edge pred sans avoir fait les traitement necessaire de suppression.
	tete->setListePred(Elt, numInput); // Ajout du predecesseur this à la liste de NGraph
	//	tete->getListePred()->push_back(Elt);
	tete->setNbPred(tete->getNbPred() + 1);
}
