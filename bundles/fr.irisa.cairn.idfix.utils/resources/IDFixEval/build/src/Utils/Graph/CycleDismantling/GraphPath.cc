/*!
 *  \author Daniel Menard, Jean-Charles Naud
 *  \date   28.01.2002
 *  \version 1.0
 *	\class NoeudGraph
 *  \brief Classe generique definissant les noeuds present dans un graphe
 */

// === Bibliothèques .hh associe
#include "utils/graph/cycledismantling/GraphPath.hh"
#include "graph/Graph.hh"

// === Namespaces
using namespace std;

/**
 * Constructeur de la classe Chemin
 */
GraphPath::GraphPath() {
	this->Path = (LigneMatrice *) malloc(sizeof(int));
	this->Path->size = 0;
	this->Suiv = NULL;
}

/**
 *  constructeur de recopie
 *  \param copie objet à recopier
 *
 *	Les maillons suivant du path sont également copiés.
 *
 *  by Cloatre loic creation le 10/02/2008
 */
GraphPath::GraphPath(const GraphPath & copie) :
	m_Edges(copie.m_Edges) {
	this->Path = (LigneMatrice *) malloc(sizeof(int) * (copie.Path->size + 1));
	this->Path->size = copie.Path->size;

	for (int i = 0; i < copie.Path->size; i++) {
		this->Path->contenu[i] = copie.Path->contenu[i];
	}

	if (copie.Suiv != NULL) {
		this->Suiv = new GraphPath(*copie.Suiv); //si il reste des chemins on les recree
	}
	else {
		this->Suiv = NULL; //sinon on met a NULL
	}
}

GraphPath::GraphPath(LigneMatrice * ligne) {
	this->Path = (LigneMatrice *) malloc(sizeof(int) * (ligne->size + 1));
	this->Path->size = 0;
	for (int i = 0; i < ligne->size; i++) {
		if (ligne->contenu[i] != 0) {
			this->Path->contenu[i] = ligne->contenu[i];
			this->Path->size++;
		}
	}
	this->Suiv = NULL;
}

/**
 * Destructeur de la classe Chemin
 */
GraphPath::~GraphPath() {
	free(this->Path);
	delete Suiv;
}

/**
 * Methode qui regarde si les deux numeros sont contenus dans une seule ligne
 *
 *  \param  unsigned int 	numero recherche
 *  \param  unsigned int 	numero recherche
 *  \return bool 			true si trouve
 *
 *  by Loic Cloatre creation le 10/02/2009
 */
bool GraphPath::CircuitContientDeuxNoeuds(unsigned int x1, unsigned int x2) {

	bool ret = false;
	//cout<<"fonction non testee dans path.cc: attention au clone et reference sur le path, marche?"<<endl;
	const GraphPath *cheminSuivant = this;

	if (this->Appartient(x1) && //test sur le premier circuit si x1 est dedans
			this->Appartient(x2)) //test sur le premier circuit si x2 est dedans
	{
		ret = true;
	}
	else {
		while (cheminSuivant->Suiv != NULL) //s'il en reste encore
		{
			cheminSuivant = cheminSuivant->Suiv; //on se place sur le chemin suivant

			if (cheminSuivant->Appartient(x1) && //test sur le circuit si x1 est dedans
					cheminSuivant->Appartient(x2)) //test sur le circuit si x2 est dedans
			{
				ret = true;
			}
		}
	}

	return ret;
}

/**
 * Methode qui renvoie true si les deux circuits n'ont aucun noeud en commun
 *
 *  \param  GraphPath * 	circuit a comparer
 *  \return bool 			true si aucun noeud en communs
 *
 *  by Loic Cloatre creation le 26/03/2009
 */
bool GraphPath::CircuitSansPointCommunAvec(GraphPath * ListePath) {
	bool ret = true;

	if ((this != NULL) && (ListePath != NULL)) {
		for (int i = 0; i < (int) this->Path->size; i++) {
			for (int j = 0; j < (int) ListePath->Path->size; j++) {
				if (this->Path->contenu[i] == ListePath->Path->contenu[j]) //on regarde si les numeros sont les memes
				{
					ret = false;
					j = ListePath->Path->size; //pour sortir de la boucle
					i = this->Path->size; //pour sortie de la boucle
				}
			}
		}
	}

	return ret;
}

/**
 * Methode qui renvoie true si les deux circuits ont un seul noeud en commun
 *
 *  \param  GraphPath * 	circuit a comparer
 *  \return bool 			true si un noeud en communs
 *
 *  by Loic Cloatre creation le 26/03/2009
 */
bool GraphPath::CircuitAvecUnPointCommun(GraphPath * ListePath) {
	int nbNoeuds = 0;
	bool ret = false;

	if ((this != NULL) && (ListePath != NULL)) {
		for (int i = 0; i < (int) this->Path->size; i++) {
			for (int j = 0; j < (int) ListePath->Path->size; j++) {
				if (this->Path->contenu[i] == ListePath->Path->contenu[j]) //on regarde si les numeros sont les memes
				{
					nbNoeuds++;
				}
			}
		}
	}
	//cout<<"debug lcl: nbNoeud en communs: "<<nbNoeuds<<endl;
	if (nbNoeuds == 1) {
		ret = true;
	}

	return ret;
}

void GraphPath::addElemPath(const NoeudGraph* noeud, EdgeGFT* edge) {
	Path = (LigneMatrice *) realloc(Path, (Path->size + 2) * sizeof(int));
	Path->contenu[Path->size] = noeud->getNumero();
	Path->size++;

	if (edge)
		m_Edges.push_back(edge);
}


/*
 *	Rend le nombre de path contenu dans le GraphPath
 *
 *	Nicolas Simon
 *
 *	01/06/11
 *
 */
int GraphPath::sizeGraphPath() {
	GraphPath *MaillonCourant = this;
	int i = 0;
	while (MaillonCourant != NULL) {
		i++;
		MaillonCourant = MaillonCourant->Suiv;
	}
	return i;
}

/*
 *	Rend vrai si l'élément est présent dans le graphPath en cours, faux sinon
 *
 *	Nicolas Simon
 * 	16/06/11
 */

bool GraphPath::Appartient(int element) const{
	for(int i = 0 ; i<this->pathSize() ; i++){
		if(element == this->pathItem(i))
				return true;
	}	
	return false;
}


bool GraphPath::Appartient(int x, vector<int>*u) {
	assert(u != NULL);
	for (int i = 0;  i < (int) u->size(); i++) {
			if ( x == (*u)[i]) {
				return true;
			}
		}
	return false;
}


/*
 * Permet a partir d'un GraphPath de determiner quels path ont des noeuds communs et de les classer
 *
 *	Nicolas Simon
 *	01/06/11
 *
 */

map<int, list<int> > GraphPath::recherchePathAvecNoeudCommun(){
	map<int , list<int> > listPathCommun; // First = numero du GraphPath ; Second = les numero de GraphPath avec qui il y a des noeuds communs
	GraphPath* pathCourant = this;
	GraphPath* pathRecherche;
	int indicePathRecherche; //Numero du GraphPath où on effectue la recherche
	list<int>::iterator itListMap;
	bool dejaCommun;


	for(int i = 0 ; i <this->sizeGraphPath()-1 ; i++){ // Pour tout les paths
		for(int j = 0 ; j<pathCourant->pathSize() ; j++){ // Pour tout les éléments du path courant

			for(pathRecherche = pathCourant->Suiv, indicePathRecherche = i+1 ; pathRecherche != NULL ; pathRecherche = pathRecherche->Suiv, indicePathRecherche++){ // Pour tout les path suivants
				for(itListMap  = listPathCommun[i].begin(), dejaCommun = false ; itListMap != listPathCommun[i].end() ; itListMap++){ // Test si le path courant est deja dans la map du path suivant
					if(indicePathRecherche == *itListMap){
						dejaCommun = true;
						break;
					}
				}
				if(!dejaCommun && pathRecherche->Appartient(pathCourant->pathItem(j))){ // Si pathCourant et le path de recherche ont un noeud commun, on ajoute l'information dans la map des 2 paths
					listPathCommun[i].push_back(indicePathRecherche);
					listPathCommun[indicePathRecherche].push_back(i);
				}
			}
		}	
		pathCourant = pathCourant->Suiv;
	}
	
	
	// Affichage
	/*map<int , list<int> >::iterator itMap;
	list<int>::iterator it;
	cout<<"Affichage des Path et de leur path qui ont des points communs"<<endl;
	for(itMap = listPathCommun.begin() ; itMap != listPathCommun.end() ; itMap++){
		cout<<"Numero du path : "<<(*itMap).first<<endl;
		it = (*itMap).second.begin();
		while(it != (*itMap).second.end()){
			cout<<(*it)<<endl;
			it++;
		}
	}
	cout<<endl<<endl;*/

	return listPathCommun;
}
/*
 * Rend le path accocié a indice dans le GraphPath
 *
 *	Nicolas Simon
 *	01/06/11
 *
 */
GraphPath* GraphPath::getPath(int indice){
	GraphPath* pathCourant = this;
	for(int i = 0 ; i<indice; i++)
		pathCourant = pathCourant->Suiv;

	return pathCourant;
}


/*
 * Permet de fournir quels noeud sont les noeuds communs entre les paths dependant (ayant au moins un noeud commun)
 *
 *	Nicolas Simon
 *	01/06/11
 *
 */

vector<list<NoeudGraph*> > GraphPath::rechercheNoeudCommunDansPath(map<int, list<int> > listPathCommun, Graph* graph){
	list<int>::iterator itListNodeCommun;
	GraphPath* pathCourant;
	GraphPath* pathRecherche;
	map<int, list<int> >::iterator itMap;
	bool isCommunEntrePath;
	bool isCommun;

	vector<list<NoeudGraph*> > vectorNodeCommun;
	list<NoeudGraph*> temp;

	for(itMap = listPathCommun.begin() ; itMap != listPathCommun.end() ; itMap++) { // Pour chaque element de la map
		pathCourant = this->getPath((*itMap).first);
		for(int i = 0 ; i<pathCourant->pathSize() ; i++){	// Pour chaque noeud du path courant
			isCommunEntrePath = true;
			for(itListNodeCommun = (*itMap).second.begin() ; itListNodeCommun != (*itMap).second.end() && isCommunEntrePath ; itListNodeCommun++){ // Pour chaque path associé

				pathRecherche = this->getPath(*itListNodeCommun);
				isCommun = false;

				for(int j = 0 ; j<pathRecherche->pathSize() && !isCommun ; j++){
					if(pathCourant->pathItem(i) == pathRecherche->pathItem(j)){
						isCommun = true;
					}
				}

				isCommunEntrePath = isCommunEntrePath & isCommun;
			}
			if(isCommunEntrePath && !(*itMap).second.empty()){ // Pour ne pas ajouter les noeud des boucles unitaires
				temp.push_back(graph->getElementGf(pathCourant->pathItem(i)));
			}
		}
		if(!temp.empty())
		vectorNodeCommun.push_back(temp);
		temp.clear();
	}

	// Affichage
	/*list<NoeudGraph*>::iterator itListNGraph;
	cout<<"Numero des noeud Commun par Path dépendant"<<endl;
	for(int i = 0 ; i<(int)vectorNodeCommun.size() ; i++){
		cout<<"Case "<<i<<" : "<<endl;
		for(itListNGraph = vectorNodeCommun[i].begin() ; itListNGraph != vectorNodeCommun[i].end() ; itListNGraph++)
			cout<<(*itListNGraph)->getNumero()<<" ";
		cout<<endl;
	}
	cout<<endl<<endl;*/

	return vectorNodeCommun;
}
