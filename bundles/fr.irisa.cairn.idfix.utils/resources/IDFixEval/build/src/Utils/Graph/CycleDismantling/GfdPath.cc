/*!
 *  \author Daniel Menard, Jean-Charles Naud
 *  \date   10.06.2002
 *  \version 1.0
 */
// === Bibliothèques standard
//#include <iostream>
//#include <math.h>
//#include <string>
//#include <set>
//
//#include <algorithm>
//
//
//// === Bibliothèques non standard (Forward Declaration)
//
//// === Bibliothèques non standard (utiliser dans les Methodes et typeVarFonctions)
#include "graph/dfg/datanode/NoeudData.hh"
#include "graph/dfg/Gfd.hh"
#include "graph/dfg/operatornode/NoeudOps.hh"
#include "graph/toolkit/AdjacentMatrix.hh"

#include "utils/graph/cycledismantling/GraphPath.hh"
//
//#include <TypeVar.hh>
//
//********************************************************************************************************
//********************************************************************************************************
//********************************************************************************************************
//********************************************************************************************************


/**
 * Methode qui affiche le circuit trouve
 *
 *  \param  GraphPath * objet contenant la liste contenant les numeros des noeuds du circuit
 *
 *  \author Loic Cloatre
 *  \date 16/12/2008
 */
void GraphPath::displayPath() {
	int i;
	GraphPath *MaillonCourant;

	MaillonCourant = this;

	while (MaillonCourant != NULL) {
		cout << "Path:  " << endl;
		i = 0;
		cout << "Taille du path : " << MaillonCourant->Path->size << endl;

		while (i < (int) MaillonCourant->Path->size) {
			if ((MaillonCourant->Path->contenu[i] != 0)) {
				cout << " " << MaillonCourant->Path->contenu[i] << " ";
			}
			i++;
		} //endof while
		MaillonCourant = MaillonCourant->Suiv;
		cout << endl;
	} //endof while
}

/**
 * Methode qui regarde dans une ligne si le numero est present
 *
 *  \param  unsigned int 	numero recherche
 *  \param  LigneMatrice*	ligne ou on cherche
 *  \return bool 			true si trouve
 *
 *  by Loic Cloatre creation le 16/12/2008
 */
/*inline bool GraphPath::Appartient(int x) const {
	int i;

	if (Path == NULL)
		return false; //securite au cas ou la ligne n'a pas ete acquise dans la fonction getLigne
	for (i = 0; i < pathSize(); i++) {
		if (x == pathItem(i)) {
			return true;
		}
	}
	return false;
}*/

/**
 * Methode qui regarde dans une ligne si le numero est present
 *  \param  x 	numero recherche
 *  \param  u	ligne ou on cherche
 *  \return bool 			true si trouve
 *  by Loic Cloatre creation le 16/12/2008
 */
/*static*//*inline bool GraphPath::Appartient(unsigned int x, vector<int>*u) {
	unsigned int i;
	bool Trouve = false;

	if (u != NULL) //securite au cas ou la ligne n'a pas ete acquise dans la fonction getLigne
	{
		for (i = 0; i < u->size(); i++) {
			if ((int) x == (*u)[i]) {
				Trouve = true;
				i = u->size(); //pour sortir de la boucle
			}
		}
	}

	return (Trouve);

}*/

/**
 * Methode qui permet de savoir si un circuit a été trouvé
 *
 *  \return bool 			true si vide
 *
 *  \author Loic Cloatre
 *  \date 16/12/2008
 */
bool GraphPath::isEmpty() {
	return Path->size == 0; //si la taille du vecteur est nul c'est qu'aucun circuit n'a ete trouve
}

/**
 * Methode qui Renvoi l indice du noeud permettant d etendre le chemin
 *
 * Look at each successor of VecteurP[k] and find one that is not already in 
 * VecteurP or MatriceH. This node can be used to extend the path.
 *
 *  \param  MatriceG	matrice adjacente du GFD
 *  \param  VecteurP	vecteur contenant les noeuds du chemin elementaire
 *  \param  MatriceH	matrice contenant les noeuds deja traite
 *  \param  k			indice dans VecteurP du noeud a traiter
 *
 *  \return int			indice de l elememt a ajouter dans le chemin -1 si aucun element a ete trouve
 *
 *  \author Loic Cloatre
 *  \date le 16/12/2008
 */
//int GraphPath::pathExtension(AdjacentMatrix * MatriceG, LigneMatrice * VecteurP, AdjacentMatrix * MatriceH, int k, int NbElements) {
//	int j = -1;
//	int x;
//	int y;
//
//	if (VecteurP == NULL) {
//		trace_error("VecteurP est NULL");
//		return -1;
//	}
//
//	if (k >= VecteurP->size) {
//		trace_error("Accès à l'élément %d dans un vecteur de taille %d", k,
//				VecteurP->size);
//		return -1;
//	}
//
//	x = VecteurP->contenu[k]; /* x : Numero du noeud traite */
//
//	if (x > NbElements) {
//		trace_error("Accès au noeud %d dans une matrice en comportant %d",x,NbElements);
//		return -1;
//	}
//
//	// On parcourt les listSucc de x
//	while (j < MatriceG->getLongueurLigne(x - 1) - 1) {
//		j++;
//		y = MatriceG->getCelulle(x - 1, j); // y : numéro du successeur courant
//
//		// On regarde si y ne fait pas partie des noeuds déja traités
//		if (y > VecteurP->contenu[0] && VecteurP->Appartient(y) == false && (MatriceH->getLigne(x - 1) == NULL || MatriceH->getLigne(x - 1)->Appartient(y) == false)) {
//			return j;
//		}
//	}
//
//	return -1;
//}

/**
 * Methode qui ajoute un nouveau circuit
 *
 *  \param  LigneMatrice *VecteurP  	vecteur contenant les numeros du circuit a ajouter
 *  \param  GraphPath *ListePath  	Objet GraphPath ou ajouter le chemin
 *  \param  int NbElement  				nombre d'elements
 *  \return GraphPath  *				Objet GraphPath actualise
 *
 *  \author Loic Cloatre
 *  \date 16/12/2008
 */
GraphPath *GraphPath::addPath(LigneMatrice * VecteurP, GraphPath * ListePath) {
	GraphPath *MaillonCourant;
	GraphPath* MaillonNouv = new GraphPath(VecteurP);

	// Ajout a la fin de la liste
	if (ListePath == NULL) {
		ListePath = MaillonNouv;
	}
	else {
		MaillonCourant = ListePath;

		while (MaillonCourant->Suiv != NULL) {
			MaillonCourant = MaillonCourant->Suiv;
		}
		MaillonCourant->Suiv = MaillonNouv;
	} //endof if
	MaillonNouv->Suiv = NULL;

	return (ListePath);
}

/**
 * Methode qui regarde si le Noeud appartient au circuit
 *
 *  \param  TypeListePath * liste contenant les numeros des noeuds du circuit
 *  \return bool
 *
 *  \author Loic Cloatre
 *  \date le 16/12/2008
 */
bool NoeudData::belongsToTheCircuit(GraphPath * circuit) {
	for (int i = 0; i < (int) circuit->pathSize(); i++) {
		if (this->getNumero() == circuit->pathItem(i)) {
			return true;
		}
	}
	return false;
}

/**
 * Methode qui recherche le point commun entre un circuit et un graphe en parcourant celui-ci
 *
 *  \param  GraphPath *  liste contenant les numeros des noeuds du circuit
 *  \param  NoeudData *		output du graphe en cours
 *  \param  bool			true si parcours du demi graphe uniquement
 *  \param  ListNoeudData*	liste des noeud a demanteler
 *
 *  \author Loic Cloatre
 *  \date 16/12/2008
 */
void NoeudData::rechercheDernierPointCommun(GraphPath * circuit, NoeudData * raciceGraphe, vector<NoeudData *>*listNoeudAdemanteler, bool demiGrapheParcouru) {
	NoeudOps *tempNoeudOps;
	NoeudGFD *tempNoeudGFD = (NoeudGFD *) this;

	//le graphe est parcouru tant qu'on n'a pas trouve de point commun(ou bien une source du graphe)
	if (tempNoeudGFD->getEtat() == VISITE) //cas d'une boucle avec z-1
	{
		//cout<<"**"<<endl;
	}
	else {
		//cout<<"**"<<endl;
		this->Etat = VISITE; //au premier passage on marque le passage
		if (isNodeSource() || isNodeConst()) {
			//si le noeud est une entree, il ne peut pas appartenir a un circuit car il n'a pas de predecesseur
		}
		else if (this->belongsToTheCircuit(circuit) == true) //on regarde si le noeud appartient au circuit recherche
		{
			listNoeudAdemanteler->push_back(this);
		}
		else //on continue le parcours
		{
			tempNoeudOps = dynamic_cast<NoeudOps *> (getElementPred(0));
			if (tempNoeudOps == NULL) {
				trace_error("Le prédecesseur n'est pas une opération ! PROBLEME DE CONSTRUCTION DU GRAPHE");
				exit(-1);
			}
			else
				tempNoeudOps->rechercheDernierPointCommun(circuit, raciceGraphe, listNoeudAdemanteler, demiGrapheParcouru);
		}
	} //endof if(tempNoeudGFD->getEtat()== VISITE)
}



/**
 * Methode qui recherche le point commun entre un circuit et un graphe en parcourant celui-ci (a partir d'un graphe qui a été simplfié via la graphSimplificationToCycleDismantling
 *
 *  \param  GraphPath *  liste contenant les numeros des noeuds du circuit
 *  \param  NoeudData *		output du graphe en cours
 *  \param  bool			true si parcours du demi graphe uniquement
 *  \param  ListNoeudData*	liste des noeud a demanteler
 *
 *  \author Loic Cloatre
 *  \author Nicolas Simon
 *  \date 16/12/2008
 */
void NoeudData::rechercheDernierPointCommunFromSimpleGraph(GraphPath * circuit, NoeudData * raciceGraphe, vector<NoeudData *>*listNoeudAdemanteler, bool demiGrapheParcouru) {
	NoeudData *tempNoeudData;

	//le graphe est parcouru tant qu'on n'a pas trouve de point commun(ou bien une source du graphe)
	if (!this->getEtat() == VISITE && !isNodeSource() && !isNodeConst()){ //cas d'une boucle avec z-1
		this->Etat = VISITE; //au premier passage on marque le passage
		if (this->belongsToTheCircuit(circuit) == true) //on regarde si le noeud appartient au circuit recherche
		{
			listNoeudAdemanteler->push_back((NoeudData*)this->getPtrclone());
		}
		else //on continue le parcours
		{
			for(int i = 0 ; i<this->getNbPred() ; i++){
				tempNoeudData = dynamic_cast<NoeudData*> (this->getElementPred(i));
				assert(tempNoeudData != NULL);
				tempNoeudData->rechercheDernierPointCommunFromSimpleGraph(circuit, raciceGraphe, listNoeudAdemanteler, demiGrapheParcouru);
			}
		}
	} //endof if(tempNoeudGFD->getEtat()== VISITE)
}

/**
 * Methode qui a partir de la liste de circuits va compter combien de fois intervient chaque noeud
 *
 *  \param  vector<int>*			numero des noeuds
 *  \param  vector<int> *			nombre de fois qu'apparait le noeud
 *  \return int						noeud qui apparait le max(pas de choix particulier si plusieurs execaux
 *
 *  \author Loic Cloatre
 *  \date 09/02/2009
 */
int GraphPath::histogrammeOccurenceNoeudDansCircuits(vector<int>*numeroNoeud, vector<int>*nbOccurence) {
	int ret = 0;
	//vector<int>::iterator itBase;                                                                 //iterateur qui parcours les circuits
	int index;

	vector<int>::iterator itVectNumeroNoeud; //iterateur utilise pour determiner le max
	vector<int>::iterator itVectNbOccurence; //iterateur utilise pour determiner le max

	if ((numeroNoeud != NULL) && (nbOccurence != NULL)) {
		GraphPath *cheminSuivant = this;

		while (cheminSuivant != NULL) //tant qu'il reste des circuits( on en a au moins 2 si on arrive a cette methode)
		{
			//itBase =cheminSuivant->Path.begin();
			index = 0;

			if (cheminSuivant->Path->size > 1) //on ne regarde pas les circuit unitaires
			{
				for (int i = 0; i < (int) cheminSuivant->Path->size; i++) {
					cheminSuivant-> incrementeCompteurOccurenceNoeud(cheminSuivant->Path-> contenu[index], numeroNoeud, nbOccurence);
					//itBase++;                                                                             //on incremente pour avoir le numero du noeud suivant du circuit
					index++;
				}
			}

			cheminSuivant = cheminSuivant->Suiv; //on se place sur le chemin suivant
		}
	}
	else {
		trace_error("fct histogrammeOccurenceNoeudDansCircuits, vector<int>* numeroNoeud ou vector<int> *nbOccurence est NULL");
		exit(-1);
	}

	return ret;
}

/**
 * Methode qui incremente le compteur associe au numero
 *
 *  \param  int						numero du noeud cible
 *  \param  vector<int>*			numero des noeuds
 *  \param  vector<int> *			nombre de fois qu'apparait le noeud
 *  \return bool					false si nouveau noeud
 *  \author Loic Cloatre 
 *  \date 09/02/2009
 */
bool GraphPath::incrementeCompteurOccurenceNoeud(int numeroDuNoeud, vector<int>*numeroNoeud, vector<int>*nbOccurence) {
	bool ret = false;
	if ((numeroNoeud != NULL) && (nbOccurence != NULL)) {
		vector<int>::iterator itNumero = numeroNoeud->begin();
		vector<int>::iterator itOccurence = nbOccurence->begin();

		while (itNumero != numeroNoeud->end()) {
			if (*itNumero == numeroDuNoeud) //si on a trouve le bon noeud
			{
				*itOccurence = *itOccurence + 1; //on incremente le nombre d'occurence
				itNumero = numeroNoeud->end(); //pour sortir du while
				ret = true;
			}
			else {
				itOccurence++; //on incremente
				itNumero++; //on incremente
			}
		}
		if (ret == false) //ca veut dire que c'est un nouveau noeud
		{
			numeroNoeud->push_back(numeroDuNoeud); //on l'ajoute au vecteur
			nbOccurence->push_back(1); //une occurence donc on met le compteur a 1
		}
	}
	else {
		trace_error("fct incrementeCompteurOccurenceNoeud, vector<int>* numeroNoeud ou vector<int> *nbOccurence est NULL");
		exit(-1);
	}

	return ret;
}

/**
 * Methode qui va regarder si le choix du noeud ne tombe pas sur un cas ou les circuits ne sont pas enumeres dans l'ordre
 * ex: si on a [38 41 40] et [38 40] le noeud propose sera 38, cependant la bonne enumeration devrait etre [40 38 41] et [40 38] d'ou 40
 *
 *  \param  int			numero du noeud cible actuel
 *  \return int			le meme numero si pas de motif trouve
 *  \author Loic Cloatre
 *  \date 08/04/2009
 */
int GraphPath::searchFirstNodeCommun(int NumeroTest) {
	int ret = NumeroTest;
	GraphPath *cheminSuivant = new GraphPath(*this);
	GraphPath *debut = cheminSuivant;

	GraphPath *cheminAvecNumeroTest = new GraphPath(); //chemins contenant le NumeroTest
	GraphPath *debutNumeroTest = cheminAvecNumeroTest;

	GraphPath *cheminTemp = NULL; //chemins contenant le NumeroTest
	GraphPath *cheminRechercheMax = NULL; //chemins utilise pour rechercher la taille max des chemins
	int tailleMaxPath = -1;
	LigneMatrice *ligneMax = NULL;
	vector<int>::iterator itBase; //iterateur qui parcours les circuits

	while (cheminSuivant != NULL) //tant qu'il reste des circuits( on en a au moins 2 si on arrive a cette methode)
	{
		if (cheminSuivant->Appartient(NumeroTest) == true) {
			cheminAvecNumeroTest->addPath(cheminSuivant->Path, cheminAvecNumeroTest
					/*,cheminSuivant->Path.size() */);
			ret = NumeroTest;
		}
		cheminSuivant = cheminSuivant->Suiv; //on se place sur le chemin suivant
	} //endof while(cheminSuivant->Suiv!=NULL)

	cheminTemp = new GraphPath(*cheminAvecNumeroTest);

	GraphPath *debutTemp = cheminTemp;

	cheminRechercheMax = cheminAvecNumeroTest;
	vector<int> vecteurIntersection; // 0  0  0  0  0  0  0  0  0  0
	vector<int> vecteurTemp; // 0  0  0  0  0  0  0  0  0  0
	vector<int>::iterator it, itRet;
	vector<int>::iterator itSuiv, itEnd, itEndSuiv;

	while (cheminRechercheMax != NULL) //on recherche la taille max des chemins
	{
		if (tailleMaxPath < (int) cheminRechercheMax->Path->size) {
			// on a trouvé un chemin plus long, on remplace celui qu'on avait
			tailleMaxPath = cheminRechercheMax->Path->size;

			ligneMax = (LigneMatrice *) realloc(ligneMax, sizeof(int) * (tailleMaxPath + 1));
			memcpy(ligneMax, cheminRechercheMax->Path, sizeof(int) * (cheminRechercheMax->Path->size + 1));
		}
		cheminRechercheMax = cheminRechercheMax->Suiv;
	}

	// cout << "Ligne de la matrice" << endl;
	for (int i = 0; i < tailleMaxPath; i++) {
		// cout << i << " numéro " << ligneMax->contenu[i] << endl;
		vecteurIntersection.push_back(ligneMax->contenu[i]); //on alloue dans le vecteur la taille max
		vecteurTemp.push_back(0);
	}

	sort(vecteurIntersection.begin(), vecteurIntersection.end()); //on reordonne le vecteur Intersection,

	// cout << "Intersection initiale triée" << endl;
	//for (unsigned int j = 0; j < vecteurIntersection.size(); j++)
	//cout << j << "numéro " << vecteurIntersection[j] << endl;

	while (cheminTemp != NULL) {
		sort(cheminTemp->Path->contenu, cheminTemp->Path->contenu + cheminTemp->Path->size);
		// La fonction set_intersection a besoin que les éléments du path et de l'intersection soient triés.

		/* cout << "Chemin trié" << endl;
		   for(int j=0; j < cheminTemp->Path->size; j++) {
		   cout << j << "numéro " << cheminTemp->Path->contenu[j] << endl;
		   }
		   */

		if (cheminTemp->Path->size > 0) //pour ne pas traiter un path vide
		{
			//cette fonction recherche l'intersection de deux ensembles
			//      itRet=set_intersection (cheminTemp->Path.begin(), cheminTemp->Path.end(), vecteurIntersection.begin(), vecteurIntersection.end(), vecteurTemp.begin());
			itRet = set_intersection(cheminTemp->Path->contenu, cheminTemp->Path->contenu + cheminTemp->Path->size, vecteurIntersection.begin(), vecteurIntersection.end(), vecteurTemp.begin());

			// cout << "Intersection temporaire" << endl;
			for (int j = 0; j < tailleMaxPath; j++) {
				// cout << j << " numéro " << vecteurTemp[j] << endl;
				vecteurIntersection[j] = vecteurTemp[j];
				vecteurTemp[j] = 0;
			}

		} //endof if(cheminTemp->Path.size()>0)

		cheminTemp = cheminTemp->Suiv; //on se place sur le chemin suivant
	}

	//si dans l'intersection il n'y a qu'un numero: c'est lui qu'on choisit a substituer
	//sinon on regarde l'ordre des noeuds dans le chemin le plus long
	// cout << "Intersection des chemins : " << endl;
	int num = 0;
	for (int i = 0; i < (int) vecteurIntersection.size(); i++) {
		if (vecteurIntersection[i] != 0) {
			int j = 0;
			num = vecteurIntersection[i];
			while (num != ligneMax->contenu[j]) {
				j++;
			}
			if (j == 0) {
				j = ligneMax->size - 1; //pour se placer sur le dernier element
			}
			else {
				j--;
			}

			if (cheminTemp->Appartient(ligneMax->contenu[j], &vecteurIntersection)) //on regarde si le noeud precedent fait partie de l'intersection
			{
				ret = ligneMax->contenu[j]; //si oui c'est le premier noeud
			}
		}
	}

	free(ligneMax);
	delete debut;
	delete debutTemp;
	delete debutNumeroTest;

	return ret;
}
