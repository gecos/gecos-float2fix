/*
 * LTIAnalysis.cc
 *
 *  Created on: Jul 9, 2010
 *      Author: nicolas
 */

#include "graph/dfg/datanode/NoeudData.hh"
#include "graph/dfg/Gfd.hh"
#include "graph/dfg/operatornode/NoeudOps.hh"

#include "graph/toolkit/Types.hh"
#include "graph/toolkit/TypeVar.hh"

/**
 *  Return true if the graph is LTI, false otherwise
 *  	\return : boolean
 *
 *
 *  \author Nicolas Simon
 *  \date 09/07/2010
 */
bool Gfd::isLTI() {
	NoeudData *NData;

	for (int i = 0; i < this->getNbOutput(); i++) {
		NData = (NoeudData *) this->getOutputGf(i);
		if (NData->isLTI() == NOT_LTI) {
			return false;
		}
	}
	return true;
}

/**
 *  pass through the information if not a source Node, determine the type of the node otherwise
 *    	\return : TypeLti
 *
 *
 *  \author Nicolas Simon
 *  \date 09/07/2010
 */

TypeLti NoeudData::isLTI() {
	NoeudOps *nodePred;
	TypeLti Type;
	if (!this->isNodeInput() && !this->isNodeConst()) {
		nodePred = dynamic_cast<NoeudOps *> (this->getElementPred(0));
		assert(nodePred != NULL);
		Type = nodePred->isLTI();
	}
	else if (this->isNodeInput()) {
		Type = SIGNAL_LTI;
	}
	else {
		Type = CONST_LTI;
	}
	return Type;
}
