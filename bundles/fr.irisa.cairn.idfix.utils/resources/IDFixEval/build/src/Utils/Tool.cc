/*
 * Tool.cc
 *
 */

#include <string>
#include <cstring>
#include <sstream>
#include <iostream>
#include <fstream>
#include <cerrno>
#include <cstdlib>
#include <cassert>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#ifdef OCTAVE
#include <sys/wait.h>
#endif

#include "utils/Tool.hh"

using namespace std;

std::string get_working_path() {
	char temp[FILENAME_MAX];
	return (getcwd(temp, FILENAME_MAX) ? std::string(temp) : std::string(""));
}

std::string get_idfix_root() {
	string res = getenv("IDFIX_ROOT");
	assert(res != "");
	res = res + "/IDFix/";
	return res;
}

bool ProgParameters::debugMode = false;
bool ProgParameters::timingMode = false;
string ProgParameters::xmlInputFile;
string ProgParameters::dtdInputFile;
string ProgParameters::outputDirectory;
string ProgParameters::outputFilename;
string ProgParameters::logFile;
string ProgParameters::resourcesDirectory;
string ProgParameters::evalType;
string ProgParameters::outputFormat;
string ProgParameters::xmlInputDir;
string ProgParameters::dynProcess;
string ProgParameters::overflowPathFile;
string ProgParameters::simulatedValuesPathFile;
FILE *ProgParameters::pLogFile = NULL;
string ProgParameters::pathLogTiming;
fstream* ProgParameters::logTiming = NULL;
bool ProgParameters::correlationMode = false;
int ProgParameters::nbThread = 1;

int RuntimeParameters::numVar = 0;
bool RuntimeParameters::isNoise;
bool RuntimeParameters::isLTI;
bool RuntimeParameters::isRecursive = false;
bool RuntimeParameters::binaryWithoutZero = false;

void ProgParameters::setXMLInputFile(char *xml_file) {
	xmlInputFile = (const char *) xml_file;
}

void ProgParameters::setDTDInputFile(char *dtd_file) {
	dtdInputFile = (const char *) dtd_file;
}

void ProgParameters::setOutputDirectory(char *output_directory) {
	outputDirectory = (const char *) output_directory;
    if(outputDirectory[outputDirectory.size() - 1] != '/')
        outputDirectory += "/";

	/* On créé le répertoire */
	if (mkdir(output_directory, 0755) == -1) {
		if (errno == 17) {
			std::cout << "*** Notice: Output directory " << outputDirectory << " already exists" << std::endl;
		}
		else {
			std::cout << "*** Error while creating output directory " << outputDirectory << ": " << strerror(errno) << std::endl;
		}
	}

	if (mkdir((outputDirectory + GENERATED_DIR).data(), 0755) == -1) {
		if (errno != 17) {
			assert(false);
		}
	}

	if(mkdir((outputDirectory + GENERATED_DIR + COMPUTEPB_DIR).data(), 0755) == -1){
		if (errno != 17) {
			assert(false);
		}
	}

	if (mkdir((outputDirectory + GRAPHS_DIR).data(), 0755) == -1) {
		if (errno != 17) {
			assert(false);
		}
	}

	if (mkdir((outputDirectory + GRAPHS_DIR + DYN_DIR).data(), 0755) == -1) {
		if (errno != 17) {
			assert(false);
		}
	}

	if (mkdir((outputDirectory + GRAPHS_DIR + NOISE_DIR).data(), 0755) == -1) {
		if (errno != 17) {
			assert(false);
		}
	}

}

void ProgParameters::setOutputFilename(char *filename) {
	outputFilename = (const char *) filename;
}

void ProgParameters::setLogFile(string logfile) {
	logFile = logfile;
}

void ProgParameters::setResourcesDirectory(char *dirname) {
	resourcesDirectory = (const char *) dirname;
    if(resourcesDirectory[resourcesDirectory.size() - 1] != '/')
        resourcesDirectory += "/";
}

void ProgParameters::setEvalType(char *eval_type) {
	evalType = (const char *) eval_type;
}

void ProgParameters::setOutputFormat(char *format) {
	outputFormat = (const char *) format;
}

void ProgParameters::setDebugMode(bool mode) {
	debugMode = mode;
}

void ProgParameters::setTimingMode(bool mode) {
	timingMode = mode;
}

void ProgParameters::setPathLogTiming(string path) {
	pathLogTiming = path;
}

void ProgParameters::setLogTiming(fstream* timingFile){
	logTiming = timingFile;
}

void ProgParameters::setPLogFile(FILE * pFile) {
	pLogFile = pFile;
}

void ProgParameters::setDynProcess(char * dyn_Process) {
	dynProcess = dyn_Process;
}

void ProgParameters::setOverflowPathFile(char * overflow_file) {
	overflowPathFile = overflow_file;
}

void ProgParameters::setSimulatedValuesPathFile(char * simulatedValues) {
	simulatedValuesPathFile = simulatedValues;
}

void ProgParameters::setCorrelationMode(bool mode){
	correlationMode = mode;
}

void ProgParameters::setNbThread(int numberThread){
	nbThread = numberThread;
}

string ProgParameters::getXMLInputFile() {
	return xmlInputFile;
}

string ProgParameters::getDTDInputFile() {
	return dtdInputFile;
}

string ProgParameters::getOutputDirectory() {
	return outputDirectory;
}

string ProgParameters::getOutputGenDirectory() {
	string res = outputDirectory + GENERATED_DIR;
	return res;
}

string ProgParameters::getOutputGenComputePbDirectory() {
	string res = outputDirectory + GENERATED_DIR + COMPUTEPB_DIR;
	return res;
}

string ProgParameters::getOutputGraphDynDirectory() {
	string res = outputDirectory + GRAPHS_DIR + DYN_DIR;
	return res;
}

string ProgParameters::getOutputGraphNoiseDirectory() {
	string res = outputDirectory + GRAPHS_DIR + NOISE_DIR;
	return res;
}

string ProgParameters::getOutputFilename() {
	return outputFilename;
}

string ProgParameters::getLogFile() {
	return logFile;
}

string ProgParameters::getResourcesDirectory() {
	return resourcesDirectory;
}

string ProgParameters::getEvalType() {
	return evalType;
}

string ProgParameters::getOutputFormat() {
	return outputFormat;
}

bool ProgParameters::getDebugMode() {
	return debugMode;
}

bool ProgParameters::getTimingMode() {
	return timingMode;
}

string ProgParameters::getPathLogTiming() {
	return pathLogTiming;
}

fstream* ProgParameters::getLogTiming(){
	return logTiming;
}

FILE *ProgParameters::getPLogFile() {
	return pLogFile;
}

string ProgParameters::getDynProcess() {
	return dynProcess;
}

string ProgParameters::getOverflowPathFile() {
	return overflowPathFile;
}

string ProgParameters::getSimulatedValuesPathFile() {
	return simulatedValuesPathFile;
}

bool ProgParameters::isCorrelationMode(){
	return correlationMode;
}

int ProgParameters::getNbThread(){
	return nbThread;
}


void RuntimeParameters::timing(string information, Chrono* chrono) {
	if(ProgParameters::getTimingMode()){
		if(chrono == NULL){
			*ProgParameters::getLogTiming() << "<listTiming timing=\"-1\" section=\"" << information.data() << "\"/>" <<endl;
		}
		else {
			*ProgParameters::getLogTiming() << "<listTiming timing=\"" << fixed << chrono->sec() + chrono->nsec()*1e-9 << "\" section=\"" << information.data() << "\"/>" <<endl;
		}
	}
}
  //<listTiming timing="0.004" section="AnnotateOperators"/>


string RuntimeParameters::generateVarName(string originalName) {
	std::ostringstream o;
	o << RuntimeParameters::numVar;
	string fin_m_var = "";
	// On verifie que l'expression ne contient que des caracteres a..z A..Z 0..9 ' '
	for (unsigned int i = 0; i < originalName.size(); i++) {
		if (isalnum(originalName[i])) {
			fin_m_var += originalName[i];
		}
	}
	string res = "m_var_" + o.str() + "_" + fin_m_var;
	RuntimeParameters::numVar++;
	return res;
}

bool RuntimeParameters::isProcessingNoise() {
	return isNoise;
}

bool RuntimeParameters::isProcessingDynamic() {
	return !isNoise;
}

void RuntimeParameters::setProcessingType(bool isNoiseProcess){
	isNoise = isNoiseProcess;
}

void RuntimeParameters::setIsLTIGraph(bool lti){
	isLTI = lti;
}

bool RuntimeParameters::isLTIGraph(){
	return isLTI;
}

void RuntimeParameters::setIsRecursiveGraph(bool recursive){
	isRecursive = recursive;
}

bool RuntimeParameters::isRecursiveGraph(){
	return isRecursive;
}

void RuntimeParameters::setIsBinaryWriteWithoutZero(bool withoutZero){
	binaryWithoutZero = withoutZero;
}

bool RuntimeParameters::isBinaryWriteWithoutZero(){
	return binaryWithoutZero;
}

#ifdef OCTAVE
int runOctaveCommand(string path, string command){
   // int iNumProc = 0, iChildiStatus = 0, iStatus = 0, iDeadId = 0;
    pid_t pid;
    int iChildStatus = 0;
	//int iExitFlag = 0;


    string octaveCommand = "octave ";
    octaveCommand.append(" --eval \"addpath('");
    octaveCommand.append(path);
    octaveCommand.append("'),");
    octaveCommand.append(command);
    octaveCommand.append("\"");


	if((pid = vfork()) == 0) {
        int ret = execl("/bin/sh","sh","-c", octaveCommand.c_str(), NULL);
        _exit(ret);
	}
	else if (pid == -1){
        trace_error("Fork problem during octave using\n");
        exit(-1);
	}
	else{
        waitpid(pid, &iChildStatus, 0);
        if(!WIFEXITED(iChildStatus)){
            trace_error("Execution of octave failed");
            exit(-1);
        }
	}
	return 0;
}
#endif
