/*
 * RecurrentEquationGeneration.hh
 *
 *  Created on: Mar 18, 2015
 *      Author: Nicolas Simon
 */

#ifndef NOISE_RECURRENTEQUATIONGENERATION_HH_
#define NOISE_RECURRENTEQUATIONGENERATION_HH_

#include <utils/linearfunction/LinearFunction.hh>

void RecurrentEquationGeneration(LinearFunction *linearFunction);

#endif /* NOISE_RECURRENTEQUATIONGENERATION_HH_ */
