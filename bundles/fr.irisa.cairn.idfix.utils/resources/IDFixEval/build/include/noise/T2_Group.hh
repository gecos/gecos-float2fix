#ifndef __T2_Group_HH__
#define __T2_Group_HH__

#include "graph/dfg/Gfd.hh"
#include "graph/tfg/GFT.hh"

Gft* T2_SystemPseudoTransferFunctionDetermination(Gfd* gfd);
void T21_CycleDismantling(Gfd* gfd);
Gfl* T22_ReccurentEquationDetermination(Gfd* gfd);
void T23_PartialPseudoTransferFunctionDetermination(Gfl* gfl);
Gft* T24_GlobalPseudoTransferFunctionDetermination(Gfl* gfl);

#endif
