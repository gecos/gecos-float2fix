#ifndef __NOISEFUNCTIONGENERATION_HH__
#define __NOISEFUNCTIONGENERATION_HH__

#include "graph/dfg/Gfd.hh"
#include "graph/tfg/GFT.hh"

void NoiseFunctionGeneration(Gft* gft, Gfd* gfd);

#endif
