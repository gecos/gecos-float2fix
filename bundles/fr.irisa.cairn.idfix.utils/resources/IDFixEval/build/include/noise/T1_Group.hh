#ifndef __T1_Group_HH__
#define __T1_Group_HH__

#include "graph/dfg/Gfd.hh"

void T1_SystemNoiseModelDetermination(Gfd* gfd);
void T11_NoiseSourceInsertion(Gfd* gfd);
void T12_NoiseDataAndOperatorModelInsertion(Gfd* gfd);

#endif
