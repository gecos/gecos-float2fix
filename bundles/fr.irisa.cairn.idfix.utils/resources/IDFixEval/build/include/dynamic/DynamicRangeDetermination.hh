#ifndef __DYNAMIC_HH__
#define __DYNAMIC_HH__

#include "graph/dfg/Gfd.hh"
#include "graph/tfg/GFT.hh"

namespace dynamic {
	void DynamicRangeDetermination(Gfd* gfd);
	void InitializeDataDynamicToNaN(Gfd* gfd);

	Gfl* T1_SystemPseudoFTDetermination(Gfd* gfd);
	void T2_DynamicRangeExpressionDetermination(Gfl* gfl);
	void T3_DynamicTransferToGFD(Gfl* Gfl, Gfd* gfdDismantled, Gfd* gfd);

	void GenerateDynamicFixedPointSpecification(Gfd* gfd);
}
#endif
