/*
 * XmlDyn.hh
 *
 *  Created on: Jul 20, 2010
 *      Author: nicolas
 */

#ifndef XMLDYN_HH_
#define XMLDYN_HH_
#include <iostream>
#include <string>
#include <list>

#include "graph/lfg/GFL.hh"


using namespace std;

class XmlDynNode;
class XmlDynAtt;

class XmlDyn {
private:
protected:
	list<XmlDynNode*> ListNode;

public:
	XmlDyn();
	~XmlDyn();

	XmlDynNode* NoeudCourant;

	void addNode(char* Id);
	list<XmlDynNode*> GetListNode() {
		return ListNode;
	}
	void addAtt(char* Key, char* Value);

	void XmlDyn2GFL(Gfl* Graph);
	void Affiche();
};

class XmlDynAtt {
private:
protected:
	/** Clee de l'attribut <attr key="xxx" value="xxx" />   */
	string Key;

	/** Valeur de la clee de l'attribut  <attr key="xxx" value="xxx" />  */
	double Value;

public:
	// ======================================= Constructeur - Destructeur

	/** Constructeur */
	XmlDynAtt(char* Key, char* Value);
	/** Destructeur  */
	~XmlDynAtt();

	// ======================================= Methode utilisant Key

	string getKey() {
		return Key;
	} /// Return key dans le fichier XML, <attr key="xxx" value="xxx" />*/

	// ======================================= Methode utilisant Value

	double getValue() {
		return Value;
	} /// Return value dans le fichier XML, <attr key="xxx" value="xxx" />*/

};

class XmlDynNode {

private:
protected:
	/** Nom du noeud  */
	string* Nom;
	/** Numero du noeud  */
	int Num;

	list<XmlDynAtt*> ListAtt;

public:
	// ======================================= Constructeur - Destructeur

	/** Constructeur */
	XmlDynNode(char* Id);
	/** Destructeur  */
	~XmlDynNode();

	// ======================================= Methode utilisant Nom

	string* getNom() {
		return Nom;
	} ///  return le name  <node name="xxx" id="xxx">*/

	// ======================================= Methode utilisant Num

	int getNum() {
		return Num;
	} ///  return l'id sous forme d'entier (int) <node name="xxx" id="xxx">*/

	list<XmlDynAtt*> GetListAtt() {
		return ListAtt;
	}

	void addAtt(char *Key, char *Value);
	void Affiche();
};

#endif /* XMLDYN_HH_ */
