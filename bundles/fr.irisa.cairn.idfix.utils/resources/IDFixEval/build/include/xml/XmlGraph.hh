/*!
 *  \author Daniel Menard, Jean-Charles Naud
 *  \date   22.2.2008
 *  \version 1.0
 */

#ifndef CLASS_GRAPH_XML
#define CLASS_GRAPH_XML

// === Bibliothèques standard
#include "graph/toolkit/Types.hh"
#include "utils/graph/Block.hh"
#include "utils/Tool.hh"
#include <iostream>
#include <list>
#include <map>
#include <string>

// === Bibliothèques non standard

// === Forward Declaration
class XmlGraph;
class XmlNode;
class XmlAtt;
class XmlEdge;
class XmlElt;
class Gfd;
class NoeudGraph;

// === Namespaces
using namespace std;

//------------------------------------------------ XmlElt ------------------------------------------------
/*!
 *  \class XmlElt
 *  \ingroup	InterfaceXML
 *  \brief Classe de base definissant un element du graphe au format XML
 */
class XmlElt {
private:
	XmlElt(); // interdit à l'utilisation !

protected:
	XmlGraph& ownerGraph; /// graphe auquel cet élément appartient */
	/** Liste des attributs associes au noeud  */
	list<XmlAtt> ListeAtt;

public:
	// ======================================= Constructeur - Destructeur

	XmlElt(XmlGraph& in); // Constructeur
	~XmlElt(); // Destructeur


	// === XmlGraph.hh
	void addAttributs(char* Key, char* Value); /// ajout d'un XmlAtt dans la ListeAtt
	list<XmlAtt>& getListeAtt() {
		return ListeAtt;
	}

};

//------------------------------------------------ XmlAtt ------------------------------------------------
/*!
 *  \class XmlAtt
 *  \ingroup	InterfaceXML
 *  \brief Classe d�finissant les attributs d'un noeud (#XmlNode) ou d'un arc (#XmlEdge) present dans le
 *			graphe au format XML
 *
 *  \details Syntaxe associee dans le fichier XML
 *			- <attr key="xxx" value="xxx" />
 */

class XmlAtt {
private:
protected:
	string key; /// Clee de l'attribut <attr key="xxx" value="xxx" />
	string value; /// Valeur de la clee de l'attribut  <attr key="xxx" value="xxx" />

public:
	// ======================================= Constructeur - Destructeur
	XmlAtt(string key, string value); /// Constructeur
	~XmlAtt(); /// Destructeur


	// ======================================= Methodes
	void setKey(string key) {
		this->key = key;
	}
	string getKey() {
		return key;
	} /// Return key dans le fichier XML, <attr key="xxx" value="xxx" />
	void setValue(string value) {
		this->value = value;
	}
	string getValue() {
		return value;
	} /// Return value dans le fichier XML, <attr key="xxx" value="xxx" />


};

//------------------------------------------------ XmlNode ------------------------------------------------
/*!
 *  \class XmlNode
 *  \ingroup	InterfaceXML
 *  \brief Classe derivee de #XmlElt et definissant un noeud du graphe au format XML
 *
 *  \details Syntaxe associee dans le fichier XML
 *			- <node name="xxx" id="xxx"> xxxx  </node>
 *
 **/

class XmlNode: public XmlElt {
private:
	XmlNode(); // interdit
protected:
	//xxx
	string* Nom; /// Nom du noeud
	int Num; /// Numero du noeud
public:
	// ======================================= Constructeur - Destructeur
	XmlNode(XmlGraph& in, char* Nom, char* Id); /// Constructeur
	~XmlNode(); /// Destructeur

	// ======================================= Methodes
	void setNom(string* Nom) {
		this->Nom = Nom;
	}
	string* getNom() {
		return Nom;
	} ///  return le name  <node name="xxx" id="xxx">

	void setNum(int Num) {
		this->Num = Num;
	}
	int getNum() {
		return Num;
	} ///  return l'id sous forme d'entier (int) <node name="xxx" id="xxx">

	/* int		NumInput; /// Numéro d'entrée sur l'opération suivante
	 void		setNumero(int NumInput){this->NumInput = NumInput;}
	 int		getNumero(){return NumInput;}	///  return le numéro d'entrée sur l'opération suivante sous forme d'entier (int) <node name="xxx" id="xxx">
	 */
	// === XmlGraph.cc
	NoeudGraph* createNode(bool type);
};

//------------------------------------------------ XmlEdge ------------------------------------------------
/*!
 *  \class XmlEdge
 *  \ingroup	InterfaceXML
 *  \brief Classe derivee de #XmlElt et definissant un arc du graphe au format XML
 *
 *  \details Syntaxe associée dans le fichier XML
 *			- <edge id_pred="xx" id_succ="xx" >  xx </edge>
 */

class XmlEdge: public XmlElt {
private:
	XmlEdge(); // interdit !
protected:
	int Depart; /// Numero du noeud de depart de l'arc
	int Arrivee; /// Numero du noeud d'arrivee de l'arc


public:
	// ======================================= Constructeur - Destructeur
	XmlEdge(XmlGraph& in, char* Depart, char* Arrivee); /// Constructeur
	~XmlEdge(); /// Destructeur

	// ======================================= Methode utilisant Depart
	int getDepart() {
		return Depart;
	}

	// ======================================= Methode utilisant Arrivee
	int getArrivee() {
		return Arrivee;
	}
};

//------------------------------------------------ XmlGraph ------------------------------------------------
/*!
 *  \class XmlGraph
 *  \ingroup	InterfaceXML
 *  \brief Classe definissant un graphe au format XML
 *
 *  \details Syntaxe associee dans le fichier XML
 *			   <graph> xxxxx  </graph>
 */
class XmlGraph {
private:
	Gfd* m_output; /// Graph that will be filled in
protected:
	list<XmlNode*> ListeNode; /// Liste des noeuds presents dans le fichier XML
	list<XmlEdge*> ListeEdge; /// Liste des arcs presents dans le fichier XML

	XmlNode* NoeudCourant; /// Noeud  en cours de traitement
	XmlEdge* EdgeCourant; /// Arc en cours de traitement

public:
	// ======================================= Constructeur - Destructeur
	XmlGraph(Gfd* target);
	~XmlGraph();

	// ======================================= Methodes d'ajouts Node, Edge, Attributs
	void addXmlNode(char * Nom, char * Id);
	void addXmlEdge(char * ValDepart, char * ValArrivee);
	void addAttributs(char* Key, char* Value, TypeNodeXml TypeDeNoeud); // TypeNodeXml = (EDGE, NODE )
	void addBlock(int id);

	Block& getBlock(int id);

	void xmlGraph2GFD(bool noiseEval);
};

#endif
