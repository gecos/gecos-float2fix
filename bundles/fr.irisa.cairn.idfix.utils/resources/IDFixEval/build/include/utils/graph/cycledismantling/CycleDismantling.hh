#ifndef __CYCLEDISMANTLING_HH__
#define __CYCLEDISMANTLING_HH__

#include "graph/dfg/Gfd.hh"
#include "graph/lfg/GFL.hh"
#include "graph/Graph.hh"

// Global
bool CycleDetection(Graph* graph);

// GFD
void CycleDismantling(Gfd* gfd);

// GFL
void CycleReduction(Gfl* gfl);

#endif
