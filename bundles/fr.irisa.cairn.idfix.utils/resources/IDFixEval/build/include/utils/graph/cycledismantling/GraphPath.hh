/**
 *  \author Daniel Menard, Jean-Charles Naud
 *  \date   03.06.2002
 *  \class GraphPath
 *  \brief Classe definissant les chemins du graphe
 *
 *  This class defines a path in a graph. A path is an ordered list of nodes, each having an edge linking to the next one,
 *  and going from a node to another one.
 *  Path can be used as a way to travel through the graph, for example when looking for circuits, or when computing the
 *  transfert function between two nodes (which is the product of all the partial TF on the path).
 */

#ifndef _GRAPHPATH_HH_
#define _GRAPHPATH_HH_

// === Bibliothèques standard
#include <vector>
#include <list>
#include <vector>
#include <map>

#include "graph/tfg/EdgeGFT.hh"
#include "graph/toolkit/AdjacentMatrix.hh"
#include "graph/toolkit/Types.hh"
#include "utils/Tool.hh"



class Graph;

// === Namespaces
using namespace std;

/**
 * 	\class GraphPath
 *	\brief Classe definissant les chemins du graphe.
 */
class GraphPath {
private:
	LigneMatrice* Path;
	std::list<EdgeGFT*> m_Edges; /// Blocks traversés par ce chemin

public:

	GraphPath *Suiv;
	// ======================================= Constructeurs - Destructeurs
	GraphPath();
	GraphPath(const GraphPath& copie);
	GraphPath(LigneMatrice* ligne);
	~GraphPath();

	// ======================================= Methodes
	//static inline bool Appartient(unsigned int x, vector<int>* u); /// Regarde dans une ligne si le numero est present

	GraphPath* getPath(int indice); // Rend le path accocié a indice dans le GraphPath
	/**
	 * Methode qui regarde si les deux numeros sont contenus dans une seule ligne
	 *  \param  x1 	numero recherche
	 *  \param  x2 	numero recherche
	 *  \return bool 			true si trouve
	 *  by Loic Cloatre creation le 10/02/2009
	 */
	bool CircuitContientDeuxNoeuds(unsigned int x1, unsigned int x2);

	bool CircuitSansPointCommunAvec(GraphPath * ListePath); /// Methode qui renvoie true si les deux circuits n'ont aucun noeud en commun*/
	bool CircuitAvecUnPointCommun(GraphPath * ListePath); /// Methode qui renvoie true si les deux circuits ont un seul noeud en commun*/

	// === GfdPath.cc
	//bool Appartient(int x) const;
	void displayPath(); /// Affiche tous les circuits trouves
	bool isEmpty(); /// Test si un circuit a été trouvé
	//int pathExtension(AdjacentMatrix *MatriceG, LigneMatrice * VecteurP, AdjacentMatrix * MatriceH, int k, int NbElements); /// Renvoi l indice du noeud permettant d etendre le chemin
	GraphPath *addPath(LigneMatrice *VecteurP, GraphPath * ListePath); /// Ajoute un nouveau circuit
	int histogrammeOccurenceNoeudDansCircuits(vector<int>* numeroNoeud, vector<int> *nbOccurence); /// Methode qui a partir de la liste de circuits va compter combien de fois intervient chaque noeud
	bool incrementeCompteurOccurenceNoeud(int numeroDuNoeud, vector<int>* numeroNoeud, vector<int> *nbOccurence);/// Methode qui incremente le compteur associe au numero*/
	int searchFirstNodeCommun(int NumeroTest); /// Methode qui va regarder si le choix du noeud ne tombe pas sur un cas ou les circuits ne sont pas enumeres dans l'ordre*/

	//======================================== Methodes utilisees pour generer le fichier matlab ParamFT.m
	string getExpressionFTGlobalePourMatlab(int numeroSortie); /// Methode qui à partir d'un circuit donne l'expression de la FT: [2 3 4] donne Hg(3,2)*Hg(4,3) */

	//======================================== Methodes utilisees pour generer le fichier matlab ParamFTNonLti.m
	string getExpressionFTGlobalePourMatlabNonLti(int numeroSortie); /// Methode qui à partir d'un circuit donne l'expression de la FT: [2 3 4] donne Hg(3,2)*Hg(4,3) */
	void addElemPath(const NoeudGraph* noeud, EdgeGFT* edge);
	
	int sizeGraphPath(); // Renvoi la taille du graphPath
	map<int, list<int> > recherchePathAvecNoeudCommun(); //  Permet a partir d'un GraphPath de determiner quels path ont des noeuds communs et de les classer
	vector<list<NoeudGraph*> > rechercheNoeudCommunDansPath(map<int , list<int> > listPathCommun, Graph* graph); // Rend tout les noeuds communs a partir de l'ensemble des path dépendant (qui ont aux moins un noeud commun entre eux) fourni

	int pathSize() const {
		return Path->size;
	}
	int pathItem(int i) const {
		return Path->contenu[i];
	}

	bool Appartient(int element) const; // Rend vrai si l'element est présent dans le graphPath en cours, faux sinon
	bool Appartient(int element, vector<int>* u);

	// === T21_CycleDismantling
    GraphPath *searchPathCircuits(AdjacentMatrix* MatriceG, int NbLignes, int DernierElement); /// Methode qui a partir de la matrice adjacente va retouver tous les circuit

};

#endif // _GRAPHPATH_HH_
