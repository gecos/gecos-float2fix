/**
 *  \file Block.hh
 *  \author Adrien Destugues
 *  \date   08.10.2010
 *  \brief Class representing a code block.
 */

#ifndef _BLOCK_HH_
#define _BLOCK_HH_

// === Bibliothèques non standard
#include <queue>
#include <stdlib.h>

/**
 * \class Block
 * \brief Classe définisant les Block
 *
 */
class Block {
private:
	int m_id; ///< ID du block
	Block* m_parent; ///< Block parent
	float m_proba; ///< Probability of execution of this block within the parent Block

public:
	// ======================================= Constructeur - Destructeur
	Block(int idBlock, Block* parent = NULL); /// Create a block with the given identifier

	// =================== Methodes
	void setProbability(float m_proba); ///< Ajout de la probabilité
	float getProbability() {
		return m_proba;
	} ///< Retourne la probabilité associé au block

	void setParent(Block* p) {
		m_parent = p;
	} ///< Ajout le parent du block

	int getID() const {
		return m_id;
	} ///< Retourne le ID di block 
	std::queue<int> getFQID(); ///< Get the fully qualified ID, ie the path from the root of the block tree.

	bool operator==(const Block& other) {
		return getID() == other.getID();
	}///< Vrai si l'ID du block this est égale à l'ID de other
};

#endif // _BLOCK_HH_
