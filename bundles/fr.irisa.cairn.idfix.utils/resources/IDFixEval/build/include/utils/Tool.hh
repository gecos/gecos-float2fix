#ifndef TOOL
#define TOOL

#include <string>
#include <cstdio>
#include <cassert>
#include <algorithm>
#include <stdlib.h>
#include "utils/Chrono.hh"

/* Generated (arbitrary names) */
#define GENERATED_DIR  "Generated/"
#define COMPUTEPB_DIR "ComputePb/"
#define GRAPHS_DIR     "Graphs/"
#define DYN_DIR        "Dynamic/"
#define NOISE_DIR      "Noise/"

/* Input: must correspond to the name in the repository */
#define RESOURCES_DIR  "Resources/"
#define SIMULATION_DIR "simulation/"

#define NAME_NOISE_NODE "Br"
#define TOOL_FILE_DEBUG __FILE__<<' '<<__LINE__<<':'

/* Generated (arbitrary names) */
#define PARAMFT_FILENAME        "ParamFT.m"
#define PARAMFTNONLTI_FILENAME  "ParamFTNonLti.m"
#define CONSTANT_VAR_FILENAME   "ConstantVariable.txt"
#define DYN_SAVE_FILENAME       "DynSave.txt"
#define FILE_AC_FIXED_FILENAME  "FileAcFixed.cc"
#define FP_SPECIF_FILENAME      "fp_specification.xml"
#define LOG_TIMING_FILENAME     "timing_IDFixEval.xml"
#define PARAM_DYN_FT_FILENAME   "ParamDynFT.m"
#define REF_FLOAT_FILENAME      "typeVarReferenceFloat.txt"
#define NOISE_KLM_FILEMANE		"ValeurHgDb.bin"
#define DYNAMIC_ENGINE_FILE_NAME "DynNode.xml"
#define OUTPUT_FILENAME         "ComputePb"
#define LOG_FILENAME            "output.log"

/* Input: must correspond to the name in the repository */
#define DTD_FILENAME "sfg.dtd"

#define TOOL_XML_PARSING	"\t Step 0 : Xml parsing..................................................\n"
#define	STEP_XML_VALIDATION		"\t . XML file validation started\n"
#define	STEP_XML_VALIDATED		"\t . XML file %s has been validated\n"
#define	STEP_XML_CONVERT		"\t . XML file conversion started\n"
#define	STEP_XML_END			"\t . SFG has been created from the XML description\n"
#define	STEP_XML_ANALYSE		"\t . XML file analyse started\n"

#define TOOL_STEP_1        "INTERMEDIATE REPRESENTATION GENERATION MODULE _________________________________\n"
#define TOOL_STEP_1_1      "\tStep 1.1 : SUIF Intermediate Representation Reading ...................\n"
#define TOOL_STEP_1_2      "\t Step 1.2 : Application CDFG Generation ................................\n"
#define TOOL_STEP_1_3      "\t Step 1.3 : Application SFG Generation .................................\n"

#define TOOL_STEP_2        "ACCURACY EVALUATION MODULE _____________________________________________________\n"
#define TOOL_STEP_2_0      "\t Step 1.0 : Module Initialisation ......................................\n"
#define TOOL_STEP_2_1      "\t Step 1.1 : Noise Level SFG Generation .................................\n"
#define TOOL_STEP_2_1_1    "\t Step 1.1.1 : Noise source insertion . .................................\n"
#define TOOL_STEP_2_1_2    "\t Step 1.1.2 : Data and operation model insertion........................\n"
#define TOOL_STEP_2_2      "\t Step 1.2 : Transfert Function Determination Module ....................\n"
#define TOOL_STEP_2_2_1    "\t Step 1.2.1 : Cycle Dismantling ........................................\n"
#define TOOL_STEP_2_2_1_1  "\t Step 1.2.1.1 : Cycle Detection ........................................\n"
#define TOOL_STEP_2_2_1_2  "\t Step 1.2.1.2 : Cycle Enumeration ......................................\n"
#define TOOL_STEP_2_2_1_3  "\t Step 1.2.1.3 : Directed Acyclic Graphs Generation Processing ..........\n"
#define TOOL_STEP_2_2_2    "\t Step 1.2.2 : Linear Function Determination ............................\n"
#define TOOL_STEP_2_2_3    "\t Step 1.2.3 : Partial Transfer Function Determination ..................\n"
#define TOOL_STEP_2_2_4    "\t Step 1.2.4 : Global Transfer Function Determination ...................\n"
#define TOOL_STEP_2_3      "\t Step 1.3 : Noise Power Expression Determination .......................\n"

#define TOOL_STEP_3         "DYNAMIC EVALUATION MODULE _______________________________________________________\n"
#define TOOL_STEP_3_1       "\t Step 1 : Transfert Function Determination Module ....................\n"
#define TOOL_STEP_3_1_1     "\t Step 1.1 : Cycle Dismantling ........................................\n"
#define TOOL_STEP_3_1_1_1   "\t Step 1.1.1 : Cycle Detection ........................................\n"
#define TOOL_STEP_3_1_1_2   "\t Step 1.1.2 : Cycle Enumeration ......................................\n"
#define TOOL_STEP_3_1_1_3   "\t Step 1.1.3 : Directed Acyclic Graphs Generation Processing ..........\n"
#define TOOL_STEP_3_1_2     "\t Step 1.2 : LTI Checking .............................................\n"
#define TOOL_STEP_3_1_3     "\t Step 1.3 : Linear Function Determination ............................\n"
#define TOOL_STEP_3_1_4     "\t Step 1.4 : Partial Transfer Function Determination ..................\n"
#define TOOL_STEP_3_1_5     "\t Step 1.5 : Global Transfer Function Determination ...................\n"

#define TOOL_STEP_3_2       "\t Step 2 : Dynamic Expression Determination ...........................\n"
#define TOOL_STEP_3_2_1     "\t Step 2.1 : Engine File Creation......................................\n"
#define TOOL_STEP_3_2_2     "\t Step 2.2 : Engine Execution..........................................\n"

#define TOOL_STEP_3_3       "\t Step 3 : Dynamic Propagation ........................................\n"
#define TOOL_STEP_3_3_1_rec "\t Step 3.1 : Engine Result Transmition.................................\n"
#define TOOL_STEP_3_3_2_rec "\t Step 3.2 : Dynamic Propagation ......................................\n"
#define TOOL_STEP_3_3_1     "\t Step 3.1 : Dynamic Propagation ......................................\n"
#define TOOL_STEP_3_4       "\t Step 3.3 : Accumulation uniformization ..............................\n"
#define TOOL_STEP_3_5       "\t Step 3.4 : Data Dynamic Update  .....................................\n"
#define TOOL_STEP_3_6       "\t Step 3.5 : SFG with Dynamic backup in file : \n"

#define TOOL_STEP_4      "BINARY-POINT POSITION DETERMINATION MODULE ______________________________________\n"
#define TOOL_STEP_5      "DATA TYPE DETERMINATION MODULE __________________________________________________\n"
#define TOOL_STEP_6      "FIXED-POINT OPTIMIZATION MODULE _________________________________________________\n"
#define TOOL_STEP_7      "OUTPUT GENERATION MODULE ________________________________________________________\n"

/* variables pour le generation de paramFT et ComputePb*/
#define TAB_NUMERO_SORTIE    "TabNumeroSortie"
#define NOMBRE_SORTIE        "nombreSortie"
#define NOMBRE_SOURCE_BRUIT  "nombreSourceBruit"
#define NOMBRE_NOEUD         "nombreNoeud"

#define NOMBRE_SOURCE        "nombreSource"
#define TAB_NUMERO_ENTREE    "TabNumeroEntree"
#define TAB_NUMERO_VARINT    "TabNumeroVatInt"

/* Outils de debug et affichage */
#define __STR__(X) #X
#define __STRING__(X) __STR__(X)
#define SAVE_GRAPH DEBUG

#define trace_output(...)       \
   ({                                     \
    fprintf(ProgParameters::getPLogFile(),__VA_ARGS__);        \
    printf(__VA_ARGS__);                  \
    })

#define trace_debug(...)               \
   ({                                            \
    if (ProgParameters::getDebugMode()){                             \
    fprintf(ProgParameters::getPLogFile(),"* DEBUG "__FILE__":"__STRING__(__LINE__)": "__VA_ARGS__);   \
    printf("* \x1B[32mDEBUG\x1B[0m in "__FILE__":"__STRING__(__LINE__)": " __VA_ARGS__);             \
    }                                            \
    })

#define trace_info(...)               \
   ({                                            \
    fprintf(ProgParameters::getPLogFile(),"* INFO "__FILE__ ":"__STRING__(__LINE__)": "__VA_ARGS__);   \
    printf("* \x1B[32mINFO\x1B[0m "__FILE__ ":"__STRING__(__LINE__)": " __VA_ARGS__);             \
    })

#define trace_notice(...)               \
   ({                                            \
    fprintf(ProgParameters::getPLogFile(),"* NOTICE "__FILE__ ":"__STRING__(__LINE__)": "__VA_ARGS__);   \
    printf("* \x1B[32mNOTICE\x1B[0m "__FILE__ ":"__STRING__(__LINE__)": "__VA_ARGS__);             \
    })

#define trace_warn(...)               \
   ({                                            \
    fprintf(ProgParameters::getPLogFile(),"* WARNING "__FILE__ ":"__STRING__(__LINE__)": "__VA_ARGS__);   \
    printf("* \x1B[33mWARNING\x1B[0m "__FILE__ ":"__STRING__(__LINE__)": " __VA_ARGS__);             \
    })

#define trace_error(...)               \
   ({                                            \
    fprintf(ProgParameters::getPLogFile(),"* ERROR "__FILE__ ":"__STRING__(__LINE__)": "__VA_ARGS__);   \
    printf("* \x1B[31mERROR\x1B[0m "__FILE__ ":"__STRING__(__LINE__)": " __VA_ARGS__);             \
    })

#define trace_crit(...)               \
   ({                                            \
    fprintf(ProgParameters::getPLogFile(),"* CRITICAL "__FILE__ ":"__STRING__(__LINE__)": "__VA_ARGS__);   \
    printf("* \x1B[31mCRITICAL\x1B[0m "__FILE__ ":"__STRING__(__LINE__)": " __VA_ARGS__);             \
    })

#define trace_alert(...)               \
   ({                                            \
    fprintf(ProgParameters::getPLogFile(),"* ALERT "__FILE__ ":"__STRING__(__LINE__)": "__VA_ARGS__);   \
    printf("* \x1B[31mALERT\x1B[0m "__FILE__ ":"__STRING__(__LINE__)": " __VA_ARGS__);             \
    })

#define trace_fatal(...)               \
   ({                                            \
    fprintf(ProgParameters::getPLogFile(),"* FATAL "__FILE__ ":"__STRING__(__LINE__)": "__VA_ARGS__);   \
    printf("* \x1B[31mFATAL\x1B[0m "__FILE__ ":"__STRING__(__LINE__)": " __VA_ARGS__);             \
    })

std::string get_idfix_root();

using namespace std;
/**
 * \class ProgParameters
 * \brief Definition de la classe ProgParameters
 *
 * Définie les paramètres globaux du programme
 */
class ProgParameters {

	static bool debugMode;
	static bool timingMode;
	static string pathLogTiming;
	static fstream* logTiming;
	static string xmlInputFile;
	static string dtdInputFile;
	static string outputDirectory;
	static string outputFilename;
	static string logFile;
	static string resourcesDirectory;
	static string evalType;
	static string outputFormat;
	static string xmlInputDir;
	static FILE* pLogFile;
	static string dynProcess;
	static string overflowPathFile;
	static string simulatedValuesPathFile;
	static bool correlationMode;
	static int nbThread;

public:

	ProgParameters() {
	}

	static void setXMLInputFile(char * xml_file);
	static void setDTDInputFile(char * dtd_file);
	static void setOutputDirectory(char * output_dir);
	static void setOutputFilename(char * complete_output_filename);
	static void setLogFile(string logfile);
	static void setResourcesDirectory(char * matlab_dir);
	static void setEvalType(char * eval_type);
	static void setOutputFormat(char * output_format);
	static void setDebugMode(bool mode);
	static void setTimingMode(bool mode);
	static void setPathLogTiming(string path);
	static void setLogTiming(fstream* timingFile);
	static void setPLogFile(FILE* pFile);
	static void setDynProcess(char * dyn_Process);
	static void setOverflowPathFile(char* overflow_file);
	static void setSimulatedValuesPathFile(char* simulatedValues);
	static void setCorrelationMode(bool mode);
	static void setNbThread(int numberThread);

	static string getXMLInputFile();
	static string getDTDInputFile();
	static string getOutputDirectory();
	static string getOutputGenDirectory();
	static string getOutputGenComputePbDirectory();
	static string getOutputGraphDynDirectory();
	static string getOutputGraphNoiseDirectory();
	static string getOutputFilename();
	static string getLogFile();
	static string getResourcesDirectory();
	static string getEvalType();
	static string getOutputFormat();
	static bool getDebugMode();
	static bool getTimingMode();
	static string getPathLogTiming();
	static fstream* getLogTiming();
	static FILE * getPLogFile();
	static string getDynProcess();
	static string getOverflowPathFile();
	static string getSimulatedValuesPathFile();
	static bool isCorrelationMode();
	static int getNbThread();
};

class RuntimeParameters {
	static int numVar;
	static bool isNoise;
	static bool isLTI;
	static bool isRecursive;
	static bool binaryWithoutZero;

public:
	static void timing(string information , Chrono* chrono = NULL);
	static string generateVarName(string endName);
	static bool isProcessingNoise();
	static bool isProcessingDynamic();
	static void setProcessingType(bool isNoiseProcess);
	static void setIsLTIGraph(bool lti);
	static bool isLTIGraph();
	static void setIsRecursiveGraph(bool recursive);
	static bool isRecursiveGraph();
	static void setIsBinaryWriteWithoutZero(bool withoutZero);
	static bool isBinaryWriteWithoutZero();
};

#ifdef OCTAVE
int runOctaveCommand(string path, string command);
#endif

#endif // TOOL
