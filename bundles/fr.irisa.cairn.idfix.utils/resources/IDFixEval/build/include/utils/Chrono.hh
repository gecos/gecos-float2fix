#ifndef CHRONO_HH_
#define CHRONO_HH_

#include <stdexcept>
#include <time.h>
#include <iostream>
#include <cstdio>
#include <stdio.h>
#include <stdlib.h>
#include <string>

using namespace std;

struct Clock {
	enum Type {
		Thread, Process, Mono, Real
	};
};

//! Execution time chronometer.
class Chrono {
	private:
		bool stopped;
		timespec start_time; /**< Chronometer's start time. */
		timespec total_time; /**< Cumulative chronometer's running time. */
		int clock_id;        /**< Chronometer underlying clock id. */

	public:
		Chrono();
		Chrono(Clock::Type);

		void start() throw(std::runtime_error);

		void stop() throw(std::runtime_error);

		void reset();

		double nsec() const throw (std::logic_error);

		double sec() const throw (std::logic_error);
};


#endif /* CHRONOMETER_HPP_ */
