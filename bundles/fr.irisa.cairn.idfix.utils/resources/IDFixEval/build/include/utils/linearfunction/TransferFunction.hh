
/**
 *  \author Nicolas Simon
 *  \date   17/11/11
 *  \version 1.0
 *  \class FonctionTransfert
 */

#ifndef FONCTION_TRANSFERT
#define FONCTION_TRANSFERT

#include <utils/linearfunction/PseudoLinearFunction.hh>
#include <utils/Tool.hh>
#include <cstring>
#include <iostream>
#include <set>
#include <string>
#include <sstream>
#include <vector>
#include <cassert>


class TransferFunction{

private:
	PseudoLinearFunction* m_denominateur; ///< Représente le denominateur de la fonction de transfert
	PseudoLinearFunction* m_numerateur; ///< Représente le numerateur de la fonction de transfert

public:

	TransferFunction(); 
	TransferFunction(PseudoLinearFunction* num, PseudoLinearFunction* den); 
	TransferFunction(PseudoLinearFunction* num);
	TransferFunction(const TransferFunction& other);
	~TransferFunction();
	
	void setDenominateur(PseudoLinearFunction* fct){
		this->m_denominateur = fct;
	} ///< Setteur du dénominateur
	PseudoLinearFunction* getDenominateur(){
		return m_denominateur;
	} ///< Getteur du dénominateur

	void setNumerateur(PseudoLinearFunction* fct){
		this->m_numerateur = fct;
	} ///< Setteur du numerateur
	PseudoLinearFunction* getNumerateur(){
		return m_numerateur;
	} ///< Getteur du numerateur

	string toString() const; ///< Rend en format string la fonction de transfert
	string toStringNum() const; ///< Rend le numerateur en format string
	string toStringDen() const; ///< Rend le denominateur en format string
	string getMatlabExpression(int numEdge) const; ///< Rend la fonction de transfert dans le format adéquat pour les traitements Matlab


};

#endif
