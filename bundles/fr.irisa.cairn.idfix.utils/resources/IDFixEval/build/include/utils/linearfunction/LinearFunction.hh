
/**
 *  \author Nicolas Simon
 *  \date   09/11/11
 *  \version 1.0
 *  \class FonctionLinaiere
 *	\breif Classe correspondant representant chaque element Z d'une fonction linéaire (exposant, coefficient)
 */

#ifndef LINEAR_FUNCTION_HH__
#define LINEAR_FUNCTION_HH__

#include <cstring>
#include <iostream>
#include <set>
#include <string>
#include <sstream>
#include <vector>
#include <cassert>

#include "utils/linearfunction/PseudoLinearFunction.hh"

class NoeudGFD;

class LinearFunction{
private:
	vector<PseudoLinearFunction*> m_vectorPseudoFctLineaire; ///< Conteneur représentant les sous parties de la fonction lineaire
	NoeudGFD* m_node;	///< Noeud associé a la fonction lineaire
	bool m_isCste; ///< Vrai si la FonctionLineaire représente en faites une constante
	float m_csteValue; ///< Valeur de la constante si la FonctionLineaire en représente une

public:
	LinearFunction(NoeudGFD* node);
	LinearFunction(float value, NoeudGFD* node);
	LinearFunction(const LinearFunction& other);
	~LinearFunction();

	void setNode(NoeudGFD* node){
		this->m_node = node;
	} ///< Setteur de m_node
	NoeudGFD* getNode() const{
		return m_node;
	} ///< Getteur de m_node

	void addPseudoFctLineaire(PseudoLinearFunction* pseudoFct){
		this->m_vectorPseudoFctLineaire.push_back(pseudoFct);
	} ///< Ajoute une PseudoFonctionLineaire a la fin du conteneur

	PseudoLinearFunction* getPseudoFctLineaire(int i) const{
		assert(i<(int) m_vectorPseudoFctLineaire.size());
		return m_vectorPseudoFctLineaire[i];
	} ///< Renvoi la ième PseudoFonctionLineaire du conteneur 

	int getNbPseudoFctLineaire() const{
		return (int) m_vectorPseudoFctLineaire.size();
	} ///< Renvoi le nombre de PseudoFonctionLineaire de la fonction lineaire

	bool isCste() const{
		return m_isCste;
	}

	void setCsteValue(float value){
		this->m_csteValue = value;
	} ///< Setteur de m_csteValue
	float getCsteValue() const{
		return m_csteValue;
	} /// Getteur de m_csteValue

	LinearFunction* operator+(const LinearFunction& other); ///< Redefinition de l'operateur +
	LinearFunction* operator-(const LinearFunction& other); ///< Redefinition de l'operateur -
	LinearFunction* operator*(const LinearFunction& other); ///< Redefinition de l'operateur *
	LinearFunction* operator/(const LinearFunction& other); ///< Redefinition de l'operateur / 
	LinearFunction* operator-(); ///< Redefinition de l'operateur / 
	LinearFunction* FTMult(float value, string name) const; ///< Permet la multiplication d'une fonction lineaire avec une constante
	LinearFunction* FTDiv(float value, string name) const; ///< Permet la multiplication d'une fonction lineaire avec une constante
	void operatorDelay(); ///< Défini l'operateur delay

	void print() const; /// Defini la fonction d'affichage pour les FonctionLineaire
    string toString();
};

#endif
