/**
 *  \author Nicolas Simon
 *  \date   09/11/11
 *  \version 1.0
 *  \class FonctionLinaire
 *	\breif Classe correspondant representant chaque element Z d'une fonction linéaire (exposant, coefficient)
 */

#ifndef PSEUDO_FONCTION_LINEAIRE
#define PSEUDO_FONCTION_LINEAIRE

#include "utils/linearfunction/EltZ2.hh"
#include <cstring>
#include <iostream>
#include <set>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <cassert>

class NoeudSrc;
class NoeudGFD;

class PseudoLinearFunction {
	//friend FonctionLineaire2* operator+(const FonctionLineaire2& elt1, const FonctionLineaire2& elt2);


private:
	vector<EltZ2*> m_vecEltZ; ///< Element Z de la fonction lineaire
	NoeudGFD* m_startNode; ///< Noeud de départ de la fonction linéaire

public:
	PseudoLinearFunction(NoeudGFD* srcNode); ///< Constructeur
	PseudoLinearFunction(NoeudGFD* srcNode, EltZ2* elt);
	PseudoLinearFunction(const PseudoLinearFunction& other); ///< Constructeur par copie
	~PseudoLinearFunction(); ///< Destructeur

	void setStartNode(NoeudGFD* startNode){
		this->m_startNode = startNode;
	}///< Attribue m_startNode
	NoeudGFD* getStartNode(){
		return m_startNode;
	}///< Retourne m_startNode

	int getMaxExposant(){
		return (int) m_vecEltZ.size();
	}///< Retourne la valeur de l'exposant max

	void print(); ///< Methode d'affichage de la fonction lineaire
	string toString(); 
	string getNumerPourMatlab();
	string getDenomPourMatlab();

	void addEltZ(int exposant, float coeff, string coeffStr); ///< Créé et ajoute un EltZ. Additionne les EltZ2 si un est deja présent ils ont le même exposant 
	EltZ2* getEltZ(int i); ///< Rend le ieme EltZ de la fonction lineaire 
	bool removeEltZ(int i); ///< Supprime l'EltZ a la case i. Diminue la size du vecteur de 1
	PseudoLinearFunction* operator+(const PseudoLinearFunction& other); ///< Redefinition de l'opérateur +
	PseudoLinearFunction* operator+=(const PseudoLinearFunction& other); ///< Redefinition de l'opérateur +
	PseudoLinearFunction* operator*(const PseudoLinearFunction& other); ///< Redefinition de l'opérateur * 
	PseudoLinearFunction* operator*=(const PseudoLinearFunction& other); ///< Redefinition de l'opérateur -
	PseudoLinearFunction* operator-(const PseudoLinearFunction& other); ///< Redefinition de l'opérateur -
	PseudoLinearFunction* operator-=(const PseudoLinearFunction& other); ///< Redefinition de l'opérateur -
	PseudoLinearFunction* operator/(const PseudoLinearFunction& other); ///< Redefinition de l'opérateur / 
	PseudoLinearFunction* operator-(); ///< Redefinition de l'opérateur -
	void operatorDelay(); ///< Defini l'operateur delay

	double computeK();
	double computeL();
};


#endif	
