/**
 *  \file GFL.hh
 *  \author cloatre
 *  \date   12.2008
 *  \brief Classe definissant un Graphe typeVarFonction lineaire (GFL)
 *  The GFL is a graph with data nodes and linear functions on the edges.
 * Recursivity is made with 1-node loops (the loop edge is the denominator).
 * Phi nodes are also present on the GFL for they are not linear and can't be made into
 * linear functions. They are converted to pseudo-transfert functions later, in the Gft.
 *
 */

#ifndef _GFL_HH_
#define _GFL_HH_


#include <list>
#include <vector>

#include "graph/lfg/NoeudGFL.hh"
#include "graph/toolkit/Types.hh"
#include "graph/Graph.hh"
#include "utils/linearfunction/LinearFunction.hh"
#include "utils/graph/cycledismantling/GraphPath.hh"

using namespace std;

/**
 * \class Gfl
 * \brief Classe définisant les Gfl
 *
 * Classe définisant les manipulations de la structure Gfl
 */
class Gfl: public Graph {
private:
	void mergeBetween2GFL(Gfl* petitGflAFusionner);

public:
	// ======================================= Constructeur - Destructeur
	Gfl(bool noiseEval); ///Constructeur par defaut de la classe Gfl
	Gfl(list<LinearFunction*> listefonctionlineaire, bool noiseEval); ///Class GFL constructor from list of LinearFonc
	Gfl(NoeudGFL* noeudDeSortie, bool noiseEval); /// Ccree un GFL avec comme sortie le parametre donne et avec tous les listPred immediats
	virtual ~Gfl(); ///Destructeur

	// ======================================= Methodes
	void addNoeudGFL(NoeudGFL* Noeud); ///typeVarFonction d'ajout d'un noeud NoeudGFL dans le GFL
	void addNodeFromLinearFonction(LinearFunction* fct);

	void deleteNoeudGFL(NoeudGFL* Noeud); ///typeVarFonction d'enlevement d'un noeud NoeudGFL dans le GFL
	void deleteIsolatedNodeNoise(); /// function which delete separated node got after grouping or some processing, and new numbering

	void updateTypeNoeudGf(void); /// Methodes permetant mettre a jour les types: entree/sortie ou variable

	void xmlDynReader(const char* FichierXML); /// Read DynNode.xml to transmit dynamics get thanks to Matlab into GFL node

	Gfl* mergeOfGFL(); /// Function used to put together different Gfl into one final Gfl*/

	/****** Ensemble de méthode permettant de réduire les cycles d'un GFL apres son merge *******/
	int choixNoeudASubstituerIndependant(GraphPath * listeDesCircuit, vector<int>* numeroNoeud, vector<int> *nbOccurence);
	list<NoeudGraph*> determinationNoeudsASubstituer(vector<list<NoeudGraph*> > vecListNode); // Determine les noeuds commun a substituer
};

#endif // _GFL_HH_
