/**
 *  \file EdgeGFD.cc
 *  \author Nicolas Simon
 *  \date   25.06.2010
 *  \brief Class which mean edges of GFL
 */

#ifndef _EDGEGFL_HH_
#define _EDGEGFL_HH_

// === Bibliothèques non standard
#include "utils/linearfunction/LinearFunction.hh"
#include "graph/Edge.hh" //Heritage

// === Forward Declaration
class VarLinearFonc;

/**
 * \class EdgeGFL
 * \brief Classe définisant les EdgeGFL
 *
 */
class EdgeGFL: public Edge {
private:
protected:
	// Linear fonction related with the edge
	PseudoLinearFunction * m_fct;

public:
	// ======================================= Constructeurs - Destructeurs
	EdgeGFL(NoeudGraph* nodePred, NoeudGraph* nodeSucc, PseudoLinearFunction* Mfct); /// Constructeur
	~EdgeGFL(); /// Destructeur

	// =================== Methodes
	void setPseudoFonctionLineaire(const PseudoLinearFunction* Mfct) {
		delete this->m_fct;
		this->m_fct = new PseudoLinearFunction(*Mfct);
	} /// Initialize Mfct related with this object
	PseudoLinearFunction* getPseudoFonctionLineaire() const {
		return this->m_fct;
	} /// Return Mfct related with this object
};

#endif // _EDGEGFL_HH_
