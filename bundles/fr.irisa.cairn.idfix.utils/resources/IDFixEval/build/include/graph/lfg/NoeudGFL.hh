/**
 *  \file NoeudGFL.hh
 *  \author cloatre
 *  \date   5 janv. 2009
 *  \brief Classe definissant les noeuds  present dans un GFL
 */

#ifndef NOEUDGFL_HH_
#define NOEUDGFL_HH_

// === Bibliothèques standard
#include <iostream>
#include <string>
#include <list>

#include "graph/dfg/datanode/NoeudData.hh"
#include "graph/NoeudGraph.hh" // Heritage
#include "graph/lfg/EdgeGFL.hh"
#include "utils/graph/cycledismantling/GraphPath.hh"

// === Utilisation de la librairie standard ST
using namespace std;

/**
 * \class NoeudGFL
 * \brief Classe définisant les nœuds dGFL
 *
 */
class NoeudGFL: public NoeudGraph {
protected:
	TypeNoeudGFL TypeNodeGFL; /// Type de Noeud du GFL ( VAR_GFL, ENTREE_GFL, SORTIE_GFL, INIT_GFL, FIN_GFL)
	int numSrc; /// Numero de bruit
private:
	list<NoeudGFD*> refParamSrc; /// Liste des noeud data (data, source ou bruit) que représente le noeud GFL  

public:
	// ======================================= Constructeurs - Destructeurs
	NoeudGFL(TypeNoeudGFL TypeNodeGFL, int NumNoeud, TypeEtat Etat, const string& nom, string originalName);
	NoeudGFL(const NoeudGFL& other);
	~NoeudGFL();

	// ======================================= Methodes
	NoeudGraph* clone();

	void setTypeNoeudGFL(TypeNoeudGFL TypeNodeGFL) {
		this->TypeNodeGFL = TypeNodeGFL;
	} /// Affecte le type de noeud GFL ( VAR_GFL,VAR_ENTREE_GFL,VAR_SORTIE_GFL, INIT_GFL, FIN_GFL)
	TypeNoeudGFL getTypeNoeudGFL() const {
		return TypeNodeGFL;
	} /// Retourne le type de noeud GFL ( VAR_GFL,VAR_ENTREE_GFL,VAR_SORTIE_GFL,INIT_GFL, FIN_GFL)

	void setNumSrc(int numSrc) {
		this->numSrc = numSrc;
	} /// Affecte du Numero de Bruit
	int getNumSrc() {
		return numSrc;
	} /// Retourne le Numero de Bruit

	bool operator> (const NoeudGFL& lhs) const {
		return Numero>lhs.getNumero();
	}
	void setRefParamSrc(NoeudGFD* NoeudParamSrc);/// cree un nouveau NoeudSrcBruit contenant les parametres de bruit, si NoeudSrc => Rend le NoeudSrc
	NoeudGFD* getRefParamSrc(unsigned int Numero = 0) const; /// Renvoi la ième reference ajouté du noeud contenant les param
	int getNbRefParamSrc(){
		return refParamSrc.size();
	}
	bool isContainRefParamSrc(NoeudGFD* nodeData);

	void displayParamSrc();

	NoeudGFL* getSuccFromNumber(int numeroDuNoeud); /// Renvoi le successeur de numero donne, NULL si pas trouve
	NoeudGFL* getPredFromNumber(int numeroDuNoeud); /// Renvoi le predecesseur de numero donne, NULL si pas trouve

	void edgeFromOf(NoeudGraph * Queue, VarLinearFonc* Mfct); /// Cree un arc venant de Queue
	//void edgeFromOf(NoeudGraph * Queue, PseudoFonctionLineaire* Mfct); /// Cree un arc venant de Queue

	bool isNodeSource(); /// typeVarFonction determinant si un noeud est une Source du graphe GFL
	bool isNodeInput(); /// typeVarFonction determinant si un noeud est une entree du graphe GFL
	bool isNodeOutput(); /// typeVarFonction determinant si un noeud est une sortie du graphe GFL


	void print(FILE * f = stdout); /// Ecriture du Noeud et de ces Suc Pred dans un ficher(fic), ou sur ecran(si fic == NULL) (Utiliser par GFL::print())
	void printInfo(FILE *fic); /// Ecriture des infos du noeud


	//======================================Methode de recherche
	bool noeudGflEstDansPredImmediat(NoeudGFL* noeudCible); /// Methode qui regarde si le noeud courant fait partie des listPred immediats du noeud cible*/
	bool noeudSortieEstDansPredThis(NoeudGFL* noeudCible); /// Methode qui regarde si le noeud cible fait partie des listPred du noeud courant*/

	bool detectionCircuitUnitaire(int *succ); /// typeVarFonction determinant si il y a un circuit unitaire revenant vers lui meme, et le numero du successeur correspondant*/
	EdgeGFL* detectionCircuitUnitaire();

	bool belongsToTheCircuit(GraphPath * circuit); /// Methode qui regarde si le Noeud appartient au circuit*/
	void substituerSuccesseur(NoeudGFL* suc, GraphPath * circuit); /// Methode qui fusionne le noeud successeur*/

	//=====================================methode pour les fonctions de transfert
	TransferFunction* calculFonctionTransfertAvecSuccesseur(EdgeGFL* edge); /// Methode qui va calculer la fonction de transfert entre ce noeud et son successeur de numeroSuccesseur*/

	void nodePartOfTreeParcours(); /// methode appelee pour faire un scan d'un arbre pour set le flag 	*/


	// ======================================= Methodes permettant l'ajour d'un Element EdgeGFL entre 2 NoeudGraph de type GFL
	void addEdge(NoeudGraph * NGraph, VarLinearFonc* Mfct); /// Add a EdgeGFL element between this and NGraph, the VarLinearFonc is save too */
	void addEdge(NoeudGraph * NGraph, PseudoLinearFunction* Mfct); /// Add a EdgeGFL element between this and NGraph, the VarLinearFonc is save too */
	void addEdgeRapide(NoeudGraph * NGraph, VarLinearFonc* Mfct); /// Add a EdgeGFL element between this and NGraph, the VarLinearFonc is save too */
	void addEdgeRapide(NoeudGraph * NGraph, PseudoLinearFunction* Mfct); /// Add a EdgeGFL element between this and NGraph, the VarLinearFonc is save too */


	// === GfdPrint.cc
	void printVCG(FILE *fp);
	void printDOTNode(FILE *f, bool noiseEval = 0); /// Methode permettant la definition et de ses propriété d'affichage des noeud GFL au format DOT
	void printDOTEdge(FILE *f); /// Methode permettant la création des arcs vers les successeurs du noeud GFL et leurs propriétés au format DOT

	void printInfoGflVCG(FILE *fp);

};

#endif /* NOEUDGFL_HH_ */
