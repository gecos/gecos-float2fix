/**
*	
* \file Graph.hh
* \date 28.12.2009
* \author cloatre
* \class Graph
* \brief Generic Graph class
*
* A graph is made of edges and nodes. The graph class only points to the nodes (in various lists : full list, only inputs, only outputs),
* and the nodes themselves link to their edges.
* The class also provide export to dot format and output to standard output for testing.
*/

#ifndef GF_HH_
#define GF_HH_

// === Bibliothèques standard
#include <fstream>
#include <iostream>
#include <list>
#include <iterator>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <map>
// === Namespaces
using namespace std;

// === Bibliothèques non standard
#include "graph/toolkit/Types.hh"
#include "utils/Tool.hh"
#include "graph/NoeudGraph.hh"
#include "utils/graph/cycledismantling/GraphPath.hh"
#include "graph/toolkit/AdjacentMatrix.hh"


class NoeudFin;
class NoeudInit;

template<class T>
ostream& operator<<(ostream& os, const vector<T>& v) {
	copy(v.begin(), v.end(), ostream_iterator<T> (cout, " "));
	return os;
}

#ifndef __GRAPH_NODE_CONTAINER__
#define __GRAPH_NODE_CONTAINER__
typedef std::vector<NoeudGraph *> NodeGraphContainer;
#endif //LIST_NOEUD_GRAPH
class Graph {

protected:
	bool noiseEval; ///< Booléen indiquant si le Graph est spécifique a l'évaluation de la précision ou à la determination de la dynamique

	NodeGraphContainer *tabNode; ///< Liste des NoeudGraph du Graph
	int nbNode; ///< Nombre de NoeudGraph dans le Graph
	int NumNode; ///< Numero du prochain NoeudGraph

	NodeGraphContainer *tabInput; ///< Liste des NoeudGraph correspondant a l'entree du Graph
	int nbInput; ///< Nombre de NoeudGraph entree

	NodeGraphContainer *tabOutput; ///< Liste des NoeudGraph correspondant à la sortie du Graph
	int nbOutput; ///< Nombre de NoeudGraph sortie

	NodeGraphContainer *tabVarInt; ///< Liste des NoeudGraph correspondant a une variable intermediare (symboles )
	int nbVarInt; ///< Nombre de variables intermediaires

	NoeudInit * noeudInit; ///< Pointeur sur NoeudInit
	NoeudFin * noeudFin; ///< Pointeur sur le NoeudFin

public:
	fstream m_logDebug_pf; ///< Pointeur vers le fichier log, ouvert une fois a la construction du Graph et fermé à la destruction

	// ======================================= Constructeur - Destructeur
	Graph(bool noiseEval); // Constructeur par defaut de la classe Graph
	virtual ~Graph(); // Destructeur

	// ======================================= Methodes


	void setNumNode(int Val) {
		NumNode = Val;
	} ///< Initialisation du numero du noeuds du Graph disponible (prochain noeud)
	int getNumNode() {
		return NumNode;
	} ///< Numero du noeuds du Graph disponible (prochain noeud)

	int getNbNode() const {
		return (int) tabNode->size();
	} ///< Nombre de noeuds du f (dans la liste tabNode )
	void setNbNode(int Val) {
		nbNode = Val;
	} ///< Initialisation Nombre de noeuds du Gf (dans la liste tabNode )

	int getNbInput() const {
		return nbInput;
	} ///< Rend le nombre d'entrees du Gf (dans la liste TabEntrres )
	int getNbOutput() const {
		return nbOutput;
	} ///< Rend le nombre de sorties du Gf (dans la liste tabOutputs )
	int getNbVarInt() const {
		return nbVarInt;
	} ///< Rend le nombre de variables intermediaires  du GF

	NodeGraphContainer::iterator getBeginTabNode() const{
		return tabNode->begin();
	}
	NodeGraphContainer::iterator getEndTabNode() {
		return tabNode->end();
	}
	NodeGraphContainer::iterator getBeginTabOutput(){
		return tabOutput->begin();
	}
	NodeGraphContainer::iterator getEndTabOutput(){
		return tabOutput->end();
	}
	NodeGraphContainer::iterator getBeginTabInput(){
		return tabInput->begin();
	}
	NodeGraphContainer::iterator getEndTabInput(){
		return tabInput->end();
	}

//	NodeGraphContainer getTabOutput() const {
//		return * tabOutput;
//	} ///< Rend le pointeur sur la liste de noeuds correspondant aux sorties
//	NodeGraphContainer getTabNode() const {
//		return * tabNode;
//	} ///< Rend le pointeur sur la liste de noeuds contenu dans le graphe
//	NodeGraphContainer getTabInput() const{
//		return * tabInput;
//	} ///< Rend le pointeur sur la liste de noeud correspondant au entrées


	// ======================================= Methodes d'acces au noeuds INIT et FIN

	NoeudInit* getNoeudInit() {
		return noeudInit;
	} ///< Acces au pointeur sur le noeud INIT
	NoeudFin* getNoeudFin() {
		return noeudFin;
	} ///< Acces au pointeur sur le noeud FIN

	NoeudGraph* getElementGf(int i) const; ///< Acces a l element i du Gf ( liste tabNode )

	// ======================================= Methodes d'ajout et de supression de NoeudGraph dans le Gf

	NodeGraphContainer::iterator deleteGraphNode(NoeudGraph* Noeud); ///< Fonction de suppression d'un noeud NoeudGraph dans le Gf
	void deleteGraphNode(int i); ///< Suppression du ieme noeud dans la liste des noeuds du graph
	bool isGraphContain(const NoeudGraph* node); ///< Rend vrai si node appartient a la table des entrées

	// ======================================= Methodes de recherche

	bool isGfContientNoeud(NoeudGraph* NoeudCible); ///< Fonction de detection d'un NouedGraph
	NoeudGraph* GfRechercheNoeud(string* nameNoeudCible); ///< Fonction de recherche d'un NouedGraph
	NoeudGraph* GfRechercheNoeud(int numeroNoeudCible); ///< Fonction de recherche d'un NouedGraph

	// ======================================= Methodes d'ajout, de suppression et de consultation  des noeuds Entrees

	NoeudGraph* getInputGf(int i); ///< Fonction d'obtention de l'element i de la liste des entrees
	bool isListInputGfEmpty(); ///< Fonction indiquant si la liste des Sorties est vide
	bool isInputGfContain(const NoeudGraph* node); ///< Rend vrai si node appartient a la table des entrées

	// ======================================= Methodes d'ajout, de suppression et de consultation  des noeuds Sorties

	NoeudGraph* getOutputGf(int i); ///< Fonction d'obtention de l'element i de la liste des sorties
	bool isListOutputGfEmpty(); ///< Fonction indiquant si la liste des Sorties est vide
	bool isOutputGfContain(const NoeudGraph* node); ///< Rend vrai si node appartient a la table des entrées

	// ======================================= Methodes d'ajout, de suppression de consultation  des noeuds VarInt

	NoeudGraph* getVarIntGf(int i); ///< Fonction d'obtention de l'élement i de la liste des variables intermédiaires
	bool isListVarIntGfEmpty(); ///< Fonction indiquant si la liste des variables intermédiaires est vide
	bool isVarGfContain(const NoeudGraph* node); ///< Rend vrai si node appartient a la table des entrées

	// ======================================= Methodes virtuelles definies dans les classes filles
	void resetInfoVisite();

	// ======================================= Methodes permetant de lier les Init et les Fin avec le reste du graph (finition du xml2Gf)

	void liaisonsSuccPredInitFin(void); ///< function which creates edge with init and end node
	void deleteIsolatedNode(); ///< function which call deleteIsolatedNodeNoise for the Noise evaluation or deleteIsolatedNodeDyn for the Dynamic evaluation
	virtual void deleteIsolatedNodeNoise(); ///< function which delete separated node got after grouping or some processing, and new numbering
	void deleteIsolatedNodeDyn(); ///< function which delete separated node got after grouping or some processing, and new numbering

	// ======================================= Methodes d affichage et de sauvegarde dans un fichier

	void saveGf(const char * Extension) const; ///< Fonction de sauvegarde du Gf dans un fichier au format DOT

	typedef void TraitementNoeud(NoeudGraph& n);
	void parcoursPrefixe(TraitementNoeud& fonctionTraitement, NoeudGraph& noeudInitial);
	friend void timing(string function, int nbNode, time_t timing);

	void setNoiseEval(bool TypeEval) {
		this->noiseEval = TypeEval;
	} ///< Initialisation du booléen indiquant si le graphe est spécifique à l'évaluation de la précision ou à la détermination de la dynamique 
	bool getNoiseEval() const {
		return this->noiseEval;
	} ///< Rend le booléen indiquant si le grpahe est spécifique à l'évaluation de la précision ou à la détermination de la dynamique

	// === Gfdprint.cc
	void print(FILE *fic = NULL) const; ///< Ecriture global dans un ficher(fic), ou sur ecran(si fic == NULL)
	void printListEntrees(FILE *fic) const; ///< Ecriture de la liste des Entrees (appeler par print)
	void printListSorties(FILE *fic) const; ///< Ecriture de la liste des Sorties (appeler par print)
	void printListVarInts(FILE *fic) const; ///< Ecriture de la liste des VarInts (appeler par print)

	void printVCG(FILE *fp) const; ///< Ecriture au format VCG (appeler par saveGf)
	void printDOT(FILE *fp) const; ///< Ecriture au format DOT (appeler par saveGf)
	
	
	pair<NoeudGraph*, int>* algoDeDijkstra(NoeudGraph* input); ///< Algorithme qui permet de determiner le plus court chemun entre un noeud quelconque fourni et tout les autres noeuds du graph
	list<NoeudGraph*> cheminPlusCourtEntreDeuxNoeud(NoeudGraph* noeudOrigine, NoeudGraph* noeudArrivee); ///< Fourni le plus court chemun entre deux noeud précis, liste vide si pas de chemin
	map<NoeudGraph*, list<NoeudGraph*> > cheminPlusCourtEntreDeuxNoeud(NoeudGraph* noeudOrigine, list<NoeudGraph*> listArrivee); ///< Fourni le plus court chemin entre un noeud de départ et une liste de noeuds sous la forme d'une map. Si il n'y a pas de chemin vers un noeud précis, la liste du chemin associé sera vide
	list<NoeudGraph*> noeudPlusJeuneDansMultipleNoeudCommun(map<NoeudGraph*, list<NoeudGraph*> > mapChemin, vector<list<NoeudGraph*> > vectorDesNoeudsCommun); ///< Determine les noeud les plus jeune a partir d'un ensemble de noeud commun de différent circuit et des chemins les plus court de la source vers ces noeuds 

	
	vector<NoeudGraph*>::iterator rechercheNoeud(NoeudGraph* nodeGraph); ///< recherche dichotomique du noeud nodeGraph dans le tableau des noeuds tabNode de this
	vector<NoeudGraph*>::iterator rechercheNoeudInput(NoeudGraph* nodeGraph); ///< recherche dichotomique du noeud nodeGraph dans le tableau des Input
	vector<NoeudGraph*>::iterator rechercheNoeudOutput(NoeudGraph* nodeGraph); ///< recherche dichotomique du noeud nodeGraph dans le tableau des Output
	vector<NoeudGraph*>::iterator rechercheNoeudVarInt(NoeudGraph* nodeGraph); ///< recherche dichotomique du noeud nodeGraph dans le tableau des VarInt

private:
	vector<NoeudGraph*>::iterator rechercheDichotomique(NoeudGraph* nodeGraph, vector<NoeudGraph*> * tabNode); ///< recherche dichotomique du noeud nodeGraph dans le tableau des noeuds tabNode de this
	bool nodeIsIn(const vector<NoeudGraph*> *tab, const NoeudGraph* node); ///< Rend vrai si node est dans tab, faux sinon

public:
	void addNoeudGraph(NoeudGraph* Noeud); ///< Fonction d'ajout d'un noeud NoeudGraph dans le Gf en testant si il existe deja
	void addNoeudGraphRapide(NoeudGraph* Noeud); ///< Fonction d'ajout d'un noeud NoeudGraph dans le Gf sans tester si il existe deja
	
    void addInputGf(NoeudGraph * Noeud); ///< Fonction d'ajout d'un noeud dans la liste des entrees
	void deleteInputGf(NoeudGraph * Noeud); ///< Fonction de suppression d'un noeud dans la liste des entrees
	
    void addOutputGf(NoeudGraph * Noeud); ///< Fonction d'ajout d'un noeud dans la liste des sorties
	void deleteOutputGf(NoeudGraph * Noeud); ///< Fonction de suppression d'un noeud dans la liste des sorties
	
    void addVarIntGf(NoeudGraph * Noeud); ///< Fonction d'ajout d'un noeud dans la liste das variables intermédiaires
	void deleteVarIntGf(NoeudGraph * Noeud); ///< Fonction de suppression d'un noeud de la liste des variables intermédiaires
};

#endif /* GF_HH_ */
