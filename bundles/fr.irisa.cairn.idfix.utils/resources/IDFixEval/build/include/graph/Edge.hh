/**
 *  \file Edge.hh
 *  \author Nicolas Simon
 *  \date   25.06.2010
 *  \brief Class which mean edges (herited by EdgeGFD and EdgeGFL class)
 */

#ifndef _EDGE_HH_
#define _EDGE_HH_

/**
 * \class Edge
 * \brief Classe définisant les Edge
 *
 */
#include "graph/NoeudGraph.hh"
#include "utils/Tool.hh"

class NoeudGraph;

class Edge {
protected:
	int numInput; ///< Number means the input of the successor Node
	int numEdge; ///< Number of the edge
	NoeudGraph* nodePred; ///< Pointer to the predecessor node
	NoeudGraph* nodeSucc; ///< Pointer to the successor node

public:
	// ======================================= Constructeur - Destructeur
	Edge(NoeudGraph* nodePred, NoeudGraph* nodeSucc, int numInput); ///< Constructeur
	Edge(const Edge &other); ///< Use the clone method instead
	virtual ~Edge(); ///< Destructeur

	// ======================================= Methodes
	void setNumInput(int numInput) {
		this->numInput = numInput;
	} ///< Affectation of Numero related with this object
	int getNumInput() const {
		return this->numInput;
	} ///< Return Numero related with this object

	void setNumEdge(int numEdge) {
		this->numEdge = numEdge;
	}
	int getNumEdge() const {
		return this->numEdge;
	}

	void setNodePred(NoeudGraph* nodePred) {
		this->nodePred = nodePred;
	} ///< Affectation nodePred related with this object
	NoeudGraph* getNodePred() const {
		return this->nodePred;
	} ///< Return nodePred related with this object

	void setNodeSucc(NoeudGraph* nodeSucc) {
		this->nodeSucc = nodeSucc;
	} ///< Affectation nodeSucc related with this object
	NoeudGraph* getNodeSucc() const {
		return this->nodeSucc;
	} ///< Return nodeSucc related with this object

	//void print(); 

	virtual string getLabel() const {
		return string("");
	}

	virtual Edge* clone(); ///< Properly clone an edge even if it is from a derived class (the copy constructor can't do that)


};
#endif // _EDGE_HH_
