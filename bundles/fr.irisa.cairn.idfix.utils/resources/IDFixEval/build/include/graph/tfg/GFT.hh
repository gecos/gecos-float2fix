/**
 * \file GFT.hh
 * \author Adrien Destugues
 * \date    19.11.2010
 * \brief Class for graph of Transfer Functions
 * This graph is similar to the GFL, but instad of allowing only linear non-recursive functions on the edges, it allows all kinds of transfert functions and
 * even pseudo-transfert functions for some non-linear cases. These cases include the phi operators and the ones obtained by simulation only.
 * The GFT makes it possible to make all these kinds of transfert functions working together.
 *
 */

#ifndef _GFT_HH_
#define _GFT_HH_

// === Bibliothèques non standard
#include "graph/lfg/GFL.hh"
#include "graph/Graph.hh"

class Edge;
class Gfd;

/**
 * \class Gft
 * \brief Classe définisant les Gft
 *
 *  Classe définisant les Gft
 */
class Gft: public Graph {
public:
	// ======================================= Constructeurs - Destructeurs
	Gft(const Gfl& linearGraph); /// Build a GFT from a GFL, converting the linear functions to transfer functions
	virtual ~Gft(){}
	 /// Destructeur

	// ======================================= Methodes
	void addNoeudGraph(NoeudGraph* n); /// Add a node to this graph. If it is a GFL node, convert it to a GFT one before adding.
	void addEdge(Edge* e); /// Add an edge to this graph. If it is a GFL edge, convert it to a GFT one and compute the transfer func first.
	void moveAllPhisDown(); /// Move all PHIs toward the output of the graph, computing the global transfer functions in the process

	// Methodes utilisees pour generer le fichier matlab ParamFTNonLti.m
	void genereFTPartiellesNonLti(fstream& outputMatFile); /// Methode pour generer les fonctions de transferts partielles entre chaque noeud
	void genereFTGlobalesNonLti(fstream & outputMatFile);
	bool genereMatFileParamFTNonLti(string pathFile);

	// Methodes utilisees pour generer le fichier matlab ParamFT.m
	bool genereMatFileParamFTDyn(string pathFile); /// Methode principale pour generer le fichier matlab ParamFTDyn.m
	bool genereMatFileParamFT(string pathFile); /// Methode principale pour generer le fichier matlab ParamFT.m

	void genereFTPartielles(fstream& outputMatFile); /// Methode pour generer les fonctions de transferts partielles entre chaque noeud
	void genereFTGlobales(fstream& outputMatFile); /// Methode pour generer les fonctions de transferts globales entre chaque noeud et la sortie

	void genereFTGlobalesDyn(fstream& outputMatFile); /// Methode pour generer les fonctions de transferts globales entre chaque noeud et la sortie
	void genereInputDyn(fstream& outputMatFile); /// Methode pour generer la spécification de la dynamique des entrée par rapport au type de calcul utilisé dans la partie dynamique

	void generateNoiseFunction(Gfd* grapheDepart); /// Methode principale pour generer le fichier ComputePb.c

	void computeKLM(); ///< Appelle les fonctions calculant et écrivant dans un fichier binaire les matrices KLM
	void computeKLMMultiThreading(); ///< Appelle les fonctions calculant et écrivant dans un fichier binaire les matrices KLM
	void computeK(FILE* file); ///< Fonction calculant et écrivant dans un fichier binaire la matrice K
	void computeKMultiThreading(float **vectorK); ///< Fonction calculant et écrivant dans un fichier binaire la matrice K
	void computeL(FILE* file); ///< Fonction calculant et écrivant dans un fichier binaire la matrice K
	void computeLMultiThreading(float*** vectorL); ///< Fonction calculant et écrivant dans un fichier binaire la matrice K
	void computeLWithoutZero(FILE* file, map<NoeudGFT*, list<NoeudGFT*> > mapInterdependance); ///< Fonction calculant et écrivant dans un fichier binaire la matrice K
	void computeLWithoutZeroMultiThreading(float*** vectorL, map<NoeudGFT*, list<NoeudGFT*> > mapInterdependance); ///< Fonction calculant et écrivant dans un fichier binaire la matrice K

	void computeKLMTest(); ///< Appelle les fonctions calculant et écrivant dans un fichier binaire les matrices KLM
	void readKLMBinariesFile(); ///<Fonction permettant la lecture du fichier binaire généré et de l'écrire dans un fichier. Genere aussi un comparatif a partir d'un nouveau calcul des matrices
	void computeKCompareBin(fstream& file); ///< Generation dans le fichier file du la matrice K servant au comparatif
	void computeLCompareBin(fstream& file); ///< Generation dans le fichier file du la matrice L servant au comparatif

	map<NoeudGFT*, list<NoeudGFT*> > determinationInterdependanceOutputInput();
	bool isComputeLWithoutZero();

};

#endif // _GFT__H_
