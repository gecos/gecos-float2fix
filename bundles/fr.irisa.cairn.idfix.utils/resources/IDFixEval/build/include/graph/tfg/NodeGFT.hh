/**
 * \file NodeGFT.hh
 * \author Adrien Destugues
 * \date    19.11.2010
 * \brief Class for NodeGFT
 *
 */

#ifndef _NODEGFT_HH_
#define _NODEGFT_HH_

// === Bibliothèques non standard
#include "graph/NoeudGraph.hh"
#include "graph/lfg/NoeudGFL.hh"
#include "graph/toolkit/Types.hh"

// === Forward Declaration
class EdgeGFT;

/**
 * \class NoeudGFT
 * \brief Classe définisant les nœuds GFT
 *
 */
class NoeudGFT: public NoeudGraph {
private:
	list<NoeudGFD*> refParamSrc; 	/// Noeud associe contenant les param de bruit, NULL sinon
	TypeNoeudGFT 	typeNoeudGFT; 	/// Type de Noeud CFT (VAR_GFT, PHI_GFT)

public:
	// ======================================= Constructeurs - Destructeurs
	NoeudGFT(const NoeudGFL& n); /// Build a GFT node from a GFL one. This is almost  a copy constructor.
	NoeudGFT(const NoeudGFT& other); /// Copy constructor. Also copy the edges to/from this node.
	NoeudGFT(); /// interdit!

	// ======================================= Methodes
	void 			setTypeNodeGraphGFT(TypeNoeudGFT typeNoeudGFT) {this->typeNoeudGFT = typeNoeudGFT;}
	TypeNoeudGFT 	getTypeNodeGraphGFT() {return typeNoeudGFT;}

	NoeudGraph* 	clone();

	//void 			addEdge(NoeudGFT* NGraph, TransfertFunction Mfct);
	void 			addEdge(NoeudGFT* NGraph, TransferFunction* Mfct);

	NoeudGFD* 		getRefParamSrc(unsigned int Numero = 0 ) const; /// Renvoi la première reference sur le noeud
	//NoeudData*		getRefParamSrc();
	bool 			isNodeOutput(); 	/// typeVarFonction determinant si un noeud est une sortie du graphe
	bool 			isNodeSource();

	GraphPath* 		enumerationCheminVersSource(NoeudGFT* sourcebruit, EdgeGFT* origine = NULL);
	/// Methode qui parcourt tous les chemins à partir d'une sortie et renvoie la liste des chemins possible vers une source

	void 			Swap(NoeudGFT* other); /// Swap this node with one of his predecessors. Used to move phi down in the graph

	// === Gfdprint.cc
	void 			print(FILE* f = stdout);
	void 			printVCG(FILE* f);
	void 			printDOTEdge(FILE* f);	/// Methode permettant la création des arcs vers les successeurs du noeud GFT et leurs propriétés au format DOT 
	void 			printDOTNode(FILE* f, bool noiseEval);	/// Methode permettant la definition et de ses propriété d'affichage des noeud GFT au format DOT

};

#endif
