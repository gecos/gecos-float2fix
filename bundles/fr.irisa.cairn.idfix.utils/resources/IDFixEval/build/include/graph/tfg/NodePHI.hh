/**
 * \file NodePHI.hh
 * \author Adrien Destugues
 * \date    19.11.2010
 *  \brief Classe pour représenter un noeud phi dans un GFT
 */

#ifndef _NODEPHI_HH_
#define _NODEPHI_HH_

// === Bibliothèques non standard
#include "graph/tfg/NodeGFT.hh"

/**
 * \class NoeudPHI
 * \brief Classe définisant les nœuds PHI
 *
 */
class NoeudPHI: public NoeudGFT {
	// ======================================= Constructeurs - Destructeurs
public:
	NoeudPHI(const NoeudGFL& n) :
		NoeudGFT(n) {
	}
	; /// Constructeur

	// ======================================= Methodes
	NoeudGraph* clone();
	//xxx
	void Merge(NoeudPHI* other);

};
#endif // _NODEPHI_HH_
