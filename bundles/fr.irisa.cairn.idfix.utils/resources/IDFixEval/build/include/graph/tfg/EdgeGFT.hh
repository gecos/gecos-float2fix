/**
 *  \file EdgeGFT.hh
 *  \author Adrien Destugues
 *  \date   12.2010
 *  \brief Class for edges in the Gft (transfert function graph)
 *	The edges of the Gft have only one attribute, which is the transfert functions between
 * two nodes. This transfert function may be a regular linear one, a pseudo-FT or something else.
 */

#ifndef _EDGEGFT_HH_
#define _EDGEGFT_HH_

#include "graph/lfg/EdgeGFL.hh"

#include "utils/linearfunction/TransferFunction.hh"
#include "graph/Edge.hh"

class NoeudGFT;
class Gft;

/**
 * \class EdgeGFT
 * \brief Classe définisant les nœuds EdgeGFT
 *
 * Classe définisant les nœuds EdgeGFT
 */
class EdgeGFT: public Edge {
	//TransfertFunction m_FT;
	TransferFunction* m_FT;

public:
	// ======================================= Constructeurs - Destructeurs
	EdgeGFT(NoeudGFT* s, NoeudGFT* d, TransferFunction* f);
	~EdgeGFT();

	// ======================================= Methodes
	Edge* clone() {
		return new EdgeGFT(*this);
	}

	string getLabel() const;
	TransferFunction* getFT() const {
		return m_FT;
	}

	void setFonctionTransfert(TransferFunction* fct){
		this->m_FT = fct;
	}
	TransferFunction* getFonctionTransfert(){
	   return this->m_FT;
	}	   


	void Mult(const TransferFunction& f){
		trace_error("Fonction a définir");
		exit(1);
	}

	string getMatlabExpression() const;

};

#endif // _EDGEGFT_H_
