/**
 *  \file Types.hh
 *  \author Daniel Menard
 *  \date   03 03 2001
 *  \brief Définition des types utilisés dans ID.Fix
 */

#ifndef _TYPES_HH_
#define _TYPES_HH_


/**
 *	\enum DynamicProcess
 *	\brief type of Dynamic processing used
 *
 *
 */
typedef enum DynamicProcess {
	INTERVAL,
	OVERFLOW_PROBA
} DynamicProcess;
	

/**
 * \struct DataDynamic
 * \brief Dynamic of a variable
 *
 * Dynamic of a variable (min and max values)
 */
typedef struct {
	double valMin; /// Valeur minimale
	double valMax; /// Vlaur Maximale
} DataDynamic;

/**
 * \enum TypeVarFonct
 * \brief Types de variable
 *
 * Definition des types de variable en virgule fixe : VAR_ENTREE, VAR_INT, CONST, ...
 */
typedef enum TypeVarFonct {
	VAR_ENTREE, /// Variable d'entree du bloc
	VAR_SORTIE, /// Variable de sortie du bloc
	VAR, /// Variable intermediaire
	VAR_INT, /// Variable intermediaire presente dans la liste des VarInt d'un GFD
	CONST, /// Constante
	CONST_SORTIE
/// Constante en sortie
} TypeVarFonct;


/**
 * \enum TypeSignalBruit
 * \brief Type de noeud au niveau bruit
 *
 * Definition du type de noeud au niveau bruit : SIGNAL, BRUIT
 */
typedef enum TypeSignalBruit {
	SIGNAL, /// DATA SIGNAL
	BRUIT
/// DATA BRUIT
} TypeSignalBruit;

/**
 * \enum TypeNodeData
 * \brief Type d'un Noeud Data
 *
 * Definition du type d'un Noeud Data  :    DATA_BASE, DATA_ARRAY, DATA_SRC_BRUIT
 */
typedef enum TypeNodeData {
	DATA_BASE, /// Donnee classique
	DATA_ARRAY, /// Donnee tableau
	DATA_SRC,
	DATA_SRC_BRUIT
/// Donnee un source de bruit
} TypeNodeData;

/**
 * \enum TypeNodeGraph
 * \brief Type de NoeudGraph
 *
 * Defintion du type de NoeudGraph : GFD, GFC, ELT_ES, OPS_ASS, OPR
 */
typedef enum TypeNodeGraph {
	GFD, /// Graphe flot de donnees
	GFT, /// Graphe de fonctions de transfert
	GFL, /// Graphe fonction lineaire

	INIT, /// Noeud de depart
	FIN
/// Noeud de fin
} TypeNodeGraph;

/**
 * \enum TypeNodeGFD
 * \brief Type de NoeudGFD
 *
 * Defintion du type de NoeudGFD : OPS, DATA, INIT, FIN
 */
typedef enum TypeNodeGFD {
	OPS, /// Noeud Operation
	DATA
/// Noeud Donnee
} TypeNodeGFD;

/**
 * \enum TypeNoeudGFL
 * \brief Type de NoeudGFL
 *
 * Defintion du type de NoeudGFL : INTER, INIT, FIN
 */
typedef enum TypeNoeudGFL {
	VAR_GFL, /// Noeud intermediaire
	VAR_ENTREE_GFL, /// Noeud entree du GFL
	VAR_SORTIE_GFL, /// Noeud sortie du GFL(identique a l'entree normalement)
	PHI_GFL
/// Noeud PHI
} TypeNoeudGFL;

/**
 * \enum TypeNoeudGFT
 * \brief Type de NoeudGFT
 *
 * Defintion du type de NoeudGFT: VAR, PHI
 */
typedef enum TypeNoeudGFT {
	VAR_GFT, PHI_GFT
} TypeNoeudGFT;

/**
 * \enum TypeEtat
 * \brief Type de Etat
 *
 * Defintion de l'etat d'un noeud : VISITE, ENTRAITEMENT, NONVISITE
 */
typedef enum TypeEtat {
	VISITE, /// Noeud visité
	ENTRAITEMENT, /// Noeud en traitement
	NONVISITE,
	R1,
	R2,
	R3,
	R4
/// Noeud non visité
} TypeEtat;

/**
 * \enum TypeNodeXml
 * \brief Type de NodeXml
 *
 *   Definition du type des noeuds Xml : EDGE, NODE
 */
typedef enum TypeNodeXml {
	EDGE, /// Arc
	NODE
/// Noeud
} TypeNodeXml;

/**
 * \enum TypeXmlOpsData
 * \brief Type de XmlOpsData
 *
 * Definition du type des noeuds Ops et Data Xml : DATA_XML, OPS_XML
 */
typedef enum TypeXmlOpsData {
	DATA_XML, /// Donnée XML
	OPS_XML
/// Opération XML
} TypeXmlOpsData;

/**
 * \enum TypeLti
 * \brief Type de LTI
 *
 * Definition du type de LTI : SIGNAL_LTI, CONST_LTI, NOT_LTI
 */
typedef enum TypeLti {
	SIGNAL_LTI = 0, CONST_LTI = 1, NOT_LTI = 2,
} TypeLti;

#endif // _TYPES_HH_
