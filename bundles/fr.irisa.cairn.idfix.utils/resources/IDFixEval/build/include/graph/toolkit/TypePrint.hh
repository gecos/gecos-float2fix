/**
 * \file Typeprint.hh
 * \author  Jean-Charles Naud
 * \date   1.09.2008
 * \brief header definissant les fonction globale de Convertion en string de Type
 */

#ifndef _TYPEPRINT_HH_
#define _TYPEPRINT_HH_

// === Bibliothèques standard
#include	<string>

// === Bibliothèques non standard
#include "graph/toolkit/Types.hh"	// Types
#include "utils/Tool.hh"
// === Namespaces
using namespace std;

string typetypeVarFonctConvToStr(TypeVarFonct typeVarFonct); /// Convertion en string de TypeVarFonct
string typeSignalBruitConvToStr(TypeSignalBruit SignalBruit); /// Convertion en string de TypeSignalBruit
string typeNodeDataConvToStr(TypeNodeData NodeData); /// Convertion en string de TypeNodeData
string typeNodeGraphConvToStr(TypeNodeGraph Graphe); /// Convertion en string de TypeNodeGraph
string typeNoeudGFDConvToStr(TypeNodeGFD NoeudGFD); /// Convertion en string de TypeNodeGFD
string typeEtatConvToStr(TypeEtat Etat); /// Convertion en string de TypeEtat
string typeNodeXmlConvToStr(TypeNodeXml NodeXml); /// Convertion en string de TypeNodeXml
string typeXmlOpsDataConvToStr(TypeXmlOpsData XmlOpsData); /// Convertion en string de TypeXmlOpsData


#endif // _TYPEPRINT_HH_
