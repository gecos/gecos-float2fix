/**
 *  \file TypeVar.hh
 *  \author Daniel Menard, Jean-Charles Naud
 *  \date   31.01.2002
 *  \brief Classe definissant les TypesVar
 */

#ifndef _TYPEVAR_HH_
#define _TYPEVAR_HH_

// === Bibliothèques standard
#include "graph/toolkit/Types.hh"	// Types
#include <iostream>

// === Bibliothèques non standard
// === Forward Declaration
class TypeVar;

/**
 * \class TypeVar
 * \brief Definition de la classe specifiant le type d'une variable
 *
 *  Definition de la classe specifiant le type d'une variable
 */
class TypeVar {
private:
protected:
	TypeVarFonct typeVarFonct; /// Type de variable  ( VAR_ENTREE, VAR_SORTIE, VAR, VAR_INT, CONST, CONST_SORTIE)
	//TypeVarPortee typeVarPortee; /// Porte de la variable dans le code source (INTER, GLOBAL, LOCAL, EXTERN)
	//TypeVarRef typeVarRef; ///	typeVarReference de la variable  (DIRECTE,POINTEUR)
	//TypeVarStruct typeVarStruct; ///	typeVarStructure associee a la variable  (SCALAIRE, TABLEAU)
	//TypeVarReb typeVarReb; ///	Type de rebouclage (AUCUN, INITIALISATION, REBOUCLAGE, VIEILLISSEMENT, CYCLE)

public:

	// ======================================= Constructeurs - Destructeurs
	//TypeVar(TypeVarFonct typeVarFonct, TypeVarPortee typeVarPortee, TypeVarRef typeVarRef, TypeVarStruct typeVarStruct, TypeVarReb typeVarReb); /// Constructeur
	TypeVar(TypeVarFonct typeVarFonct); /// Constructeur
	~TypeVar(); /// Destructeur

	// ======================================= Methodes
	void setTypeVarFonct(TypeVarFonct F) {
		typeVarFonct = F;
	} ///	Initialise le type de variable
	TypeVarFonct getTypeVarFonct() {
		return typeVarFonct;
	} ///	Renvoi le type de variable

	/*void setTypeVarPortee(TypeVarPortee P) {
		typeVarPortee = P;
	} ///	Initialise la porte de la variable dans le code source
	TypeVarPortee getTypeVarPortee() {
		return typeVarPortee;
	} ///	Renvoi la porte de la variable dans le code source

	void setTypeVarRef(TypeVarRef R) {
		typeVarRef = R;
	} ///	Initialise la reference de la variable
	TypeVarRef getTypeVarRef() {
		return typeVarRef;
	} ///	Renvoi la reference de la variable

	void setTypeVarStruct(TypeVarStruct S) {
		typeVarStruct = S;
	} ///	Initialise la structure associee a la variable
	TypeVarStruct getTypeVarStruct() {
		return typeVarStruct;
	} ///	Renvoi la structure associee a la variable

	void setTypeVarReb(TypeVarReb Re) {
		typeVarReb = Re;
	} ///	Initialise le type de rebouclage
	TypeVarReb getTypeVarReb() {
		return typeVarReb;
	} ///	Renvoi le type de rebouclage*/

	TypeVar* clone();

	// === Gfdprint.cc
	void printInfo(FILE *fic); /// Methode d'ecriture des infos du noeud dans un ficher(fic), ou sur ecran(si fic == stdout))

};

#endif // _TYPEVAR_HH_
