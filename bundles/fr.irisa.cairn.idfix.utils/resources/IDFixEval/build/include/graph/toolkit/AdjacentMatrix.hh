/**
 * \file AdjacentMatrix.hh
 * \author Loic Cloatre
 * \date   09.12.2008
 * \brief Classe definissant la matrice adjacente
 * The adjacent matrix is another way of storing a graph.
 * The matrix is a 2d array where each line represents a node, and each element in the line
 * is a successor of this node. This compact representation allows fast exploration of the
 * graph, but isn't as convenient for usual graph algorithms. The matrix is best suited for random-access
 * operations in the graph, not directly linked to its structure. Each node can be accessed directly.
 *
 * This class is built around a C-style dynamic array, with lines of different length. This is because
 * C++ style objects (like vectors) are too slow.
 *
 * It's used for finding circuits in the GFD.
 */

#ifndef _ADJACENTEMATRICE_HH_
#define _ADJACENTEMATRICE_HH_

// === Bibliothèques standard
#include <cstdlib>
#include <iostream>
#include "utils/Tool.hh"
using namespace std;

/**
 * \brief Dynamic-size struct representing a line in the adjacent matrix or a node path in the graph
 *
 * This struct tells the size of the line. The contents is empty (zero size array)
 * at creation, and the struct is grown or shrink with realloc operations, as needed.
 *
 * The content is the index of the nodes that are successors of this line.
 */
typedef struct {
	int size;
	int contenu[0];

	bool Appartient(int elem) {
		for (int i = 0; i < size; i++)
			if (contenu[i] == elem)
				return true;
		return false;
	}
} LigneMatrice;

/**
 * \class AdjacentMatrix
 * \brief Classe définisant Matrice Adjacente
 *
 * Definition de la classe Matrice Adjacente pour lister les listSucc/listPred
 */
class AdjacentMatrix {
private:
protected:

	LigneMatrice** tab; /// Tableau à deux dimension
	int Taille; /// Line count in the matrix (equals the number of nodes in the graph)
	//vector<vector<int> > 	mOrigine;

public:
	// ======================================= Constructeurs - Destructeurs
	AdjacentMatrix() {
	}
	; /// Constructeur vide
	AdjacentMatrix(int Taille); /// Pre-allocation of a matrix of known height. All the lines are empty.
	AdjacentMatrix(AdjacentMatrix& autreMat) {
		cerr << "operateur de recopie matrice adjacente non implante" << endl;
	}
	; /// Constructeur recopie
	AdjacentMatrix(LigneMatrice* uneLigne, int Taille); /// Constructeur a partir d'une ligne
	AdjacentMatrix(LigneMatrice* Init, LigneMatrice* Fin, int Taille); /// Constructeur a partir d'une ligne
	~AdjacentMatrix(); /// Destructeur virtuel

	// ======================================= Methodes
	void setCellule(int numLigne, int numColonne, int valeur); /// Set a cells value. Does not allocate memory if the cell doesn't exist
	int getCelulle(int numLigne, int numColonne); /// Retourne la valeur de la celulle d'une matrice

	int getLongueurLigne(int numLigne); /// TypeVarFonction pour recuperer la longueur d'une ligne
	int getHauteurMatrice() {
		return Taille;
	} /// TypeVarFonction pour recuperer la ligne d'une matrice sous forme de vector<int>

	int addCelulle(const int numLigne, const int valeur); /// TypeVarFonction pour inserer une celulle au bout de la ligne (vector<int>.push_back utilise)

	void displayMatrice(); /// TypeVarFonction qui affiche la matrice [12 , 15 , ...   ]
	void displayLigneMatrice(LigneMatrice *numLigne); /// TypeVarFonction qui affiche une ligneMatrice

	LigneMatrice* getLigne(int numero) {
		return tab[numero];
	} /// TypeVarFonction pour recuperer la ligne d'une matrice sous forme de vector<int>

	void resetLigneMatrice(int numero); /// Clear a line in the matrix (and free the memory)
	void resetMatrice(); /// TypeVarFonction qui remet a 0 la matrice
};

#endif // _ADJACENTEMATRICE_HH_
