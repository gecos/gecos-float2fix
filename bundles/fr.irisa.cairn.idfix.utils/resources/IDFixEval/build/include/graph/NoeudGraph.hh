/**
 *  \file NoeudGraph.hh
 *  \author Daniel Menard
 *  \author Jean-Charles Naud
 *  \date   31.01.2002
 *  \brief Classe definissant les noeuds representant une donnee dans un GFD
 */

#ifndef _NOEUDGRAPH_HH_
#define _NOEUDGRAPH_HH_

// === Bibliothèques standard
#include <iostream>
#include <fstream>
#include <list>
#include <vector>
#include <cstring>
#include <cmath>

// === Bibliothèques non standard
#include "graph/toolkit/Types.hh"	// Types
#include "utils/Tool.hh"
#include "graph/Edge.hh"
#include "utils/linearfunction/LinearFunction.hh"

class Gfd;
class Edge;
class NoeudGraph;
class NoeudSrcBruit;

// === Namespaces
using namespace std;

#ifndef LIST_SUC_PRED
	typedef std::vector<Edge*> ListSuccPred;
	#define LIST_SUC_PRED
#endif

class NoeudGraph {
private:
	string Name; ///< Nom de la variable dans matlab : nom construit de la manière suivante : * "m_var_i_realname" ou i est un numéro et realname le nom original dans lequel on a enlevé les caractères à problème
	string originalName; ///< Nom de la variable dans le SFG fournit en entrée

protected:
	NoeudGraph(TypeNodeGraph typeNodeGraph, int Numero, string name); ///< Constructeur
	NoeudGraph(const NoeudGraph& other); ///< Do not call this! Use clone instead
	TypeNodeGraph typeNodeGraph; ///< Type de Noeud Graph (GFD, GFC, ELT_ES, OPS_ASS, OPR)
	int Numero; ///< Numero du Noeud Graph
	TypeEtat Etat; ///< Etat du noeud ( VISITE, ENTRAITEMENT, NONVISITE )

	int nbSucc; ///< Nombre de successeur
	int nbPred; ///< Nombre de predecesseurs

 /* Utilisation de vector pour les listes de Succ et Pred pour pouvoir avoir des accès direct aux edges correspondant
 *
 * Au niveau de la listPred des opérateur, la case correspond au numéro d'opérande. Le numInput de l'edge correspond avec la case du vecteur. Un opérateur a tout ces opérandes d'utilisé, il ne peut avoir de "trou" dans le vector hors transformations sur les graphes.
 *
 * Pour la listPred des NoeudData let listSucc de tous les noeud, les vectors sont utilisés comme des liste. L'ordre n'est pas important.
 *
 */
	vector<Edge*> listSucc;
	vector<Edge*> listPred;


	// ======================================= Constructeur - Destructeur
public:
	virtual ~NoeudGraph(); ///< Destructeur virtuel

	// ======================================= Methodes

	bool operator < (const NoeudGraph& lhs) {
		return Numero<lhs.getNumero();
	} ///< Vrai si le numero du NoeudGraph this est inférieur au numero du NoeudGraph lhs

	void setName(string Name) {
		this->Name = Name;
	} ///< Affecte le nom du NoeudGraph
	virtual string getName() const {
		return Name;
	} ///< Retourne le nom du NoeudGraph

	void setOriginalName(string originalName) {
		this->originalName = originalName;
	} ///< Affecte le nom d'origine de la variable
	string getOriginalName() {
		return this->originalName;
	} ///< Retourne le nom d'origine de la variable


	void setTypeNodeGraph(TypeNodeGraph typeNodeGraph) {
		this->typeNodeGraph = typeNodeGraph;
	} ///< Affecte le type de Noeud
	TypeNodeGraph getTypeNodeGraph() {
		return typeNodeGraph;
	} ///< Retourne le type de Noeud

	void setNumero(int Numero) {
		this->Numero = Numero;
	} ///< Affecte le numero du Noeud
	int getNumero() const {
		return Numero;
	} ///< Retourne le numero du Noeud

	void setEtat(TypeEtat Etat) {
		this->Etat = Etat;
	} ///< Affecte l'etat du noeud ( VISITE, ENTRAITEMENT, NONVISITE )*/
	TypeEtat getEtat() {
		return Etat;
	} ///< Retourne l'etat du noeud ( VISITE, ENTRAITEMENT, NONVISITE )	*/

	void setNbSucc(int nb) {
		nbSucc = nb;
	} ///< edite nombre de successeur !! UNSAFE METHODS
	int getNbSucc() {
		return nbSucc;
	} ///< Retourne le nombre de successeur

	void setNbPred(int nb) {
		nbPred = nb;
	} ///< edite nombre de listPred  !! UNSAFE METHODS
	int getNbPred() {
		return nbPred;
	} ///< Retourne le nombre de listPred

	void addSuccEdge(Edge* e); ///< Ajoute e dans la liste des edges successeur
	void addPredEdge(Edge* e); ///< Ajoute e dans la liste des edges prédecesseur

	Edge* getEdgeSucc(int i); ///< Return Edge element pointer of this successor list with i like index
	Edge* getEdgePredTransformation(int i); ///< Return Edge element pointer of this predecessor which have like node input number i. Use when graph transformations. See exemple to the implementation of this function.
	Edge* getEdgePred(int i); ///< Return Edge element pointer of this predecessor which have like node input number i.

	Edge* getEdgeSucc(NoeudGraph* nGraph);	///< Return edge pointer from this to nGraph if nGraph is successor of this, else NULL
	Edge* getEdgePred(NoeudGraph* nPred);	///< Return edge pointer from nGraph to this if nGraph is predecessor of this, else NULL


	void setNumInputEdgePred(int i, int Val); ///< Initialise le numero du Noeud predecesseur i
	int getNumInputEdgePred(int i); ///< Renvoi le numero du Noeud predecesseur i

	NoeudGraph * getElementSucc(int i); ///< Renvoi le pointeur sur le successeur i
	NoeudGraph * getElementPredTransformation(int i); ///< Renvoi le pointeur sur le predecesseur correspondant au numero Input i du noeud. A utilier lors des transformations de graph. Voir exemple dans l'implementation de cette fonction
	NoeudGraph * getElementPred(int i); ///< Renvoi le pointeur sur le predecesseur correspondant au numero Input i du noeud

	void removeEdge(NoeudGraph* NGraph); ///< Removes the Edge which has a way like this->NGraph
	void removeSuccPred(NoeudGraph* Init, NoeudGraph* Fin); ///< Removes edges between, this, Init and Fin

	// DO NOT USE! UNSAFE METHODS... (should not be public)
	void setListeSucc(Edge * element, int i);///< ajoute la liste de successeur !! UNSAFE METHODS
	vector<Edge*>* getListeSucc() {
		return &listSucc;
	} ///< Renvoi la liste de successeur !! UNSAFE METHODS
	void setListePred(Edge * element, int i); /// ajoute la liste de listPred !! UNSAFE METHODS
	vector<Edge*>* getListePred() {
		return &listPred;
	} ///< Renvoi la liste de listPred !! UNSAFE METHODS


	virtual NoeudGraph* clone() = 0;

	bool isNodeIsolate(); ///< Retourne true si le noeud est isole: ni succ, ni pred
	bool isNodeSink(); ///< typeVarFonction determinant si un noeud est une Racine du graphe GFL


	virtual void edgeDirectedTo(NoeudGraph * Tete, int numInput = -1); ///< Cree un arc dirige vers Tete
	//void edgeDirectedTo(NoeudGraph * Tete, Format* FormatArc, int numInput = -1); ///< Cree un arc dirige vers Tete(Data) (Utilise pour les GFD)
	void edgeDirectedTo(NoeudGraph * Tete, PseudoLinearFunction* Mfct); ///< Cree un arc dirige vers Tete (Utilise pour les GFL)
	//void edgeFromOf(NoeudGraph * Queue, int numInput = -1); ///< Cree un arc venant de Queue
	virtual void edgeFromOf(NoeudGraph * Queue, int numInput = -1); ///< Cree un arc venant de Queue vers this(OPS (Utilise pour les GFD)
	virtual void edgeFromOf(NoeudGraph* Queue, PseudoLinearFunction* Mfct); ///< Cree un arc venant de Queue (Utilise pour les GFL)
	void edgeDirectedToRapide(NoeudGraph * Tete, int numInput = -1); ///< Cree un arc dirige vers Tete (ne teste pas si il existe deja)
	void edgeDirectedToRapide(NoeudGraph * Tete, PseudoLinearFunction* Mfct); ///< Cree un arc dirige vers Tete (Utilise pour les GFL)
	void edgeFromOfRapide(NoeudGraph * Queue, int numInput = -1); ///< Cree un arc venant de Queue (ne test pas si il existe deja)

	void deleteEdgeDirectedTo(NoeudGraph * Tete); ///< Enleve un arc dirige vers Tete
	void EnleveedgeFromOf(NoeudGraph * Queue); ///< Enleve un arc venant de Queue

	// === ???

	//virtual LinearFonc* calculFT(LinearFonc *FT){return NULL;}


	virtual void addEdge(NoeudGraph* NGraph, int numInput) {
	} ///< Méthode virtuel
	virtual void addEdge(NoeudGraph* NGraph, PseudoLinearFunction* Mfct) {
	} ///< Méthode virtuel
	virtual void addEdgeRapide(NoeudGraph* NGraph, PseudoLinearFunction* Mfct) {
	} ///< Méthode virtuel

	void addEdge(NoeudGraph* NGraph); ///< Add a new Edge element between this and NGraph
	void addEdgeRapide(NoeudGraph* Tete, int numInput); ///< Add a new Edge element between this and NGraph

	bool isSucc(NoeudGraph* NGraph); ///< True if NGraph is a successor of this, false otherwise
	bool isPred(NoeudGraph* NGraph); ///< True if NGraph is a predecessor of this, false otherwise

	ListSuccPred::iterator searchSuccPred(NoeudGraph * NGraph, ListSuccPred * Liste);
	int searchPred(NoeudGraph * NGraph); ///< Determine if NGraph is a predecessor of this and return version iterator pointer of predecessor list element */
	int searchSucc(NoeudGraph * NGraph); ///< Determine if NGraph is a successor of this and return version iterator pointer of successor list element */

	/*
	 * typeVarFonction d'affichage virtuelle du Noeud et de ces Suc Pred dans un ficher(fic), ou sur ecran
	 */
	/*virtual void NoeudGraph::print(FILE * f = stdout) {
	 printInfo(f);
	 printListSucc(f);
	 printListPred(f);
	 }*/

	// === GenerateCalcuclRSBQ.cc
	int preCalculWFPsfgToWFPsfgeff_TriBrGen(list<NoeudGraph *> * listTriBr);
	int preCalculWFPsfgToWFPsfgeff_TriBrGen_SmallSize(list<NoeudGraph *> * listTriBr);
	int preCalculWFPsfgToWFPsfgeff_TriBrGen_SmallSizeCpp(list<NoeudGraph *> * listTriBr);

	// === Gfdprint.cc
	virtual void print(FILE * f = stdout); ///< typeVarFonction d'affichage virtuelle du Noeud et de ces Suc Pred dans un ficher(fic), ou sur ecran
	virtual void printInfo(FILE *); ///< typeVarFonction d'affichage virtuelle du Noeud dans un ficher(fic), ou sur ecran(si fic == NULL), sur une seule ligne
	void printListSucc(FILE *fic); ///< Ecriture de la liste des listSucc	(Utiliser par Gfd::print())
	void printListPred(FILE *fic); ///< Ecriture de la liste des listPred	(Utiliser par Gfd::print())

	virtual void printVCG(FILE *) = 0; ///< TypeVarFonction d'affichage virtuelle au format VCG (Utiliser par Gfd::printVCG())
	virtual void printDOTNode(FILE *f, bool noiseEval) = 0; ///< Fonction virtuelle permettant la definition des differents type de noeud et de leur propriété au format DOT
	virtual void printDOTEdge(FILE *f) = 0; ///< Fonction virtuelle permettant la création des arc vers les successeurs du noeud et de leurs propriétées au format DOT
	void printListSuccVCG(FILE *fp); ///< Impression de la liste des listSucc	au format VCG (Utiliser indirectement par printVCG())

};

#endif // _NOEUDGRAPH_HH_
