/**
 *  \file Gfd.hh
 *  \author Daniel Menard, Jean-Charles Naud
 *  \date   31.01.2002
 *  \class Gfd
 *  \brief Classe definissant un Graphe Flot de Donnees (GFD)
 */

#ifndef _GFD_HH_
#define _GFD_HH_

// === Bibliothèques standard
#include <list>
#include <vector>
#include <string>
#include <sstream>              //pour convertir un int en string (atoi pas ANSI^^)

#include <graph/dfg/EdgeGFD.hh>
#include <graph/dfg/NoeudInitFin.hh>
#include "graph/toolkit/AdjacentMatrix.hh"
#include "graph/Graph.hh"
#include "graph/lfg/GFL.hh"
#include "graph/tfg/GFT.hh"
#include "graph/dfg/operatornode/NoeudOps.hh"
#include "graph/dfg/datanode/NoeudSrcBruit.hh"
#include "graph/toolkit/TypeVar.hh"
#include "utils/graph/Block.hh"
// === Forward Declaration

// === Namespaces
using namespace std;

/**
 * \class NoeudData
 * \brief Classe définisant les Gfd
 *
 *  Classe de base de manipulation de la structure Gfd
 */
class Gfd: public Graph {
private:
	int numSrcNode;

protected:

	vector<NoeudSrcBruit*> tabNoiseSourcesNode; ///< list containing all noise sources introduced in GFD
	map<int, Block> m_blocks; ///< Liste des blocks identifiés dans le graphe


	list<string> listeStrWFP_sfg; 	///< (ComputePb) list contenant les sous expressions pour le calcul des WFP_sfg
	list<string> listeStrWFP_sfg_eff;	///< (ComputePb) list contenant les sous expressions pour le calcul des WFP_sfg_eff



public:
	//TODO Problème si il y a plusieurs sortie!!
	//Block& getOutputBlock() {
	//	return getNoeudFin()->getElementPred(0)->getBlock();
//
//	}///< Retourne le block du noeud de sortie
	// ======================================= Constructeur - Destructeur
	Gfd(bool NoiseEval); ///< Constructeur
	virtual ~Gfd(); ///< Destructeur

	// ======================================= Methodes

	/*vector<NoeudSrcBruit*> * gettabNoiseSourcesNode() {
		return tabNoiseSourcesNode;
	}
	void settabNoiseSourcesNode(vector<NoeudSrcBruit*> *tabNoiseSourcesNode) {
		this->tabNoiseSourcesNode = tabNoiseSourcesNode;
	}*/

	int getNbNoiseNode(){
		return (int) tabNoiseSourcesNode.size();
	}

	NoeudSrcBruit* getNoiseNode(int i){
		return tabNoiseSourcesNode[i];
	}
	void addNoiseNode(NoeudSrcBruit* node){ // TODO A supprimer quand le système d'ajout de noeud au différents graph sera refait NS
		tabNoiseSourcesNode.push_back(node);
	}
	void deleteNoiseNode(NoeudSrcBruit* node); // TODO A supprimer quand le système d'ajout de noeud au différents graph sera refait NS

	int getNumSrcNode(){
		return numSrcNode;
	}

	void incNumSrcNode(){
		numSrcNode = numSrcNode + 1;
	}

	void addWFPSFG(string s){
		listeStrWFP_sfg.push_back(s);
	}
	void addWFPSFGEff(string s){
		listeStrWFP_sfg_eff.push_back(s);
	}

	list<string>::iterator getBeginWFPSFG() {
		return listeStrWFP_sfg.begin();
	}
	list<string>::iterator getEndWFPSFG() {
		return listeStrWFP_sfg.end();
	}

	list<string>::iterator getBeginWFPSFGEff() {
		return listeStrWFP_sfg_eff.begin();
	}
	list<string>::iterator getEndWFPSFGEff() {
		return listeStrWFP_sfg_eff.end();
	}

	map<int, Block>& getBlocks() {
		return m_blocks;
	} ///< Retourne la liste des blocks identifiés dans le Gfd*/

	NoeudGraph * getElementGFD(const string& name); ///< Acces au noeud de nom name, pas de test si plusieurs noeuds ont le meme nom

    void addOPNode(NoeudOps *node);

	NoeudData * addNoeudData(string originalName, TypeVar * typeVariable); ///< Fonction d'ajout d'un noeudData dans le Gfd
	NoeudData * addNoeudData(string originalName, TypeVar * typeVariable, TypeSignalBruit typeSignalBruit); ///< Fonction d'ajout d'un noeudData dans le Gfd
	void addNoeudData(NoeudData * NData); ///< Fonction d'ajout d'un noeudData dans le Gfd
	void addNoeudSrcBruit(NoeudData* nodeData); ///< Fonction d'ajout d'un NoeudSrcBruit dans le Gfd
	void addNoeudSrcBruit(NoeudOps* nodeOps); ///< Fonction d'ajout d'un NoeudSrcBruit dans le Gfd
	NoeudSrcBruit * addNoeudSrcBruitCopie(NoeudSrcBruit* node); ///< Copie et node et ajoute la copie dans le graphe

	void addVarIntNodes(); ///< Rajoute des noeuds intermédiaires entre deux noeuds opérations sur le graphe en entrée
    void resolveConstantSubTree();

	void printFormat(); /// Ecriture du format

	void deleteIsolatedNodeNoise(); ///< function which delete separated node got after grouping or some processing, and new numbering


	// === FusionGFD.cc
	void fusionGFD(Gfd * GFD2);

	// === XML
	void lectureFluxXML(const char* FichierXML, const char* FichierDTD); ///< Creation d'un Gfd à partir d'un fichier XML
	bool readProbas(const char* path); ///< Read the path file and fill in the data of blocks in the blockmap.
	bool readSimulatedNode(const char* path, string pathGeneratedFile); ///< Récupere les informations issue de la simulation effectué de IDFix-Conv nécessaire pour le traitement non LTI au sein de IDFix-Eval
	void GFD2XML(const char* nomfichier) const; //TODO Verifié si cela est utile

	// === GfdPath.cc
	void calculFT();

	// === GraphToOutputCFile.c
	void addTypeSimuNode(); ///< Determine and tag (TypeSimu) node which split the signal and noise part of the graph

	// === XmlGraph.cc
	void liaisonsSuccPredInitFin(void); ///< Methodes permetant de lier les Init et les Fin avec le reste du graph (finition du xml2gfd)

	// === GraphToOutputCFile.cc
	void genArithmeticOperation(string pathFile); ///< Determine les expressions arithmétiques de tout les types simu présent dans le graph

	int calculNombreSrcBruit(); ///< Methodes comptant le nombre de sources de bruit
	int calculNombreKQAssocieSrcBruit(); ///< Methodes comptant le nombre max de KQ associes aux sources de bruit
	int calculNombreSFG(); ///< Methodes comptant le nombre d'Ops SFG ( num max SGF);
	int calculNombreCDFG();  ///< Methodes comptant le nombre d'Ops CDFG ( num max CDFG);

	// === GenrateConstantVariabe.cc
	void generateConstantVariable(); ///< Methode pour generer le fichier contenant les variables initialisées de l'application

	bool isLTI(); ///< Return true if the graph is LTI, false otherwise

	void initNumSrc(); ///< Function which update number of all NoeudSrc
	void displayNumSrc(); ///< Function which display number of all NoeudSrc

	Gfd* copie(); ///< Create a copy of a GFD
	void dynamicsPropagationCycle(Gfd* copieGfd); ///< Dynamic propagation for all data node even if there are cycle into the graph

	void sortListEdgePred(); ///< sort the list of pred edge of a ops node to have in begin, the 0 input and in end, the 1
};

#endif // _GFD_HH_
