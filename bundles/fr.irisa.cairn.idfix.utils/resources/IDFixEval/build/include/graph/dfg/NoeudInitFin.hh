/**
 *  \file NoeudInitFin.hh
 *  \author Daniel Menard, Jean-Charles Naud
 *  \date   31.01.2002
 *  \brief Classe definissant le noeud DEBUT d'un GFD et FIN
 */

#ifndef _NOEUDINITFIN_HH_
#define _NOEUDINITFIN_HH_

// === Bibliothèques standard
#include <iostream>

// === Bibliothèques non standard
#include "graph/NoeudGraph.hh"	// Heritage

//---------------------------------------------- NoeudInit ---------------------------------------------
/**
 * \class NoeudInit
 * \brief Classe  définisant NoeudInit
 */
class NoeudInit: public NoeudGraph {
public:
	// ======================================= Constructeur - Destructeur
	NoeudInit(int NumNoeud); /// Constructeur
	~NoeudInit(); /// Destructeur

	// ======================================= Methodes
	string getName() {
		return string("INIT");
	}
	NoeudGraph* clone() {
		return new NoeudInit(Numero);
	}
	void printFormat() {
		printf("Init Node");
	} /// Affiche le format, mais il n'y en a pas pour NoeudInit

	// === Gfdprint.cc
	void printInfo(FILE *fic); /// Ecriture des info du Noeud dans un ficher(fic), ou sur ecran(si fic == NULL))
	void print(FILE *);
	void 		addEdge(NoeudGraph* NGraph);
private:
	void printVCG(FILE*) {
	}
	void printDOTNode(FILE *, bool) {
		cerr << "Pas d'affichage DOT disponible pour le NoeudInit";
		exit(0);
	}
	void printDOTEdge(FILE *) {
	}
};

//---------------------------------------------- NoeudFin ---------------------------------------------
/**
 * \class NoeudFin
 * \brief Classe  définisant NoeudFin
 */
class NoeudFin: public NoeudGraph {
public:
	// ======================================= Constructeur - Destructeur
	NoeudFin(int NumNoeud); /// Constructeur
	~NoeudFin(); /// Destructeur

	// ======================================= Methodes
	string getName() {
		return string("FIN");
	}
	NoeudGraph* clone() {
		return new NoeudFin(this->Numero);
	}
	void printFormat() {
		printf("Fin Node");
	} /// Affiche le format, mais il n'y en a pas pour NoeudFin
	void 			addEdge(NoeudGraph* NGraph);

	// === Gfdprint.cc
	void printInfo(FILE *fic); /// Ecriture des info du Noeud dans un ficher(fic), ou sur ecran(si fic == NULL)) */
	void print(FILE *);
private:
	void printVCG(FILE*) {
	}
	void printDOTNode(FILE*, bool) {
		cerr << "Pas d'affichage DOT disponible pour le noeudFIN";
		exit(0);
	}
	void printDOTEdge(FILE*) {
	}
};

#endif // _NOEUDINITFIN_HH_
