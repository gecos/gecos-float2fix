/**
 *  \file NoeudSrcBruit.hh
 *  \author Mohammad DIAB , Jean-Charles Naud, Loic Cloatre
 *  \date
 *  \brief Classe definissant les noeuds representant une donnee dans un GFD
 */

#ifndef _NOEUDSRCBRUIT_HH_
#define _NOEUDSRCBRUIT_HH_

// === Bibliothèques standard
#include <iostream>
#include <fstream>
#include <string>

// === Bibliothèques non  standard
#include "graph/dfg/datanode/NoeudSrc.hh"	// Heritage
#include "utils/graph/Block.hh"

// === Namespaces
using namespace std;

/**
 * \class NoeudSrcBruit
 * \brief Classe définisant les nœuds de source de bruit
 *
 *  Classe définisant les nœuds de source de bruit utilisés pendant l'évaluation de la précision
 */
class NoeudSrcBruit: public NoeudData {
protected:
	vector<string> m_PasQuantif; ///< Pas de quantification (Q dans ComputePb)
	vector<string> m_KbitsElimine; ///< Nombre de bits elimines (K dans ComputePb)
	vector<string> m_ModeQuantif; ///< Mode de quantification
	vector<int> m_SigneMoyenne; ///< Signe de la moyenne, indique si le noeud a été déplace a travers un operateur- par ex
	vector<float> m_Constante; ///< Constante associé à chaque K et Q. Utilisé pour l'aggrégation des Br au niveau des multiplications par des constantes
	bool noiseOfSource;	///< Vrai si le Br est associé à une source lors de son ajout dans la phase NoiseSourceInsertion, nécessaire pour determiner quels Br doit être utilisé dans la détection des noeuds commun pour la réduction de cycle du GFL

    Block &m_block;

public:
	// ======================================= Constructeur - Destructeur
	NoeudSrcBruit(int NumNoeud, string Nom, TypeSignalBruit TSB, Block &block, bool noiseOfSource = false); /// Constructeur
	NoeudSrcBruit(const NoeudSrcBruit& other);
	~NoeudSrcBruit() {
	}
	; ///< Destructeur

    NoeudSrcBruit* clone();

	// ======================================= Methodes
    Block & getBlock() const{
        return m_block;
    }

	void addStepQuantif(string pasQuantif) {
		m_PasQuantif.push_back(pasQuantif);
	} ///< Affecte le pas de quantification
	string getNumStepQuantif(int numero); ///< Retourne le pas de quantification
	vector<string>& getTypeVarRefVectQuantif() {
		return m_PasQuantif;
	} ///< Retourne le pas de quantification


	int getNbSuppressKBits() {
		return m_KbitsElimine.size();
	}
	void addSuppressKBits(string KbitsElimine) {
		m_KbitsElimine.push_back(KbitsElimine);
	} ///< Affecte le nombre de bits éliminés
	string getNumSuppressKBits(int numero); ///< Retourne le nombre de bits éliminés
	vector<string>& gettypeVarRefVectKBits() {
		return m_KbitsElimine;
	} ///< Retourne le nombre de bits éliminés


	void addSigneMean(int signe) {
		m_SigneMoyenne.push_back(signe);
	} ///< Affecte le signe de la moyenne
	int getSigneMoyenne(int numero); ///< Retourne le signe de la moyenne
	vector<int>& gettypeVarRefVectSigneMean() {
		return m_SigneMoyenne;
	} ///< Retourne la reference sur le vecteur de SigneMoyenne


	void addModeQuantif(string ModeQuantif) {
		m_ModeQuantif.push_back(ModeQuantif);
	} ///< Affecte le type de quantification
	vector<string>& getModeQuantif() {
		return m_ModeQuantif;
	} ///< Retourne le nombre de bits éliminés
	string getNumModeQuantif(int numero); ///< Retourne le type de quantification

	void addConstante(float cste){
		this->m_Constante.push_back(cste);
	}
	int getNbConstante(){
		return this->m_Constante.size();
	}
	float getConstante(int i);
	void multConstante(float cste);
	void divConstante(float cste);

	void setNoiseOfSource(bool noiseOfSource){
		this->noiseOfSource = noiseOfSource;
	}///< Affecte noiseOfSource
	bool getNoiseOfSource(){
		return noiseOfSource;
	}///< Retourne noiseOfSource

	void inverseVectSigneMean(); ///< Méthode pour inverser tous les signe sur le vecteur de SigneMoyenne, lors de passage dans un operateur -

	void calculSumParamNoise(NoeudSrcBruit* noeudCible); ///< Methode pour effectuer la somme des parametres de bruit entre deux noeuds
	void calculSubtractionParamNoise(NoeudSrcBruit* noeudCible); ///< Methode pour effectuer la soustraction des parametres de bruit entre deux noeuds

	void setParamSrcNoise(NoeudOps *OpsOut, NoeudOps *OpsIn); ///< function which set attribute of data noise node corresponding of number eliminated bit between operator*/
	void setParamSrcNoiseJava(NoeudOps *OpsOut, NoeudOps *OpsIn); ///< function which set attribute of data noise node corresponding of number eliminated bit between operator*/
	void setParamSrcNoiseJniCC(NoeudOps *OpsOut, NoeudOps *OpsIn); ///< function which set attribute of data noise node corresponding of number eliminated bit between operator*/
	void setParamSrcNoiseInputJava(NoeudData *NData, NoeudOps *OpsIn); ///< function which set attribute of data noise node corresponding of number eliminated bit between operator and data input*/
	void setParamSrcNoiseInputCC(NoeudData *NData, NoeudOps *OpsIn); ///< function which set attribute of data noise node corresponding of number eliminated bit between operator and data input*/
	void setParamSrcNoiseInputJniCC(NoeudData *NData, NoeudOps *OpsIn); ///< function which set attribute of data noise node corresponding of number eliminated bit between operator and data input*/
	void displayParamNoise();///< Affiche les param de bruit

	void writeCalculMoments(fstream& fileCpp_pf); ///< Utilisées pour générer le fichier calculRSQB.cc definies dans GenerateCalculRSQBccFile.cc
	void writeCalculMoments_SmallSize(fstream& fileCpp_pf); ///< Utilisées pour générer le fichier calculRSQB.cc definies dans GenerateCalculRSQBccFile.cc
	void writeCalculMoments_SmallSizeCpp(fstream& fileCpp_pf); ///< Utilisées pour générer le fichier calculRSQB.cc definies dans GenerateCalculRSQBccFile.cc
	void writeCalculMomentsJava(fstream& fileCpp_pf); ///< Utilisées pour générer le fichier calculRSQB.cc definies dans GenerateCalculRSQBccFile.cc
	void writeCalculMomentsJniCC(fstream& fileCpp_pf); ///< Utilisées pour générer le fichier calculRSQB.cc definies dans GenerateCalculRSQBccFile.cc
	void printDOTNode(FILE *f, bool noiseEval); ///< Méthode permettant la représentation des NoeudOps en format dot
};

#endif // _NOEUDSRCBRUIT_HH_
