/**
 *  \file NoeudGFD.hh
 *  \author Daniel Menard, Jean-Charles Naud
 *  \date   06.02.2002
 *  \brief Classe generique definissant les noeuds  present dans un GFD
 */

#ifndef _NOEUDGFD_HH_
#define _NOEUDGFD_HH_

// === Bibliothèques standard
#include <iostream>
#include <string>
#include <list>
#include <map>

#include "graph/NoeudGraph.hh" // Heritage


// === Bibliothèques non standard
// === Forward Declaration
//class NoeudGFD;

class NoeudOps;
class NoeudData;
class NoeudSrcBruit;

// === Namespaces
using namespace std;

/**
 * \class NoeudGFD
 * \brief Definition de la classe NoeudGFD
 *
 */
class NoeudGFD: public NoeudGraph {
private:
	NoeudGFD(); // interdit
protected:
	TypeNodeGFD typeNodeGFD; ///< Type de Noeud du GFD ( OPS, DATA, INIT, FIN)
	TypeSignalBruit typeSignalBruit; ///< Type de noeud au niveau bruit ( SIGNAL, BRUIT)
	bool cloned; ///< Boolean indiquant si le noeud est deja clone
	NoeudGFD* ptrclone; ///< Pointeur sur le clone du noeud
	int numCDFG; ///< number in CDFG

	NoeudGFD(const NoeudGFD& other);
public:
	// ======================================= Constructeurs - Destructeurs
	NoeudGFD(TypeNodeGFD typeNodeGFD, int NumNoeud, TypeEtat Etat, string name); ///< Constructeur
	NoeudGFD(TypeNodeGFD typeNodeGFD, int NumNoeud, TypeEtat Etat, string name, TypeSignalBruit typeSignalBruit); ///< Constructeur
	virtual ~NoeudGFD(); ///< Destructeur virtuel

	// ======================================= Methodes
	void setTypeNodeGFD(TypeNodeGFD typeNodeGFD) {
		this->typeNodeGFD = typeNodeGFD;
	} ///< Initialise le type de noeud GFD ( OPS, DATA, INIT, FIN)
	TypeNodeGFD getTypeNodeGFD() {
		return typeNodeGFD;
	} ///< Renvoi le type de noeud GFD ( OPS, DATA, INIT, FIN)
	void setTypeSignalBruit(TypeSignalBruit typeSignalBruit) {
		this->typeSignalBruit = typeSignalBruit;
	} ///< Initialise le type de noeud au niveau bruit  ( SIGNAL, BRUIT)
	TypeSignalBruit getTypeSignalBruit() {
		return typeSignalBruit;
	} ///< Renvoi le type de noeud au niveau bruit  ( SIGNAL, BRUIT)

	void setInfoclone(bool clone) {
		this->cloned = clone;
	} ///< Initialise  le booleen indiquant si le noeud a été cloné
	bool isNodecloned() {
		return cloned;
	} ///< Renvoi le booleen indiquant si le noeud a été cloné

	void setPtrclone(NoeudGFD * ptrclone) {
		this->ptrclone = ptrclone;
	} ///< Initialise le pointeur sur le clone du noeud GFD
	NoeudGFD* getPtrclone() {
		return ptrclone;
	} ///< Renvoi le pointeur sur le clone du noeud GFD

	void setNumCDFG(int numCDFG) {
		this->numCDFG = numCDFG;
	} ///< set numberCDFG
	int getNumCDFG() const {
		return numCDFG;
	} ///< Return number in CDFG

	//TODO A peut supprimer, surement pareil que isNodeInput, isNodeOutput
	bool isNodeSource(); ///< Fonction determinant si un noeud est une Source du graphe GFD
	bool isNodeSink(); ///< Fonction determinant si un noeud est une Racine du graphe GFD


	// === Gfdprint.cc
	void print(FILE * f = stdout); ///< Ecriture du Noeud et de ces Suc Pred dans un ficher(f), ou sur ecran (Utilisé par Gfd::print())
	virtual void printInfo(FILE *) = 0; ///< Ecriture du Noeud dans un ficher(f), ou sur ecran (Utilisée indirectement par Gfd::print())


	virtual void printFormat() {
	} ///< Ecriture du format (Utilisée par Gfd::printFormat)

	// === virtual
	virtual DataDynamic propagationDynamique() {
		return DataDynamic();
	} ///< Methode virtuelle de propagation de la dynamique

	virtual void generateFixPtSpecification(map<int, vector<NoeudData*> > &mapNumCDFGListNodeData, map<int, vector<NoeudOps*> > &mapNumCDFGListNodeOps) {
		cout << "virtual class" << endl;
	} ///< function wich fill in vector of dataNode and OpsNode for the fixPtSpecif>

    virtual float resolveConstantSubTree() = 0;

	// T11
	virtual void moveNoiseSource(NoeudSrcBruit* noiseSourceNode) = 0;
	virtual list<NoeudData*> detectionBrCorrelated(list< list<NoeudData*> >* tabListBr) = 0;


	void edgeDirectedTo(NoeudGraph * Tete, int numInput = -1); ///< Cree un arc venant de this vers queue

	void edgeFromOf(NoeudGraph * Queue, int numInput = -1); ///< Cree un arc venant de Queue vers this
	void addEdge(NoeudGraph * NGraph, int numInput); /// Add a EdgeGFD element between this and NGraph, the format is save too
};

#endif // _NOEUDGFD_HH_
