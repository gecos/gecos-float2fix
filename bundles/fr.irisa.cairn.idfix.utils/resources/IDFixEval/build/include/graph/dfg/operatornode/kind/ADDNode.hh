#ifndef __DFG_OP_ADDNODE_HH__
#define __DFG_OP_ADDNODE_HH__

#include "graph/dfg/operatornode/NoeudOps.hh"

namespace gfd {
    class ADDNode : public NoeudOps {
        protected:
            LinearFunction* computeRuleLinearFunction(unsigned int size, LinearFunction* operand[]); 

        public:
            ADDNode(Block &block, unsigned int input_number = 2); 
            ADDNode(const ADDNode &other);

            ADDNode* clone();

            // Global
            TypeLti isLTI(); ///< Return if the operator is LTI from operands
            float resolveConstantSubTree();

            // Dynamic determination
            DataDynamic dynamicRule(unsigned int size, DataDynamic operand[]); ///< Apply the dynamic rules of the operator from the dynamic of his operand

            // Noise model and noise system behaviour determination
            NoeudData* arithmeticOperationGeneration(fstream & matfile); ///< Generate the athmetic operation needed by the non-lti system for this operator
            void moveNoiseSource(NoeudSrcBruit* noiseNode); ///< Move, if possible, through the operator the noise node given
            void noiseModelInsertion(Gfd *gfd); ///< Insert the noise model of the operator
            
            // Back End (noise function generation)
            string WFPEffDetermination(); ///< Return the number of bit effetive of this operator
            string KDetermination(); ///< Return the K bit eleminated of this operator

	 void rechercheDernierPointCommun(GraphPath * circuit, NoeudData * raciceGraphe, vector<NoeudData *>*listNoeudAdemanteler, bool demiGrapheParcouru);

            std::string toString(){return "+";}
            std::string getDotColor(){return "orange";}
    };
}


#endif

