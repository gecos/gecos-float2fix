/**
 *  \file NoeudSrc.hh
 *  \author Nicolas Simon
 *  \date  Jul 6, 2010
 *  \brief Classe definissant les noeuds representant une donnee dans un GFD
 */

#ifndef _NOEUDSSCR_HH_
#define _NOEUDSSCR_HH_

// === Bibliothèques standard
#include <iostream>
#include <fstream>
#include <string>

#include "graph/dfg/datanode/NoeudData.hh"	// Heritage

// === Bibliothèques non  standard
// === Namespaces
using namespace std;

/**
 * \class NoeudSrc
 * \brief Classe définisant les nœuds de source
 *
 *  Classe définisant les nœuds de source pour l'évaluation de la dynamique
 */
class NoeudSrc: public NoeudData {
public:
	// ======================================= Constructeurs - Destructeurs
	NoeudSrc(TypeNodeData typeNodeData, int NumNoeud, string Nom, TypeVar * TypeVariable, string originalName = "");///< Constructeur
    NoeudSrc(const NoeudData &other);
    NoeudSrc(const NoeudSrc &other);
	virtual ~NoeudSrc();///< Destructeur

	// ======================================= Methodes
	void printVCG(FILE *fp) {
		(this)->printSrcVCG(fp);
	} ///< Function makes for writing node information to vcg format
	void printSrcVCG(FILE *fp); ///< Function makes for writing node information to vcg format
	void printInfoSrcVCG(FILE *fp); ///< Function makes for writing node information to vcg format
	void printDOTNode(FILE *f, bool noiseEval); ///< Methode permettant la definition et de ses propriété d'affichage des noeud Src au format DOT

    NoeudSrc* clone();
};

#endif // _NOEUDSSCR_HH_
