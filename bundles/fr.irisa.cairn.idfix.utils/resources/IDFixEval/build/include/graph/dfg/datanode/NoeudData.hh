/**
 *  \file NoeudData.hh
 *  \author Daniel Menard, Jean-Charles Naud
 *  \date   31.01.2002
 *  \brief Classe definissant les noeuds representant une donnee dans un GFD
 */

#ifndef _NOEUDDATA_HH_
#define _NOEUDDATA_HH_

// === Bibliothèques standard
#include	<iostream>
#include	<string>
#include	<list>

#include "graph/dfg/NoeudGFD.hh"	// Heritage

// === Bibliothèques non standard
#include "graph/toolkit/Types.hh"		// Types
#include "graph/toolkit/TypeVar.hh"
// === Forward Declaration
class NoeudSrc;
class GraphPath;
class NoeudSrcBruit;

// === Namespaces
using namespace std;

/**
 * \class NoeudData
 * \brief Classe définisant les nœuds de données
 *
 *  Classe définisant les nœuds de données pouvant être des variables d'entrée, de sortie, interne, ...
 */
class NoeudData: public NoeudGFD {

protected :
	int numSrc; ///<< Numero nécéssaire au niveau de la création des varLinearFonc et des transformations du GFL. Celui ci est aussi utilisé de la création du computePb. Comme une fonction Lineaire peut être entre n'importe quels noeuds Data, ce lui-ci doit se trouver dans la classe mère. Init a -1 si pas nécéssaire. 
	TypeNodeData typeNodeData; ///< Type de la variable (DATA_BASE,	DATA_ARRAY,DATA_SRC_BRUIT)
	TypeVar * typeVar; ///< Definition des differentes caracteristiques de la donnees
	float Valeur; ///< Valeur de la donnee si c'est une constante : max(|x|)
	bool noeudDemantele; ///< Indique si le noeud a été créé lors d'un dementellment
	bool typeSimu; ///< Indique si le noeud doit etre un float ou bien un typeSimu pour la partie non linéaire
	bool srcSignal; ///< Indique si le noeud est un srcSignal. Noeud rajouté lors lors des pattern de bruit en T11
	bool variableWidth; ///< Indique si la largeur de la data est variable (au choix de l'utilisateur) ou non. non variable par défaut;
	DynamicProcess dynamicProcess; ///< Tag indiquant des informations spécifique sur le type de calcul de la dynamique appliqué sur ce noeud
	float probDeb; ///< Probabilité de Debordement du noeud
	DataDynamic dataDynamic; ///< Dynamique de la donnee
	int numAffect; ///< Numero d'affection si le noeud a été simulé pour la partie non LTI. Utilisé pour la correspondance entre le vecteur d'échantillon fourni en paramétre
	string indexArray; ///< Index représenté en string de la variable, vide si ce n'est pas un tableau (Tableau ou variable au niveau de la simulation pour la partie non LTI)

	LinearFunction* m_ptrFonctionLineaire;///< pointeur de sauvegarde d'une fonction lineaire nécessaire pour la determination des fonction linéaire du systeme

public:
	// ======================================= Constructeurs - Destructeurs
	NoeudData(TypeNodeData typeNodeData, int NumNoeud, string Nom, TypeVar * TypeVariable, string originalName = ""); ///< Constructeur
	NoeudData(TypeNodeData typeNodeData, int NumNoeud, string Nom, TypeVar * TypeVariable, TypeSignalBruit typeSignalBruit, string originalName = ""); ///< Constructeur
	NoeudData(const NoeudData& other); ///< Constructeur par copie
	~NoeudData(); ///< Destructeur

	// ======================================= Methodes
	void setNumSrc(int numSrc) {
		this->numSrc = numSrc;
	} ///< Affecte le Numero Src
	int getNumSrc() {
		return numSrc;
	} ///< Retourne le Numero Src
	
	void setProbDeb(float probDeb){
		this->probDeb = probDeb;
	} ///< Affecte la probabilité de débordement 
	float getProbDeb(){
		return probDeb;
	} ///< Retourne la probabilité de débordement

    void setTypeNodeData(TypeNodeData type) {
		typeNodeData = type;
	} ///< Affecte le type du noeud DATA
	TypeNodeData getTypeNodeData() {
		return typeNodeData;
	} ///< Retourne le type du noeud DATA

	void setTypeVar(TypeVar * typeVar) {
		this->typeVar = typeVar;
	} ///< Initialisation du type de donnee
	TypeVar* getTypeVar() {
		return typeVar;
	} ///< Acces au pointeur sur le type d'operation

	void setDataDynamic(DataDynamic ValDyn) {
		this->dataDynamic.valMin = ValDyn.valMin;
		this->dataDynamic.valMax = ValDyn.valMax;
	} ///< Initialisation de la valeur de la dynamique
	//xxx classe surchargé
	virtual DataDynamic getDataDynamic() {
		return dataDynamic;
	} ///< Renvoi de la valeur de la dynamique
	void displayDataDynamic() {
		cout << "ValMin : " << dataDynamic.valMin << endl;
		cout << "ValMax : " << dataDynamic.valMax << endl;
	} ///< Affiche la dynamique d'un Noeud

	void setValeur(float Valeur) {
		this->Valeur = Valeur;
	} ///< Affecte la valeur de la variable
	virtual float getValeur() {
		return Valeur;
	} ///< Retourne la valeur de la variable

	void setNumAffect(int numAffect){
		this->numAffect = numAffect;
	} ///< Affecte le numéro d'affectation obtenu via la simulation
	int getNumAffect(){
		return this->numAffect;
	}///< Retourne le numéro d'affectation obtenu via la simulation

	void setVariableWidth(bool variableWidth){
		this->variableWidth = variableWidth;
	} ///< Affecte variableWidth
	bool getVariableWidth(){
		return this->variableWidth;	
	} ///< Retourne variableWidth 

	void setTypeSimu(bool typeSimu) {
		this->typeSimu = typeSimu;
	}
	bool getTypeSimu() const {
		return typeSimu;
	}

	void setSrcSignal(bool srcSignal) {
		this->srcSignal = srcSignal;
	}
	bool getSrcSignal() const {
		return srcSignal;
	}

	DynamicProcess getDynProcess() {
		return dynamicProcess;
	} ///< Retourne le tag indiquant le type de calcul dynamique appliqué
	void setDynProcess(DynamicProcess dynamicProcess) {
		this->dynamicProcess = dynamicProcess;
	} ///< Affecte le tag indiquant le type de calcul dynamique appliqué

	void setIndexArray(string index){
		this->indexArray = index;
	}///< Affecte l'index du tableau fourni via la simulation
	string getIndexArray(){
		return indexArray;
	}///< Retourne l'index du tableau fourni via la simulation

	void setPtrFonctionLineaire(LinearFunction* ptrFonctionLineaire){
		this->m_ptrFonctionLineaire = ptrFonctionLineaire;
	} ///< Attribue m_ptrFonctionLineare
	LinearFunction* getPtrFonctionLineaire(){
		return m_ptrFonctionLineaire;
	}///< Retourne m_ptrFonctionLineare

	void setNoeudDemantele(bool a){
		this->noeudDemantele = a;
	}

	bool isNodeInput(); ///< Indique si le noeudData est une entree (VAR_ENTREE ?)
	bool isNodeOutput(); ///< Indique si le noeudData est une sortie (VAR_SORTIE ou CONST_SORTIE ?)
	bool isNodeSystemOutput(); ///< Indique si le noeudData est une sortie systeme (VAR_SORTIE et ..... yn ?)
	bool isNodeVarInt(); ///< Indique si le noeudData est une variable intermediaire (VAR_INT ?)
	bool isNodeVar();
	bool isNodeConst(); ///< Indique si le noeudData est une constantes (CONST ou CONST_SORTIE ?)
	bool isNodeSourceBruitBR(); ///< renvoie true si le noeud est une source 'Br'isNodeSourceBruitBR
	bool isNodeNoeudDemantele() {
		return noeudDemantele;
	} ///< renvoie true si le noeud est un noeud cree lors d'un demantelement


	// === Clonage.cc
	NoeudData * clone(); ///< Cloning Function of NoeudData node

	// === GraphToOutputCFile.cc
	NoeudData* genArithmeticOperation(fstream & matFile); ///< Detecte tout les noeuds typeSimu est lance une récursivité sur ce noeud pour obtenir son opération arithmétique qui permet d'obtenir sa valeur en fonction des variables simulées

	bool belongsToTheCircuit(GraphPath * circuit); ///< Regarde si le Noeud appartient au circuit

	// T11
	void moveNoiseSource(NoeudSrcBruit* noiseSourceNode);///<  function which moves noise sources if possible
	list<NoeudData*> detectionBrCorrelated(list< list<NoeudData*> >* tabListBr);

    float resolveConstantSubTree();
	TypeLti isLTI(); ///< Use for determine if a Graph is LTI or not

	// === Gfdprint.cc
	virtual void printInfo(FILE *fic); ///<  (virtuel mais defini) Methode d'ecriture des infos du noeud dans un ficher(fic), ou sur ecran(si fic == NULL))

	void printVCG(FILE *fp) {
		(this)->printDataVCG(fp);
	} ///< Methode permettant la definition et de ses propriété d'affichage des noeud Data au format VCG

	void printDOTNode(FILE *f, bool noiseEval); /// Methode permettant la definition et de ses propriété d'affichage des noeud Data au format DOT
	void printDOTEdge(FILE *f); /// Methode permettant la création des arcs vers les successeurs du noeud Data et leurs propriétés au format DOT
	void printDataVCG(FILE *fic); /// Methode global d'ecriture des infos du noeud Data au format VCG
	virtual void printInfoDataVCG(FILE *fp); /// Methode d'ecriture des infos du noeud Data au format VCG

	void rechercheDernierPointCommun(GraphPath * circuit, NoeudData * raciceGraphe, vector<NoeudData *>*listNoeudAdemanteler, bool demiGrapheParcouru);
	void rechercheDernierPointCommunFromSimpleGraph(GraphPath* circuit, NoeudData* raciceGraphe, vector<NoeudData*>* listNoeudAdemanteler, bool demiGrapheParcouru); ///< Methode qui recherche le point commun entre un circuit et un graphe en parcourant celui-ci (a partir d'un graphe qui a été simplfié via la graphSimplificationToCycleDismantling

	 LinearFunction* calculLinearFoncLTI(); ///< Fonction récursive permettant le calcul des fonction lineaire du systeme (LTI seulement)
	 LinearFunction* calculLinearFoncNonLTI(); ///< Fonction récursive permettant le calcul des fonction lineaire du systeme (NONLTI seulement)


};

#endif // _NOEUDDATA_HH_
