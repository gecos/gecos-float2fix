#ifndef __DFG_OP_MULNODE_HH__
#define __DFG_OP_MULNODE_HH__

#include "graph/dfg/operatornode/NoeudOps.hh"

namespace gfd {
    class MULNode : public NoeudOps {
        protected:
            LinearFunction* computeRuleLinearFunction(unsigned int size, LinearFunction* operand[]); 

        public:
            MULNode(Block &block, unsigned int input_number = 2); 
            MULNode(const MULNode &other);

            MULNode* clone();
            
            // Global
            TypeLti isLTI(); ///< Return if the operator is LTI from operands
            float resolveConstantSubTree();

            // Dynamic determination
            DataDynamic dynamicRule(unsigned int size, DataDynamic operand[]); ///< Apply the dynamic rules of the operator from the dynamic of his operand

            // Noise model and noise system behaviour determination
            NoeudData* arithmeticOperationGeneration(fstream & matfile); ///< Generate the athmetic operation needed by the non-lti system for this operator
            void moveNoiseSource(NoeudSrcBruit* noiseNode); ///< Move, if possible, through the operator the noise node given
            void noiseModelInsertion(Gfd *gfd); ///< Insert the noise model of the operator
	    void rechercheDernierPointCommun(GraphPath * circuit, NoeudData * raciceGraphe, vector<NoeudData *>*listNoeudAdemanteler, bool demiGrapheParcouru);
            
            // Back End (noise function generation)
            string WFPEffDetermination(); ///< Return the number of bit effetive of this operator
            string KDetermination(); ///< Return the K bit eleminated of this operator

            std::string toString(){return "*";}
            std::string getDotColor(){return "pink";}
    };
}

#endif

