/**
 *  \file EdgeGFD.hh
 *  \author Daniel Menard, Nicolas Simon
 *  \date   25.06.2010
 *  \brief Class which mean edges of GFD
 */

#ifndef _EDGEGFD_HH_
#define _EDGEGFD_HH_

// === Bibliothèques non standard
#include "graph/Edge.hh" // Heritage


/**
 * \class EdgeGFD
 * \brief Classe définisant les EdgeGFD
 *
 * Classe définisant les nœuds de données pouvant être des variables d'entrée, de sortie, interne, ...
 */
class EdgeGFD: public Edge {
private:
protected:

public:
	// ======================================= Constructeurs - Destructeurs
	EdgeGFD(NoeudGraph* Npred, NoeudGraph* nodeSucc, int numInput); /// Constructeur
	~EdgeGFD(); /// Destructeur
};

#endif // _EDGEGFD_HH_
