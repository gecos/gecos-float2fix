/**
 *  \file NoeudOps.hh
 *  \author Daniel Menard, Jean-Charles Naud
 *  \date   31.01.2002
 *  \brief Classe definissant les noeuds representant une operation dans un GFD
 */

#ifndef _NOEUDOPS_HH_
#define _NOEUDOPS_HH_

#include "graph/dfg/Gfd.hh"
#include "graph/dfg/NoeudGFD.hh"	// Heritage
#include <string>
#include <list>

#include "utils/graph/Block.hh"

using namespace std;

/**
 * \class NoeudOps
 * \brief Classe définisant les nœuds d'opération
 *
 *  Classe définisant les nœuds d'opération aritmétique ou non
 */
class NoeudOps: public NoeudGFD {
protected:
	int m_inputNumber; ///< Nombre d'entree
	DataDynamic m_dynamic; ///< Output dynamic
	int m_SFGNumber; ///< Field got with input XML file
	Block &m_block; ///< ID of the block this node is inside of

	NoeudOps(int NumNode, int m_inputNumber, Block &block, string name); ///< Constructeur
	NoeudOps(int NumNode, TypeSignalBruit typeSignalBruit, int m_inputNumber, Block &block, string name); ///< Constructeur
	NoeudOps(const NoeudOps& other);

    virtual string getDotColor(){return "";}
    virtual LinearFunction* computeRuleLinearFunction(unsigned int size, LinearFunction* operand[]) = 0;
public:
	// ======================================= Constructor/Destructor
	virtual ~NoeudOps(); ///< Destructeur
    virtual NoeudOps* clone() = 0;
	
	// ======================================= Getter/Setter 
    Block &getBlock() const {
		return m_block;
	}///< Retourne l'ID du block

	/*void setTypeNoeudOps(TypeNoeudOps typeNoeudOps) {
		this->typeNoeudOps = typeNoeudOps;
	} ///< Affectation du type du noeud OPS
	TypeNoeudOps getTypeNoeudOps() {
		return typeNoeudOps;
	} ///< Indique le type du noeud OPS*/

	void setNbInput(int NbIn) {
		m_inputNumber = NbIn;
	} ///< Affectation du nombre d'entrée de l'opération
	int getNbInput() {
		return m_inputNumber;
	} ///< Retourne le nombre d'entrée de l'opération

	void setOutputDynamic(DataDynamic m_dynamic) {
		this->m_dynamic = m_dynamic;
	} ///< setter of output dynamic
	void setOutputDynamic(double val_min, double val_max) {
		this->m_dynamic.valMin = val_min;
		this->m_dynamic.valMax = val_max;
	} ///< Setter of output dynamic
	DataDynamic getOutputDynamic() const {
		return m_dynamic;
	} ///< getter of output dynamic

	void setNumSFG(int m_SFGNumber) {
		this->m_SFGNumber = m_SFGNumber;
	} ///< Setter of NumSFG
	int getNumSFG() const {
		return m_SFGNumber;
	} ///< Getter of NumSFG

    
	// ======================================= Virtual methods
    // Global
	virtual TypeLti isLTI() = 0; ///< Return if the operator is LTI from operands
    virtual float resolveConstantSubTree() = 0;
    
    // Dynamic determination
    virtual DataDynamic dynamicRule(unsigned int size, DataDynamic operand[]) = 0; ///< Apply the dynamic rules of the operator from the dynamic of his operand

    // Noise model and noise system behaviour determination
    virtual NoeudData* arithmeticOperationGeneration(fstream & matfile) = 0; ///< Generate the athmetic operation needed by the non-lti system for this operator
	virtual void moveNoiseSource(NoeudSrcBruit* noiseSourceNode) = 0;  ///< Insert the noise model of the operator
    virtual void noiseModelInsertion(Gfd *gfd) = 0; ///< Move, if possible, through the operator the noise node given
    
    // Back End (noise function generation)
    virtual string WFPEffDetermination() = 0; ///< Return the number of bit effetive of this operator
    virtual string KDetermination() = 0; ///< Return the K bit eleminated of this operator

    virtual string toString() = 0;
    virtual bool isPHINode(){return false;}

	virtual void rechercheDernierPointCommun(GraphPath * circuit, NoeudData * raciceGraphe, vector<NoeudData *>*listNoeudAdemanteler, bool demiGrapheParcouru) = 0;

	// ======================================= Methods
	// T11
	list<NoeudData*> detectionBrCorrelated(list< list<NoeudData*> >* tabListBr);

    //T22
    LinearFunction* calculLinearFoncLTI();
	LinearFunction* calculLinearFoncNonLTI();

	// ======================================= Display
    void printInfo(FILE *fic); /// Methode d ecriture des infos du noeud dans un ficher(fic), ou sur ecran(si fic == stdout))

	void printVCG(FILE *fp) {
		(this)->printOpsVCG(fp);
	} ///< Affiche le NoeudOps sous formatVCG
	void printDOTEdge(FILE *f); ///< Affichage de l'edge vers un NoeudOps au format DOT
	void printDOTNode(FILE *f, bool noiseEval = 0); ///< Affichage du NoeudOps au format DOT

	void printOpsVCG(FILE *fp); ///< Affichage du NoeudOps au format VCG
	void printInfoOpsVCG(FILE *fp); ///< Affichage des info du NoeudOps au format VCG


};

#endif // _NOEUDOPS_HH_
