#ifndef __NOISEPOWEREXPRESSIONGENERATION_HH__
#define __NOISEPOWEREXPRESSIONGENERATION_HH__

#include "graph/dfg/Gfd.hh"

#if DEBUG

static string __NOISEPOWER_WFP__("WFP");
static string __NOISEPOWER_WFPQuantif__("WFPquantMode");
static string __NOISEPOWER_WFPSFG__("WFP_sfg");
static string __NOISEPOWER_WFPSFGQuantif__("WFP_sfg_ModeQuantif");
static string __NOISEPOWER_WFPSFGEff__("WFP_sfg_eff");
static string __NOISEPOWER_InputWD__("tabInputWd");
static string __NOISEPOWER_InputWDQuantif__("tabInputWdquantMode");
static string __NOISEPOWER_XBMean__("Xb_Mean");
static string __NOISEPOWER_XBVar__("Xb_Var");

#else

static string __NOISEPOWER_WFP__("WFP");
static string __NOISEPOWER_WFPQuantif__("WFPquantMode");
static string __NOISEPOWER_WFPSFG__("WFP_sfg");
static string __NOISEPOWER_WFPSFGQuantif__("WFP_sfg_ModeQuantif");
static string __NOISEPOWER_WFPSFGEff__("WFP_sfg_eff");
static string __NOISEPOWER_InputWD__("tabInputWd");
static string __NOISEPOWER_InputWDQuantif__("tabInputWdquantMode");
static string __NOISEPOWER_XBMean__("Xb_Mean");
static string __NOISEPOWER_XBVar__("Xb_Var");

//static string __NOISEPOWER_WFP__("A");
//static string __NOISEPOWER_WFPQuantif__("C");
//static string __NOISEPOWER_WFPSFG__("B");
//static string __NOISEPOWER_WFPSFGQuantif__("D");
//static string __NOISEPOWER_WFPSFGEff__("E");
//static string __NOISEPOWER_InputWD__("H");
//static string __NOISEPOWER_InputWDQuantif__("I");
//static string __NOISEPOWER_XBMean__("F");
//static string __NOISEPOWER_XBVar__("G");

#endif

#define TYPE_MODE 1
#if TYPE_MODE == 0
#define MEAN_VAR_TYPE "float"
#define GAIN_TYPE "float"
#define RESULT_TYPE "float"
#define RESULT_MIN "-FLT_MAX"
#elif TYPE_MODE == 1
#define MEAN_VAR_TYPE "double"
#define GAIN_TYPE "double"
#define RESULT_TYPE "double"
#define RESULT_MIN "-DBL_MAX"
#else
#define MEAN_VAR_TYPE "double"
#define GAIN_TYPE "double"
#define RESULT_TYPE "double"
#define RESULT_MIN "-DBL_MAX"
#endif

void WFPCDFGToWFPSFG(Gfd* gfd);
void WFPSFGToWFPSFGEff(Gfd* gfd);

void NoisePowerExpressionLibraryCompilation();

#endif
