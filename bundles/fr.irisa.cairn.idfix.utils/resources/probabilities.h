// === probabilites.h ===
//
// Ce code permet de construire puis d'extraire les probabilités de traces relatives.
// Ce code utilise les informations sur les probabilité des trace global précédement obtenue
// lors de la simulation du code instrumenté
// Les probabilités de traces globales est donnée dans la fonction init_automated_generate_sequence
// qui est généré par gecos.
// Les autres fonctions sont simplement copiées-colées
// 
//	Utilisation:
// Ce code compilé par gecos comme une librairie dynamique qui est appelé par le ComputePb
// 
// Exemple de code d'appelle depuis le computePb:
// --- Initialisation ---
//	int id =0;
//	int nb_total_times;
//	block * block_parcours_1;    
//	block * block_parcours_2;  
//
//	// Pour les test
//	int p;
//	int tabSeq[2] = {4,5};
//	int tabSeq2[4] = {1,2,4,6};
//
//	// --- initialisation ---
//	init_sequence(&block_parcours_1, &block_parcours_2, &id);
//	nb_total_times = init_automated_generate_sequence(block_parcours_1, block_parcours_2, &id);
//
//
//	// --- Affichage ---
//	displayDot(block_parcours_1, block_parcours_2,"toto");
//
//
//	// --- Extracteur de probabilité de la séquence ---
//	p = probabilite_sequence(block_parcours_2, 2,tabSeq ); // 4(NbElement) , 0(num block 1), 2(num block 2), ....)
//
//	printf("La probabilité de la séquence est de %d sur %d\n", p, nb_total_times);
//
//	p = probabilite_croise_sequence(block_parcours_2, 2,tabSeq, 4, tabSeq2); // 4(NbElement) , 0(num block 1), 2(num block 2), ....)
//
//	printf("La probabilité croisé des deux sequences est de %d sur %d\n", p, nb_total_times);
//
//	// --- Destructeur ---
//	free_all_sequence(&block_parcours_1, &block_parcours_2);
//

#ifndef __PROBABILITIES_H__
#define __PROBABILITIES_H__



// === Bibliothèques standards ===
#include <stdarg.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>


// === Bibliothèques non standards ===

typedef struct st_block block;

struct st_block {
	int id; 		// Valeur unique pour l'affichage Dot
	int valeur;		// Valeur du block
	int nbTimes;	// Nombre d'éxécution
	block * suiv;		// suivant sur le même étage
	block * downLevel;	// étage en dessous sur le même étage
};

// === Initialisation ===
void init_sequence(block ** pt_block_parcours_1, block ** pt_block_parcours_2, int *id); ///< Constrution des entrées du graphe

int init_automated_generate_sequence(block * block_parcours_1, block * block_parcours_2, int * id); ///< code généré par gécos représentant les séquences

// === Construction du graphe de repésentation ===
void add_new_sequence(block * block_parcours_1, block * block_parcours_2, int *id, int nbTimes, int nbElement, ...); ///< Permet d'ajouter une sequence (trace global) dans le graphe
block * add_element (block * block_parcours_1, block * block_parcours_2, int *id, int valeur, int nbTimes, bool flag_end_sequence); ///< utilisé par add_new_sequence pour ajouter ou metre a jours un élément
void add_element_block_parcours_2 (block * block_parcours_1, block * block_parcours_2, int *id, int valeur); ///< utilisé par add_element pour créé la seconde liste de parcours permettant d'extraire les informations plus rapidement

// === Extraction de la probabilité d'une séquence ===
int probabilite_sequence(block * block_parcours_2, int nbElement, int  * tabElement);
int probabilite_sequence_routine(block * s, int nbElement, int  * tabElement); ///< Appelé par probabilite_sequence: (routine récursive)
int probabilite_croise_sequence(block * block_parcours_2, int nbElement, int  * tabElement, int nbElement2, int  * tabElement2);

// === Destructions ===
void free_all_sequence(block ** pt_block_parcours_1, block ** pt_block_parcours_2); ///< Destruction du graphe proprement


// === Affichage pour débeugage
void displayDot( block * block_parcours_1, block * block_parcours_2, const char * nom);	///< Génération du dot file (affichage graphique: utiliser xdot.py pour lire les *.dot)
void displayDotRoutineParcours1(FILE * file, block *  block_parcours_1);	///< Appellé par displayDot
void displayDotRoutineParcours2(FILE * file, block *  block_parcours_2);	///< Appellé par displayDot


#endif // #__PROBABILITIES_H__
