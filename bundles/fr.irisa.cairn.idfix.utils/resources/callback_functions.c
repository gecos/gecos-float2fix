

void initiate_execution(){
	jclass cls = (*globalEnv)->GetObjectClass(globalEnv, globalJobj);
	jmethodID mid = (*globalEnv)->GetMethodID(globalEnv, cls, "initiateExecution", "()V");
	(*globalEnv)->CallVoidMethod(globalEnv, globalJobj, mid);
}




void terminate_execution(){
	jclass cls = (*globalEnv)->GetObjectClass(globalEnv, globalJobj);
	jmethodID mid = (*globalEnv)->GetMethodID(globalEnv, cls, "terminateExecution", "()V");
	(*globalEnv)->CallVoidMethod(globalEnv, globalJobj, mid);
}




void passInBlock(int a){
	jclass cls = (*globalEnv)->GetObjectClass(globalEnv, globalJobj);
	jmethodID mid = (*globalEnv)->GetMethodID(globalEnv, cls, "traceBlock", "(I)V");
	(*globalEnv)->CallVoidMethod(globalEnv, globalJobj, mid, a);
}


void traceAffectationValue(int affect, double value){
	jclass cls = (*globalEnv)->GetObjectClass(globalEnv, globalJobj);
	jmethodID mid = (*globalEnv)->GetMethodID(globalEnv, cls, "traceAffectationValue", "(ID)V");
	(*globalEnv)->CallVoidMethod(globalEnv, globalJobj, mid, affect, value);
}
