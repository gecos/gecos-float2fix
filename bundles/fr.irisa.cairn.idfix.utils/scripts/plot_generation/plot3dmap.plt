# number_value = parameter
# matrix = parameter
# filepath = parameter

set dgrid3d number_value,number_value
set view map

set xrange [] reverse

set xlabel "User constraint (dB)"
set ylabel "Signal range"
set zlabel "analytic noise power/simulation noise power (dB)"

# Legend
set key off

set terminal postscript eps color "Times-Roman" 16
set output filepath  

splot matrix u 1:2:3 w pm3d

set output
