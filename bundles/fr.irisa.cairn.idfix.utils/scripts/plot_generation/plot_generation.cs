debug(1);

source_file = $1;
user_constraint = "-"+$2;
output_folder = $3;
architecture_model = $4;

#  - 0 => Branch and Bound algorithm
#  - 1 => Branch and Bound algorithm with fixed size operators
#  - 2 => Min+1
#  - 3 => Min+1 adaptative
#  - 4 => tabu search
#  - 5 => GRASP
optimization_used = $5;

simulation_sample = $6;
non_lti_simulation_sample = $7;
thread_number = $8;
folder_number = $9;
	
AdditionalOptions(thread_number);
simulator = CreateSimulatorConfiguration(simulation_sample);
idfix_project = CreateIDFixProject(source_file, output_folder, folder_number);
Float2FixConversion(idfix_project,user_constraint, architecture_model, optimization_used, non_lti_simulation_sample);
