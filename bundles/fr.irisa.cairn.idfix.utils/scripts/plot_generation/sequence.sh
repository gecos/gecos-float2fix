#!/bin/bash

FOLDER1=/home/nicolas/Tools/gecosluna/workspace_trunk/idfix_plot_generation/src_simu_loop/fft64_0_N
FOLDER2=/home/nicolas/Tools/gecosluna/workspace_trunk/idfix_plot_generation/src_simu_loop/fft64_N_N

FOLDER3=/home/nicolas/Tools/gecosluna/workspace_trunk/idfix_plot_generation/src_simu_loop/fir64_0_N
FOLDER4=/home/nicolas/Tools/gecosluna/workspace_trunk/idfix_plot_generation/src_simu_loop/fir64_N_N

FOLDER5=/home/nicolas/Tools/gecosluna/workspace_trunk/idfix_plot_generation/src_simu_loop/fir512_0_N
FOLDER6=/home/nicolas/Tools/gecosluna/workspace_trunk/idfix_plot_generation/src_simu_loop/fir512_N_N

FOLDER7=/home/nicolas/Tools/gecosluna/workspace_trunk/idfix_plot_generation/src_simu_loop/fft128_0_N
FOLDER8=/home/nicolas/Tools/gecosluna/workspace_trunk/idfix_plot_generation/src_simu_loop/fft128_N_N

FOLDER9=/home/nicolas/Tools/gecosluna/workspace_trunk/idfix_plot_generation/src_simu_loop/fft256_0_N
FOLDER10=/home/nicolas/Tools/gecosluna/workspace_trunk/idfix_plot_generation/src_simu_loop/fft256_N_N

./plotgeneration.sh ${FOLDER1} /home/nicolas/plotgeneration/ fft64_0_N
./plotgeneration.sh ${FOLDER2} /home/nicolas/plotgeneration/ fft64_N_N
./plotgeneration.sh ${FOLDER3} /home/nicolas/plotgeneration/ fir64_0_N
./plotgeneration.sh ${FOLDER4} /home/nicolas/plotgeneration/ fir64_N_N
./plotgeneration.sh ${FOLDER5} /home/nicolas/plotgeneration/ fir512_0_N
./plotgeneration.sh ${FOLDER6} /home/nicolas/plotgeneration/ fir512_N_N
./plotgeneration.sh ${FOLDER7} /home/nicolas/plotgeneration/ fft128_0_N
./plotgeneration.sh ${FOLDER8} /home/nicolas/plotgeneration/ fft128_N_N
./plotgeneration.sh ${FOLDER9} /home/nicolas/plotgeneration/ fft256_0_N
./plotgeneration.sh ${FOLDER10} /home/nicolas/plotgeneration/ fft256_N_N
