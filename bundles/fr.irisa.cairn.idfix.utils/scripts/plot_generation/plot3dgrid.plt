# number_value = parameter
# matrix = parameter
# filepath = parameter

set dgrid3d number_value,number_value
#set hidden3d
#set view map
set pm3d at b

set xrange [] reverse

set xlabel "User constraint (dB)"
set ylabel "Signal range"
set zlabel "Analytic noise power - simulation noise power (dB)" rotate by 90

# Legend
set key off

set terminal postscript eps color "Times-Roman" 16
set output filepath 

splot matrix u 1:2:3 w lines

set output
