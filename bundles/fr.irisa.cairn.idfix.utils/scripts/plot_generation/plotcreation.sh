#!/bin/bash
#
# $1 = IDFix result folder
# $2 = Generated output folder
# $3 = Informative name
#
# Generate many plot with the IDFix process result information

# Regroup all system result to create a data matrix needed to generate 3d plot
function mergeplotvalue {
if [ "$1" == "" ];
then
    echo "System folder path not specified"
    exit 1
else
    local systempath=$1
fi

if [ -d "${systempath}" ];
then
    if [ ! -f "${systempath}/matrix.value" ];
    then 
        echo "Missing simulation result value file"
	echo "${systempath}/matrix.value"
        exit -1
    else
        cat ${systempath}/matrix.value >> ${idfixoutputfolder}/merge_matrix.data
    fi
else
    echo "System folder folder not exist or is not a directory"
    exit -1
fi
}

# use the merge matrix to create 3d plot
function plotcreationwithmergematrix {
if [ -f "${idfixoutputfolder}/merge_matrix.data" ];then
	gnuplot -e "filepath='${generatedgraphfolder}/_3dmap.eps'" -e "number_value=100" -e "matrix='${idfixoutputfolder}/merge_matrix.data'" plot3dmap.plt
	gnuplot -e "filepath='${generatedgraphfolder}/_3dgrid.eps'" -e "number_value=100" -e "matrix='${idfixoutputfolder}/merge_matrix.data'" plot3dgrid.plt
else
	echo "Merge matrix value not found"
	echo "${idfixoutputfolder}/merge_matrix.data"
	exit 1
fi
}

# Generate simple 2d plot
function plotcreation2d {
if [ "$1" == "" ];
then
    echo "System folder path not specified"
    exit 1
else
    local systempath=$1
fi

if [ -d "${systempath}" ];then
	if [ -f "${systempath}/matrix.value" ];
	then
		local fullfilename=$(basename $1)
		local filename=${fullfilename%.*}
		gnuplot -e "plottitle='${filename}'" -e "filepath='${generatedgraphfolder}/${filename}_2d.eps'" -e "matrix='${systempath}/matrix.value'" plot2d.plt
		gnuplot -e "plottitle='${filename}'" -e "filepath='${generatedgraphfolder}/${filename}_2d_analytic_simulation.eps'" -e "matrix='${systempath}/matrix.value'" plot2d_analytic_simulation.plt
	else
		echo "Matrix value not found"
		echo "${systempath}/matrix.value"
		exit 1
	fi
else
    echo "System folder folder not exist or is not a directory"
    exit -1
fi
}


#### MAIN ####
if [ "$1" == "" ];then
	echo "IDFix result output folder not specified"
	exit 1
else
	idfixoutputfolder=${1}
	if [ ! -d ${idfixoutputfolder} ];then
		echo "${idfixoutputfolder} is not a directory or not exist"
		exit 1
	fi
fi

if [ "$2" == "" ];then
	echo "Generated graph output folder not specified "
	exit 1
else
	generatedgraphfolder=${2}
	if [ ! -d ${generatedgraphfolder} ];then
		echo "${idfixoutputfolder} is not a directory or not exist"
		exit 1
	fi
fi

if [ "$3" == "" ];then
	echo "Missing informative name"
	exit 1
else
	informativename=${3}
fi



# Generate and merge matrix result for each system
(( subsystem = 0 ))
for entry in "${idfixoutputfolder}"/*;do
    if [ -d ${entry} ];
    then
		(( subsystem++ ))
		plotcreation2d ${entry}
        mergeplotvalue ${entry}
	echo "System ${subsystem} processed"
    fi
done

#plot 3d graph
plotcreationwithmergematrix

gs -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER -dEPSCrop -sOutputFile=${generatedgraphfolder}/${informativename}_plots.pdf -f ${generatedgraphfolder}/*.eps


