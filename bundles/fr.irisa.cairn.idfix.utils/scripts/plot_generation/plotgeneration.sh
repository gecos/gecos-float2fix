#!/bin/bash

# $1: Path to a source folder
# $2: Path to an output folder
# $3: Information name
# 
# Iterate for each C source file contained in the $1 folder and subfolder and
# run IDFix process with backend simulation. Get back the information of the
# analytical and simulation result and call the plot generator

# //TODO runidfixon function

##############################################################################
##			     USER VARIABLES 	                            ##
##                                                                          ##
##############################################################################
JAVA_BINARY=/usr/lib/jvm/java-7-openjdk-amd64/bin/java
ECLIPSE=/home/nicolas/Tools/gecosluna/eclipse
WORKSPACE=/home/nicolas/Tools/gecosluna/workspace_trunk
ARCHITECTURE_MODEL=/home/nicolas/Tools/gecosluna/workspace_trunk/idfix_demo/utils/architecture_model.xml
OPTIMIZATION_ALGO=4
SIMULATION_SAMPLE=5000
NON_LTI_SIMULATION_SAMPLE=0
THREAD_NUMBER=16
MAX_DATED_FOLDER=2

declare -a USER_CONSTRAINTS=("20" "21" "22" "23" "24" "25" "26" "27" "28" "29" "30" "31" "32" "33" "34" "35" "36" "37" "38" "39" "40" "41" "42" "43" "44" "45" "46" "47" "48" "49" "50" "51" "52" "53" "54" "55" "56" "57" "58" "59" "60" "61" "62" "63" "64" "65" "66" "67" "68" "69" "70" "71" "72" "73" "74" "75" "76" "77" "78" "79" "80" "81" "82" "83" "84" "85" "86" "87" "88" "89" "90" "91" "92" "93" "94" "95" "96" "97" "98" "99" "100")

#declare -a USER_CONSTRAINTS=("20" "25" "30" "35" "40" "45" "50" "55" "60" "65" "70" "75" "80" "85" "90" "95" "100")

#declare -a USER_CONSTRAINTS=("20" "25" "30" "40") 
##############################################################################

# Call IDFix on each file in the directory and sub directory given as parameters
# //TODO Add detection of the extension file and run only if it is a C source file file 
# Parameters : folder path
function runidfixon {
(( callnumber = 0 ))
for i in "$1"/*;do
    if [ -d "$i" ];then
        runidfixon "$i"
    elif [ -f "$i" ]; then
        (( callnumber++ ))
        callidfix ${i} ${callnumber}
    fi
done

}

function sourcefilecounting {
(( sourcefilenumber = 0 ))
for i in "$1"/*;do
    if [ -d "$i" ];then
        ret=$(sourcefilecounting "$i")
        (( sourcefilenumber += ret))
    elif [ -f "$i" ]; then
        (( sourcefilenumber++ ))
    fi
done

echo "${sourcefilenumber}"
}

# Run IDFix process on the file given as parameters. Take other parameters from the user variables.
# Test each constraint specified in the user constraint.
function callidfix {

(( constraintiteration = 0 ))
for constraint in "${USER_CONSTRAINTS[@]}";do
    (( constraintiteration++ ))
    echo "file ${2}/${filenumber} constraint ${constraintiteration}/${constraintnumber} - Run IDFix on $1"

	rm -rf ${WORKSPACE}/../runtime-GecosScriptplot_generation.cs	

    ${JAVA_BINARY} -Declipse.pde.launch=true -Dfile.encoding=UTF-8\
        -classpath ${ECLIPSE}/plugins/org.eclipse.equinox.launcher_1.3.0.v20140415-2008.jar\
        org.eclipse.equinox.launcher.Main -launcher ${ECLIPSE}/eclipse -name Eclipse -nosplash -clean\
        -application fr.irisa.r2d2.gecos.framework.compiler\
        -data ${WORKSPACE}/../runtime-GecosScriptplot_generation.cs\
        -configuration "file:${WORKSPACE}/.metadata/.plugins/org.eclipse.pde.core/Gecos Script plot_generation.cs/"\
        -dev "file:${WORKSPACE}/.metadata/.plugins/org.eclipse.pde.core/Gecos Script plot_generation.cs/dev.properties"\
        -os linux -ws gtk -arch x86_64 -c plot_generation.cs $1 ${constraint} ${idfixoutputfolder} ${ARCHITECTURE_MODEL} ${OPTIMIZATION_ALGO} ${SIMULATION_SAMPLE} ${NON_LTI_SIMULATION_SAMPLE} ${THREAD_NUMBER} ${MAX_DATED_FOLDER} >> ${idfixoutputfolder}/console.log

	
done
}

echo "#############################"
echo "## PLOT GENERATION SCRIPT  ##"
echo "##      FOR IDFIX          ##"
echo "#############################"

if [ "$1" == "" ];then 
    echo "Source folder not specified"
    exit 1;
else
    sourcefolder=$1
fi

if [ "$2" == "" ];then
    echo "IDFix output folder not specified"
    exit 1
else

    if [ ! -d ${2} ];then
        mkdir -p ${2}
    fi

    if [ "$3" != "" ];then 
	informativename=${3}
        outputfolder=${2}/${3}_$(date +%H%M%S)
    else
	echo "Missing informative name"
	exit 1
    fi
    mkdir ${outputfolder}

    idfixoutputfolder=${outputfolder}/idfix_result/
    mkdir ${idfixoutputfolder}

    generatedgraphfolder=${outputfolder}/generated_graph/
    mkdir ${generatedgraphfolder}
fi

filenumber=$(sourcefilecounting ${sourcefolder})
constraintnumber=${#USER_CONSTRAINTS[@]}
iterationnumber=$((filenumber * constraintnumber))
echo "Source folder: ${sourcefolder}"
echo "IDFix result folder: ${idfixoutputfolder}"
echo "Generated graph folder: ${generatedgraphfolder}"
echo "Number of file detected: ${filenumber}"
echo "Number of constraint detected: ${constraintnumber}"
echo "Number of iteration will process: ${iterationnumber}"

# IDFix process call
runidfixon ${sourcefolder}

./plotcreation.sh ${idfixoutputfolder} ${generatedgraphfolder} ${informativename}

