# matrix = parameter
# filepath = parameter
# plottitle

set xrange [] reverse
set yrange [] reverse

set xlabel "User constraint (dB)"
set ylabel "Noise power (dB)"

# Legend
set key on
set title plottitle

set terminal postscript eps color "Times-Roman" 16
set output filepath  

plot matrix u 1:4 t 'analytic' w lp, matrix u 1:5 t 'simulation' w lp


set output
