# matrix = parameter
# filepath = parameter
# plottitle

set xrange [] reverse

set xlabel "User constraint (dB)"
set ylabel "Analytic noise power/simulation noise power (dB)"

# Legend
set key off
set title plottitle

set terminal postscript eps color "Times-Roman" 16
set output filepath  

plot matrix u 1:3 w lp

set output
