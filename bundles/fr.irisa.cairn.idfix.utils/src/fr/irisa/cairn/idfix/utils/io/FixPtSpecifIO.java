package fr.irisa.cairn.idfix.utils.io;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import FixPtSpecification.FixPtSpecif;
import FixPtSpecification.FixPtSpecificationPackage;

/**
 * @author chourou
 * Provides the implementation for the save and load operations of the FixPtSpecif
 */
public class FixPtSpecifIO {

	public static FixPtSpecif loadFixPtSpecif(String fileName) {
		ResourceSet rs = new ResourceSetImpl();
		Resource.Factory.Registry f = rs.getResourceFactoryRegistry();
		Map<String, Object> m = f.getExtensionToFactoryMap();
		m.put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		rs.getPackageRegistry().put(FixPtSpecificationPackage.eNS_URI, FixPtSpecificationPackage.eINSTANCE);
		
		URI uri = URI.createFileURI(fileName);
		//System.out.println(uri);
		Resource resource = rs.getResource(uri, true);
		FixPtSpecif fixSpecif = null;
		if (resource.isLoaded() && resource.getContents().size() > 0) {
			fixSpecif = (FixPtSpecif) resource.getContents().get(0);
		}
		return fixSpecif;
	}

	public static void saveFixPtSpecif(String fileName, FixPtSpecif fixSpecif) {
		ResourceSet rs = new ResourceSetImpl();
		Resource.Factory.Registry f = rs.getResourceFactoryRegistry();
		Map<String, Object> m = f.getExtensionToFactoryMap();
		m.put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		rs.getPackageRegistry().put(FixPtSpecificationPackage.eNS_URI, FixPtSpecificationPackage.eINSTANCE);

		Resource packageResource = rs.createResource(URI.createFileURI(fileName));
		packageResource.getContents().add(FixPtSpecificationPackage.eINSTANCE);
		try {
			packageResource.load(null);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		URI uri = URI.createFileURI(fileName);
		Resource resource = rs.createResource(uri);
		resource.getContents().add(fixSpecif);
		try {
			HashMap<String, Boolean> options = new HashMap<String, Boolean>();
			options.put(XMIResource.OPTION_SCHEMA_LOCATION, Boolean.TRUE);
			resource.save(options);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
