package fr.irisa.cairn.idfix.utils.exceptions;

@SuppressWarnings("serial")
public class IDFixEvalException extends Exception {
	public IDFixEvalException(String msg){
		super(msg);
	}
	
	public IDFixEvalException(Throwable cause){
		super(cause);
	}
	
	public IDFixEvalException(String msg, Throwable cause){
		super(msg, cause);
	}

}
