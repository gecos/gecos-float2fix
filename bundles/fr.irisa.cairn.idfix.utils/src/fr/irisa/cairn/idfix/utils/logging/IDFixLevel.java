package fr.irisa.cairn.idfix.utils.logging;

import java.util.logging.Level;

/**
 * Custom level for the IDFix logging
 * @author Nicolas Simon
 * @date Oct 11, 2013
 */
public class IDFixLevel extends Level {
	private static final long serialVersionUID = -6792156417313543031L;
	public static final Level NOTICE = new IDFixLevel("NOTICE", Level.CONFIG.intValue() + 1);
	
	protected IDFixLevel(String name, int value) {
		super(name, value);
	}
}
