package fr.irisa.cairn.idfix.utils.exceptions;

@SuppressWarnings("serial")
public class BackEndException extends Exception {
	public BackEndException(String msg){
		super(msg);
	}
	
	public BackEndException(Throwable cause){
		super(cause);
	}
}
