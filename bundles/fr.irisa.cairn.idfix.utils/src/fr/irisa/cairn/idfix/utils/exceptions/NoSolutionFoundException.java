package fr.irisa.cairn.idfix.utils.exceptions;

@SuppressWarnings("serial")
public class NoSolutionFoundException extends OptimException {

	public NoSolutionFoundException(String msg) {
		super(msg);
	}

}
