package fr.irisa.cairn.idfix.utils;

/**
 * Regroup all file name and folder hierarchy needed
 * Regroup static method for generate folder hierarchy
 *  
 * @author Nicolas Simon
 * @date Apr 12, 2013
 */
public class ConstantPathAndName {
	// General
	public static final String GEN_RESOURCES_FOLDER = "resources/";
	public static final String GEN_RESOURCES_FOLDER_PATH = ConstantPathAndName.class.getProtectionDomain().getCodeSource().getLocation().getPath() + GEN_RESOURCES_FOLDER;
	public static final String GEN_RESOURCES_SYSTEMC_FOLDER = "systemC/include/";
	public static final String GEN_LAST_RUN_FOLDER_NAME = ".lastrun/";
	public static final String GEN_LAST_RUN_C_SOURCE_FILE_NAME = "last_c_source_file.c";
	public static final String GEN_SFG_FILE_NAME = "sfg.xml";
	public static final String GEN_NOISE_LIBRARY_FILE_NAME = "libcomputepb.so";
	public static final String GEN_DYNAMIC_SPECIFICATION_FILE_NAME = "fp_specification.xml";
	public static final String GEN_KLM_MATRIX_FILE_NAME = "ValeurHgDb.bin";
	
	// IDFix Conv
	public static final String IDFIX_CONV_OUTPUT_FOLDER_NAME = "generated_IDFixConv/";
	
	// Annotations 
	public static final String ANNOTATION_CDFG_DATA = "NUM_DATA_CDFG";
	public static final String ANNOTATION_CDFG_OPERATOR = "OPERATOR_CDFG_NUMBER";
	public static final String ANNOTATION_VALUE_AFFECTATION = "NUM_AFFECTATION";
	public static final String ANNOTATION_MAIN_FUNC = "MAIN_FUNC";
	public static final String ANNOTATION_WIDTH_VARIABLE = "WIDTH";
	public static final String ANNOTATION_OUTPUT_VARIABLE = "OUTPUT";
	public static final String ANNOTATION_DELAY_VARIABLE = "DELAY";
	public static final String ANNOTATION_CONST_VARIABLE = "CONST";
	public static final String ANNOTATION_MAX_RANGE_VARIABLE = "VAL_MAX";
	public static final String ANNOTATION_MIN_RANGE_VARIABLE = "VAL_MIN";
	public static final String ANNOTATION_DYNAMIC_PROCESSING = "DYNAMIC_PROCESSING";
	public static final String ANNOTATION_OVERFLOW_PROBABILITY = "OVERFLOW_PROBABILITY";
	
	// IDFixEval
	public static final String IDFIX_EVAL_OUTPUT_FOLDER_NAME = "generated_IDFixEval/";
	public static final String IDFIX_EVAL_FOLDER_PATH = GEN_RESOURCES_FOLDER_PATH + "IDFixEval/";
	public static final String IDFIX_EVAL_RESOURCES_FOLDER_NAME = "Resources/";
	public static final String IDFIX_EVAL_BUILD_FOLDER_NAME = "build/";
	public static final String IDFIX_EVAL_GENERATED_FODLER_NAME = "Generated/";
	public static final String IDFIX_EVAL_KLM_VALUE_FILE_NAME = "ValeurHgDb.bin";
	public static final String IDFIX_BIN_FOLDER_NAME = "bin/";
	public static final String IDFIX_RELEASE_BIN_NAME = "IDFix_release";
	public static final String IDFIX_TRACE_BIN_NAME = "IDFix_trace";
	public static final String IDFIX_DEBUG_BIN_NAME = "IDFix_debug";

	// Simulation for non-lti system
	public static final String SIMU_FOLDER = "SimulationForNonLTI/";
	public static final String SIMU_INSTRUMENTED_NAME_FILE = "instrumentedCode.c";
	public static final String SIMU_VALUES_FILE_NAME = "simulatedValues.xml";
	public static final String SIMU_PROBABILITIES_FILE_NAME = "probabilities_to_compile.c";
	public static final String SIMU_FILE_TO_COMPILE_FILE_NAME = "simulatedvalues_to_compile.c";
	public static final String SIMU_CALL_BACK_FUNCTIONS_FILE_NAME = "callback_functions.c";
	public static final String SIMU_PROBA_FUNCTIONS_FILE_NAME = "proba_functions.c";
	public static final String SIMU_RUN_LIBRARY_FILE_NAME = "libsimulatedvalues.so";
	public static final String SIMU_PROBA_LIBRARY_FILE_NAME = "libprobabilities.so";
	public static final String SIMU_PROBA_LIBRARY_HEADER = "probabilities.h";
	public static final String SIMU_JNI_HEADER = "jni.h";
	public static final String SIMU_JNI_MD_HEADER = "jni_md.h";
	public static final String SIMU_SIMULATION_LIBRARY_HEADER = "runsimulation.h";
	
	// Simulation for noise function verification
	public static final String OPTIM_SIMU_FIXED_FOLDER_NAME = "fixed/";
	public static final String OPTIM_SIMU_FLOAT_FOLDER_NAME = "float/";
	public static final String OPTIM_SIMU_RESULT_FODLER_NAME = "result/";
	
	public static final String VERIF_FOLDER = "noiseevaluation_simulation/";
	public static final String VERIF_HH_FIXED_FILE_NAME = "simulation_computePb_cFixedFile.hh";
	public static final String VERIF_CC_FIXED_FILE_NAME = "simulation_computePb_cFixedFile.cc";
	public static final String VERIF_CC_FIXED_FILE_TO_COMPILE_NAME = "simulation_computePb_cFixedFile_to_compile.cc";
	public static final String VERIF_FIXED_LIBRARY_NAME = "libcomputepbfixedsimulation.so";
	public static final String VERIF_HH_FLOAT_FILE_NAME = "simulation_computePb_cFloatFile.hh";
	public static final String VERIF_CC_FLOAT_FILE_NAME = "simulation_computePb_cFloatFile.cc";
	public static final String VERIF_CC_FLOAT_FILE_TO_COMPILE_NAME = "simulation_computePb_cFloatFile_to_compile.cc";
	public static final String VERIF_FLOAT_LIBRARY_NAME = "libcomputepbfloatsimulation.so";
	public static final String VERIF_CALL_BACK_FUNCTIONS_FILE_NAME = "callback_functions.cc";
	public static final String VERIF_FLOAT_JNI_HEADER_NAME = "fr_irisa_cairn_idfix_simulation_noiseevaluation_optimization_old_utils_NEvalSimulationForComputePbVerificationFloat.h";
	public static final String VERIF_FIXED_JNI_HEADER_NAME = "fr_irisa_cairn_idfix_simulation_noiseevaluation_optimization_old_utils_NEvalSimulationForComputePbVerificationFixed.h";
	public static final String VERIF_LOG_FILE_NAME = "simulation_informations.log";
	public static final String VERIF_LOG_DEBUG_FILE_NAME = "simulation_debug_informations.log";
	public static final String VERIF_MAIN_FUNC_FILE_NAME = "mainFunc.cc";
	
	// SystemC
	public static final String SYSTEMC_FOLDER_PATH = GEN_RESOURCES_FOLDER_PATH + "systemc-2.3.0/";
	public static final String SYSTEMC_BUILD_FOLDER_NAME = "build/";
	public static final String SYSTEMC_BIN_FODLER_NAME = "bin/";
	public static final String SYSTEMC_BIN_64_FOLDER_NAME = "lib-linux64/";
	public static final String SYSTEMC_BIN_32_FOLDER_NAME = "lib-linux/";
	public static final String SYSTEMC_INCLUDE_FODLER_NAME = "include/";
	public static final String SYSTEMC_BIN_NAME = "libsystemc-2.3.0.so";
	
	// Simulation manager
	public static final String SIMU_MANAGER_FOLDER_PATH = GEN_RESOURCES_FOLDER_PATH + "SimulationManager/";
	public static final String SIMU_MANAGER_BUILD_FOLDER_NAME = "build/";
	public static final String SIMU_MANAGER_BIN_FOLDER_NAME = "bin/";
	public static final String SIMU_MANAGER_INCLUDE_FOLDER_NAME = "include/";
	public static final String SIMU_MANAGER_INCLUDE_CWRAPPER_NAME = "csimulationManager.h";
	public static final String SIMU_MANAGER_BIN_NAME = "libsimulationmanager.so";
	public static final String SIMU_MANAGER_FIXED_BIN_NAME = "fixedsimulation";
	public static final String SIMU_MANAGER_FLOAT_BIN_NAME = "floatsimulation";
	
	// BackEnd simulator
	public static final String BACKEND_SIMU_OUTPUT_FOLDER_NAME = "backend_simulator/";
	public static final String BACKEND_SIMU_FIXED_FODLER_NAME = "fixed/";
	public static final String BACKEND_SIMU_FLOAT_FODLER_NAME = "float/";
	public static final String BACKEND_SIMU_RESULT_FODLER_NAME = "result/";
	
	// Log generation
	public static final String LOG_FODLER_NAME = "logs/";
	public static final String LOG_RELEASE_CONFIG_FILE_NAME = "log4j2-release.xml";
	public static final String LOG_DEBUG_CONFIG_FILE_NAME = "log4j2-debug.xml";
	public static final String LOG_TRACE_CONFIG_FILE_NAME = "log4j2-trace.xml";
	public static final String LOG_FILE_NAME = "information_and_timing.log";
	public static final String LOG_NAME_GLOBAL = "global";
	public static final String LOG_NAME_FRONTEND = "frontend";
	public static final String LOG_NAME_EVALUATION = "evaluation";
	public static final String LOG_NAME_OPTIMIZATION = "optimization";
	public static final String LOG_NAME_BACKEND = "backend";
	public static final String LOG_NAME_SIMULATION_BACKEND = "simulationbackend";
	public static final String LOG_NAME_ENVIRONMENT = "environment";
	
	public static final int MAX_DATA_BITWIDTH = 1000;
}
