package fr.irisa.cairn.idfix.utils.exceptions;

@SuppressWarnings("serial")
public class OptimException extends Exception {
	
	public OptimException(String msg){
		super(msg);
	}
	
	public OptimException(Throwable cause){
		super(cause);
	}
}
