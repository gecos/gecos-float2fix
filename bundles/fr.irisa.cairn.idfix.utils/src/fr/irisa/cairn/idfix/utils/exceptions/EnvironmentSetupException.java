package fr.irisa.cairn.idfix.utils.exceptions;

@SuppressWarnings("serial")
public class EnvironmentSetupException extends Exception {
	public EnvironmentSetupException(Throwable e){
		super(e);
	}
	
	public EnvironmentSetupException(String e){
		super(e);
	}
	
	public EnvironmentSetupException(String e, Throwable a){
		super(e, a);
	}
}
