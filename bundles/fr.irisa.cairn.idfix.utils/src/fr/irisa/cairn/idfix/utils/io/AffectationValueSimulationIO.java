package fr.irisa.cairn.idfix.utils.io;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import affectationValueBySimulation.AffectationValueBySimulationPackage;
import affectationValueBySimulation.ValuesList;


/**
 * @author qmeunier
 * Provides the implementation for the save and load operations of the AffectationList
 */
public class AffectationValueSimulationIO {

	public static ValuesList loadAffectationList(String fileName) {
		ResourceSet rs = new ResourceSetImpl();
		Resource.Factory.Registry f = rs.getResourceFactoryRegistry();
		Map<String, Object> m = f.getExtensionToFactoryMap();
		m.put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		rs.getPackageRegistry().put(AffectationValueBySimulationPackage.eNS_URI, AffectationValueBySimulationPackage.eINSTANCE);
		
		URI uri = URI.createFileURI(fileName);
		Resource resource = rs.getResource(uri, true);
		ValuesList fixSpecif = null;
		if (resource.isLoaded() && resource.getContents().size() > 0) {
			fixSpecif = (ValuesList) resource.getContents().get(0);
		}
		return fixSpecif;
	}

	public static void saveAffectationList(String fileName, ValuesList valuesList) {
		ResourceSet rs = new ResourceSetImpl();
		Resource.Factory.Registry f = rs.getResourceFactoryRegistry();
		Map<String, Object> m = f.getExtensionToFactoryMap();
		m.put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		rs.getPackageRegistry().put(AffectationValueBySimulationPackage.eNS_URI, AffectationValueBySimulationPackage.eINSTANCE);

		Resource packageResource = rs.createResource(URI.createFileURI(fileName));
		packageResource.getContents().add(AffectationValueBySimulationPackage.eINSTANCE);
		try {
			packageResource.load(null);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		URI uri = URI.createFileURI(fileName);
		Resource resource = rs.createResource(uri);
		resource.getContents().add(valuesList);
		try {
			HashMap<String, Boolean> options = new HashMap<String, Boolean>();
			options.put(XMIResource.OPTION_SCHEMA_LOCATION, Boolean.TRUE);
			resource.save(options);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
