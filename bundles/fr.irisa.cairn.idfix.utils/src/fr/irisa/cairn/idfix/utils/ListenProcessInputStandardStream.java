package fr.irisa.cairn.idfix.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;

/**
 * Thread which get back information from a input stream and display it into an custom {@link Logger} specified by the user
 * @author Nicolas Simon
 * @date Jun 6, 2013
 */
public class ListenProcessInputStandardStream extends Thread {
	private InputStream _inputStream;
	private Logger _logger;
	private Level _level;
	
	/**
	 * Create a thread using custom logger to display information
	 * @param inputStream
	 * @param logger
	 * @param level
	 */
	public ListenProcessInputStandardStream(InputStream inputStream, Logger logger, Level level){
		_inputStream = inputStream;
		_logger = logger;
		_level = level;
	}
	
//	/**
//	 * Create a thread using standard output stream to display information
//	 * @param inputStream
//	 */
//	public ListenProcessInputStandardStream(InputStream inputStream){
//		_inputStream = inputStream;
//		_out = System.out;
//		_err = System.err;
//	}
	
//	/**
//	 * Create a thread using custom output stream to display information
//	 * @param inputStream
//	 */
//	public ListenProcessInputStandardStream(InputStream inputStream, PrintStream out, PrintStream err){
//		_inputStream = inputStream;
//		_out = out;
//		_err = err;
//	}
	
	@Override
	public void run() {
		super.run();
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(_inputStream));
		String line;
		try {
			while(!isInterrupted() && (line = reader.readLine()) != null){
				_logger.log(_level, line);
			}
		} catch (IOException e) {
			_logger.log(_level, e.getMessage(), e);
		}
	}
}
