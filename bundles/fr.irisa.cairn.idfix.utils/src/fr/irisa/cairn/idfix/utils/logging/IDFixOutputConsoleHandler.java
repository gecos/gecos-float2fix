package fr.irisa.cairn.idfix.utils.logging;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import java.util.logging.StreamHandler;

/**
 * Handler for logger writing to the standard output
 * @author Nicolas Simon
 * @date Oct 10, 2013
 */
public class IDFixOutputConsoleHandler extends StreamHandler {

	public IDFixOutputConsoleHandler(Formatter formatter) {
		super(System.out, formatter);
	}

	
	@Override
	public synchronized void publish(LogRecord record) {
		super.publish(record);
		super.flush();
	}

	@Override
	public synchronized void close() throws SecurityException {
		super.flush();
	}

}
