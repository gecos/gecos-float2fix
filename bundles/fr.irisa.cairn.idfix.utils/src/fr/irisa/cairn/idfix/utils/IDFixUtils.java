package fr.irisa.cairn.idfix.utils;

import fr.irisa.cairn.idfix.utils.exceptions.SFGModelCreationException;
import gecos.annotations.PragmaAnnotation;
import gecos.annotations.StringAnnotation;
import gecos.blocks.BasicBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.Symbol;
import gecos.dag.DAGNode;
import gecos.instrs.Instruction;
import gecos.types.ArrayType;
import gecos.types.Field;
import gecos.types.Type;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Regroup all utilities function
 * @author Nicolas Simon
 * @date Jun 14, 2013
 */
public class IDFixUtils {
	private final static String OS_NAME = System.getProperty("os.name").toLowerCase();
	private final static String OS_ARCH = System.getProperty("os.arch").toLowerCase();
 
	public final static boolean is_OS_LINUX = "linux".equals(OS_NAME);
	public final static boolean is_OS_MAC = "mac".equals(OS_NAME);
	
	public final static boolean is_64_arch = "amd64".equals(OS_ARCH) || "ppc".equals(OS_ARCH);
	public final static boolean is_32_arch = "i386".equals(OS_ARCH);
	
	// Value used by gecos (TYPE FACTORY) and its regenerator
	public final static int SIZE_OF_CHAR = 8;
	public final static int SIZE_OF_SHORT = 16;
	public final static int SIZE_OF_INT = 32;
	public final static int SIZE_OF_LONG = 32;
	public final static int SIZE_OF_FLOAT = 32;
	public final static int SIZE_OF_DOUBLE = 64;
	public final static int SIZE_OF_LONG_LONG = 64;
	
	
	/*************************************************************************
	 *				            ANNOTATIONS CHECK                            *
	 *                                                                       * 
	 ************************************************************************/
	/**
	 * Extract of a procedure set the main procedure and return it
	 * @param ps The procedure where search the main procedure 
	 * @return The main procedure into a procedure set, null if not found
	 */
	public static Procedure getMainProcedure(ProcedureSet ps){
		Procedure res = null;
		for (Procedure proc : ps.listProcedures()){
			if (isMainFunc(proc.getSymbol())){
				res = proc;
				break;
			}
		}
		return res;
	}
	
	/**
	 * Research into the annotation set of the symbol set if there are the annotation MAIN_FUNC
	 * @param s
	 * @return true if the pragma MAIN_FUNC is found, else otherwise
	 */
	public static boolean isMainFunc(Symbol s){
		PragmaAnnotation pragmaAnnotation = s.getPragma();
		if (pragmaAnnotation != null) {
			StringBuilder sBuilder = new StringBuilder();
			for (String str : ((PragmaAnnotation) pragmaAnnotation)
					.getContent()) {
				sBuilder.append(str);
			}
			Pattern pattern = Pattern.compile("\\.*MAIN_FUNC\\.*");
			Matcher matcher = pattern.matcher(sBuilder);
			if (matcher.find()) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @param s
	 * @return Return true if the symbol s is delayed var (Annotation DELAY), false otherwise
	 */
	public static boolean isDelayedVar(Symbol s){
		StringAnnotation strAnnot = (StringAnnotation) s.getAnnotation(ConstantPathAndName.ANNOTATION_DELAY_VARIABLE);
		if (strAnnot == null)
			return false;
		else
			return true;
	}
	
	/**
	 * @param s
	 * @return Return true if the symbol s is output var (Annotation OUTPUT), false otherwise
	 */
	public static boolean isOutputVar(Symbol s){
		StringAnnotation strAnnot = (StringAnnotation) s.getAnnotation(ConstantPathAndName.ANNOTATION_OUTPUT_VARIABLE);
		if (strAnnot == null)
			return false;
		else
			return true;
	}
	
	public static boolean haveOutputPragma(Symbol s){
		PragmaAnnotation pragma = s.getPragma();
		if(pragma != null){
			for(String content : pragma.getContent()){
				Pattern pattern = Pattern.compile("\\.*OUTPUT\\.*");
				Matcher matcher = pattern.matcher(content);
				if (matcher.find())
					return true;
			}
		}
		return false;
	}
	
	/**
	 * @param s
	 * @return Return true if the symbol s is input var (Annotation DYNAMIC), false otherwise
	 */
	public static boolean isInputVar(Symbol s) throws SFGModelCreationException{
		Double varMin = getVarMin(s);
		Double varMax = getVarMax(s);
		if(varMin != null && varMax != null)
			return true;
		else if(varMin != null || varMax != null)
			throw new SFGModelCreationException("A variable have only a minimum range or a maximum range");
		else
			return false;
			
	}
	
	public static boolean haveDynamicPragma(Symbol s){
		PragmaAnnotation pragma = s.getPragma();
		if(pragma != null){
			for(String content : pragma.getContent()){
				Pattern pattern = Pattern.compile("\\.*DYNAMIC\\s*\\[\\s*(-?\\d+\\.?\\d*)\\s*[,;]\\s*(-?\\d+\\.?\\d*)\\s*\\]\\.*");
				Matcher matcher = pattern.matcher(content);
				if (matcher.find())
					return true;
			}
		}
		return false;
	}
	
	/**
	 * @param s
	 * @return Return the min range value if s is an input, null otherwise
	 */
	public static Double getVarMin(Symbol s){
		StringAnnotation strAnnot = (StringAnnotation) s.getAnnotation(ConstantPathAndName.ANNOTATION_MIN_RANGE_VARIABLE);
		if (strAnnot == null){
			return null;
		}
		else {
			double res = Double.parseDouble(strAnnot.getContent());
			return res;
		}
	}
	
	/**
	 * @param s
	 * @return Return the max range value if s is an input, null otherwise
	 */
	public static Double getVarMax(Symbol s){
		StringAnnotation strAnnot = (StringAnnotation) s.getAnnotation(ConstantPathAndName.ANNOTATION_MAX_RANGE_VARIABLE);
		if (strAnnot == null){
			return null;
		}
		else {
			Double res = Double.parseDouble(strAnnot.getContent());
			return res;
		}
	}
	
	/**
	 * 
	 * @param s
	 * @return Return the dynamic process annotation
	 */
	public static String getDynamicProcessing(Symbol s){
		StringAnnotation strAnnot = (StringAnnotation) s.getAnnotation(ConstantPathAndName.ANNOTATION_DYNAMIC_PROCESSING);
		if (strAnnot == null){
			return "NONE";
		}
		else {
			String res = strAnnot.getContent();
			return res;
		}
	}
	
	/**
	 * @param s
	 * @return Return the overflow probability of value using the overflow dynamic processing option
	 */
	public static Double getOverflowProbability(Symbol s){
		StringAnnotation strAnnot = (StringAnnotation) s.getAnnotation(ConstantPathAndName.ANNOTATION_DYNAMIC_PROCESSING);
		if (strAnnot == null || !strAnnot.getContent().equals("OVERFLOW")){
			// Ou lancer une exception ?
			return -1.0;
		}
		else {
			strAnnot = (StringAnnotation) s.getAnnotation(ConstantPathAndName.ANNOTATION_OVERFLOW_PROBABILITY);
			Double res = Double.parseDouble(strAnnot.getContent());
			return res;
		}
	}
	
	/**
	 * Return the CDFG of the symbol.
	 * NOTE: If you manipulate the fixed point specification, prefer use FixedPointSpecification.findOperatorFromSymbol to get the Data
	 * @param s
	 * @return Return the CDFG data number of the symbol s, null otherwise
	 */
	// TODO Add test to check the type of the symbol
	public static Integer getSymbolAnnotationNumber(Symbol s){
		StringAnnotation strAnnot = (StringAnnotation) s.getAnnotation(ConstantPathAndName.ANNOTATION_CDFG_DATA);
		if (strAnnot == null){
			//myassert(false,"");
			return null;
		}
		else {
			Integer res = Integer.parseInt(strAnnot.getContent());
			return res;
		}
	}
	
	// TODO Add test to check the type of the symbol
	public static Integer getSymbolAnnotationNumber(Symbol s, Field f) {
		try{
			StringAnnotation strAnnot = (StringAnnotation) s.getAnnotation(ConstantPathAndName.ANNOTATION_CDFG_DATA);
			if(strAnnot == null){
				return null;
			}
			else{
				String[] content = strAnnot.getContent().split("/");
				String[] fieldcdfg;
				for(int i = 0 ; i < content.length ; i++){
					 fieldcdfg = content[i].split(":");
					 if(fieldcdfg.length != 2)
						 throw new SFGModelCreationException("Wrong syntax into structure CDFG annotation");
					 
					 if(fieldcdfg[0].equals(f.getName())){
						 int temp = Integer.parseInt(fieldcdfg[1]);
						 return temp;
					 }
				}
				return null;
			}
		} catch (SFGModelCreationException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Return the CDFG of the instruction.
	 * NOTE: If you manipulate the fixed point specification, prefer use FixedPointSpecification.findOperatorFromInstruction to get the operator
	 * @param inst
	 * @return the CDFG number of the instruction, null otherwise
	 */
	public static Integer getCDFGInstructionNumber(Instruction inst){
		Integer res = null;
		StringAnnotation numAnnotation = (StringAnnotation) inst.getAnnotation(ConstantPathAndName.ANNOTATION_CDFG_OPERATOR);
		if (numAnnotation == null){
			return null;
		}
		res = Integer.valueOf(numAnnotation.getContent());
		return res;
	}
	
	public static Integer getCDFGInstructionNumber(DAGNode inst){
		Integer res = null;
		StringAnnotation numAnnotation = (StringAnnotation) inst.getAnnotation(ConstantPathAndName.ANNOTATION_CDFG_OPERATOR);
		if (numAnnotation == null){
			return null;
		}
		res = Integer.valueOf(numAnnotation.getContent());
		return res;
	}
	
	public static Integer getNumAffectation(Instruction i){
		StringAnnotation strAnnot = (StringAnnotation) i.getAnnotation(ConstantPathAndName.ANNOTATION_VALUE_AFFECTATION);
		if (strAnnot == null){
			return null;
		}
		Integer res = Integer.parseInt(strAnnot.getContent());
		return res;
	}
	
	public static Integer getVarWidth(Symbol s){
		StringAnnotation strAnnot = (StringAnnotation) s.getAnnotation(ConstantPathAndName.ANNOTATION_WIDTH_VARIABLE);
		if (strAnnot == null){
			return null;
		}
		else {
			Integer res = Integer.parseInt(strAnnot.getContent());
			return res;
		}
	}
	
	public static String getBasicBlockNumber(BasicBlock b) throws SFGModelCreationException{
		//FIXME why return NONE
		if (b == null){
			return "NONE";
		}
		StringAnnotation strAnnot = (StringAnnotation) b.getAnnotation("NUM_BASIC_BLOCK");
		if (strAnnot == null){
			throw new SFGModelCreationException("The basic block use as parameters have not a NUM_BASIC_BLOCK annotation");
		}
		else {
			String res = strAnnot.getContent();
			return res;
		}
	}
	
	/*************************************************************************
	 *				        RANDOM NUMBER GENERATOR                          *
	 *                                                                       * 
	 ************************************************************************/
	/**
	 * Generate a random float value contains into the range [valmin, valmax[
	 * @param valMin
	 * @param valMax
	 * @return random float value contains into the range [valmin, valmax[
	 */
	public static float generateRandomFloatValue(float valMin, float valMax){
		Random r = new Random();
		return (r.nextFloat() * (valMax - valMin)) + valMin;
	}
	
	/**
	 * Generate a random double value contains into the range [valmin, valmax[
	 * @param valMin
	 * @param valMax
	 * @return random double value contains into the range [valmin, valmax[
	 */
	public static double generateRandomDoubleValue(double valMin, double valMax){
		Random r = new Random();
		return (r.nextDouble() * (valMax - valMin)) + valMin;
	}
	
	/**
	 * Generate a random integer value contains into the range [valmin, valmax[
	 * @param valMin
	 * @param valMax
	 * @return random integer value contains into the range [valmin, valmax[
	 */
	public static int generateRandomIntValue(int valMin, int valMax){
		Random r = new Random();
		return r.nextInt(valMax - valMin) + valMin;
	}
	
	/**
	 * Generate a random long value contains into the range [valmin, valmax[
	 * @param valMin
	 * @param valMax
	 * @return random long value contains into the range [valmin, valmax[
	 */
	public static long generateRandomLongValue(long valMin, long valMax){
		Random r = new Random();
		return (r.nextLong() * (valMax - valMin)) + valMin;
	}	
	
	/*************************************************************************
	 *				          FILE/DIRECTORY STUFF                           *
	 *                                                                       * 
	 ************************************************************************/	
	/**
	 * Copy the content from the src file to the dest file
	 * @param src
	 * @param dest
	 * @throws IOException
	 */
	public static void copyFile(InputStream src, OutputStream dest) throws IOException{
		BufferedInputStream reader = new BufferedInputStream(src);
		BufferedOutputStream writer = new BufferedOutputStream(dest);
		
		int b;
		while((b = reader.read()) != -1)
			writer.write(b);
		
		reader.close();
		writer.flush();
	}
	
	/**
	 * Copy the content from the src file to the dest file
	 * @param src
	 * @param dest
	 * @throws IOException
	 */
	public static void copyFile(String src, String dest) throws IOException{
		copyFile(new File(src), new File(dest));
	}
	
	/**
	 * Copy the content from the src file to the dest file
	 * @param src
	 * @param dest
	 * @throws IOException
	 */
	public static void copyFile(File src, File dest) throws IOException{
		BufferedInputStream reader = new BufferedInputStream(new FileInputStream(src));
		BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream(dest));
		
		int b;
		while((b = reader.read()) != -1)
			writer.write(b);
		
		reader.close();
		writer.close();
	}
	
	/**
	 * Copy file with the relative path from the IDFix resources folder into the default system temporary folder
	 * 
	 * @param relativeFilePath
	 * @return
	 * @throws IOException
	 */
	public static File copyFileFromResourcesFolderToTemporaryFolder(String relativeFilePath) throws IOException {
		InputStream in = ConstantPathAndName.class.getResourceAsStream("/" + ConstantPathAndName.GEN_RESOURCES_FOLDER + relativeFilePath);
		if(in == null){
			throw new RuntimeException("The file " + relativeFilePath + " not exist in the IDFix resource folder");
		}
		
		File temp = File.createTempFile("idfix_" + relativeFilePath, ".tmp");
		temp.deleteOnExit();

		try (FileOutputStream out = new FileOutputStream(temp)) {
			IDFixUtils.copyFile(in, out);
		}
		
		return temp;
	}
	
	/**
	 * Copy file with the relative path from the IDFix resources folder into the folder given.
	 * Create the folder if not exist.
	 * 
	 * @param relativeFilePath
	 * @param folderPath
	 * @return
	 * @throws IOException
	 */
	public static File copyFileFromResourcesFolderToFolder(String relativeFilePath, String folderPath) throws IOException {
		InputStream in = ConstantPathAndName.class.getResourceAsStream("/" + ConstantPathAndName.GEN_RESOURCES_FOLDER + relativeFilePath);
		if(in == null){
			throw new RuntimeException("The file " + relativeFilePath + " not exist in the IDFix resource folder");
		}

		File folder = new File(folderPath);
		folder.mkdir();
		if(!folder.isDirectory())
			throw new IOException("Error during the creation of the folder " + folderPath);
		
		File file = new File(folder, relativeFilePath);
		try (FileOutputStream out = new FileOutputStream(file)) {
			IDFixUtils.copyFile(in, out);
		}
		
		return file;
	}
	
	public static void deleteDirectory(File directory){
		if(!directory.isDirectory())
			throw new RuntimeException("The directory to delete is not a directory");
		
		for(File file : directory.listFiles()){
			if(file.isFile())
				file.delete();
			else
				deleteDirectory(file);
		}
		directory.delete();
	}
	
	/**
	 * Check if two file have the same content
	 * @param file1
	 * @param file2
	 * @return true is the content of the 2 file is equal
	 * @throws IOException If parameters doesn't exist or is not a file
	 */
	public static boolean areFilesEquals(File file1, File file2) throws IOException {
		if(!file1.isFile())
			throw new IOException(file1.getCanonicalPath() + " doesn't exist or is not a file");
		
		if(!file2.isFile())
			throw new IOException(file2.getCanonicalPath() + " doesn't exist or is not a file");
		
		boolean areEquals = false;
		BufferedReader bufferedReader1 = new BufferedReader(new FileReader(file1));
		BufferedReader bufferedReader2 = new BufferedReader(new FileReader(file2));
		int int1;
		int int2;
		do {
			int1 = bufferedReader1.read();
			int2 = bufferedReader2.read();
			areEquals = int1 == int2;
		} while(areEquals && int1 != -1 && int2 != -1);
		bufferedReader1.close();
		bufferedReader2.close();
		return areEquals;
	}

	/*************************************************************************
	 *				                  OTHERS                                 *
	 *                                                                       * 
	 ************************************************************************/
	
	
	public static Vector<Long> getArrayIndexes(Type type, Long indice){
		Type localType = type;
		ArrayType arrayType;
		Vector<Long> indices = new Vector<Long>();
		Long currIndice = indice;
		Long indiceDim;
		Long nbElemDim;
		while (localType instanceof ArrayType){
			arrayType = (ArrayType) localType;
			nbElemDim = arrayType.getUpper() - arrayType.getLower() + 1;
			indiceDim = currIndice % nbElemDim;
			currIndice -= indiceDim;
			currIndice = currIndice / nbElemDim;
			localType = arrayType.getBase();
			indices.add(indiceDim);
		}
		if(((ArrayType) type).getNbDims() != indices.size())
			throw new RuntimeException("The number of dimensions (" + ((ArrayType) type).getNbDims() + ") is not equal to the number of table index generated (" + indices.size()+")");
		
		Vector<Long> res = new Vector<Long>();
		for (int i = indices.size() - 1; i >= 0; i--){
			res.add(indices.get(i));
		}
		//System.out.println("indice : " + indice + " - res : " + res);
		return res;
	}
}
