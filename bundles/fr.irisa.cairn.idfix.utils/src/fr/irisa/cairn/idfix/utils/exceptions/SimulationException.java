package fr.irisa.cairn.idfix.utils.exceptions;

@SuppressWarnings("serial")
public class SimulationException extends Exception {
	public SimulationException(String msg){
		super(msg);
	}
	
	public SimulationException(Throwable cause){
		super(cause);
	}
	
	public SimulationException(String message, Throwable cause){
		super(message, cause);
	}

}
