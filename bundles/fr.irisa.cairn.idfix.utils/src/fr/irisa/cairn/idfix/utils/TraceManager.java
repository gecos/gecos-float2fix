package fr.irisa.cairn.idfix.utils;

import java.io.PrintStream;

import affectationValueBySimulation.ValuesList;

public interface TraceManager {

	public void initiateExecution();
	public void terminateExecution();
	public void traceBlock(int numBlock);
	public void showProbabilities();
	public void printSequences(PrintStream fichierSortie);
	public void traceAffectationValue(int numAffect, double value);
	public ValuesList getValuesList();
	
}
