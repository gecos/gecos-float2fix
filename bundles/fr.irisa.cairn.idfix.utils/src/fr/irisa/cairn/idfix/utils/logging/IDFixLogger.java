package fr.irisa.cairn.idfix.utils.logging;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;

import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.IDFixUtils;


/**
 * Class use to configure logging in IDFix.
 * @author Nicolas Simon
 * @date Oct 11, 2013
 */
public class IDFixLogger {
	private static boolean _notInitialized = true;
	public static void initializeReleaseLogging(String folderPath){
		initializeLogging(folderPath, ConstantPathAndName.LOG_RELEASE_CONFIG_FILE_NAME);
	}
	public static void initializeDebugLogging(String folderPath){
		initializeLogging(folderPath, ConstantPathAndName.LOG_DEBUG_CONFIG_FILE_NAME);
	}
	public static void initializeTraceLogging(String folderPath){
		initializeLogging(folderPath, ConstantPathAndName.LOG_TRACE_CONFIG_FILE_NAME);
	}
	
	private static void initializeLogging(String folderPath, String configFileName){
		try {
			System.setProperty("idfixLogPathFolder", (folderPath.endsWith("/")?folderPath:folderPath.concat("/")));
			File tempFile = IDFixUtils.copyFileFromResourcesFolderToTemporaryFolder(configFileName);
			System.setProperty("log4j.configurationFile", tempFile.getAbsolutePath());
			LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
			ctx.reconfigure();
			_notInitialized = false;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static Logger getLogger(String name){
		if(_notInitialized){
			System.err.println("Logger not initialized. Initializing in debug mode at the root folder");
			initializeLogging(".", ConstantPathAndName.LOG_DEBUG_CONFIG_FILE_NAME);
			_notInitialized = false;
		}
		return LogManager.getLogger(name);
	}	
}
