package fr.irisa.cairn.idfix.utils.exceptions;

@SuppressWarnings("serial")
public class FlattenerException extends Exception {
	public FlattenerException(String msg){
		super(msg);
	}
	
	public FlattenerException(Throwable cause){
		super(cause);
	}
	
	public FlattenerException(String msg, Throwable cause){
		super(msg, cause);
	}
}
