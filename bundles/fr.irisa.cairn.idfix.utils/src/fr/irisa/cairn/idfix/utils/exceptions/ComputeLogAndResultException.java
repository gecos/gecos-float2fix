package fr.irisa.cairn.idfix.utils.exceptions;

@SuppressWarnings("serial")
public class ComputeLogAndResultException extends Exception {
	public ComputeLogAndResultException(String msg){
		super(msg);
	}
	
	public ComputeLogAndResultException(Throwable cause){
		super(cause);
	}
}
