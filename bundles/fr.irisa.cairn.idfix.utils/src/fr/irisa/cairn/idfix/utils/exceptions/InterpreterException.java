package fr.irisa.cairn.idfix.utils.exceptions;

@SuppressWarnings("serial")
public class InterpreterException extends Exception {
	
	public InterpreterException(String msg){
		super(msg);
	}
}
