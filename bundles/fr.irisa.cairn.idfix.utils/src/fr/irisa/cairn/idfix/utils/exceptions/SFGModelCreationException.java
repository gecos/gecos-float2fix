package fr.irisa.cairn.idfix.utils.exceptions;

@SuppressWarnings("serial")
public class SFGModelCreationException extends Exception {
	public SFGModelCreationException(String msg){
		super(msg);
	}
}
