package fr.irisa.cairn.idfix.utils.exceptions;

@SuppressWarnings("serial")
public class SFGGeneratorException extends Exception {
	public SFGGeneratorException(String msg){
		super(msg);
	}
}
