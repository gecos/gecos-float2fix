package fr.irisa.cairn.idfix.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;

import fr.irisa.cairn.idfix.utils.exceptions.EnvironmentSetupException;
import fr.irisa.cairn.idfix.utils.exceptions.IDFixEvalException;
import fr.irisa.cairn.idfix.utils.logging.IDFixLogger;


/**
 * Use to create a configuration file at the first start of the flow
 * This installation can be reset with the command script "Reset_Environment"
 * @author Nicolas Simon
 * @date Jun 5, 2013
 */
public class EnvironmentInformations {	
	public enum ENGINE {MATLAB, OCTAVE};
	private enum IDFIX_COMPILATION_MODE {RELEASE, TRACE, DEBUG};
	
	private static ENGINE _ENGINE_USED = null; 
	
	// MATLAB
	public final static String KEY_MATLAB = "matlab";
	public final static String KEY_MATLAB_INC = "matlabinc";
	public final static String KEY_MATIO = "matio";
	public final static String KEY_MATIO_INC = "matioinc";
	
	// OCTAVE
	public final static String KEY_OCTAVE = "octave";
	
	//SIMULATION
	public final static String KEY_SYSTEMC = "systemc";
	public final static String KEY_SYSTEMC_INC = "systemcinc";
	public final static String KEY_SIMUMANAGER = "simumanager";
	public final static String KEY_SIMUMANAGER_INC = "simumanagerinc";

	// OTHER
	public final static String KEY_ENGINE = "engine";
	public final static String KEY_XML2 = "xml2";
	public final static String KEY_XML2_INC = "xml2inc";
	
	private final static String _SEPARATOR = ":";
	private final static String _CONFIG_FILE_NAME = "env.cfg";
	private final static String _CONFIG_FILE_PATH = ConstantPathAndName.GEN_RESOURCES_FOLDER_PATH +_CONFIG_FILE_NAME;
	private final static String _idfixDebugBinaryPath = ConstantPathAndName.IDFIX_EVAL_FOLDER_PATH + ConstantPathAndName.IDFIX_BIN_FOLDER_NAME + ConstantPathAndName.IDFIX_DEBUG_BIN_NAME;
	private final static String _idfixReleaseBinaryPath = ConstantPathAndName.IDFIX_EVAL_FOLDER_PATH + ConstantPathAndName.IDFIX_BIN_FOLDER_NAME + ConstantPathAndName.IDFIX_RELEASE_BIN_NAME;
	
	
	private static Map<String, String> _configurationBuffer = new HashMap<String, String>();
	
	private static final String _message = "\n#######################    ID.Fix Installation    #######################\n"
									+ "#  Environment informations is need to a proper functionning            #\n"
									+ "#                                                                       #\n"
									+ "#    1) Engine user selection (required)                                #\n"
									+ "#    2) Engine installation path folder (required)                      #\n"
									+ "#    3) Compilation of IDFixEval                                        #\n"
									+ "#    4) Compilation of SystemC                                          #\n"
									+ "#    5) Compilation of the simulation manager                           #\n"
									+ "#                                                                       #\n"
									+ "#  To reset, use the script command   \"Reset_Environment()\"             #\n"
									+ "#########################################################################";
	
	private static File _file = new File(_CONFIG_FILE_PATH);
	@SuppressWarnings("unused")
	private static PrintStream _out = System.out;	
	@SuppressWarnings("unused")
	private static PrintStream _err = System.err;
	
	private final static Logger _logger = IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_ENVIRONMENT);

	/**
	 * Use to provide a Gecos Script command
	 */
	public void compute() {
		try{
			reset();
		} catch(EnvironmentSetupException e){
			_logger.error(e.getMessage());
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * @return Engine used by IDFix-AccEval
	 */
	public static ENGINE getEngineUsed(){
		if(_ENGINE_USED == null)
			throw new RuntimeException("Problem in the IDFix configuration. Need to reset the configuration");
		return _ENGINE_USED;
	}
	
	/**
	 * Ask to the user missing information of the environment (path to Matlab for example) if the configuration file is not present.
	 * Check if all the information needed is already present before launch the setup procedure  
	 */
	public static void setup() {
		try{
			boolean filePresent = !_file.createNewFile();
			if(!filePresent){
				_logger.info(_message);
				_ENGINE_USED = engineUserSelection(); // Need to validate the engine used before run the procedure installation
				setupProcedure();
			}
			else{
				loadConfigFileIntoConfigBuffer(); // Need to read the configuration file to build the configuration buffer			
				if(engineInformationMissing() || informationMissing()){
					_logger.info(_message);
					_ENGINE_USED = engineUserSelection(); // Need to validate the engine used before run the procedure installation
					setupProcedure();
				}				
			}
			flushConfigBufferToConfigFile(); // Flush the configuration buffer into the configuration file
		} catch(Exception e){
			_logger.error("Error during IDFix installation step", e);
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Use to reset the installation
	 * @throws EnvironmentSetupException 
	 * @throws IDFixEvalException 
	 */
	private static void reset() throws EnvironmentSetupException {
		cleanIDFixEval(true);
		cleanSystemC();
		uninstallSystemC();
		_file.delete();		
	}
	
	private static void displayCurrentConfiguration() throws EnvironmentSetupException{
		String value;
		StringBuffer strBuffer = new StringBuffer();
		strBuffer.append("Current configuration used\n");
		if((value = _configurationBuffer.get(KEY_ENGINE)) == null)
			throw new EnvironmentSetupException("Engine information missing. Reset the configuration");
		strBuffer.append(KEY_ENGINE + _SEPARATOR + value + "\n");
		
		if(_ENGINE_USED == ENGINE.MATLAB){	
			if((value = _configurationBuffer.get(KEY_MATLAB)) == null)
				throw new EnvironmentSetupException("Matlab libraries path information missing. Reset the configuration");
			strBuffer.append(KEY_MATLAB + _SEPARATOR + value + "\n");
			
			if((value = _configurationBuffer.get(KEY_MATLAB_INC)) == null)
				throw new EnvironmentSetupException("Matlab headers path information missing. Reset the configuration");
			strBuffer.append(KEY_MATLAB_INC + _SEPARATOR + value + "\n");
			
			if((value = _configurationBuffer.get(KEY_MATIO)) == null)
				throw new EnvironmentSetupException("Matio library path information missing. Reset the configuration");
			strBuffer.append(KEY_MATIO + _SEPARATOR + value + "\n");
			
			if((value = _configurationBuffer.get(KEY_MATIO_INC)) == null)
				throw new EnvironmentSetupException("Matio headers path information missing. Reset the configuration");
			strBuffer.append(KEY_MATIO_INC + _SEPARATOR + value + "\n");
		} else if(_ENGINE_USED == ENGINE.OCTAVE) {
			if((value = _configurationBuffer.get(KEY_OCTAVE)) == null)
				throw new EnvironmentSetupException("Octave binary path information missing. Reset the configuration");
			strBuffer.append(KEY_OCTAVE + _SEPARATOR + value + "\n");
		}
		else{
			throw new EnvironmentSetupException("The engine information contained in the configuration file is not valid");
		}
		
		if((value = _configurationBuffer.get(KEY_XML2)) == null)
			throw new EnvironmentSetupException("libxml2 library path information missing. Reset the configuration");
		strBuffer.append(KEY_XML2 + _SEPARATOR + value + "\n");
		
		if((value = _configurationBuffer.get(KEY_XML2_INC)) == null)
			throw new EnvironmentSetupException("libxml2 header path information missing. Reset the configuration");
		strBuffer.append(KEY_XML2_INC + _SEPARATOR + value + "\n");		
		
		_logger.info(strBuffer.toString());
	}
	
	/*********************         Method which access to the configuration file         *********************/
	/**                                                                                                     **/
	/**                                                                                                     **/
	/**                                                                                                     **/
	/*********************************************************************************************************/
	
	/**
	 * Return the value of the key word contained into the configuration file, null if key is not found
	 * @param key
	 * @return value of key contained into the configuration file
	 * @throws EnvironmentSetupException 
	 */
	public static String getEnv(String key) throws EnvironmentSetupException {
		FileReader reader = null;
		try{
			try{
				reader = new FileReader(new File(_CONFIG_FILE_PATH));
			} catch (FileNotFoundException e) {
				setup();
				reader = new FileReader(new File(_CONFIG_FILE_PATH));
			}
			BufferedReader bufReader = new BufferedReader(reader);
			String p[];
			String line;
			while((line = bufReader.readLine()) != null){
				p = line.split(_SEPARATOR);
				if(p.length != 2){
					bufReader.close();
					throw new RuntimeException("the configuration file is not valid");
				}
				
				if(p[0].equals(key)){
						bufReader.close();
						return p[1];				
				}
			}
			bufReader.close();
			return null;
		} catch (IOException e){
			throw new EnvironmentSetupException(e);
		}
	}
	
	/**
	 * Load the information contained in the configuration file into a data structure use as a buffer
	 * @throws EnvironmentSetupException
	 */
	private static void loadConfigFileIntoConfigBuffer() throws EnvironmentSetupException{
		String value;
		if((value = getEnv(KEY_ENGINE)) != null){
			_configurationBuffer.put(KEY_ENGINE, value);
			if(value.toLowerCase().equals("matlab")) {
				_ENGINE_USED = ENGINE.MATLAB; 
			}
			else if(value.toLowerCase().equals("octave")) {
				_ENGINE_USED = ENGINE.OCTAVE; 
			}
			else{
				throw new EnvironmentSetupException("The engine information contained in the configuration file is not valid");
			}
		}
		
		if((value = getEnv(KEY_MATLAB)) != null)
			_configurationBuffer.put(KEY_MATLAB, value);
		if((value = getEnv(KEY_MATLAB_INC)) != null)
			_configurationBuffer.put(KEY_MATLAB_INC, value);
		if((value = getEnv(KEY_MATIO)) != null)
			_configurationBuffer.put(KEY_MATIO, value);
		if((value = getEnv(KEY_MATIO_INC)) != null)
			_configurationBuffer.put(KEY_MATIO_INC, value);
		
		if((value = getEnv(KEY_OCTAVE)) != null)
			_configurationBuffer.put(KEY_OCTAVE, value);
		
		if((value = getEnv(KEY_SYSTEMC)) != null)
			_configurationBuffer.put(KEY_SYSTEMC, value);
		if((value = getEnv(KEY_SYSTEMC_INC)) != null)
			_configurationBuffer.put(KEY_SYSTEMC_INC, value);
		if((value = getEnv(KEY_SIMUMANAGER)) != null)
			_configurationBuffer.put(KEY_SIMUMANAGER, value);
		if((value = getEnv(KEY_SIMUMANAGER_INC)) != null)
			_configurationBuffer.put(KEY_SIMUMANAGER_INC, value);
		
		if((value = getEnv(KEY_XML2)) != null)
			_configurationBuffer.put(KEY_XML2, value);
		if((value = getEnv(KEY_XML2_INC)) != null)
			_configurationBuffer.put(KEY_XML2_INC, value);
	}
	
	/**
	 * Write key:value into the configuration buffer
	 * @param key
	 * @param value
	 */
	private static void writeInformationToConfig(String key, String value) {
		_configurationBuffer.put(key, value);
	}
	
	/**
	 * Write the content of the configuration buffer in the configuration file
	 * @throws IOException
	 */
	private static void flushConfigBufferToConfigFile() throws IOException{
		FileWriter writer = new FileWriter(_file, false);
		for(String key : _configurationBuffer.keySet()){
			writer.write(key + _SEPARATOR + _configurationBuffer.get(key) + "\n");
		}
		writer.close();
	}
	
	/********************* Method to check missing information in the configuration file *********************/
	/**                                                                                                     **/
	/**                                                                                                     **/
	/**                                                                                                     **/
	/*********************************************************************************************************/
	
	/**
	 * Return true if the engine information is contained into the configuration buffer, false otherwise
	 * @return
	 */
	private static boolean engineInformationMissing(){
		return _configurationBuffer.get(KEY_ENGINE) == null;
	}
	
	/**
	 * Check if all the information except the engine information needed is contained into the buffer
	 * @return true if informations are missing, false otherwise
	 * @throws IOException
	 * @throws EnvironmentSetupException 
	 */
	private static boolean informationMissing() throws IOException, EnvironmentSetupException{
		boolean infos;		
		if(_ENGINE_USED == ENGINE.MATLAB){
			infos =  _configurationBuffer.get(KEY_MATLAB) == null ||  _configurationBuffer.get(KEY_MATLAB_INC) == null;
		}
		else{
			infos =   _configurationBuffer.get(KEY_OCTAVE) == null;
		}
		
		infos = infos ||  _configurationBuffer.get(KEY_SYSTEMC) == null
				||  _configurationBuffer.get(KEY_SYSTEMC_INC) == null
				||  _configurationBuffer.get(KEY_XML2) == null
				||  _configurationBuffer.get(KEY_XML2_INC) == null
				||  _configurationBuffer.get(KEY_SIMUMANAGER) == null
				||  _configurationBuffer.get(KEY_SIMUMANAGER_INC) == null
				|| !(new File( _configurationBuffer.get(KEY_SYSTEMC) + ConstantPathAndName.SYSTEMC_BIN_NAME).isFile())
				|| !(new File( _configurationBuffer.get(KEY_SIMUMANAGER) + ConstantPathAndName.SIMU_MANAGER_BIN_NAME).isFile())
				|| !(new File(_idfixReleaseBinaryPath).isFile())
				|| !(new File(_idfixDebugBinaryPath).isFile());
		
		return infos;
	}
	
	private static void setupProcedure() throws EnvironmentSetupException, IOException{
		if(!IDFixUtils.is_OS_LINUX)
			throw new EnvironmentSetupException("Operator system or arhitecture not managed yet to use IDFix");
		
		// 1 : Get back information of tools used by IDFix before process compilations
		if(_ENGINE_USED == ENGINE.MATLAB){
			setupMatlab();
			setupMatio();
		} else {
			setupOctave();
		}
		setupXml2();

		displayCurrentConfiguration();
		
		// 2 : Run compilation
		_logger.info("1/3 IDFixEval compilation...");
		compileIDFix(IDFIX_COMPILATION_MODE.RELEASE); // Compile release binary
		cleanIDFixEval(false); // Clean compilation file
		compileIDFix(IDFIX_COMPILATION_MODE.TRACE); // Compile trace binary
		cleanIDFixEval(false);
		compileIDFix(IDFIX_COMPILATION_MODE.DEBUG); // Compile debug binary
		cleanIDFixEval(false); // Clean compilation file
		
		_logger.info("2/3 SystemC compilation...");
		compileSystemC();
		cleanSystemC();
		
		_logger.info("3/3 Simulation manager compilation...");
		compileSimulationManager();
		cleanSimulationManager();				
	}
	
	/****************** Method to get back informations of the different tools used in IDFix *****************/
	/**                                                                                                     **/
	/**                                                                                                     **/
	/**                                                                                                     **/
	/*********************************************************************************************************/
	
	/**
	 * Get back information from the user
	 * @param msg message to display to ask information
	 * @return information give by the user
	 * @throws IOException
	 */
	private static String getBackInformations(String msg) throws IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		_logger.info(msg);
		String info = reader.readLine();
		return info;
	}
	
	/**
	 * Use to get back the user selection engine used during IDFix-AccEval process (Matlab or Octave)
	 * @return
	 * @throws EnvironmentSetupException
	 * @throws IOException
	 */
	private static ENGINE engineUserSelection() throws EnvironmentSetupException, IOException {
		String engine;
		if((engine = _configurationBuffer.get(KEY_ENGINE)) == null){			
			do{
				engine = getBackInformations("Specify the engine to used (matlab or octave) :");
				if(engine.toLowerCase().equals("matlab")) {
					writeInformationToConfig(KEY_ENGINE, "matlab");
					return ENGINE.MATLAB; 
				}
				if(engine.toLowerCase().equals("octave")) {
					writeInformationToConfig(KEY_ENGINE, "octave");
					return ENGINE.OCTAVE; 
				}
			} while(true);
		
		}
		else{
			if(engine.toLowerCase().equals("matlab")) {
				return ENGINE.MATLAB; 
			}
			if(engine.toLowerCase().equals("octave")) {
				return ENGINE.OCTAVE; 
			}
			else{
				throw new EnvironmentSetupException("The engine information contained in the configuration file is not valid");
			}
		}
	}
	
	/**
	 * Use to get back and save the information needed about Octave
	 * @throws IOException
	 * @throws EnvironmentSetupException 
	 */
	private static void setupOctave() throws IOException, EnvironmentSetupException {
		if(_configurationBuffer.get(KEY_OCTAVE) == null){
			String pathBinToSearch, binname;
			if(IDFixUtils.is_OS_LINUX) {
				pathBinToSearch = "/usr/bin/";
				binname = "octave";
			}
			else if (IDFixUtils.is_OS_MAC){
				throw new EnvironmentSetupException("Not yet implemented");
			}
			else {
				throw new EnvironmentSetupException("OS not managed");
			}
			
			// Binary search
			if(!(findFile(pathBinToSearch, binname))){
				do{
					pathBinToSearch = getBackInformations("Specify the path of " + binname + " binary :");
				} while(!findFile(pathBinToSearch, binname));
			}
			writeInformationToConfig(KEY_OCTAVE, pathBinToSearch);
		}
	}
	
	/**
	 * Use to get back and save the information needed about Matlab
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws EnvironmentSetupException 
	 */
	private static void setupMatlab() throws IOException, EnvironmentSetupException{
		if(_configurationBuffer.get(KEY_MATLAB) == null){
			String path = "";
			do{
				path = getBackInformations("Specify the matlab installation folder (bin/arch/ folder) :");
			} while((!findFile(path, "libeng.so")));
			writeInformationToConfig(KEY_MATLAB, path);
		}
		else{
			_logger.info("Matlab binary path folder already specified...");
		}
		
		if( _configurationBuffer.get(KEY_MATLAB_INC) == null){
		String path = "";
			do{
				path = getBackInformations("Specify the matlab include folder (extern/include/ folder) :");
			} while((!findFile(path, "engine.h")));
			writeInformationToConfig(KEY_MATLAB_INC, path);
		}
		else{
			_logger.info("Matlab include folder already specified...");
		}
	}
	
	/**
	 * Detect if possible the where is the library libmatio, ask to the user if not found.
	 * After get back the information, write into the configuration file
	 * @throws IOException
	 * @throws EnvironmentSetupException 
	 */
	private static void setupMatio() throws IOException, EnvironmentSetupException {
		if(_configurationBuffer.get(KEY_MATIO) == null){
			String pathIncToSearch, libname, incname;
			List<String> listPathLibToSearch = new LinkedList<String>();
			if(IDFixUtils.is_OS_LINUX) {
				pathIncToSearch = "/usr/include";
				libname = "libmatio.so";
				incname = "matio.h";
				if(IDFixUtils.is_64_arch){
					listPathLibToSearch.add("/usr/lib");
					listPathLibToSearch.add("/usr/lib/x86_64-linux-gnu/");
				}
				else{
					listPathLibToSearch.add("/usr/lib");
					listPathLibToSearch.add("/usr/lib/i386-linux-gnu");
				}
			}
			else if (IDFixUtils.is_OS_MAC){
				throw new EnvironmentSetupException("Not yet implemented");
			}
			else {
				throw new EnvironmentSetupException("OS not managed");
			}
			
			// Library search
			boolean libFind = false;
			for(String pathLibToSearch : listPathLibToSearch){
				if((new File(pathLibToSearch)).isDirectory() && (findFileFromNameBeginning(pathLibToSearch, libname))){
					libFind = true;
					writeInformationToConfig(KEY_MATIO, pathLibToSearch);
					break;
				}
			}
			if(!libFind){
				String pathLibToSearch;
				do{
					pathLibToSearch = getBackInformations("Specify the path of " + libname + " library :");
				} while(!findFileFromNameBeginning(pathLibToSearch, libname));
				writeInformationToConfig(KEY_MATIO, pathLibToSearch);
			}
			
			// Headers search
			if(!findFile(pathIncToSearch, incname)){
				do{
					pathIncToSearch = getBackInformations("Specify the headers folder of " + libname + " library :");
				} while(!findFile(pathIncToSearch, incname));
			}
			writeInformationToConfig(KEY_MATIO_INC, pathIncToSearch);
		}
	}
	
	/**
	 * Detect if possible the where is the library libxml2, ask to the user if not found.
	 * After get back the information, write into the configuration file
	 * @throws IOException
	 * @throws EnvironmentSetupException 
	 */
	private static void setupXml2() throws IOException, EnvironmentSetupException {
		if(_configurationBuffer.get(KEY_XML2) == null){
			String pathIncToSearch, libname, incname;
			List<String> listPathLibToSearch = new LinkedList<String>();
			if(IDFixUtils.is_OS_LINUX) {
				pathIncToSearch = "/usr/include/libxml2";
				libname = "libxml2.so";
				incname = "libxml";
				if(IDFixUtils.is_64_arch){
					listPathLibToSearch.add("/usr/lib");
					listPathLibToSearch.add("/usr/lib/x86_64-linux-gnu/");
				}
				else{
					listPathLibToSearch.add("/usr/lib");
					listPathLibToSearch.add("/usr/lib/i386-linux-gnu");
				}
			}
			else if (IDFixUtils.is_OS_MAC){
				throw new EnvironmentSetupException("Not yet implemented");
			}
			else {
				throw new EnvironmentSetupException("OS not managed");
			}
			
			// Library search
			boolean libFind = false;
			for(String pathLibToSearch : listPathLibToSearch){
				if((new File(pathLibToSearch)).isDirectory() && (findFileFromNameBeginning(pathLibToSearch, libname))){
					libFind = true;
					writeInformationToConfig(KEY_XML2, pathLibToSearch);
					break;
				}
			}
			if(!libFind){
				String pathLibToSearch;
				do{
					pathLibToSearch = getBackInformations("Specify the path of " + libname + " library :");
				} while(!findFileFromNameBeginning(pathLibToSearch, libname));
				writeInformationToConfig(KEY_XML2, pathLibToSearch);
			}
			
			// Headers search
			if(!findFile(pathIncToSearch, incname)){
				do{
					pathIncToSearch = getBackInformations("Specify the headers folder of " + libname + " library :");
				} while(!findFile(pathIncToSearch, incname));
			}
			writeInformationToConfig(KEY_XML2_INC, pathIncToSearch);
		}
	}
	
	
	
	/*********************      Method to compile tools or libraries used by IDFix        ********************/
	/**                                                                                                     **/
	/**                                                                                                     **/
	/**                                                                                                     **/
	/*********************************************************************************************************/
	
	
	/**
	 * Compile systemC library used by the simulation
	 * @throws EnvironmentSetupException
	 * @throws IOException
	 */
	private static void compileSystemC() throws EnvironmentSetupException, IOException{
		String folderArch;
		if(IDFixUtils.is_OS_LINUX && IDFixUtils.is_64_arch)
			folderArch = ConstantPathAndName.SYSTEMC_BIN_64_FOLDER_NAME;
		else if(IDFixUtils.is_OS_LINUX && IDFixUtils.is_32_arch)
			folderArch = ConstantPathAndName.SYSTEMC_BIN_32_FOLDER_NAME;
		else
			throw new EnvironmentSetupException("Operator system or arhitecture not managed yet to use systemC");
		
		File binFolder = new File(ConstantPathAndName.SYSTEMC_FOLDER_PATH + ConstantPathAndName.SYSTEMC_BIN_FODLER_NAME + folderArch);
		if(!binFolder.isDirectory() && !binFolder.mkdirs())
			throw new EnvironmentSetupException("SystemC binaries folder doesn't exist or can't be created");
		
		File incFolder = new File(ConstantPathAndName.SYSTEMC_FOLDER_PATH + ConstantPathAndName.SYSTEMC_BIN_FODLER_NAME + ConstantPathAndName.SYSTEMC_INCLUDE_FODLER_NAME);
		if(!incFolder.isDirectory() && !incFolder.mkdirs())
			throw new EnvironmentSetupException("SystemC headers folder doesn't exist or can't be created");
			
		configurateSystemC();
		buildSystemC();
		
		writeInformationToConfig(KEY_SYSTEMC, ConstantPathAndName.SYSTEMC_FOLDER_PATH + ConstantPathAndName.SYSTEMC_BIN_FODLER_NAME + folderArch);
		writeInformationToConfig(KEY_SYSTEMC_INC, ConstantPathAndName.SYSTEMC_FOLDER_PATH + ConstantPathAndName.SYSTEMC_BIN_FODLER_NAME + ConstantPathAndName.SYSTEMC_INCLUDE_FODLER_NAME);
	}
	
	/**
	 * Run the configurate step of autotools of systemC
	 * @throws EnvironmentSetupException
	 */
	private static void configurateSystemC() throws EnvironmentSetupException{
		File configurate = new File(ConstantPathAndName.SYSTEMC_FOLDER_PATH + ConstantPathAndName.SYSTEMC_BUILD_FOLDER_NAME + "configure");
		configurate.setExecutable(true);		
		if(!configurate.canExecute())
			throw new EnvironmentSetupException("Problem during setting file rule for the configure file to compile systemc");
		
		File configqt = new File(ConstantPathAndName.SYSTEMC_FOLDER_PATH + ConstantPathAndName.SYSTEMC_BUILD_FOLDER_NAME + "src/sysc/qt/config");
		configqt.setExecutable(true);		
		if(!configqt.canExecute())
			throw new EnvironmentSetupException("Problem during setting file rule for the configure file to compile systemc");
			
		List<String> cmd = new LinkedList<String>();
		cmd.add("./configure");
		cmd.add("--prefix=" + ConstantPathAndName.SYSTEMC_FOLDER_PATH + ConstantPathAndName.SYSTEMC_BIN_FODLER_NAME);
		ProcessBuilder pb = new ProcessBuilder(cmd);
		pb.directory(new File(ConstantPathAndName.SYSTEMC_FOLDER_PATH + ConstantPathAndName.SYSTEMC_BUILD_FOLDER_NAME));
		try {
			Process p = pb.start();
			
			BufferedReader errReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			ListenProcessInputStandardStream inputReader = new ListenProcessInputStandardStream(p.getInputStream(), _logger, Level.DEBUG);
			inputReader.start();
			String line;
			StringBuffer bufLine = new StringBuffer();
			while((line = errReader.readLine()) != null){
				bufLine.append(line).append("\n");
			}
			p.waitFor();
			inputReader.join();
			
			if(bufLine.length() > 0){
				_logger.debug(bufLine.toString());
				throw new EnvironmentSetupException("build configurate of systemc library failed");
			}
		} catch (IOException | InterruptedException e) {
			throw new EnvironmentSetupException("build configurate of systemc library failed", e);
		}
	}
	
	/**
	 * Run the compilation/installation of systemC
	 * @throws EnvironmentSetupException
	 */
	private static void buildSystemC() throws EnvironmentSetupException{
		List<String> cmd = new LinkedList<String>();
		cmd.add("make");
		// TODO add mutli thread compilation
//		cmd.add("-j");
//		cmd.add(Integer.toString(AdditionalOptions.getNbThread()));
		cmd.add("install");
		ProcessBuilder pb = new ProcessBuilder(cmd);
		pb.directory(new File(ConstantPathAndName.SYSTEMC_FOLDER_PATH + ConstantPathAndName.SYSTEMC_BUILD_FOLDER_NAME));
		try {
			Process p = pb.start();
			
			BufferedReader errReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			ListenProcessInputStandardStream inputReader = new ListenProcessInputStandardStream(p.getInputStream(), _logger, Level.DEBUG);
			inputReader.start();
			String line;
			StringBuffer bufLine = new StringBuffer();
			while((line = errReader.readLine()) != null){
				bufLine.append(line).append("\n");
			}
			p.waitFor();
			inputReader.join();		
			
			if(bufLine.length() > 0){
				_logger.debug(bufLine.toString());
				String folderArch;
				if(IDFixUtils.is_OS_LINUX && IDFixUtils.is_64_arch)
					folderArch = ConstantPathAndName.SYSTEMC_BIN_64_FOLDER_NAME;
				else if(IDFixUtils.is_OS_LINUX && IDFixUtils.is_32_arch)
					folderArch = ConstantPathAndName.SYSTEMC_BIN_32_FOLDER_NAME;
				else
					throw new EnvironmentSetupException("Operator system or arhitecture not managed yet to use systemC");
				if(!(new File(ConstantPathAndName.SYSTEMC_FOLDER_PATH + ConstantPathAndName.SYSTEMC_BIN_FODLER_NAME + folderArch + ConstantPathAndName.SYSTEMC_BIN_NAME).isFile()))
					throw new EnvironmentSetupException("build of systemc library failed");
			}
		} catch (IOException | InterruptedException e) {
			throw new EnvironmentSetupException("build of systemc library failed", e);

		}
	}
	
	/**
	 * Clean the compilation temporary object created 
	 * @throws EnvironmentSetupException
	 */
	private static void cleanSystemC() throws EnvironmentSetupException {
		List<String> cmd = new LinkedList<String>();
		cmd.add("make");
		cmd.add("clean");
		ProcessBuilder pb = new ProcessBuilder(cmd);
		pb.directory(new File(ConstantPathAndName.SYSTEMC_FOLDER_PATH + ConstantPathAndName.SYSTEMC_BUILD_FOLDER_NAME));
		try {
			Process p = pb.start();
			
			BufferedReader errReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			ListenProcessInputStandardStream inputReader = new ListenProcessInputStandardStream(p.getInputStream(), _logger, Level.DEBUG);
			inputReader.start();
			String line;
			StringBuffer bufLine = new StringBuffer();
			while((line = errReader.readLine()) != null){
				bufLine.append(line).append("\n");
			}
			p.waitFor();
			inputReader.join();		
			
			if(bufLine.length() > 0){
				_logger.error(bufLine.toString());
				throw new EnvironmentSetupException("clean of systemc library failed");
			}
		} catch (IOException | InterruptedException e) {
			throw new EnvironmentSetupException("clean of systemc library failed", e);
		}
		
		cmd.clear();
		
		
	}
	
	/**
	 * Run the uninstallation option the makefile
	 * @throws EnvironmentSetupException
	 */
	private static void uninstallSystemC() throws EnvironmentSetupException{
		List<String> cmd = new LinkedList<String>();
		cmd.add("make");
		cmd.add("uninstall");
		
		ProcessBuilder pb = new ProcessBuilder(cmd);
		pb.directory(new File(ConstantPathAndName.SYSTEMC_FOLDER_PATH + ConstantPathAndName.SYSTEMC_BUILD_FOLDER_NAME));
		try {
			Process p = pb.start();
			
			BufferedReader errReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			ListenProcessInputStandardStream inputReader = new ListenProcessInputStandardStream(p.getInputStream(), _logger, Level.INFO);
			inputReader.start();
			String line;
			StringBuffer bufLine = new StringBuffer();
			while((line = errReader.readLine()) != null){
				bufLine.append(line).append("\n");
			}
			p.waitFor();
			inputReader.join();		
			
			if(bufLine.length() > 0){
				_logger.debug(bufLine.toString());
//				throw new EnvironmentSetupException("clean of systemc library failed");
			}
		} catch (IOException | InterruptedException e) {
			throw new EnvironmentSetupException("uninstall of systemc library failed", e);
		}
	}
	
	private static void compileSimulationManager() throws EnvironmentSetupException, IOException{
		File installHeaderFolder = new File(ConstantPathAndName.SIMU_MANAGER_FOLDER_PATH + ConstantPathAndName.SIMU_MANAGER_BIN_FOLDER_NAME
				+ ConstantPathAndName.SIMU_MANAGER_INCLUDE_FOLDER_NAME);
		if(!installHeaderFolder.isDirectory() && !installHeaderFolder.mkdirs())
			throw new EnvironmentSetupException("SimulationManager binary and headers folder doesn't exist or can't be created");
		
		configurateSimulationManager();
		buildSimulationManager();
		
		IDFixUtils.copyFile(ConstantPathAndName.SIMU_MANAGER_FOLDER_PATH + ConstantPathAndName.SIMU_MANAGER_BUILD_FOLDER_NAME
				+ ConstantPathAndName.SIMU_MANAGER_INCLUDE_FOLDER_NAME + ConstantPathAndName.SIMU_MANAGER_INCLUDE_CWRAPPER_NAME,
				ConstantPathAndName.SIMU_MANAGER_FOLDER_PATH + ConstantPathAndName.SIMU_MANAGER_BIN_FOLDER_NAME
				+ ConstantPathAndName.SIMU_MANAGER_INCLUDE_FOLDER_NAME + ConstantPathAndName.SIMU_MANAGER_INCLUDE_CWRAPPER_NAME);
		
		writeInformationToConfig(KEY_SIMUMANAGER, ConstantPathAndName.SIMU_MANAGER_FOLDER_PATH + ConstantPathAndName.SIMU_MANAGER_BIN_FOLDER_NAME);
		writeInformationToConfig(KEY_SIMUMANAGER_INC, ConstantPathAndName.SIMU_MANAGER_FOLDER_PATH + ConstantPathAndName.SIMU_MANAGER_BIN_FOLDER_NAME
				+ ConstantPathAndName.SIMU_MANAGER_INCLUDE_FOLDER_NAME);
	}
	
	private static void configurateSimulationManager() throws EnvironmentSetupException{
		File script = new File(ConstantPathAndName.SIMU_MANAGER_FOLDER_PATH + ConstantPathAndName.SIMU_MANAGER_BUILD_FOLDER_NAME + "cmake_release.sh");
		if(!script.isFile())
			throw new EnvironmentSetupException("Simulation manager CMake script build not found");
		script.setExecutable(true);
		
		List<String> cmd = new LinkedList<String>();
		cmd.add("./cmake_release.sh");
		
		ProcessBuilder pb = new ProcessBuilder(cmd);
		pb.directory(new File(ConstantPathAndName.SIMU_MANAGER_FOLDER_PATH + ConstantPathAndName.SIMU_MANAGER_BUILD_FOLDER_NAME));
		
		try {
			Process p = pb.start();
			
			BufferedReader errReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			ListenProcessInputStandardStream inputReader = new ListenProcessInputStandardStream(p.getInputStream(), _logger, Level.DEBUG);
			inputReader.start();
			String line;
			StringBuffer bufLine = new StringBuffer();
			while((line = errReader.readLine()) != null){
				bufLine.append(line).append("\n");
			}
			p.waitFor();
			inputReader.join();		
			
			if(bufLine.length() > 0){
				_logger.debug(bufLine.toString());
				throw new EnvironmentSetupException("build of simulation manager library failed");
			}
		} catch (IOException | InterruptedException e) {
			throw new EnvironmentSetupException("build of simulation manager library failed", e);
		}
	}
	
	private static void buildSimulationManager() throws EnvironmentSetupException{
		List<String> cmd = new LinkedList<String>();
		cmd.add("make");
		
		ProcessBuilder pb = new ProcessBuilder(cmd);
		pb.directory(new File(ConstantPathAndName.SIMU_MANAGER_FOLDER_PATH + ConstantPathAndName.SIMU_MANAGER_BUILD_FOLDER_NAME));
		
		try {
			Process p = pb.start();
			
			BufferedReader errReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			ListenProcessInputStandardStream inputReader = new ListenProcessInputStandardStream(p.getInputStream(), _logger, Level.DEBUG);
			inputReader.start();
			String line;
			StringBuffer bufLine = new StringBuffer();
			while((line = errReader.readLine()) != null){
				bufLine.append(line).append("\n");
			}
			p.waitFor();
			inputReader.join();		
			
			if(bufLine.length() > 0){
				_logger.debug(bufLine.toString());
				throw new EnvironmentSetupException("build of simulation manager library failed");
			}
			
			File installHeaderFolder = new File(ConstantPathAndName.SIMU_MANAGER_FOLDER_PATH + ConstantPathAndName.SIMU_MANAGER_BIN_FOLDER_NAME
					+ ConstantPathAndName.SIMU_MANAGER_INCLUDE_FOLDER_NAME);
			if(!installHeaderFolder.isDirectory())
				installHeaderFolder.mkdir();
			IDFixUtils.copyFile(ConstantPathAndName.SIMU_MANAGER_FOLDER_PATH + ConstantPathAndName.SIMU_MANAGER_BUILD_FOLDER_NAME
					+ ConstantPathAndName.SIMU_MANAGER_INCLUDE_FOLDER_NAME + ConstantPathAndName.SIMU_MANAGER_INCLUDE_CWRAPPER_NAME,
					ConstantPathAndName.SIMU_MANAGER_FOLDER_PATH + ConstantPathAndName.SIMU_MANAGER_BIN_FOLDER_NAME
					+ ConstantPathAndName.SIMU_MANAGER_INCLUDE_FOLDER_NAME + ConstantPathAndName.SIMU_MANAGER_INCLUDE_CWRAPPER_NAME);
			
			writeInformationToConfig(KEY_SIMUMANAGER, ConstantPathAndName.SIMU_MANAGER_FOLDER_PATH + ConstantPathAndName.SIMU_MANAGER_BIN_FOLDER_NAME);
			writeInformationToConfig(KEY_SIMUMANAGER_INC, ConstantPathAndName.SIMU_MANAGER_FOLDER_PATH + ConstantPathAndName.SIMU_MANAGER_BIN_FOLDER_NAME
					+ ConstantPathAndName.SIMU_MANAGER_INCLUDE_FOLDER_NAME);
		} catch (IOException | InterruptedException e) {
			throw new EnvironmentSetupException("build of simulation manager library failed", e);
		}
	}
	
	private static void cleanSimulationManager(){
		File cmakefile = new File(ConstantPathAndName.SIMU_MANAGER_FOLDER_PATH + ConstantPathAndName.SIMU_MANAGER_BUILD_FOLDER_NAME + "CMakeFiles");
		if(cmakefile.isDirectory())
			IDFixUtils.deleteDirectory(cmakefile);
		
		File cmakecache = new File(ConstantPathAndName.SIMU_MANAGER_FOLDER_PATH + ConstantPathAndName.SIMU_MANAGER_BUILD_FOLDER_NAME + "CMakeCache.txt");
		if(cmakecache.isFile())
			cmakecache.delete();
		
		File makefile = new File(ConstantPathAndName.SIMU_MANAGER_FOLDER_PATH + ConstantPathAndName.SIMU_MANAGER_BUILD_FOLDER_NAME + "Makefile");
		if(makefile.isFile());
			makefile.delete();
			
		File cmakeinstall = new File(ConstantPathAndName.SIMU_MANAGER_FOLDER_PATH + ConstantPathAndName.SIMU_MANAGER_BUILD_FOLDER_NAME + "cmake_install.cmake");
		if(cmakeinstall.isFile());
			cmakeinstall.delete();
	}

	/**
	 * Search into path a file by name
	 * @param path path to search
	 * @param name Name of the file to find
	 * @return true of the file is found, false otherwise
	 * @throws IOException 
	 */
	private static boolean findFile(String path, String name) throws FileNotFoundException{
		File dir = new File(path);
		if(!dir.isDirectory()){
			_logger.warn(path + " : No such directory");
			return false;
		}
		
		for(File file : dir.listFiles()){
			if(name.equals(file.getName()))
				return true;
		}
		
		return false;
	}
	
	/**
	 * Search into path a file by the beginning of the name
	 * @param path path to search
	 * @param name Name of the file to find
	 * @return true of the file is found, false otherwise
	 * @throws IOException 
	 */
	private static boolean findFileFromNameBeginning(String path, String name) throws FileNotFoundException{
		File dir = new File(path);
		if(!dir.isDirectory()){
			_logger.warn(path + " : No such directory");
			return false;
		}
		
		for(File file : dir.listFiles()){
			if(file.getName().startsWith(name))
				return true;
		}
		
		return false;
	}
	
		
	/**
	 * Compile IDFixEval thanks to environment informations provide by the user
	 * @throws IDFixEvalException
	 * @throws EnvironmentSetupException 
	 */
	private static void compileIDFix(IDFIX_COMPILATION_MODE mode) throws EnvironmentSetupException{		
		String matlablib, matlabinc, matiolib, matioinc, xml2lib, xml2inc;
		
		String pathToMakefile = ConstantPathAndName.IDFIX_EVAL_FOLDER_PATH + ConstantPathAndName.IDFIX_EVAL_BUILD_FOLDER_NAME;
		File filePathToMakefile = new File(pathToMakefile);
		if(!filePathToMakefile.isDirectory())
			throw new EnvironmentSetupException("The directory \""+ pathToMakefile + "\" have been not found");
		
		File binFolder = new File(ConstantPathAndName.IDFIX_EVAL_FOLDER_PATH + ConstantPathAndName.IDFIX_BIN_FOLDER_NAME);
		if(!binFolder.isDirectory() && !binFolder.mkdirs())
			throw new EnvironmentSetupException("IDFix binary folder doesn't exist or can't be created");

		xml2lib =  _configurationBuffer.get(KEY_XML2);
		xml2inc =  _configurationBuffer.get(KEY_XML2_INC);
		if(xml2lib == null || xml2inc == null)
			throw new EnvironmentSetupException("XML2 environment information have not been found in the configuration files (Do a reset of the environment informations)");
		
		String extralibs = "EXTRA_LIBS=" + xml2lib;
		String extraincs = "EXTRA_INCDIR=" + xml2inc;
		if(_ENGINE_USED == ENGINE.MATLAB){
			matiolib = _configurationBuffer.get(KEY_MATIO);
			matioinc = _configurationBuffer.get(KEY_MATIO_INC);
			if(matiolib == null || matioinc == null)
				throw new EnvironmentSetupException("Matio librarenvironment information have not been found in the configuration files (Do a reset of the environment informations)");
			
			matlablib =  _configurationBuffer.get(KEY_MATLAB);
			matlabinc =  _configurationBuffer.get(KEY_MATLAB_INC);
			
			if(matlablib == null || matlabinc == null)
				throw new EnvironmentSetupException("Matlab path information have not been found in the configuration files (Do a reset of the environment informations)");
			
			
			extralibs += " " + matlablib+ " " + matiolib;
			extraincs += " " + matlabinc + " " + matioinc;
		}
		
		List<String> cmd = new LinkedList<String>();
		cmd.add("make");
		String str;
		if(IDFixUtils.is_OS_LINUX){
			if(_ENGINE_USED == ENGINE.MATLAB){
				str = "linuxmatlab";					
			}
			else
				str = "linuxoctave";
		}	
		else if(IDFixUtils.is_OS_MAC){
			throw new UnsupportedOperationException("Mac is not fully supported");
		}
		else
			throw new RuntimeException("OS not manage by the tools");
		
		switch (mode) {
		case TRACE:
			str += "trace";
			break;
		case DEBUG:
			str += "debug";
			break;
		default:
			break;
		}
		
		cmd.add(str);
		cmd.add(extralibs);
		cmd.add(extraincs);
		// TODO add mutli thread compilation
//		cmd.add("-j");
//		cmd.add(String.valueOf(AdditionalOptions.getNbThread()));
		
		ProcessBuilder pb = new ProcessBuilder(cmd);
		pb.directory(filePathToMakefile);
		try {
			if(_ENGINE_USED == ENGINE.MATLAB){
				String value;
				if((value = pb.environment().get("LD_LIBRARY_PATH")) != null){
					value += ":" +  _configurationBuffer.get(KEY_MATLAB);
				}
				else{
					value =  _configurationBuffer.get(KEY_MATLAB);
				}
				pb.environment().put("LD_LIBRARY_PATH", value);
			}
			_logger.info(cmd);
			Process process = pb.start();
			BufferedReader errReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
			ListenProcessInputStandardStream inputReader = new ListenProcessInputStandardStream(process.getInputStream(), _logger, Level.DEBUG);
			inputReader.start();
			String line;
			StringBuffer bufLine = new StringBuffer();
			while((line = errReader.readLine()) != null){
				bufLine.append(line).append("\n");
			}
			process.waitFor();
			inputReader.join();
			
			if(bufLine.length() > 0){
				_logger.debug(bufLine.toString());
				throw new EnvironmentSetupException("Compilation of IDFixEval failed");
			}
		} catch (IOException | InterruptedException e) {
			throw new EnvironmentSetupException("Compilation of IDFixEval failed", e);
		}
	}
	
	/**
	 * Clean the build of IDFixFixEval.
	 * @param all clean *.o files, build folder and delete IDFixEval binary if true, just clean *.o files and build folder otherwise 
	 * @throws EnvironmentSetupException 
	 */
	private static void cleanIDFixEval(boolean all) throws EnvironmentSetupException {
		String pathToMakefile = ConstantPathAndName.IDFIX_EVAL_FOLDER_PATH + ConstantPathAndName.IDFIX_EVAL_BUILD_FOLDER_NAME;
		File filePathToMakefile = new File(pathToMakefile);
		if(!filePathToMakefile.isDirectory())
			throw new RuntimeException("The directory \""+ pathToMakefile + "\" have been not found");
		
		List<String> cmd = new LinkedList<String>();
		cmd.add("make");
		if(all)
			cmd.add("cleanalllinux");
		else
			cmd.add("cleanlinux");
		ProcessBuilder pb = new ProcessBuilder(cmd);
		pb.directory(filePathToMakefile);
		try {
			Process process = pb.start();
			
			BufferedReader errReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
			ListenProcessInputStandardStream inputReader = new ListenProcessInputStandardStream(process.getInputStream(), _logger, Level.DEBUG);
			inputReader.start();
			String line;
			StringBuffer bufLine = new StringBuffer();
			while((line = errReader.readLine()) != null){
				bufLine.append(line).append("\n");
			}
			process.waitFor();
			inputReader.join();
			
			if(bufLine.length() > 0){
				_logger.debug(bufLine.toString());
				throw new EnvironmentSetupException("IDFixEval cleaning failed");
			}
		} catch (IOException | InterruptedException e) {
			throw new EnvironmentSetupException("IDFixEval cleaning failed", e);
		} 
	}
}