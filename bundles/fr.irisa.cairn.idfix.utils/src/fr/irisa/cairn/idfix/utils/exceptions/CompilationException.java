package fr.irisa.cairn.idfix.utils.exceptions;

@SuppressWarnings("serial")
public class CompilationException extends Exception {
	public CompilationException(String msg){
		super(msg);
	}
	
	public CompilationException(Throwable cause){
		super(cause);
	}
}
