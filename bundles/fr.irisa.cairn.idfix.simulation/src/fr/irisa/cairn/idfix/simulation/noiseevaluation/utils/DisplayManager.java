package fr.irisa.cairn.idfix.simulation.noiseevaluation.utils;

import org.apache.logging.log4j.Logger;


/**
 * Use to display simulation result
 * @author nsimon
 * @date 24/01/14
 *
 */
public class DisplayManager {
	private double _analyticNoiseSolution;
	private double _simulationNoiseSolution;
	private float _number_of_simulation;
	private String _title;
	
	private Logger _logger;
	
	public DisplayManager(float number_of_simulation, double analyticNoiseSolution, double simulationNoiseResult, String title, Logger logger){
		_number_of_simulation = number_of_simulation;
		_analyticNoiseSolution = analyticNoiseSolution;
		_simulationNoiseSolution = simulationNoiseResult;
		_title = title;
		_logger = logger;
	}
	
	public void display(){
		_logger.info("---------------------------------------- " + _title + " ----------------------------------------");
		_logger.info("    Number of simulation : " + _number_of_simulation);
		_logger.info("    Solution noise power (db) : " + _simulationNoiseSolution + "\n");
		if(_analyticNoiseSolution != Float.NaN);
			_logger.info("    Difference analytic/simulation (db) : " + (_analyticNoiseSolution - _simulationNoiseSolution));	
	}
}
