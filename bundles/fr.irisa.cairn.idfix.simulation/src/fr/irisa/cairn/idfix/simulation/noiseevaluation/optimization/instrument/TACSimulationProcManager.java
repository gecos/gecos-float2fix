package fr.irisa.cairn.idfix.simulation.noiseevaluation.optimization.instrument;

import fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.Triplet;
import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.core.Symbol;
import gecos.instrs.Instruction;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Add temporary variable for each operand and output of operator.
 * This transformation let to quantify the number of bit of the operator thanks to data (temporary variable)
 * @author Nicolas Simon
 * @date Apr 15, 2013
 * 
 * TODO Use the logger to get back error message
 */
public class TACSimulationProcManager {
	private TACSimulationBlockManager _blockManager;
	private TACSimulationInstManager _instManager;
	private Map<Integer, List<Symbol>> _setOperatorToNewTemporarySymbol;
	private Map<Integer, List<Triplet<Symbol,Symbol,Symbol>>> _genericOperatorToNewTemporarySymbol;
	private int _numeroTmpVariable; // Number of the current temporary variable into the current procedure
	
	public TACSimulationProcManager(){
		_setOperatorToNewTemporarySymbol = new HashMap<Integer, List<Symbol>>();
		_genericOperatorToNewTemporarySymbol = new HashMap<Integer, List<Triplet<Symbol,Symbol,Symbol>>>();
		_blockManager = new TACSimulationBlockManager(this);
		_instManager = new TACSimulationInstManager(this);
		_numeroTmpVariable = 0;
	}
	
	public int useNumeroTmpVariable(){
		int res = _numeroTmpVariable;
		_numeroTmpVariable++;
		return res;
	}
	
	public TACSimulationBlockManager getBlockManager(){
		return _blockManager;
	}
	
	public TACSimulationInstManager getInstManager(){
		return _instManager;
	}
	
	public Map<Integer, List<Triplet<Symbol,Symbol,Symbol>>> getMapGenericOperatorToNewTemporarySymbol(){
		return _genericOperatorToNewTemporarySymbol;
	}
	
	public Map<Integer, List<Symbol>> getMapSetOperatorToNewTemporarySymbol(){
		return _setOperatorToNewTemporarySymbol;
	}
	
	public void doSwitch(Procedure p){
		_numeroTmpVariable = 0;
		doSwitch(p.getStart());
		doSwitch(p.getBody());
		doSwitch(p.getEnd());
	}
	
	public Object doSwitch(Block b){
		return _blockManager.doSwitch(b);
	}
	
	public Object doSwitch(Instruction inst){
		return _instManager.doSwitch(inst);
	}
	
	
}
