package fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.result;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;

public class OldVersion extends DefaultResultManager {
	private double noisePower;
	
	public OldVersion(String pathFloatResultFolder,
			String pathFixedResultFolder, double analyticNoiseSolution, int numberOfSimulation) {
		super(pathFloatResultFolder, pathFixedResultFolder, analyticNoiseSolution, numberOfSimulation);
	}

	@Override
	protected double computeNoisePower() throws SimulationException {
		if(_floatValues.size() != _fixedValues.size())
			throw new SimulationException("The number of variable profiled in the float simulation is different from the number of variable profiled in the fixed simulation");
		
		Map<String, double[]> diff2map = new HashMap<>();
		Map<String, Double> meansumdiff2 = new HashMap<>();
		
		double[] floatvalues, fixedvalues;
		
		// Compute the difference between each output and square it
		for(String key : _floatValues.keySet()){
			floatvalues = _floatValues.get(key);
			if(!_fixedValues.containsKey(key))
				throw new SimulationException("Variable profiled in the float simulation is not present in the fixed simulation");			
			fixedvalues = _fixedValues.get(key);
			
			if(fixedvalues.length != floatvalues.length)
				throw new SimulationException("Number of sample save for a float variable are not equal for the same fixed variable");
			
			double[] diff2 = new double[floatvalues.length];
			for(int i = 0 ; i < floatvalues.length ; i++){
				double diff = floatvalues[i] - fixedvalues[i];
				diff2[i] = diff*diff;
			}
			diff2map.put(key, diff2);
		}
		
		// Compute the mean of each output
		for(String key : diff2map.keySet()){
			double sum = 0;
			for(double value : diff2map.get(key)){
				sum += value;
			}
			meansumdiff2.put(key, sum/diff2map.get(key).length);
		}
		
		// Compute the mean of the mean of each output
		double sum = 0;
		for(String key : meansumdiff2.keySet()){
			sum += meansumdiff2.get(key);
		}
		
		noisePower = 10 * Math.log10(sum/meansumdiff2.size());
		
		return noisePower;
	}

	@Override
	public void display(String title, PrintWriter writer) {
		writer.println("---------------------------------------- " + title + " ----------------------------------------");
		writer.println("    Number of simulation : " + _numberOfSimulation);
		writer.println("    Solution noise power (db) : " + noisePower + "\n");
		if(_analyticNoiseSolution != Double.NaN)
			writer.println("    Difference analytic/simulation (db) : " + (_analyticNoiseSolution - noisePower));	
	}
	
	@Override
	public void display(String title, Logger logger) {
		logger.info("---------------------------------------- " + title + " ----------------------------------------");
		logger.info("    Number of simulation : " + _numberOfSimulation);
		logger.info("    Solution noise power (db) : " + noisePower + "\n");
		if(_analyticNoiseSolution != Double.NaN)
			_logger.info("    Difference analytic/simulation (db) : " + (_analyticNoiseSolution - noisePower));	
	}
}
