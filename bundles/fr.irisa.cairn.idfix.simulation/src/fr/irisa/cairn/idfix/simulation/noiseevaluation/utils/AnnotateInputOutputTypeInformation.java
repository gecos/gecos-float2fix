package fr.irisa.cairn.idfix.simulation.noiseevaluation.utils;


import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.tools.switches.DefaultTypeSwitch;
import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;
import gecos.annotations.IAnnotation;
import gecos.core.Procedure;
import gecos.core.Symbol;
import gecos.types.ArrayType;
import gecos.types.BaseType;
import gecos.types.IntegerType;
import gecos.types.PtrType;

public class AnnotateInputOutputTypeInformation {
	public static final String ANNOT_KEY = "type";
	public static final String FLOAT_TYPE = "float";
	public static final String DOUBLE_TYPE = "double";
	public static final String INT_TYPE = "int";
	public static final String UINT_TYPE = "uint";
	public static final String SHORT_TYPE = "short";
	public static final String USHORT_TYPE = "ushort";
	public static final String LONG_TYPE = "long";
	public static final String ULONG_TYPE = "ulong";
	public static final String LONGLONG_TYPE = "longlong";
	public static final String ULONGLONG_TYPE = "ulonglong";
	
	private _TypeChecker _typeChecker = new _TypeChecker();

	public void process(Procedure p) throws SimulationException{
		BaseType type;
		IAnnotation annot;
		for(Symbol s : p.listParameters()){
			if((type = _typeChecker.doSwitch(s.getType())) != null){
				if(type.asFloat() != null){
					if(type.getSize() == GecosUserTypeFactory.TypeSizes.defaultFloatSize){
						annot = GecosUserAnnotationFactory.STRING(FLOAT_TYPE);
					} else if(type.getSize() == GecosUserTypeFactory.TypeSizes.defaultDoubleSize){
						annot = GecosUserAnnotationFactory.STRING(DOUBLE_TYPE);
					} else {
						throw new SimulationException("the input " + s.getName() + " of the system have a unknown gecos int size type (" + type +")");
					}
				} else if(type.asInt() != null){
					IntegerType intType = type.asInt();
					if(type.getSize() == GecosUserTypeFactory.TypeSizes.defaultIntSize){
						if(intType.getSigned())
							annot = GecosUserAnnotationFactory.STRING(INT_TYPE);
						else
							annot = GecosUserAnnotationFactory.STRING(UINT_TYPE);
					} else if(type.getSize() == GecosUserTypeFactory.TypeSizes.defaultShortSize){
						if(intType.getSigned())
							annot = GecosUserAnnotationFactory.STRING(SHORT_TYPE);
						else
							annot = GecosUserAnnotationFactory.STRING(USHORT_TYPE);
					} else if(type.getSize() == GecosUserTypeFactory.TypeSizes.defaultLongSize){
						if(intType.getSigned())
							annot = GecosUserAnnotationFactory.STRING(LONG_TYPE);
						else
							annot = GecosUserAnnotationFactory.STRING(ULONG_TYPE);
					} else if(type.getSize() == GecosUserTypeFactory.TypeSizes.defaultLongLongSize){
						if(intType.getSigned())
							annot = GecosUserAnnotationFactory.STRING(LONGLONG_TYPE);
						else
							annot = GecosUserAnnotationFactory.STRING(ULONGLONG_TYPE);
					} else {
						throw new SimulationException("the input " + s.getName() + " of the system have a unknown gecos int size type (" + type +")");
					}
				} else {
					throw new SimulationException("the input " + s.getName() + " of the system have not a float/int base type (" + type +")");
				}
				s.setAnnotation(ANNOT_KEY, annot);
			}
			else{
				throw new SimulationException("Gecos Type not managed " + type);
			}
		}
	}
	
	private class _TypeChecker extends DefaultTypeSwitch<BaseType>{
		@Override
		public BaseType caseBaseType(BaseType object) {
			return object;
		}

		@Override
		public BaseType casePtrType(PtrType object) {
			return doSwitch(object.getBase());
		}

		@Override
		public BaseType caseArrayType(ArrayType object) {
			return doSwitch(object.getBase());
		}
	}	
}
