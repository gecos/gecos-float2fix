package fr.irisa.cairn.idfix.simulation.noiseevaluation.instrument;

import fr.irisa.cairn.gecos.model.analysis.types.ArrayLevel;
import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.idfix.utils.IDFixUtils;
import fr.irisa.cairn.idfix.utils.exceptions.SFGModelCreationException;
import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.Symbol;
import gecos.types.PtrType;


/**
 * For array with more 2 dimension, we need to transform function prototype to transfrom array type to a pointer type 
 * @author nsimon
 * @date 11/04/2014
 *
 */
public class TypeFixer {
	private static final void _assert(boolean b, String msg) throws SimulationException 
	{
		if(!b) throw new SimulationException(msg);
	}
	
	/**
	 * Check type of each parameters with the output and dynamic parameters.
	 * If the type is an array type with more than 1 dimension, we modify the array static type to array dynamic type
	 * @throws SimulationException 
	 */
	public static void fixStaticArrayCompilation(ProcedureSet ps) throws SimulationException{		
		try{
			for(Procedure p : ps.listProcedures()){
				for(Symbol param : p.listParameters()){
					if(IDFixUtils.isInputVar(param) || IDFixUtils.isOutputVar(param)){
						transformStaticArrayToDynamicArray(param);
					}
				}
			}
		} catch (SFGModelCreationException e) {
			throw new SimulationException(e);
		}
	}
	
	private static void transformStaticArrayToDynamicArray(Symbol s) throws SimulationException{
		TypeAnalyzer analyzer = new TypeAnalyzer(s.getType());
		if(analyzer.isArray()){
			ArrayLevel arrLevel = analyzer.tryGetArrayLevel(0);
			_assert(arrLevel != null, "Error in the analyze of a type by the TypeAnalyser");
			
			if(arrLevel.getNDims() > 1){
				PtrType ptrType = GecosUserTypeFactory.PTR(analyzer.getBase());
				for(int i = 0 ; i < arrLevel.getNDims() -1 ; i++){
					ptrType = GecosUserTypeFactory.PTR(ptrType);
				}
				s.setType(analyzer.revertBaseLevelOn(ptrType));
			}
		}
	}
}
