package fr.irisa.cairn.idfix.simulation.noiseevaluation.instrument;

import fr.irisa.cairn.gecos.model.analysis.types.ArrayLevel;
import fr.irisa.cairn.gecos.model.analysis.types.BaseLevel;
import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.tools.switches.BasicBlockSwitch;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.IDFixUtils;
import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;
import fr.irisa.cairn.idfix.utils.logging.IDFixLogger;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.Instruction;
import gecos.instrs.RetInstruction;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.Logger;
/**
 * For each procedure in a procedure set, detect the position to add call function to save output variable.
 * Add a save function at each end of output symbol scope.
 * @author nsimon
 * @date 21/01/14
 *
 */
public class InstructionInstrumentationManager {
	private ProcedureSet _ps;
	
	private int _profiling_number;
	
	// Manager library symbol
	private ProcedureSymbol _saveDoubleVariableProcSymbol;
	@SuppressWarnings("unused")
	private ProcedureSymbol _saveFloatVariableProcSymbol;
	private ProcedureSymbol _saveLongVariableProcSymbol;
	@SuppressWarnings("unused")
	private ProcedureSymbol _saveIntVariableProcSymbol;
	@SuppressWarnings("unused")
	private ProcedureSymbol _saveUIntVariableProcSymbol;
	private ProcedureSymbol _saveDoubleArrayVariableProcSymbol;
	@SuppressWarnings("unused")
	private ProcedureSymbol _saveFloatArrayVariableProcSymbol;
	private ProcedureSymbol _saveLongArrayVariableProcSymbol;
	@SuppressWarnings("unused")
	private ProcedureSymbol _saveIntArrayVariableProcSymbol;
	@SuppressWarnings("unused")
	private ProcedureSymbol _saveUIntArrayVariableProcSymbol;
	private Symbol _manager;
	
	private final Logger _logger = IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_OPTIMIZATION);
	
	public InstructionInstrumentationManager(ProcedureSet ps) throws SimulationException{
		_ps = ps;
		_profiling_number = 0;
				
		Symbol saveDoubleVariable = _ps.getScope().lookup("SimulationManager_saveDoubleVariable");
		if(saveDoubleVariable == null || !(saveDoubleVariable instanceof ProcedureSymbol))
			throw new SimulationException("Simulation manager library saveDoubleVariable function not found in the procedure set");
		_saveDoubleVariableProcSymbol = (ProcedureSymbol) saveDoubleVariable;
		
		Symbol saveFloatVariable = _ps.getScope().lookup("SimulationManager_saveFloatVariable");
		if(saveFloatVariable == null || !(saveFloatVariable instanceof ProcedureSymbol))
			throw new SimulationException("Simulation manager library saveFloatVariable function not found in the procedure set");
		_saveFloatVariableProcSymbol = (ProcedureSymbol) saveDoubleVariable;
		
		Symbol saveLongVariable = _ps.getScope().lookup("SimulationManager_saveLongVariable");
		if(saveLongVariable == null || !(saveLongVariable instanceof ProcedureSymbol))
			throw new SimulationException("Simulation manager library saveLongVariable function not found in the procedure set");
		_saveLongVariableProcSymbol = (ProcedureSymbol) saveLongVariable;
		
		Symbol saveIntVariable = _ps.getScope().lookup("SimulationManager_saveIntVariable");
		if(saveIntVariable == null || !(saveIntVariable instanceof ProcedureSymbol))
			throw new SimulationException("Simulation manager library saveIntVariable function not found in the procedure set");
		_saveIntVariableProcSymbol = (ProcedureSymbol) saveIntVariable;
		
		Symbol saveUIntVariable = _ps.getScope().lookup("SimulationManager_saveUIntVariable");
		if(saveUIntVariable == null || !(saveUIntVariable instanceof ProcedureSymbol))
			throw new SimulationException("Simulation manager library saveUIntVariable function not found in the procedure set");
		_saveUIntVariableProcSymbol = (ProcedureSymbol) saveUIntVariable;	
		
		Symbol saveDoubleArrayVariable = _ps.getScope().lookup("SimulationManager_saveDoubleArrayVariable");
		if(saveDoubleArrayVariable == null || !(saveDoubleArrayVariable instanceof ProcedureSymbol))
			throw new SimulationException("Simulation manager library saveDoubleArrayVariable function not found in the procedure set");
		_saveDoubleArrayVariableProcSymbol = (ProcedureSymbol) saveDoubleArrayVariable;
		
		Symbol saveFloatArrayVariable = _ps.getScope().lookup("SimulationManager_saveFloatArrayVariable");
		if(saveFloatArrayVariable == null || !(saveFloatArrayVariable instanceof ProcedureSymbol))
			throw new SimulationException("Simulation manager library saveFloatArrayVariable function not found in the procedure set");
		_saveFloatArrayVariableProcSymbol = (ProcedureSymbol) saveFloatArrayVariable;
		
		Symbol saveLongArrayVariable = _ps.getScope().lookup("SimulationManager_saveLongArrayVariable");
		if(saveLongArrayVariable == null || !(saveLongArrayVariable instanceof ProcedureSymbol))
			throw new SimulationException("Simulation manager library saveLongArrayVariable function not found in the procedure set");
		_saveLongArrayVariableProcSymbol = (ProcedureSymbol) saveLongArrayVariable;
		
		Symbol saveIntArrayVariable = _ps.getScope().lookup("SimulationManager_saveIntArrayVariable");
		if(saveIntArrayVariable == null || !(saveIntArrayVariable instanceof ProcedureSymbol))
			throw new SimulationException("Simulation manager library saveIntArrayVariable function not found in the procedure set");
		_saveIntArrayVariableProcSymbol = (ProcedureSymbol) saveIntArrayVariable;
		
		Symbol saveUIntArrayVariable = _ps.getScope().lookup("SimulationManager_saveUIntArrayVariable");
		if(saveUIntArrayVariable == null || !(saveUIntArrayVariable instanceof ProcedureSymbol))
			throw new SimulationException("Simulation manager library saveUIntArrayVariable function not found in the procedure set");
		_saveUIntArrayVariableProcSymbol = (ProcedureSymbol) saveUIntArrayVariable;
	}
	
	public void instrument() {
		_BlockInstrumentationSwitch blockSwitch = new _BlockInstrumentationSwitch();		
		for(Procedure p : _ps.listProcedures()){
			blockSwitch.doSwitch(p.getBody());
		}	
	}
	
	private final void _assert(boolean b, String msg) {
		if(!b){
			 RuntimeException e = new RuntimeException(msg);
			_logger.error(e.getMessage(), e);
			throw e;
		}
	}
	
	/**
	 * scan the block hierarchy to and check in each new context if we need to add a save instruction.
	 * For each output symbol in a context, a save instruction is add before each return instruction or/and end of the context
	 *
	 */
	private class _BlockInstrumentationSwitch extends BasicBlockSwitch<Instruction>{
		@Override
		public Instruction caseCompositeBlock(CompositeBlock cb) {
			Block lastBlock = null;
			Instruction ret = null;
			for(Block b : cb.getChildren()){
				ret = doSwitch(b);
				lastBlock = b;
			}
			
			if(ret != null){ // if we have a retInstruction
				addSaveInstruction(ret);
			}
			// If the last block of the composite block is a basic block, we just add the save instruction in it
			else if(lastBlock != null && lastBlock instanceof BasicBlock){ 
				addSaveInstruction((BasicBlock)lastBlock);
			}
			// If the last block of the composite block is non basic block, we create a new basic block where add save instruction
			else if(lastBlock != null){
				BasicBlock newBB = GecosUserBlockFactory.BBlock();
				cb.addChildren(newBB);
				addSaveInstruction(newBB);
			}
			else{
				RuntimeException e = new RuntimeException("Impossible case during the instrumentation of the backend simulator");
				_logger.error(e.getMessage(), e);
				throw e;
			}
			return null;
		}

		@Override
		public Instruction caseBasicBlock(BasicBlock b) {
			for(Instruction inst : b.getInstructions()){
				if(inst instanceof RetInstruction)
					return inst;
			}
			return null;
		}
	}	
	
	/**
	 * Check if is the current scope of the basic block, we find a local symbol with the output pragma. If yes we create and add the save instruction.
	 * @param ret
	 */
	private void addSaveInstruction(Instruction ret){
		Block save;
		
		List<Symbol> symb;
		// we are in the main composite block and the symbol can be are contained in upper scope (procedure parameters for example)
		if(ret.getBasicBlock().getContainingProcedure().getBody() == ret.getBasicBlock().eContainer().eContainer()){
			symb = ret.getBasicBlock().getScope().lookupAllSymbols();
		}
		else{
			symb = ret.getBasicBlock().getScope().getSymbols();
		}
		
		for(Symbol s : symb){
			if(IDFixUtils.haveOutputPragma(s)){
				save = generateSaveBlockForSymbol(s, ret.getBasicBlock().getScope());
				if(save != null){
					if(save instanceof BasicBlock){
						for(int i = 0 ; i < ((BasicBlock) save).getInstructions().size() ; i++)
							ret.getBasicBlock().insertInstructionBefore(((BasicBlock) save).getInstructions().get(i), ret);
					}
					else if (save instanceof CompositeBlock){
						CompositeBlock cb = insertBeforeBlockInBasicBlock(ret.getBasicBlock(), ret, save);
						CompositeBlock parent = ((CompositeBlock)ret.getBasicBlock().eContainer());
						parent.replace(ret.getBasicBlock(), cb);
						parent.simplifyBlock();
					}
					else{
						RuntimeException e = new RuntimeException("Wrong kind of block expected during the instrumentation for the backend simulation");
						_logger.error(e.getMessage(), e);
						throw e;
					}
				}
			}
		}
	}
	
	/**
	 * Check if is the current scope of the basic block, we find a local symbol with the output pragma. If yes we create and add the save instruction.
	 * @param bb
	 */
	private void addSaveInstruction(BasicBlock bb){
		Block save;
		
		List<Symbol> symb;
		// we are in the main composite block and the symbol can be are contained in upper scope (procedure parameters for example)
		if(bb.getContainingProcedure().getBody() == bb.eContainer().eContainer()){
			symb = bb.getScope().lookupAllSymbols();
		}
		else{
			symb = bb.getScope().getSymbols();
		}
		
		for(Symbol s : symb){
			if(IDFixUtils.haveOutputPragma(s)){
				save = generateSaveBlockForSymbol(s, bb.getScope());
				if(save != null){
					if(save instanceof BasicBlock){
						for(int i = 0 ; i < ((BasicBlock) save).getInstructions().size() ; i++)
							bb.addInstruction(((BasicBlock) save).getInstructions().get(i));
					}
					else if (save instanceof CompositeBlock){
						bb.addBlockAfter(save, bb); 
					}
					else{
						RuntimeException e = new RuntimeException("Wrong kind of block expected during the instrumentation for the backend simulation");
						_logger.error(e.getMessage(), e);
						throw e;
					}
				}
			}
		}
	}
	
	/**
	 * Build save instructions. 
	 */
	private Block generateSaveBlockForSymbol(Symbol s, Scope scope){
		_manager = s.getContainingScope().lookup("manager");
		_assert(_manager != null, "No simulation manager found to build the save function of the output variable " + s);
		TypeAnalyzer typeAnalyzer = new TypeAnalyzer(s.getType());
		_assert(!typeAnalyzer.getBaseLevel().isFixedPoint(), "Need to instrument simulation code on a floating code");
		
		Block ret;
		if(typeAnalyzer.isArray()){
			ret = generateSaveBlockForArrayType(typeAnalyzer, s, scope);
		}
		else if(typeAnalyzer.isBase() && typeAnalyzer.getBaseLevel().isBaseType()){
			ret = generateSaveBlockForBaseType(typeAnalyzer, s, scope);
		}
		else{
			throw new RuntimeException("Type not managed in the IDFix simulator : " + s.getType());
		}

		return ret;
	}

	//TODO Result manager is compatible with double value. Need to manage float, long, and int
	private Block generateSaveBlockForArrayType(TypeAnalyzer typeAnalizer, Symbol s, Scope scope){
		ArrayLevel arrLevel = typeAnalizer.tryGetArrayLevel(0);
		_assert(arrLevel != null, "Type analyzer type construction problem : " + typeAnalizer);
		
		// multi for block creation for each array dimension
		CompositeBlock cb = GecosUserBlockFactory.CompositeBlock();
		GecosUserTypeFactory.setScope(cb.getScope());
		BasicBlock blockCall = GecosUserBlockFactory.BBlock(); // Block a where add call instruction 
		Block currentBlock = blockCall;
		LinkedList<Symbol> indexes = new LinkedList<>();
		for(int i = 0 ; i < arrLevel.getNDims() ; i++){
			Symbol index = GecosUserCoreFactory.symbol("i", GecosUserTypeFactory.INT(), cb.getScope());
			cb.getScope().makeUnique(index);
			indexes.addFirst(index);
			BasicBlock init = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(index), GecosUserInstructionFactory.Int(0)));
			BasicBlock test = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.lt(GecosUserInstructionFactory.symbref(index), arrLevel.getSizes().get(i)));
			BasicBlock step = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(index), GecosUserInstructionFactory.add(
					GecosUserInstructionFactory.symbref(index), GecosUserInstructionFactory.Int(1))));
			
			currentBlock = GecosUserBlockFactory.For(init, test, step, currentBlock);
		}
		cb.addChildren(currentBlock);
		
	
		// Array instruction building
		List<Instruction> indexInst = new ArrayList<>(); 
		for(Symbol symbol : indexes){
			indexInst.add(GecosUserInstructionFactory.symbref(symbol));
		}
		Instruction arrInstSave = GecosUserInstructionFactory.array(s, indexInst);
		
		// Index instruction building
		Instruction unrolledArrayIndex = GecosUserInstructionFactory.symbref(indexes.get(indexes.size()-1));
		for(int i = 0 ; i < indexes.size() - 1 ; i++){
			Instruction temp = GecosUserInstructionFactory.symbref(indexes.get(i));
			for(int j = 0  ; j < arrLevel.getNDims() - (i+1) ; j++){
				temp = GecosUserInstructionFactory.mul(temp, arrLevel.getSizes().get(j).copy());
			}
			unrolledArrayIndex = GecosUserInstructionFactory.add(unrolledArrayIndex, temp);
		}
		
		BaseLevel baseLevel = typeAnalizer.getBaseLevel();
		ProcedureSymbol procedureRequired;
		if(baseLevel.isFloat()){
			procedureRequired = _saveDoubleArrayVariableProcSymbol;
		}
		else if(baseLevel.isInt()){
			procedureRequired = _saveLongArrayVariableProcSymbol;
		}
		else{
			throw new RuntimeException("Type not managed in the IDFix simulator : " + s.getType());
		}
		
		Instruction call = GecosUserInstructionFactory.call(procedureRequired,
				GecosUserInstructionFactory.symbref(_manager),
				GecosUserInstructionFactory.Int(_profiling_number),
				unrolledArrayIndex,
				GecosUserInstructionFactory.string(s.getName()),
				arrInstSave
				);
		
		_profiling_number++;
		blockCall.addInstruction(call);
		return cb;
	}
	
	//TODO Result manager is compatible with double value. Need to manage float, long, and int
	private Block generateSaveBlockForBaseType(TypeAnalyzer typeAnalizer, Symbol s, Scope scope){
		Instruction call;
		BaseLevel baseLevel = typeAnalizer.getBaseLevel();
		ProcedureSymbol procedureRequired;
		if(baseLevel.isFloat()){
			procedureRequired = _saveDoubleVariableProcSymbol;
		}
		else if(baseLevel.isInt()){
			procedureRequired = _saveLongVariableProcSymbol;
		}
		else{
			throw new RuntimeException("Type not managed in the IDFix simulator : " + s.getType());
		}
	
		call = GecosUserInstructionFactory.call(procedureRequired,
				GecosUserInstructionFactory.symbref(_manager),
				GecosUserInstructionFactory.Int(_profiling_number),
				GecosUserInstructionFactory.string(s.getName()),
				GecosUserInstructionFactory.symbref(s)
				);
		
		return GecosUserBlockFactory.BBlock(call);
	}	
	
	
	private CompositeBlock insertBeforeBlockInBasicBlock(BasicBlock bb, Instruction pos, Block b){
		if(b instanceof BasicBlock){
			for(Instruction inst : ((BasicBlock) b).getInstructions()){
				bb.insertInstructionBefore(inst, pos);
			}
			return null;
		}
		else{
			CompositeBlock cb = GecosUserBlockFactory.CompositeBlock();
			BasicBlock newbb = GecosUserBlockFactory.BBlock();
			for(int i = 0 ; i < bb.getInstructionCount() ; i++){
				if(bb.getInstruction(i) != pos)
					newbb.addInstruction(bb.getInstruction(i).copy());
				else
					break;
			}
			
			cb.addChildren(newbb);
			cb.addChildren(b);
			cb.addChildren(GecosUserBlockFactory.BBlock(pos.copy()));
			return cb;
		}
	}
}
