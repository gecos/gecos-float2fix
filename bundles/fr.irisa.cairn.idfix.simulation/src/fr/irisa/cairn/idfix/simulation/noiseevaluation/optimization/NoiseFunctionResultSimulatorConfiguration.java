package fr.irisa.cairn.idfix.simulation.noiseevaluation.optimization;

/**
 * Class which contains simulator options
 * @author Nicolas Simon
 * @date Apr 12, 2013
 */
public class NoiseFunctionResultSimulatorConfiguration {
	private int _nbSimu;
	private boolean _eachCallNoiseFunction;
	private float _superiorLimit;
	private int _freq;
	
	public NoiseFunctionResultSimulatorConfiguration(int nbSimu){
		_nbSimu = nbSimu;
		_eachCallNoiseFunction = false;
		_superiorLimit = 0;
		_freq = 1;
	}
	
	public NoiseFunctionResultSimulatorConfiguration(int nbSimu, boolean eachCallNoiseFunction, float superiorLimit, int freq){
		_nbSimu = nbSimu;
		_eachCallNoiseFunction = eachCallNoiseFunction;
		_superiorLimit = superiorLimit;
		_freq = freq;
	}
	
	public int getNbSimu(){
		return _nbSimu;
	}
	
	public boolean isEachCallNoiseFunction(){
		return _eachCallNoiseFunction;
	}
	
	public float getSuperiorLimit(){
		return _superiorLimit;
	}
	
	public int getFreq(){
		return _freq;
	}
}
