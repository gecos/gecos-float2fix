package fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.result;

import java.io.PrintWriter;

import org.apache.logging.log4j.Logger;

import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;

public interface IResultManager {
	public double process() throws SimulationException;
	public void display(String title, Logger logger);
	public void display(String title, PrintWriter writer);
}
