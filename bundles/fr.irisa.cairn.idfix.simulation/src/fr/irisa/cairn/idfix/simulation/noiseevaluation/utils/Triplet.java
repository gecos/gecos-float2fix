package fr.irisa.cairn.idfix.simulation.noiseevaluation.utils;

/**
 * Class générique permettant de représenter un Triplet
 * 
 * @author nicolas simon
 *
 * @param <T>
 */
public class Triplet<T,U,V> {
	private T _first;
	private U _second;
	private V _third;
	
	public Triplet(){};
	
	public Triplet(T first, U second, V third){
		_first = first;
		_second = second;
		_third = third;
	}
	
	public T getFirst(){
		return _first;
	}
	
	public U getSecond(){
		return _second;
	}
	
	public V getThird(){
		return _third;
	}
	
	public void setFirst(T first){
		_first = first;
	}
	
	public void setSecond(U second){
		_second = second;
	}
	
	public void setThird(V third){
		_third = third;
	}
	
	public void setTriplet(T first, U second, V third){
		_first = first;
		_second = second;
		_third = third;
	}
	
	public String toString(){
		return "{ " + _first + "," + _second + "," + _third +" }";
	}
	
}
