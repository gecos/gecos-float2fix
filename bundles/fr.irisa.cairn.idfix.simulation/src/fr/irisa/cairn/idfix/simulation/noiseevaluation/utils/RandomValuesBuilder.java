package fr.irisa.cairn.idfix.simulation.noiseevaluation.utils;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import fr.irisa.cairn.gecos.model.analysis.types.ArrayLevel;
import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.idfix.utils.IDFixUtils;
import fr.irisa.cairn.idfix.utils.exceptions.SFGModelCreationException;
import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.Symbol;

/**
 * Check if the parameter between the fixed and float part have the same name and the same type.
 * Generate random values used for the simulation from the dynamic pragma of each input of the system.
 * 
 * For a scalar variable, we generate N sample in a binary file.
 * For a array variable, we generate N*array_size sample in a binary file.
 * The name of the binary file is the name of the input variable
 * @author nsimon
 * @date 24/02/14
 *
 */
public class RandomValuesBuilder {	
	private static final void _assert(boolean b, String msg) throws SimulationException 
	{
		if(!b) throw new SimulationException(msg);
	}
	
	/**
	 * Generate random values for each input (with DYNAMIC pragma). This values are save in a binary file for each system input. 
	 * - For an scalar variable, we generate N sample in a binary file.
	 * - For an array variable, we generate N*array_size sample in a binary file.
	 * @throws SimulationException
	 */
	public static void generate(ProcedureSet psFloat, ProcedureSet psFixed, int nbSimulation, String outputFolderPath) throws SimulationException{
		Procedure pWithMainPragmaFloat = IDFixUtils.getMainProcedure(psFloat);
		if(pWithMainPragmaFloat == null)
			throw new SimulationException("No procedure with \"MAIN_FUNC\" pragma found in the float procedure set");
		Procedure pWithMainPragmaFixed = IDFixUtils.getMainProcedure(psFixed);
		if(pWithMainPragmaFixed == null)
			throw new SimulationException("No procedure with \"MAIN_FUNC\" pragma found in the fixed procedure set");
		
		if(pWithMainPragmaFixed.listParameters().size() != pWithMainPragmaFloat.listParameters().size())
			throw new SimulationException("The number of parameters of the pragmated main function differed between the float and fixed simulation source code");
		
		int numberOfParameters = pWithMainPragmaFixed.listParameters().size();
		for(int i = 0 ; i < numberOfParameters ; i++){
			if(!pWithMainPragmaFixed.listParameters().get(i).getName().equals(pWithMainPragmaFloat.listParameters().get(i).getName()))
				throw new SimulationException("Fixed input and float input at the position " + i + " in the simulation code have not the same name!");
		}
		
		try{
			for(Symbol s : pWithMainPragmaFloat.listParameters()){
				if(IDFixUtils.isInputVar(s)){
					buildRandomValues(s, nbSimulation, outputFolderPath);					
				}
			}
		} catch (SFGModelCreationException e){
			throw new SimulationException(e);
		}
	}
	
	/**
	 * Return the number element to generate to a given type
	 * In this number of elements, we add the number of sample.
	 * Example (10 sample) : 
	 * 	- Scalar x -> return 10
	 *  - Array x[5] -> return 50
	 *  - Array x[5][2] -> return 100
	 * @param analyzer
	 * @return number sample value needed to generate
	 * @throws SimulationException
	 */
	private static int getNumberElementToGenerate(TypeAnalyzer analyzer, int numberOfSimulation) throws SimulationException{
		int numberOfElement;
		if(analyzer.isBase() && analyzer.getBaseLevel().isBaseType()){
			numberOfElement = numberOfSimulation;
		}
		else if(analyzer.isArray()){
			ArrayLevel arrLevel = analyzer.tryGetArrayLevel(0);
			_assert(arrLevel != null, "Error in the analyze of a type by the TypeAnalyser");
			
			numberOfElement = numberOfSimulation;
			for(int i = 0 ; i < arrLevel.getNDims() ; i++){
				Long value = arrLevel.tryGetSize(i);
				_assert(value != -1, "Simulation backend only manage integer dimension array size (not generic instruction for example)");
				numberOfElement *= value.intValue();
			}
		}
		else{
			throw new SimulationException("Pointer as system input are not managed in the IDFix backend simulator");
		}
		
		return numberOfElement;
	}
	
	/**
	 * Create a binary file where we save generated sample value for the symbol given as parameter 
	 * @param s
	 * @throws SimulationException
	 */
	private static void buildRandomValues(Symbol s, int numberOfSimulation, String outputFolderPath) throws SimulationException{
		TypeAnalyzer typeAnalyzer = new TypeAnalyzer(s.getType());
		int numberOfElementToGenerate = getNumberElementToGenerate(typeAnalyzer, numberOfSimulation);
		File file = new File(outputFolderPath + s.getName() + "_values.bin");
		try {
			DataOutputStream outstream = new DataOutputStream(new FileOutputStream(file));
			ByteBuffer buffer;
			if(typeAnalyzer.getBaseLevel().isFloatSingle()) {
				buffer = ByteBuffer.allocate(numberOfElementToGenerate * Float.SIZE/Byte.SIZE).order(ByteOrder.nativeOrder());
				float valmin = IDFixUtils.getVarMin(s).floatValue();
				float valmax = IDFixUtils.getVarMax(s).floatValue();
				for(int i = 0 ; i < numberOfElementToGenerate ; i++){
					float temp = IDFixUtils.generateRandomFloatValue(valmin, valmax);
					buffer.putFloat(temp);
				}
			}
			else if(typeAnalyzer.getBaseLevel().isFloatDouble()){
				buffer = ByteBuffer.allocate(numberOfElementToGenerate * Double.SIZE/Byte.SIZE).order(ByteOrder.nativeOrder());
				double valmin = IDFixUtils.getVarMin(s);
				double valmax = IDFixUtils.getVarMax(s);
				for(int i = 0 ; i < numberOfElementToGenerate ; i++){
					buffer.putDouble(IDFixUtils.generateRandomDoubleValue(valmin, valmax));
				}
			}
			else if(typeAnalyzer.getBaseLevel().isInt32()){
				buffer = ByteBuffer.allocate(numberOfElementToGenerate * Integer.SIZE/Byte.SIZE).order(ByteOrder.nativeOrder());
				int valmin = IDFixUtils.getVarMin(s).intValue();
				int valmax = IDFixUtils.getVarMax(s).intValue();
				for(int i = 0 ; i < numberOfElementToGenerate ; i++){
					buffer.putInt(IDFixUtils.generateRandomIntValue(valmin, valmax));
				}
			}
			else if(typeAnalyzer.getBaseLevel().isInt64()){
				buffer = ByteBuffer.allocate(numberOfElementToGenerate * Long.SIZE/Byte.SIZE).order(ByteOrder.nativeOrder());
				long valmin = IDFixUtils.getVarMin(s).longValue();
				long valmax = IDFixUtils.getVarMax(s).longValue();
				for(int i = 0 ; i < numberOfElementToGenerate ; i++){
					buffer.putLong(IDFixUtils.generateRandomLongValue(valmin, valmax));
				}
			}
			else{
				outstream.close();
				throw new SimulationException("Type not managed in the input sample generation in the backend simulator : " + typeAnalyzer);
			}
			outstream.write(buffer.array());
			outstream.close();
		} catch (IOException e) {
			throw new SimulationException(e);
		}
	}
}