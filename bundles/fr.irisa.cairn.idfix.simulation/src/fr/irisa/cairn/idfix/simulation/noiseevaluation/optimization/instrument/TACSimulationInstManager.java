package fr.irisa.cairn.idfix.simulation.noiseevaluation.optimization.instrument;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.tools.switches.DefaultInstructionSwitch;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.Triplet;
import fr.irisa.cairn.idfix.utils.IDFixUtils;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.CallInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.RetInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.SymbolInstruction;

public class TACSimulationInstManager  extends DefaultInstructionSwitch<Object>{
	private TACSimulationProcManager _procManager;
	private List<SetInstruction> _listInst;
	private Map<Instruction, List<Instruction>> _map;
	
	private final int LHS = 1;
	private final int RHS = 2;
	private int _mode;
	
	public TACSimulationInstManager(TACSimulationProcManager procManager){
		_procManager = procManager;
		_listInst = new LinkedList<SetInstruction>();
		_map = new HashMap<Instruction, List<Instruction>>();
	}
	
	private void myassert(boolean b, String s){
		if (!b){
			throw new RuntimeException(s);
		}
	}
	
	public Map<Instruction, List<Instruction>> getMap(){
		return _map;
	}
	
	private SetInstruction createNewSimpleSetInstruction(Instruction inst){
		// Création du nom temporaire de la variable
		// Une vérification est faites pour choisir un nom de symbol non utilisé dans le scope courant
		Scope scopeCurrent = inst.getBasicBlock().getScope();
		StringBuilder sBuilder = new StringBuilder();
		for(Symbol symb : scopeCurrent.lookupAllSymbols()){
			sBuilder.append(symb.getName());
		}
		Pattern pattern;
		Matcher matcher;
		
		String nameSymbol = inst.getBasicBlock().getContainingProcedure().getSymbol().getName();
		do{
			nameSymbol += "_temp";
			pattern = Pattern.compile(nameSymbol+"\\d+");
			matcher = pattern.matcher(sBuilder);
		}while(matcher.find());
		int temp = _procManager.useNumeroTmpVariable();
		nameSymbol += temp;
		
		SetInstruction res = null;
		Symbol destSymbol = GecosUserCoreFactory.symbol(nameSymbol, inst.getType(), _procManager.getBlockManager().getNewScope());
		SymbolInstruction dest = GecosUserInstructionFactory.symbref(destSymbol);
		res = GecosUserInstructionFactory.set(dest, inst.copy());
		
		return res;
	}
	
	private void saveOperatorToNewTemporarySymbol(GenericInstruction g, SetInstruction res){
		SymbolInstruction tempSymbInst1 = (SymbolInstruction) res.getDest();
		SymbolInstruction tempSymbInst2 = (SymbolInstruction) ((GenericInstruction) res.getSource()).getOperand(0);
		SymbolInstruction tempSymbInst3 = null;
		if(((GenericInstruction) res.getSource()).getOperands().size() > 1){
			tempSymbInst3 = (SymbolInstruction) ((GenericInstruction) res.getSource()).getOperand(1);
		}
		Integer annotation = IDFixUtils.getCDFGInstructionNumber(g);
		myassert(annotation != null, "Annotation on instruction is missing. CDFG operator required");
		
		if(_procManager.getMapGenericOperatorToNewTemporarySymbol().get(annotation) == null){
			List<Triplet<Symbol,Symbol,Symbol>> temp = new ArrayList<Triplet<Symbol,Symbol,Symbol>>();
			if(tempSymbInst3 == null){
				temp.add(new Triplet<Symbol, Symbol, Symbol>(tempSymbInst1.getSymbol(), tempSymbInst2.getSymbol(), null));
			} else {
				temp.add(new Triplet<Symbol, Symbol, Symbol>(tempSymbInst1.getSymbol(), tempSymbInst2.getSymbol(), tempSymbInst3.getSymbol()));
			}
			_procManager.getMapGenericOperatorToNewTemporarySymbol().put(annotation, temp);
		}
		else{
			List<Triplet<Symbol,Symbol,Symbol>> temp = _procManager.getMapGenericOperatorToNewTemporarySymbol().get(annotation);
			if(tempSymbInst3 == null){
				temp.add(new Triplet<Symbol, Symbol, Symbol>(tempSymbInst1.getSymbol(), tempSymbInst2.getSymbol(), null));
			} else {
				temp.add(new Triplet<Symbol, Symbol, Symbol>(tempSymbInst1.getSymbol(), tempSymbInst2.getSymbol(), tempSymbInst3.getSymbol()));
			}
		}
	}
	
	private void saveOperatorToNewTemporarySymbol(SetInstruction g){
		SymbolInstruction tempSymbolInst = (SymbolInstruction) g.getSource();
		Integer annotation = IDFixUtils.getCDFGInstructionNumber(g);
		myassert(annotation != null, "Annotation on instruction is missing. CDFG operator required");
		if(_procManager.getMapSetOperatorToNewTemporarySymbol().get(annotation) == null){
			List<Symbol> temp = new ArrayList<Symbol>();
			temp.add(tempSymbolInst.getSymbol());
			_procManager.getMapSetOperatorToNewTemporarySymbol().put(annotation, temp);
		}
		else{
			List<Symbol> temp = _procManager.getMapSetOperatorToNewTemporarySymbol().get(annotation);
			temp.add(tempSymbolInst.getSymbol());
		}
	}
	
	@Override
	public Object caseSetInstruction(SetInstruction g){
		_mode = RHS;
		Object o = doSwitch(g.getSource());
		if(o instanceof SetInstruction){
			SetInstruction setInst = (SetInstruction) o;
			_listInst.add(setInst);
			g.setSource(setInst.getDest().copy());
			if(IDFixUtils.getCDFGInstructionNumber(g) != null){
				this.saveOperatorToNewTemporarySymbol(g);
			}
		}
		
		_map.put(g, new ArrayList<Instruction>(_listInst));
		_listInst.clear();
		_mode = LHS;
		return null;
	}
	
	@Override
	public Object caseGenericInstruction(GenericInstruction g){
		for(Instruction inst : g.getOperands()){
			Object o = doSwitch(inst);
			if(o instanceof SetInstruction){
				_listInst.add((SetInstruction) o);
				g.getOperands().set(g.getOperands().indexOf(inst), _listInst.get(_listInst.size()-1).getDest().copy());
			}
		}
		SetInstruction res = this.createNewSimpleSetInstruction(g);
		//myassert(g.getChildrenCount() == 2 && g.getOperand(0) instanceof SymbolInstruction && g.getOperand(1) instanceof SymbolInstruction , "Instruction build problem during the TACSimulationForm");
		myassert(res.getDest() instanceof SymbolInstruction, "The dest of the setInstruction is not a symbolInstruction");
		if(IDFixUtils.getCDFGInstructionNumber(g) != null){	
			this.saveOperatorToNewTemporarySymbol(g, res);
		}
		
		return res;
	}
	
	@Override
	public Object caseIntInstruction(IntInstruction g){
		return this.createNewSimpleSetInstruction(g);
	}
	
	@Override
	public Object caseFloatInstruction(FloatInstruction g){
		return this.createNewSimpleSetInstruction(g);
	}
	
	@Override
	public Object caseSymbolInstruction(SymbolInstruction g){
		return this.createNewSimpleSetInstruction(g);
	}
	
	@Override
	public Object caseSimpleArrayInstruction(SimpleArrayInstruction g){
		return this.createNewSimpleSetInstruction(g);
	}

	@Override
	public Object caseCallInstruction(CallInstruction g){
		if(_mode == RHS){
			for(Instruction inst : g.getArgs()){
				Object o = doSwitch(inst);
				if(o instanceof SetInstruction){
					SetInstruction setInst = (SetInstruction) o;
					_listInst.add(setInst);
//					g.getChildren().set(g.getChildren().indexOf(inst), setInst.getDest().copy());
					g.replaceChild(inst, setInst.getDest().copy());
				}
				
			}
			return this.createNewSimpleSetInstruction(g);
		}
		else{
			return null;
		}
	}
	
	@Override
	public Object caseConvertInstruction(ConvertInstruction g){
		doSwitch(g.getExpr());
		return this.createNewSimpleSetInstruction(g);
	}
	
	@Override
	public Object caseRetInstruction(RetInstruction g){
		if(!(g.getExpr() instanceof FloatInstruction) && !(g.getExpr() instanceof IntInstruction) && !(g.getExpr() instanceof SimpleArrayInstruction) && !(g.getExpr() instanceof SymbolInstruction)){
			throw new RuntimeException("The output of the system is not a simple instruction");
		}
		return null;
	}
	
}
