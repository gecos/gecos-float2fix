package fr.irisa.cairn.idfix.simulation.noiseevaluation.optimization;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import fr.irisa.cairn.gecos.model.c.generator.XtendCGenerator;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.transforms.tac.ThreeAddressCodeComputation;
import fr.irisa.cairn.idfix.model.factory.IDFixUserInformationAndTimingFactory;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixProject;
import fr.irisa.cairn.idfix.model.informationandtiming.Section;
import fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_OUTPUT_MODE;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.instrument.InputTransformationManager;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.instrument.InstructionInstrumentationManager;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.instrument.MainProcedureCreationManager;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.instrument.TypeFixer;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.optimization.instrument.ChangeTempVariableTypeToSCFixedManager;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.optimization.instrument.TACSimulationProcManager;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.CompilationManager;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.CompilationManager.Version;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.RandomValuesBuilder;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.RunManager;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.Triplet;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.result.IResultManager;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.result.MeanVersion;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.result.WorstVersion;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;
import fr.irisa.cairn.idfix.utils.logging.IDFixLogger;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Symbol;
import gecos.gecosproject.GecosHeaderDirectory;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;
import gecos.types.AliasType;
import gecos.types.IntegerType;
import gecos.types.PtrType;
import gecos.types.Type;

/**
 * Implement the noise function result simulator.
 * This simulator use the SimulationManager to save informations during the simulation process.
 * Step of the simulator :
 *  1 - Transform C Float and Fixed procedure set.
 *  	1 - Detect the system inputs (dynamic pragma) and get back these variables as main function parameters.
 *  	2 - Instrument the source code. Add the save function call to get back the needed information during the simulation process.
 *  	3 - Create the "main" function and the input variable initialization function.
 *  2 - Generate random value file used by the float and fixed simulation process.
 *  3 - Fix array type (static to dynamic)
 * 	4 - Generate the new source code and process the simulation
 * 	5 - Get back and handle float/fixed result.
 * @author nsimon
 * @date 11/04/2014
 */
public class NoiseFunctionResultSimulator implements INoiseFunctionResultSimulator {
	private final boolean _debug;
	
	private IdfixProject _idfixProject;
	private ProcedureSet _psReference, _psFloat, _psFixed;
	private String _outputFolder, _floatFolder, _fixedFolder;
	
	private Map<Integer, List<Triplet<Symbol, Symbol, Symbol>>> _mapGenericOpToNewTempSymbol;
	private Map<Integer, List<Symbol>> _mapSetOpToNewTempSymbol;
	private ChangeTempVariableTypeToSCFixedManager _changeTempVariable;
	
	private int _nbSimulationSample;
	private int _frequencyOfSimulation;
	private float _superiorLimit;
	
	private RunManager _runManager;
	private IResultManager _resultManager;
	private boolean _lastSimulationSuccessful;
	
	// Use to log information if there are more than one simulation asked by the user
	private int _nbSuperiorThanLimit;
	private List<Double> _listDiffAnalSimu;
	private PrintWriter _filePrintWriter;
	private Section _sectionTimeLog;
	private float _totalTimeOfFixedCompilation;
	private float _totalTimeOfFixedProcess;
	
	private final Logger _logger = IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_OPTIMIZATION);

	public NoiseFunctionResultSimulator(IdfixProject idfixProject){
		_idfixProject = idfixProject;
		
		if(_idfixProject.getGecosProject().getSources().size() != 1)
			throw new RuntimeException("IDFix FrontEnd is only compatible to work with just one procedure set");
		_psReference = _idfixProject.getGecosProject().getSources().get(0).getModel();
		
		_psFloat = null;
		_psFixed = null;
	
		_outputFolder = _idfixProject.getIDFixConvOutputFolderPath().endsWith("/")?_idfixProject.getIDFixConvOutputFolderPath()+ConstantPathAndName.VERIF_FOLDER:_idfixProject.getIDFixConvOutputFolderPath()+"/"+ConstantPathAndName.VERIF_FOLDER;
		
		_floatFolder = _outputFolder + ConstantPathAndName.OPTIM_SIMU_FLOAT_FOLDER_NAME;
		_fixedFolder = _outputFolder+ ConstantPathAndName.OPTIM_SIMU_FIXED_FOLDER_NAME;
		
		_sectionTimeLog = IDFixUserInformationAndTimingFactory.SECTION("Optimization verification simulation (included in total optimization time)");
		_idfixProject.getProcessInformations().getTiming().getSections().add(_sectionTimeLog);
		
		_nbSimulationSample = _idfixProject.getUserConfiguration().getOptimizationSimulatorSampleNumber();
		_frequencyOfSimulation = _idfixProject.getUserConfiguration().getOptimizationSimulatorFrequency();
		_superiorLimit = _idfixProject.getUserConfiguration().getOptimizationSimulatorSuperiorLimit();
		
		_runManager = null;
		_lastSimulationSuccessful = true;
		_nbSuperiorThanLimit = 0;
		_listDiffAnalSimu = new ArrayList<>();
		_totalTimeOfFixedCompilation = 0;
		_totalTimeOfFixedProcess = 0;
		
		_debug = _idfixProject.getUserConfiguration().isDebugMode();
	}
	
	private static final void _assert(boolean b, String msg) throws SimulationException 
	{
		if(!b) throw new SimulationException(msg);
	}
	
	@Override
	public boolean isEachCallNoiseFunction() {
		return _frequencyOfSimulation > 0;
	}

	@Override
	public boolean lastSimulationSuccessful() {
		return _lastSimulationSuccessful;
	}
	
	@Override
	public int getSimulationFrequencies() {
		return _frequencyOfSimulation;
	}
	
	@Override
	public void initializeSimulation() {
		try{
			_logger.info("               - Result verification by simulation activated (x" + _nbSimulationSample +")");
			if(_psReference == null)
				throw new SimulationException("The reference procedure set given to the simulator is null");
			
			// Initialize float and fixed simulation directory
			createDirectory();
			// logger creation
			_filePrintWriter = new PrintWriter(_idfixProject.getOutputFolderPath() + ConstantPathAndName.LOG_FODLER_NAME + ConstantPathAndName.VERIF_LOG_FILE_NAME);
			
			_logger.info("               - Float instrumentation");
			transformFloatProcedureSet();
			_logger.info("               - Fixed instrumentation");
			transformFixedProcedureSet();

			// FIXME sample number getting
			// Generate random value used by the fixed and float simulation
			RandomValuesBuilder.generate(_psFloat, _psFixed, _idfixProject.getUserConfiguration().getOptimizationSimulatorSampleNumber(), _outputFolder);
			
			// Transform x[N][M] to x** in function prototype
			TypeFixer.fixStaticArrayCompilation(_psFloat);
			TypeFixer.fixStaticArrayCompilation(_psFixed); 
			
			// Regenerate float
			new XtendCGenerator(_psFloat, _floatFolder).compute();
		
			// Compile float source code to generate library
			_logger.info("               - Float binary compilation");
			CompilationManager.compile(getFileName(((GecosSourceFile) _psFloat.eContainer()).getName()), _floatFolder, Version.FLOAT, _logger, _idfixProject.getUserConfiguration().isDebugMode());
			
			// Run the float simulation
			_logger.info("               - Float running process");
			_runManager = new RunManager(_floatFolder, _fixedFolder);
			_runManager.runFloatLibrary();			 
		} catch (SimulationException | FileNotFoundException e){
			_logger.info("Error during the compilation of simulation libraries");
			_logger.info("            Simulation is disactivated");
			_logger.debug("Simulation during optimization failed during initialization", e);
			_lastSimulationSuccessful = false;
		}
	}
	
	@Override
	public void finalizeSimulation(int numberCallNoiseFunction) {
		try {
			if(_lastSimulationSuccessful){
				this.saveReportInformation(numberCallNoiseFunction);
				_filePrintWriter.close();
				this.saveFixedTimingLogInformation();
			}
		} catch (SimulationException e) {
			_logger.info("Error during the compilation of simulation libraries (path of systemC is add to LD_LIBRARY_PATH?)");
			_logger.info("            Simulation is disactivated");
			_lastSimulationSuccessful = false;
		}			
	}
	
	@Override
	public void runFixedSimulation(double noise){
		long timingStart, timingStop;
		if(_runManager == null)
			throw new RuntimeException("Need to initialize simulation before try to run it");
		
		try{
			//_fixedPointSpecification.updateDataFixedInformation();
			_logger.info("               - Fixed data type update");
			timingStart = System.currentTimeMillis();
			_changeTempVariable.apply();
			new XtendCGenerator(_psFixed, _fixedFolder).compute();
			_logger.info("               - Fixed binary compilation");
			CompilationManager.compile(getFileName(((GecosSourceFile) _psFixed.eContainer()).getName()), _fixedFolder, Version.FIXED, _logger, _idfixProject.getUserConfiguration().isDebugMode());
			timingStop = System.currentTimeMillis();
			_totalTimeOfFixedCompilation += (timingStop - timingStart) / 1000f;
			
			timingStart = System.currentTimeMillis();
			_logger.info("               - Fixed running process");
			_runManager.runFixedLibrary();
			if(_idfixProject.getUserConfiguration().getNoiseFunctionOutputMode() == NOISE_FUNCTION_OUTPUT_MODE.WORST)
				_resultManager = new WorstVersion(_floatFolder + ConstantPathAndName.BACKEND_SIMU_RESULT_FODLER_NAME, _fixedFolder + ConstantPathAndName.BACKEND_SIMU_RESULT_FODLER_NAME, noise, _nbSimulationSample);
			else if(_idfixProject.getUserConfiguration().getNoiseFunctionOutputMode() == NOISE_FUNCTION_OUTPUT_MODE.MEAN)
				_resultManager = new MeanVersion(_floatFolder + ConstantPathAndName.BACKEND_SIMU_RESULT_FODLER_NAME, _fixedFolder + ConstantPathAndName.BACKEND_SIMU_RESULT_FODLER_NAME, noise, _nbSimulationSample);
			else
				throw new SimulationException("Noise function output mode not managed by the backend simulator " +  _idfixProject.getUserConfiguration().getNoiseFunctionOutputMode());
			double lastSimulationNoisePower = _resultManager.process();
			
			// write current result manager information in the log
			_resultManager.display("", _filePrintWriter);
			_filePrintWriter.println();

			// save information for the final summary
			_listDiffAnalSimu.add(noise - lastSimulationNoisePower);
			
			timingStop = System.currentTimeMillis();
			_totalTimeOfFixedProcess += (timingStop - timingStart) / 1000f;
		} catch (SimulationException e){
			_logger.info("Error during the fixed simulation library computing");
			_logger.info("            Simulation is disactivated");
			_logger.debug("Simulation during optimization failed the fixed library computation", e);
			_lastSimulationSuccessful = false;
		}
	}
	
	/**
	 * Apply the transformation needed by the simulation process to the float procedure set 
	 * @throws SimulationException
	 */
	private void transformFloatProcedureSet() throws SimulationException{
		_psFloat = _psReference.copy();
		GecosProject projectfloat = GecosUserCoreFactory.project("GecosProjectFloat");
		GecosSourceFile sourceFloat = GecosUserCoreFactory.source(((GecosSourceFile) _psReference.eContainer()).getName(), _psFloat);
		projectfloat.getSources().add(sourceFloat);
		GecosHeaderDirectory headerdirectory = GecosUserCoreFactory.includeDir(ConstantPathAndName.SIMU_MANAGER_FOLDER_PATH + ConstantPathAndName.SIMU_MANAGER_BIN_FOLDER_NAME
				+ ConstantPathAndName.SIMU_MANAGER_INCLUDE_FOLDER_NAME);
		projectfloat.getIncludes().add(headerdirectory);
		GecosUserAnnotationFactory.pragma(_psFloat, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION+"#include \"csimulationManager.h\"");
		addDummyTypeAndSymbol(_psFloat);
		
		Type simulationManager = _psFloat.getScope().lookupAliasType("CSimulationManager");
		_assert(simulationManager != null, "Simulation manager library scalar type not found in the procedure set");
		
		PtrType ptrSimulationManager = GecosUserTypeFactory.PTR(simulationManager);
		Symbol paramManager = GecosUserCoreFactory.symbol("manager", ptrSimulationManager);
		_psFloat.getScope().getSymbols().add(paramManager);
	
		// All system input are pulled up as parameter their respective function, and the call of the function are updated
		new InputTransformationManager(_psFloat).upInputToMainProcedureParameters();
		if(_debug)
			new XtendCGenerator(_psFloat, _floatFolder + "inputManager/").compute();
		
		// Add save call function to save all the output value
		new InstructionInstrumentationManager(_psFloat).instrument();
		if(_debug)
			new XtendCGenerator(_psFloat, _floatFolder + "instrumentationManager/").compute();
		
		// Generate main procedure and the input/ouput initialization function
		new MainProcedureCreationManager(_psFloat, _nbSimulationSample, Version.FLOAT, _idfixProject.getUserConfiguration().isDebugMode()).generate();
		if(_debug)
			new XtendCGenerator(_psFloat, _floatFolder + "mainCreationManager/").compute();
	}
	
	/**
	 * Apply the transformation needed by the simulation process to the float procedure set
	 * @throws SimulationException
	 */
	private void transformFixedProcedureSet() throws SimulationException{
		_psFixed = _psReference.copy();
		GecosProject projectfixed = GecosUserCoreFactory.project("GecosProjectFixed");
		GecosSourceFile sourceFixed = GecosUserCoreFactory.source(((GecosSourceFile) _psReference.eContainer()).getName() ,_psFixed);
		projectfixed.getSources().add(sourceFixed);
		GecosHeaderDirectory headerdirectory = GecosUserCoreFactory.includeDir(ConstantPathAndName.SIMU_MANAGER_FOLDER_PATH + ConstantPathAndName.SIMU_MANAGER_BIN_FOLDER_NAME
				+ ConstantPathAndName.SIMU_MANAGER_INCLUDE_FOLDER_NAME);
		projectfixed.getIncludes().add(headerdirectory);
		
		GecosUserAnnotationFactory.pragma(_psFixed, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION+"#define SC_INCLUDE_FX");
		GecosUserAnnotationFactory.pragma(_psFixed, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION+"#include \"systemc.h\"");
		GecosUserAnnotationFactory.pragma(_psFixed, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION+"#include \"csimulationManager.h\"");
		addDummyTypeAndSymbol(_psFixed);
		
		Type simulationManager = _psFixed.getScope().lookupAliasType("CSimulationManager");
		_assert(simulationManager != null, "Simulation manager library scalar type not found in the procedure set");
		
		PtrType ptrSimulationManager = GecosUserTypeFactory.PTR(simulationManager);
		Symbol paramManager = GecosUserCoreFactory.symbol("manager", ptrSimulationManager);
		_psFixed.getScope().getSymbols().add(paramManager);
		
		new ThreeAddressCodeComputation(_psFixed).compute();
		if(_debug)
			new XtendCGenerator(_psFixed, _fixedFolder + "tacManager/").compute();
		
		TACSimulationProcManager procManager = new TACSimulationProcManager();
		for(Procedure proc : _psFixed.listProcedures())
			procManager.doSwitch(proc);
		_mapSetOpToNewTempSymbol = procManager.getMapSetOperatorToNewTemporarySymbol();
		_mapGenericOpToNewTempSymbol = procManager.getMapGenericOperatorToNewTemporarySymbol();
		if(_debug)
			new XtendCGenerator(_psFixed, _fixedFolder + "tacManagerSimu/").compute();
		
		// Need to be done before InstructionInstrumentManager
		// All system input are pulled up as parameter their respective function, and the call of the function are updated
		new InputTransformationManager(_psFixed).upInputToMainProcedureParameters();
		if(_debug)
			new XtendCGenerator(_psFixed, _fixedFolder + "inputManager/").compute();
		
		new InstructionInstrumentationManager(_psFixed).instrument();
		if(_debug)
			new XtendCGenerator(_psFixed, _fixedFolder + "instrumentationManager/").compute();
		
		new MainProcedureCreationManager(_psFixed, _nbSimulationSample, Version.PREQUANTIFIED, _idfixProject.getUserConfiguration().isDebugMode()).generate();
		if(_debug)
			new XtendCGenerator(_psFixed, _fixedFolder + "mainCreationManager/").compute();
		
		_changeTempVariable = new ChangeTempVariableTypeToSCFixedManager(_psFixed, _idfixProject.getFixedPointSpecification(), _mapSetOpToNewTempSymbol, _mapGenericOpToNewTempSymbol);
	}
	
	/**
	 * Add all the required type and symbol representing the simulation manager library
	 * @param ps
	 */
	private void addDummyTypeAndSymbol(ProcedureSet ps){
		GecosUserTypeFactory.setScope(ps.getScope());
		
		// Type creation
		Type voidType = GecosUserTypeFactory.VOID();
		AliasType csimulationManager = GecosUserTypeFactory.ALIAS(voidType, "CSimulationManager");
		IntegerType charconst = GecosUserTypeFactory.CHAR();
		charconst.setConstant(true);
		
		Type doubleType = GecosUserTypeFactory.DOUBLE();
		Type floatType = GecosUserTypeFactory.FLOAT();
		Type longType = GecosUserTypeFactory.LONG();
		Type intType = GecosUserTypeFactory.INT();
		Type uintType = GecosUserTypeFactory.UINT();
		
		// Pointer creation
		PtrType ptrmanager = GecosUserTypeFactory.PTR(csimulationManager);
		PtrType ptrcharconst = GecosUserTypeFactory.PTR(charconst);
		
		
		// Parameter Symbol creation
		ParameterSymbol manager = GecosUserCoreFactory.paramSymbol("manager", ptrmanager);
		ParameterSymbol name = GecosUserCoreFactory.paramSymbol("name", ptrcharconst);
		ParameterSymbol doubleParam = GecosUserCoreFactory.paramSymbol("value", doubleType);
		ParameterSymbol floatParam = GecosUserCoreFactory.paramSymbol("value", floatType);
		ParameterSymbol longParam = GecosUserCoreFactory.paramSymbol("value", longType);
		ParameterSymbol intParam = GecosUserCoreFactory.paramSymbol("value", intType);
		ParameterSymbol uintParam = GecosUserCoreFactory.paramSymbol("value", uintType);
		ParameterSymbol profilingNumber = GecosUserCoreFactory.paramSymbol("profiling_number", uintType);
		ParameterSymbol index = GecosUserCoreFactory.paramSymbol("index", uintType);
		
		
		// Dummy procedure set creation
		List<ParameterSymbol> params = new LinkedList<>();
		ProcedureSymbol newManager = GecosUserCoreFactory.procSymbol("newSimulationManager", ptrmanager, params);
		ps.getScope().getSymbols().add(newManager);
		ProcedureSymbol deleteManager = GecosUserCoreFactory.procSymbol("deleteSimulationManager", voidType, params);
		ps.getScope().getSymbols().add(deleteManager);
		
		params.add(manager);
		ProcedureSymbol newIteration = GecosUserCoreFactory.procSymbol("SimulationManager_newIteration", voidType, params);
		ps.getScope().getSymbols().add(newIteration);
		ProcedureSymbol writeHuman = GecosUserCoreFactory.procSymbol("SimulationManager_writeHuman", voidType, params);
		ps.getScope().getSymbols().add(writeHuman);
		ProcedureSymbol writeBinary = GecosUserCoreFactory.procSymbol("SimulationManager_writeBinary", voidType, params);
		ps.getScope().getSymbols().add(writeBinary);
		
		
		params.add(profilingNumber);params.add(name);params.add(doubleParam);
		ProcedureSymbol saveDoubleScalarVariable = GecosUserCoreFactory.procSymbol("SimulationManager_saveDoubleVariable", voidType, params); 
		ps.getScope().getSymbols().add(saveDoubleScalarVariable);
		params.clear();params.add(manager);params.add(profilingNumber);params.add(name);params.add(floatParam);
		ProcedureSymbol saveFloatScalarVariable = GecosUserCoreFactory.procSymbol("SimulationManager_saveFloatVariable", voidType, params); 
		ps.getScope().getSymbols().add(saveFloatScalarVariable);
		params.clear();params.add(manager);params.add(profilingNumber);params.add(name);params.add(longParam);
		ProcedureSymbol saveLongScalarVariable = GecosUserCoreFactory.procSymbol("SimulationManager_saveLongVariable", voidType, params); 
		ps.getScope().getSymbols().add(saveLongScalarVariable);
		params.clear();params.add(manager);params.add(profilingNumber);params.add(name);params.add(intParam);
		ProcedureSymbol saveIntScalarVariable = GecosUserCoreFactory.procSymbol("SimulationManager_saveIntVariable", voidType, params);
		ps.getScope().getSymbols().add(saveIntScalarVariable);
		params.clear();params.add(manager);params.add(profilingNumber);params.add(name);params.add(uintParam);
		ProcedureSymbol saveUIntScalarVariable = GecosUserCoreFactory.procSymbol("SimulationManager_saveUIntVariable", voidType, params); 
		ps.getScope().getSymbols().add(saveUIntScalarVariable);
		
		params.clear();params.add(manager);params.add(profilingNumber);params.add(index);params.add(name);params.add(doubleParam);
		ProcedureSymbol saveDoubleArrayVariable = GecosUserCoreFactory.procSymbol("SimulationManager_saveDoubleArrayVariable", voidType, params); 
		ps.getScope().getSymbols().add(saveDoubleArrayVariable);
		params.clear();params.add(manager);params.add(profilingNumber);params.add(index);params.add(name);params.add(floatParam);
		ProcedureSymbol saveFloatArrayVariable = GecosUserCoreFactory.procSymbol("SimulationManager_saveFloatArrayVariable", voidType, params); 
		ps.getScope().getSymbols().add(saveFloatArrayVariable);
		params.clear();params.add(manager);params.add(profilingNumber);params.add(index);params.add(longParam);
		ProcedureSymbol saveLongArrayVariable = GecosUserCoreFactory.procSymbol("SimulationManager_saveLongArrayVariable", voidType, params); 
		ps.getScope().getSymbols().add(saveLongArrayVariable);
		params.clear();params.add(manager);params.add(profilingNumber);params.add(index);params.add(name);params.add(intParam);
		ProcedureSymbol saveIntArrayVariable = GecosUserCoreFactory.procSymbol("SimulationManager_saveIntArrayVariable", voidType, params);
		ps.getScope().getSymbols().add(saveIntArrayVariable);
		params.clear();params.add(manager);params.add(profilingNumber);params.add(index);params.add(name);params.add(uintParam);
		ProcedureSymbol saveUIntArrayVariable = GecosUserCoreFactory.procSymbol("SimulationManager_saveUIntArrayVariable", voidType, params); 
		ps.getScope().getSymbols().add(saveUIntArrayVariable);
				
		// Add ignore annotations
		GecosUserAnnotationFactory.pragma(csimulationManager, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(newManager, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(deleteManager, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(newIteration, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(writeBinary, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(writeHuman, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(saveDoubleScalarVariable, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(saveFloatScalarVariable, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(saveLongScalarVariable, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(saveIntScalarVariable, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(saveUIntScalarVariable, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(saveDoubleArrayVariable, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(saveFloatArrayVariable, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(saveLongArrayVariable, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(saveIntArrayVariable, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(saveUIntArrayVariable, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
	}
	
	private String getFileName(String file) {
		String fileName = file;
		File f = new File(file);
		if (f.isFile())
			fileName = f.getName();
		return fileName;
	}
	
	@Override
	public void printInformation() {
		_resultManager.display("Optimization simulation", _logger);
	}
	
	private void saveReportInformation(int nbCallComputePb) throws SimulationException{
		if(_listDiffAnalSimu == null || _listDiffAnalSimu.size() == 0)
			throw new SimulationException("Impossible to log information because no information of simulation have been saved");
		
		double meanDiffAnalSimu = 0;
		for(double diff : _listDiffAnalSimu){
			meanDiffAnalSimu += diff;
		}
		meanDiffAnalSimu = meanDiffAnalSimu/_listDiffAnalSimu.size();
		_filePrintWriter.println();
		_filePrintWriter.println();
		_filePrintWriter.println("---------------------------------------------------------------------------------------------");
		_filePrintWriter.println("Simulation frequency : " + _frequencyOfSimulation);

		_filePrintWriter.println("Sample number per simulation : " + _nbSimulationSample);
		_filePrintWriter.println("Number total of computePb evaluation : " + nbCallComputePb);
		_filePrintWriter.println("Number of fixed simulation : " + _runManager.getNumberOfFixedSimulationRunned());
		_filePrintWriter.println("Means of the diff Anal/Simu : " + meanDiffAnalSimu);
		_filePrintWriter.println();
		if(_superiorLimit < 0)
			_filePrintWriter.println("Superior limit : " + _superiorLimit);
		_filePrintWriter.println("Number of superior simulation : " + _nbSuperiorThanLimit);
		_filePrintWriter.println("---------------------------------------------------------------------------------------------");
	}
	
	private void saveFixedTimingLogInformation(){
		_sectionTimeLog.addTimeLog("Fixed compilation total time (x" + _runManager.getNumberOfFixedSimulationRunned() + ")", _totalTimeOfFixedCompilation);
		_sectionTimeLog.addTimeLog("Fixed process total time (x" + _runManager.getNumberOfFixedSimulationRunned() + ")", _totalTimeOfFixedProcess);
	}
	
	/**
	 * Create the hierarchy of folder used by the simulator
	 * @throws SimulationException
	 */
	private void createDirectory() throws SimulationException{
		File outfolder = new File(_outputFolder);
		if(!outfolder.isDirectory())
			if(!outfolder.mkdir())
				throw new SimulationException("The creation of the folder " + _outputFolder + " have failed");
		
		File fixedfolder = new File(_fixedFolder);
		if(!fixedfolder.isDirectory())
			if(!fixedfolder.mkdir())
				throw new SimulationException("The creation of the folder " + _fixedFolder + " have failed");
		
		File floatfolder = new File(_floatFolder);
		if(!floatfolder.isDirectory())
			if(!floatfolder.mkdir())
				throw new SimulationException("The creation of the folder " + _floatFolder + " have failed");
		
		File resultFloatFolder = new File(_floatFolder + ConstantPathAndName.OPTIM_SIMU_RESULT_FODLER_NAME);
		if(!resultFloatFolder.isDirectory())
			if(!resultFloatFolder.mkdir())
				throw new SimulationException("The creation of the folder " + _floatFolder + ConstantPathAndName.OPTIM_SIMU_RESULT_FODLER_NAME + " have failed");
		
		File resultFixedFolder = new File(_fixedFolder + ConstantPathAndName.OPTIM_SIMU_RESULT_FODLER_NAME);
		if(!resultFixedFolder.isDirectory())
			if(!resultFixedFolder.mkdir())
				throw new SimulationException("The creation of the folder " + _fixedFolder + ConstantPathAndName.OPTIM_SIMU_RESULT_FODLER_NAME + " have failed");
	}
}
