package fr.irisa.cairn.idfix.simulation.noiseevaluation.backend;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.Logger;

import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.c.generator.XtendCGenerator;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.tools.switches.SymbolSwitch;
import fr.irisa.cairn.idfix.backend.FixedPtSpecificationApplier;
import fr.irisa.cairn.idfix.backend.cpp.SCFixedBackend;
import fr.irisa.cairn.idfix.backend.utils.BackEndUtils;
import fr.irisa.cairn.idfix.model.factory.IDFixUserInformationAndTimingFactory;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Data;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixProject;
import fr.irisa.cairn.idfix.model.informationandtiming.Section;
import fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_OUTPUT_MODE;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.instrument.InputTransformationManager;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.instrument.InstructionInstrumentationManager;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.instrument.MainProcedureCreationManager;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.instrument.TypeFixer;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.AnnotateInputOutputTypeInformation;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.CompilationManager;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.CompilationManager.Version;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.RandomValuesBuilder;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.RunManager;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.result.IResultManager;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.result.MeanVersion;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.result.WorstVersion;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.IDFixUtils;
import fr.irisa.cairn.idfix.utils.exceptions.BackEndException;
import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;
import fr.irisa.cairn.idfix.utils.logging.IDFixLogger;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.ScopeContainer;
import gecos.core.Symbol;
import gecos.gecosproject.GecosHeaderDirectory;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;
import gecos.types.ACFixedType;
import gecos.types.AliasType;
import gecos.types.IntegerType;
import gecos.types.PtrType;
import gecos.types.Type;

/**
 * Simulator which instrument fixed and float procedure set, regenerate source code, compile, run the simulation and process results
 * This simulator use the SimulationManager to save informations during the simulation process.
 * Step of the simulator :
 *  1 - Transform C Float procedure set.
 *  	1 - Detect the system inputs (dynamic pragma) and get back these variables as main function parameters.
 *  	2 - Instrument the source code. Add the save function call to get back the needed information during the simulation process.
 *  	3 - Create the "main" function and the input variable initialization function.
 *  
 *  2 - Transform C Float procedure set.
 *  	1 - Detect the system inputs (dynamic pragma) and get back these variables as main function parameters.
 *  	2 - Annotate the input with to save the data type information. This information is needed to create the initialization procedure (read the random values binary file)
 *  	3 - Instrument the source code. Add the save function call to get back the needed information during the simulation process.
 *  	4 - Regenerate fixed data type from the fix point specification. 
 *  	3 - Create the "main" function and the input variable initialization function.
 *  3 - Generate random value file used by the float and fixed simulation process.
 *  4 - Fix array type (static to dynamic)
 * 	5 - Generate the new source code and process the simulation
 * 	6 - Get back and handle float/fixed result.
 * @author nsimon
 * @date 21/01/14
 *
 */
public class OutputNoiseVerificationSimulator {
	private final boolean _debug;
	private final Version _floatVersion = Version.PREQUANTIFIED;
	
	private IdfixProject _idfixProject;
	private ProcedureSet _psReference;
	private ProcedureSet _psFloat, _psFixed;
	private int _nbSimulation;
	
	private String _outputFolder;
	private String _fixedFolder;
	private String _floatFolder;
	
	private double _analyticSolution;
	
	private final Logger _logger = IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_SIMULATION_BACKEND);

	public OutputNoiseVerificationSimulator(IdfixProject idfixProject) {
		_idfixProject = idfixProject;
		
		if(_idfixProject.getGecosProjectAnnotedAndNotUnrolled().getSources().size() != 1)
			throw new RuntimeException("IDFix FrontEnd is only compatible to work with just one procedure set");
		_psReference = _idfixProject.getGecosProjectAnnotedAndNotUnrolled().getSources().get(0).getModel();
		
		_psFloat = null;
		_psFixed = null;
		
		_outputFolder = _idfixProject.getIDFixConvOutputFolderPath() + ConstantPathAndName.BACKEND_SIMU_OUTPUT_FOLDER_NAME;
		_nbSimulation = _idfixProject.getUserConfiguration().getBackendSimulatorSampleNumber();
		
		_fixedFolder = _outputFolder + ConstantPathAndName.BACKEND_SIMU_FIXED_FODLER_NAME;
		_floatFolder = _outputFolder + ConstantPathAndName.BACKEND_SIMU_FLOAT_FODLER_NAME;
		
		_analyticSolution = _idfixProject.getProcessInformations().getAnalyticInformations().getFinalNoiseConstraint();
		
		_debug = _idfixProject.getUserConfiguration().isDebugMode();
	}
	
	private static final void _assert(boolean b, String msg) throws SimulationException 
	{
		if(!b) throw new SimulationException(msg);
	}
	
	public boolean simulate() {
		Section backendSimulator = IDFixUserInformationAndTimingFactory.SECTION("BackEnd simulator");
		_idfixProject.getProcessInformations().getTiming().getSections().add(backendSimulator);
		long timingStart,  timingStop, sectionTimingSart, sectionTimingStop;
		sectionTimingSart = System.currentTimeMillis();
		
		try{
			timingStart = System.currentTimeMillis();
			_logger.info("     *** Backend simulation process ***");
			// Initialize float and fixed simulation directory
			createDirectory();
			// Copy the procedure set in two other one and apply transformation on it
			
			_logger.info("                - Float instrumentation");	
			transformFloatProcedureSet();
			_logger.info("                - Fixed instrumentation");
			transformFixedProcedureSet();
			
			// Generate random value used by the fixed and float simulation
			RandomValuesBuilder.generate(_psFloat, _psFixed, _nbSimulation, _outputFolder);
			
			// Transform x[N][M] to x** in function prototype
			TypeFixer.fixStaticArrayCompilation(_psFloat);
			TypeFixer.fixStaticArrayCompilation(_psFixed);
			
			// Regenerate float and fixed source code
			new XtendCGenerator(_psFloat, _floatFolder).compute();
			new XtendCGenerator(_psFixed, _fixedFolder).compute();
			
			timingStop = System.currentTimeMillis();
			backendSimulator.addTimeLog("Fixed/Float Instrumentation + random value generation", timingStart, timingStop);
			
			// Compile float and fixed source code to generate respective library
			timingStart = System.currentTimeMillis();
			_logger.info("                - Float and Fixed binary compilation");
			CompilationManager.compile(getFileName(((GecosSourceFile) _psFloat.eContainer()).getName()), _floatFolder, _floatVersion, _logger, _idfixProject.getUserConfiguration().isDebugMode());
			CompilationManager.compile(getFileName(((GecosSourceFile) _psFixed.eContainer()).getName()), _fixedFolder, Version.FIXED, _logger, _idfixProject.getUserConfiguration().isDebugMode());
			timingStop = System.currentTimeMillis();
			backendSimulator.addTimeLog("Fixed/Float compilation", timingStart, timingStop);
			
			// Run the float and fixed simulation
			timingStart = System.currentTimeMillis();
			RunManager runManager = new RunManager(_floatFolder, _fixedFolder);
			_logger.info("                - Float running process");
			runManager.runFloatLibrary();
			_logger.info("                - Fixed running process");
			runManager.runFixedLibrary();
			
			// Get back float and fixed result and process it
			IResultManager resultManager;
			if(_idfixProject.getUserConfiguration().getNoiseFunctionOutputMode() == NOISE_FUNCTION_OUTPUT_MODE.WORST)
				resultManager = new WorstVersion(_floatFolder +  ConstantPathAndName.BACKEND_SIMU_RESULT_FODLER_NAME,  _fixedFolder +  ConstantPathAndName.BACKEND_SIMU_RESULT_FODLER_NAME, _analyticSolution, _nbSimulation);
			else if(_idfixProject.getUserConfiguration().getNoiseFunctionOutputMode() == NOISE_FUNCTION_OUTPUT_MODE.MEAN)
				resultManager = new MeanVersion(_floatFolder +  ConstantPathAndName.BACKEND_SIMU_RESULT_FODLER_NAME,  _fixedFolder +  ConstantPathAndName.BACKEND_SIMU_RESULT_FODLER_NAME, _analyticSolution, _nbSimulation);
			else
				throw new SimulationException("Noise function output mode not managed by the backend simulator " +  _idfixProject.getUserConfiguration().getNoiseFunctionOutputMode());
			
			double simulationNoisePower = resultManager.process();
			resultManager.display("", _logger);
			timingStop = System.currentTimeMillis();
			backendSimulator.addTimeLog("Fixed/Float process execution + result processing", timingStart, timingStop);
			
			//TODO modify plot save information system
			_idfixProject.getProcessInformations().getSimulationInformations().setSampleNumber(_nbSimulation);
			_idfixProject.getProcessInformations().getSimulationInformations().setNoisePowerResult(simulationNoisePower);
		} catch (SimulationException e) {
			_logger.error("Simulation failed", e);
			return false;
		}
		
		sectionTimingStop = System.currentTimeMillis();
		backendSimulator.setTime((sectionTimingStop - sectionTimingSart) / 1000f);
		
		return true;
	}
	
	
	/**
	 * Apply the transformation needed by the simulation process to the float procedure set 
	 * @throws SimulationException 
	 */
	private void transformFloatProcedureSet() throws SimulationException {
		_psFloat = _psReference.copy();
		GecosProject projectfloat = GecosUserCoreFactory.project("GecosProjectFloat");
		GecosSourceFile sourceFloat = GecosUserCoreFactory.source(((GecosSourceFile) _psReference.eContainer()).getName(), _psFloat);
		projectfloat.getSources().add(sourceFloat);
		GecosHeaderDirectory headerdirectory = GecosUserCoreFactory.includeDir(ConstantPathAndName.SIMU_MANAGER_FOLDER_PATH + ConstantPathAndName.SIMU_MANAGER_BIN_FOLDER_NAME
				+ ConstantPathAndName.SIMU_MANAGER_INCLUDE_FOLDER_NAME);
		projectfloat.getIncludes().add(headerdirectory);
		GecosUserAnnotationFactory.pragma(_psFloat, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION+"#include \"csimulationManager.h\"");
		addDummyTypeAndSymbol(_psFloat);	
		
		Type simulationManager = _psFloat.getScope().lookupAliasType("CSimulationManager");
		_assert(simulationManager != null, "Simulation manager library scalar type not found in the procedure set");
		
		PtrType ptrSimulationManager = GecosUserTypeFactory.PTR(simulationManager);
		Symbol paramManager = GecosUserCoreFactory.symbol("manager", ptrSimulationManager);
		_psFloat.getScope().getSymbols().add(paramManager);
		
		// All system input are pulled up as parameter their respective function, and the call of the function are updated
		new InputTransformationManager(_psFloat).upInputToMainProcedureParameters();
		if(_debug)
			new XtendCGenerator(_psFloat, _floatFolder + "inputManager/").compute();
		
		// FORTEST : Use prequantified constant in the float source code
		if(_floatVersion == Version.PREQUANTIFIED){
			ApplyFixedConstantTypeInfromation();
			new SCFixedBackend(projectfloat).compute();
		}
		
		// Add save call function to save all the output value
		new InstructionInstrumentationManager(_psFloat).instrument();
		if(_debug)
			new XtendCGenerator(_psFloat, _floatFolder + "instrumentationManager/").compute();
		
		// Generate main procedure and the input/ouput initialization function
		new MainProcedureCreationManager(_psFloat, _nbSimulation, _floatVersion, _idfixProject.getUserConfiguration().isDebugMode()).generate();
		if(_debug)
			new XtendCGenerator(_psFloat, _floatFolder + "mainCreationManager/").compute();
	}
	
	/**
	 * Apply the transformation needed by the simulation process to the fixed procedure set 
	 * @throws SimulationException 
	 */
	private void transformFixedProcedureSet() throws SimulationException {		
		_psFixed = _psReference.copy();
		GecosProject projectfixed = GecosUserCoreFactory.project("GecosProjectFixed");
		GecosSourceFile sourceFixed = GecosUserCoreFactory.source(((GecosSourceFile) _psReference.eContainer()).getName() ,_psFixed);
		projectfixed.getSources().add(sourceFixed);
		GecosHeaderDirectory headerdirectory = GecosUserCoreFactory.includeDir(ConstantPathAndName.SIMU_MANAGER_FOLDER_PATH + ConstantPathAndName.SIMU_MANAGER_BIN_FOLDER_NAME
				+ ConstantPathAndName.SIMU_MANAGER_INCLUDE_FOLDER_NAME);
		projectfixed.getIncludes().add(headerdirectory);
		
		GecosUserAnnotationFactory.pragma(_psFixed, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION+"#define SC_INCLUDE_FX");
		GecosUserAnnotationFactory.pragma(_psFixed, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION+"#include \"systemc.h\"");
		GecosUserAnnotationFactory.pragma(_psFixed, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION+"#include \"csimulationManager.h\"");
		addDummyTypeAndSymbol(_psFixed);
		
		Type simulationManager = _psFixed.getScope().lookupAliasType("CSimulationManager");
		_assert(simulationManager != null, "Simulation manager library scalar type not found in the procedure set");

		PtrType ptrSimulationManager = GecosUserTypeFactory.PTR(simulationManager);
		Symbol paramManager = GecosUserCoreFactory.symbol("manager", ptrSimulationManager);
		_psFixed.getScope().getSymbols().add(paramManager);
						
		Procedure pWithMainPragma = IDFixUtils.getMainProcedure(_psFixed);
		_assert(pWithMainPragma != null, "Simulation manager library scalar type not found in the procedure set");
		
		// Need to be done before InstructionInstrumentManager
		// All system input are pulled up as parameter their respective function, and the call of the function are updated
		new InputTransformationManager(_psFixed).upInputToMainProcedureParameters();
		if(_debug)
			new XtendCGenerator(_psFixed, _fixedFolder + "inputManager/").compute();
		
		// Add annotation of the original type in the system outputs and inputs. It is needed by the MainProcedureCreationManager
		// Need to be done before the FixedPtSpecificationApplier
		// TODO Regroup in the InputTransformationManager class
		// FIXME work only if the input and output of the system are in the "main" function
		new AnnotateInputOutputTypeInformation().process(pWithMainPragma);
		
		// Dont work when fixed point data are present. Need to do it before FixedPtSpecificationApplier
		// Add save call function to save all the output value
		//TODO Use AnnotateInputOutputTypeInformation annotation to do it after FixedPtSpecificationApplier??? 
		new InstructionInstrumentationManager(_psFixed).instrument();
		if(_debug)
			new XtendCGenerator(_psFixed, _fixedFolder + "instrumentationManager/").compute();
		
//		new SCTypeTransformation(_psFixed, _idfixProject.getFixedPointSpecification()).apply();
		new FixedPtSpecificationApplier(_psFixed, _idfixProject.getFixedPointSpecification()).compute();
		new SCFixedBackend(projectfixed).compute();
		if(_debug)
			new XtendCGenerator(_psFixed, _fixedFolder + "scTypeTranformation/").compute();
		
		// Need to do after FixedPtSpecificationApplier because this transformation add new function and complex type not managed by the FixedPtSpecificationApplier.
		// Generate main procedure and the input/ouput initialization functions
		new MainProcedureCreationManager(_psFixed, _nbSimulation, Version.FIXED, _idfixProject.getUserConfiguration().isDebugMode()).generate();
		if(_debug)
			new XtendCGenerator(_psFixed, _fixedFolder + "mainCreationManager/").compute();
	}
	
	/**
	 * Add all the required type and symbol representing the simulation manager library
	 * @param ps
	 */
	private void addDummyTypeAndSymbol(ProcedureSet ps){
		GecosUserTypeFactory.setScope(ps.getScope());
		
		// Type creation
		Type voidType = GecosUserTypeFactory.VOID();
		AliasType csimulationManager = GecosUserTypeFactory.ALIAS(voidType, "CSimulationManager");
		IntegerType charconst = GecosUserTypeFactory.CHAR();
		charconst.setConstant(true);
		
		Type doubleType = GecosUserTypeFactory.DOUBLE();
		Type floatType = GecosUserTypeFactory.FLOAT();
		Type longType = GecosUserTypeFactory.LONG();
		Type intType = GecosUserTypeFactory.INT();
		Type uintType = GecosUserTypeFactory.UINT();
		
		// Pointer creation
		PtrType ptrmanager = GecosUserTypeFactory.PTR(csimulationManager);
		PtrType ptrcharconst = GecosUserTypeFactory.PTR(charconst);
		
		
		// Parameter Symbol creation
		ParameterSymbol manager = GecosUserCoreFactory.paramSymbol("manager", ptrmanager);
		ParameterSymbol name = GecosUserCoreFactory.paramSymbol("name", ptrcharconst);
		ParameterSymbol doubleParam = GecosUserCoreFactory.paramSymbol("value", doubleType);
		ParameterSymbol floatParam = GecosUserCoreFactory.paramSymbol("value", floatType);
		ParameterSymbol longParam = GecosUserCoreFactory.paramSymbol("value", longType);
		ParameterSymbol intParam = GecosUserCoreFactory.paramSymbol("value", intType);
		ParameterSymbol uintParam = GecosUserCoreFactory.paramSymbol("value", uintType);
		ParameterSymbol profilingNumber = GecosUserCoreFactory.paramSymbol("profiling_number", uintType);
		ParameterSymbol index = GecosUserCoreFactory.paramSymbol("index", uintType);
		
		
		// Dummy procedure set creation
		List<ParameterSymbol> params = new LinkedList<>();
		ProcedureSymbol newManager = GecosUserCoreFactory.procSymbol("newSimulationManager", ptrmanager, params);
		ps.getScope().getSymbols().add(newManager);
		ProcedureSymbol deleteManager = GecosUserCoreFactory.procSymbol("deleteSimulationManager", voidType, params);
		ps.getScope().getSymbols().add(deleteManager);
		
		
		params.add(manager);
		ProcedureSymbol newIteration = GecosUserCoreFactory.procSymbol("SimulationManager_newIteration", voidType, params);
		ps.getScope().getSymbols().add(newIteration);
		ProcedureSymbol writeHuman = GecosUserCoreFactory.procSymbol("SimulationManager_writeHuman", voidType, params);
		ps.getScope().getSymbols().add(writeHuman);
		ProcedureSymbol writeBinary = GecosUserCoreFactory.procSymbol("SimulationManager_writeBinary", voidType, params);
		ps.getScope().getSymbols().add(writeBinary);
		
		
		params.add(profilingNumber);params.add(name);params.add(doubleParam);
		ProcedureSymbol saveDoubleScalarVariable = GecosUserCoreFactory.procSymbol("SimulationManager_saveDoubleVariable", voidType, params); 
		ps.getScope().getSymbols().add(saveDoubleScalarVariable);
		params.clear();params.add(manager);params.add(profilingNumber);params.add(name);params.add(floatParam);
		ProcedureSymbol saveFloatScalarVariable = GecosUserCoreFactory.procSymbol("SimulationManager_saveFloatVariable", voidType, params); 
		ps.getScope().getSymbols().add(saveFloatScalarVariable);
		params.clear();params.add(manager);params.add(profilingNumber);params.add(name);params.add(longParam);
		ProcedureSymbol saveLongScalarVariable = GecosUserCoreFactory.procSymbol("SimulationManager_saveLongVariable", voidType, params); 
		ps.getScope().getSymbols().add(saveLongScalarVariable);
		params.clear();params.add(manager);params.add(profilingNumber);params.add(name);params.add(intParam);
		ProcedureSymbol saveIntScalarVariable = GecosUserCoreFactory.procSymbol("SimulationManager_saveIntVariable", voidType, params);
		ps.getScope().getSymbols().add(saveIntScalarVariable);
		params.clear();params.add(manager);params.add(profilingNumber);params.add(name);params.add(uintParam);
		ProcedureSymbol saveUIntScalarVariable = GecosUserCoreFactory.procSymbol("SimulationManager_saveUIntVariable", voidType, params); 
		ps.getScope().getSymbols().add(saveUIntScalarVariable);
		
		params.clear();params.add(manager);params.add(profilingNumber);params.add(index);params.add(name);params.add(doubleParam);
		ProcedureSymbol saveDoubleArrayVariable = GecosUserCoreFactory.procSymbol("SimulationManager_saveDoubleArrayVariable", voidType, params); 
		ps.getScope().getSymbols().add(saveDoubleArrayVariable);
		params.clear();params.add(manager);params.add(profilingNumber);params.add(index);params.add(name);params.add(floatParam);
		ProcedureSymbol saveFloatArrayVariable = GecosUserCoreFactory.procSymbol("SimulationManager_saveFloatArrayVariable", voidType, params); 
		ps.getScope().getSymbols().add(saveFloatArrayVariable);
		params.clear();params.add(manager);params.add(profilingNumber);params.add(index);params.add(longParam);
		ProcedureSymbol saveLongArrayVariable = GecosUserCoreFactory.procSymbol("SimulationManager_saveLongArrayVariable", voidType, params); 
		ps.getScope().getSymbols().add(saveLongArrayVariable);
		params.clear();params.add(manager);params.add(profilingNumber);params.add(index);params.add(name);params.add(intParam);
		ProcedureSymbol saveIntArrayVariable = GecosUserCoreFactory.procSymbol("SimulationManager_saveIntArrayVariable", voidType, params);
		ps.getScope().getSymbols().add(saveIntArrayVariable);
		params.clear();params.add(manager);params.add(profilingNumber);params.add(index);params.add(name);params.add(uintParam);
		ProcedureSymbol saveUIntArrayVariable = GecosUserCoreFactory.procSymbol("SimulationManager_saveUIntArrayVariable", voidType, params); 
		ps.getScope().getSymbols().add(saveUIntArrayVariable);
				
		// Add ignore annotations
		GecosUserAnnotationFactory.pragma(csimulationManager, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(newManager, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(deleteManager, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(newIteration, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(writeBinary, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(writeHuman, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(saveDoubleScalarVariable, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(saveFloatScalarVariable, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(saveLongScalarVariable, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(saveIntScalarVariable, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(saveUIntScalarVariable, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(saveDoubleArrayVariable, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(saveFloatArrayVariable, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(saveLongArrayVariable, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(saveIntArrayVariable, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(saveUIntArrayVariable, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
	}
		
	private String getFileName(String file) {
		String fileName = file;
		File f = new File(file);
		if (f.isFile())
			fileName = f.getName();
		return fileName;
	}
	
	/**
	 * Create the hierarchy of folder used by the simulator
	 * @throws BackEndException
	 */
	private void createDirectory() throws SimulationException{
		File outfolder = new File(_outputFolder);
		if(!outfolder.isDirectory())
			if(!outfolder.mkdir())
				throw new SimulationException("The creation of the folder " + _outputFolder + " have failed");
		
		File fixedfolder = new File(_fixedFolder);
		if(!fixedfolder.isDirectory())
			if(!fixedfolder.mkdir())
				throw new SimulationException("The creation of the folder " + _fixedFolder + " have failed");
		
		File floatfolder = new File(_floatFolder);
		if(!floatfolder.isDirectory())
			if(!floatfolder.mkdir())
				throw new SimulationException("The creation of the folder " + _floatFolder + " have failed");
		
		File resultFloatFolder = new File(_floatFolder + ConstantPathAndName.BACKEND_SIMU_RESULT_FODLER_NAME);
		if(!resultFloatFolder.isDirectory())
			if(!resultFloatFolder.mkdir())
				throw new SimulationException("The creation of the folder " + _floatFolder + ConstantPathAndName.BACKEND_SIMU_RESULT_FODLER_NAME + " have failed");
		
		File resultFixedFolder = new File(_fixedFolder + ConstantPathAndName.BACKEND_SIMU_RESULT_FODLER_NAME);
		if(!resultFixedFolder.isDirectory())
			if(!resultFixedFolder.mkdir())
				throw new SimulationException("The creation of the folder " + _fixedFolder + ConstantPathAndName.BACKEND_SIMU_RESULT_FODLER_NAME + " have failed");
	}
	
		
	public void ApplyFixedConstantTypeInfromation(){
		GecosUserAnnotationFactory.pragma(_psFloat, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION+"#define SC_INCLUDE_FX");
		GecosUserAnnotationFactory.pragma(_psFloat, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION+"#include \"systemc.h\"");
		new ApplyFixedTypeToFloatConstant().doSwitch(_psFloat);
	}
	
	private class ApplyFixedTypeToFloatConstant extends SymbolSwitch<Boolean> {
		@Override
		public Boolean caseProcedureSet(ProcedureSet object) {
			for (Procedure proc: object.listProcedures()) {
				doSwitch(proc);
			}
			return true;
		}
		
		@Override
		public Boolean caseScopeContainer(ScopeContainer object) {
			Scope scope = object.getScope();
			if(scope != null){
				for(Symbol sym : scope.getSymbols()){
					TypeAnalyzer typeAnalyzer = new TypeAnalyzer(sym.getType());
					if(typeAnalyzer.getBaseLevel().isFloat() && sym.getValue() != null){
						Data data = _idfixProject.getFixedPointSpecification().findDataFromSymbol(sym);
						if(data != null){
							ACFixedType fixedType = BackEndUtils.createACFixedFromFixedPtInfo(data.getFixedInformation());
							sym.setType(new TypeAnalyzer(sym.getType()).revertAllLevelsOn(fixedType));
						}
					}							
				}
			}
			
			return true;
		}
	}
}