package fr.irisa.cairn.idfix.simulation.noiseevaluation.instrument;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import fr.irisa.cairn.float2fix.model.extended.factory.ExtendedInstrFactory;
import fr.irisa.cairn.gecos.model.analysis.types.ArrayLevel;
import fr.irisa.cairn.gecos.model.analysis.types.BaseLevel;
import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.AnnotateInputOutputTypeInformation;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.CompilationManager.Version;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.IDFixUtils;
import fr.irisa.cairn.idfix.utils.exceptions.BackEndException;
import fr.irisa.cairn.idfix.utils.exceptions.SFGModelCreationException;
import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;
import gecos.annotations.StringAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Symbol;
import gecos.extended.types.SCFixedType;
import gecos.extended.types.SCUFixedType;
import gecos.instrs.CallInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SizeofTypeInstruction;
import gecos.types.AliasType;
import gecos.types.BaseType;
import gecos.types.PtrType;
import gecos.types.SignModifiers;
import gecos.types.Type;

/**
 * Create the main function needed to compile and run the simulation process.
 * The main function contained :
 * 	- declaration of each system inputs and output (get back from the function with the main pragma)
 * 	- the initialization of the simulation manager (simulation manager library use)
 * 	- the main loop which call N times (simulation number) the function with the main pragma)
 * 
 * There are adding of initialization function for each system inputs and outputs.
 * These functions contained :
 * 	- dynamic memory allocation of the variable
 * 	- just input features :
 * 		- all the necessary stuff to read a file
 * 		- initialization of each inputs from the file 
 * 
 * @author nsimon
 * @date 27/03/2014
 */
public class MainProcedureCreationManager {	
	private ProcedureSet _ps;
	private int _nbSimulation;
	private Version _version;
	private boolean _debug;
	
	private final String _initializationFunctionName = "idfix_simu_init_";
	
	// Dummy types and symbols
	private ProcedureSymbol _fopen, _fseek, _rewind, _malloc, _free, _fread, _printf, _ftell, _exit;
	private Symbol _snull, _sseekend;
	private Type _ptrcfile;
	
	public MainProcedureCreationManager(ProcedureSet procedureSet, int nbSimulation, Version version, boolean debug){
		_ps = procedureSet;
		_nbSimulation = nbSimulation;
		_version = version;
		_debug = debug;
		
		GecosUserTypeFactory.setScope(_ps.getScope());
	}
	
	private static final void _assert(boolean b, String msg) throws SimulationException 
	{
		if(!b) throw new SimulationException(msg);
	}
	
	public void generate() throws SimulationException{
		Procedure pWithMainPragma = IDFixUtils.getMainProcedure(_ps);
		_assert(pWithMainPragma != null, "No procedure with the \"MAIN_FUNC\" pragma detected in the procedure set");
		
		addDummyTypeAndSymbol();
		
		// Step 1 : Creation of the main procedure.
		CompositeBlock bodyMain = createMainProcedure();
		
		// Step 2 : Adding in the main procedure declaration for each system inputs and ouputs. Adding of the inputs/ouputs associated initialization procedure.
		// We use the parameters of the function with the main pragma to get back some information (name, type...) to create the inputs/outputs declaration.
		declareParametersOfSystemInputProcedure(pWithMainPragma, bodyMain);
		
		// Step 3 : Adding of the main loop. In this part, we create run manager initialization part too.
		buildSimulationLoop(pWithMainPragma, bodyMain);
		
		//TODO need to delete the main function parameters allocation to avoid memory loss
		
		// Just add return 0
		bodyMain.addChildren(GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.ret(GecosUserInstructionFactory.Int(0))));
	}

	/**
	 * Add the main function of the simulation source code the declaration of each system input and output contained as parameters of the MAIN_FUNCTION of the system.
	 * Build the initialization function.
	 * @param pWithMainPragma
	 * @param bodyMain
	 * @throws SimulationException
	 */
	private void declareParametersOfSystemInputProcedure(Procedure pWithMainPragma, CompositeBlock bodyMain) throws SimulationException {
		try{
			Symbol newSymbol;
			for(Symbol param : pWithMainPragma.listParameters()){
				_assert(IDFixUtils.isInputVar(param) || IDFixUtils.isOutputVar(param) || IDFixUtils.isDelayedVar(param),
						"Parameter " + param.getName() + " of the main function is not a input, output or a delay");
				
				TypeAnalyzer analyzer = new TypeAnalyzer(param.getType());
				if(analyzer.isBase() && analyzer.getBaseLevel().isBaseType()){ 
					newSymbol = buildScalarDeclaration(param, bodyMain);
				}
				else if(analyzer.isArray()){
					newSymbol = buildArrayDeclaration(param, bodyMain);	
				}
				else{
					throw new SimulationException("Kind of paramater type not managed yet in the back end simulator : " + analyzer);
				}				
				buildInitializationProcedure(newSymbol, bodyMain, _nbSimulation, getDimensions(param), IDFixUtils.isInputVar(param));
			}
		} catch (SFGModelCreationException e){
			throw new SimulationException(e);
		}
	}

	/**
	 * Create the main simulation procedure.
	 * @return Body composite block of the main procedure
	 */
	private CompositeBlock createMainProcedure() {
		CompositeBlock bodyMain = GecosUserBlockFactory.CompositeBlock();
		List<ParameterSymbol> params = new ArrayList<>();
		params.add(GecosUserCoreFactory.paramSymbol("argc", GecosUserTypeFactory.INT()));
		params.add(GecosUserCoreFactory.paramSymbol("argv", GecosUserTypeFactory.ARRAY(GecosUserTypeFactory.PTR(GecosUserTypeFactory.CHAR()), null)));
		Procedure pMain;
		if(_version == Version.FIXED || _version == Version.PREQUANTIFIED)
			pMain = GecosUserCoreFactory.proc(_ps,GecosUserCoreFactory.procSymbol("sc_main",  GecosUserTypeFactory.INT(), params), bodyMain);
		else
			pMain = GecosUserCoreFactory.proc(_ps, GecosUserCoreFactory.procSymbol("main", GecosUserTypeFactory.INT(), params), bodyMain);
		_ps.addProcedure(pMain);
		return bodyMain;
	}
	
	/**
	 * Add all dummy type and symbol in the IR needed to read a file
	 * @param ps
	 */
	private void addDummyTypeAndSymbol(){
		GecosUserAnnotationFactory.pragma(_ps, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION+"#include <stdio.h>");
		GecosUserAnnotationFactory.pragma(_ps, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION+"#include <stdlib.h>");
		
		// Type creation
//		Type voidType = GecosUserTypeFactory.VOID(); due to gecos auto instruction type deduction...
		Type voidType = GecosUserTypeFactory.INT();
		Type charType = GecosUserTypeFactory.CHAR();
		Type intType = GecosUserTypeFactory.INT();
		Type longType = GecosUserTypeFactory.LONG();
		Type uintType = GecosUserTypeFactory.UINT();
		AliasType cfile = GecosUserTypeFactory.ALIAS(intType, "FILE");		
		
		// Pointer creation
		PtrType ptrchar = GecosUserTypeFactory.PTR(charType);
		_ptrcfile = GecosUserTypeFactory.PTR(cfile);
		PtrType ptrvoid = GecosUserTypeFactory.PTR(voidType);
		
		// Const creation
		Type constptrtype = ptrchar.copy();
		constptrtype.setConstant(true);
		
		
		// Parameter symbol creation
		ParameterSymbol filename = GecosUserCoreFactory.paramSymbol("filename", constptrtype);
		ParameterSymbol mode = GecosUserCoreFactory.paramSymbol("mode", constptrtype);
		ParameterSymbol stream = GecosUserCoreFactory.paramSymbol("stream", _ptrcfile);
		ParameterSymbol offset = GecosUserCoreFactory.paramSymbol("offset", intType);
		ParameterSymbol origin = GecosUserCoreFactory.paramSymbol("origin", intType);
		ParameterSymbol size = GecosUserCoreFactory.paramSymbol("size", uintType);
		ParameterSymbol count = GecosUserCoreFactory.paramSymbol("count", uintType);
		ParameterSymbol ptr = GecosUserCoreFactory.paramSymbol("ptr", ptrvoid);
		ParameterSymbol msg = GecosUserCoreFactory.paramSymbol("msg", ptrchar);
		ParameterSymbol status = GecosUserCoreFactory.paramSymbol("status", intType);
		
		// Symbol creation
		_snull = GecosUserCoreFactory.symbol("NULL", voidType);
		_sseekend = GecosUserCoreFactory.symbol("SEEK_END", intType);
		_ps.getScope().getSymbols().add(_snull);
		_ps.getScope().getSymbols().add(_sseekend);
		
		// Dummy procedure creation
		List<ParameterSymbol> params = new ArrayList<>();
		params.add(filename);params.add(mode);
		_fopen = GecosUserCoreFactory.procSymbol("fopen", _ptrcfile, params);
		_ps.getScope().getSymbols().add(_fopen);
		
		params.clear();params.add(stream);params.add(offset);params.add(origin);
		_fseek = GecosUserCoreFactory.procSymbol("fseek", intType, params);
		_ps.getScope().getSymbols().add(_fseek);
		
		params.clear();params.add(stream);
		_ftell = GecosUserCoreFactory.procSymbol("ftell", longType, params);
		_ps.getScope().getSymbols().add(_ftell);
		
		params.clear(); params.add(stream);
		_rewind = GecosUserCoreFactory.procSymbol("rewind", voidType, params);
		_ps.getScope().getSymbols().add(_rewind);
		
		params.clear();params.add(size);
		_malloc = GecosUserCoreFactory.procSymbol("malloc", ptrvoid, params);
		_ps.getScope().getSymbols().add(_malloc);
		
		params.clear();params.add(ptr);
		_free = GecosUserCoreFactory.procSymbol("free", voidType, params);
		_ps.getScope().getSymbols().add(_free);
		
		params.clear();params.add(ptr);params.add(size);params.add(count);params.add(stream);
		_fread = GecosUserCoreFactory.procSymbol("fread", uintType, params);
		_ps.getScope().getSymbols().add(_fread);
		
		params.clear();params.add(msg);
		_printf = GecosUserCoreFactory.procSymbol("printf", voidType, params);
		_ps.getScope().getSymbols().add(_printf);
		
		params.clear();params.add(status);
		_exit = GecosUserCoreFactory.procSymbol("exit", voidType, params);
		_ps.getScope().getSymbols().add(_exit);
			
		GecosUserAnnotationFactory.pragma(cfile, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(_snull, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(_sseekend, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(_fopen, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(_fseek, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(_ftell, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(_rewind, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(_malloc, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(_free, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(_fread, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(_printf, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(_exit, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
	}
	
	/**
	 * Create and add declaration for each system scalar inputs and outputs
	 * @param s Symbol which is a parameters of the function with the main pragma.
	 * @param bodyMain Composite block where add the declaration of the new symbol created
	 * @return new symbol created
	 * @throws SimulationException
	 */
	private Symbol buildScalarDeclaration(Symbol s, CompositeBlock bodyMain) throws SimulationException{
		TypeAnalyzer analyzer = new TypeAnalyzer(s.getType());
		_assert(analyzer.isBase() && analyzer.getBaseLevel().isBaseType(), "The type of the symbol " + s.getName() + " not correspond to a scalar type");

		// Create type* s = new type[N]
		BaseLevel baseLevel = analyzer.getBaseLevel();
		BaseType baseType = (BaseType) baseLevel.getBase();
		PtrType ptrType = GecosUserTypeFactory.PTR(baseType);
		Symbol input = GecosUserCoreFactory.symbol(s.getName(), ptrType);
		Instruction newInstruction = ExtendedInstrFactory.NewArray(baseType, getDimensions(s));
		input.setValue(newInstruction);
		bodyMain.getScope().getSymbols().add(input);
		
		StringAnnotation annot;
		if((annot = (StringAnnotation) s.getAnnotation(AnnotateInputOutputTypeInformation.ANNOT_KEY)) != null){
			input.setAnnotation(AnnotateInputOutputTypeInformation.ANNOT_KEY, GecosUserAnnotationFactory.STRING(annot.getContent()));
		}
		
		return input;
	}
	
	/**
	 * Create and add declaration for each system array inputs and outputs
	 * @param s Symbol which is a parameters of the function with the main pragma.
	 * @param bodyMain Composite block where add the declaration of the new symbol created
	 * @return new symbol created
	 * @throws SimulationException
	 */
	private Symbol buildArrayDeclaration(Symbol s, CompositeBlock bodyMain) throws SimulationException{
		TypeAnalyzer analyzer = new TypeAnalyzer(s.getType());
		ArrayLevel arrLevel = analyzer.tryGetArrayLevel(0);
		_assert(analyzer.isArray() && arrLevel != null, "The type of the symbol " + s.getName() + " not correspond to a array type");
		
		// Create type** s = new type*[N]
		BaseLevel baseLevel = analyzer.getBaseLevel();
		_assert(baseLevel.isBaseType(), "Type base type of the array is not a scalar base type");
		BaseType baseType = (BaseType) baseLevel.getBase();
		PtrType ptrType = GecosUserTypeFactory.PTR(baseType);
		for(int i = 0 ; i < arrLevel.getNDims() ; i++){
			ptrType = GecosUserTypeFactory.PTR(ptrType);
		}
		Symbol input = GecosUserCoreFactory.symbol(s.getName(),ptrType);
		Instruction newInstruction = ExtendedInstrFactory.NewArray(baseType, getDimensions(s));
		input.setValue(newInstruction);
		bodyMain.getScope().addSymbol(input);
		
		StringAnnotation annot;
		if((annot = (StringAnnotation) s.getAnnotation(AnnotateInputOutputTypeInformation.ANNOT_KEY)) != null){
			input.setAnnotation(AnnotateInputOutputTypeInformation.ANNOT_KEY, GecosUserAnnotationFactory.STRING(annot.getContent()));
		}
		
		return input;
	}
	
	/**
	 * Create a array which contained dimension instructions of the symbol. Dimension instructions represent the array index of the array created in the main function.
	 * So, the number of simulation is the first instruction.
	 * @param s
	 * @return array which contained the dimension instruction of the symbol s
	 * @throws SimulationException 
	 */
	private Instruction[] getDimensions(Symbol s) throws SimulationException{
		TypeAnalyzer analyzer = new TypeAnalyzer(s.getType());
		Instruction[] dimensions;
		
		if(analyzer.isBase() && analyzer.getBaseLevel().isBaseType()){
			dimensions = new Instruction[1];
			dimensions[0] = GecosUserInstructionFactory.Int(_nbSimulation);
		}
		else if(analyzer.isArray()){
			ArrayLevel arrLevel = analyzer.tryGetArrayLevel(0);
			_assert(analyzer.isArray() && arrLevel != null, "The type of the symbol " + s.getName() + " not correspond to a array type");
			dimensions = new Instruction[arrLevel.getNDims() + 1];
			dimensions[0] = GecosUserInstructionFactory.Int(_nbSimulation);
			for(int i = 0 ; i < arrLevel.getNDims() ; i++){
				Long dim = arrLevel.tryGetSize(i);
				_assert(dim != null, "An array dimension size of symbol '" + s + "' is not a immediate integer : " + arrLevel.getSize(i));
				dimensions[arrLevel.getNDims() - i] = GecosUserInstructionFactory.Int(dim);
			}
		}
		else{
			throw new SimulationException("Kind of paramater type not managed yet in the back end simulator : " + analyzer);
		}
		
		return dimensions;
	}
	
	/**
	 * Create and build the initialization procedure where we read binary file to initialize the specified input s
	 * @param input_output
	 * @param bodyMain
	 * @param nbSimulation
	 * @param dimension
	 * @param isInput
	 * @throws SimulationException
	 */
	private void buildInitializationProcedure(Symbol input_output, CompositeBlock bodyMain, int nbSimulation, Instruction[] dimensions, boolean isInput) throws SimulationException{
		// Initialization procedure creation
		TypeAnalyzer analyzer = new TypeAnalyzer(input_output.getType());
		CompositeBlock bodyproc = GecosUserBlockFactory.CompositeBlock();
		
		List<ParameterSymbol> params = new ArrayList<>();
		ParameterSymbol paramSym = GecosUserCoreFactory.paramSymbol(input_output.getName(), input_output.getType());
		params.add(paramSym);
		ParameterSymbol pathFile = null;
		if(isInput){
			pathFile = GecosUserCoreFactory.paramSymbol("pathFile", GecosUserTypeFactory.PTR(GecosUserTypeFactory.CHAR()));	
			params.add(pathFile);
		}
		
		Procedure p = GecosUserCoreFactory.proc(_ps, GecosUserCoreFactory.procSymbol(_initializationFunctionName + input_output.getName(), GecosUserTypeFactory.VOID(), params), bodyproc);
		_ps.addProcedure(p);
		
		if(isInput){
			bodyMain.addChildren(GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.call(p.getSymbol(), GecosUserInstructionFactory.symbref(input_output), GecosUserInstructionFactory.string("../" + input_output.getName() + "_values.bin"))));
		}
		else{
			bodyMain.addChildren(GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.call(p.getSymbol(), GecosUserInstructionFactory.symbref(input_output))));
		}
		
		_assert(analyzer.isPointer(), "An input in the main procedure have not a ptr type");
		BaseLevel baseLevel = analyzer.getBaseLevel();
		_assert(baseLevel.isBaseType(), "Base type of the pointer is not a scalar base type. Impossible to create the input/output initialization procedure");
		BaseType baseType = (BaseType) baseLevel.getBase();
		
		// Creation of the loop which allocate memory of the input and output
		CompositeBlock currentCB;
		ForBlock forBlock;
		CompositeBlock cbToAdd = bodyproc;
		LinkedList<Symbol> indexes = new LinkedList<>();
		Symbol index;
		for(int i = 0 ; i < dimensions.length - 1 ; i++){
			index = GecosUserCoreFactory.symbol("i", GecosUserTypeFactory.INT(), bodyproc.getScope());
			indexes.add(index);
			bodyproc.getScope().makeUnique(index);
			BasicBlock init = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(index), GecosUserInstructionFactory.Int(0)));
			BasicBlock test = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.lt(GecosUserInstructionFactory.symbref(index), dimensions[i].copy()));
			BasicBlock step = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(index), GecosUserInstructionFactory.add(
								GecosUserInstructionFactory.symbref(index), GecosUserInstructionFactory.Int(1))));
			
			currentCB = GecosUserBlockFactory.CompositeBlock();
			forBlock = GecosUserBlockFactory.For(init, test, step, currentCB);
			
			
			Instruction[] tempDimension = new Instruction[dimensions.length - (i + 1)];
			for(int j = 0 ; j < tempDimension.length ; j++){						
					tempDimension[j] = dimensions[i + j + 1].copy();
			}
			Instruction newInst = ExtendedInstrFactory.NewArray(baseType, tempDimension);
			
			List<Instruction> indexInst = new ArrayList<>(); 
			for(Symbol sym : indexes){
				indexInst.add(GecosUserInstructionFactory.symbref(sym));
			}
			Instruction arrInst = GecosUserInstructionFactory.array(input_output, indexInst);
			
			SetInstruction setInst = GecosUserInstructionFactory.set(arrInst, newInst);
			
			currentCB.addChildren(GecosUserBlockFactory.BBlock(setInst));
			cbToAdd.addChildren(forBlock);
			cbToAdd = currentCB;
		}
		
		// Creation of all the necessary stuff to read a file only if we create a the initialization function for a system input 
		if(isInput){
			Symbol file = GecosUserCoreFactory.symbol("pfile", _ptrcfile);		
			Symbol fsize = GecosUserCoreFactory.symbol("fsize", GecosUserTypeFactory.LONG());
			Symbol nbElems = GecosUserCoreFactory.symbol("nbElems", GecosUserTypeFactory.LONG());
			Symbol buffer;
			BaseType bufferType;
			
			// BackEnd simulator fixed version case
			if(_version == Version.FIXED){
				_assert(baseLevel.isFixedPoint() || baseLevel.getBase() instanceof SCUFixedType || baseLevel.getBase() instanceof SCFixedType, "System input or output have not a fixed type in the fixed IR version : " + input_output);
				StringAnnotation annot;
				if((annot = (StringAnnotation) input_output.getAnnotation(AnnotateInputOutputTypeInformation.ANNOT_KEY)) != null){
					if(annot.getContent().equals(AnnotateInputOutputTypeInformation.FLOAT_TYPE)){
						bufferType = GecosUserTypeFactory.FLOAT();
					} else if(annot.getContent().equals(AnnotateInputOutputTypeInformation.DOUBLE_TYPE)){
						bufferType = GecosUserTypeFactory.DOUBLE();
					} else if(annot.getContent().equals(AnnotateInputOutputTypeInformation.INT_TYPE)){
						bufferType = GecosUserTypeFactory.INT();
					} else if(annot.getContent().equals(AnnotateInputOutputTypeInformation.UINT_TYPE)){
						bufferType = GecosUserTypeFactory.UINT();
					} else if(annot.getContent().equals(AnnotateInputOutputTypeInformation.SHORT_TYPE)){
						bufferType = GecosUserTypeFactory.SHORT();
					} else if(annot.getContent().equals(AnnotateInputOutputTypeInformation.USHORT_TYPE)){
						bufferType = GecosUserTypeFactory.USHORT();
					} else if(annot.getContent().equals(AnnotateInputOutputTypeInformation.LONG_TYPE)){
						bufferType = GecosUserTypeFactory.LONG(SignModifiers.SIGNED);
					} else if(annot.getContent().equals(AnnotateInputOutputTypeInformation.ULONG_TYPE)){
						bufferType = GecosUserTypeFactory.LONG(SignModifiers.UNSIGNED);
					} else if(annot.getContent().equals(AnnotateInputOutputTypeInformation.LONGLONG_TYPE)){
						bufferType = GecosUserTypeFactory.LONGLONG();
					} else if(annot.getContent().equals(AnnotateInputOutputTypeInformation.ULONGLONG_TYPE)){
						bufferType = GecosUserTypeFactory.ULONGLONG();
					} else {
						throw new SimulationException("The annotation which indicate the original type of the fixed type have a incorrect value");
					}
				}
				else{
					throw new SimulationException("The annotation which indicate the original type of the fixed type value is missing");
				}
			} else {
				bufferType = baseType;
			}
			buffer = GecosUserCoreFactory.symbol("buffer", GecosUserTypeFactory.PTR(bufferType));
			
			bodyproc.getScope().getSymbols().add(file);
			bodyproc.getScope().getSymbols().add(fsize);
			bodyproc.getScope().getSymbols().add(nbElems);
			bodyproc.getScope().getSymbols().add(buffer);
			
			// Creation of the instruction needed to init the input array from the binary file
			Instruction callfopen = GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(file),
					GecosUserInstructionFactory.call(_fopen, GecosUserInstructionFactory.symbref(pathFile), GecosUserInstructionFactory.string("rb")));
			bodyproc.addChildren(GecosUserBlockFactory.BBlock(callfopen));
			
			BasicBlock then = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.call(_printf, GecosUserInstructionFactory.string("Impossible to open binary random values file")), GecosUserInstructionFactory.call(_exit ,GecosUserInstructionFactory.Int(1)));
			IfBlock iffopen = GecosUserBlockFactory.IfThen(GecosUserInstructionFactory.eq(GecosUserInstructionFactory.symbref(file), GecosUserInstructionFactory.symbref(_snull)), then);
			bodyproc.addChildren(iffopen);
			
			Instruction callfseek = GecosUserInstructionFactory.call(_fseek, GecosUserInstructionFactory.symbref(file), GecosUserInstructionFactory.Int(0), GecosUserInstructionFactory.symbref(_sseekend));
			Instruction callftell = GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(fsize), GecosUserInstructionFactory.call(_ftell, GecosUserInstructionFactory.symbref(file)));
			Instruction callrewind = GecosUserInstructionFactory.call(_rewind, GecosUserInstructionFactory.symbref(file));
			Instruction castmalloc = GecosUserInstructionFactory.cast(buffer.getType(), GecosUserInstructionFactory.call(_malloc, GecosUserInstructionFactory.symbref(fsize)));
			Instruction callmalloc = GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(buffer), castmalloc);
			bodyproc.addChildren(GecosUserBlockFactory.BBlock(callfseek, callftell, callrewind, callmalloc));
			
			then = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.call(_printf, GecosUserInstructionFactory.string("Impossible to allocate memory to store random values")), GecosUserInstructionFactory.call(_exit, GecosUserInstructionFactory.Int(1)));
			IfBlock ifmalloc = GecosUserBlockFactory.IfThen(GecosUserInstructionFactory.eq(GecosUserInstructionFactory.symbref(buffer), GecosUserInstructionFactory.symbref(_snull)), then);
			bodyproc.addChildren(ifmalloc);
			
			Instruction callfread = GecosUserInstructionFactory.call(_fread, GecosUserInstructionFactory.symbref(buffer), GecosUserInstructionFactory.Int(1), GecosUserInstructionFactory.symbref(fsize), GecosUserInstructionFactory.symbref(file));
			SizeofTypeInstruction sizeoftype = GecosUserInstructionFactory.sizeOf(bufferType);
			Instruction setNbElems = GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(nbElems), GecosUserInstructionFactory.div(GecosUserInstructionFactory.symbref(fsize), sizeoftype));
			bodyproc.addChildren(GecosUserBlockFactory.BBlock(callfread, setNbElems));
			
			// Add the test to check if the element contained in the binary file is equal of the number of element contained to the array allocation
			_assert(dimensions.length >= 1, "An input have not index size during the reading of random value. Need to check if the value is a pointer of a base type.");
			Instruction insttemp = dimensions[0].copy();
			for(int i = 1 ; i < dimensions.length ; i++){
				insttemp = GecosUserInstructionFactory.mul(insttemp, dimensions[i].copy());				
			}
			
			Instruction cond = GecosUserInstructionFactory.ne(insttemp, GecosUserInstructionFactory.symbref(nbElems));
			then = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.call(_printf, GecosUserInstructionFactory.string("Number of random values contains in the binary files is different from the input array size")), GecosUserInstructionFactory.call(_exit, GecosUserInstructionFactory.Int(1)));
			IfBlock ifCheckSize = GecosUserBlockFactory.IfThen(cond, then);
			bodyproc.addChildren(ifCheckSize);
		
		
			// Creation of the loop allow to do the initialization of the corresponding array symbol
			BasicBlock blockInit = GecosUserBlockFactory.BBlock();  
			Block currentBlock = blockInit;
			indexes = new LinkedList<>();
			for(int i = dimensions.length - 1 ; i >= 0 ; i--){
				index = GecosUserCoreFactory.symbol("i", GecosUserTypeFactory.INT(), bodyproc.getScope());
				bodyproc.getScope().makeUnique(index);
				indexes.addFirst(index);
				BasicBlock init = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(index), GecosUserInstructionFactory.Int(0)));
				BasicBlock test = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.lt(GecosUserInstructionFactory.symbref(index), dimensions[i].copy()));
				BasicBlock step = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(index), GecosUserInstructionFactory.add(
									GecosUserInstructionFactory.symbref(index), GecosUserInstructionFactory.Int(1))));
				
				currentBlock = GecosUserBlockFactory.For(init, test, step, currentBlock);
			}
			bodyproc.addChildren(currentBlock);
	
			// Create the set instruction to add in the body of the last for loop
			List<Instruction> indexInst = new ArrayList<>(); 
			for(Symbol sym : indexes){
				indexInst.add(GecosUserInstructionFactory.symbref(sym));
			}
			Instruction current = GecosUserInstructionFactory.symbref(indexes.get(indexes.size()-1));
			Instruction temp;
			for(int i = 0 ; i < indexes.size() - 1 ; i++){
				temp = GecosUserInstructionFactory.symbref(indexes.get(i));
				for(int j = dimensions.length - 1  ; j >= (i+1) ; j--){
	
					temp = GecosUserInstructionFactory.mul(temp, dimensions[j].copy());
				}
				current = GecosUserInstructionFactory.add(current, temp);
			}
			Instruction arrInstDest = GecosUserInstructionFactory.array(input_output, indexInst);
			Instruction arrInstSource = GecosUserInstructionFactory.array(buffer, current);
			Instruction init = GecosUserInstructionFactory.set(arrInstDest, arrInstSource);
			blockInit.addInstruction(init);
			
			// need to free the buffer memory allocation
			Instruction callFree = GecosUserInstructionFactory.call(_free, GecosUserInstructionFactory.symbref(buffer));
			BasicBlock freeBlock = GecosUserBlockFactory.BBlock(callFree);
			bodyproc.addChildren(freeBlock);
		}
	}
	
	/**
	 * Add the Simulation variable and the associate call needed. Add the simulation loop.
	 * @param pMain
	 * @param body
	 * @throws BackEndException
	 */
	private void buildSimulationLoop(Procedure pMain, CompositeBlock body) throws SimulationException{	
		Symbol newSimulationManager = body.getScope().lookup("newSimulationManager");
		_assert(newSimulationManager != null && newSimulationManager instanceof ProcedureSymbol, "Simulation manager library newSimulationManager function not found in the procedure set");
				
//		Creation of the simulation manager
		Symbol manager = pMain.getScope().lookup("manager");
		_assert(manager != null, "manager not declared in the procedure set");
		
		Instruction createManager = GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(manager), GecosUserInstructionFactory.call((ProcedureSymbol) newSimulationManager));
		body.addChildren(GecosUserBlockFactory.BBlock(createManager));
		
		Symbol index = GecosUserCoreFactory.symbol("i", GecosUserTypeFactory.INT(), body.getScope());
		ForBlock forBlock = GecosUserBlockFactory.For(index , 0, _nbSimulation, 1, GecosUserBlockFactory.BBlock());
		body.getChildren().add(forBlock);
		BasicBlock bb = (BasicBlock) forBlock.getBodyBlock();
		
		List<Instruction> newParams = new LinkedList<Instruction>();
		for(Symbol param : pMain.listParameters()){	
			Symbol currentSymbol = null;
			for(Symbol s : forBlock.getScope().getSymbols()){
				if(s.getName().equals(param.getName())){
					currentSymbol = s;
					break;
				}
			}
			_assert(currentSymbol != null, "A parameters of the main function have not been found into the scope of the simulation main");
			newParams.add(GecosUserInstructionFactory.array(currentSymbol, GecosUserInstructionFactory.symbref(index)));
		}
		CallInstruction callPMain = GecosUserInstructionFactory.call(pMain.getSymbol(), newParams);
		bb.addInstruction(callPMain);			
		
		// add xml writing
		BasicBlock write = GecosUserBlockFactory.BBlock();
		Symbol writeBinary = body.getScope().lookup("SimulationManager_writeBinary");
		_assert(writeBinary != null && writeBinary instanceof ProcedureSymbol, "Simulation manager library writeBinary function not found in the procedure set");
		write.addInstruction(GecosUserInstructionFactory.call((ProcedureSymbol) writeBinary, GecosUserInstructionFactory.symbref(manager), GecosUserInstructionFactory.string(ConstantPathAndName.BACKEND_SIMU_RESULT_FODLER_NAME)));
		
		if(_debug){
			Symbol writeHuman = body.getScope().lookup("SimulationManager_writeHuman");
			_assert(writeHuman != null && writeHuman instanceof ProcedureSymbol, "Simulation manager library writeHuman function not found in the procedure set");			
			write.addInstruction(GecosUserInstructionFactory.call((ProcedureSymbol) writeHuman, GecosUserInstructionFactory.symbref(manager), GecosUserInstructionFactory.string(ConstantPathAndName.BACKEND_SIMU_RESULT_FODLER_NAME)));
		}
		
		// add delete simulation manager
		Symbol deleteManager = body.getScope().lookup("deleteSimulationManager");
		_assert(deleteManager != null || deleteManager instanceof ProcedureSymbol, "Simulation manager library deleteSimulationManager function not found in the procedure set");
		write.addInstruction(GecosUserInstructionFactory.call((ProcedureSymbol) deleteManager, GecosUserInstructionFactory.symbref(manager)));
		
		body.addChildren(write);
	}	
}
