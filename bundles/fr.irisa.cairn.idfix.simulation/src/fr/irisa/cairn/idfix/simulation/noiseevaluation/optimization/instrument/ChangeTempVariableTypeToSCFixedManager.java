package fr.irisa.cairn.idfix.simulation.noiseevaluation.optimization.instrument;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.irisa.cairn.float2fix.model.extended.factory.ExtendedTypeFactory;
import fr.irisa.cairn.gecos.model.tools.switches.SymbolSwitch;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification;
import fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Operation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.Triplet;
import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;
import gecos.core.ProcedureSet;
import gecos.core.Symbol;
import gecos.extended.types.OverflowMode;
import gecos.extended.types.QuantificationMode;
import gecos.types.Type;

/**
 * Use the information provide by the transformation step and the fixed point specification to change the type of the temporary variable representing
 * each operator contained in the data flow
 * @author nsimon
 * @date 24/02/14
 */
public class ChangeTempVariableTypeToSCFixedManager {
	private ProcedureSet _ps;
	private FixedPointSpecification _fixedPointSpecification;
	private Map<Integer, List<Symbol>> _mapSetOpToNewTempSymbol;
	private Map<Integer, List<Triplet<Symbol,Symbol,Symbol>>> _mapGenericOpToNewTempSymbol;
	private Map<Symbol, FixedPointInformation> _symbolToChange;
	
	public ChangeTempVariableTypeToSCFixedManager(ProcedureSet ps, FixedPointSpecification fixedPointSpecification, Map<Integer, List<Symbol>> mapSetOperatorToNewTemporarySymbol, Map<Integer, List<Triplet<Symbol,Symbol,Symbol>>> mapGenericOperatorToNewTemporarySymbol){
		_ps = ps;
		_fixedPointSpecification = fixedPointSpecification;
		_mapSetOpToNewTempSymbol = mapSetOperatorToNewTemporarySymbol;
		_mapGenericOpToNewTempSymbol = mapGenericOperatorToNewTemporarySymbol;
		_symbolToChange = new HashMap<Symbol, FixedPointInformation>();
	}
	
	private void myassert(boolean b, String s) throws SimulationException{
		if (!b)
			throw new SimulationException(s);
	}
	
	public void apply() throws SimulationException{
		boolean isInSetOperationMap;
		boolean isInGenericOperationMap;
			
		for(Operation operator : _fixedPointSpecification.getOperations()){
			isInSetOperationMap = _mapSetOpToNewTempSymbol.containsKey(operator.getConv_cdfg_number());
			isInGenericOperationMap = _mapGenericOpToNewTempSymbol.containsKey(operator.getConv_cdfg_number());
			myassert(!(isInSetOperationMap && isInGenericOperationMap), "The operation appear twice into the information between the numOpsCDFG and the new temporary variable due to the TACSimulation transform");
			
			if(isInGenericOperationMap){
				for(Triplet<Symbol, Symbol, Symbol> triplet : _mapGenericOpToNewTempSymbol.get(operator.getConv_cdfg_number())){
					_symbolToChange.put(triplet.getFirst(), operator.getOperands().get(2));
					_symbolToChange.put(triplet.getSecond(), operator.getOperands().get(0));
					_symbolToChange.put(triplet.getThird(), operator.getOperands().get(1));
				}
			}
			else if (isInSetOperationMap){
				for(Symbol sym : _mapSetOpToNewTempSymbol.get(operator.getConv_cdfg_number())){
					_symbolToChange.put(sym, operator.getOperands().get(2));
				}
			}
			else {
				throw new SimulationException("The operation don't appear into the information between the numOpsCDFG and the new temporary variable due to the TACSimulation transformation");
			}
		}
		
		new _SymbolChangeTypeSwitch().doSwitch(_ps);
	}
	
	private class _SymbolChangeTypeSwitch extends SymbolSwitch<Object>{
		@Override
		public Object caseSymbol(Symbol sym){
			Type newType;
			ExtendedTypeFactory.setScope(sym.getContainingScope());
			if(_symbolToChange.containsKey(sym) && sym.getAnnotation("CONST") == null){
				if(_symbolToChange.get(sym).isSigned()){
					newType = ExtendedTypeFactory.SCFIXED(_symbolToChange.get(sym).getBitWidth(), _symbolToChange.get(sym).getIntegerWidth(), resolveQuantification(_symbolToChange.get(sym).getQuantification()),  resolveOverflow(_symbolToChange.get(sym).getOverflow()));
					
				}
				else{
					newType = ExtendedTypeFactory.SCUFIXED(_symbolToChange.get(sym).getBitWidth(), _symbolToChange.get(sym).getIntegerWidth(), resolveQuantification(_symbolToChange.get(sym).getQuantification()),  resolveOverflow(_symbolToChange.get(sym).getOverflow()));
				}
				sym.setType(newType);
			}
			return null;
		}
	}
	
	private QuantificationMode resolveQuantification(QUANTIFICATION_TYPE quantification){
		switch (quantification) {
		case TRUNCATION:
			return QuantificationMode.SC_TRN;
			
		case ROUNDING:
			return QuantificationMode.SC_RND;
		default:
			throw new RuntimeException("Quantification kind " + quantification + "not managed yet in the idfix backend");
		}
	}
	
	private OverflowMode resolveOverflow(OVERFLOW_TYPE overflow){
		switch (overflow) {
		case SATURATION:
			return OverflowMode.SC_SAT;
		case WRAP:
			return OverflowMode.SC_WRAP;
			
		default:
			throw new RuntimeException("Overflow kind " + overflow + "not managed yet in the idfix backend");
		}
	}
}
