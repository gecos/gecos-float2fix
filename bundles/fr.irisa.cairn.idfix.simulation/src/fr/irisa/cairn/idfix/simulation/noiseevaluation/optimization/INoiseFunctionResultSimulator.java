package fr.irisa.cairn.idfix.simulation.noiseevaluation.optimization;


/**
 * Interface of the optimization simulator.
 * This kind of simulator check the validity of the noise function result in relation to the space solution provide by the optimization algorithm
 * @author nsimon
 * @date 11/04/2014
 *
 */
public interface INoiseFunctionResultSimulator {

	public void initializeSimulation();
	
	public void finalizeSimulation(int numberCallNoiseFunction);
	
	public void runFixedSimulation(double noise);
	
	public boolean isEachCallNoiseFunction();
	
	public boolean lastSimulationSuccessful();
	
	public int getSimulationFrequencies();
	
	public void printInformation();	
}
