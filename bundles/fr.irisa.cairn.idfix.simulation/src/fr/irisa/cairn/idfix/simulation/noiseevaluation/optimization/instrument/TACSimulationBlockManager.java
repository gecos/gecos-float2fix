package fr.irisa.cairn.idfix.simulation.noiseevaluation.optimization.instrument;

import java.util.LinkedList;
import java.util.List;

import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.tools.switches.BasicBlockSwitch;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Scope;
import gecos.instrs.Instruction;

public class TACSimulationBlockManager extends BasicBlockSwitch<Object>{
	private TACSimulationProcManager _procManager;
	private Scope _newScope; // New scope which contains the new symbol creating during computing of basic block instructions
	
	public TACSimulationBlockManager(TACSimulationProcManager procManager){
		_procManager = procManager;
	}
		
	public Scope getNewScope(){
		return _newScope;
	}

	@Override
	public Object caseBasicBlock(BasicBlock b) {
		_newScope = GecosUserCoreFactory.scope();
		for(Instruction inst : b.getInstructions()){
			_procManager.doSwitch(inst);
		}
		
		for(Instruction key : _procManager.getInstManager().getMap().keySet()){
			int pos = b.getInstructions().indexOf(key);
			if(pos >= 0){
				b.getInstructions().addAll(pos, _procManager.getInstManager().getMap().get(key));
			}
			else{
				throw new RuntimeException("The last instruction representing the instruction to transform is not present into the current basic block");
			}
		}

		_procManager.getInstManager().getMap().clear();
		
		return null;
	}
	
	@Override
	public Object caseCompositeBlock(CompositeBlock b){
		List<Scope> saveScope = new LinkedList<Scope>();
		for(Block block : b.getChildren()){
			doSwitch(block);
			if(block instanceof BasicBlock){
				saveScope.add(_newScope);
			}
		}
		
		for(Scope scope : saveScope)
			b.getScope().mergeWith(scope);
		
		return null;
	}

	@Override
	public Object caseIfBlock(IfBlock b){
		if(b.getThenBlock() != null)
			doSwitch(b.getThenBlock());
		if(b.getElseBlock() != null)
			doSwitch(b.getElseBlock());
		return null;
	}
	
	@Override
	public Object caseForBlock(ForBlock b){
		if(b.getBodyBlock() != null)
			doSwitch(b.getBodyBlock());
		return null;
	}
	
	@Override
	public Object caseWhileBlock(WhileBlock b){
		if(b.getBodyBlock() != null)
			doSwitch(b.getBodyBlock());
		return null;
	}
	
	@Override
	public Object caseDoWhileBlock(DoWhileBlock b){
		if(b.getBodyBlock() != null)
			doSwitch(b.getBodyBlock());
		return null;
	}
	
}
