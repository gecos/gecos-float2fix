package fr.irisa.cairn.idfix.simulation.noiseevaluation.instrument;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.apache.logging.log4j.Logger;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.IDFixUtils;
import fr.irisa.cairn.idfix.utils.exceptions.BackEndException;
import fr.irisa.cairn.idfix.utils.exceptions.SFGModelCreationException;
import fr.irisa.cairn.idfix.utils.logging.IDFixLogger;
import fr.irisa.cairn.tools.ecore.EcoreTools;
import gecos.blocks.CompositeBlock;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.ScopeContainer;
import gecos.core.Symbol;
import gecos.instrs.CallInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.FunctionType;
import gecos.types.Type;

/**
 * Regroup all the function which work on the variable annotated with the dynamic pragma.
 * @author Nicolas Simon
 * @date Nov 6, 2013
 */
public class InputTransformationManager {
	private ProcedureSet _ps;
	private Stack<Procedure> _procStack = new Stack<>();
	List<Procedure> _listp = new LinkedList<>();
	
	private final Logger _logger = IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_OPTIMIZATION);

	public InputTransformationManager(ProcedureSet ps){
		_ps = ps;
	}

	/**
	 * Up into the main function as parameters all the variable annotated with the dynamic pragma
	 */
	public void upInputToMainProcedureParameters(){
		Procedure pmain = IDFixUtils.getMainProcedure(_ps);
		_procStack.add(pmain);
		new _FunctionCallOrder().doSwitch(pmain);
		Procedure p;
		while(!_procStack.isEmpty()){
			p = _procStack.pop();
			new _InputUpToParameters().doSwitch(p);
			new _UpdateCallInstruction(p).doSwitch(_ps);
		}
		
		try{
			for(Procedure pr : _listp){
				for(Symbol s : pr.listParameters()){
					if(IDFixUtils.isInputVar(s)){
						s.getAnnotations().clear();	
					}
				}
			}
		} catch (SFGModelCreationException e) {
			_logger.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Create a stack which have the call function order information
	 * @author Nicolas Simon
	 * @date Nov 6, 2013
	 */
	private class _FunctionCallOrder extends BlockInstructionSwitch<Object> {
		private boolean _alreadyAddMath = false;
		public Object caseCallInstruction(CallInstruction call){
			ProcedureSymbol sym = (ProcedureSymbol) call.getProcedureSymbol();
			if(!sym.getName().equals("sin") && !sym.getName().equals("cos") && !sym.getName().equals("log2")){
				_procStack.add(sym.getProcedure());
				_listp.add(sym.getProcedure());
				doSwitch(sym.getProcedure());
			}
			else{
				if(!_alreadyAddMath){
					_alreadyAddMath = true;
					GecosUserAnnotationFactory.pragma(_ps, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION+"#include <math.h>");
				}
			}
			return null;
		}
	}

	/**
	 * Up into the parameters of the procedure all the variable contained in scopes which have dynamic pragma 
	 * @author Nicolas Simon
	 * @date Nov 6, 2013
	 */
	private class _InputUpToParameters extends BlockInstructionSwitch<Object> {
		private Map<Symbol, ParameterSymbol> _symbolToParamSymbol = new HashMap<>();
		
		@Override
		public Object caseScopeContainer(ScopeContainer sc){
			try{
				if(!(sc instanceof CompositeBlock))
					throw new BackEndException("Computation to a scope not contained into a composite block");
				
				CompositeBlock cb = (CompositeBlock) sc;				
				for(Symbol s : cb.getScope().getSymbols()){
					if(IDFixUtils.isInputVar(s)){
						Procedure p = cb.getContainingProcedure();
						
						// Create parameter symbol and copy annotation into it
						ParameterSymbol param = GecosUserCoreFactory.paramSymbol(s.getName(), s.getType());
						p.getScope().getSymbols().add(param);
						for(String key : s.getAnnotations().keySet())
							param.setAnnotation(key, EcoreTools.copy(s.getAnnotation(key)));
												
						// Create a new function type with the new parameter type
						FunctionType funcType = (FunctionType) p.getSymbol().getType();
						GecosUserTypeFactory.setScope(funcType.getContainingScope());
						List<Type> newParams = new ArrayList<>();
						for(Type type : funcType.listParameters())
							newParams.add(type);
						newParams.add(s.getType());
						FunctionType newFuncType = GecosUserTypeFactory.FUNCTION(funcType.getReturnType(), false, newParams);
						p.getSymbol().setType(newFuncType);
						_symbolToParamSymbol.put(s, param);
					}					
				}
				
				// Delete old symbol
				for(Symbol s : _symbolToParamSymbol.keySet()){
					cb.getScope().getSymbols().remove(s);
				}
			} catch (SFGModelCreationException | BackEndException e){
				_logger.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
			return null;
		}

		@Override
		public Object caseSymbolInstruction(SymbolInstruction object) {
			if(_symbolToParamSymbol.containsKey(object.getSymbol()))
				object.setSymbol(_symbolToParamSymbol.get(object.getSymbol()));
			return null;
		}
	}
	
	/**
	 * Update all call instruction with p as addressee in the procedure set. 
	 * @author Nicolas Simon
	 * @date Nov 6, 2013
	 */
	private class _UpdateCallInstruction extends BlockInstructionSwitch<Object> {
		private Procedure _p;
		
		public _UpdateCallInstruction(Procedure p){
			_p = p;
		}
		
		@Override
		public Object caseCallInstruction(CallInstruction call){
			try{
				ProcedureSymbol procSym = (ProcedureSymbol) call.getProcedureSymbol();
				if(procSym == _p.getSymbol()){
					if(call.getChildrenCount() - 1 < _p.listParameters().size()){
						Scope scope = call.getBasicBlock().getScope();
						for(int i = call.getChildrenCount() - 1 ; i < _p.listParameters().size() ; i++ ){		
							Symbol param = _p.listParameters().get(i);
							if(!IDFixUtils.isInputVar(param))
								throw new BackEndException("An input up to parameters is not annotated as an input");
							
							// Create new symbol and copy annotation
							Symbol sym = GecosUserCoreFactory.symbol(param.getName(), param.getType());
							scope.getSymbols().add(sym);
							for(String key : param.getAnnotations().keySet())
								sym.setAnnotation(key, EcoreTools.copy(param.getAnnotation(key)));
							call.addArg(GecosUserInstructionFactory.symbref(sym));	
						}
					}
					else{
						throw new BackEndException("Call instruction have too many parameters to add a new one due to the input transformation apply during the simulation");					
					}
				}
			} catch (SFGModelCreationException | BackEndException e){
				_logger.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
			return null;
		}
	}
}