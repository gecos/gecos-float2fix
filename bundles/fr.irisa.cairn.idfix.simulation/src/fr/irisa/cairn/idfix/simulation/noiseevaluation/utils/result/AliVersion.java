package fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.result;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;

/**
 * Method used by Ali to compute noise power
 * @author nicolas
 * @date 17/02/2015
 */
public class AliVersion extends DefaultResultManager {
	private double _noisePowerMean, _varianceMean, _meanMean;
	private double powerMax = Double.NEGATIVE_INFINITY;
	private double _diffMax = Double.NEGATIVE_INFINITY;
	
	public AliVersion(String pathFloatResultFolder,
			String pathFixedResultFolder, double analyticNoiseSolution, int numberOfSimulation) {
		super(pathFloatResultFolder, pathFixedResultFolder, analyticNoiseSolution, numberOfSimulation);
	}

	@Override
	protected double computeNoisePower() throws SimulationException {
		if(_floatValues.size() != _fixedValues.size())
			throw new SimulationException("The number of variable profiled in the float simulation is different from the number of variable profiled in the fixed simulation");
		
		List<Double> floatValues, fixedValues;
		double noisepowersum = 0, varsum = 0, meansum = 0;
		for(int i = 0 ; i < _numberOfSimulation ; i++){
			floatValues = getFloatListSample(_floatValues, i);
			fixedValues = getFloatListSample(_fixedValues, i);
			
			ErrorStatsCalculator c = new ErrorStatsCalculator(floatValues, fixedValues);
			c.compute();
			
			//FIXME noisepowersum accumulation = -Infinity due to owerflow
			noisepowersum += c.noisePowerdB;
			varsum += c.variance;
			meansum += c.meanT;
			
			powerMax = powerMax < c.noisePowerdB?c.noisePowerdB:powerMax;
			_diffMax = _diffMax < c.diffMax?c.diffMax:_diffMax;
		}
		
		_noisePowerMean = noisepowersum/_numberOfSimulation;
		_varianceMean = varsum/_numberOfSimulation;
		_meanMean = meansum/_numberOfSimulation;
		
		return _noisePowerMean;
	}
	
	@Override
	public void display(String title, PrintWriter writer) {
		writer.println("---------------------------------------- " + title + " ----------------------------------------");
		writer.println("    Number of simulation : " + _numberOfSimulation);
		writer.println("    Analytic solution noise power (db) : " + _analyticNoiseSolution);
		writer.println("    Simulation Solution noise power mean (db) : " + _noisePowerMean);
		writer.println("    Error mean : " + _meanMean);
		writer.println("    Error variance mean : " + _varianceMean);
		writer.println("    Error (max) : " + _diffMax + "\n");
		if(_analyticNoiseSolution != Double.NaN)
			writer.println("    Difference analytic/simulation (db) : " + (_analyticNoiseSolution - _noisePowerMean));	
	}

	@Override
	public void display(String title, Logger logger) {
		logger.info("---------------------------------------- " + title + " ----------------------------------------");
		logger.info("    Number of simulation : " + _numberOfSimulation);
		logger.info("    Analytic solution noise power (db) : " + _analyticNoiseSolution);
		logger.info("    Simulation Solution noise power mean (db) : " + _noisePowerMean);
		logger.info("    Error mean : " + _meanMean);
		logger.info("    Error variance mean : " + _varianceMean);
		logger.info("    Error (max) : " + _diffMax + "\n");
		if(_analyticNoiseSolution != Double.NaN)
			logger.info("    Difference analytic/simulation (db) : " + (_analyticNoiseSolution - _noisePowerMean));	
	}
	
	private List<Double> getFloatListSample(Map<String, double[]> map, int i){
		List<Double> ret = new ArrayList<Double>();
		
		for(String key : map.keySet()){
			ret.add(map.get(key)[i]);
		}
		
		return ret;
	}
}
