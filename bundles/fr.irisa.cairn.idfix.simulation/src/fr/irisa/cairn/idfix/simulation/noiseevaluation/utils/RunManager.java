package fr.irisa.cairn.idfix.simulation.noiseevaluation.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;

import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.ListenProcessInputStandardStream;
import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;
import fr.irisa.cairn.idfix.utils.logging.IDFixLogger;

public class RunManager {
	private String _pathFloatLibrary;
	private String _pathFixedLibrary;
	
	private int _nbFixedSimulationRunned = 0;
	
	private final Logger _logger = IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_OPTIMIZATION);
	
	public RunManager(String pathFloatLibrary, String pathFixedLibrary){
		_pathFloatLibrary = pathFloatLibrary;
		_pathFixedLibrary = pathFixedLibrary;
	}
	
	FilenameFilter _resultFilter = new FilenameFilter() {
		@Override
		public boolean accept(File dir, String name) {
			String lowercaseName = name.toLowerCase();
			if(lowercaseName.endsWith(".bin"))
				return true;
			else
				return false;
		}
	};
	
	public int getNumberOfFixedSimulationRunned(){
		return _nbFixedSimulationRunned;
	}

	public void runFloatLibrary() throws SimulationException{
		run(_pathFloatLibrary, ConstantPathAndName.SIMU_MANAGER_FLOAT_BIN_NAME);
	}
	
	public void runFixedLibrary() throws SimulationException{
		run(_pathFixedLibrary, ConstantPathAndName.SIMU_MANAGER_FIXED_BIN_NAME);
		_nbFixedSimulationRunned++;
	}
	
	private void run(String folder, String binaryName) throws SimulationException{
		if(!(new File(folder + binaryName).isFile()))
			throw new SimulationException("Binary to run not found or is not a file");
		
		List<String> cmd = new LinkedList<>();
		cmd.add("./"+binaryName);
		
		ProcessBuilder pb = new ProcessBuilder(cmd);
		pb.directory(new File(folder));
		try {
			Process p = pb.start();
			BufferedReader errReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			ListenProcessInputStandardStream outReader = new ListenProcessInputStandardStream(p.getInputStream(), _logger, Level.INFO);
			outReader.start();
			
			String line;
			StringBuffer bufLine = new StringBuffer();
			while((line = errReader.readLine()) != null){
				bufLine.append(line).append("\n");
			}
			p.waitFor();
			outReader.join();
			
			File file = new File(folder + ConstantPathAndName.BACKEND_SIMU_RESULT_FODLER_NAME);
			if(bufLine.length() != 0 && file.listFiles(_resultFilter).length == 0){
				_logger.debug(bufLine.toString());
				throw new SimulationException("Computation of " + binaryName + " failed");
			}
		} catch (IOException | InterruptedException e) {
			throw new SimulationException(e);
		}	
	}
}
