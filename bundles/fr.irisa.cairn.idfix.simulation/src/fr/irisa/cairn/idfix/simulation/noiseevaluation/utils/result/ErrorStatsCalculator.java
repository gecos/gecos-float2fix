package fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.result;

import java.util.List;

public class ErrorStatsCalculator {
	
	public double meanT;
	public double variance;
	public double deviationT;
	public double noisePowerdB;
	public double diffMax;

	private double[] floatValues, fixedValues;
	
	public ErrorStatsCalculator(List<Double> refValues ,List<Double> values){
		this(refValues.stream().mapToDouble(Double::doubleValue).toArray(), 
				values.stream().mapToDouble(Double::doubleValue).toArray());
	}
	
	public ErrorStatsCalculator(double[] refValues ,double[] values){
		this.floatValues = refValues;
		this.fixedValues = values;
	}
	
	public void compute(){
		double sumdiff = 0;
		double sumdiff2 = 0;
		double diff;
		int i;
		
		diffMax = 0;
		for (i = 0; i < floatValues.length ; i++) {
			double a2 = floatValues[i] - fixedValues[i];
			diff = (a2 >= 0 ? a2 : -a2);

			if (diff > diffMax)
				diffMax = diff;
			if (diff != 0) {
				sumdiff += diff;
				sumdiff2 += diff * diff;
			}
		}
		double nelem = floatValues.length;
		
		/*
		 * mean = sum(diff)/nelem
		 * variance = sum((diff - mean)^2) / nelem = sum(diff^2/nelem) + sum(mean^2/nelem) -2.mean.sum(diff/nelem)
		 * = sumdiff2/nelem + mean^2 -2.mean^2 = sumdiff2/nelem - mean^2
		 * deviation = sqrt(variance)
		 * Noise Power (dB) = 10.log10(deviation^2 + mean^2) = 10.log10(variance + mean^2) = 10.log10(sumdiff2/nelem)
		 */
		meanT = sumdiff / nelem;
		variance = (sumdiff2 / nelem) - (meanT * meanT);
		deviationT = Math.sqrt(variance);
		noisePowerdB = 10 * Math.log10(sumdiff2 / nelem);
	}

	/**
	 * @return mean error as: sum(error) / Total number of elements. Where error = refValue - value. 
	 */
	public double getMeanError() {
		return meanT;
	}

	public double getVariance() {
		return variance;
	}

	public double getDeviationT() {
		return deviationT;
	}

	public double getNoisePowerdB() {
		return noisePowerdB;
	}

	public double getMaxAbsoluteError() {
		return diffMax;
	}

	public String generateReport() {
		StringBuilder sb = new StringBuilder();
		sb.append("\tNoise Pow= ").append(getNoisePowerdB()).append(" dB \n");
		sb.append("\tVariance = ").append(getVariance()    ).append("\n");
		sb.append("\tNb elem  = ").append(floatValues.length).append("\n");
		return sb.toString();
	}
	
}