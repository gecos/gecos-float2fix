package fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.result;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;

public class WorstVersion extends DefaultResultManager {
	private double _maxNoisePowerDb = Double.NEGATIVE_INFINITY, _maxVariance, _maxMean, _maxDiff = Double.NEGATIVE_INFINITY;
	private Map<String, Double> _noisePowersDb = new HashMap<>();
	private Map<String, Double> _variances = new HashMap<>();
	private Map<String, Double> _means = new HashMap<>();
	private Map<String, Double> _noisePowers = new HashMap<>();
	
	public WorstVersion(String pathFloatResultFolder,
			String pathFixedResultFolder, double analyticNoiseSolution, int numberOfSimulation) {
		super(pathFloatResultFolder, pathFixedResultFolder, analyticNoiseSolution, numberOfSimulation);
	}
	
	private static final void _assert(boolean b, String msg) throws SimulationException 
	{
		if(!b) throw new SimulationException(msg);
	}

	@Override
	protected double computeNoisePower() throws SimulationException {
		if(_floatValues.size() != _fixedValues.size())
			throw new SimulationException("The number of variable profiled in the float simulation is different from the number of variable profiled in the fixed simulation");
		
		double[] floatvalues, fixedvalues;
		for(String key : _floatValues.keySet()){
			floatvalues = _floatValues.get(key);
			if(!_fixedValues.containsKey(key))
				throw new SimulationException("Variable profiled in the float simulation is not present in the fixed simulation");			
			fixedvalues = _fixedValues.get(key);
			
			ComputeOneVariable oneOutput = new ComputeOneVariable(floatvalues, fixedvalues);
			oneOutput.compute();
			
			_noisePowersDb.put(key, oneOutput._noisePowerDb);
			_noisePowers.put(key, oneOutput._noisePower);
			_variances.put(key, oneOutput._variance);
			_means.put(key, oneOutput._meanT);
			
			if(oneOutput._noisePowerDb > _maxNoisePowerDb){
				_maxNoisePowerDb = oneOutput._noisePowerDb;
				_maxVariance = oneOutput._variance;
				_maxMean = oneOutput._meanT;
			}
			
			if(oneOutput._maxDiff > _maxDiff)
				_maxDiff = oneOutput._maxDiff;
		}
		
		_assert(_noisePowers.size() == _variances.size() && _noisePowers.size() == _means.size(), "Number of noise powers, variances and means are not equals");		
		return _maxNoisePowerDb;
	}
	
	private class ComputeOneVariable{
		private double[] _floatValues;
		private double[] _fixedValues;
		
		public double _meanT, _variance, _noisePowerDb, _noisePower, _maxDiff = Double.NEGATIVE_INFINITY;
		
		public ComputeOneVariable(double[] floatValues, double[] fixedValues){
			_floatValues = floatValues;
			_fixedValues = fixedValues;
		}
		
		public void compute() throws SimulationException{
			_assert(_floatValues.length == _fixedValues.length, "Number of sample save for a float variable are not equal for the same fixed variable");
			double diff;
			double sumdiff = 0, sumdiff2 = 0;	
			/*
			* mean = sum(diff)/nelem
			* variance = sum((diff - mean)^2) / nelem = sum(diff^2/nelem) + sum(mean^2/nelem) -2.mean.sum(diff/nelem)
			* = sumdiff2/nelem + mean^2 -2.mean^2 = sumdiff2/nelem - mean^2
			* deviation = sqrt(variance)
			* Noise Power (dB) = 10.log10(deviation^2 + mean^2) = 10.log10(variance + mean^2) = 10.log10(sumdiff2/nelem)
			*/
			for(int i = 0 ; i < _floatValues.length ; i++){
				double temp = _floatValues[i] - _fixedValues[i];
				diff = temp;// >= 0?temp:-temp;
				double tmp = Math.abs(diff);
				if(tmp > _maxDiff)
					_maxDiff = tmp;
					
				sumdiff += diff;
				sumdiff2 += diff*diff;				
			}
			int nelem = _floatValues.length;
			
			_meanT = sumdiff / nelem;
			_variance = (sumdiff2/nelem) - (_meanT * _meanT);
			_noisePower = sumdiff2/nelem;
			_noisePowerDb = 10 * Math.log10(_noisePower);
		}
	}
	
	@Override
	public void display(String title, PrintWriter writer) {
		writer.println("----------------------- " + title + " (Worst result manager) -----------------------");
		writer.println("    Number of simulation : " + _numberOfSimulation);
		writer.println("    Analytic solution noise power (db) : " + _analyticNoiseSolution);
		writer.println("    Simulation Solution noise power (db) : " + _maxNoisePowerDb);
		writer.println("    Simulation Solution error mean   : " + _maxMean);
		writer.println("    Simulation Solution error variance (db) : " + _maxVariance);
		writer.println("    Simulation max difference : " + _maxDiff);
		if(_analyticNoiseSolution != Double.NaN)
			writer.println("    Difference analytic/simulation (db) : " + (_analyticNoiseSolution - _maxNoisePowerDb));
	}
	
	@Override
	public void display(String title, Logger logger) {
		logger.info("----------------------- " + title + " (Worst result manager) -----------------------");
		logger.info("    Number of simulation : " + _numberOfSimulation);
		logger.info("    Analytic solution noise power (db) : " + _analyticNoiseSolution);
		logger.info("    Simulation Solution noise power (db) : " + _maxNoisePowerDb);
		logger.info("    Simulation Solution error mean   : " + _maxMean);
		logger.info("    Simulation Solution error variance (db) : " + _maxVariance);
		logger.info("    Simulation max difference : " + _maxDiff);
		if(_analyticNoiseSolution != Double.NaN)
			logger.info("    Difference analytic/simulation (db) : " + (_analyticNoiseSolution - _maxNoisePowerDb));	

		StringBuffer strBuf = new StringBuffer();
		strBuf.append("\n");
		for(String key : _noisePowersDb.keySet()){
			strBuf.append(key + ": Noise power = " + _noisePowersDb.get(key) + " Mean = " + _means.get(key) + " Variance = " + _variances.get(key) + "\n");
		}
		_logger.debug(strBuf.toString());
	}
}