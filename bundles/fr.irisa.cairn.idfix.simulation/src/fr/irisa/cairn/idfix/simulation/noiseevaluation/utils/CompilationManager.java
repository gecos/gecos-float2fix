package fr.irisa.cairn.idfix.simulation.noiseevaluation.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;

import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.EnvironmentInformations;
import fr.irisa.cairn.idfix.utils.ListenProcessInputStandardStream;
import fr.irisa.cairn.idfix.utils.exceptions.EnvironmentSetupException;
import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;


/**
 * Module which manage the compilation of the simulation source file generated.
 * The version parameters allow to adapt option of the compiler to compile float or fixed version
 * @author nsimon
 * @date 31/03/14
 *
 */
public class CompilationManager {
	public static enum Version {FIXED, FLOAT, PREQUANTIFIED};
	
	private static final void _assert(boolean b, String msg) throws SimulationException 
	{
		if(!b) throw new SimulationException(msg);
	}
	
	/**
	 * Compile the source file given and generate the library in the folder given as parameters
	 * Release mode -> O3 Wall Wno-unknown-pragmas
	 * Debug mode -> g Wall
	 * @throws SimulationException
	 */
	public static void compile(String fileName, String folderPath, Version version, Logger logger, boolean debug) throws SimulationException{
		String _folderPath = folderPath.endsWith("/")?folderPath:folderPath+"/";
		if(!(new File(_folderPath + fileName).isFile()))
			throw new SimulationException("file to compile not found at " + _folderPath + fileName);
		
		String cSimuManagerHeader, cSimuManagerLib, systemcHeader = null, systemcLib = null;
		try{
			cSimuManagerHeader = EnvironmentInformations.getEnv(EnvironmentInformations.KEY_SIMUMANAGER_INC);
			cSimuManagerLib = EnvironmentInformations.getEnv(EnvironmentInformations.KEY_SIMUMANAGER);
			_assert(cSimuManagerHeader != null && cSimuManagerLib != null, "Simulation Manager library informations is missing in the configuration file");
			
			if(version == Version.FIXED || version == Version.PREQUANTIFIED){
				systemcHeader = EnvironmentInformations.getEnv(EnvironmentInformations.KEY_SYSTEMC_INC);
				systemcLib = EnvironmentInformations.getEnv(EnvironmentInformations.KEY_SYSTEMC);
				_assert(systemcHeader != null && systemcLib != null, "SystemC library informations is missing in the configuration file");
			}
		} catch (EnvironmentSetupException e){
			throw new SimulationException(e);
		}
		
		List<String> cmd = new LinkedList<String>();
		cmd.add("g++");
		if(debug){
			cmd.add("-g");
			cmd.add("-Wall");
		}
		else{
			cmd.add("-O3");
			cmd.add("-Wall");
			cmd.add("-Wno-unknown-pragmas");
		}		
		
		cmd.add("-I" + cSimuManagerHeader);
		cmd.add("-L" + cSimuManagerLib);
		if(version == Version.FIXED || version == Version.PREQUANTIFIED){
			cmd.add("-I" + systemcHeader);
			cmd.add("-L" + systemcLib);
		}
		
		cmd.add(fileName);
		cmd.add("-o");
		if(version == Version.FIXED)
			cmd.add(ConstantPathAndName.SIMU_MANAGER_FIXED_BIN_NAME);
		else
			cmd.add(ConstantPathAndName.SIMU_MANAGER_FLOAT_BIN_NAME);

		cmd.add("-lsimulationmanager");
		cmd.add("-Wl,-rpath=" + cSimuManagerLib);
		if(version == Version.FIXED || version == Version.PREQUANTIFIED){
			cmd.add("-lsystemc");
			cmd.add("-Wl,-rpath=" + systemcLib);
		}

		logger.debug("Command: " + cmd);
		
		ProcessBuilder pb = new ProcessBuilder(cmd);
		pb.directory(new File(_folderPath));
		try{
			Process p = pb.start();
			BufferedReader errReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			ListenProcessInputStandardStream outReader = new ListenProcessInputStandardStream(p.getInputStream(), logger, Level.INFO);
			outReader.start();
			
			String line;
			StringBuffer bufLine = new StringBuffer();
			while((line = errReader.readLine()) != null){
				bufLine.append(line).append("\n");
			}
			p.waitFor();
			outReader.join();
			if(bufLine.length() != 0 ){
				if(debug)
					logger.debug(bufLine.toString());
				if(version == Version.FIXED){
					if(!new File(_folderPath + ConstantPathAndName.SIMU_MANAGER_FIXED_BIN_NAME).isFile()){
						logger.debug(bufLine.toString());
						throw new SimulationException("Compilation of the fixed simulation library failed");
					}
				}
				else{
					if(!new File(_folderPath + ConstantPathAndName.SIMU_MANAGER_FLOAT_BIN_NAME).isFile()){
						logger.debug(bufLine.toString());
						throw new SimulationException("Compilation of the float simulation library failed");
					}
				}
			}
		} catch(IOException | InterruptedException e){
			throw new SimulationException(e);
		}
	}
}
