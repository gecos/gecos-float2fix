package fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.result;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.DoubleBuffer;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;
import fr.irisa.cairn.idfix.utils.logging.IDFixLogger;

/**
 * Abstract class which to extend to implement a simulation result manager.
 * This abstract class read the result file and provide data structure.
 * @author nicolas
 * 25/02/15
 */
public abstract class DefaultResultManager implements IResultManager {
	protected abstract double computeNoisePower() throws SimulationException;
	
	private static final short _SCALAR_BINARY = 0;
	private static final short _ARRAY_BINARY = 1;
	
	protected String _pathFloatResultFolder, _pathFixedResultFolder;
	protected double _analyticNoiseSolution;
	protected int _numberOfSimulation;
	
	protected Map<String, double[]> _floatValues, _fixedValues;
	protected Logger _logger = IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_SIMULATION_BACKEND);
	
	private static final void _assert(boolean b, String msg) throws SimulationException 
	{
		if(!b) throw new SimulationException(msg);
	}

	protected DefaultResultManager(String pathFloatResultFolder, String pathFixedResultFolder, double analyticNoiseSolution, int numberOfSimulation) {
		_pathFloatResultFolder = pathFloatResultFolder;
		_pathFixedResultFolder = pathFixedResultFolder;
		_analyticNoiseSolution = analyticNoiseSolution;
		_numberOfSimulation = numberOfSimulation;
		_floatValues = new HashMap<>();
		_fixedValues = new HashMap<>();
	}
	
	@Override
	public double process() throws SimulationException {
		buildValuesMap(_pathFloatResultFolder, _floatValues);
		buildValuesMap(_pathFixedResultFolder, _fixedValues);
		
		return computeNoisePower();
	}
	
	private class _BinaryResultFilenameFilter implements FilenameFilter {
		@Override
		public boolean accept(File dir, String name) {
			return name.endsWith(".bin");
		}
	}
	
	/**
	 * Result binary :
	 * ProfilingNumber(Integer) VariableKind(Short) 
	 * if VariableKind = 0 (Scalar variable)
	 * 		NumberOfValue(Integer) N*value(Double)
	 * else VariableKind = 1 (Array variable)
	 * 		NumberOfIndex(Integer) For each index - IndexNumber(Integer) NumbeOfValue(Integer) N*value(Double)
	 * @param folderPath
	 * @param values
	 * @throws SimulationException
	 */
	private void buildValuesMap(String folderPath, Map<String, double[]> values) throws SimulationException{
		int profiling_number, variable_kind;
		File ffolder = new File(folderPath);
		_assert(ffolder.isDirectory(), folderPath + " not found or is not a directory");
		
		DataInputStream instream;
		try{
			for(File file : ffolder.listFiles(new _BinaryResultFilenameFilter())){
				_assert(file.isFile(), file.getAbsolutePath() + "not found or is not a file");		
				String[] splitName = file.getName().split("_");
				String variableName = splitName[0];
				
				instream = new DataInputStream(new FileInputStream(file));
				profiling_number = readNumberProfiling(instream);
				switch (variable_kind = readVariableKind(instream)) {
				case _SCALAR_BINARY:
					buildValuesMapFromScalarBinary(variableName, profiling_number, instream, values);
					break;

				case _ARRAY_BINARY:
					buildValuesMapFromArrayBinary(variableName, profiling_number, instream, values);
					break;
					
				default:
					instream.close();
					_logger.debug("File path : " + file.getAbsolutePath() + "\n" + "Kind of variable : " + variable_kind);
					throw new SimulationException("Kind of variable read in the binary file " + file.getName() + " represent a unknown kind of variable"); 
				}
				
				instream.close();
			}
		} catch (IOException e){
			throw new SimulationException(e);
		}
	}
	
	/**
	 * Read the first information (integer value) which represent the profiling number
	 * @param instream
	 * @return the value (integer) of the first information contained in the binary
	 * @throws IOException
	 */
	private int readNumberProfiling(DataInputStream instream) throws IOException{
		ByteBuffer intBuffer = ByteBuffer.allocate(Integer.SIZE/Byte.SIZE).order(ByteOrder.nativeOrder());
		instream.read(intBuffer.array());
		return intBuffer.getInt();
	}
	
	/**
	 * Read the second information (short value) which represent the kind of storage/variable used in the binary
	 * @param instream
	 * @return the value (short) of the second information contained in the binary
	 * @throws IOException
	 */
	private short readVariableKind(DataInputStream instream) throws IOException{
		ByteBuffer shortBuffer = ByteBuffer.allocate(Short.SIZE/Byte.SIZE).order(ByteOrder.nativeOrder());
		instream.read(shortBuffer.array());
		return shortBuffer.getShort();
	}
	
	private void buildValuesMapFromScalarBinary(String variableName, int profiling_number, DataInputStream instream, Map<String, double[]> values) throws IOException, SimulationException{
		ByteBuffer intBuffer = ByteBuffer.allocate(Integer.SIZE/Byte.SIZE).order(ByteOrder.nativeOrder());
		instream.read(intBuffer.array());
		int number_value = intBuffer.getInt();
		
		ByteBuffer byteBuffer = ByteBuffer.allocate(Double.SIZE/Byte.SIZE * number_value).order(ByteOrder.nativeOrder());
		instream.read(byteBuffer.array());
		DoubleBuffer doubleBuffer = byteBuffer.asDoubleBuffer();
		if(_numberOfSimulation != doubleBuffer.remaining()){
			throw new SimulationException("The number of value saved for the scalar data with the profiling number " + profiling_number + " is not equal to the simulation sample number");
		}
			
		double[] dvalues = new double[_numberOfSimulation];
		doubleBuffer.get(dvalues);
		
		values.put(variableName + '_' + Integer.toString(profiling_number), dvalues);		
	}

	private void buildValuesMapFromArrayBinary(String variableName, int profiling_number, DataInputStream instream, Map<String, double[]> values) throws IOException, SimulationException{
		ByteBuffer intBuffer = ByteBuffer.allocate(Integer.SIZE/Byte.SIZE).order(ByteOrder.nativeOrder());
		instream.read(intBuffer.array());
		int numberOfIndex = intBuffer.getInt();
		
		for(int i = 0 ; i < numberOfIndex ; i++){
			intBuffer.clear();
			instream.read(intBuffer.array());
			int index = intBuffer.getInt();
			
			intBuffer.clear();
			instream.read(intBuffer.array());
			int number_value = intBuffer.getInt();
			
			ByteBuffer byteBuffer = ByteBuffer.allocate(Double.SIZE/Byte.SIZE * number_value).order(ByteOrder.nativeOrder());
			instream.read(byteBuffer.array());
			DoubleBuffer doubleBuffer = byteBuffer.asDoubleBuffer();
			if(_numberOfSimulation != doubleBuffer.remaining()){
				throw new SimulationException("The number of value saved for the array data with the profiling number " + profiling_number + " at the index " + index + " is not equal to the simulation sample number");
			}
			double[] dvalues = new double[_numberOfSimulation];
			doubleBuffer.get(dvalues);
			
			values.put(variableName + '_' + Integer.toString(profiling_number) + '_' + Integer.toString(index) , dvalues);
		}
	}
}
