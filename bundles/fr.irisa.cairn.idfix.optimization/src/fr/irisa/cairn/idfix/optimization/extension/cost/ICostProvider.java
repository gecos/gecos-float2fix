package fr.irisa.cairn.idfix.optimization.extension.cost;

import fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace;

public interface ICostProvider {
	float getCostValue(SolutionSpace space);
}
