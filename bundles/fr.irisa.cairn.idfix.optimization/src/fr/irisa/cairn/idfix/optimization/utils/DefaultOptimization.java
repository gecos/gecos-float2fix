package fr.irisa.cairn.idfix.optimization.utils;

import java.io.PrintStream;

import org.apache.logging.log4j.Logger;

import fr.irisa.cairn.idfix.model.factory.IDFixUserSolutionSpaceFactory;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Data;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Operation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixProject;
import fr.irisa.cairn.idfix.model.informationandtiming.Informations;
import fr.irisa.cairn.idfix.model.informationandtiming.Section;
import fr.irisa.cairn.idfix.model.io.OperatorLibraryXMLParser;
import fr.irisa.cairn.idfix.model.operatorlibrary.OperatorLibrary;
import fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace;
import fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_LOADER;
import fr.irisa.cairn.idfix.optimization.extension.cost.ICostProvider;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.optimization.INoiseFunctionResultSimulator;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.exceptions.NoSolutionFoundException;
import fr.irisa.cairn.idfix.utils.exceptions.OptimException;
import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;
import fr.irisa.cairn.idfix.utils.logging.IDFixLogger;

/**
 * Extend this class and implement the method optimAlgorithm which represent the optimization algorithm.
 * This class provide some feature :
 * 		- Create the solution space from the operator library and the fixed point specification.
 * 		- Update the fixed point specification from the solution space
 * 		- Provide noise function compute and cost function compute method
 * 		- Creation of real time basic chart.
 * 		- Manage the simulator if the simulation is activated.
 * 		- Display result optimization information
 * 
 * @author nicolas simon
 * @date 22/09/14
 *
 */
public abstract class DefaultOptimization implements IOptimization {
	
	protected static boolean ENFORCE_FIXEDPT_CONTRAINTS = false; //consider that all operators only keep LSBs
	private static final boolean DEBUG = false;
	
	protected abstract boolean optimAlgorithm() throws IllegalAccessException, OptimException, SimulationException;
		
	protected float _userConstraint; // Noise constraint provided by the user
	protected SolutionSpace _solutionSpace; // Solution space created from the fixed point specification and the operator library.
	private double _noiseSolutionResult;
	private double _costSolutionResult; 
	
	protected IdfixProject _idfixproject;
	protected FixedPointSpecification _fixedPointSpecification; // Fixed point specification of the current IDFIX project
	private OperatorLibrary _newOperatorLibrary;
	private boolean _displayChart; // Directive for the chart displaying
	private INoiseFunctionResultSimulator _simulator; // Simulator used for the simulation process
	protected ICostProvider _costProvider;
	
	// Noise chart stuff
	private OptimisationChart _chartNoise;
	private RunOptimisationChart _runOptiChartNoise;
	private Thread _threadChartNoise;
	// Cost chart stuff
	private OptimisationChart _chartCost;
	private RunOptimisationChart _runOptiChartCost;
	private Thread _threadChartCost;
	// Chart stuff
	private Long _timingStartChart;
	private Long _timingStopChart;
	private final long _freqRefreshingChart = (long) 50;
	
	// Logs and informations stuff
	private boolean _displayInformation = true;
	protected final Logger _logger = IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_OPTIMIZATION);
	private float _totalTimingNoiseEvaluation = 0f;
	private int _numberNoiseEvaluation = 0;
	private int _numberCostEvaluation =0;
	private Informations _processInformations;
	private Section _sectionTimeLog;
	
	private NOISE_FUNCTION_LOADER _loader;
	
	protected void myassert(boolean b, String s) throws OptimException{
		if (!b){
			throw new OptimException(s);
		}
	}
	
	public static void setENFORCE_FIXEDPT_CONTRAINTS(boolean b) {
		ENFORCE_FIXEDPT_CONTRAINTS = b;
	}
	
	public abstract String getName();
	protected abstract double getFinalNoisePowerResult();
	protected abstract double getFinalCostResult();
	
	public DefaultOptimization(IdfixProject idfixproject, ICostProvider costProvider, float userConstraint, String operatorLibrary, Section sectionTimeLog, INoiseFunctionResultSimulator simulator) {
		_idfixproject = idfixproject;
		_costProvider = costProvider;
		_userConstraint = userConstraint;
		_newOperatorLibrary = new OperatorLibraryXMLParser().load(operatorLibrary);
		_fixedPointSpecification = idfixproject.getFixedPointSpecification();
		_displayChart = idfixproject.getUserConfiguration().isOptimizationGraphDisplay();
		_simulator = simulator;
		_processInformations = idfixproject.getProcessInformations();
		_sectionTimeLog = sectionTimeLog;
		
		_noiseSolutionResult = Float.NEGATIVE_INFINITY;
		_costSolutionResult = Float.POSITIVE_INFINITY;
		
		_loader = idfixproject.getUserConfiguration().getNoiseFunctionLoader();
	}
	
	@Override
	public void run() throws IllegalAccessException, OptimException, SimulationException {
		_logger.info("               - " + getName() + " algorithm used");
		this.creationChart(getName());
		
		// Source generation, compilation and library load
		// Float part simulation process
		if(_simulator != null)
			_simulator.initializeSimulation();
		
		// Timing save and chart creation
		Long timingStart = System.currentTimeMillis();
		this.startChart();
		
		// Creation of the solution space and run the optimization process
//		_solutionSpace = IDFixUserSolutionSpaceFactory.SOLUTIONSPACE(_fixedPointSpecification, _operatorLibrary);
		_solutionSpace = IDFixUserSolutionSpaceFactory.SOLUTIONSPACE(_idfixproject, _newOperatorLibrary);
		boolean solutionFound = optimAlgorithm();
				
		// Check if an solution has been found
		if(!solutionFound){
			throw new NoSolutionFoundException("    ***  No solution found ***");
		}
		_noiseSolutionResult = getFinalNoisePowerResult();
		_costSolutionResult = getFinalCostResult();
		
		this.stopChart();
						
		// Log timing saving
		Long timingStop = System.currentTimeMillis();
		this.saveTimingForLog(timingStop, timingStart, getName());
		
		// Run the last simulation iteration if needed
		if(_simulator != null){
			if((!_simulator.isEachCallNoiseFunction() && _simulator.lastSimulationSuccessful()) || ( _simulator.isEachCallNoiseFunction() && _simulator.lastSimulationSuccessful() && _simulator.getSimulationFrequencies() > 1)){
				_simulator.runFixedSimulation(_noiseSolutionResult);
			}
			_simulator.finalizeSimulation(_numberNoiseEvaluation);
		}
		
		if(_displayInformation){
			this.printInformation();
		}
	}

	@Override
	public void setPrintStreamErr(PrintStream err) {
//		throw new UnsupportedOperationException("not yet implemented");
	}

	@Override
	public void setPrintStreamOut(PrintStream out) {
//		throw new UnsupportedOperationException("not yet implemented");
	}
	
	/**
	 * Save timing log
	 * 
	 * @auhtor nicolas simon
	 * 
	 * @param timingStop
	 * @param timingStart
	 * @param kindOfOpti
	 */
	private void saveTimingForLog(Long timingStop, Long timingStart, String kindOfOpti){
		_processInformations.getAnalyticInformations().setFinalNoiseConstraint(_noiseSolutionResult);
		_processInformations.getAnalyticInformations().setFinalCostConstraint((float) _costSolutionResult);
		_processInformations.getAnalyticInformations().setNoiseEvalutationNumber(_numberNoiseEvaluation);
		_processInformations.getAnalyticInformations().setCostEvaluationNumber(_numberCostEvaluation);
		_processInformations.setUserNoiseConstraint(_userConstraint);
		_processInformations.setOptimizationAlgorithmUsed(getName());
		
		_sectionTimeLog.addTimeLog("Optimization", timingStart, timingStop);
		_sectionTimeLog.addTimeLog("Noise function total time", _totalTimingNoiseEvaluation);
		_sectionTimeLog.addTimeLog("Noise function one execution",  _totalTimingNoiseEvaluation / _numberNoiseEvaluation);
	}
	
	/**
	 * Display the optimization algorithm result
	 * 
	 * @author nicolas simon
	 * 
	 */
	private void printInformation(){
		StringBuffer logBuffer = new StringBuffer();
		
		logBuffer.append("---------------------------------------- Analytic result ----------------------------------------").append("\n");
		logBuffer.append("\tOperators : ").append("\n");
		for(Operation operator : _fixedPointSpecification.getOperations()){
			logBuffer.append("\t\tnumOpCDFG-IDFixConv: " + operator.getConv_cdfg_number() + "\t(numOpCDFG-IDFixEval:" + operator.getEval_cdfg_number() + ")\tOpType: " + operator.getKind()   + "\tTriplet : [" + operator.getOperands().get(0).getBitWidth() + " " + operator.getOperands().get(1).getBitWidth() + " " + operator.getOperands().get(2).getBitWidth() + "]").append("\n");
		}
		
		logBuffer.append("\n").append("\tDatas : ").append("\n");
		for(Data data : _fixedPointSpecification.getDatas()){
			logBuffer.append("\t\tnumOpCDFG-IDFixEval: " + data.getCDFGNumber() + "\t(name: " + data.getCorrespondingSymbol().getName() +")\t      " + data.getFixedInformation().toString()).append("\n");
		}
		
		logBuffer.append("\tnbEvalCost = " + _numberCostEvaluation + "     nbEvalPb = " + _numberNoiseEvaluation).append("\n");
		logBuffer.append("\tSolution cost : " + _costSolutionResult).append("\n");
		logBuffer.append("\tSolution noise power (db) : " + _noiseSolutionResult).append("\n");
		_logger.info(logBuffer.toString());
		
		if(_simulator != null && _simulator.lastSimulationSuccessful()){
			_simulator.printInformation();
		}
		
		_logger.info("---------------------------------------------------------------------------------------------------");
	}
	
	/**
	 * Return the cost of the solution space
	 * @param space
	 * @return cost of the solution space
	 */
	protected float costEvaluationProcess(SolutionSpace space){
		float ret =  _costProvider.getCostValue(space);
		_numberCostEvaluation++;
		
		this.printToCostChart(ret);
		
		return ret;
	}
	
	/**
	 * Return the noise of the solution space
	 * @param solutionSpace
	 * @return noise of the solution space
	 * @throws OptimException
	 */
	protected double noiseEvaluationProcess(SolutionSpace solutionSpace) throws OptimException{

		if(ENFORCE_FIXEDPT_CONTRAINTS)
			enforceFixedPointConstraints(solutionSpace);
		
		double ret;
		long timingStart = System.currentTimeMillis();
		ret = NoiseFunctionCallManager.call(_loader, solutionSpace);
		long timingStop = System.currentTimeMillis();
		_totalTimingNoiseEvaluation += (timingStop - timingStart) / 1000f;
		_numberNoiseEvaluation++;
		
		this.printToNoiseChart(ret);
		
		// Lancement de la simulation si la simulation est activée et si on veut tester chaque appel de du computePb
		if(_simulator != null && _simulator.isEachCallNoiseFunction() && _simulator.lastSimulationSuccessful()){
			if(_numberNoiseEvaluation%_simulator.getSimulationFrequencies() == 0){
				_simulator.runFixedSimulation(ret);
			}
		}

		return ret;
	}
	
	protected void enforceFixedPointConstraints(SolutionSpace solutionSpace) {
		for(Operation op : solutionSpace.getOperators()) {
			
			FixedPointInformation op0 = op.getOperands().get(0);
			FixedPointInformation op1 = op.getOperands().get(1);
			FixedPointInformation op2 = op.getOperands().get(2);
			
			/* 
			 * get (max) available fractional width: 
			 *    AF = bitWidth - (minimum IntegerWidth := dynamicIntegerWidth)
			 */
			final int DynIW0 = op0.getDynamicIntegerWidth();
			final int DynIW1 = op1.getDynamicIntegerWidth();
			final int DynIW2 = op2.getDynamicIntegerWidth();
			
			final int AF0 = op0.getBitWidth() - DynIW0;
			final int AF1 = op1.getBitWidth() - DynIW1;
			final int AF2 = op2.getBitWidth() - DynIW2;
			
			/* Compute Effective fractional width by enforcing arithmetic constraints */
			int f0 = AF0;
			int f1 = AF1;
			int f2 = AF2;
			
			switch(op.getKind().getValue()) {
			case OperatorKind.ADD_VALUE:
			case OperatorKind.SUB_VALUE:
				/* 
				 * input and output operands must be aligned to same Fractional part 
				 * f0 = f1 = f2 = min(AF0,AF1,AF2).
				 * Align to min to avoid need for extension!
				 */
				f0 = AF0 < AF1? AF0 : AF1;
				f0 = f0 < AF2? f0 : AF2;
				f1 = f0;
				f2 = f0;
				break;
				
			case OperatorKind.MUL_VALUE:
				/* no alignment is required on the inputs BUT wfOut must be >= to wf0 + wf1 */
				if(AF2 >= AF0 + AF1) {
					//there is enough bits to hold the fractional part of the result
					f0 = AF0;
					f1 = AF1;
				}
				else {
					//no enough bits to hold the fractional part: f1+f2 must be (<)= AF3.
					//XXX Many solutions are possible !!!
					int excess = AF0+AF1 - AF2;
					f0 = AF0 - (excess/2) - (excess%2);
					f1 = AF1 - (excess/2);
				}
				//FIXME: is it the same (f1 + f2) for signed and unsigned ???
				f2 = f0 + f1;// <= AF2
				break;
				
			case OperatorKind.DIV_VALUE:
				if(DEBUG) System.err.println(new Exception().getStackTrace()[0] + " ** WARNING: not tested for operation: " + op.getKind().getName());
				
				/* XXX wfOut must be = wf0 - wf1 <=> wf0 = wfOut + wf1 */
				if(AF0 >= AF2 + AF1) {
					f0 = AF0;
					f1 = AF1;
				}
				else {
					//no enough bits to hold the fractional part
					//XXX Many solutions are possible !!!
					int excess = AF2+AF1 - AF0;
					f1 = AF1 - (excess/2) - (excess%2);
					f2 = AF2 - (excess/2);
				}
				
				f2 = f0 - f1;
				break;
				
			case OperatorKind.SET_VALUE:
				break;
				
			default:
				System.err.println(new Exception().getStackTrace()[0] + " ** WARNING: operation not supported: " + op.getKind().getName());
				break;
			}
			
			op0.setIntegerWidth(DynIW0 + (AF0 - f0));
			op1.setIntegerWidth(DynIW1 + (AF1 - f1));
			op2.setIntegerWidth(DynIW2 + (AF2 - f2));
			
			if(DEBUG) {
				StringBuffer sb = new StringBuffer();
				sb.append("*** aligning FW of: ").append(op.getKind()).append(op.getEval_cdfg_number()).append("\n");
				sb.append("   in0: <").append(DynIW0).append(",").append(AF0).append("> -> <");
				sb.append(op0.getIntegerWidth()).append(",").append(op0.getFractionalWidth()).append(">\n");
				
				sb.append("   in1: <").append(DynIW1).append(",").append(AF1).append("> -> <");
				sb.append(op1.getIntegerWidth()).append(",").append(op1.getFractionalWidth()).append(">\n");
				
				sb.append("   out: <").append(DynIW2).append(",").append(AF2).append("> -> <");
				sb.append(op2.getIntegerWidth()).append(",").append(op2.getFractionalWidth()).append(">\n");
				
				_logger.info(sb.toString());
			}
		}		
	}
	
	/*************************************************************************
	 *				               Chart method                              *
	 *                                                                       * 
	 ************************************************************************/
	
	/**
	 * Initialize the noise and cost chart object
	 * 
	 * @author nicolas simon
	 * 
	 * @param title
	 */
	private void creationChart(String title){
		if(_displayChart){
			_chartNoise = new OptimisationChartNoise(title, -_userConstraint);
			_runOptiChartNoise = new RunOptimisationChart(_chartNoise, _freqRefreshingChart);
			_threadChartNoise = new Thread(_runOptiChartNoise);
			
			_chartCost = new OptimisationChartCost(title);
			_runOptiChartCost = new RunOptimisationChart(_chartCost, _freqRefreshingChart);
			_threadChartCost = new Thread(_runOptiChartCost);
		}
	}
	
	/**
	 * Run threads which manage noise and cost chart
	 * 
	 * @author nicolas simon
	 */
	private void startChart(){
		if(_displayChart){
			_timingStartChart = System.currentTimeMillis();
			_threadChartNoise.start();
			_threadChartCost.start();
		}
	}
	
	/**
	 * Stop threads which manage noise and cost chart
	 *
	 * @author nicolas simon
	 */
	private void stopChart(){
		if(_displayChart){
			try {
				_timingStopChart = System.currentTimeMillis();
				_runOptiChartNoise.stop(_timingStopChart-_timingStartChart, -_noiseSolutionResult); // mise à jour du graph
				_threadChartNoise.interrupt();
				_threadChartNoise.join();
				
				_runOptiChartCost.stop(_timingStopChart-_timingStartChart, _costSolutionResult);
				_threadChartCost.interrupt();
				_threadChartCost.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
	}
	
	/**
	 * Update the noise chart
	 * 
	 * @author nicolas simon 
	 * @param noise
	 */
	private void printToNoiseChart(double noise){
		if(_displayChart){
			_timingStopChart = System.currentTimeMillis();
			_runOptiChartNoise.setInformation(_timingStopChart-_timingStartChart, -noise);			
		}
	}
	
	/**
	 * Update the cost chart
	 * 
	 * @author nicolas simon
	 * 
	 * @param cost
	 */
	private void printToCostChart(float cost){
		if(_displayChart){
			_timingStopChart = System.currentTimeMillis();
			_runOptiChartCost.setInformation(_timingStopChart-_timingStartChart, cost);
		}
	}

}
