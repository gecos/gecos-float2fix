package fr.irisa.cairn.idfix.optimization.utils;


/**
 * Classe permettant de générer des threads d'objet OptimisationChart
 * 
 * @author nicolas simon
 *
 */
public class RunOptimisationChart implements Runnable {

	private OptimisationChart chart;
	private long freq;
	private double x = 0;
	private double y = 0;
	private double xFinal;
	private double yFinal;
	boolean update = false;
	boolean stop = false;
	
	public RunOptimisationChart(OptimisationChart chart, long freq){
		this.chart = chart;
		this.freq = freq;
	}
	
	@Override
	public void run() {
		while(!stop){
			while(!update && !stop){
				try {
					Thread.sleep(freq);
				} catch (InterruptedException e) {}
			}
			chart.addInformationToDataSet(x, y);
			update = false;
		}
		chart.addInformationToDataSet(xFinal, yFinal);
	}
	
	public void setInformation(double x, double y){
		if(!update){
			this.x = x;
			this.y =y;
			update = true;
		}
	}
			
	public void stop(double x, double y){
		xFinal = x;
		yFinal = y;
		this.stop = true;
		
		
	}

}