package fr.irisa.cairn.idfix.optimization.utils;

import java.io.File;

import com.sun.jna.Native;

/**
 * Provide jna access method for the CNoise function library
 * @author nsimon
 * @date 16/04/14
 */
public class CNoiseFunctionLibraryManager {
	private interface CLibrary extends com.sun.jna.Library {
		double ComputePb(int[] tabTriplet, int[] WFPquandMode, int[] tabInputWd, int[] tabInputWdquantMode, int outputMode);
	}
	
	private static CLibrary _lib;
	private static int _outputMode;
	
	public static void loadLibrary(String pathNoiseLibrary, int outputMode){
		_outputMode = outputMode;
		File flib = new File(pathNoiseLibrary);
		if(!flib.isFile())
			throw new RuntimeException("*** Error: Impossible to load the noise library. Problem occured during the noise library generation (IDFix-Eval module)");
	
		_lib = (CLibrary) Native.loadLibrary(flib.getAbsolutePath(), CLibrary.class);
	}
	
	public static void unloadLibrary(){
		_lib = null;
		Runtime.getRuntime().gc();
	}
	
	public static double compute(int[] tabTriplet, int[] WFPquandMode, int[] tabInputWd, int[] tabInputWdquantMode){
		return _lib.ComputePb(tabTriplet, WFPquandMode, tabInputWd, tabInputWdquantMode, _outputMode);
	}
}
