package fr.irisa.cairn.idfix.optimization.aglorithms;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import fr.irisa.cairn.idfix.model.fixedpointspecification.Operation;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixProject;
import fr.irisa.cairn.idfix.model.informationandtiming.Section;
import fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace;
import fr.irisa.cairn.idfix.optimization.extension.cost.ICostProvider;
import fr.irisa.cairn.idfix.optimization.utils.DefaultOptimization;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.optimization.INoiseFunctionResultSimulator;
import fr.irisa.cairn.idfix.utils.exceptions.OptimException;
import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;

public class MinPlusOne extends DefaultOptimization {
	
	private boolean _dichotomicSearch = true;

	public MinPlusOne(IdfixProject project, ICostProvider costProvider, float userConstraint, String operatorLibrary, Section sectionTimeLog, INoiseFunctionResultSimulator simulator) {
		super(project, costProvider, userConstraint, operatorLibrary, sectionTimeLog, simulator);		
	}

	@Override
	public String getName() {
		return "Min plus one bit";
	}
	
	@Override
	protected double getFinalNoisePowerResult() {
		try {
			return noiseEvaluationProcess(_solutionSpace);
		} catch (OptimException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected double getFinalCostResult() {
		return costEvaluationProcess(_solutionSpace);
	}
	
	private Comparator<Operation> _operatorComparator = new Comparator<Operation>() {
		@Override
		public int compare(Operation o1, Operation o2) {
			return o1.getEval_cdfg_number() - o2.getEval_cdfg_number();
		}
	};
	
	@Override
	protected boolean optimAlgorithm() throws IllegalAccessException, OptimException, SimulationException {
		double currentNoise;
		
		_solutionSpace.setAllOperatorToMax();
		currentNoise = noiseEvaluationProcess(_solutionSpace);
		if(currentNoise > _userConstraint)
			return false;
		
		if(_dichotomicSearch){
			this.startingSolutionSearchDichotomic();
		}
		else{
			_solutionSpace.setAllOperatorToMin();
		}

		currentNoise = noiseEvaluationProcess(_solutionSpace);
		if(currentNoise >= _userConstraint)
			minPlusOne();
		
		_fixedPointSpecification.updateDataFixedInformation();
		return true;
	}
		
	@Override
	protected float costEvaluationProcess(SolutionSpace space) {
		_fixedPointSpecification.updateDataFixedInformation();
		return super.costEvaluationProcess(space);
	}

	private void minPlusOne() throws OptimException{
		// Base solution
		double currentNoise;
		float currentCost;
		
		// next direction
		double nextNoise;
		float nextCost;
		SortedMap<Operation, Double> criterion = new TreeMap<Operation, Double>(_operatorComparator);
		Operation nextOperator;
		
		currentNoise = noiseEvaluationProcess(_solutionSpace);
		currentCost = costEvaluationProcess(_solutionSpace);
		while(currentNoise > _userConstraint){			
			for(Operation operator : _solutionSpace.getOperators()){
				if(!_solutionSpace.isMaxIndexOf(operator)){
					_solutionSpace.indexPlusOneTo(operator);
					nextNoise = noiseEvaluationProcess(_solutionSpace);
					nextCost = costEvaluationProcess(_solutionSpace);
					criterion.put(operator, computeCriterion(currentNoise, nextNoise, currentCost, nextCost));
					_solutionSpace.indexMinusOneTo(operator);
				}
			}
			
			nextOperator = maxCriterion(criterion);
			_solutionSpace.indexPlusOneTo(nextOperator);
			
			currentNoise = noiseEvaluationProcess(_solutionSpace);
			currentCost = costEvaluationProcess(_solutionSpace);
			
			criterion.clear();
		}
	}
	
	
	
	/**
	 * Search a start solution thanks to a dichotomous algorithm
	 * 
	 * @throws OptimException 
	 */
	private void startingSolutionSearchDichotomic() throws OptimException {
		int indexMin, indexMax, indexDich;
		double noise;
		Map<Operation, Integer> resultSaving = new HashMap<Operation, Integer>();
		_solutionSpace.setAllOperatorToMax();
		
		for(Operation op : _solutionSpace.getOperators()){
			_solutionSpace.setOperatorTo(op, 0);
			noise = noiseEvaluationProcess(_solutionSpace);
			if(noise > _userConstraint){
				indexMin = -1;
				indexMax = _solutionSpace.getMaxIndexOf(op);
				indexDich = (indexMax - indexMin) / 2;
				while(indexMin + 1 != indexMax){
					_solutionSpace.setOperatorTo(op, indexDich);
					noise = noiseEvaluationProcess(_solutionSpace);
					
					if(noise < _userConstraint){
						indexMax = indexDich;
						indexDich -= (indexMax - indexMin) / 2; 
					}
					else{
						indexMin = indexDich;
						indexDich += (indexMax - indexMin) / 2;
					}
				}
			
				resultSaving.put(op, indexMax);
			}
			else{
				resultSaving.put(op, 0);
			}
			_solutionSpace.setOperatorTo(op, _solutionSpace.getMaxIndexOf(op));
		}
		
		for(Operation op : resultSaving.keySet()){
			_solutionSpace.setOperatorTo(op, resultSaving.get(op));
		}
	}
	
	/**
	 * 	Criterion for direction search 
	 * 
	 * @param noise
	 * @param noiseNext
	 * @param cost
	 * @param costNext
	 * @return criterion
	 * @throws OptimException 
	 */
	private double computeCriterion(double noise, double nextNoise, float cost, float nextCost) throws OptimException{
		if(cost == nextCost){
			throw new OptimException("Division by 0 detected during the computation of the criterion");
		}
		
		return (noise - nextNoise) / ( nextCost - cost);
	}
	
	/**
	 * 	Return the first operator with the the higher criterion
	 * 
	 * @param criterion
	 * @return operator
	 */
	private Operation maxCriterion(SortedMap<Operation, Double> criterion) throws OptimException{
		double kCriterionValue = Double.NEGATIVE_INFINITY;
		Operation ret = null;
		
		for(Operation op : criterion.keySet()){
			if(kCriterionValue < criterion.get(op)){
				kCriterionValue = criterion.get(op);
				ret = op;
			}
		}
		
		if(ret == null)
			throw new OptimException("No operator direction chosen to continue the tabu search algorithm");
		
		return ret;
	}
}
