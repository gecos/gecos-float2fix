package fr.irisa.cairn.idfix.optimization.utils;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;



/**
 * Class permettant de gérer la création et l'affichage du graphe utilisé pour afficher le bruit en fonction du temps pendant l'optimisation
 * 
 * @author nicolas simon
 *
 */
@SuppressWarnings("serial")
public class OptimisationChart extends ApplicationFrame{

	protected XYSeries data;
	protected XYSeriesCollection dataset;
	protected JFreeChart chart;
	protected ChartPanel chartPanel;

	protected OptimisationChart(String title, String nameFunction, String xName, String yName){
		super(title);
		this.dataset = new XYSeriesCollection();
		this.data = new XYSeries(nameFunction);
		this.dataset.addSeries(this.data);
		
		this.chart = ChartFactory.createXYLineChart(title, xName, yName, dataset, PlotOrientation.VERTICAL, true, false, false);
		this.chartPanel = new ChartPanel(chart);
		this.setContentPane(chartPanel);
		this.pack();
		this.setVisible(true);
	}
	
	/**
	 * Ajout un nouveau point au graph et actualise l'affichage
	 * 
	 * @author nicolas simon
	 * @param x
	 * @param y
	 */
	
	public void addInformationToDataSet(double x, double y){
		data.add(x, y);
	}
//	
//	public void saveChartAsJPEG() throws IOException{
//		File fileChart = new File(Float2fixParams.getOutputDirectory()+"graphOptimisation.jpeg");
//		ChartUtilities.saveChartAsJPEG(fileChart, chart, chartPanel.getWidth(), chartPanel.getHeight());
//	}
}