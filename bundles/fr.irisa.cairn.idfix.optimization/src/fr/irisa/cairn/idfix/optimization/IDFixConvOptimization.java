package fr.irisa.cairn.idfix.optimization;

import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import fr.irisa.cairn.idfix.model.factory.IDFixUserInformationAndTimingFactory;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixProject;
import fr.irisa.cairn.idfix.model.informationandtiming.Section;
import fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_LOADER;
import fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_OUTPUT_MODE;
import fr.irisa.cairn.idfix.optimization.aglorithms.GreedyRandomizedAdaptativeSearchProcedures;
import fr.irisa.cairn.idfix.optimization.aglorithms.MinPlusOne;
import fr.irisa.cairn.idfix.optimization.aglorithms.MinPlusOneAdaptative;
import fr.irisa.cairn.idfix.optimization.aglorithms.TabuSearch;
import fr.irisa.cairn.idfix.optimization.extension.cost.ICostProvider;
import fr.irisa.cairn.idfix.optimization.utils.CNoiseFunctionLibraryManager;
import fr.irisa.cairn.idfix.optimization.utils.IOptimization;
import fr.irisa.cairn.idfix.optimization.utils.NEvalLibNatives;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.optimization.INoiseFunctionResultSimulator;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.optimization.NoiseFunctionResultSimulator;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.exceptions.OptimException;
import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;
import fr.irisa.cairn.idfix.utils.logging.IDFixLogger;

/**
 * Block which regroup all the optimization process
 * @author Nicolas Simon
 * @date Apr 12, 2013
 */
public class IDFixConvOptimization {
	private IdfixProject _idfixProject;
	private ICostProvider _costProvider;
	private String _noiseConstraint;
	private String _operatorLibrary;
	private int _optimizationAlgorithm;
	private IOptimization optim;
	
	private Section _sectionTimeLog;

	private PrintStream _out;
	private PrintStream _err;
	
	private static final Logger _logger = IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_OPTIMIZATION);
	
	protected static Map<Integer, Class<? extends IOptimization>> _algorithms_map = new HashMap<Integer, Class<? extends IOptimization>>() {
		private static final long serialVersionUID = -4556336378727562459L;
		{
			put(0, null); //OptimBranchAndBound
			put(1, null); //OptimBABFixedSizeOperators
			put(2, MinPlusOne.class);
			put(3, MinPlusOneAdaptative.class);
			put(4, TabuSearch.class);
			put(5, GreedyRandomizedAdaptativeSearchProcedures.class);
		}
	};
	
	public static Integer getAvailableAlgorithmNumber() {
		Integer id = _algorithms_map.size();
		while(_algorithms_map.containsKey(id)) ++id;
		return id;
	}
	
	public static void addAlgorithm(Integer unique_number, Class<? extends IOptimization> algoritmClass) {
		if(_algorithms_map.containsKey(unique_number)) {
			throw new InvalidParameterException("unique_number " + unique_number + " is already used! use a differnt number to map your Algorithm");
		}
		_algorithms_map.put(unique_number, algoritmClass);
	}
	
	/**
	 * Block which regroup all the optimization process
	 * @param dynSpecifPath
	 * @param noiseLibraryPath
	 * @param noiseConstraint
	 * @param operatorLibrary
	 * @param optimizationAlgorithm
	 * @param outputDirPath
	 */
	public IDFixConvOptimization(IdfixProject idfixProject, ICostProvider costProvider, String noiseConstraint, String operatorLibrary, int optimizationAlgorithm){
		_idfixProject = idfixProject;
		_costProvider = costProvider;
		_noiseConstraint = noiseConstraint;
		_operatorLibrary = operatorLibrary;
		_optimizationAlgorithm = optimizationAlgorithm;
		_sectionTimeLog = IDFixUserInformationAndTimingFactory.SECTION("Optimization");
		_idfixProject.getProcessInformations().getTiming().getSections().add(_sectionTimeLog);
		
		_out = System.out;
		_err = System.err;
		
		init_optimizationAlgorithm();
	}
	
	protected void init_optimizationAlgorithm() {
		Class<? extends IOptimization> algoClass = _algorithms_map.get(_optimizationAlgorithm);
		if(algoClass == null) {
			_logger.error("Optimisation choice not valid");
			StringBuffer buf = new StringBuffer(200);
			for(Integer num : _algorithms_map.keySet()) {
				buf.append("- ").append(num).append(" => ").append(_algorithms_map.get(num).getName()).append("\n");
			}
			_logger.info(buf.toString());
			throw new RuntimeException("Unknow optimisation choice");
		}
		
		Class<?>[] constructorParameters= new Class[]{IdfixProject.class, ICostProvider.class, float.class, String.class, Section.class, INoiseFunctionResultSimulator.class};
		
		Constructor<? extends IOptimization> constructor;
		try {
			constructor = algoClass.getConstructor(constructorParameters);
		} catch (Exception e) {
			throw new RuntimeException("Algorithm class " + algoClass.getName() + "does not provide constructor with following parameter types: " + constructorParameters, e);
		}
		
		try {
			// Create simulator if needed
			INoiseFunctionResultSimulator simulator = null;
			if(_idfixProject.getUserConfiguration().optimizationSimulationIsActivated()){
				simulator = new NoiseFunctionResultSimulator(_idfixProject);
			}
			
			optim = constructor.newInstance(_idfixProject, _costProvider, Float.parseFloat(_noiseConstraint), this._operatorLibrary, _sectionTimeLog, simulator);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
				
		optim.setPrintStreamErr(_err);
		optim.setPrintStreamOut(_out);
	}
	
	public IOptimization getOptimizationAlgorithm() {
		return this.optim;
	}
	
	/**
	 * Set a custom {@link PrintStream} for all out messages. It is System.out by default
	 * @param out
	 */
	public void setPrintStreamOut(PrintStream out){
		_out = out;
	}
	
	/**
	 * Set a custom {@link PrintStream} for all err messages. It is System.err by default
	 * @param err
	 */
	public void setPrintStreamErr(PrintStream err){
		_err = err;
	}
	
	/**
	 * Launch the optimization process and the simulation if activated
	 * @return Fix point specification
	 * @throws OptimException
	 * @throws IllegalAccessException
	 * @throws SimulationException
	 */
	public void compute() {
		long timingStart, timingStop, sectionTimingStart, sectionTimingStop;
		sectionTimingStart = System.currentTimeMillis();
		try{
			_logger.info("    *** Optimisation process ***");
			_logger.info("               - Noise function output mode used : " + _idfixProject.getUserConfiguration().getNoiseFunctionOutputMode());
			_logger.info("               - Constraint : " + _noiseConstraint);
			
			// Choose the java native system from load the C noise library
			timingStart = System.currentTimeMillis();
			if(_idfixProject.getUserConfiguration().getNoiseFunctionLoader() == NOISE_FUNCTION_LOADER.JNI)
				NEvalLibNatives.loadDynamicLibrary(_idfixProject.getNoiseLibraryFilePath());
			else if(_idfixProject.getUserConfiguration().getNoiseFunctionLoader() == NOISE_FUNCTION_LOADER.JNA)
				CNoiseFunctionLibraryManager.loadLibrary(_idfixProject.getNoiseLibraryFilePath(), resolveNoiseFunctionOutputMode(_idfixProject.getUserConfiguration().getNoiseFunctionOutputMode()));
			else
				throw new OptimException("Tools to run native code is not correct. JNI or JNA can be used");
			timingStop = System.currentTimeMillis();
			_sectionTimeLog.addTimeLog("Noise library loading", timingStart, timingStop);
			
			preRun();
			
			// run the optimization
			optim.run();
			
			postRun();
			
			// unload
			if(_idfixProject.getUserConfiguration().getNoiseFunctionLoader() == NOISE_FUNCTION_LOADER.JNA)
				CNoiseFunctionLibraryManager.unloadLibrary();
		} catch (OptimException | IllegalAccessException | SimulationException e){
			_logger.error(e.getMessage(), e);
			if(_idfixProject.getUserConfiguration().getNoiseFunctionLoader() == NOISE_FUNCTION_LOADER.JNA)
				CNoiseFunctionLibraryManager.unloadLibrary();
			throw new RuntimeException(e);
		}
		sectionTimingStop = System.currentTimeMillis();
		_sectionTimeLog.setTime((sectionTimingStop - sectionTimingStart) / 1000f);
	}

	protected void postRun() {}
	protected void preRun() {}
	
	private int resolveNoiseFunctionOutputMode(NOISE_FUNCTION_OUTPUT_MODE mode) throws OptimException{
		switch (mode) {
		case WORST:
			return 0;
			
		case MEAN:
			return 1;
			
		default:
			throw new OptimException("Incorrect noise function output mode " + mode);
		}
	}
}
