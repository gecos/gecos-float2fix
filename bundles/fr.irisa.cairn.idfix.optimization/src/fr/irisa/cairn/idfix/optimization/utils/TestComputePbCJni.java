package fr.irisa.cairn.idfix.optimization.utils;


/**
 * Interface for testing a ComputePb function manually
 */
@Deprecated
public class TestComputePbCJni {
	private String _libraryPath;

	public TestComputePbCJni(String libraryPath) {
		_libraryPath = libraryPath;
		NEvalLibNatives.loadDynamicLibrary(_libraryPath);
	}

	public void compute() {
		int width = 8;
		int width2 = 8;
		int tabSize = 20;
		int [] tabTriplet = new int[tabSize*3];
		int [] WFPquantMode = new int[tabSize*3];
		int [] tabInputWd = new int[tabSize];
		int [] tabInputWdquantMode = new int[tabSize];
		
		
		for (int i = 0; i < tabSize; i++){
			tabTriplet[i*3] = width;
			tabTriplet[i*3 + 1] = width;
			tabTriplet[i*3 + 2] = width;
			tabInputWd[i] = width2;
		}
		
		
		for (int i = 0; i < tabSize; i++){
			System.out.println("tabInput[" + i + "] = " + tabInputWd[i]);
			System.out.println("tabTriplet[" + i + "] = [ " + tabTriplet[i*3] + " ; " + tabTriplet[i*3 + 1] + " ; " + tabTriplet[i*3 + 2] + " ]");
		}
		
		System.out.println("width : " + width);
		System.out.println("tabSize : " + tabSize);
		
	
		double ret;
		ret = NEvalLibNatives.computePbCC(tabTriplet, WFPquantMode ,tabInputWd,tabInputWdquantMode);
		
		System.out.println("Bruit obtenu : " + ret);
	}
}
