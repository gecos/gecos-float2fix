package fr.irisa.cairn.idfix.optimization.utils;

/**
 * Class représentant le graph de coût
 * 
 * @author idfix
 *
 */

@SuppressWarnings("serial")
public class OptimisationChartCost extends OptimisationChart {

	public OptimisationChartCost(String title) {
		super(title, "Cost", "Time (seconds)", "Cost (PC)");
	}
}
