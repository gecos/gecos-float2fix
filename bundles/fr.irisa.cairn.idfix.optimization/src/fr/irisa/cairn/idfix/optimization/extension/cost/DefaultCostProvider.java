package fr.irisa.cairn.idfix.optimization.extension.cost;

import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Operation;
import fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace;

public class DefaultCostProvider implements ICostProvider {
	@SuppressWarnings("unused")
	private FixedPointSpecification _fixedPointSpecification;

	public DefaultCostProvider(FixedPointSpecification fixedPointSpecification){
		_fixedPointSpecification = fixedPointSpecification;
	}
	
	@Override
	public float getCostValue(SolutionSpace space) {
		float ret = 0;
		for(Operation op : space.getOperators()){
			ret += op.getCost() * op.getUsageNumber();//AnnotateOperators.getUsedOperatorsAndCost().get(op.getConv_cdfg_number());
		}		
		return ret;
	}
}
