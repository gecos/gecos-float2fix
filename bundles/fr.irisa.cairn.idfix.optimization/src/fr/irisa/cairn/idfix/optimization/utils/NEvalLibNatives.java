package fr.irisa.cairn.idfix.optimization.utils;
/*******************************************************************************
 * Copyright (c) 2007 Irisa / Inria / Universite de Rennes 1.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ludovic L'Hours - initial API and implementation
 *******************************************************************************/

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public abstract class NEvalLibNatives {
	
	public static native double computePbCC(int[] tabTriplet, int[] WFPquandMode, int[] tabInputWd, int[] tabInputWdquantMode);
	
	static {
		//loadDynamicLibrary(RunIDFixParams.getLibName());
	}
	
	public static boolean loadDynamicLibrary(String libName){
		try {
			File f = new File(libName);
			InputStream inputStream = f.toURI().toURL().openStream();
			
			// Copy resource to file system in a temp folder with a unique name
			File temporaryDll = File.createTempFile((new File(libName)).getName(),".tmp");
			FileOutputStream outputStream = new FileOutputStream(temporaryDll);
			byte [] array = new byte[(int) f.length() + 1];
			int read = 0;
			while ((read = inputStream.read(array)) > 0){
				outputStream.write(array, 0, read);
			}
			outputStream.close();
			temporaryDll.deleteOnExit();

			// Finally, load the dll
			System.load(temporaryDll.getAbsolutePath());
		} catch(Throwable e) {
			e.printStackTrace();
			System.err.println("*** Error: Impossible to load the noise library. Problem occured during the noise library generation (IDFix-Eval module) " + libName);
			System.exit(1);
			return false;
		}
		//System.out.println("Library " + libName + " loaded with success.");
		return true;
	}
	
}

