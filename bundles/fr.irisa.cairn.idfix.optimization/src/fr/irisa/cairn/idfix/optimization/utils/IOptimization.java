package fr.irisa.cairn.idfix.optimization.utils;

import java.io.PrintStream;

import fr.irisa.cairn.idfix.utils.exceptions.OptimException;
import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;

/**
 * Interface used to implement optimization algorithm.
 * See {@link DefaultOptimization} to have a good start.
 * @author nicolas simon
 *
 */
public interface IOptimization {
	public void run() throws IllegalAccessException, OptimException, SimulationException;
	public void setPrintStreamErr(PrintStream err);
	public void setPrintStreamOut(PrintStream out);
}
