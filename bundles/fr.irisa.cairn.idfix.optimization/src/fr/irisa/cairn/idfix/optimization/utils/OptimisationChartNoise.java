package fr.irisa.cairn.idfix.optimization.utils;

import org.jfree.data.xy.XYSeries;

/**
 * Class représentant le graph de bruit
 * 
 * @author idfix
 *
 */
@SuppressWarnings("serial")
public class OptimisationChartNoise extends OptimisationChart {
	protected XYSeries dataPbMax;
	protected float pbMax;
	
	public OptimisationChartNoise(String title, float pbMax) {
		super(title, "Noise", "Time (seconds)", "Noise (db)");
		this.pbMax = pbMax;
		this.dataPbMax = new XYSeries("PbMax");
		this.dataset.addSeries(this.dataPbMax);
	}
	
	@Override
	public void addInformationToDataSet(double x, double y){
		super.addInformationToDataSet(x, y);
		this.dataPbMax.add(x, pbMax);
	}
	
}
