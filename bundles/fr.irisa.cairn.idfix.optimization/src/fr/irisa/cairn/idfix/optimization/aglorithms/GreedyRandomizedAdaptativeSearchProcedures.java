package fr.irisa.cairn.idfix.optimization.aglorithms;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.SortedMap;
import java.util.TreeMap;

import fr.irisa.cairn.idfix.model.fixedpointspecification.Operation;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixProject;
import fr.irisa.cairn.idfix.model.informationandtiming.Section;
import fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace;
import fr.irisa.cairn.idfix.optimization.extension.cost.ICostProvider;
import fr.irisa.cairn.idfix.optimization.utils.DefaultOptimization;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.optimization.INoiseFunctionResultSimulator;
import fr.irisa.cairn.idfix.utils.exceptions.OptimException;
import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;

/**
 * Algorithme GRASP (Greedy Randomized Adaptative Search Procedures) est un algorithme basé sur un algo min+1 avec un choix aléatoire dans K meilleurs direction à chaque itération.
 * Dans certains système comme par exemple une fft, l'algorithme min+1 classique est en incapaciter de trouver un chemin améliorant la précision, nous rentrons dans une zone de plat.
 * Pour sortir de cette zone de plat, une modification a été ajouté à l'algorithme min+1 classique. Dès que nous ne trouvons pas de chemin améliorant la précision, un saut puis un retour en arrière sont effectués
 * permettant de trouver une solution nous permettant de sortir de cette zone de plat en restant le plus proche possible de la solution d'avant le saut.
 * 
 * Une fois avoir atteint la contrainte via l'algorithme min+1. Une recherche locale basée sur le recherche via tabou est appliqué.
 * 
 * Algorithme basé sur la thèse de H.Nguyen "Chapitre 6 - Recherche locale stochastique" * 
 * @author nicolas simon
 *
 */
public class GreedyRandomizedAdaptativeSearchProcedures extends DefaultOptimization {
	
	private boolean _dichotomicSearch = true;
	private int _TRCLBestValue = 3;

	public GreedyRandomizedAdaptativeSearchProcedures(IdfixProject project, ICostProvider costProvider,  float userConstraint, String operatorLibrary, Section sectionTimeLog, INoiseFunctionResultSimulator simulator) {
		super(project, costProvider, userConstraint, operatorLibrary, sectionTimeLog, simulator);
	}
	
	@Override
	public String getName() {
		return "GRASP";
	}
	
	@Override
	protected double getFinalNoisePowerResult() {
		try {
			return noiseEvaluationProcess(_solutionSpace);
		} catch (OptimException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected double getFinalCostResult() {
		return costEvaluationProcess(_solutionSpace);
	}
	
	private Comparator<Operation> _operatorComparator = new Comparator<Operation>() {
		@Override
		public int compare(Operation o1, Operation o2) {
			return o1.getEval_cdfg_number() - o2.getEval_cdfg_number();
		}
	};

	@Override
	protected boolean optimAlgorithm() throws IllegalAccessException,
			OptimException, SimulationException {
		
		_solutionSpace.setAllOperatorToMax();
		if(noiseEvaluationProcess(_solutionSpace) > _userConstraint)
			return false;
		
		if(_dichotomicSearch){
			this.startingSolutionSearchDichotomic();
		}
		else{
			_solutionSpace.setAllOperatorToMin();
		}
		buildPart();
		localSearch();
		
		_fixedPointSpecification.updateDataFixedInformation();
		return true;
	}
		
	@Override
	protected float costEvaluationProcess(SolutionSpace space) {
		_fixedPointSpecification.updateDataFixedInformation();
		return super.costEvaluationProcess(space);
	}
	
	/**
	 * GRASP build part.
	 * it is minus plus one bit algorithm with a random direction choice on the K best direction.
	 *  
	 * @author nicolas simon
	 * @throws OptimException  
	 */
	private void buildPart() throws OptimException{
		// Base solution
		double noise;
		float cost;
	
		// next direction
		double nextNoise;
		float nextCost;
		double criterionK;
		int indexOperator;
		Operation operatorNext;
		
		// Random stuff object
		List<Operation> listRCL;
		Random rand = new Random(System.currentTimeMillis());
		
		// Sorted criterion map
		Comparator<Double> comparator = new Comparator<Double>() {
	        @Override public int compare(Double s1, Double s2) {
	            return s2.compareTo(s1);
	        } 
		};
		SortedMap<Double, List<Operation>> criterionSorted = new TreeMap<Double, List<Operation>>(comparator);
		List<Operation> listOperators;
		
		// Save each operator criterion
		Map<Operation, Double> criterion = new HashMap<Operation, Double>();
		
		noise = noiseEvaluationProcess(_solutionSpace);
		cost = costEvaluationProcess(_solutionSpace);
		
		while(noise > _userConstraint){
			for(Operation op : _solutionSpace.getOperators()){
				indexOperator = _solutionSpace.getCurrentIndexOf(op);
				if(indexOperator < _solutionSpace.getMaxIndexOf(op)){
					_solutionSpace.indexPlusOneTo(op);
					nextNoise = noiseEvaluationProcess(_solutionSpace);
					nextCost = costEvaluationProcess(_solutionSpace);
					criterionK = this.directionSearchCriterionComputation(noise, nextNoise, cost, nextCost);
					if(criterionSorted.containsKey(criterionK)){
						criterionSorted.get(criterionK).add(op);
					}
					else{
						listOperators = new LinkedList<Operation>();
						listOperators.add(op);
						criterionSorted.put(criterionK, listOperators);
					}
					criterion.put(op, criterionK);
					
					_solutionSpace.indexMinusOneTo(op);
				}
			}
			myassert(criterionSorted.size() > 0, "The sorted set of operator criterion is empty");
			listRCL = this.bestRCL(criterionSorted, _TRCLBestValue);
			operatorNext = listRCL.get(rand.nextInt(listRCL.size()));
			if(criterion.get(operatorNext) <= 0){
				this.optimizedSolutionSearchSpecialCase();
			}
			else{
				_solutionSpace.indexPlusOneTo(operatorNext);
			}
			
			noise = noiseEvaluationProcess(_solutionSpace);
			cost = costEvaluationProcess(_solutionSpace);
			
			criterion.clear();
			criterionSorted.clear();
		}
	}
	
	/**
	 * Local search based on tabu algorithm 
	 *  
	 * @throws OptimException  
	 */
	private void localSearch() throws OptimException{
		// Base solution
		double noise, bestNoise = Double.NEGATIVE_INFINITY;
		float cost;
		
		// next direction information
		double nextNoise;
		float nextCost;
		SortedMap<Operation, Double> criterion = new TreeMap<Operation, Double>(_operatorComparator);
		Operation opDirection;
	
		boolean directionUp;
		List<Operation> tabuList = new LinkedList<Operation>();
		int indexMax;
		int indexMin = 0;
		
		noise = noiseEvaluationProcess(_solutionSpace);
		cost = costEvaluationProcess(_solutionSpace);
		
		if(noise < _userConstraint){
			directionUp = false;
		}
		else{
			directionUp = true;
		}
		
		while(tabuList.size() < _solutionSpace.getOperators().size()){
			for(Operation op : _solutionSpace.getOperators()){
				if(!tabuList.contains(op)){
					indexMax = _solutionSpace.getMaxIndexOf(op);
					if(directionUp && _solutionSpace.getCurrentIndexOf(op) < indexMax){
						_solutionSpace.indexPlusOneTo(op);
						nextNoise = noiseEvaluationProcess(_solutionSpace);
						nextCost = costEvaluationProcess(_solutionSpace);
						criterion.put(op, directionSearchCriterionComputation(noise, nextNoise, cost, nextCost));
						
						_solutionSpace.indexMinusOneTo(op);
					}
					else if(!directionUp && _solutionSpace.getCurrentIndexOf(op) > indexMin){
						_solutionSpace.indexMinusOneTo(op);
						nextNoise = noiseEvaluationProcess(_solutionSpace);
						nextCost = costEvaluationProcess(_solutionSpace);
						criterion.put(op, directionSearchCriterionComputation(noise, nextNoise, cost, nextCost));
						
						_solutionSpace.indexPlusOneTo(op);
					}
					else{
						tabuList.add(op);
					}
				}
			}
						
			if(tabuList.size() == _solutionSpace.getOperators().size()) // TODO a modifier le break
				break;

			if(directionUp){
				opDirection = maxCriterion(criterion);
				if(criterion.get(opDirection) <= 0){
					optimizedSolutionSearchSpecialCase();
				}
				else{
					_solutionSpace.indexPlusOneTo(opDirection);
				}
				
				noise = noiseEvaluationProcess(_solutionSpace);
				if(noise < _userConstraint){
					directionUp = false;
					tabuList.add(opDirection);
				}
			}
			else{
				opDirection = minCriterion(criterion);
				_solutionSpace.indexMinusOneTo(opDirection);
				noise = noiseEvaluationProcess(_solutionSpace);
				if(noise > _userConstraint){
					directionUp = true;
				}
			}
			
			cost = costEvaluationProcess(_solutionSpace);
			if(noise <= _userConstraint && noise > bestNoise){ // La contrainte de précision est négative
				_solutionSpace.saveCurrentSolutionSpace();
				bestNoise = noise;
			}
			
			criterion.clear();
		}
	}
	
	/**
	 * 	Criterion for direction search 
	 * 
	 * @param noise
	 * @param noiseNext
	 * @param cost
	 * @param costNext
	 * @return criterion
	 * @throws OptimException 
	 */
	private double directionSearchCriterionComputation(double noise, double nextNoise, float cost, float nextCost) throws OptimException {
		if(cost == nextCost){
			throw new OptimException("Division by 0 detected during the computation of the criterion");
		}
		return (nextNoise - noise) / ( cost - nextCost);
	}
	
	/**
	 * Return the list of the TRCLBestValue best value from the criterion given
	 * 
	 * @author nicolas simon 
	 * @param criterion
	 * @param TRCLBestValue
	 * @throws OptimException 
	 */
	private List<Operation> bestRCL(Map<Double, List<Operation>> criterionSorted, int TRCLBestValue) throws OptimException{
		int nbValue = 0;
		List<Operation> listRCL = new LinkedList<Operation>();
		List<Operation> tmpListCriterion;
		for(double key : criterionSorted.keySet()){
			tmpListCriterion = criterionSorted.get(key); 
			if(nbValue + tmpListCriterion.size() >= TRCLBestValue){
				for(int i = 0 ; i < TRCLBestValue - nbValue ; i++){
					listRCL.add(tmpListCriterion.get(i));
				}
				break;
			}
			else{
				listRCL.addAll(tmpListCriterion);
			}
			
			nbValue += tmpListCriterion.size();
		}
		
		myassert(listRCL.size() > 0, "List of the best path/value from le set of criterion for all operator is empty");
		return listRCL;
	}
	
	/**
	 * 	Return the first operator with the the higher criterion
	 * 
	 * @param criterion
	 * @return operator
	 */
	private Operation maxCriterion(SortedMap<Operation, Double> criterion) throws OptimException{
		double kCriterionValue = Double.NEGATIVE_INFINITY;
		Operation ret = null;
		
		for(Operation op : criterion.keySet()){
			if(kCriterionValue < criterion.get(op)){
				kCriterionValue = criterion.get(op);
				ret = op;
			}
		}
		
		if(ret == null)
			throw new OptimException("No operator direction chosen to continue the tabu search algorithm");
		
		return ret;
	}
	
	/**
	 * 	Return the first operator with the the smaller criterion
	 * 
	 * @param criterion
	 * @return operator
 	 * @throws OptimException 
	 */
	private Operation minCriterion(SortedMap<Operation, Double> criterion) throws OptimException{
		double kCriterionValue = Double.POSITIVE_INFINITY;
		Operation ret = null;
		
		for(Operation op : criterion.keySet()){
			if(kCriterionValue > criterion.get(op)){
				kCriterionValue = criterion.get(op);
				ret = op;
			}
		}
		
		if(ret == null)
			throw new OptimException("No operator direction chosen to continue the tabu search algorithm");

		return ret;
	}
	
	/**
	 * Search a start solution thanks to a dichotomy algorithm
	 * 
	 * @throws OptimException 
	 */
	private void startingSolutionSearchDichotomic() throws OptimException {
		int indexMin, indexMax, indexDich;
		double noise;
		Map<Operation, Integer> resultSaving = new HashMap<Operation, Integer>();
		_solutionSpace.setAllOperatorToMax();
		
		for(Operation op : _solutionSpace.getOperators()){
			_solutionSpace.setOperatorTo(op, 0);
			noise = noiseEvaluationProcess(_solutionSpace);
			if(noise > _userConstraint){
				indexMin = -1;
				indexMax = _solutionSpace.getMaxIndexOf(op);
				indexDich = (indexMax - indexMin) / 2;
				while(indexMin + 1 != indexMax){
					_solutionSpace.setOperatorTo(op, indexDich);
					noise = noiseEvaluationProcess(_solutionSpace);
					
					if(noise < _userConstraint){
						indexMax = indexDich;
						indexDich -= (indexMax - indexMin) / 2; 
					}
					else{
						indexMin = indexDich;
						indexDich += (indexMax - indexMin) / 2;
					}
				}
			
				resultSaving.put(op, indexMax);
			}
			else{
				resultSaving.put(op, 0);
			}
			_solutionSpace.setOperatorTo(op, _solutionSpace.getMaxIndexOf(op));
		}
		
		for(Operation op : resultSaving.keySet()){
			_solutionSpace.setOperatorTo(op, resultSaving.get(op));
		}
	}

	/**
	 * Special case which happening in some system. This case doesn't permit to find a direction which improve the solution.
	 * This case have a special treatment. We find a new solution from the stucked solution. 
	 * 
	 * @author nicolas simon
	 * @throws OptimException  
	 */
	private void optimizedSolutionSearchSpecialCase() throws OptimException {
		_logger.info("Specific case = > optimizedSolutionSearchSpecialCase");
		double pbPlusOne;
		double pb;
		float cost;
		float costPlusOne;
		double delta;
		ArrayList<Operation> orderedList = new ArrayList<Operation>();
		SortedMap<Double, List<Operation>> deltaMap = new TreeMap<Double, List<Operation>>(); // Map triée en fonction de la key (ici la key représente le delta pour le choix de la direction)
		List<Operation> applyPlus = new LinkedList<Operation>();
				
		for(Operation op : _solutionSpace.getOperators()){
			if(_solutionSpace.getCurrentIndexOf(op) < _solutionSpace.getMaxIndexOf(op)){
				_solutionSpace.indexPlusOneTo(op);
				applyPlus.add(op);
			}
		}
				
		pb = noiseEvaluationProcess(_solutionSpace);
		cost = costEvaluationProcess(_solutionSpace);
		
		// on ordonne les différents chemins inverse pour determiner dans quel ordre nous devons incrémenter les opérateurs de la solution de départ pour sortir du plat
		for(Operation op : applyPlus){
			_solutionSpace.indexMinusOneTo(op);
			pbPlusOne = noiseEvaluationProcess(_solutionSpace);
			costPlusOne = costEvaluationProcess(_solutionSpace);
			delta = (pbPlusOne - pb) / (costPlusOne- cost);
			if(deltaMap.containsKey(delta)){
				deltaMap.get(delta).add(op);
			}
			else{
				List<Operation> listTmp = new ArrayList<Operation>();
				listTmp.add(op);
				deltaMap.put(delta, listTmp);
			}
			
			_solutionSpace.indexPlusOneTo(op);
		}
		
		for(Operation op : applyPlus){
			_solutionSpace.indexMinusOneTo(op);
		}
		
		// Ajout dans une liste les opérateurs dans l'ordre
		for(double d : deltaMap.keySet()){
			for(Operation i : deltaMap.get(d)){
				orderedList.add(i);
			}
		}
		
		pb = noiseEvaluationProcess(_solutionSpace); // bruit de référence
		for(Operation op : orderedList){
			_solutionSpace.indexPlusOneTo(op);
			pbPlusOne = noiseEvaluationProcess(_solutionSpace);
			if(pbPlusOne < pb)
				break;
		}
	}

}
