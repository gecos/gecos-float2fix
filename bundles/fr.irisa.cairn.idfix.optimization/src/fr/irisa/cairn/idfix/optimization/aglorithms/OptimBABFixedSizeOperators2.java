//package fr.irisa.cairn.idfix.optimization.aglorithms;
//
//import java.util.EnumMap;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Map;
//import java.util.SortedMap;
//
//import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification;
//import fr.irisa.cairn.idfix.model.fixedpointspecification.Operator;
//import fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind;
//import fr.irisa.cairn.idfix.optimization.utils.DefaultOptimization;
//import fr.irisa.cairn.idfix.simulation.noiseevaluation.optimization.INoiseFunctionResultSimulator;
//import fr.irisa.cairn.idfix.utils.exceptions.OptimException;
//import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;
//
//public class OptimBABFixedSizeOperators2 extends DefaultOptimization {
//	private Map<OperatorKind, List<Operator>> _operatorSortedByKind;
//	private List<OperatorKind> _operatorKindSorted;
//	
//	public OptimBABFixedSizeOperators2(float userConstraint, String operatorLibrary, FixedPointSpecification fixedPointSpecification, boolean displayChart, INoiseFunctionResultSimulator simulator) {
//		super("Branch and Bound with fixed size operators 2", userConstraint, operatorLibrary, fixedPointSpecification, 	displayChart, simulator);
//		
//		_operatorSortedByKind = new EnumMap<OperatorKind, List<Operator>>(OperatorKind.class);
//		
//	}
//
//	@Override
//	protected boolean optimAlgorithm() throws IllegalAccessException,
//			OptimException, SimulationException {
//		
//		createOperatorKindSolutionSpace();
//		
//		return false;
//	}
//
//	private void createOperatorKindSolutionSpace(){
//		List<Operator> list;
//		for(Operator operator : _solutionSpace.getOperators()){
//			if(_operatorSortedByKind.containsKey(operator.getKind())){
//				_operatorSortedByKind.get(operator.getKind()).add(operator);
//			}
//			else{
//				list = new LinkedList<Operator>();
//				list.add(operator);
//				_operatorSortedByKind.put(operator.getKind(), list);
//			}
//		}
//		
//		_operatorKindSorted = new LinkedList<OperatorKind>(_operatorSortedByKind.keySet());
//	}
//	
//	private void branchAndBound() throws OptimException{
//		float cost;
//		float noise;
//		
//		
//		
//		for(OperatorKind operatorKind : _operatorKindSorted){
//			for(int index = 0 ; index < _solutionSpace.getIndexMaxOf(operatorKind) ; index++){
//				setIndexOperatorKind(operatorKind, index);
//				cost = evalCostOptimistic(operatorKind);
//				noise = evalNoiseOptimistic(operatorKind);
//				if(cost < _costSolutionResult && noise < _userConstraint){
//					i
//				}
//			}
//			
//			
//			
//			if(cost < _costSolutionResult && noise < _userConstraint){
//				
//			}
//		}
//	}
//	
//	private void setIndexOperatorKind(OperatorKind operatorKind, int index){
//		for(Operator operator : _operatorSortedByKind.get(operatorKind)){
//			_solutionSpace.setOperatorTo(operator, index);
//		}
//	}
//	
//	private float evalNoiseOptimistic(OperatorKind operatorKind) throws OptimException{
//		OperatorKind opKind;
//		int indexList = _operatorKindSorted.indexOf(operatorKind);
//		for(int i = indexList+1 ; i < _operatorKindSorted.size() ; i++){
//			opKind = _operatorKindSorted.get(i);
//			for(Operator op : _operatorSortedByKind.get(opKind)){
//				_solutionSpace.setOperatorTo(op, _solutionSpace.getMaxIndexOf(op));
//			}
//		}
//		
//		return super.evalpb(_solutionSpace);
//	}
//	
//	private float evalCostOptimistic(OperatorKind operatorKind){
//		OperatorKind opKind;
//		int indexList = _operatorKindSorted.indexOf(operatorKind);
//		for(int i = indexList+1 ; i < _operatorKindSorted.size() ; i++){
//			opKind = _operatorKindSorted.get(i);
//			for(Operator op : _operatorSortedByKind.get(opKind)){
//				_solutionSpace.setOperatorTo(op, 0);
//			}
//		}
//		
//		return super.evalC(_solutionSpace);
//	}
//}
