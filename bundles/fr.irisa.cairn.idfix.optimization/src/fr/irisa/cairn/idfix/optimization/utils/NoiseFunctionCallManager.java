package fr.irisa.cairn.idfix.optimization.utils;

import fr.irisa.cairn.idfix.model.fixedpointspecification.Data;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Operation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE;
import fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace;
import fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_LOADER;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.exceptions.OptimException;

/**
 * Interface used by the optimization process to run the noise library from a optimization solution space.
 * 
 * @author nicolas simon
 *
 */
public class NoiseFunctionCallManager {
	
	public static double call(NOISE_FUNCTION_LOADER loader, SolutionSpace space) throws OptimException{
		double ret;
		int maxCDFG = 0;
		int tabTriplet[];
		int WFPquantMode[];
		int[] tabInputWdquantMode;
		int[] tabDataWidth;
		for(Operation op : space.getOperators()){
			if(op.getEval_cdfg_number() > maxCDFG){
				maxCDFG = op.getEval_cdfg_number();
			}
		}
		
		// OPERATOR
		tabTriplet = new int[(maxCDFG + 1) * 3];
		WFPquantMode = new int[tabTriplet.length]; // Init a 0 de base => troncature
		for(Operation op : space.getOperators()){
			if(op.getOperands().size() != 3){
				throw new RuntimeException("Operator with operand different of 3 are not managed yet is the noise function expression");
			}
			else{
				for(int i = 0 ; i < 3 ; i++){
					tabTriplet[op.getEval_cdfg_number()*3 + i] = op.getOperands().get(i).getFractionalWidth();
					WFPquantMode[op.getEval_cdfg_number()* 3 + i] = getQuantificationID(op.getOperands().get(i).getQuantification());
				}
			}
		}
		
		// DATA
		maxCDFG = 0;
		for(Data data : space.getDatas()){
			if(data.getCDFGNumber() > maxCDFG)
				maxCDFG = data.getCDFGNumber();
		}
		
		tabDataWidth = new int[maxCDFG + 1]; // width of Data specified by the user (pragma WIDTH), -1 otherwise
		tabInputWdquantMode = new int[tabDataWidth.length]; // Init a 0 de base => troncature
		for(int i = 0 ; i < tabDataWidth.length ; i++){
			tabDataWidth[i] = ConstantPathAndName.MAX_DATA_BITWIDTH;
		}
		for(Data data : space.getDatas()){
			if (data.getFixedInformation().isFixedBitWidth()){ // if width specified by the user
				tabDataWidth[data.getCDFGNumber()] = data.getFixedInformation().getFractionalWidth();
			}
			
			tabInputWdquantMode[data.getCDFGNumber()] = getQuantificationID(data.getFixedInformation().getQuantification());
		}
				
		// CALL NOISE FUNCTION
		if(loader == NOISE_FUNCTION_LOADER.JNI)
			ret = NEvalLibNatives.computePbCC(tabTriplet, WFPquantMode, tabDataWidth, tabInputWdquantMode);
		else if(loader == NOISE_FUNCTION_LOADER.JNA)
			ret = CNoiseFunctionLibraryManager.compute(tabTriplet, WFPquantMode, tabDataWidth, tabInputWdquantMode);
		else
			throw new OptimException("Tools to run native code is not correct. JNI or JNA can be used");
		
		if(Double.isNaN(ret)){
			for(int i = 0 ; i<tabTriplet.length ; i++){
				System.out.println("tabTriplet["+i+"] = " + tabTriplet[i]);
			}
			System.out.println();
			for(int i = 0 ; i < tabDataWidth.length ; i++){
				System.out.println("tabInputWd["+i+"] = " + tabDataWidth[i]);
			}
			throw new OptimException("SNR result obtain from the noise function is NaN");
		}
		
		return ret;
	}
	
	private static int getQuantificationID(QUANTIFICATION_TYPE quantification){
		switch (quantification) {
		case TRUNCATION:
			return 0;
			
		case ROUNDING:
			return 1;
		default:
			throw new RuntimeException("Missing quantification checking");
		}
	}
}
