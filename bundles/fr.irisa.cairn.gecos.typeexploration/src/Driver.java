
public class Driver {

	
	public static void main(String[] args) {
		int N = 11;
		double gaussian_stddev = 1.5;
  		double gaussian_filter[][] = new double[N][N];

		int center = (N/2);
  		double total = 0;
  		double unnormalized[][] = new double[N][N];
		double gaussian_variance=gaussian_stddev*gaussian_stddev;
		
  	  	for (int y = 0; y < N; y++){
			for (int x = 0; x < N; x++){
         				double dist = Math.abs(x-center)*Math.abs(x-center)+Math.abs(y-center)*Math.abs(y-center);
				unnormalized[x][y] = Math.exp(-0.5*dist/gaussian_variance);
				total = total + unnormalized[x][y];
  			}
		}
  	  	for (int y = 0; y < N; y++){
			for (int x = 0; x < N; x++){
				gaussian_filter[x][y] = unnormalized[x][y] / total;
				System.err.println(gaussian_filter[x][y]);
			}
  	  	}
	}
}
