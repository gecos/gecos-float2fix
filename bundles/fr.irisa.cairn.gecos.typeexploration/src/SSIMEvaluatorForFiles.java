import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import fr.irisa.cairn.gecos.core.profiling.backend.ProfilingInfo;
import fr.irisa.cairn.gecos.core.profiling.backend.ProfilingInfoParser;
import fr.irisa.cairn.gecos.typeexploration.accuracy.SSIMEvaluator;

/**
 * Class with method for computing SSIM directory from output files instead of going through the full type exploration flow.
 * 
 * @author tyuki
 *
 */
public class SSIMEvaluatorForFiles {

	
	public static void main(String[] args) {
		Path path1 = Paths.get("/Users/tyuki/projects/GeCoS/gecos-float2fix/bundles/fr.irisa.cairn.gecos.typeexploration.demo/demos/nlm_png/outputs/nlm/2018-09-12-10:54:32/exploration/simulations/ref/profiling-files");
		Path path2 = Paths.get("/Users/tyuki/projects/GeCoS/gecos-float2fix/bundles/fr.irisa.cairn.gecos.typeexploration.demo/demos/nlm_png/outputs/nlm/2018-09-12-10:54:32/exploration/simulations/sol1/profiling-files");

		double ssim = evaluate(384, 512, path1, path2, 1);
		System.out.println(ssim);
	}
	
	private static double evaluate(int H, int W, Path dir1, Path dir2, int nbSims) {
		
		try {
			Map<Integer, ProfilingInfo<? extends Number>> variablesInfo1 = new ProfilingInfoParser(dir1).retrieve();
			Map<Integer, ProfilingInfo<? extends Number>> variablesInfo2 = new ProfilingInfoParser(dir2).retrieve();
			
			if (variablesInfo1.size() != 1 || variablesInfo2.size() != 1) {
				throw new RuntimeException("Expecting exactly one output file.");
			}
			
			ProfilingInfo<? extends Number> profilingInfo1 = variablesInfo1.values().iterator().next();
			ProfilingInfo<? extends Number> profilingInfo2 = variablesInfo2.values().iterator().next();

			double ssim = 1.0;
			for (int i = 0; i < nbSims; i++) {
				Number[] array1 = profilingInfo1.getSnapshot(i);
				Number[] array2 = profilingInfo2.getSnapshot(i);
				
				SSIMEvaluator ssimEval = new SSIMEvaluator(W, H, array1, array2);
				ssim = Math.min(ssim, ssimEval.evaluate());
			}
			
			return ssim;
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return -1;
	}
}