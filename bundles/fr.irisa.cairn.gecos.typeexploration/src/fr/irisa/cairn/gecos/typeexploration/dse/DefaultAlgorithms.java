package fr.irisa.cairn.gecos.typeexploration.dse;

import fr.irisa.cairn.gecos.typeexploration.model.IExplorationAlgorithm;
import fr.irisa.cairn.gecos.typeexploration.model.ISolution;
import typeexploration.SolutionSpace;

public enum DefaultAlgorithms implements IExplorationAlgorithm {
	BRUTE_FORCE(new BruteForceExploration()),
	MIN_PLUS_ONE(new MinPlusOneExploration()),
	TABU_SEARCH(new TabuExploration()),
	ENUMERATION(new Enumeration())
	;

	private IExplorationAlgorithm algo;
	
	private DefaultAlgorithms(IExplorationAlgorithm algo) {
		this.algo = algo; 
	}
	
	@Override
	public ISolution explore(SolutionSpace solSpace, String[] options) throws NoSolutionFoundException {
		return algo.explore(solSpace, options);
	}
}