package fr.irisa.cairn.gecos.typeexploration.model;

import fr.irisa.cairn.gecos.typeexploration.cost.CostEvaluationException;

public interface ICostEvaluator {

	public static String FULL_APPLICATION = "FULL_APPLICATION";
	
	public void evaluate(ISolution solution) throws CostEvaluationException;
	
}
