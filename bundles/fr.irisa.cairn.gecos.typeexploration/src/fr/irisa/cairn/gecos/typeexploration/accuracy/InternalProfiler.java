package fr.irisa.cairn.gecos.typeexploration.accuracy;

import static fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION;
import static fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory.pragma;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.ALIAS;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.VOID;
import static java.util.stream.Collectors.toMap;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import fr.irisa.cairn.gecos.core.profiling.SimulationBasedProfiler;
import fr.irisa.cairn.gecos.core.profiling.backend.ProfilingBackendFailure;
import fr.irisa.cairn.gecos.core.profiling.backend.ProfilingInfo;
import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.c.generator.XtendCGenerator;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.utils.misc.PathUtils;
import fr.irisa.cairn.gecos.typeexploration.Components;
import fr.irisa.cairn.gecos.typeexploration.dse.ExplorationMode;
import fr.irisa.cairn.gecos.typeexploration.model.ISolution;
import fr.irisa.cairn.gecos.typeexploration.model.UserFactory;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.types.StorageClassSpecifiers;
import gecos.types.Type;
import typeexploration.SolutionSpace;

class InternalProfiler {
		
		/**
		 * 
		 */
		private final SimulationAccuracyEvaluator simulationAccuracyEvaluator;
		ISolution referenceSolution;
		SimulationBasedProfiler prof;
		private ITypeParamGenerator generator;
		private Map<Symbol, String> symbolGenericTypeNameMap;
		
		
		public InternalProfiler(SimulationAccuracyEvaluator simulationAccuracyEvaluator) {
			this.simulationAccuracyEvaluator = simulationAccuracyEvaluator;
			this.prof = new SimulationBasedProfiler(this.simulationAccuracyEvaluator.project, false, false, this.simulationAccuracyEvaluator.logger);
			this.generator = Objects.requireNonNull(Components.getInstance().getTypesGenerator(), "No Types generator component was registerd!");
					
			/** 
			 * Replace solution space symbols base types with Macros.
			 */
			SolutionSpace solutionSpace = Components.getInstance().getSolutionSpaceBuilder().getSolutionSpace();
			replaceSymbolTypesWithMacros(solutionSpace);
			
			/** Insert instrumentation code **/
			this.simulationAccuracyEvaluator.logger.info("                - Transforming code: add instrumentation");
			GecosProject projectCopyWithNoDirectives = prof.insertProfiling(Components.getInstance().getConfiguration().getNbSimulations());
			
			/** generate generic instrumented code **/
			Path originalNoDirectivesOutputDir = PathUtils.createDir(this.simulationAccuracyEvaluator.rootOutDir, "code-no-instrumentation", true);
			this.simulationAccuracyEvaluator.logger.info("                - Generating original code without directives in: " + originalNoDirectivesOutputDir);
			new XtendCGenerator(projectCopyWithNoDirectives, originalNoDirectivesOutputDir.toString()).compute();
			
			Path refImplOutputDir = PathUtils.createDir(this.simulationAccuracyEvaluator.rootOutDir, "code-instrumented", true);
			this.simulationAccuracyEvaluator.logger.info("                - Generating Instrumented code in: " + refImplOutputDir);
			prof.generateProfiledCode(refImplOutputDir);
		}
		
	
		private void replaceSymbolTypesWithMacros(SolutionSpace solutionSpace) {
			this.symbolGenericTypeNameMap = new HashMap<>();
			this.referenceSolution = UserFactory.solution(solutionSpace);
			
			/** replace type of s by a alias */
			solutionSpace.getSymbols().forEach(s->generalizeType(solutionSpace, s));
				
			/** 
			 * if multiple symbol have same generic type name, this mean that multiple symbols
			 * use the same typedef.
			 * This is currently not supported! 
			 */
			Map<String, Symbol> genericTypeNameToSymbol = new HashMap<>();
			for(Symbol symb : symbolGenericTypeNameMap.keySet()) {
				String name = symbolGenericTypeNameMap.get(symb);
				if(genericTypeNameToSymbol.containsKey(name)) {
					//TODO This can be solved by adding a SAME constraint between both symbols
					// but this should better be done before starting explorations!
					throw new RuntimeException("Not yet supported: Multiple symbols cannot use the same type defined with a 'typedef'! " 
							+ symb + " and " + genericTypeNameToSymbol.get(name));
				} else
					genericTypeNameToSymbol.put(name, symb);
			}
		}

		private void generalizeType(SolutionSpace sol, Symbol s) {
			GecosUserTypeFactory.setScope(s.getContainingScope().getRoot());
			TypeAnalyzer ta = new TypeAnalyzer(s.getType());
			
			String genericTypeName;
			if(ta.getBaseLevel().getAlias() != null) {
				// TODO: 
				// if symbol base type is already an alias => keep using it as the generic type but only change its definition
				// This will enable a generic use of this type in the code (like casting, sizeof ...)
				// without it becoming hard coded causing compilation errors
				
				genericTypeName = ta.getBaseLevel().getAlias().getName();
				setGenericTypeName(s, genericTypeName);
				
				//XXX 
				// this is a hack that only works if typedef is defined in the scope of s.
				// It is needed currently because the (alias) types are copied for each typedElement 
				// (i.e. they do not refer to the same type, so changing the type does not affect others ..) 
				Type lookupAliasType = s.getContainingScope().lookupAliasType(genericTypeName);
				if(lookupAliasType != null)
					pragma(lookupAliasType, CODEGEN_IGNORE_ANNOTATION);
			} else {
				/* create a macro generic Type */
				genericTypeName = getGenericTypeName(sol, s);
			}
			
			Type generic = ALIAS(VOID(), genericTypeName);
			pragma(generic, CODEGEN_IGNORE_ANNOTATION);
			
			
			/* replace Symbol's type with the generic one */
			s.setType(ta.revertAllLevelsOn(generic));
			
			/* save original (reference) type definition */
			Type refTypeBase = ta.getBase().copy();
			// Qualifiers are kept with the generic type so we remove all of them
			refTypeBase.setConstant(false);
			refTypeBase.setStorageClass(StorageClassSpecifiers.NONE);
			refTypeBase.setVolatile(false);
			
			referenceSolution.setType(s, UserFactory.genericTypeParam(refTypeBase));
		}
		
		int uid = 0;
		private String getGenericTypeName(SolutionSpace sol, Symbol s) {
			String name = symbolGenericTypeNameMap.get(s);
			if(name == null) {
				name = sol.getUniqueTypeName(s);
				setGenericTypeName(s, name);
			}
			return name;
		}
		
		private void setGenericTypeName(Symbol s, String name) {
			symbolGenericTypeNameMap.put(s, name);
		}
	
		Map<Symbol, ProfilingInfo<? extends Number>> runSimulationAndProfile(ISolution solution, String solIDName, int nbSimus, int initSeedValue)
				throws ProfilingBackendFailure {
			Path outDir;
			synchronized (this) {
				outDir = PathUtils.createDir(this.simulationAccuracyEvaluator.simulationsDir, solIDName, true);
			}
			
			Map<String, String> typeDefMacros = solution.getSymbols().stream()
					.collect(toMap(	s -> getGenericTypeName(solution.getSolutionSpace(), s), 
									s -> generator.generate(solution.getType(s))));

			if(solIDName.equals(SimulationAccuracyEvaluator.REF_IMPLEM_NAME))
				typeDefMacros.put(SimulationAccuracyEvaluator.GECOS_REF_IMPL_MACRO, "1");
			
			ExplorationMode expMode = Components.getInstance().getExplorationMode();
			boolean useFixed = true;
			boolean useFloat = true;
			if(expMode == ExplorationMode.FIXED) useFloat = false;
			if(expMode == ExplorationMode.FLOAT) useFixed = false;
			
			Map<Symbol, ProfilingInfo<? extends Number>> profiledSymbols = prof.profile(outDir, 
					generator.getIncDirs(useFixed, useFloat), 
					generator.getLibDirs(useFixed, useFloat), 
					generator.getLibs(useFixed, useFloat), 
					generator.getHeaders(useFixed, useFloat),
					typeDefMacros, nbSimus, initSeedValue);
			
			return profiledSymbols;
		}
		
		
//		/** 
//		 * Cast operands of ?: to the destination's type in case it is a SetInstruction
//		 * Otherwise we may get an error if each operand has a different type)
//		 */
//		private void castOperandsofMux(Symbol s) {
//			//FIXME if symbol is use outside ps => need to propagate in those procedure sets as well
//			ProcedureSet ps = EcoreUtil2.getContainerOfType(s, ProcedureSet.class);
//			Objects.requireNonNull(ps, "Symbol is not contained in a procedureSet: " + s);
//			
//			Set<GenericInstruction> muxes = Streams.stream(ps.eAllContents())
//				.filter(SymbolInstruction.class::isInstance)
//				.map(SymbolInstruction.class::cast)
//				.filter(symref -> symref.getSymbol() == s) // symref refers to s
//				.map(symref -> EObjectUtils.getContainerOfType(symref, GenericInstruction.class, symref.getRoot(), 
//						g -> g.getName().equals(SelectOperator.MUX.getLiteral())))
//				.filter(Objects::nonNull)
//				.collect(toSet());
//			
//			muxes.forEach(mux -> {
//					Type type = null;
//					if(mux.getRoot() instanceof SetInstruction)
//						type = ((SetInstruction) mux.getRoot()).getDest().getType();
//						
//					if(type != null) {
//						System.out.println("$$$$ " + s);
//						muxes.forEach(i -> System.out.println(i.getRoot() + " at line " + i.getRoot().getFileLocation()));
//						
//						EcoreUtil.replace(mux.getOperand(1), cast(type, mux.getOperand(1).copy()));
//						EcoreUtil.replace(mux.getOperand(2), cast(type, mux.getOperand(2).copy()));
//					}
//				});
//		}

	}