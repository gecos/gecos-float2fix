package fr.irisa.cairn.gecos.typeexploration.utils;

import java.io.IOException;
import java.nio.file.Path;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.statistics.HistogramType;

@SuppressWarnings("serial")
public class HistogramChart extends JAppFrame {
	
	protected HistogramDataset dataset;
	protected JFreeChart chart;
	protected ChartPanel chartPanel;

	public HistogramChart(String title, String nameFunction, String xName, String yName, HistogramType histogramType, double[] data, int bins){
		super(title);
		this.dataset = new HistogramDataset();
        dataset.setType(histogramType);
		dataset.addSeries(nameFunction, data, bins);
		
		this.chart = ChartFactory.createHistogram(title, xName, yName, dataset, PlotOrientation.VERTICAL, true, true, true);
		this.chartPanel = new ChartPanel(chart);
		this.setContentPane(chartPanel);
		this.pack();
		this.setVisible(true);
	}
	
	public void saveAsJpeg(Path outputDir, String filename) {
		try {
			ChartUtils.saveChartAsJPEG(outputDir.resolve(filename + ".jpeg").toFile(), chart, this.getWidth(), this.getHeight());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}