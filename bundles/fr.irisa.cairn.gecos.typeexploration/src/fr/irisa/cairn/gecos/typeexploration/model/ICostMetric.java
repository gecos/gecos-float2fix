package fr.irisa.cairn.gecos.typeexploration.model;

import java.util.function.Function;

public class ICostMetric extends IMetric {

	public static ICostMetric createMetric(String description, boolean higherIsBetter, Function<ISolution, Double> metric, double normalizationRangeLB) {
		ICostMetric m = new ICostMetric();
		m.description = description;
		m.higherIsBetter = higherIsBetter;
		m.metric = metric;
		m.normalizationRangeLB = normalizationRangeLB;
		return m;
	}
	
}