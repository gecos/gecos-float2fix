package fr.irisa.cairn.gecos.typeexploration.accuracy;

import static java.util.stream.Collectors.toList;

import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.IntStream;

import org.jfree.data.statistics.HistogramType;

import fr.irisa.cairn.gecos.core.profiling.backend.ErrorStats;
import fr.irisa.cairn.gecos.core.profiling.backend.ProfilingBackendFailure;
import fr.irisa.cairn.gecos.core.profiling.backend.ProfilingInfo;
import fr.irisa.cairn.gecos.model.utils.misc.PathUtils;
import fr.irisa.cairn.gecos.typeexploration.Components;
import fr.irisa.cairn.gecos.typeexploration.Configuration;
import fr.irisa.cairn.gecos.typeexploration.TypesExploration;
import fr.irisa.cairn.gecos.typeexploration.model.IAccuracyEvaluator;
import fr.irisa.cairn.gecos.typeexploration.model.ISolution;
import fr.irisa.cairn.gecos.typeexploration.utils.BitHistogramChart;
import fr.irisa.cairn.gecos.typeexploration.utils.HistogramChart;
import fr.irisa.cairn.gecos.typeexploration.utils.ImageViewer;
import fr.irisa.cairn.gecos.typeexploration.utils.SolutionSpaceUtils;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;

/**
 * @author aelmouss
 */
public class SimulationAccuracyEvaluator implements IAccuracyEvaluator {

	public static final String GECOS_TYPE_EXPL_MACRO = "__GECOS_TYPE_EXPL__";
	public static final String GECOS_REF_IMPL_MACRO = "__GECOS_REF_IMPL__";
	
	public static final String METRIC_NAME_MAX_ABS_ERR = "MAX_ABS_ERR";
	public static final String METRIC_NAME_PSNR        = "PSNR";
	public static final String METRIC_NAME_PSNR_DB     = "PSNR_DB";
	public static final String METRIC_NAME_POWER       = "POWER";
	public static final String METRIC_NAME_POWER_DB    = "POWER_DB";
	//TODO add other metric available in ErrorStats
	
	public static final String METRIC_NAME_SSIM    		= "SSIM";
	
	static final String REF_IMPLEM_NAME = "ref";

	Logger logger;
	
	Path rootOutDir;
	Path simulationsDir;
	GecosProject project;
	private Configuration config;
	
	private InternalProfiler profiler;
	private IAccuracyMetricCalculator accuracyMetricCalculator;
	private Map<Symbol, ProfilingInfo<? extends Number>> referenceProfilingData;

//	private boolean enableCaching = true;
//	private List<ISolution> cache;
	
	
	public SimulationAccuracyEvaluator(GecosProject project) {
		this.project = project;
	}
	
	public GecosProject getProject() {
		return project;
	}
	
	public GecosProject getProjectCopyWithoutDirectives() {
		return profiler.prof.getProjectCopyWithNoDirectives();
	}
	
	/**
	 * Do not call this in a constructor (since Components might not be yet initialized)!
	 * @throws AccuracyEvaluationException
	 */
	public void initialize() throws AccuracyEvaluationException {
		Components comp = Components.getInstance();
		this.logger = comp.getExploartionLogger();
		this.rootOutDir = comp.getOutputLoactions().getExplorationDir();
		this.accuracyMetricCalculator = comp.getAccuracyMetricCalculator();
		this.config = comp.getConfiguration();
		
		this.simulationsDir = PathUtils.createDir(rootOutDir, "simulations", true);
		this.profiler = new InternalProfiler(this);
		
		/* profile the reference implementation */
		try {
			this.referenceProfilingData = profiler.runSimulationAndProfile(profiler.referenceSolution, REF_IMPLEM_NAME, this.config.getNbSimulations(), -1);
		} catch (ProfilingBackendFailure e) {
			throw new AccuracyEvaluationException("Error occured while simulating reference solution", e);
		}
	}
	
	//XXX
	@Override
	public Map<Symbol, ProfilingInfo<? extends Number>> getReferenceProfilingInfo() throws AccuracyEvaluationException {
		synchronized (this) {
			if(profiler == null)
				initialize();
		}
		return referenceProfilingData;
	}
	
	
	/**
	 * @throw AccuracuEvaluationException if fails
	 */
	@Override
	public void evaluate(ISolution solution) throws AccuracyEvaluationException {
		if(profiler == null)
			throw new RuntimeException("Not initialized! must call initialize() or getReferenceProfilingInfo() first");
		
		
		//TODO
//		if(enableCaching) {
//			int idx = cache.indexOf(solution);
//			if(idx >= 0) {
//				solution.getErrorStats() cache.get(idx);
//			}
//		}
		
		Map<Symbol, ProfilingInfo<? extends Number>> symbolValues = null;
		try {
			symbolValues = profiler.runSimulationAndProfile(solution, "sol" + solution.getID(), this.config.getNbSimulations(), -1);
		} catch(Exception e) {
			throw new AccuracyEvaluationException("Error occured while simulating solution " + solution.getID(), e);
		}
		
		try {
			Map<Symbol, List<ErrorStats>> snapshotErrorStatsMap = computeError(solution, symbolValues);
			ErrorStats errorStats = accuracyMetricCalculator.compute(snapshotErrorStatsMap);
			solution.addAccuracy(METRIC_NAME_MAX_ABS_ERR, errorStats.getAbsoluteMax());
			// TODO: when Gecos profiling code is released, use following:
			// solution.addAccuracy(METRIC_NAME_MAX_ABS_ERR, errorStats.getDynamicRange());
			solution.addAccuracy(METRIC_NAME_PSNR       , errorStats.getPSNR());
			solution.addAccuracy(METRIC_NAME_PSNR_DB    , errorStats.getPSNRdB());
			solution.addAccuracy(METRIC_NAME_POWER      , errorStats.getPower());
			solution.addAccuracy(METRIC_NAME_POWER_DB   , errorStats.getPowerdB());
			//TODO add other metric available in ErrorStats
			
			//XXX this should be optional
			snapshotErrorStatsMap.keySet().forEach(s -> solution.setErrorStats(s, snapshotErrorStatsMap.get(s)));
		} catch(Exception e) {
			throw new AccuracyEvaluationException("Error occured while compute error for solution " + solution.getID(), e);
		}
		
		//SSIM calculation
		if (config.isSSIMEnabled()) {
			Optional<Symbol> target = symbolValues.keySet().stream().filter(s->s.getName().contentEquals(config.getSSIMtarget())).findFirst();
			if (target.isPresent()) {
				Symbol targetVal = target.get();
				int numSS = referenceProfilingData.get(targetVal).getNbSnapshots();
				if (numSS != symbolValues.get(targetVal).getNbSnapshots()) {
					throw new RuntimeException("Number of snapshots for the SSIM target variable does not match that of the reference run.");
				}

				double ssim = 1.0;
				for (int i = 0; i < numSS; i++) {
					SSIMEvaluator ssimEvaluator = SSIMEvaluator.fromSymbol(target.get(), 
							referenceProfilingData.get(targetVal).getSnapshot(i), symbolValues.get(targetVal).getSnapshot(i));
					ssim = Math.min(ssim, ssimEvaluator.evaluate());
				}
				
				solution.addAccuracy(METRIC_NAME_SSIM   , ssim);
			}
		}
		
		
		//XXX temporary for demo only
		{
			if(TypesExploration.SHOW_EXPLORAION_HISTOGRAMS) {
				double[] values = symbolValues.values().iterator().next().getAllSnapshotValues().mapToDouble(Number::doubleValue).toArray();
				
				try {
					new HistogramChart("Histogram", "reference out", "value", "relative frequency", HistogramType.RELATIVE_FREQUENCY, values, values.length);
				
					BitHistogramChart bitHisto = BitHistogramChart.ofDoubles("Bit Histogram " + solution.getID(), "values", HistogramType.FREQUENCY, values);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if(TypesExploration.SHOW_EXPLORAION_IMAGES) {
				ImageViewer imgViewer = new ImageViewer("Image " + solution.getID(), symbolValues.values().iterator().next().getSnapshotAsImage(0, 128, 128, 255));
				imgViewer.saveAsJpeg(simulationsDir.resolve("sol" + solution.getID()), "output-image");
			}
		}
	}

	/**
	 * Compute error:
	 * 
	 *!! Assumes Symbol are the same objects between reference version and current one
	 * (i.e.) the project is the same (no copy)
	 * If a copy was made for some reason we need to associate Symbols from both versions based
	 * on their UID.
	 * @return ErrorStats per Snapshot per Symbol
	 */
	private Map<Symbol, List<ErrorStats>> computeError(ISolution solution, Map<Symbol, ProfilingInfo<? extends Number>> symbolValues)
			throws AccuracyEvaluationException {

		Map<Symbol, List<ErrorStats>> snapshotErrorStatsMap = new LinkedHashMap<>();
		for(Symbol sym : referenceProfilingData.keySet()) {
			try {
				ProfilingInfo<? extends Number> refProfile = Objects.requireNonNull(referenceProfilingData.get(sym), 
						"Profiling info from reference implementation are not found for : " + sym);
				ProfilingInfo<? extends Number> curProfile = Objects.requireNonNull(symbolValues.get(sym), 
						"Profiling info from current implementation are not found for : " + sym);
				
				if (refProfile.getNbSnapshots() == 0 || refProfile.getNbSnapshots() != curProfile.getNbSnapshots())
					throw new AccuracyEvaluationException("Invalid or different number of snapshots between current and reference implementation: "
							+ refProfile.getNbSnapshots() + " vs " + curProfile.getNbSnapshots());
				
				List<ErrorStats> snapshotErrorStats = IntStream.range(0, refProfile.getNbSnapshots()).mapToObj(i -> {
					Number[] refSnap = refProfile.getSnapshot(i);
					Number[] curSnap = curProfile.getSnapshot(i);
					//XXX
					if(refSnap.length != curSnap.length)
						throw new RuntimeException("Number of values in snapshot " + i + " differ between current and "
								+ "reference implemetation: " +refSnap.length + " vs " + curSnap.length);
					return ErrorStats.ofDiff(refSnap, curSnap);
				}).collect(toList());
				
				snapshotErrorStatsMap.put(sym, snapshotErrorStats);
			} catch(Exception e) {
				throw new AccuracyEvaluationException("Failed to compute error for symbol " + SolutionSpaceUtils.printSymbol(sym), e);
			}
		}
		
		return snapshotErrorStatsMap;
	}
	
}
