package fr.irisa.cairn.gecos.typeexploration.model;

import static fr.irisa.cairn.gecos.model.utils.misc.StreamUtils.join;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Function;

import com.google.common.base.Preconditions;

public abstract class IMetric {

	protected String description;
	protected boolean higherIsBetter;
	protected Function<ISolution, Double> metric;
	protected double normalizationRangeLB;
	

	protected void setDescription(String description) {
		this.description = description;
	}

	protected void setHigherIsBetter(boolean higherIsBetter) {
		this.higherIsBetter = higherIsBetter;
	}

	protected void setMetric(Function<ISolution, Double> metric) {
		this.metric = metric;
	}
	
	protected void setNormalizationRangeLB(double lb) {
		normalizationRangeLB = lb;
	}
	
	public double getNormalizationRangeLB() {
		return normalizationRangeLB;
	}

	/**
	 * @return true to indicate that this metric is considered better 
	 * (i.e. improved) for higher values.
	 */
	public boolean isHigherBetter() {
		return higherIsBetter;
	}
	
	/**
	 * @param s solution
	 * @return a Function that when applied on a solution, returns its
	 * metric value. 
	 */
	public Optional<Double> getMetric(ISolution s) {
		try {
			return Optional.of(metric.apply(s));
		} catch (Exception e) {
			return Optional.empty();
		}
	}
	
	/**
	 * @return a String that describe this metric.
	 */
	public String getDescription() {
		return description;
	}
	
	
	@SuppressWarnings("unchecked")
	public static <T extends IMetric> T mergeMetrics(Collection<T> metrics) {
		if(metrics == null || metrics.isEmpty())
			return null;
		
		if(metrics.size() == 1)
			return metrics.iterator().next();
		
		Class<? extends IMetric> clazz = metrics.iterator().next().getClass();
		Preconditions.checkArgument(metrics.stream().allMatch(clazz::isInstance), "All metrics should be of the same type");
		
		try {
			T mergedMetric = (T) clazz.newInstance();
			mergedMetric.description = "Increasing Sum of: " + join(metrics.stream(), ", ", m -> m.getDescription());
			mergedMetric.higherIsBetter = true;
			mergedMetric.metric = sol -> metrics.stream()
				.mapToDouble(m -> m.getMetric(sol)
						.map(v -> m.isHigherBetter()? v : -v)
						.orElseThrow(() -> new RuntimeException("Metric value is not present")))
					.sum();
			return mergedMetric;
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
	
}