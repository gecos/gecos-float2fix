package fr.irisa.cairn.gecos.typeexploration.cost;

public class CostEvaluationException extends Exception {

	private static final long serialVersionUID = -2734485262335389600L;

	public CostEvaluationException(String string) {
		super(string);
	}

	public CostEvaluationException(String string, Exception e) {
		super(string, e);
	}
}
