package fr.irisa.cairn.gecos.typeexploration.utils;

import java.awt.event.WindowEvent;

import org.jfree.chart.ui.ApplicationFrame;

public class JAppFrame extends ApplicationFrame {

	public JAppFrame(String title) {
		super(title);
	}

    @Override
    public void windowClosing(WindowEvent event) {
        if (event.getWindow() == this) {
            dispose();
//            System.exit(0);
        }
    }
	
}
