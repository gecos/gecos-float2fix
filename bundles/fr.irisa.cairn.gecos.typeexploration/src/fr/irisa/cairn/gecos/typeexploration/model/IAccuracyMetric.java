package fr.irisa.cairn.gecos.typeexploration.model;

import java.util.function.Function;

public class IAccuracyMetric extends IMetric {

	public static IAccuracyMetric createMetric(String description, boolean higherIsBetter, Function<ISolution, Double> metric, double normalizationRangeLB) {
		IAccuracyMetric m = new IAccuracyMetric();
		m.description = description;
		m.higherIsBetter = higherIsBetter;
		m.metric = metric;
		m.normalizationRangeLB = normalizationRangeLB;
		return m;
	}
	
}