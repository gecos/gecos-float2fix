package fr.irisa.cairn.gecos.typeexploration.accuracy;

import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.ACFIXED;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.ACFLOAT;

import java.security.InvalidParameterException;

import fr.irisa.cairn.gecos.model.c.generator.ExtendableTypeCGenerator;
import gecos.types.ACFixedType;
import gecos.types.Type;
import typeexploration.FixedPointTypeParam;
import typeexploration.FloatPointTypeParam;
import typeexploration.GenericTypeParam;
import typeexploration.TypeParam;

/**
 * @author aelmouss
 */
public abstract class AbstractCodeGenerator implements ITypeParamGenerator {
	
	@Override
	public String generate(TypeParam t) {
		Type type = null;
		if(t instanceof FixedPointTypeParam)
			type = createFixedPointType((FixedPointTypeParam) t);
		else if(t instanceof FloatPointTypeParam)
			type  = createFloatingPointType((FloatPointTypeParam) t);
		else if(t instanceof GenericTypeParam)
			type = ((GenericTypeParam) t).getType();
		
		if(type == null)
			throw new InvalidParameterException("unknown type param: " + t);
		
		return ExtendableTypeCGenerator.eInstance.generate(type);
	}
	
	//! need to be synchronized since GecosUserTypeFactory is not thread safe
	private synchronized ACFixedType createFixedPointType(FixedPointTypeParam info) {
		return ACFIXED(info.getTotalWidth(), info.getIntegerWidth(), info.isSigned(), info.getQuantificationMode(), info.getOverflowMode());
	}
	
	private synchronized Type createFloatingPointType(FloatPointTypeParam t) {
		return ACFLOAT(t.getExponentWidth(), t.getTotalWidth()-t.getExponentWidth()-1); // 1 sign bit
	}
}