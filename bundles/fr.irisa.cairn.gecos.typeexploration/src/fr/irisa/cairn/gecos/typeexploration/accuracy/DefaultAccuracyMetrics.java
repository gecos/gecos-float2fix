package fr.irisa.cairn.gecos.typeexploration.accuracy;

import java.util.function.Function;

import fr.irisa.cairn.gecos.typeexploration.model.IAccuracyMetric;
import fr.irisa.cairn.gecos.typeexploration.model.ISolution;

public enum DefaultAccuracyMetrics {
	
	// Error signal stats
	MAX_ABS_ERR("Maximum Absolute Error", false, s -> s.getAccuracy(SimulationAccuracyEvaluator.METRIC_NAME_MAX_ABS_ERR), 1),
	PSNR("Peak Signal to Noise Ratio", true, s -> s.getAccuracy(SimulationAccuracyEvaluator.METRIC_NAME_PSNR), 1),
	PSNR_DB("Peak Signal to Noise Ratio in dB", true, s -> s.getAccuracy(SimulationAccuracyEvaluator.METRIC_NAME_PSNR_DB), 1),
	POWER("Noise Power", false, s -> s.getAccuracy(SimulationAccuracyEvaluator.METRIC_NAME_POWER), 1),
	POWER_DB("Noise Power in dB", false, s -> s.getAccuracy(SimulationAccuracyEvaluator.METRIC_NAME_POWER_DB), 1),
	//TODO many other metrics are available, just expose them here if needed..
	
	// Image quality metrics
	SSIM("Structural Similarity (SSIM)", true, s -> s.getAccuracy(SimulationAccuracyEvaluator.METRIC_NAME_SSIM), 0.1),
	;
	
	private IAccuracyMetric metric;
	
	private DefaultAccuracyMetrics(String description, boolean higherIsBetter, Function<ISolution, Double> accuracyMetric, double normalizationRangeLB) {
		metric = IAccuracyMetric.createMetric(description, higherIsBetter, accuracyMetric, normalizationRangeLB);
	}
	
	public IAccuracyMetric getMetric() {
		return metric;
	}
	
	public String describe() {
		return name() + ": " + metric.getDescription() + ". Higher values are " + (metric.isHigherBetter()? "more":"less") + " accurate";
	}
}