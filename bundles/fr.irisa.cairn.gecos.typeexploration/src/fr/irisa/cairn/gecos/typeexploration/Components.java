package fr.irisa.cairn.gecos.typeexploration;

import java.util.logging.Logger;

import fr.irisa.cairn.gecos.typeexploration.accuracy.IAccuracyMetricCalculator;
import fr.irisa.cairn.gecos.typeexploration.accuracy.ITypeParamGenerator;
import fr.irisa.cairn.gecos.typeexploration.dse.ExplorationMode;
import fr.irisa.cairn.gecos.typeexploration.model.IAccuracyEvaluator;
import fr.irisa.cairn.gecos.typeexploration.model.IAccuracyMetric;
import fr.irisa.cairn.gecos.typeexploration.model.ICostEvaluator;
import fr.irisa.cairn.gecos.typeexploration.model.ICostMetric;
import fr.irisa.cairn.gecos.typeexploration.model.IExplorationAlgorithm;
import fr.irisa.cairn.gecos.typeexploration.model.ISolutionSpaceBuilder;
import fr.irisa.cairn.gecos.typeexploration.model.IUserConstraint;
import typeexploration.computation.ComputationModel;

/**
 * @author aelmouss
 */
public class Components {
	
	OutputLocations outputLoc;
	Logger mainLogger;
	Logger expLogger;
	Logger expStatsLogger;
//	int maxNbThreads;
	
	Configuration configuration;
	ComputationModel computationModel;
	
	ISolutionSpaceBuilder solutionSpaceBuilder;
	
	IUserConstraint accuracyConstraint;
	
	ExplorationMode explorationMode;
	IExplorationAlgorithm explorationAlgorithm;
//	boolean enablePruning;
	ICostEvaluator costEvaluator;
	ICostMetric costMetric;
	IAccuracyEvaluator accuracyEvaluator;
	IAccuracyEvaluator ssimEvaluator;
	IAccuracyMetric accuracyMetric;
	IAccuracyMetricCalculator accuracyMetricCalculator;	//! Specific to SimulationAccuracyEvaluator
	
	ITypeParamGenerator typesGenerator;
	
	
	
	private static Components instance;
	
	static void setCompoments(Components components) {
		instance = components;
	}
	
	public static Components getInstance() {
		return instance;
	}
	
	public ISolutionSpaceBuilder getSolutionSpaceBuilder() {
		return instance.solutionSpaceBuilder;
	}
	
	public IExplorationAlgorithm getExplorationAlgorithm() {
		return instance.explorationAlgorithm;
	}
	
	public IAccuracyEvaluator getAccuEvaluator() {
		return instance.accuracyEvaluator;
	}

	public IAccuracyEvaluator getSSIMEvaluator() {
		return instance.ssimEvaluator;
	}
	
	public ICostEvaluator getCostEvaluator() {
		return instance.costEvaluator;
	}
	
	public OutputLocations getOutputLoactions() {
		return instance.outputLoc;
	}
	
	public ITypeParamGenerator getTypesGenerator() {
		return instance.typesGenerator;
	}
	
	public Logger getMainLogger() {
		return instance.mainLogger;
	}
	
	public Logger getExploartionLogger() {
		return instance.expLogger;
	}
	
	public Logger getExploartionStatsLogger() {
		return instance.expStatsLogger;
	}

	public IAccuracyMetricCalculator getAccuracyMetricCalculator() {
		return instance.accuracyMetricCalculator;
	}
	
	public IUserConstraint getAccuracyConstraint() {
		return instance.accuracyConstraint;
	}

//	public int getMaxNbThreads() {
//		return instance.maxNbThreads;
//	}
	
	public IAccuracyMetric getAccuracyMetric() {
		return instance.accuracyMetric;
	}
	
	public ICostMetric getCostMetric() {
		return instance.costMetric;
	}
	
//	public boolean isEnablePruning() {
//		return enablePruning;
//	}
	
	public ExplorationMode getExplorationMode() {
		return explorationMode;
	}
	
	public Configuration getConfiguration() {
		return configuration;
	}
}
