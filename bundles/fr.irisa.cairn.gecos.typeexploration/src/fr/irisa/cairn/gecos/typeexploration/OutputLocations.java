package fr.irisa.cairn.gecos.typeexploration;

import static fr.irisa.cairn.gecos.typeexploration.GlobalParameters.EXPLORATION_OUTPUT_FOLDER_NAME;
import static fr.irisa.cairn.gecos.typeexploration.GlobalParameters.LOGS_FODLER_NAME;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

import fr.irisa.r2d2.gecos.framework.utils.FileUtils;

public class OutputLocations {

	private final SimpleDateFormat dateFormater = new SimpleDateFormat("y-MM-dd-HH:mm:ss");
	
	private Path rootDir;
	private Path logsDir;
	private Path explorationDir;

	/**
	 * @param dir the output directory.
	 * @param timeTag if false (re)use the 'no-time-tag' directory. This is useful
	 * for debugging if you want outputs to remain in the same location across multiple runs.
	 * @param keepLast number of previous runs to keep. 
	 * Older runs outputs are deleted. Ignored if negative.
	 */
	public OutputLocations(Path dir, boolean timeTag, int keepLast) {
		try {
			cleanupOld(dir, keepLast);
		} catch (Exception e) {
			new RuntimeException("Failed to cleanup old output locations", e).printStackTrace();
		}
		
		if(timeTag)
			this.rootDir = dir.resolve(dateFormater.format((new Date())));
		else
			this.rootDir = dir.resolve("no-time-tag");
		
		this.logsDir = rootDir.resolve(LOGS_FODLER_NAME);
		this.explorationDir = rootDir.resolve(EXPLORATION_OUTPUT_FOLDER_NAME);
		
		try {
			mkdirs();
		} catch (Exception e) {
			throw new RuntimeException("Failed to create output ocations", e);
		}
	}

	private void cleanupOld(Path dir, int keepLast) throws IOException {
		if(keepLast >= 0 && dir.toFile().isDirectory()) {
			Files.list(dir)
				.filter(Files::isDirectory)
				.map(d -> d.getFileName().toString())
				.filter(dname -> dateFormater.parse(dname, new ParsePosition(0)) != null)
				.sorted((d1, d2) -> d2.compareTo(d1))
				.skip(keepLast)
				.forEach(dname -> FileUtils.deleteRecursive(dir.resolve(dname).toFile()));
		}
	}

	private void mkdirs() {
		logsDir.toFile().mkdirs();
		explorationDir.toFile().mkdirs();
	}
	
	/**
	 * @return the root output location of the current run.
	 */
	public Path getRootDir() {
		return rootDir;
	}
	
	public Path getLogsDir() {
		return logsDir;
	}
	
	public Path getExplorationDir() {
		return explorationDir;
	}
}
