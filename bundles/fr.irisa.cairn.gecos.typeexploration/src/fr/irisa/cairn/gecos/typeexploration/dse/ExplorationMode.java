package fr.irisa.cairn.gecos.typeexploration.dse;

public enum ExplorationMode {
	FIXED("Explore only fixed-point types"),
	FLOAT("Explore only custom floating-point types"),
	MIXED("Explore both floating-point and fixed-point types. This mode is not unsupported yet !") //! This mode is currently not fully supported
	;
	
	private String description;
	private ExplorationMode(String description) {
		this.description = description;
	}
	public String getDescription() {
		return description;
	}
}