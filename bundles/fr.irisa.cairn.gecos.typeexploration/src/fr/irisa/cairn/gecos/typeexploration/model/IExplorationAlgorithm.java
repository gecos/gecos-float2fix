package fr.irisa.cairn.gecos.typeexploration.model;

import fr.irisa.cairn.gecos.typeexploration.dse.NoSolutionFoundException;
import typeexploration.SolutionSpace;

/**
 * @author aelmouss
 */
public interface IExplorationAlgorithm {
	
	public ISolution explore(SolutionSpace solSpace, String[] options) throws NoSolutionFoundException;
	
}
