package fr.irisa.cairn.gecos.typeexploration.model;

import static java.util.Collections.emptyList;

import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import fr.irisa.cairn.gecos.model.utils.misc.StreamUtils;
import gecos.core.Symbol;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import gecos.types.Type;
import typeexploration.FixedPointTypeConfiguration;
import typeexploration.FixedPointTypeParam;
import typeexploration.FloatPointTypeConfiguration;
import typeexploration.FloatPointTypeParam;
import typeexploration.GenericTypeParam;
import typeexploration.SameTypeConstraint;
import typeexploration.SolutionSpace;
import typeexploration.TypeConfiguration;
import typeexploration.TypeConfigurationSpace;
import typeexploration.TypeConstraint;
import typeexploration.TypeexplorationFactory;

/**
 * @author aelmouss
 */
public class UserFactory {

	private UserFactory() {
	}

	private static final TypeexplorationFactory factory = TypeexplorationFactory.eINSTANCE;

	public static SolutionSpace emptySolutionSpace() {
		return factory.createSolutionSpace();
	}

	public static FixedPointTypeConfiguration defaultFixedConfiguration(Collection<Integer> totalWidthValues,
			Collection<Integer> integerWidthtValues, Collection<Integer> signValues) {
		Objects.requireNonNull(totalWidthValues, "totalWidthValues cannot be null");
		Objects.requireNonNull(integerWidthtValues, "integerWidthtValues cannot be null");
		Objects.requireNonNull(signValues, "signValues cannot be null");
		if (totalWidthValues.isEmpty() || integerWidthtValues.isEmpty() || signValues.isEmpty()) {
			throw new IllegalArgumentException("Default configuration should define all type attributes");
		}
		return fixedConfiguration(null, totalWidthValues, integerWidthtValues, signValues);
	}

	public static FixedPointTypeConfiguration fixedConfiguration(FixedPointTypeConfiguration defaultConfig,
			Collection<Integer> totalWidthValues, Collection<Integer> integerWidthtValues,
			Collection<Integer> signValues) {
		Objects.requireNonNull(totalWidthValues == null, "totalWidthValues cannot be null");

		FixedPointTypeConfiguration c = factory.createFixedPointTypeConfiguration();
		c.getTotalWidthValues().addAll(totalWidthValues);
		if (integerWidthtValues != null)
			c.getIntegerWidthValues().addAll(integerWidthtValues);
		else {
			if (defaultConfig == null) {
				throw new NullPointerException();
			} else {
				c.getIntegerWidthValues().addAll(defaultConfig.getIntegerWidthValues());
			}
		}
		if (signValues != null)
			c.getSigned().addAll(signValues.stream().map(i -> i != 0).collect(Collectors.toList()));
		else {
			if (defaultConfig == null) {
				throw new NullPointerException();
			} else {
				c.getSigned().addAll(defaultConfig.getSigned());
			}
		}

		// XXX
		c.getOverflowMode().add(OverflowMode.AC_WRAP);
		c.getQuantificationMode().add(QuantificationMode.AC_TRN);

		return c;
	}

	public static FloatPointTypeConfiguration defaultFloatConfiguration(Collection<Integer> totalWidthValues,
			Collection<Integer> exponentWidthValues, Collection<Integer> signValues) {
		Objects.requireNonNull(totalWidthValues, "totalWidthValues cannot be null");
		Objects.requireNonNull(exponentWidthValues, "exponentWidthValues cannot be null");
		Objects.requireNonNull(signValues, "signValues cannot be null");
		if (totalWidthValues.isEmpty() || exponentWidthValues.isEmpty() || signValues.isEmpty()) {
			throw new IllegalArgumentException("Default configuration should define all type attributes");
		}
		return floatConfiguration(null, totalWidthValues, exponentWidthValues, signValues);
	}

	public static FloatPointTypeConfiguration floatConfiguration(FloatPointTypeConfiguration defaultConfig,
			Collection<Integer> totalWidthValues, Collection<Integer> exponentWidthValues,
			Collection<Integer> signValues) {
		Objects.requireNonNull(totalWidthValues, "totalWidthValues cannot be null");

		FloatPointTypeConfiguration c = factory.createFloatPointTypeConfiguration();
		c.getTotalWidthValues().addAll(totalWidthValues);
		if (exponentWidthValues != null)
			c.getExponentWidthValues().addAll(exponentWidthValues);
		else {
			if (defaultConfig == null) {
				throw new NullPointerException();
			} else {
				c.getExponentWidthValues().addAll(defaultConfig.getExponentWidthValues());
			}
		}
		if (signValues != null)
			c.getSigned().addAll(signValues.stream().map(i -> i != 0).collect(Collectors.toList()));
		else {
			if (defaultConfig == null) {
				throw new NullPointerException();
			} else {
				c.getSigned().addAll(defaultConfig.getSigned());
			}
		}
		return c;
	}

	public static SameTypeConstraint sameTypeConstraint(Symbol asSymbol) {
		Objects.requireNonNull(asSymbol);

		SameTypeConstraint c = factory.createSameTypeConstraint();
		c.setSymbol(asSymbol);
		return c;
	}

	public static TypeConfigurationSpace configurationSpace(FixedPointTypeConfiguration fixedPtConfig,
			FloatPointTypeConfiguration floatPtConfig, Collection<TypeConstraint> constraints) {
		Objects.requireNonNull(constraints);

		TypeConfigurationSpace c = factory.createTypeConfigurationSpace();
		c.getConstraints().addAll(constraints);
		if (fixedPtConfig != null)
			c.setFixedPtCongurations(fixedPtConfig);
		if (floatPtConfig != null)
			c.setFloatPtCongurations(floatPtConfig);

		return c;
	}

	public static TypeConfigurationSpace configurationSpace(Collection<TypeConfiguration> configs,
			Collection<TypeConstraint> constraints) {
		FixedPointTypeConfiguration fixedPtConfig = StreamUtils
				.filterByType(configs.stream(), FixedPointTypeConfiguration.class).reduce((c1, c2) -> c1.mergeIn(c2))
				.orElse(null);
		FloatPointTypeConfiguration floatPtConfig = StreamUtils
				.filterByType(configs.stream(), FloatPointTypeConfiguration.class).reduce((c1, c2) -> c1.mergeIn(c2))
				.orElse(null);

		return configurationSpace(fixedPtConfig, floatPtConfig, constraints);
	}

	public static TypeConfigurationSpace configurationSpace(Collection<TypeConfiguration> configs) {
		return configurationSpace(configs, emptyList());
	}

	public static FixedPointTypeParam fixedTypeParam(int w, int i, boolean signed, QuantificationMode q,
			OverflowMode o) {
		FixedPointTypeParam t = factory.createFixedPointTypeParam();
		t.setTotalWidth(w);
		t.setIntegerWidth(i);
		t.setSigned(signed);
		t.setQuantificationMode(q);
		t.setOverflowMode(o);
		return t;
	}

	public static FloatPointTypeParam floatTypeParam(int w, int e, boolean signed) {
		FloatPointTypeParam t = factory.createFloatPointTypeParam();
		t.setTotalWidth(w);
		t.setExponentWidth(e);
		t.setSigned(signed);
		return t;
	}

	public static GenericTypeParam genericTypeParam(Type type) {
		GenericTypeParam t = factory.createGenericTypeParam();
		t.setType(type);
		return t;
	}

	private static AtomicInteger solId = new AtomicInteger(0);
	public static ISolution solution(SolutionSpace solSpace) {
		ISolution sol = new SolutionImpl(solSpace);
		sol.setID(solId.incrementAndGet());
		return sol;
	}

}
