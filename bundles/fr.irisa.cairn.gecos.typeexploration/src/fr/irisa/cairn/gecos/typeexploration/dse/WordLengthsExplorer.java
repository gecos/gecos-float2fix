package fr.irisa.cairn.gecos.typeexploration.dse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.Preconditions;

import fr.irisa.cairn.gecos.typeexploration.Components;
import fr.irisa.cairn.gecos.typeexploration.model.ISolution;
import fr.irisa.cairn.gecos.typeexploration.model.UserFactory;
import fr.irisa.cairn.gecos.typeexploration.solutionspace.SolutionSpaceAnalyzer;
import gecos.core.Symbol;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import typeexploration.FixedPointTypeConfiguration;
import typeexploration.FixedPointTypeParam;
import typeexploration.FloatPointTypeConfiguration;
import typeexploration.FloatPointTypeParam;
import typeexploration.NumberTypeConfiguration;
import typeexploration.SolutionSpace;
import typeexploration.TypeParam;

/**
 * Currently only explores wordlengths. For other type parameters are currently
 * fixed to their first possible value specified in the solution space.
 * 
 * @author aelmouss
 */
public class WordLengthsExplorer {

	protected SolutionSpaceAnalyzer analyzer;
	protected Map<Symbol, Integer> symbolWs;
	protected TypeParamExplorer typeParamsExplorer;

	private abstract class TypeParamExplorer {
		abstract List<Integer> getWidths(Symbol s);

		abstract TypeParamExplorer copy();

		abstract TypeParam createCurrentType(Symbol s, Integer currentW);
	}

	private class FixedPointParamExplorer extends TypeParamExplorer {
		protected Map<Symbol, Integer> symbolIs;
		protected Map<Symbol, Boolean> symbolSs;
		protected Map<Symbol, QuantificationMode> symbolQMods;
		protected Map<Symbol, OverflowMode> symbolSMods;

		FixedPointParamExplorer() {
			this.symbolIs = new HashMap<>();
			this.symbolSs = new HashMap<>();
			this.symbolQMods = new HashMap<>();
			this.symbolSMods = new HashMap<>();

			// XXX TMP: currently use the first possible value of each parameter.
			// TODO add support to specify different values for these parameters
			WordLengthsExplorer.this.analyzer.getSymbolsWithoutConstraints().forEach(s -> {
				FixedPointTypeConfiguration config = getFixedPointConfiguration(s);
				symbolIs.put(s, config.getIntegerWidthValues().get(0));
				symbolSs.put(s, config.getSigned().get(0));
				symbolQMods.put(s, config.getQuantificationMode().get(0));
				symbolSMods.put(s, config.getOverflowMode().get(0));
			});
		}

		private FixedPointTypeConfiguration getFixedPointConfiguration(Symbol s) {
			return WordLengthsExplorer.this.analyzer.getFixedPointConfigurationOrDefault(s);
		}

		@Override
		public List<Integer> getWidths(Symbol s) {
			NumberTypeConfiguration config = getFixedPointConfiguration(s);
			return config.getTotalWidthValues();
		}

		@Override
		public FixedPointParamExplorer copy() {
			FixedPointParamExplorer typeParam = new FixedPointParamExplorer();
			typeParam.symbolIs.putAll(symbolIs);
			typeParam.symbolSs.putAll(symbolSs);
			typeParam.symbolQMods.putAll(symbolQMods);
			typeParam.symbolSMods.putAll(symbolSMods);
			return typeParam;
		}

		@Override
		public FixedPointTypeParam createCurrentType(Symbol s, Integer currentW) {
			Integer i = symbolIs.get(s);
			Boolean sign = symbolSs.get(s);
			QuantificationMode q = symbolQMods.get(s);
			OverflowMode o = symbolSMods.get(s);
			return UserFactory.fixedTypeParam(currentW, i, sign, q, o);
		}
	}

	private class FloatingPointParamExplorer extends TypeParamExplorer {
		protected Map<Symbol, Integer> symbolEs;
		protected Map<Symbol, Boolean> symbolSs;

		public FloatingPointParamExplorer() {
			this.symbolEs = new HashMap<>();
			this.symbolSs = new HashMap<>();

			// XXX TMP: currently use the first possible value of each parameter.
			// TODO add support to specify different values for these parameters
			WordLengthsExplorer.this.analyzer.getSymbolsWithoutConstraints().forEach(s -> {
				FloatPointTypeConfiguration config = getFloatPointConfiguration(s);
				symbolEs.put(s, config.getExponentWidthValues().get(0));
				symbolSs.put(s, config.getSigned().get(0));
			});
		}

		private FloatPointTypeConfiguration getFloatPointConfiguration(Symbol s) {
			return WordLengthsExplorer.this.analyzer.getFloatPointConfigurationOrDefault(s);
		}

		@Override
		public List<Integer> getWidths(Symbol s) {
			FloatPointTypeConfiguration config = getFloatPointConfiguration(s);
			return config.getTotalWidthValues();
		}

		@Override
		public FloatingPointParamExplorer copy() {
			FloatingPointParamExplorer typeParam = new FloatingPointParamExplorer();
			typeParam.symbolEs.putAll(symbolEs);
			typeParam.symbolSs.putAll(symbolSs);
			return typeParam;
		}

		@Override
		public FloatPointTypeParam createCurrentType(Symbol s, Integer currentW) {
			return UserFactory.floatTypeParam(currentW, symbolEs.get(s), symbolSs.get(s));
		}
	}

	public WordLengthsExplorer(SolutionSpace solSpace) {
		this.analyzer = solSpace.getAnalyzer();
		this.symbolWs = new HashMap<>();

		ExplorationMode explorationMode = Components.getInstance().getExplorationMode();
		switch (explorationMode) {
		case FIXED:
			this.typeParamsExplorer = new FixedPointParamExplorer();
			break;
		case FLOAT:
			this.typeParamsExplorer = new FloatingPointParamExplorer();
			break;
		case MIXED:
		default:
			throw new RuntimeException("This exploration mode is not supported yet: " + explorationMode);
		}
	}

	protected WordLengthsExplorer() {
	}

	public WordLengthsExplorer copy() {
		WordLengthsExplorer copy = new WordLengthsExplorer();
		copy.analyzer = this.analyzer;
		copy.symbolWs = new HashMap<>(this.symbolWs);
		copy.typeParamsExplorer = this.typeParamsExplorer.copy();
		return copy;
	}

	protected Integer getCurrentW(Symbol s) {
		return symbolWs.get(s);
	}

	public Integer getCurrentWIdx(Symbol s) {
		return getWidths(s).indexOf(getCurrentW(s));
	}

	public int getMaxWIdx(Symbol s) {
		List<Integer> widths = getWidths(s);
		return widths.size() - 1;
	}

	public Integer getMaxW(Symbol s) {
		List<Integer> widths = getWidths(s);
		return widths.get(widths.size() - 1);
	}

	/**
	 * Unique and sorted (min first)
	 * 
	 * @param s
	 * @return
	 */
	protected List<Integer> getWidths(Symbol s) {
		return typeParamsExplorer.getWidths(s);
	}

	protected ISolution currentSolution;

	public ISolution createCurrentSolution() {
		ISolution sol = UserFactory.solution(analyzer.getSolSpace());
		analyzer.getSymbolsWithoutConstraints().forEach(s -> {
			Integer currentW = getCurrentW(s);
			TypeParam createCurrentType = typeParamsExplorer.createCurrentType(s, currentW);
			sol.setType(s, createCurrentType);
		});

		currentSolution = sol;
		return currentSolution;
	}

	public ISolution getLastCreatedSolution() {
		return currentSolution;
	}

	/**
	 * Set W(s) as the wIdx'th width in the ordered (min-first) list of all possible
	 * bit widths for s.
	 * 
	 * @param s
	 * @param wIdx
	 */
	public void setWTo(Symbol s, int wIdx) {
		List<Integer> widths = getWidths(s);
		Preconditions.checkArgument(wIdx >= 0 && wIdx < widths.size(), "index out of range", wIdx);
		Integer w = widths.get(wIdx);
		symbolWs.put(s, w);

		lastModifiedSymbol = s;
	}

	public void setWToMin(Symbol s) {
		setWTo(s, 0);
	}

	public void setWToMax(Symbol s) {
		setWTo(s, getMaxWIdx(s));
	}

	public void setAllWToMin() {
		analyzer.getSymbolsWithoutConstraints().forEach(this::setWToMin);
	}

	public void setAllWToMax() {
		analyzer.getSymbolsWithoutConstraints().forEach(this::setWToMax);
	}

	/**
	 * Set W of symbol the next higer one (i.e. current index+1) and return true, if
	 * present. Otherwise do nothing and return false.
	 * 
	 * @param symbol
	 * @return true if changed. false if symbol is already at its max W.
	 */
	public boolean setToNextHigherW(Symbol symbol) {
		int currentIdx = getCurrentWIdx(symbol);
		if (currentIdx < getMaxWIdx(symbol)) {
			setWTo(symbol, currentIdx + 1);
			return true;
		}
		return false;
	}

	/**
	 * Set W of symbol the next lower one (i.e. current index-1) and return true, if
	 * present. Otherwise do nothing and return false.
	 * 
	 * @param symbol
	 * @return true if changed. false if symbol is already at its min W.
	 */
	public boolean setToNextLowerW(Symbol symbol) {
		int currentIdx = getCurrentWIdx(symbol);
		if (currentIdx > 0) {
			setWTo(symbol, currentIdx - 1);
			return true;
		}
		return false;
	}

	protected Symbol lastModifiedSymbol;

	public Symbol getLastModifiedSymbol() {
		return lastModifiedSymbol;
	}

}