package fr.irisa.cairn.gecos.typeexploration.dse;

import static fr.irisa.cairn.gecos.model.utils.misc.LoggingUtils.anonymousLoggerToFile;
import static fr.irisa.cairn.gecos.typeexploration.utils.SolutionSpaceUtils.streamSymboltypeConfigs;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.stream.Stream;

import org.javatuples.Pair;

import com.google.common.collect.Lists;
import com.google.common.collect.Streams;

import fr.irisa.cairn.gecos.model.utils.misc.StreamUtils;
import fr.irisa.cairn.gecos.model.utils.misc.ThreadUtils;
import fr.irisa.cairn.gecos.typeexploration.Components;
import fr.irisa.cairn.gecos.typeexploration.accuracy.AccuracyEvaluationException;
import fr.irisa.cairn.gecos.typeexploration.cost.CostEvaluationException;
import fr.irisa.cairn.gecos.typeexploration.model.ISolution;
import fr.irisa.cairn.gecos.typeexploration.model.UserFactory;
import fr.irisa.cairn.gecos.typeexploration.solutionspace.SolutionSpaceAnalyzer;
import typeexploration.SolutionSpace;
import typeexploration.TypeParam;

/**
 * @author aelmouss
 */
public class BruteForceExploration extends AbstractExplorationAlgorithm {

	@Override
	public ISolution runExploration(SolutionSpace solSpace) throws NoSolutionFoundException {
		logger.info("      *** Starting Brute Force Exploration");
		
		SolutionSpaceAnalyzer analyzer = solSpace.getAnalyzer();
		
		/* create solutions */
		List<List<TypeParam>> configurations = analyzer.getSymbolsWithoutConstraints().stream()
			.map(s -> streamSymboltypeConfigs(solSpace, s, Components.getInstance().getExplorationMode()).collect(toList()))
			.collect(toList());

		List<ISolution> solutions = Lists.cartesianProduct(configurations).stream()
//			.limit(100)
			.map(config -> createSolution(analyzer, config))
			.collect(toList());
		
		ThreadUtils.runInNewThread(() -> anonymousLoggerToFile(Components.getInstance().getOutputLoactions().getLogsDir().resolve("solutions.rpt"))
				.info(StreamUtils.join(solutions.stream(), "\n", ISolution::toString)));
		
		
		/* Evaluate solutions */
		logger.info(() -> "Nb of solutions: " + solutions.size());
		
		ThreadUtils.runTaskInForkJoinPool(Components.getInstance().getConfiguration().getNbThreads(), () -> solutions.stream()
			.parallel()
			.forEach(s -> {
				try {
					evaluateAccuracy(s);
					evaluateCost(s);
					display(s);
				} catch (AccuracyEvaluationException | CostEvaluationException e) {
					logger.warning("Skipping solution " + s.getID());
				}
			}));
		
		/* return a valid solution with the best cost (if any exist) */
		Stream<ISolution> validSolutions = solutions.stream()
			.filter(s -> getAccuracy(s).isPresent() && getCost(s).isPresent() )
			.filter(userAccuracyConstraint::isValid);
		return (costMetric.isHigherBetter() ? 
				validSolutions.max((s1,s2) -> Double.compare(getCost(s1).get(), getCost(s2).get())) :
				validSolutions.min((s1,s2) -> Double.compare(getCost(s1).get(), getCost(s2).get())))
			.orElseThrow(() -> new NoSolutionFoundException("No solution satisfying the user constraints was found! \n"
					+ "Consider increasing the allowed wordlengths, or relaxing the acuracy constraint."));
	}

	private ISolution createSolution(SolutionSpaceAnalyzer analyzer, List<TypeParam> list) {
		ISolution solution = UserFactory.solution(analyzer.getSolSpace());
		Streams.zip(analyzer.getSymbolsWithoutConstraints().stream(), list.stream(), 
				(s, p) -> new Pair<>(s, p)).forEach(pair -> solution.setType(pair.getValue0(), pair.getValue1()));;
		return solution;
	}
	
}
