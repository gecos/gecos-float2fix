package fr.irisa.cairn.gecos.typeexploration.dse;

public class NoSolutionFoundException extends Exception {

	public NoSolutionFoundException(String msg) {
		super(msg);
	}

	private static final long serialVersionUID = -6533550823785272437L;

	
}
