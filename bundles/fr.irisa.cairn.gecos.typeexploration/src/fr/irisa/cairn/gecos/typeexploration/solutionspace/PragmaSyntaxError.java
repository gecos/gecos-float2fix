package fr.irisa.cairn.gecos.typeexploration.solutionspace;

/**
 * @author aelmouss
 */
public class PragmaSyntaxError extends RuntimeException {

	private static final long serialVersionUID = -4586055647670725782L;

	
	public PragmaSyntaxError(String string) {
		super(string);
	}

	public PragmaSyntaxError() {
		super();
	}

	public PragmaSyntaxError(String msg, Exception cause) {
		super(msg, cause);
	}

}
