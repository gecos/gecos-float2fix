package fr.irisa.cairn.gecos.typeexploration.cost;

import java.util.List;
import java.util.Optional;

import fr.irisa.cairn.gecos.typeexploration.model.ICostEvaluator;
import fr.irisa.cairn.gecos.typeexploration.model.ISolution;
import gecos.core.Symbol;
import typeexploration.computation.AddExpression;
import typeexploration.computation.BlockGroup;
import typeexploration.computation.ComputationBlock;
import typeexploration.computation.ComputationModel;
import typeexploration.computation.MaxExpression;
import typeexploration.computation.OperandTerm;
import typeexploration.computation.Operation;
import typeexploration.computation.Parameter;
import typeexploration.computation.ProductExpression;
import typeexploration.computation.SummationExpression;
import typeexploration.computation.concrete.ConcreteComputationBlock;
import typeexploration.computation.concrete.ConcreteComputationModelEvaluator;
import typeexploration.computation.concrete.ConcreteOperation;
import typeexploration.computation.concrete.HWModelParameters;
import typeexploration.computation.util.ComputationSwitch;

/**
 * Note: evaluate is called by different threads in parallel.
 * 
 * @author tyuki
 *
 */
public class ComputationModelEvaluator implements ICostEvaluator {
	public static final String AREA_MODEL = "AREA_MODEL";
	public static final String ENERGY_MODEL = "ENERGY_MODEL";
	
	protected final ComputationModel computationModel; 
	protected static final ConcreteComputationModelEvaluator concreteModelEvaluator;
	protected BlockGroup targetBlockGroup;
	
	//FIXME
	private static final HWModelParameters data ;
	
	static {
		data = new HWModelParameters();
		concreteModelEvaluator = new ConcreteComputationModelEvaluator(data);
	}

	//Target design used to be hard-coded as:
	//spec = new HWTargetDesign(500, 2, 384*512);

	
	//FIXME temporarily solution for allowing SUM_W metric all the time
	private final DummyCostEvaluator dummyEvaluator = new DummyCostEvaluator();
	
	public ComputationModelEvaluator(ComputationModel computationModel, String[] options) {
		this.computationModel = computationModel;
		parseOptions(options);
	}
	
	private void parseOptions(String[] options) {
		for (String option : options) {
			String[] split = option.split("=");
			if (split.length != 2) continue;
			parseOption(split[0], split[1]);
		}
	}
	
	private void parseOption(String name, String value) {
		if (name.contentEquals("group")) {
			Optional<BlockGroup> group = computationModel.getBlockGroups().stream().filter(bg->bg.getName().contentEquals(value)).findFirst();
			if (group.isPresent()) {
				targetBlockGroup = group.get();
			}
		}
	}
	
	@Override
	public void evaluate(ISolution solution) throws CostEvaluationException {
		
		double areaTotal = 0;
		double energyTotal = 0;
		
		List<ComputationBlock> blocks = targetBlockGroup==null?computationModel.getBlocks():targetBlockGroup.getBlocks();
		for (ComputationBlock block : blocks) {
			evaluateBlock(solution, block);
			areaTotal += solution.getCost(AREA_MODEL, block.getName());
			energyTotal += solution.getCost(ENERGY_MODEL, block.getName());
		}
		solution.addCost(AREA_MODEL, FULL_APPLICATION, areaTotal);
		solution.addCost(ENERGY_MODEL, FULL_APPLICATION, energyTotal);
		
		//also compute SUM_W FIXME
		dummyEvaluator.evaluate(solution);
	}
	
	private void evaluateBlock(ISolution solution, ComputationBlock block) {
		SizeExpressionEvaluator seEval = new SizeExpressionEvaluator(solution);
		
		ConcreteComputationBlock concreteBlock = new ConcreteComputationBlock();
		for (Operation op : block.getOperations()) {
			int BWin1 = seEval.doSwitch(op.getInputExpr1()).intValue();
			int BWin2 = seEval.doSwitch(op.getInputExpr2()).intValue();
			int BWout = seEval.doSwitch(op.getOutputExpr()).intValue();
			long numOps = seEval.doSwitch(op.getNumOps());
			//FIXME everything is assumed to be unsigned
			concreteBlock.addOperation(new ConcreteOperation(op.getOpType(), BWin1, BWin2, BWout), numOps);
		}
		
		solution.addCost(AREA_MODEL, block.getName(), concreteModelEvaluator.areaEstimation(concreteBlock, computationModel.getTargetDesign()));
		solution.addCost(ENERGY_MODEL, block.getName(), concreteModelEvaluator.energyEstimation(concreteBlock, computationModel.getTargetDesign()));
	}

	private class SizeExpressionEvaluator extends ComputationSwitch<Long> {
		
		private final ISolution solution;
		
		public SizeExpressionEvaluator(ISolution solution) {
			this.solution = solution;
		}
		
		@Override
		public Long caseParameter(Parameter object) {
			return (long)object.getValue();
		}

		@Override
		public Long caseOperandTerm(OperandTerm object) {
			Optional<Symbol> res = solution.getSymbols().stream().filter(s->s.getName().contentEquals(object.getName())).findFirst();
			if (!res.isPresent()) {
				throw new RuntimeException("Symbol not found: " + object.getName());
			}
			Symbol symbol = res.get();
			
			//FIXME check if the model is appropriately initialized to 1. Following code is for the case when it is not. (potentially breaks the interpretation when degenerate input like 0N is given) 
			long coef = 1;
			if (object.getCoef() != 0) {
				coef = object.getCoef();
			}
			return coef*solution.getType(symbol).getTotalWidth();
		}
		
		@Override
		public Long caseMaxExpression(MaxExpression object) {
			return object.getExprs().stream().mapToLong((e)->doSwitch(e)).reduce((x,y)->Math.max(x, y)).getAsLong();
		}
		
		@Override
		public Long caseAddExpression(AddExpression object) {
			return doSwitch(object.getOp1()) + doSwitch(object.getOp2());
		}
		
		@Override
		public Long caseSummationExpression(SummationExpression object) {
			return Long.valueOf(object.evaluate());
		}
		
		@Override
		public Long caseProductExpression(ProductExpression object) {
			return Long.valueOf(object.evaluate());
		}
	}
	
}
