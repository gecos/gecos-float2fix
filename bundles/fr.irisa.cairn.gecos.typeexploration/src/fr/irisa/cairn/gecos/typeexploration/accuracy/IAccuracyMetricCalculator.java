package fr.irisa.cairn.gecos.typeexploration.accuracy;

import java.util.List;
import java.util.Map;

import fr.irisa.cairn.gecos.core.profiling.backend.ErrorStats;
import gecos.core.Symbol;

public interface IAccuracyMetricCalculator {

	ErrorStats compute(Map<Symbol, List<ErrorStats>> snapshotErrorStatsMap);

}
