package fr.irisa.cairn.gecos.typeexploration.utils;

import static fr.irisa.cairn.gecos.model.utils.misc.StreamUtils.join;

import java.util.stream.Stream;

import fr.irisa.cairn.gecos.core.profiling.backend.ErrorStats;
import fr.irisa.cairn.gecos.typeexploration.dse.ExplorationMode;
import fr.irisa.cairn.gecos.typeexploration.model.ISolution;
import fr.irisa.cairn.gecos.typeexploration.model.UserFactory;
import gecos.core.Symbol;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import typeexploration.FixedPointTypeConfiguration;
import typeexploration.FixedPointTypeParam;
import typeexploration.FloatPointTypeConfiguration;
import typeexploration.FloatPointTypeParam;
import typeexploration.SolutionSpace;
import typeexploration.TypeConfigurationSpace;
import typeexploration.TypeParam;

public class SolutionSpaceUtils {

	public static Stream<TypeParam> streamSymboltypeConfigs(SolutionSpace solSpace, Symbol s, ExplorationMode mode) {
		TypeConfigurationSpace configs = solSpace.getExploredSymbols().get(s);
		if(configs == null)
			configs = solSpace.getDefaultTypeConfigs();
		
		Stream<TypeParam> stream = Stream.empty();
		if(mode == ExplorationMode.FIXED || mode == ExplorationMode.MIXED) {
			stream = Stream.concat(stream, streamFixedPointTypeConfigs(solSpace.getAnalyzer().getFixedPointConfigurationOrDefault(s)));
		}
		
		if(mode == ExplorationMode.FLOAT || mode == ExplorationMode.MIXED) {
			stream = Stream.concat(stream, streamFloatPointTypeConfigs(solSpace.getAnalyzer().getFloatPointConfigurationOrDefault(s)));
		}
		
		return stream;
	}
	
	public static Stream<FixedPointTypeParam> streamFixedPointTypeConfigs(FixedPointTypeConfiguration c) {
		Stream.Builder<FixedPointTypeParam> builder = Stream.builder();
		for(OverflowMode o : c.getOverflowMode())
			for(QuantificationMode q : c.getQuantificationMode())
				for(int i : c.getIntegerWidthValues())
					for(int w : c.getTotalWidthValues())
						for (boolean sign : c.getSigned()) 
							builder.add(UserFactory.fixedTypeParam(w, i, sign, q, o));
					
		return builder.build();
	}
	
	public static Stream<FloatPointTypeParam> streamFloatPointTypeConfigs(FloatPointTypeConfiguration c) {
		Stream.Builder<FloatPointTypeParam> builder = Stream.builder();
		for(int e : c.getExponentWidthValues())
			for(int w : c.getTotalWidthValues())
				for (boolean s : c.getSigned())
					builder.add(UserFactory.floatTypeParam(w, e, s));
					
		return builder.build();
	}
	
	public static String printSymbol(Symbol s) {
		return s.getName() + "_" + Integer.toHexString(s.hashCode());
	}

	public static String printErrorStats(ISolution sol) {
		return join(sol.getAllErrorStats().entrySet().stream(), "\n", e -> 
			printSymbol(e.getKey()) + ":\n" + join(e.getValue().stream(), "\n", 
					ErrorStats::generateReport));
	}
	
	public static String printSolution(ISolution solution) {
		return "Type Specifications:\n" 
				+ join(solution.getSolutionSpace().getSymbols().stream(), "\n", 
				s -> "\t" + printSymbol(s) + ": " + solution.getType(s));
	}
	
}
