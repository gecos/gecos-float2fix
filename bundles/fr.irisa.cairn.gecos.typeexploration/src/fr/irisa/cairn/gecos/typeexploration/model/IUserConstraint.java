package fr.irisa.cairn.gecos.typeexploration.model;

import java.util.Arrays;

import com.google.common.base.Preconditions;

@FunctionalInterface
public interface IUserConstraint {

	/**
	 * @param solution the solution to check
	 * @return true if the solution is valid, false if it violates this constraint.
	 */
	public boolean isValid(ISolution solution);
	
	
	/**
	 * @return a new constraint that evaluate to true if this evaluates to false.
	 */
	public default IUserConstraint negate() {
		return solution -> ! this.isValid(solution);
	}
	
	/**
	 * @param other the other constraint
	 * @return a new constraint that evaluate to true if at least this or other
	 * evaluates to true.
	 */
	public default IUserConstraint or(IUserConstraint other) {
		return solution -> this.isValid(solution) || other.isValid(solution);
	}
	
	/**
	 * @param other the other constraint
	 * @return a new constraint that evaluate to true both this and other
	 * evaluates to true.
	 */
	public default IUserConstraint and(IUserConstraint other) {
		return solution -> this.isValid(solution) && other.isValid(solution);
	}
	
	/**
	 * @param other the other constraint
	 * @return a new constraint that evaluate to true only this or other
	 * evaluates to true.
	 */
	public default IUserConstraint xor(IUserConstraint other) {
		return solution -> this.isValid(solution) ^ other.isValid(solution);
	}
	
	
	/**
	 * @param constraints the constraints
	 * @return a new constraint that evaluate to true only if ALL constraints
	 * evaluates to true.
	 */
	public static IUserConstraint and(IUserConstraint... constraints) {
		Preconditions.checkArgument(constraints != null && constraints.length > 0, "Cannot be null or empty");
		return solution -> Arrays.stream(constraints).allMatch(c -> c.isValid(solution));
	}
	
	/**
	 * @param constraints the constraints
	 * @return a new constraint that evaluate to true if at least one of the constraints
	 * evaluates to true.
	 */
	public static IUserConstraint or(IUserConstraint... constraints) {
		Preconditions.checkArgument(constraints != null && constraints.length > 0, "Cannot be null or empty");
		return solution -> Arrays.stream(constraints).anyMatch(c -> c.isValid(solution));
	}

	/**
	 * @param constraints the constraints
	 * @return a new constraint that evaluate to true only if NONE of the constraints
	 * evaluates to true.
	 */
	public static IUserConstraint none(IUserConstraint... constraints) {
		Preconditions.checkArgument(constraints != null && constraints.length > 0, "Cannot be null or empty");
		return solution -> Arrays.stream(constraints).allMatch(c -> ! c.isValid(solution));
	}

	
}
