package fr.irisa.cairn.gecos.typeexploration.solutionspace;

import static fr.irisa.cairn.tools.ecore.query.EMFUtils.eAllContentsInstancesOf;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.EcoreUtil2;

import fr.irisa.cairn.gecos.model.utils.misc.StreamUtils;
import gecos.core.ParameterSymbol;
import gecos.core.ProcedureSet;
import gecos.core.Symbol;
import gecos.instrs.SymbolInstruction;
import typeexploration.FixedPointTypeConfiguration;
import typeexploration.FloatPointTypeConfiguration;
import typeexploration.SameTypeConstraint;
import typeexploration.SolutionSpace;
import typeexploration.TypeConfigurationSpace;
import typeexploration.TypeConstraint;

public class SolutionSpaceAnalyzer {

	private SolutionSpace solSpace;
	
	// these are all cached!
	private List<Symbol> symbols;
	private List<ProcedureSet> procedureSets; // Containing solspace  Symbols
	private Map<Symbol, List<SymbolInstruction>> symbolReferences;
	private List<Symbol> symbolsWithoutConstraints;
	private Map<Symbol, Symbol> symbolsWithSame;
	
	public SolutionSpaceAnalyzer(SolutionSpace solSpace) {
		this.solSpace = solSpace;
		reset();
	}
	
	public void reset() {
		this.symbols = null;
		this.procedureSets = null;
		this.symbolReferences = null;
		this.symbolsWithoutConstraints = null;
		this.symbolsWithSame = null;
	}
	
	public SolutionSpace getSolSpace() {
		return solSpace;
	}
	
	public List<Symbol> getSymbols() {
		if(symbols == null)
			symbols = solSpace.getSymbols();
		return symbols;
	}
	
	public List<ProcedureSet> getProcedureSetsContainingSymbols() {
		if(procedureSets == null) {
			procedureSets = getSymbols().stream()
				.map(s -> EcoreUtil2.getContainerOfType(s, ProcedureSet.class))
				.filter(Objects::nonNull)
				.distinct()
				.collect(toList());
		}
		return procedureSets;
	}
	
	public Map<Symbol, List<SymbolInstruction>> getSymbolReferences() {
		if(symbolReferences == null) {
			List<Symbol> solSpaceSymbols = getSymbols();
			symbolReferences = getProcedureSetsContainingSymbols().stream()
				.flatMap(ps -> eAllContentsInstancesOf(ps, SymbolInstruction.class).stream())
				.filter(si -> solSpaceSymbols.contains(si.getSymbol()))
				.collect(groupingBy(SymbolInstruction::getSymbol));
		}
		return symbolReferences;
	}
	
	public Stream<Symbol> getUnreferencedSymbols() {
		Map<Symbol, List<SymbolInstruction>> refs = getSymbolReferences();
		return getSymbols().stream().filter(refs::containsKey);
	}
	
	public Stream<ParameterSymbol> getParameterSymbols() {
		return StreamUtils.filterByType(getSymbols().stream(), ParameterSymbol.class);
	}
	
	public Stream<Symbol> getNonParameterSymbols() {
		return getSymbols().stream()
			.filter(s -> !(s instanceof ParameterSymbol));
	}
	
	public List<SymbolInstruction> getReferences(Symbol s) {
		return getSymbolReferences().get(s);
	}

	public List<Symbol> getSymbolsWithoutConstraints() {
		if(symbolsWithoutConstraints == null || symbolsWithSame == null)
			computeSameAsMap();
		return symbolsWithoutConstraints;
	}
	
	/**
	 * Map a Symbol with a SAME constraint to the root the of source Symbol.
	 * e.g.:
	 * s1 SAME AS s2;
	 * s2 SAME AS s3;
	 * should return [s1 = s3 (NOT s2!!), s2 = s3]
	 */
	public Map<Symbol, Symbol> getSymbolsWithSame() {
		if(symbolsWithoutConstraints == null || symbolsWithSame == null)
			computeSameAsMap();
		return symbolsWithSame;
	}
	
	private void computeSameAsMap() {
		Map<Symbol, Optional<Symbol>> symbolSameAsMap = getSymbols().stream()
				.collect(StreamUtils.toMap(s -> s, s -> tryGetSameRootSource(s), LinkedHashMap::new));
		
		symbolsWithoutConstraints = symbolSameAsMap.entrySet().stream()
				.filter(e -> !e.getValue().isPresent())
				.map(e -> e.getKey())
				.collect(toList());
		
		symbolsWithSame = symbolSameAsMap.entrySet().stream()
				.filter(e -> e.getValue().isPresent())
				.collect(toMap(e -> e.getKey(), e -> e.getValue().get()));
	}
	
	/**
	 * This is not cached!
	 * @return the Symbol's SAME constraints sources. Empty stream if has no SAME.
	 */
	public Stream<Symbol> computeSameSources(Symbol s) {
		return computeSameConstraints(s)
			.map(SameTypeConstraint::getSymbol);
	}

	public Stream<SameTypeConstraint> computeSameConstraints(Symbol s) {
		EList<TypeConstraint> constraints = solSpace.getTypeConstraints(s);
		if(constraints == null)
			return Stream.empty();
		return constraints.stream()
			.filter(SameTypeConstraint.class::isInstance)
			.map(SameTypeConstraint.class::cast);
	}

	/**
	 * Check if the given Symbol has a SAME constraint. If so return the ROOT same source.
	 * i.e. if s1 SAME AS s2; s2 SAME AS s3; then tryGetSameRootSource(s1) returns s3 (not s2 !).
	 * 
	 * This also performs cycles detection.
	 */
	private Optional<Symbol> tryGetSameRootSource(Symbol s) {
		Optional<Symbol> source = computeSameSources(s).findFirst();
		
		if(source.isPresent()) {
			List<Symbol> chain = new ArrayList<>();
			chain.add(source.get());
			Optional<Symbol> next;
			while(source.isPresent() && (next = computeSameSources(source.get()).findFirst()).isPresent()) {
				source = next;
				if(chain.contains(source.get()))
					throw new RuntimeException("Cycle detected in SAME constraints involving symbols: " + chain); 
				
				chain.add(source.get());
			}
		}
		
		return source;
	}
	
	public FixedPointTypeConfiguration getFixedPointConfigurationOrDefault(Symbol s) {
		TypeConfigurationSpace space = solSpace.getTypeSpaceOrDefault(s);
		FixedPointTypeConfiguration configs = space.getFixedPtCongurations();
		if(configs == null)
			configs = solSpace.getDefaultTypeConfigs().getFixedPtCongurations();
		return configs;
	}
	
	public FloatPointTypeConfiguration getFloatPointConfigurationOrDefault(Symbol s) {
		TypeConfigurationSpace space = solSpace.getTypeSpaceOrDefault(s);
		FloatPointTypeConfiguration floatConfigs = space.getFloatPtCongurations();
		if(floatConfigs == null)
			floatConfigs = solSpace.getDefaultTypeConfigs().getFloatPtCongurations();
		return floatConfigs;
	}

}
