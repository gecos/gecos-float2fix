package fr.irisa.cairn.gecos.typeexploration.cost;

import fr.irisa.cairn.gecos.typeexploration.model.ICostEvaluator;
import fr.irisa.cairn.gecos.typeexploration.model.ISolution;
import typeexploration.TypeParam;

/**
 * @author aelmouss
 */
public class DummyCostEvaluator implements ICostEvaluator {

	public static final String COST_METRIC_NAME = "W_SUM";
	
	@Override
	public void evaluate(ISolution solution) throws CostEvaluationException {
		long cost = solution.getSymbols().stream()
			.map(solution::getType)
			.mapToInt(TypeParam::getTotalWidth)
			.sum();
		
		solution.addCost(COST_METRIC_NAME, ComputationModelEvaluator.FULL_APPLICATION, (double) cost);
	}
	
}
