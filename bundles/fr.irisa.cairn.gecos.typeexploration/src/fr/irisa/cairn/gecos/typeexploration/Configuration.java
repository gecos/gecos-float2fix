package fr.irisa.cairn.gecos.typeexploration;

import static fr.irisa.cairn.gecos.model.utils.misc.StreamUtils.join;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.ACCURACY_METRIC;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.ACCURACY_THRESHOLD;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.CC;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.COST_METRIC;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.COST_MODEL_FILEPATH;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.COST_MODEL_FLAGS;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.DEFAULT_FIXED_I;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.DEFAULT_FIXED_W;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.DEFAULT_FLOAT_E;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.DEFAULT_FLOAT_W;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.DEFAULT_SIGN;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.DO_PRUNING_FIRST;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.DO_TAG_OUTPUT;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.ENABLE_CHARTS;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.EXPLORATION_ALGO;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.EXPLORATION_MODE;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.EXPLORATION_FLAGS;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.EXPL_LOG_LEVEL;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.EXTRA_CFLAGS;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.MAIN_LOG_LEVEL;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.N_KEEP_OUTPUT;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.N_THREADS;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.SNAPSHOTS_ERROR_MERGE;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.SSIM_ENABLED;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.SSIM_TARGET;
import static fr.irisa.cairn.gecos.typeexploration.Configuration.Property.SYMBOLS_ERROR_MERGE;
import static fr.irisa.cairn.gecos.typeexploration.GlobalParameters.LOG_MAIN;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import com.google.common.collect.Streams;

import fr.irisa.cairn.gecos.typeexploration.accuracy.DefaultAccuracyMetricCalculator.MergeMode;
import fr.irisa.cairn.gecos.typeexploration.accuracy.DefaultAccuracyMetrics;
import fr.irisa.cairn.gecos.typeexploration.cost.DefaultCostMetrics;
import fr.irisa.cairn.gecos.typeexploration.dse.DefaultAlgorithms;
import fr.irisa.cairn.gecos.typeexploration.dse.ExplorationMode;
import fr.irisa.cairn.gecos.typeexploration.model.IAccuracyMetric;
import fr.irisa.cairn.gecos.typeexploration.model.ICostMetric;
import fr.irisa.cairn.gecos.typeexploration.model.IExplorationAlgorithm;
import fr.irisa.cairn.gecos.typeexploration.solutionspace.SolutionSpaceBuilderFromPragma;

/**
 * @author aelmouss
 */
public class Configuration {

	protected Properties properties;
	
	enum Property {
		CC("CC", "g++", 
				"Compiler used during profiling. The current simulation based profiler assumes gcc/g++ options are accepted."),
		EXTRA_CFLAGS("EXTRA_CFLAGS", "-O2 -Wno-everything", 
				"Additional compiler flags to be used during profiling runs."),
		COST_MODEL_FILEPATH("COST_MODEL_DSL", "", 
				"Path to DSL that specify model of computation used in cost models."),
		COST_MODEL_FLAGS("COST_MODEL_FLAGS", "", 
				"Additional flags to be used for cost models. Use group=<group name> to specify a pre-defined group to include in the cost calculation."),
		N_THREADS("nbThreads", "C", 
				"Max number of threads to be used: Positive integer value or C to match available Processors"),
		N_SIMULATIONS("nbSimulations", "1", 
				"Number of times a simulation is repeated."
				+ "Main usage is to inject content of different files as inputs."),
		ENABLE_CHARTS("enableCharts", "true", 
				"If false no charts will be created/displayed during exploration: [true, false]"),
		MAIN_LOG_LEVEL("mainLogLevel", "ALL", 
				"Logging level for the main logger: [OFF, SEVERE, WARNING, CONFIG, INFO, FINE, FINER, FINEST, ALL]"),
		EXPL_LOG_LEVEL("explorationLogLevel", "ALL", 
				"Logging level for the exploration logger: [OFF, SEVERE, WARNING, CONFIG, INFO, FINE, FINER, FINEST, ALL]"),
		EXPLORATION_MODE("explorationMode", "FIXED", 
				"Exploration Mode: " + Arrays.toString(ExplorationMode.values())),
		DO_TAG_OUTPUT("timeTagOutput", "true", 
				"If false generate outputs at the same directory (no time tag): [true, false]"),
		N_KEEP_OUTPUT("nbOutputsToKeep", String.valueOf(3), 
				"The number of latest time-tagged generated output directories to keep: Positive integer value"),
		DO_PRUNING_FIRST("pruneFirst", "false", 
				"If true prune solution space before starting the exploration: [true, false]"),
		EXPLORATION_ALGO("explorationAlgo", "TABU_SEARCH", 
				"Exploration algorithm: " + Arrays.toString(DefaultAlgorithms.values())),
		EXPLORATION_FLAGS("explorationFlags", "", 
				"Additional flags to be used by specific exploration algorithms. Available options depend on the algorithm."),
		ACCURACY_METRIC("accuracyMetric", "PSNR_DB", 
				"Comma-separated Accuracy metrics: " + Arrays.toString(DefaultAccuracyMetrics.values())),
		ACCURACY_THRESHOLD("accuracyThreshold", "40", 
				"Comma-separated Threshold values of the corresponding Accuracy Metrics (in the same order). " +
				"It's considered as a Lower (Upper) bound if higher values -of the specified metric- are considered more (less) accurate"),
		SSIM_ENABLED("SSIMenabled", "false", 
				"Structural Similarity is used as an additional accuracy metric when enabled."),
		SSIM_TARGET("SSIMtarget", "", 
				"Variable name to be used for SSIM calculation."),
		COST_METRIC("costMetric", "SUM_W", 
				"Comma-separated Cost metrics: " + Arrays.toString(DefaultCostMetrics.values())),
		SNAPSHOTS_ERROR_MERGE("snapshotsErrorMerge", "MEAN", 
				"How to merge Error stats from all snapshots of a given Symbol: " + Arrays.toString(MergeMode.values())),
		SYMBOLS_ERROR_MERGE("symbolsErrorMerge", "CONCAT", 
				"How to merge Error stats from all output Symbols (after merging their snapshots): " + Arrays.toString(MergeMode.values())),
		DEFAULT_FIXED_W("defaultFixedW", String.valueOf(32), 
				"The default wordlength values to use for Symbols without EXPLORE_FIX pragma: Comma-separated, Positive integer value or value range (min..max)"),
		DEFAULT_FIXED_I("defaultFixedI", String.valueOf(16), 
				"The default integer-part-length values to use for Symbols without EXPLORE_FIX pragma: Comma-separated, Positive integer value or value range (min..max)"),
		DEFAULT_FLOAT_W("defaultFloatW", String.valueOf(32), 
				"The default wordlength values to use for Symbols without EXPLORE_FLOAT pragma: Comma-separated, Positive integer value or value range (min..max)"),
		DEFAULT_FLOAT_E("defaultFloatE", String.valueOf(8),
				"The default exponent length values to use for Symbols without EXPLORE_FLOAT pragma: Comma-separated, Positive integer value or value range (min..max)"),
		DEFAULT_SIGN("defaultSigned", String.valueOf(1),
				"Set to 1 if the values are signed by default, 0 otherwise."),
		;
		
		private String key;
		private String defaultValue;
		private String description;
		private Property(String key, String defaultValue, String description) {
			this.key = key;
			this.defaultValue = defaultValue;
			this.description = description;
		}
		
		String generateDefault() {
			StringBuilder builder = new StringBuilder();
			builder.append("# " + description + "\n");
			
			if(this == ACCURACY_METRIC) {
				builder.append(join(stream(DefaultAccuracyMetrics.values()), "\n", m -> "#   " + m.describe()) + "\n");
			} else if(this == COST_METRIC) {
				builder.append(join(stream(DefaultCostMetrics.values()), "\n", m -> "#   " + m.describe()) + "\n");
			} else if(this == EXPLORATION_MODE) {
				builder.append(join(stream(ExplorationMode.values()), "\n", m -> "#   " + m.name() + ": " + m.getDescription()) + "\n");
			} else if(this == SNAPSHOTS_ERROR_MERGE || this == SYMBOLS_ERROR_MERGE) {
				builder.append(join(stream(MergeMode.values()), "\n", m -> "#   " + m.name() + ": " + m.getDescription()) + "\n");
			}
			
			builder.append("# Default value: " + defaultValue + "\n");
			builder.append(key + " = " + defaultValue + "\n");
			return builder.toString();
		}
	}
	
	public static void generateDefaultConfigurationFile(Path dir) throws IOException {
		Path file = dir.resolve("default.properties");
		Files.write(file, 
				Arrays.stream(Property.values())
				.map(Property::generateDefault)
				.collect(toList()));
	}
	
	
	private boolean warnIfNotFound;
	
	public Configuration(Path configurationFilePath) throws IOException {
		this.warnIfNotFound = true;
		this.properties = new Properties();
		properties.load(Files.newInputStream(configurationFilePath));
	}
	
	/**
	 * Default configuration
	 */
	public Configuration() {
		this.warnIfNotFound = false;
		this.properties = new Properties();
	}
	
	
	private String get(Property p) {
		if (!properties.containsKey(p.key)) {
			properties.put(p.key, p.defaultValue);
			if(warnIfNotFound) {
				Logger.getLogger(LOG_MAIN).warning("[WARNING] Did not find value for property: '" + p.key +"'! Continuing with default value '" + p.defaultValue + "'");
			}
		}
		return properties.getProperty(p.key);
	}
	
	public String getCC() {
		String cc = get(CC);
		if (cc == null || cc.length() ==0)
			return "g++";
		
		return cc;
	}
	
	public String getExtraCFlags() {
		String cflags = get(EXTRA_CFLAGS);
		if (cflags == null)
			return "";
		
		return cflags;
	}
	
	public String getCostModelFilepath() {
		return get(COST_MODEL_FILEPATH);
	}
	
	public String[] getCostModelFlags() {
		String cmflags = get(COST_MODEL_FLAGS);
		if (cmflags == null)
			return new String[] {};
		
		return cmflags.split("\\s+");
	}

	public int getNbThreads() {
		String n = get(N_THREADS);
		if(n.equalsIgnoreCase("C"))
			return Runtime.getRuntime().availableProcessors();
		return Integer.valueOf(n);
	}
	
	public int getNbSimulations() {
		return Integer.valueOf(get(Property.N_SIMULATIONS));
	}
	
	public Level getMainLoggerLevel() {
		return Level.parse(get(MAIN_LOG_LEVEL));
	}
	
	public Level getExploarationLoggerLevel() {
		return Level.parse(get(EXPL_LOG_LEVEL));
	}
	
	public ExplorationMode getExplorationMode() {
		return ExplorationMode.valueOf(get(EXPLORATION_MODE));
	}
	
	public String[] getExplorationFlags() {
		String cmflags = get(EXPLORATION_FLAGS);
		if (cmflags == null)
			return new String[] {};
		
		return cmflags.split("\\s+");
	}
	
	public boolean isTimeTagOutputDir() {
		return Boolean.valueOf(get(DO_TAG_OUTPUT));
	}
	
	public int getNbTaggedOutputsTokeep() {
		return Integer.valueOf(get(N_KEEP_OUTPUT));
	}
	
	public boolean isPruneSolutionSpaceFirst() {
		return Boolean.valueOf(get(DO_PRUNING_FIRST));
	}
	
	public boolean isEnableCharts() {
		return Boolean.valueOf(get(ENABLE_CHARTS));
	}
	
	public IExplorationAlgorithm getAlgo() {
		String string = get(EXPLORATION_ALGO);
		return DefaultAlgorithms.valueOf(string.toUpperCase());
	}
	
	public Map<IAccuracyMetric, Double> getAccuracyMetrics() {
		Map<IAccuracyMetric, Double> map = new LinkedHashMap<>();
		Streams.zip(
			valueOfSeparatedBy(get(ACCURACY_METRIC), ",", DefaultAccuracyMetrics::valueOf).map(DefaultAccuracyMetrics::getMetric),
			valueOfSeparatedBy(get(ACCURACY_THRESHOLD), ",", Double::valueOf),
			(metric, threshold) -> map.put(metric, threshold)
		).forEach(e -> {});
		return map;
	}
	
	public List<ICostMetric> getCostMetric() {
		return valueOfSeparatedBy(get(COST_METRIC), ",", DefaultCostMetrics::valueOf)
			.map(DefaultCostMetrics::getMetric).collect(toList());
	}
	
	public MergeMode getSnapshotsErrorStatsMergeMode() {
		return MergeMode.valueOf(get(SNAPSHOTS_ERROR_MERGE));
	}
	
	public MergeMode getSymbolsErrorStatsMergeMode() {
		return MergeMode.valueOf(get(SYMBOLS_ERROR_MERGE));
	}

	public int[] getDefaultFixedW() {
		return valueOfCommaSeparatedInt(get(DEFAULT_FIXED_W));
	}
	
	public int[] getDefaultFixedI() {
		return valueOfCommaSeparatedInt(get(DEFAULT_FIXED_I));
	}
	
	public int[] getDefaultFloatW() {
		return valueOfCommaSeparatedInt(get(DEFAULT_FLOAT_W));
	}
	
	public int[] getDefaultFloatE() {
		return valueOfCommaSeparatedInt(get(DEFAULT_FLOAT_E));
	}

	public int[] getDefaultSigned() {
		return valueOfCommaSeparatedInt(get(DEFAULT_SIGN));
	}

	public boolean isSSIMEnabled() {
		return Boolean.valueOf(get(SSIM_ENABLED));
	}
	
	public String getSSIMtarget() {
		return get(SSIM_TARGET);
	}
	
	
	@Override
	public String toString() {
		return join(stream(Property.values()), "\n", p -> {
			String get = properties.getProperty(p.key);
			return "\t" + p.key + " = " + (get == null ? "(Not specified! Using default) " + p.defaultValue : get);
		});
	}

	private static int[] valueOfCommaSeparatedInt(String string) {
		return SolutionSpaceBuilderFromPragma.PragmaExploreParser.parseSetValue(string).stream()
				.mapToInt(Integer::valueOf)
				.toArray();
	}
	
	private static <T> Stream<T> valueOfSeparatedBy(String string, String separator, Function<String, T> valueOf) {
		return Stream.of(string.split(","))
				.map(String::trim)
				.map(valueOf);
	}
	
}
