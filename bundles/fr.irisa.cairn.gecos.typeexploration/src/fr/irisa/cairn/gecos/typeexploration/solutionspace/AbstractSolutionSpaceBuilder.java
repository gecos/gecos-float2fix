package fr.irisa.cairn.gecos.typeexploration.solutionspace;

import static fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION;
import static fr.irisa.cairn.gecos.model.utils.annotations.PragmaUtils.findPragmaContent;

import java.util.List;

import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.typeexploration.model.ISolutionSpaceBuilder;
import fr.irisa.cairn.gecos.typeexploration.model.UserFactory;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import typeexploration.SolutionSpace;
import typeexploration.TypeConfiguration;
import typeexploration.TypeConfigurationSpace;

/**
 * @author aelmouss
 */
public abstract class AbstractSolutionSpaceBuilder implements ISolutionSpaceBuilder {
	
	protected List<TypeConfiguration> defaultTypeConfigs;
	protected SolutionSpace solSpace;
	
	public AbstractSolutionSpaceBuilder(List<TypeConfiguration> defaultTypeConfigs) {
		this.defaultTypeConfigs = defaultTypeConfigs;
	}
	
	
	@Override
	public SolutionSpace build(GecosProject project) {
		this.solSpace = UserFactory.emptySolutionSpace();
		this.solSpace.setProject(project);
		
		project.listProcedureSets()
			.forEach(this::buildSymbolsTypeConfigurations);
		
		solSpace.setAnalyzer(new SolutionSpaceAnalyzer(solSpace));
		
		return solSpace;
	}
	
	public SolutionSpace getSolutionSpace() {
		if(solSpace == null)
			throw new RuntimeException("The solution space has not been built yet! 'build' method must be invoked before");
		return solSpace;
	}
	
	protected void buildSymbolsTypeConfigurations(ProcedureSet ps) {
		if (defaultTypeConfigs == null) {
			throw new NullPointerException();
		} else {
			solSpace.setDefaultTypeConfigs(UserFactory.configurationSpace(defaultTypeConfigs));
		}

		EMFUtils.eAllContentsInstancesOf(ps, Symbol.class).stream()
			.filter(this::filterType)
			.peek(solSpace::addSymbol)
			.filter(this::hasCustomTypeConfigs)
			.forEach(s -> solSpace.getExploredSymbols().put(s, createConfigurationSpace(s)));
	}


	protected boolean filterType(Symbol s) {
		/* filter out ProcedureSymbol */
		if(s instanceof ProcedureSymbol)
			return false;
		
		/* filter out ignored symbols */
		if(findPragmaContent(s, c -> c.equals(CODEGEN_IGNORE_ANNOTATION)).findAny().isPresent())
			return false;
		
		/* 
		 * filter out ParameterSymbols :
		 *  - of a ProcedureSymbol that does not have a Procedure defined
		 *  - of main() procedure
		 */
		if(s instanceof ParameterSymbol) {
			Procedure proc = ((ParameterSymbol) s).findProcedure();
			if(proc == null || proc.getSymbolName().equals("main"))
				return false;
		}
		
		/* filter out all non Float symbols */
		TypeAnalyzer t = new TypeAnalyzer(s.getType());
		return t.getBaseLevel().isFloat(); 
	}
	
	/**
	 * Should return true if the specified {@link Symbol} has custom type configurations.
	 * In which case {@link #extractTypeConfigurations(Symbol)} will be invoked to extract them.
	 */
	protected abstract boolean hasCustomTypeConfigs(Symbol s);
	
	protected abstract TypeConfigurationSpace createConfigurationSpace(Symbol s);
	
	
}