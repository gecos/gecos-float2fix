package fr.irisa.cairn.gecos.typeexploration;

import java.nio.file.Path;
import java.nio.file.Paths;

import fr.irisa.cairn.gecos.model.utils.misc.ResourceLocator;

public class GlobalParameters {
	
	static final Path GEN_RESOURCES_FOLDER_PATH = new ResourceLocator(GlobalParameters.class, "resources").locate("resources");

	// Type Libraries
	static final Path SIM_TYPE_LIBS_FOLDER = GEN_RESOURCES_FOLDER_PATH.resolve("TypeLibraries");
	public static final Path SIM_ACTYPE_INC_FOLDER = SIM_TYPE_LIBS_FOLDER.resolve("ac_types");
	public static final Path SIM_CTTYPE_INC_FOLDER = SIM_TYPE_LIBS_FOLDER.resolve("ct_types");
	
	// Output Locations 
	static final Path ROOT_OUTPUT_FOLDER = Paths.get("outputs");
	static final String LOGS_FODLER_NAME = "logs";
	static final String EXPLORATION_OUTPUT_FOLDER_NAME = "exploration";
	
	
	// log names
	static final String LOG_MAIN = "typeexplorationMain";
	static final String LOG_EXPLORATION = "exploration";
	static final String LOG_EXPLORATION_STATS = "explorationStats";
	
}
