package fr.irisa.cairn.gecos.typeexploration.solutionspace;

import static fr.irisa.cairn.gecos.model.utils.misc.LoggingUtils.anonymousLoggerToFile;
import static fr.irisa.cairn.gecos.typeexploration.utils.SolutionSpaceUtils.printSymbol;
import static java.util.stream.Collectors.toList;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import fr.irisa.cairn.gecos.model.utils.InstructionUtils;
import fr.irisa.cairn.gecos.typeexploration.Components;
import fr.irisa.cairn.gecos.typeexploration.model.UserFactory;
import gecos.core.ParameterSymbol;
import gecos.core.Symbol;
import gecos.instrs.CallInstruction;
import typeexploration.SameTypeConstraint;
import typeexploration.SolutionSpace;
import typeexploration.TypeConfiguration;

/**
 * @author aelmouss
 */
public class SolutionSpaceConstraintsProcessor {
	
	private SolutionSpace solutionSpace;

	public SolutionSpaceConstraintsProcessor(SolutionSpace solutionSpace) {
		this.solutionSpace = solutionSpace;
	}

	/** 
	 * Infer additional type constraints: <ul>
	 *   <li> Procedure parameters MUST have same types as their corresponding call arguments; Add these constraints.
	 *   <li> In case of multiple calls to the same procedure, multiple constraints will be added to its parameter.
	 *   <li> When a Symbol have multiple SAME constraints, all the 'same as' symbols of these constraints will be
	 *   also constrained to be the same
	 * </ul>
	 */
	public void compute() {
		Path logsDir = Components.getInstance().getOutputLoactions().getLogsDir();
		SolutionSpaceAnalyzer analyzer = solutionSpace.getAnalyzer();
		
		/** 
		 * Add additional inferred type constraints:
		 * - Procedure Procedure parameters MUST have same types as the corresponding call arguments
		 *   In case of multiple calls to the same procedure, multiple constraints will be added to its parameter.
		 *   Multiple constraints will be processed later.
		 */
		Logger analysisLog = anonymousLoggerToFile(logsDir.resolve("solution-space-analysis.log"), true);
		analyzer.getSymbolReferences().keySet().stream()
			.flatMap(s -> analyzer.getReferences(s).stream())
			.filter(ref -> ref.getParent() instanceof CallInstruction)
			.forEach(ref -> {
				CallInstruction call = (CallInstruction) ref.getParent();
				int argIdx = call.getArgs().indexOf(ref);
				ParameterSymbol param = InstructionUtils.findCalledProcedureParameter(call, argIdx);
				if(param != null && analyzer.getSymbols().contains(param)) {
					Symbol refSymbol = ref.getSymbol();
					SameTypeConstraint constraint = UserFactory.sameTypeConstraint(refSymbol);
					solutionSpace.addTypeConstraint(param, constraint);
					
					analysisLog.info("Add new constraint to '" + printSymbol(param) + "' : " +  constraint);
					
					//if param had type configs => move them to ref.getSymbol()
					moveTypeConfigs(solutionSpace, analysisLog, param, refSymbol);
				}
			});
		
		/**
		 * Some symbols may by now have multiple (SAME) constraints so we need to process them. 
		 * For example if:
		 * S1: SAME as S2 and SAME as S3
		 * 
		 * Both constraints cannot be satisfied simultaneously unless we make either:
		 * S2: SAME as S3 
		 * or S3: SAME as S2
		 * Either way, the SAME source symbol should have as type configs the merge of both S2 and S3 !
		 */
		boolean revisit = false;
		int nbRevist = 0;
		do {
			revisit = false; // modified 29/11/2018 following infinite loop on SAME constraint
			List<Symbol> visited = new ArrayList<>();
			for(Symbol s : analyzer.getSymbols()) {
				List<SameTypeConstraint> sames = analyzer.computeSameConstraints(s).collect(toList());
				if(sames.stream().map(same -> same.getSymbol()).distinct().count() > 1) {
					Optional<SameTypeConstraint> constraintToKeep = sames.stream()
						.filter(same -> !visited.contains(same.getSymbol()))
						.findFirst();
					if(constraintToKeep.isPresent()) {
						// move all other same sources to new Src
						Symbol newSameHolder = constraintToKeep.get().getSymbol();
						moveAllSamesTo(solutionSpace, analysisLog, sames, constraintToKeep, newSameHolder);
					} else {
						// all sames have been visited => pick the first one and revisit
						Symbol newSameHolder = sames.get(0).getSymbol();
						moveAllSamesTo(solutionSpace, analysisLog, sames, Optional.of(sames.get(0)), newSameHolder);
						revisit = true;
						nbRevist++;
//							break;
					}
				}
				visited.add(s);
			}
			
			if(nbRevist > analyzer.getSymbols().size()) {
				throw new RuntimeException("Might have a SAME constraint cycle!");
			}
			
		} while(revisit);
	}

	private void moveAllSamesTo(SolutionSpace solutionSpace, Logger analysisLog, List<SameTypeConstraint> allSameConstraints,
			Optional<SameTypeConstraint> constraintToKeep, Symbol newSameHolder) {
		allSameConstraints.stream()
			.filter(same -> same != constraintToKeep.get())
			.forEach(same -> {
				analysisLog.info("Move SAME constraint to '" + printSymbol(newSameHolder) + "' : " +  same);
				solutionSpace.addTypeConstraint(newSameHolder, same);
				
				moveTypeConfigs(solutionSpace, analysisLog, newSameHolder, same.getSymbol());
			});
	}

	private void moveTypeConfigs(SolutionSpace solutionSpace, Logger analysisLog, Symbol from, Symbol to) {
		List<TypeConfiguration> paramConfigs = solutionSpace.listTypeConfigs(from);
		if(paramConfigs != null && !paramConfigs.isEmpty()) {
			paramConfigs.forEach(c -> solutionSpace.addTypeConfiguration(to, c));
			
			analysisLog.info("Move type configs from '" + printSymbol(from) + "' to '" + printSymbol(to) + "'");
		}
	}
}
