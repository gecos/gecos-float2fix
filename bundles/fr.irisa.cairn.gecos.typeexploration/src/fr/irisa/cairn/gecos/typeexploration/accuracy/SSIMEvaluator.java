package fr.irisa.cairn.gecos.typeexploration.accuracy;

import fr.irisa.cairn.gecos.core.profiling.frontend.DirectivesProcessor;
import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import gecos.core.Symbol;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;

public class SSIMEvaluator {

	private final int WIDTH;
	private final int HEIGHT;
	private Number[] ref;
	private Number[] test;
	
	//parameters
	private static final int N = 11;
	private static final int S = 1;
	private static final double L = Math.pow(2, 1)-1; //dynamic range
	private static final double k1 = 0.01;
	private static final double k2 = 0.03;
	private static final double c1 = Math.pow(k1*L, 2);
	private static final double c2 = Math.pow(k2*L, 2);
	private static final double[][] gaussian_filter = new double[N][N];
	private static final double gaussian_stddev = 1.5;
	
	static {

		int center = (N/2);
  		double total = 0;
  		double unnormalized[][] = new double[N][N];
		double gaussian_variance=gaussian_stddev*gaussian_stddev;
		
  	  	for (int y = 0; y < N; y++){
			for (int x = 0; x < N; x++){
         				double dist = Math.abs(x-center)*Math.abs(x-center)+Math.abs(y-center)*Math.abs(y-center);
				unnormalized[x][y] = Math.exp(-0.5*dist/gaussian_variance);
				total = total + unnormalized[x][y];
  			}
		}
  	  	for (int y = 0; y < N; y++){
			for (int x = 0; x < N; x++){
				gaussian_filter[x][y] = unnormalized[x][y] / total;
			}
  	  	}
	}
	
	
	public static SSIMEvaluator fromSymbol(Symbol symbol, Number[] ref, Number[] test) {
		TypeAnalyzer ta = new TypeAnalyzer(symbol.getType(), true);

		int totalNbDims = ta.getTotalNbDims();
		if (totalNbDims != 2)
			throw new RuntimeException("Expecting two-dimensional arrays.");
		
		int[] sizesOuterFirst = new int[totalNbDims];
		for(int dim = 0; dim < totalNbDims; dim++) { //outermost-first order
			Instruction sz = DirectivesProcessor.getDimensionSize(symbol, dim);
			if(sz == null || !(sz instanceof IntInstruction))
				throw new RuntimeException("Expecting constant array bounds.");
			sizesOuterFirst[dim] = (int) ((IntInstruction)sz).getValue();
		}

		return new SSIMEvaluator(sizesOuterFirst[1], sizesOuterFirst[0], ref, test);
	}
	
	public SSIMEvaluator(int WIDTH, int HEIGHT, Number[] ref, Number[] test) {
		this.WIDTH = WIDTH;
		this.HEIGHT = HEIGHT;
		this.ref = ref;
		this.test = test;
		
		if (ref == null || test == null || ref.length != test.length) {
			throw new RuntimeException("Sizes of the two images do not match.");
		}
		
		if (ref.length != WIDTH*HEIGHT) {
			throw new RuntimeException("Sizes of the image is inconsistent with the given width/height.");
		}
	}
	
	private double applyFilter(double[][] data) {
		double res = 0;
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				res += data[i][j]*gaussian_filter[i][j];
			}
		}
		return res;
	}
	
	public double evaluate() {
		double[][] ref_window = new double[N][N];
		double[][] test_window = new double[N][N];
		double[][] ref_window_stddev = new double[N][N];
		double[][] test_window_stddev = new double[N][N];
		double[][] window_covariance = new double[N][N];
		//iterate over windows
		double window_ssim_sum = 0;
		int window_count = 0;
		for (int x = 0; x < HEIGHT-(N+1); x+=S) {
			for (int y = 0; y < WIDTH-(N+1); y+=S) {
				//compute SSIM per window
				double ref_mean_est = 0;
				double test_mean_est = 0;
				double ref_stddev_est = 0;
				double test_stddev_est = 0;
				double covariance_est = 0;

				//fist pass to compute mean
				{
					int rowBase= x*WIDTH;
					for (int i=0; i<N; i++) {
						for (int j=0; j<N; j++) {
							ref_window[i][j]  = ref[rowBase+y+j].doubleValue();
							test_window[i][j] = test[rowBase+y+j].doubleValue();  
						}
						rowBase += WIDTH;
					}
					ref_mean_est  = applyFilter(ref_window);
					test_mean_est =  applyFilter(test_window);
				}

				//second pass for the rest
				{
					for (int i=0; i<N; i++) {
						for (int j=0; j<N; j++) {
							ref_window_stddev[i][j]  = Math.pow(ref_window[i][j]-ref_mean_est, 2);
							test_window_stddev[i][j] = Math.pow(test_window[i][j]-test_mean_est, 2);
							window_covariance[i][j] = (ref_window[i][j]-ref_mean_est)*(test_window[i][j]-test_mean_est);
						}
					}
					ref_stddev_est  = Math.sqrt(applyFilter(ref_window_stddev));
					test_stddev_est = Math.sqrt(applyFilter(test_window_stddev));
					covariance_est  = applyFilter(window_covariance);
				}
				
				//SSIM
				double numerator = (2*ref_mean_est*test_mean_est + c1) * (2*covariance_est + c2);
				double denominator = (Math.pow(ref_mean_est,2) + Math.pow(test_mean_est,2) + c1)*(Math.pow(ref_stddev_est,2) + Math.pow(test_stddev_est,2) + c2);
				double window_ssim = numerator/denominator;
				window_ssim_sum += window_ssim;
				window_count++;
			}
		}
		
		double ssim = window_ssim_sum / window_count;
		
		return ssim;
	}
	

	//old version without using Gaussian
//	public double evaluate_simple() {
//		//iterate over windows
//		double window_ssim_sum = 0;
//		int window_count = 0;
//		for (int x = 0; x < HEIGHT-(N+1); x+=S) {
//			for (int y = 0; y < WIDTH-(N+1); y+=S) {
//				//compute SSIM per window
//				double ref_mean = 0;
//				double test_mean = 0;
//				double ref_stddev = 0;
//				double test_stddev = 0;
//				double covariance = 0;
//
//				//fist pass to compute mean
//				{
//					double ref_sum = 0;
//					double test_sum = 0;
//					int rowBase= x*WIDTH;
//					for (int i=0; i<N; i++) {
//						for (int j=0; j<N; j++) {
//							ref_sum += ref[rowBase+y+j].doubleValue();
//							test_sum += test[rowBase+y+j].doubleValue();  
//						}
//						rowBase += WIDTH;
//					}
//					ref_mean = ref_sum / (double)(N*N);
//					test_mean = test_sum / (double)(N*N);
//				}
//
//				//second pass for the rest
//				{
//					int rowBase= x*WIDTH;
//					double ref_sqdev_sum = 0;
//					double test_sqdev_sum = 0;
//					double dev_prod_sum = 0;
//					for (int i=0; i<N; i++) {
//						for (int j=0; j<N; j++) {
//							ref_sqdev_sum += Math.pow(ref[rowBase+y+j].doubleValue() - ref_mean, 2);
//							test_sqdev_sum += Math.pow(test[rowBase+y+j].doubleValue() - test_mean, 2);  
//							dev_prod_sum += (ref[rowBase+y+j].doubleValue() - ref_mean)*(test[rowBase+y+j].doubleValue() - test_mean);  
//						}
//						rowBase += WIDTH;
//						ref_stddev = Math.sqrt(ref_sqdev_sum / (double)(N*N-1));
//						test_stddev = Math.sqrt(test_sqdev_sum / (double)(N*N-1));
//						covariance = dev_prod_sum / (double)(N*N-1);
//					}
//				}
//				
//				//SSIM
//				double numerator = (2*ref_mean*test_mean + c1) * (2*covariance + c2);
//				double denominator = (Math.pow(ref_mean,2) + Math.pow(test_mean,2) + c1)*(Math.pow(ref_stddev,2) + Math.pow(test_stddev,2) + c2);
//				double window_ssim = numerator/denominator;
//				window_ssim_sum += window_ssim;
//				window_count++;
//			}
//		}
//		
//		double ssim = window_ssim_sum / window_count;
//		
//		return ssim;
//	}
	
	
}
