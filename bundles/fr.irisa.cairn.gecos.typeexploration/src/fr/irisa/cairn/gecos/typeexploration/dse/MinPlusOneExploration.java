package fr.irisa.cairn.gecos.typeexploration.dse;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Streams;

import fr.irisa.cairn.gecos.model.utils.misc.ThreadUtils;
import fr.irisa.cairn.gecos.model.utils.misc.ValuesNormalizer;
import fr.irisa.cairn.gecos.typeexploration.Components;
import fr.irisa.cairn.gecos.typeexploration.accuracy.AccuracyEvaluationException;
import fr.irisa.cairn.gecos.typeexploration.cost.CostEvaluationException;
import fr.irisa.cairn.gecos.typeexploration.model.ISolution;
import fr.irisa.cairn.gecos.typeexploration.solutionspace.SolutionSpaceAnalyzer;
import typeexploration.SolutionSpace;

/**
 * @author aelmouss
 */
public class MinPlusOneExploration extends AbstractExplorationAlgorithm {

	/**
	 * {@inheritDoc}
	 * 
	 * Assume:
	 *  - Fixed-point types only
	 *  - Integer width is known and fixed as well as other parameters except W
	 */
	@Override
	public ISolution runExploration(SolutionSpace solSpace) throws NoSolutionFoundException {
		logger.info("      *** Starting MinPlusOne Exploration");

		try {
			evaluateMaxWsSolution(solSpace, userAccuracyConstraint);
		} catch (AccuracyEvaluationException | CostEvaluationException e) {
			logger.warning("Skip evalution of Max Ws solution");
		}

		SolutionSpaceAnalyzer analyzer = solSpace.getAnalyzer();
		WordLengthsExplorer explorer = new WordLengthsExplorer(solSpace);
		
		/* Set all Ws to their minimun values */
		explorer.setAllWToMin();
		
		ISolution solution = explorer.createCurrentSolution();
		boolean isValid;
		try {
			evaluateAccuracy(solution);
			evaluateCost(solution);
			isValid = userAccuracyConstraint.isValid(solution);
			display(solution);
		} catch (AccuracyEvaluationException | CostEvaluationException e) {
			//!!! aelmouss:
			//
			// Assuming the code compiles fine with the selected underlying types library (ac_fixed, ct-Float ..)
			// This could happen if, for instance, the solution has very low precision so it results in underflow 
			// that leads to segmentation faults due to divide by zero for example.
			// So this failure could indicate that the solution is not valid!
			//
			// To recover from this, I let the exploration start and during criterion computation I consider
			// the solution to be the worst in accuracy and in cost.

			logger.warning("Initial solution is not valid: cause :" + e.getMessage());
			isValid = false;
			
//			throw new RuntimeException(e);
		}
		
		
		while(! isValid) {
			logger.fine("Looking for the best next (W Plus One) solution...");
			WordLengthsExplorer next = findBestNext(analyzer, explorer);
			if(next == null)
				break;
			explorer = next;
			solution = explorer.getLastCreatedSolution();
			isValid = userAccuracyConstraint.isValid(solution);
			display(solution);
			logger.fine("Found best next (W Plus One) solution: " + solution.getID());
		}
		
		if(!getAccuracy(solution).isPresent() || !getCost(solution).isPresent())
			throw new NoSolutionFoundException("Failed to find a solution likely because the minimun precision "
					+ "configuration of some Symbols is too low, resulting in errors (like divide by zero for example).\n"
					+ "Try rerun after enabling pruning or increasing the minimum W values of some symbols.");
		
		if(!userAccuracyConstraint.isValid(solution))
			throw new NoSolutionFoundException("No valid solution was found!");
		
		return solution;
	}
	
	/** 
	 * Evaluate all next solution candidates and select one with the maximum 
	 * 'accuracy-improvement to cost-degradation' ratio
	 * <p> 
	 * A solution candidate is considered for each Symbol that didn't reach it max W by 
	 * taking its next (higher) W value while keeping the other symbol Ws unchanged.
	 * 
	 * @return the explorer containing the best next solution if any was found, {@code null} otherwise.
	 */
	private WordLengthsExplorer findBestNext(SolutionSpaceAnalyzer analyzer, WordLengthsExplorer currentExplorer) {
		List<WordLengthsExplorer> nextSolutionCandidates = new ArrayList<>();
		ThreadUtils.runTaskInForkJoinPool(Components.getInstance().getConfiguration().getNbThreads(), () ->
		analyzer.getSymbolsWithoutConstraints().stream()
			.filter(symbol -> currentExplorer.getCurrentW(symbol) < currentExplorer.getMaxW(symbol))
			.parallel()
			.forEach(symbol -> {
				WordLengthsExplorer nextExplorer = currentExplorer.copy();
				boolean changed = nextExplorer.setToNextHigherW(symbol);
				Preconditions.checkState(changed, "The Symbol is already at its maximum W: " + symbol + 
						"! This should never happen, there is probably a bug in the explorer.");
				
				ISolution nextSolution = nextExplorer.createCurrentSolution();
				try {
					evaluateAccuracy(nextSolution);
					evaluateCost(nextSolution);
					synchronized (nextSolutionCandidates) {
						nextSolutionCandidates.add(nextExplorer);
					}
				} catch (AccuracyEvaluationException | CostEvaluationException e) {
					logger.warning("Skipping next solution candidate: " + nextSolution.getID() + " cause: " + e.getMessage());
				}
			}));
		
		/* select the next Solution that results in the maximum 'accuracy improvement to cost increase' ratio */
		logger.fine("Evaluating next candidates ...");
		Stopwatch w = Stopwatch.createStarted();
		WordLengthsExplorer best = selectBestNext(currentExplorer.getLastCreatedSolution(), nextSolutionCandidates);
		logger.info("Evaluating candiates finished in " + w);
		
		return best;
	}

	private WordLengthsExplorer selectBestNext(ISolution currentSolution, List<WordLengthsExplorer> nextSolutionCandidates) {
		if(nextSolutionCandidates.isEmpty())
			return null;

		List<Double> accuracyValues = nextSolutionCandidates.stream()
			.map(e -> getAccuracy(e.getLastCreatedSolution()).get())
			.collect(toList());
		ValuesNormalizer accuracyNormalizer = new ValuesNormalizer(accuracyValues);
		
		List<Double> costValues = nextSolutionCandidates.stream()
				.map(e -> getCost(e.getLastCreatedSolution()).get())
				.collect(toList());
		ValuesNormalizer costNormalizer = new ValuesNormalizer(costValues);
		
		Double normalizedCurrentAccuray = getAccuracy(currentSolution).map(accuracyNormalizer::normalize).orElse(null);
		Double normalizedCurrentCost = getCost(currentSolution).map(costNormalizer::normalize).orElse(null);

		List<Double> criteria = Streams.zip(
				accuracyNormalizer.normalize(accuracyValues).stream(), 
				costNormalizer.normalize(costValues).stream(), 
				(nextAccuray, nextCost) -> 
					(1 + computeImprovement(normalizedCurrentAccuray, nextAccuray, accuracyMetric.isHigherBetter())) / 
					(1 + computeDegradation(normalizedCurrentCost, nextCost, costMetric.isHigherBetter())))
				.collect(toList());
		
		return criteria.stream().max(Double::compare)
				.map(max -> nextSolutionCandidates.get(criteria.indexOf(max)))
				.orElse(null);
	}

	/**
	 * @param currentNormalized current value normalized (between 0 and 1). If {@code null}, use 0 if higherIsBetter, 1 otherwise.
	 * @param nextNormalized next value normalized (between 0 and 1)
	 * @param higherIsBetter true indicates that the input values are considered better (i.e. improved) if higher. 
	 * @return positive values between 0 and 2. Higher values => more improvement (values between 0 and 1 signify degradation).
	 */
	private double computeImprovement(Double currentNormalized, double nextNormalized, boolean higherIsBetter) {
		if(currentNormalized == null)
			currentNormalized = higherIsBetter? 0.0 : 1.0;
		
		double diff = currentNormalized - nextNormalized;
		if(higherIsBetter)
			diff = -diff;
		return diff + 1;
	}
	
	/**
	 * @param currentNormalized current value normalized (between 0 and 1)
	 * @param nextNormalized next value normalized (between 0 and 1)
	 * @param higherIsBetter true indicates that the input values are considered better (i.e. improved) if higher. 
	 * @return positive values between 0 and 2. Higher values => more degradation (values between 0 and 1 signify improvement).
	 */
	private double computeDegradation(Double currentNormalized, double nextNormalized, boolean higherIsBetter) {
		return 2 - computeImprovement(currentNormalized, nextNormalized, higherIsBetter);
	}


//	private WordLengthsExplorer selectBestNext(ISolution currentSolution, List<WordLengthsExplorer> nextSolutionCandidates) {
//		return nextSolutionCandidates.stream()
//			.reduce((e1, e2) -> {
//				double c1 = computeCriterion(e1.getLastCreatedSolution(), currentSolution);
//				double c2 = computeCriterion(e2.getLastCreatedSolution(), currentSolution);
//				return c1 > c2 ? e1 : e2;
//			})
//			.orElse(null);
//	}
//
//	private double computeCriterion(ISolution nextSolution, ISolution currentSolution) {
//		double currentAccuray = getAccuracyMetric(currentSolution);
//		double currentCost = currentSolution.getCost();
//		double nextAccuray = getAccuracyMetric(nextSolution);
//		double nextCost = nextSolution.getCost();
//		
//		//FIXME: use a normalized cost and let the denominator be (1 + (currentCost - nextCost))
//		if(currentCost - nextCost == 0)
//			throw new RuntimeException("FIXME: the criterion computation encountered a divide by 0.");
//		return (currentAccuray - nextAccuray) / (currentCost - nextCost);
//	}
	
}
