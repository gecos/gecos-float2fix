package fr.irisa.cairn.gecos.typeexploration.utils;

import static java.util.stream.Collectors.toList;

import java.awt.Color;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.statistics.HistogramType;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.primitives.Floats;

@SuppressWarnings("serial")
public class BitHistogramChart extends JAppFrame {
	
	public static final int DOUBLE_BIT_WIDTH = 64;
	public static final int DOUBLE_EXP_WIDTH = 11;
	public static final int DOUBLE_MAN_WIDTH = 52;
	
	public static final int FLOAT_BIT_WIDTH = 32;
	public static final int FLOAT_EXP_WIDTH = 8;
	public static final int FLOAT_MAN_WIDTH = 23;
	
	public static BitHistogramChart ofDoubles(String title, String xName, HistogramType histogramType, double[] data) {
		List<int[]> powerOf2s = Arrays.stream(data)
			.mapToLong(Double::doubleToLongBits)
			.mapToObj(Long::toBinaryString)
			.map(s -> Strings.padStart(s, DOUBLE_BIT_WIDTH, '0'))
			.map(bits -> bits.chars().map(c -> c - '0').toArray())
			.collect(toList());
		return new BitHistogramChart(title, xName, histogramType, powerOf2s, 
				DOUBLE_BIT_WIDTH, true, DOUBLE_EXP_WIDTH+1);
	}
	
	public static BitHistogramChart ofFloats(String title, String xName, HistogramType histogramType, float[] data) {
		List<int[]> powerOf2s = Floats.asList(data).stream()
				.mapToInt(Float::floatToIntBits)
				.mapToObj(Integer::toBinaryString)
				.map(s -> Strings.padStart(s, FLOAT_BIT_WIDTH, '0'))
				.map(bits -> bits.chars().map(c -> c - '0').toArray())
				.collect(toList());
		return new BitHistogramChart(title, xName, histogramType, powerOf2s, 
				FLOAT_BIT_WIDTH, true, FLOAT_EXP_WIDTH+1);
	}
	
	public static BitHistogramChart ofFixed(String title, String xName, HistogramType histogramType, double[] data, int bitWidth, int fracWidth) {
		Preconditions.checkArgument(bitWidth <= Long.SIZE, "Width is too large only up to 64 bits is supported: " + bitWidth);
		Preconditions.checkArgument(fracWidth <= bitWidth, "Not verified for cases where fracWidth <= bitWidth!"); //XXX
		
		double one = 1L << (long)fracWidth;
		List<int[]> powerOf2s = Arrays.stream(data)
			.mapToLong(d -> (long)(d*one)) //XXX rounding mode!
			.mapToObj(Long::toBinaryString)
			.map(s -> Strings.padStart(s, Long.SIZE, '0'))
			.map(bits -> bits.substring(Long.SIZE - bitWidth))
			.map(bits -> bits.chars().map(c -> c - '0').toArray())
			.collect(toList());
		return new BitHistogramChart(title, xName, histogramType, powerOf2s, bitWidth, false, bitWidth-fracWidth);
	}
	
	
	protected DefaultCategoryDataset dataset;
	protected JFreeChart chart;
	protected ChartPanel chartPanel;
	
	
	/**
	 * @param title
	 * @param xName
	 * @param histogramType
	 * @param binaryData MSB first
	 * @param bitWidth
	 * @param separatorPos exponent width for Floats and W-F for Fixed.
	 */
	protected BitHistogramChart(String title, String xName, HistogramType histogramType, 
			List<int[]> binaryData, int bitWidth, boolean isFloat, int separatorPos) {
		super(title);
		this.dataset = new DefaultCategoryDataset();
		
		int totalDataSize = binaryData.size();
		
		List<int[]> nonZeroData = binaryData.stream()
				.filter(bits -> Arrays.stream(bits).anyMatch(bit -> bit != 0))
				.collect(toList());
		
		int nbNonZero = nonZeroData.size();
		int nbZeros = totalDataSize - nbNonZero;
		
		List<Integer> bitValuesSum = IntStream.range(0, bitWidth)
			.mapToObj(i -> nonZeroData.stream().mapToInt(bits -> bits[i])) //? use all data ?
			.map(iBitVals -> iBitVals.sum())
			.collect(toList());
		
		for(int idx = 0; idx < bitValuesSum.size(); idx++) {
			String row = getRow(idx, separatorPos, isFloat);
			String col = Integer.toString(idx);
			
			Double value = (double)bitValuesSum.get(idx);
			if(histogramType == HistogramType.RELATIVE_FREQUENCY)
				value /= nbNonZero;
			dataset.setValue(value, row, col);
		}
		
		String yName = histogramType == HistogramType.RELATIVE_FREQUENCY ? "Relative Frequency" : "Frequency";
		yName += " (N = " + nbNonZero;
		yName += ", zeros excluded = " + nbZeros;
		yName += ")";
		
		this.chart = ChartFactory.createBarChart(title, xName, yName, dataset, PlotOrientation.VERTICAL, true, true, true);
		this.chartPanel = new ChartPanel(chart);
		this.setContentPane(chartPanel);
		
		this.pack();
		this.setVisible(true);
		
		CategoryPlot plot = chart.getCategoryPlot();
		CategoryAxis dAxis = plot.getDomainAxis();
		dAxis.setMaximumCategoryLabelLines(2);
		dAxis.setLowerMargin(0);
		dAxis.setUpperMargin(0);
		
		BarRenderer br = (BarRenderer) plot.getRenderer();
		br.setItemMargin(0);
		br.setMaximumBarWidth(0.1);
		br.setDrawBarOutline(false);
		br.setSeriesPaint(0, Color.RED);
		br.setSeriesPaint(1, Color.BLUE);
		br.setSeriesPaint(2, Color.GREEN);
	}

	protected String getRow(int idx, int separatorPos, boolean isFloat) {
		String row;
		if(isFloat && idx == 0) row = "sign";
		else if (idx < separatorPos) row = isFloat ? "exp" : ("int ("+separatorPos+")");
		else row = isFloat ? "mant" : "frac";
		return row;
	}

	public void saveAsJpeg(Path outputDir, String filename) {
		try {
			ChartUtils.saveChartAsJPEG(outputDir.resolve(filename + ".jpeg").toFile(), chart, this.getWidth(), this.getHeight());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}