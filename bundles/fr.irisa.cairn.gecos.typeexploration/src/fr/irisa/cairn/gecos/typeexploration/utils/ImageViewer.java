package fr.irisa.cairn.gecos.typeexploration.utils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.Path;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public class ImageViewer extends JAppFrame {
	
	private BufferedImage image;
	
	public ImageViewer(String title, BufferedImage image) {
		super(title);
		
		this.image = image;
		
		this.getContentPane().add(new JLabel(new ImageIcon(image)));
		this.pack();
		this.setVisible(true);
	}
	
	public void saveAsJpeg(Path outputDir, String filename) {
		try {
			File imgFile = new File(outputDir.toFile(), filename+".bmp");
			ImageIO.write(image, "bmp", imgFile);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
