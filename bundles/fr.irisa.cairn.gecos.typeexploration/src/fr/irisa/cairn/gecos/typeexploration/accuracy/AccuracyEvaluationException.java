package fr.irisa.cairn.gecos.typeexploration.accuracy;

public class AccuracyEvaluationException extends Exception {

	private static final long serialVersionUID = -3986173190392680321L;

	public AccuracyEvaluationException(String string) {
		super(string);
	}

	public AccuracyEvaluationException(String string, Exception e) {
		super(string, e);
	}
}
