package fr.irisa.cairn.gecos.typeexploration.dse;

import static fr.irisa.cairn.gecos.typeexploration.utils.SolutionSpaceUtils.printErrorStats;
import static fr.irisa.cairn.gecos.typeexploration.utils.SolutionSpaceUtils.printSolution;
import static fr.irisa.cairn.gecos.typeexploration.utils.SolutionSpaceUtils.printSymbol;
import static fr.irisa.cairn.gecos.typeexploration.utils.SolutionSpaceUtils.streamSymboltypeConfigs;
import static java.util.stream.IntStream.range;

import java.math.BigInteger;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;

import com.google.common.base.Stopwatch;
import com.google.common.base.Throwables;

import fr.irisa.cairn.gecos.typeexploration.Components;
import fr.irisa.cairn.gecos.typeexploration.accuracy.AccuracyEvaluationException;
import fr.irisa.cairn.gecos.typeexploration.accuracy.ITypeParamGenerator;
import fr.irisa.cairn.gecos.typeexploration.accuracy.SimulationAccuracyEvaluator;
import fr.irisa.cairn.gecos.typeexploration.cost.CostEvaluationException;
import fr.irisa.cairn.gecos.typeexploration.model.IAccuracyEvaluator;
import fr.irisa.cairn.gecos.typeexploration.model.IAccuracyMetric;
import fr.irisa.cairn.gecos.typeexploration.model.ICostEvaluator;
import fr.irisa.cairn.gecos.typeexploration.model.ICostMetric;
import fr.irisa.cairn.gecos.typeexploration.model.IExplorationAlgorithm;
import fr.irisa.cairn.gecos.typeexploration.model.ISolution;
import fr.irisa.cairn.gecos.typeexploration.model.IUserConstraint;
import fr.irisa.cairn.gecos.typeexploration.solutionspace.SolutionSpaceAnalyzer;
import fr.irisa.cairn.gecos.typeexploration.utils.ScatterChart;
import gecos.core.Symbol;
import typeexploration.NumberTypeConfiguration;
import typeexploration.SolutionSpace;

/**
 * @author aelmouss
 */
public abstract class AbstractExplorationAlgorithm implements IExplorationAlgorithm {
	
	protected Logger explorationStats;
	protected List<String> explorationStatsNotesKeys;
	
	protected Logger logger;
	protected IAccuracyEvaluator accuracyEvaluator;
	protected ICostEvaluator costEvaluator;
	protected boolean enablePruning;
	protected IAccuracyMetric accuracyMetric;
	protected ICostMetric costMetric;
	protected IUserConstraint userAccuracyConstraint;
	protected ITypeParamGenerator generator;
	
	// stats counters
	protected AtomicInteger nbIterations = new AtomicInteger(0);
	protected AtomicInteger nbAccuracyEvals = new AtomicInteger(0);
	protected AtomicInteger nbCostEvals = new AtomicInteger(0);
	
	// Charts to display results
	private ScatterChart accuracyChart;
	private ScatterChart costChart;
	private ScatterChart accuracyVsCostChart;
//	private ScatterChart psnrDbChart;
//	private ScatterChart powerDbChart;
	
	protected Optional<Double> getAccuracy(ISolution s) {
		return accuracyMetric.getMetric(s);
	}

	protected Optional<Double> getCost(ISolution s) {
		return costMetric.getMetric(s);
	}
	
	protected void initialize() {
		if(accuracyEvaluator == null) {
			Components comp = Components.getInstance();
			
			this.accuracyEvaluator = comp.getAccuEvaluator();
			this.costEvaluator = comp.getCostEvaluator();
			this.logger = comp.getExploartionLogger();
			this.explorationStats = comp.getExploartionStatsLogger();
			
			this.enablePruning = comp.getConfiguration().isPruneSolutionSpaceFirst();
			this.accuracyMetric = comp.getAccuracyMetric();
			this.costMetric = comp.getCostMetric();
			this.userAccuracyConstraint = comp.getAccuracyConstraint();
			
			this.generator = Objects.requireNonNull(Components.getInstance().getTypesGenerator(), "No Types generator component was registerd!");
			
			if(comp.getConfiguration().isEnableCharts()) {
				accuracyChart = new ScatterChart(accuracyMetric.getDescription(), "Accuracy" , "Solution ID", accuracyMetric.getDescription());
				costChart = new ScatterChart(costMetric.getDescription(), "Cost" , "Solution ID", costMetric.getDescription());
				accuracyVsCostChart = new ScatterChart("Accuracy vs Cost", "Solution" , accuracyMetric.getDescription(), costMetric.getDescription());
	//			psnrDbChart = new ScatterChart("PSNR dB", "error" , "Solution ID", "PSNR (dB)");
	//			powerDbChart = new ScatterChart("Error Power dB", "error" , "Solution ID", "power (dB)");
			}
		}
	}
	
	protected void parseOptions(String[] options) {
		for (String option : options) {
			String[] split = option.split("=");
			if (split.length != 2) continue;
			parseOption(split[0], split[1]);
		}
	}
	
	protected void parseOption(String name, String value) {
	}

	/**
	 * This method should be overridden by a concrete subclass to specify
	 * the list of keys to query the notes field in ISolution. All values
	 * associated with a given key in this list is logged in the exploration
	 * stats log file.
	 * 
	 * @return
	 */
	protected List<String> initializeNotesKeys() {
		return null;
	}
	
	@Override
	public ISolution explore(SolutionSpace solSpace, String[] options) throws NoSolutionFoundException {
		initialize();
		parseOptions(options);
		
		Optional<BigInteger> sizeEstimate = solSpace.getAnalyzer().getSymbolsWithoutConstraints().stream()
				.map(s->BigInteger.valueOf(streamSymboltypeConfigs(solSpace, s, Components.getInstance().getExplorationMode()).count()))
				.reduce((x,y)->x.multiply(y));
		if (sizeEstimate.isPresent()) {
			logger.info(() -> "Rough Estimate of Solution Space Size: " + sizeEstimate.get());
		}
		
		explorationStatsNotesKeys = initializeNotesKeys();
		String vars = solSpace.getSymbols().stream().map(s->solSpace.getUniqueTypeName(s)).collect(Collectors.joining("\t"));
		String notes = (explorationStatsNotesKeys!=null&&explorationStatsNotesKeys.size()>0)?explorationStatsNotesKeys.stream().collect(Collectors.joining("\t"))+"\t":"";
		explorationStats.info("ID\tIteration\tAccuracy\tCost\t" + notes + vars);
		
		if(enablePruning)
			pruneSpace(solSpace);
		
		try {
			logger.info(() -> "Solution space has " + solSpace.getAnalyzer().getSymbolsWithoutConstraints().size()
					+ " unconstrained symbols (total = " + solSpace.getAnalyzer().getSymbols().size() + ")");


			Stopwatch w = Stopwatch.createStarted();
			ISolution solution = runExploration(solSpace);
			logger.info(() -> "Exploration finished in " + w + "\n"
					+ "\t- Total nb of accuracy evaluations: " + nbAccuracyEvals + "\n"
					+ "\t- Total nb of cost evaluations: " + nbCostEvals + "\n"
					+ "   Found solution (id=" + solution.getID() + "): \n"
					+ "\t- Accuracy = " + getAccuracy(solution).get() + "\n" 
					+ "\t- Cost = " + getCost(solution).get() + "\n"
					+ solution.metricDump()
					+ printSolution(solution));
			return solution;
		} finally {
			afterExploration();	
		}
	}

	/**
	 * @param solSpace
	 * @return the solution that was found
	 * @throws NoSolutionFoundException if no valid solution was found 
	 */
	public abstract ISolution runExploration(SolutionSpace solSpace) throws NoSolutionFoundException;
	

	protected void evaluateAccuracy(ISolution sol) throws AccuracyEvaluationException {
		Stopwatch watch = Stopwatch.createStarted();
		
		nbAccuracyEvals.incrementAndGet();
		try {
			accuracyEvaluator.evaluate(sol);
			logger.finest(() -> "Error stats for Solution " + sol.getID() + "\n" + printErrorStats(sol));
			Double ssim = sol.getAccuracy(SimulationAccuracyEvaluator.METRIC_NAME_SSIM);
			if ( ssim != null)
				logger.finest(() -> "SSIM for Solution " + sol.getID() + ": " + ssim );
		} catch (AccuracyEvaluationException e) {
			logger.severe("Failed to evaluate accuracy for solution " + sol.getID() + "\n" + Throwables.getStackTraceAsString(e));
			throw e;
		}
		logger.finer("      *** Accuracy evaluation done in " + watch);
	}

	protected void evaluateCost(ISolution sol) throws CostEvaluationException {
		Stopwatch watch = Stopwatch.createStarted();
		
		nbCostEvals.incrementAndGet();
		try {
			costEvaluator.evaluate(sol);
			logger.fine(() -> "Cost for Solution " + sol.getID() + " = " + getCost(sol).get());
		} catch (CostEvaluationException e) {
			logger.severe("Failed to evaluate cost for solution " + sol.getID() + "\n" + Throwables.getStackTraceAsString(e));
			throw e;
		}
		
		logger.fine("      *** Cost evaluation done in " + watch);
	}
	
	protected void display(ISolution sol) {
		if(Components.getInstance().getConfiguration().isEnableCharts()) {
			accuracyChart.addPoint(sol.getID(), getAccuracy(sol).get());
			costChart.addPoint(sol.getID(), getCost(sol).get());
			accuracyVsCostChart.addPoint(getAccuracy(sol).get(), getCost(sol).get());
	
	//		ErrorStats stats = sol.getAccuracy();
	//		psnrDbChart.addPoint(sol.getID(), stats.getPSNRdB());
	//		powerDbChart.addPoint(sol.getID(), stats.getPowerdB());
		}
	}
	
	protected void log(ISolution sol) {
		final double acc = (getAccuracy(sol).isPresent())?getAccuracy(sol).get():-1;
		final double cost = (getCost(sol).isPresent())?getCost(sol).get():-1;
		String msg = String.format("%d\t%d\t%f\t%f", sol.getID(), nbIterations.get(), acc, cost);
		String notes = (explorationStatsNotesKeys!=null&&explorationStatsNotesKeys.size()>0)?explorationStatsNotesKeys.stream().map(k->sol.getNote(k)).collect(Collectors.joining("\t"))+"\t":"";
		String types = sol.getSymbols().stream().map(s->generator.generate(sol.getType(s))).collect(Collectors.joining("\t"));
		explorationStats.info(msg + "\t" + notes + types);
	}
	
	protected void afterExploration() {
		if(Components.getInstance().getConfiguration().isEnableCharts()) {
			Path expDir = Components.getInstance().getOutputLoactions().getExplorationDir();
			accuracyChart.saveAsJpeg(expDir, "accuracy");
			costChart.saveAsJpeg(expDir, "cost");
			accuracyVsCostChart.saveAsJpeg(expDir, "accuracy-vs-cost");
			
	//		psnrDbChart.saveAsJpeg(expDir, "psnrdB");
	//		powerDbChart.saveAsJpeg(expDir, "errPowerdB");
		}
	}
	
	
	
	
	
	protected void pruneSpace(SolutionSpace solSpace) {
		logger.info("      *** Pruning Solution space ...");
		Stopwatch watch = Stopwatch.createStarted();
		
		findMinValidWs(solSpace, userAccuracyConstraint).entrySet().forEach(e -> {
			Symbol symbol = e.getKey();
			Integer minValidW = e.getValue();
			if(minValidW == 0)
				return;
			
			NumberTypeConfiguration configs = solSpace.getTypeFixedPtConfigs(symbol);
			if(configs == null) {
				// This is the case if the symbol isDefault and that the default configuration space had multiple Ws
				// In this case we add an explicit configuration space for the symbol with the pruned set of Ws.
				
				configs = solSpace.getDefaultTypeConfigs().getFixedPtCongurations().copy();
				solSpace.addTypeConfiguration(symbol, configs);
			}
			
			// Remove the first (invalid) Ws up to index minValidW-1
			EList<Integer> widthValues = configs.getTotalWidthValues();
			range(0, minValidW).forEach(i -> widthValues.remove(0));
			logger.fine(() -> "   - Reduced wordlengths set (by " + minValidW + ") of " + printSymbol(symbol) + " to: " + widthValues);
		});

		// reset solution space analyzer caches
		solSpace.getAnalyzer().reset();
		
		//pruning is counted as an iteration
		nbIterations.incrementAndGet();

		logger.info("      *** Pruning Solution space done in " + watch);
	}

	protected WordLengthsExplorer evaluateMaxWsSolution(SolutionSpace solSpace, IUserConstraint userAccuracyConstraint) 
			throws AccuracyEvaluationException, NoSolutionFoundException, CostEvaluationException {
		WordLengthsExplorer builderWithAllWtoMax = new WordLengthsExplorer(solSpace);
		builderWithAllWtoMax.setAllWToMax();
		ISolution solution = builderWithAllWtoMax.createCurrentSolution();
		evaluateAccuracy(solution);
		evaluateCost(solution);
		if(! userAccuracyConstraint.isValid(solution)) {
			String printSolution = printSolution(solution);
			logger.info("Max width solution tested : \n"+solution.metricDump() + printSolution);
			throw new NoSolutionFoundException("Using maximum allowed wordlengths for all variables still result in "
					+ "violation of the User Accuracy constraint! (maxW accuracy = " + getAccuracy(solution).get() + ")\n"
					+ "Consider increasing the allowed wordlengths, or relaxing the acuracy constraint.");
		}
		return builderWithAllWtoMax;
	}
	
	/**
	 * Find for each (non-same non-default) Symbol s in the solution space S, the minimum valid Ws such that:
	 * the solution {W(s) = Ws, W(o) = MAX_W(o)} for o in S - {s} , does not violate
	 * the accuracy constraint.
	 */
	protected Map<Symbol, Integer> findMinValidWs(SolutionSpace solSpace, IUserConstraint userAccuracyConstraint)  {
		SolutionSpaceAnalyzer analyzer = solSpace.getAnalyzer();
		WordLengthsExplorer builderWithAllWtoMax = new WordLengthsExplorer(solSpace);
		builderWithAllWtoMax.setAllWToMax();
		
		Map<Symbol, Integer> symbolMinValidW = new HashMap<>();
		analyzer.getSymbolsWithoutConstraints().stream()
			.filter(s -> builderWithAllWtoMax.getMaxWIdx(s) != 0) // s only has one W
			.parallel()
			.forEach(s -> {
				int indexMax = findMinValidWFor(s, builderWithAllWtoMax, userAccuracyConstraint);
				
				if(indexMax < 0) {
					logger.warning(() -> "The minimum valid Wordlength index found for '" + printSymbol(s) + "' is negative! " + indexMax);
					return;
				}

				synchronized (symbolMinValidW) {
					symbolMinValidW.put(s, indexMax);
				}
			});
		
		return symbolMinValidW;
	}

	private int findMinValidWFor(Symbol s, WordLengthsExplorer builderWithAllWtoMax, IUserConstraint userAccuracyConstraint) {
		if(builderWithAllWtoMax.getMaxWIdx(s) == 0) // s only has one W
			return 0;
		
		WordLengthsExplorer builder = builderWithAllWtoMax.copy();
		
		//Binary Search
		int indexMin = 0;
		int indexMax = builder.getMaxWIdx(s)+1;
		int index = (int)Math.floor((indexMax + indexMin)/2);
		int lastValid = -1;
		do {
			builder.setWTo(s, index);
			
			ISolution solution = builder.createCurrentSolution();
			boolean isValid = false;
			try {
				evaluateAccuracy(solution);
				evaluateCost(solution);
				log(solution);
				isValid = userAccuracyConstraint.isValid(solution);
			} catch (AccuracyEvaluationException | CostEvaluationException e) {
				logger.warning("Skipping pruning solution " +  + solution.getID() + " of symbol " + s + "cause: " + e.getMessage());
				isValid = false;
			}
			
			if (isValid) {
				indexMax = index;
				lastValid = index;
			} else {
				indexMin = index+1;
			}
			index = (int)Math.floor((indexMax + indexMin)/2);
		} while(indexMin != indexMax);
		
		if (lastValid<0) {
			throw new RuntimeException("Pruning failed: no valid bitwidth found.");
		}
		
		return lastValid;
	}
	
}
