package fr.irisa.cairn.gecos.typeexploration.utils;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.statistics.HistogramType;

import com.google.common.collect.Streams;

public class BitHistogramChart2 extends JAppFrame {
	
	private static final int N_BITS = 64;
	private static final int N_EXPONENT_BITS = 11;
	private static final int N_MANTISSA_BITS = 52;
	
	
//	class MyHistogramDataset extends HistogramDataset {
//		@Override
//		public double getXValue(int series, int item) {
//			return Math.floor(super.getXValue(series, item));
//		}
//	}
	
	protected HistogramDataset dataset;
	protected JFreeChart chart;
	protected ChartPanel chartPanel;

	public BitHistogramChart2(String title, String nameFunction, String xName, String yName, HistogramType histogramType, 
			List<int[]> data, int nbBits) {
		super(title);
		this.dataset = new HistogramDataset(); //SimpleHistogramDataset
        dataset.setType(histogramType);
		
        double[] sigData = data.stream().mapToDouble(bits -> bits[0]).toArray();
        double[] expData = extract(data, 1, N_EXPONENT_BITS);
        double[] manData = extract(data, 1 + N_EXPONENT_BITS, N_MANTISSA_BITS);
        
//        System.out.println(Arrays.stream(sigData).limit(100).mapToObj(String::valueOf).collect(joining(" ")));
//        System.out.println(Arrays.stream(expData).limit(100).mapToObj(String::valueOf).collect(joining(" ")));
//        System.out.println(Arrays.stream(manData).limit(100).mapToObj(String::valueOf).collect(joining(" ")));
       
		dataset.addSeries("sign", sigData, sigData.length);
		dataset.addSeries("exponent", expData, expData.length);
		dataset.addSeries("mantissa", manData, manData.length);
		
		this.chart = ChartFactory.createHistogram(title, xName, yName, dataset, PlotOrientation.VERTICAL, true, true, true);
		this.chartPanel = new ChartPanel(chart);
		this.setContentPane(chartPanel);
		this.pack();
		this.setVisible(true);
		
		
//		XYPlot plot = chart.getXYPlot();
//		ValueAxis dAxis = plot.getDomainAxis();
//		dAxis.setTickLabelPaint(Color.RED);
		
//		XYBarRenderer br = (XYBarRenderer) plot.getRenderer();
//		br.setSeriesPaint(0, Color.RED);
//		br.setSeriesPaint(1, Color.BLUE);
//		br.setSeriesPaint(2, Color.GREEN);

	}

	private double[] extract(List<int[]> data, int offset, int limit) {
		return data.stream()
        		.map(bits -> Arrays.stream(bits).skip(offset).limit(limit))
        		.flatMap(bits -> Streams.mapWithIndex(bits, (c, pos) -> c == 0 ? -1 : Double.valueOf((double)pos + offset))) //XXX
        		.filter(c -> c >= 0)
        		.mapToDouble(Number::doubleValue)
        		.toArray();
	}
	
	public void saveAsJpeg(Path outputDir, String filename) {
		try {
			ChartUtils.saveChartAsJPEG(outputDir.resolve(filename + ".jpeg").toFile(), chart, this.getWidth(), this.getHeight());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}