package fr.irisa.cairn.gecos.typeexploration.model;

import gecos.gecosproject.GecosProject;
import typeexploration.SolutionSpace;

/**
 * @author aelmouss
 */
public interface ISolutionSpaceBuilder {

	public SolutionSpace build(GecosProject project);
	
	public SolutionSpace getSolutionSpace();
}
