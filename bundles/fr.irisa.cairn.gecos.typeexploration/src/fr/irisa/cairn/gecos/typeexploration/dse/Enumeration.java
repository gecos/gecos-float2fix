package fr.irisa.cairn.gecos.typeexploration.dse;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import fr.irisa.cairn.gecos.typeexploration.accuracy.AccuracyEvaluationException;
import fr.irisa.cairn.gecos.typeexploration.cost.CostEvaluationException;
import fr.irisa.cairn.gecos.typeexploration.model.ISolution;
import fr.irisa.cairn.gecos.typeexploration.model.UserFactory;
import gecos.core.Symbol;
import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;
import typeexploration.FixedPointTypeParam;
import typeexploration.SolutionSpace;
import typeexploration.TypeParam;

public class Enumeration extends AbstractExplorationAlgorithm {
	
	protected SolutionSpace solSpace;
	protected String[] files;
	protected List<SolutionSpaceFromFile> solSpaceFromFiles;
	
	protected ISolution optimalSolution;
	
	@Override
	protected List<String> initializeNotesKeys() {
		List<String> list = new ArrayList<String>( 1 );
		
		list.add("name");
		
		return list;
	}

	@Override
	public ISolution runExploration(SolutionSpace solSpace) throws NoSolutionFoundException {
		this.solSpace = solSpace;
		this.solSpaceFromFiles = new ArrayList<>(files.length);
		
		for (String file : files) {
			SolutionSpaceFromFile ssff = new SolutionSpaceFromFile(file);
			ssff.load();
			solSpaceFromFiles.add(ssff);
		}

		exploreAllCombinations(null, 0);
		
		//currently, it is just the first solution
		return optimalSolution;
	}
	
	@Override
	protected void parseOption(String name, String value) {
		if (name.contentEquals("files")) {
			files = value.split(",");
		}
	}
	
	protected void exploreAllCombinations(List<ISolution> comb, int depth) {
		//base case
		if (depth >= solSpaceFromFiles.size()) {
			try {
				ISolution merged = mergeSolutions(comb);
				evaluateAccuracy(merged);
				evaluateCost(merged);
				log(merged);
				if (optimalSolution == null) optimalSolution = merged;
			} catch (AccuracyEvaluationException | CostEvaluationException e) {
				logger.warning("Error while evaluation solutions :" + e.getMessage());
			}
		} else {
			List<ISolution> solutions = solSpaceFromFiles.get(depth).solutions;
			for (ISolution sol : solutions) {
				List<ISolution> list;
				if (comb == null) {
					list = new LinkedList<ISolution>();
				} else {
					list = new LinkedList<ISolution>(comb);
				}
				list.add(sol);
				exploreAllCombinations(list, depth+1);
			}
		}
	}
	
	protected ISolution mergeSolutions(List<ISolution> solutions) {
		ISolution mergedSol = UserFactory.solution(solSpace);
		
		String name = solutions.stream().map(s->s.getNote("name")).collect(Collectors.joining("-"));
		mergedSol.setNote("name", name);
		
		for (ISolution solution : solutions) {
			for (Symbol symbol : solution.getSymbols()) {
				if (mergedSol.getType(symbol) == null) {
					mergedSol.setType(symbol, solution.getType(symbol));
				} else {
					TypeParam mergedType = mergeTypeParam(mergedSol.getType(symbol), solution.getType(symbol));
					mergedSol.setType(symbol, mergedType);
				}
			}
		}

		return mergedSol;
	}
	
	protected TypeParam mergeTypeParam(TypeParam typeA, TypeParam typeB) {
		if (typeA instanceof FixedPointTypeParam && typeB instanceof FixedPointTypeParam) {
			return mergeTypeParam((FixedPointTypeParam)typeA, (FixedPointTypeParam)typeB);
		}
		
		throw new IllegalArgumentException("Canot merge two types of different kind: " + typeA  + " " + typeB);
	}
	protected TypeParam mergeTypeParam(FixedPointTypeParam typeA, FixedPointTypeParam typeB) {
		if (typeA.isSigned() != typeB.isSigned()) {
			throw new IllegalArgumentException("Signed must match.");
		}
		if (typeA.getQuantificationMode() != typeB.getQuantificationMode()) {
			throw new IllegalArgumentException("Quantification Mode must match.");
		}
		if (typeA.getOverflowMode() != typeB.getOverflowMode()) {
			throw new IllegalArgumentException("Overflow Mode must match.");
		}

		int W = Math.min(typeA.getTotalWidth(), typeB.getTotalWidth());
		int I = Math.min(typeA.getIntegerWidth(), typeB.getIntegerWidth());
		boolean signed = typeA.isSigned();
		QuantificationMode Q = typeA.getQuantificationMode();
		OverflowMode O = typeA.getOverflowMode();
		
		FixedPointTypeParam mergedType = UserFactory.fixedTypeParam(W, I, signed, Q, O);
		if (!mergedType.equals(typeA) && !mergedType.equals(typeB)) {
			throw new IllegalArgumentException("Expecting the merged type to match one of the input types.");
		}
		
		return mergedType;
	}

	private static final Pattern acFixedFull =  Pattern.compile("ac_fixed<(\\d+), ((-|\\+)?\\d+), 1, AC_TRN, AC_WRAP>");
	private static final Pattern acFixedShort =  Pattern.compile("W=(\\d+) I=((-|\\+)?\\d+)");
	
	protected class SolutionSpaceFromFile {
		
		protected final String file;
		protected List<ISolution> solutions;
		
		public SolutionSpaceFromFile(String file) {
			this.file = file;
			solutions = new LinkedList<>();
		}
		
		protected void load() {
			BufferedReader reader;
			try {
				reader = new BufferedReader(new FileReader(file));
				
				String labelLine = reader.readLine().replaceAll("\r|\n", "");
				String[] labels = labelLine.split("\t");
				
				if (labels.length < 2 || !labels[0].contentEquals("name")) {
					reader.close();
					throw new IllegalArgumentException("Expecting first row to be labels, and the first column should be 'name'");
				}

				List<Symbol> symbols = new ArrayList<Symbol>(labels.length-1);
				{
					//Create a map from unique name for each variable to symbol
					Map<String, Symbol> reverseMap = new TreeMap<>();
					for (Symbol s : solSpace.getSymbols()) {
						reverseMap.put(solSpace.getUniqueTypeName(s), s);
					}
					
					//Then make sure that each label corresponds to a symbol,
					// while constructing a list that mapps indices to symbol
					for (int i = 1; i < labels.length; i++) {
						if (reverseMap.get(labels[i]) == null) {
							reader.close();
							throw new RuntimeException("Solution space does not include specified type: " + labels[i]);
						}
						symbols.add(reverseMap.get(labels[i]));
					}
				}

				String line = reader.readLine();
				while (line != null) {
					String[] values = line.replaceAll("\r|\n", "").split("\t");
					
					if (values.length > 1) {
						ISolution sol = UserFactory.solution(solSpace);
						sol.setNote("name",values[0]);
												
						for (int i = 1; i < values.length; i++) {
							sol.setType(symbols.get(i-1), parse(values[i]));
						}
						
						solutions.add(sol);
					}
					
					line = reader.readLine();
				}
				
				reader.close();
			} catch (IOException ioe) {
				throw new RuntimeException(ioe);
			}
		}
		
		private TypeParam parse(String type) {
			int W=32;
			int I=16;
			boolean signed = true;
			QuantificationMode Q = QuantificationMode.AC_TRN;
			OverflowMode O = OverflowMode.AC_WRAP;

			Matcher matcherShort = acFixedShort.matcher(type);
			Matcher matcherFull = acFixedFull.matcher(type);
			
			if (matcherFull.matches()) {
				W = Integer.parseInt(matcherFull.group(1));
				I = Integer.parseInt(matcherFull.group(2));
			} else if (matcherShort.matches()) {
				W = Integer.parseInt(matcherShort.group(1));
				I = Integer.parseInt(matcherShort.group(2));
			} else {
				throw new RuntimeException("Unable to parse type specification: " + type);
			}
			
			return UserFactory.fixedTypeParam(W, I, signed, Q, O);
		}
	}
}
