package fr.irisa.cairn.gecos.typeexploration.model;

import java.util.Map;

import fr.irisa.cairn.gecos.core.profiling.backend.ProfilingInfo;
import fr.irisa.cairn.gecos.typeexploration.accuracy.AccuracyEvaluationException;
import gecos.core.Symbol;

/**
 * @author aelmouss
 */
public interface IAccuracyEvaluator {

	public void evaluate(ISolution solution) throws AccuracyEvaluationException;
	
	//XXX this is may not be possible for other accuracy evaluator than Simulation based
	public Map<Symbol, ProfilingInfo<? extends Number>> getReferenceProfilingInfo() throws AccuracyEvaluationException;

}
