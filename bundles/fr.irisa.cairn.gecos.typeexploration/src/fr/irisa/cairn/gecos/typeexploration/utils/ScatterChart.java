package fr.irisa.cairn.gecos.typeexploration.utils;

import java.io.IOException;
import java.nio.file.Path;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

@SuppressWarnings("serial")
public class ScatterChart extends JAppFrame {
	
	protected XYSeries data;
	protected XYSeriesCollection dataset;
	protected JFreeChart chart;
	protected ChartPanel chartPanel;

	public ScatterChart(String title, String seriesName, String xName, String yName) {
		super(title);
		this.dataset = new XYSeriesCollection();
		this.data = new XYSeries(seriesName);
		this.dataset.addSeries(this.data);
		
		this.chart = ChartFactory.createScatterPlot(title, xName, yName, dataset, PlotOrientation.VERTICAL, true, true, true);
		this.chartPanel = new ChartPanel(chart);
		this.setContentPane(chartPanel);
		this.pack();
		this.setVisible(true);
	}
	
	public synchronized void addPoint(Number solId, Number accuracy) {
		data.add(solId, accuracy);
	}
	
	public void saveAsJpeg(Path outputDir, String filename) {
		try {
			ChartUtils.saveChartAsJPEG(outputDir.resolve(filename + ".jpeg").toFile(), chart, this.getWidth(), this.getHeight());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}