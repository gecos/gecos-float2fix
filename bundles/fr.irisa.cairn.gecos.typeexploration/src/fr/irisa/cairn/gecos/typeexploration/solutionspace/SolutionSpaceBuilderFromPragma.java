package fr.irisa.cairn.gecos.typeexploration.solutionspace;

import static fr.irisa.cairn.gecos.model.utils.annotations.PragmaUtils.findPragmaContent;
import static fr.irisa.cairn.gecos.model.utils.annotations.PragmaUtils.getPragmaName;
import static java.util.Collections.singletonList;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import fr.irisa.cairn.gecos.typeexploration.model.UserFactory;
import gecos.annotations.FileLocationAnnotation;
import gecos.core.Symbol;
import typeexploration.FixedPointTypeConfiguration;
import typeexploration.FloatPointTypeConfiguration;
import typeexploration.SolutionSpace;
import typeexploration.TypeConfiguration;
import typeexploration.TypeConfigurationSpace;
import typeexploration.TypeConstraint;

/**
 * @author aelmouss
 */
public class SolutionSpaceBuilderFromPragma extends AbstractSolutionSpaceBuilder {

	public SolutionSpaceBuilderFromPragma(List<TypeConfiguration> defaultTypeConfigs) {
		super(defaultTypeConfigs);
	}

	@Override
	protected boolean hasCustomTypeConfigs(Symbol s) {
		return PragmaExploreParser.fastHasPragmaExplore(s);
	}

	@Override
	protected TypeConfigurationSpace createConfigurationSpace(Symbol s) {
		PragmaExploreParser parser = new PragmaExploreParser(getSolutionSpace());
		parser.parsePragmaExplore(s);
		return UserFactory.configurationSpace(parser.configs, parser.constraints);
	}

	// ! It's probably a good idea to define a proper grammar if this keeps on
	// growing ..
	public static class PragmaExploreParser {

		// Explore FIX
		private static final String PRAGMA_EXPLORE_FIX_NAME = "EXPLORE_FIX";
		private static final Pattern PRAGMA_EXPLORE_FIX_ARG_PATTERN = Pattern
				.compile("(W|I|S)\\s*=\\s*\\{([^WIS]*)\\}");
		private static final Pattern PRAGMA_EXPLORE_FIX_PATTERN = Pattern
				.compile("^\\s*" + PRAGMA_EXPLORE_FIX_NAME + "\\s+(" + PRAGMA_EXPLORE_FIX_ARG_PATTERN + "\\s*)+");

		// Explore Float
		private static final String PRAGMA_EXPLORE_FLOAT_NAME = "EXPLORE_FLOAT";
		private static final Pattern PRAGMA_EXPLORE_FLOAT_ARG_PATTERN = Pattern
				.compile("(W|E|S)\\s*=\\s*\\{([^WES]*)\\}");
		private static final Pattern PRAGMA_EXPLORE_FLOAT_PATTERN = Pattern
				.compile("^\\s*" + PRAGMA_EXPLORE_FLOAT_NAME + "\\s+(" + PRAGMA_EXPLORE_FLOAT_ARG_PATTERN + "\\s*)+");

		// Explore Contraint
		private static final String PRAGMA_EXPLORE_CONSTRAINT_NAME = "EXPLORE_CONSTRAINT";
		private static final Pattern PRAGMA_EXPLORE_CONSTRAINT_ARG_PATTERN = Pattern.compile("(SAME)\\s*=\\s*(\\w+)");
		private static final Pattern PRAGMA_EXPLORE_CONSTRAINT_PATTERN = Pattern
				.compile("^\\s*" + PRAGMA_EXPLORE_CONSTRAINT_NAME + "\\s+" + PRAGMA_EXPLORE_CONSTRAINT_ARG_PATTERN);

		// Set values separators
		private static final String SET_VALUES_SEPARATOR = ",";
		private static final String RANGE_VALUES_SEPARATOR = "\\.\\.";

		// error msg
		private static final String PRAGMA_EXPLORE_FIX_USAGE_MSG = "#pragma EXPLORE_FIX [(W|I|S)={[(MIN..MAX)|(VAL),]*}]+";
		private static final String PRAGMA_EXPLORE_FLOAT_USAGE_MSG = "#pragma EXPLORE_FLOAT [(W|E|S)={[(MIN..MAX)|(VAL),]*}]+";
		private static final String PRAGMA_EXPLORE_CONSTRAINT_USAGE_MSG = "#pragma EXPLORE_CONSTRAINT SAME=<symbol_name>";

		private List<TypeConfiguration> configs;
		private List<TypeConstraint> constraints;
		private SolutionSpace solutionSpace;

		public PragmaExploreParser(SolutionSpace solutionSpace) {
			this.solutionSpace = solutionSpace;
		}

		public void parsePragmaExplore(Symbol s) {
			this.configs = new ArrayList<>();
			this.constraints = new ArrayList<>();

			for (String content : s.getPragma().getContent()) {
				if (isPragmaExploreFix(content)) {
					try {
						configs.add(parsePragmaExploreFix(content,
								solutionSpace.getDefaultTypeConfigs().getFixedPtCongurations()));
					} catch (PragmaSyntaxError e) {
						throw invalidPragmaExploreSyntax(PRAGMA_EXPLORE_FIX_NAME, s, e);
					}
				} else if (isPragmaExploreFloat(content)) {
					try {
						configs.add(parsePragmaExploreFloat(content,
								solutionSpace.getDefaultTypeConfigs().getFloatPtCongurations()));
					} catch (PragmaSyntaxError e) {
						throw invalidPragmaExploreSyntax(PRAGMA_EXPLORE_FLOAT_NAME, s, e);
					}
				} else if (isPragmaExploreConstraint(content)) {
					try {
						constraints.add(parsePragmaExploreConstraint(content, s));
					} catch (PragmaSyntaxError e) {
						throw invalidPragmaExploreSyntax(PRAGMA_EXPLORE_CONSTRAINT_NAME, s, e);
					}
				}
			}
		}

		private static boolean fastHasPragmaExplore(Symbol s) {
			return findPragmaContent(s,
					c -> getPragmaName(c).toUpperCase().matches("(" + PRAGMA_EXPLORE_FIX_NAME + ")|("
							+ PRAGMA_EXPLORE_FLOAT_NAME + ")|(" + PRAGMA_EXPLORE_CONSTRAINT_NAME + ")")).findAny()
									.isPresent();
		}

		private static boolean isPragmaExploreFix(String pragmaContent) {
			return PRAGMA_EXPLORE_FIX_PATTERN.matcher(pragmaContent.toUpperCase()).matches();
		}

		private static boolean isPragmaExploreFloat(String pragmaContent) {
			return PRAGMA_EXPLORE_FLOAT_PATTERN.matcher(pragmaContent.toUpperCase()).matches();
		}

		private static boolean isPragmaExploreConstraint(String pragmaContent) {
			return PRAGMA_EXPLORE_CONSTRAINT_PATTERN.matcher(pragmaContent.toUpperCase()).matches();
		}

		private static TypeConstraint parsePragmaExploreConstraint(String pragmaContent, Symbol s) {
			Matcher matcher = PRAGMA_EXPLORE_CONSTRAINT_ARG_PATTERN.matcher(pragmaContent);

			if (matcher.find()) {
				assert (matcher.groupCount() == 2);
				String param = matcher.group(1);
				String value = matcher.group(2);

				if (param.equals("SAME")) {
					Symbol asSymbol = s.getContainingScope().lookup(value);
					if (asSymbol == null)
						throw new PragmaSyntaxError("Symbol not found '" + value + "'");
					return UserFactory.sameTypeConstraint(asSymbol);
				} else
					throw new PragmaSyntaxError("invalid constraint '" + param + "'");
			}

			throw new PragmaSyntaxError("invalid type constraint '" + pragmaContent + "'");
		}

		private static FixedPointTypeConfiguration parsePragmaExploreFix(String pragmaContent,
				FixedPointTypeConfiguration fixedPointTypeConfiguration) {
			Matcher matcher = PRAGMA_EXPLORE_FIX_ARG_PATTERN.matcher(pragmaContent);

			List<Integer> iValues = null;
			List<Integer> wValues = null;
			List<Integer> sValues = null;
			while (matcher.find()) {
				assert (matcher.groupCount() == 2);
				String param = matcher.group(1);
				String setValue = matcher.group(2);

				if (param.equals("W"))
					wValues = parseSetValue(setValue);
				else if (param.equals("I"))
					iValues = parseSetValue(setValue);
				else if (param.equals("S"))
					sValues = parseSetValue(setValue);
				else
					throw new PragmaSyntaxError("invalid parameter name '" + param + "'");
			}
			if (iValues == null && wValues == null && sValues == null)
				throw new PragmaSyntaxError();

			return UserFactory.fixedConfiguration(fixedPointTypeConfiguration, wValues, iValues, sValues);
		}

		private static FloatPointTypeConfiguration parsePragmaExploreFloat(String pragmaContent,
				FloatPointTypeConfiguration floatPointTypeConfiguration) {
			Matcher matcher = PRAGMA_EXPLORE_FLOAT_ARG_PATTERN.matcher(pragmaContent);

			List<Integer> eValues = null;
			List<Integer> wValues = null;
			List<Integer> sValues = null;
			while (matcher.find()) {
				assert (matcher.groupCount() == 2);
				String param = matcher.group(1);
				String setValue = matcher.group(2);
				List<Integer> values = parseSetValue(setValue);

				if (param.equals("W"))
					wValues = values;
				else if (param.equals("E"))
					eValues = values;
				else if (param.equals("S"))
					sValues = parseSetValue(setValue);
				else
					throw new PragmaSyntaxError("invalid parameter name '" + param + "'");
			}
			if (eValues == null && wValues == null && sValues == null)
				throw new PragmaSyntaxError();

			return UserFactory.floatConfiguration(floatPointTypeConfiguration, wValues, eValues, sValues);
		}

		public static List<Integer> parseSetValue(String setValue) {
			List<Integer> set = new ArrayList<>();
			for (String v : setValue.split(SET_VALUES_SEPARATOR)) {
				String[] range = v.split(RANGE_VALUES_SEPARATOR);
				int first = Integer.parseInt(range[0].trim());
				if (range.length == 1) {
					set.add(first);
				} else if (range.length == 2) {
					int last = Integer.parseInt(range[1].trim());
					IntStream.rangeClosed(first, last).forEach(set::add);
				} else
					throw new PragmaSyntaxError("invalid parameter value in pragma EXPLORE");
			}
			return set;
		}

		private static PragmaSyntaxError invalidPragmaExploreSyntax(String pragmaName, Symbol s, Exception cause) {
			FileLocationAnnotation loc = s.getFileLocation();
			String msg = "Invalid pragma EXPLORE syntax on : " + s;
			if (loc != null)
				msg += " (at " + loc.getFilename() + ":" + loc.getStartingLine() + ")";
			msg += "\n";

			switch (pragmaName) {
			case PRAGMA_EXPLORE_FIX_NAME:
				msg += PRAGMA_EXPLORE_FIX_USAGE_MSG;
				break;
			case PRAGMA_EXPLORE_FLOAT_NAME:
				msg += PRAGMA_EXPLORE_FLOAT_USAGE_MSG;
				break;
			case PRAGMA_EXPLORE_CONSTRAINT_NAME:
				msg += PRAGMA_EXPLORE_CONSTRAINT_USAGE_MSG;
				break;
			default:
				break;
			}

			if (cause == null)
				return new PragmaSyntaxError(msg);
			return new PragmaSyntaxError(msg, cause);
		}

	}

	/*
	 * ********************************** Tests
	 **********************************/

	public static void main(String[] args) {
		String[] tests = new String[] {
			"EXPLORE_FIX W={10..20, 32} I = { 4 , 5}",
			"EXPLORE_FIX I={10..20,32} W={4,5}",
			"EXPLORE_FIX W={10..20, 32}",
			
			"EXPLORE_FLOAT W={10..20, 32} E = { 4 , 5}",
			"EXPLORE_FLOAT E={10..20,32} W={4,5}",
			"EXPLORE_FLOAT W={10..20, 32}",			
			"EXPLORE_FLOAT S={0..1} W={10..20, 32}",			
		};
		
		for(String s : tests) {
			System.out.println(s);
			if(PragmaExploreParser.isPragmaExploreFix(s)) {
				TypeConfiguration config = PragmaExploreParser.parsePragmaExploreFix(s, UserFactory.defaultFixedConfiguration(singletonList(32), singletonList(16),singletonList(1)));
				System.out.println("   " + config);
			} else if(PragmaExploreParser.isPragmaExploreFloat(s)) {
				TypeConfiguration config = PragmaExploreParser.parsePragmaExploreFloat(s, UserFactory.defaultFloatConfiguration(singletonList(32), singletonList(8),singletonList(1)));
				System.out.println("   " + config);
			} else
				System.out.println("    NO MATCH");
		}
		
	}

}
