package fr.irisa.cairn.gecos.typeexploration.accuracy;

import static fr.irisa.cairn.gecos.typeexploration.GlobalParameters.SIM_ACTYPE_INC_FOLDER;
import static java.util.Collections.singletonList;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import templates.xtend.GecosTypeTemplate;
import templates.xtend.GecosTypeTemplate.BitLevelDataType;
import templates.xtend.GecosTypeTemplate.CustomFloatDataType;

/**
 * FIXME: ac_float is not compiling !
 *  
 * @author aelmouss
 */
public class AcDatatypesGenerator extends AbstractCodeGenerator {
	
	private static final List<Path> typeLibsIncDirs = singletonList((SIM_ACTYPE_INC_FOLDER));
	
	public AcDatatypesGenerator() {
		GecosTypeTemplate.bitLevelDataType = BitLevelDataType.MentorAC;
		GecosTypeTemplate.customFloatDataType = CustomFloatDataType.AC_FLOAT;
	}

	public List<Path> getIncDirs(boolean useFixed, boolean useFloat) {
		return typeLibsIncDirs;
	}

	public List<String> getHeaders(boolean useFixed, boolean useFloat) {
		List<String> list = new ArrayList<>();
		if(useFixed) list.add("ac_fixed.h");
		if(useFloat) list.add("ac_float.h");
		return list;
	}

	public List<String> getLibs(boolean useFixed, boolean useFloat) {
		return null;
	}

	public List<Path> getLibDirs(boolean useFixed, boolean useFloat) {
		return null;
	}
	
}