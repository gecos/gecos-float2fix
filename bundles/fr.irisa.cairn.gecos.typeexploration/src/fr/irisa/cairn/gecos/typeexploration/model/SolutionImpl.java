package fr.irisa.cairn.gecos.typeexploration.model;

import static fr.irisa.cairn.gecos.model.utils.misc.StreamUtils.join;
import static fr.irisa.cairn.gecos.typeexploration.utils.SolutionSpaceUtils.printSymbol;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import fr.irisa.cairn.gecos.core.profiling.backend.ErrorStats;
import gecos.core.Symbol;
import typeexploration.SolutionSpace;
import typeexploration.TypeParam;

/**
 * @author aelmouss
 */
public class SolutionImpl implements ISolution {

	protected SolutionSpace solSpace;
	protected int id = -1;
	protected Map<String, String> notes;
	protected Map<String, Map<String,Double>> costMap;
	protected Map<String, Double> accuracyMap;
	
	//! this should be optional
	protected Map<Symbol, List<ErrorStats>> errorStats; // <saved symbols, one ErrorStats per snapshot>

	protected Map<Symbol, TypeParam> symbolBaseTypes; //! should not be publicly exposed since getType() handles automatic completion

	public SolutionImpl(SolutionSpace solSpace) {
		this.solSpace = solSpace;
		this.notes = new TreeMap<>();
		this.errorStats = new HashMap<>();
		this.costMap = new TreeMap<>();
		this.accuracyMap = new TreeMap<>();
		this.symbolBaseTypes = new HashMap<>();
	}

	@Override
	public int getID() {
		return id;
	}

	@Override
	public void setID(int id) {
		this.id = id;
	}

	@Override
	public void setNote(String key, String note) {
		notes.put(key, note);
	}

	@Override
	public String getNote(String key) {
		return notes.get(key);
	}

	@Override
	public Map<String, String> getNotes() {
		return Collections.unmodifiableMap(notes);
	}
	
	@Override
	public SolutionSpace getSolutionSpace() {
		return solSpace;
	}

	@Override
	public List<Symbol> getSymbols() {
		return solSpace.getAnalyzer().getSymbols();
	}
	
	@Override
	public void addCost(String costMetricName, String componentName, Double cost) {
		if (!costMap.containsKey(costMetricName))
			costMap.put(costMetricName, new TreeMap<>());
		costMap.get(costMetricName).put(componentName, cost);
	}
	
	@Override
	public void removeCost(String costMetricName, String componentName) {
		if (costMap.containsKey(costMetricName))
			costMap.get(costMetricName).remove(componentName);
	}

	@Override
	public Double getCost(String costMetricName, String componentName) {
		if (!costMap.containsKey(costMetricName)) return null;
		return costMap.get(costMetricName).get(componentName);
	}
	
	@Override
	public Set<String> getCostComponentNames(String costMetricName) {
		if (!costMap.containsKey(costMetricName)) return null;
		return costMap.get(costMetricName).keySet();
	}
	
	@Override
	public void addAccuracy(String accuracyMetricName, Double accuracy) {
		accuracyMap.put(accuracyMetricName, accuracy);
	}
	
	@Override
	public void removeAccuracy(String accuracyMetricName) {
		accuracyMap.remove(accuracyMetricName);
	}

	@Override
	public Double getAccuracy(String accuracyMetricName) {
		return accuracyMap.get(accuracyMetricName);
	}
	

	@Override
	public Map<Symbol, List<ErrorStats>> getAllErrorStats() {
		return errorStats;
	}
	
	@Override
	public void setErrorStats(Symbol s, List<ErrorStats> stats) {
		errorStats.put(s, stats);
	}
	
	@Override
	public String metricDump() {
		return "Metrics:\n"
				+ "\t- Accuracy = " + accuracyMap + "\n" 
				+ "\t- Cost = " + costMap + "\n";
	}
	
	@Override
	public String toString() {
		return "Solution " + getID() + ":\n"
				+ join(getSymbols().stream(), "\n", s -> "\tSymbol '" + printSymbol(s) + "' : " + getType(s));
	}


	@Override
	public void setType(Symbol s, TypeParam t) {
		symbolBaseTypes.put(s, t);
	}

	@Override
	public TypeParam getType(Symbol s) {
		TypeParam t = symbolBaseTypes.get(s);
		if(t == null) {
			Symbol sameAs = solSpace.getAnalyzer().getSymbolsWithSame().get(s);
			if(sameAs != null)
				t = getType(sameAs);
		}
		return t;
	}
}
