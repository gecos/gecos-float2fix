package fr.irisa.cairn.gecos.typeexploration.dse;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;

import fr.irisa.cairn.gecos.model.utils.misc.ThreadUtils;
import fr.irisa.cairn.gecos.model.utils.misc.ValuesNormalizer;
import fr.irisa.cairn.gecos.typeexploration.Components;
import fr.irisa.cairn.gecos.typeexploration.accuracy.AccuracyEvaluationException;
import fr.irisa.cairn.gecos.typeexploration.cost.CostEvaluationException;
import fr.irisa.cairn.gecos.typeexploration.model.ISolution;
import fr.irisa.cairn.gecos.typeexploration.solutionspace.SolutionSpaceAnalyzer;
import gecos.core.Symbol;
import typeexploration.SolutionSpace;

/**
 * @author aelmouss
 */
public class TabuExploration extends AbstractExplorationAlgorithm {

	protected boolean directionUp = true;
	protected ISolution optimalSol;
	
	@Override
	protected List<String> initializeNotesKeys() {
		List<String> list = new ArrayList<String>();

		list.add("gradient");
		list.add("selected");
		
		return list;
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * Assume:
	 *  - Fixed-point types only
	 *  - Integer width is known and fixed as well as other parameters except W
	 */
	@Override
	public ISolution runExploration(SolutionSpace solSpace) throws NoSolutionFoundException {
		logger.info("      *** Starting Tabu Exploration");
		
		try {
			evaluateMaxWsSolution(solSpace, userAccuracyConstraint);
		} catch (AccuracyEvaluationException | CostEvaluationException e) {
			logger.warning("Skip evalution of Max Ws solution");
		}

		SolutionSpaceAnalyzer analyzer = solSpace.getAnalyzer();
		WordLengthsExplorer explorer = new WordLengthsExplorer(solSpace);
		
		if(directionUp)
			explorer.setAllWToMin();
		else
			explorer.setAllWToMax();
		
		ISolution solution = explorer.createCurrentSolution();
		optimalSol = solution;
		try {
			evaluateAccuracy(solution);
			evaluateCost(solution);
			display(solution);
			log(solution);
		} catch (AccuracyEvaluationException | CostEvaluationException e) {
			//!!! aelmouss:
			//
			// Assuming the code compiles correctly with the selected underlying types library (ac_fixed, ct-Float ..)
			// This could happen if, for instance, the solution has very low precision so it results in underflow 
			// that leads to segmentation faults due to divide by zero for example.
			// So this failure could indicate that the solution is not valid!
			//
			// To recover from this, I let the exploration start and during criterion computation I consider
			// the solution to be the worst in accuracy and in cost.

			logger.warning("Initial solution is not valid: cause :" + e.getMessage());
//			throw new RuntimeException(e);
		}
		
		//Initial solution should be counted as a separate iteration
		nbIterations.incrementAndGet();
		
		List<Symbol> finished = new ArrayList<>();
		
		while(finished.size() < analyzer.getSymbolsWithoutConstraints().size()) {
			logger.fine("Looking for the best next solution...");
			WordLengthsExplorer next = findBestNext(analyzer, explorer, finished);
			if(next == explorer)
				break;
			if(next == null)
				break;
			
			explorer = next;
			solution = explorer.getLastCreatedSolution();
			display(solution);
			logger.fine("Found best next solution: " + solution.getID());
			
			if(directionUp) {
				if(userAccuracyConstraint.isValid(solution)) {
					directionUp = false;
					logger.fine("Reversed direction (going down): current solution is valid; "
							+ "accuracy = " + getAccuracy(solution).get()
							+ "cost = " + getCost(solution).get());
					finished.add(next.getLastModifiedSymbol());
					logger.fine(() -> "Symbol exploration finished; direction up and valid solution: " + next.getLastModifiedSymbol());
				}
			} else {
				if(! userAccuracyConstraint.isValid(solution)) {
					directionUp = true;
					logger.fine("Reversed direction (going up): current solution is no longer valid; "
							+ "accuracy = " + getAccuracy(solution).get()
							+ "cost = " + getCost(solution).get());
				}
			}
			nbIterations.incrementAndGet();
		}
		
		if(!getAccuracy(optimalSol).isPresent() || !getCost(optimalSol).isPresent())
			throw new NoSolutionFoundException("Failed to find a solution likely because the minimun precision "
					+ "configuration of some Symbols is too low, resulting in errors (like divide by zero for example).\n"
					+ "Try rerun after enabling pruning or increasing the minimum W values of some symbols.");
		
		if(!userAccuracyConstraint.isValid(optimalSol))
			throw new NoSolutionFoundException("No valid solution was found!");
		
		logger.info("Finished exploration.");
		display(optimalSol);
		
		return optimalSol;
	}
	
	/** 
	 * Evaluate all next solution candidates and select one with the maximum 
	 * 'accuracy-improvement to cost-degradation' ratio
	 * 
	 * @return the explorer containing the best next solution if any was found, {@code null} otherwise.
	 */
	private WordLengthsExplorer findBestNext(SolutionSpaceAnalyzer analyzer, WordLengthsExplorer currentExplorer, List<Symbol> finished) {
		List<Symbol> newlyFinished = new ArrayList<>();
		List<WordLengthsExplorer> nextSolutionCandidates = new ArrayList<>();
		ThreadUtils.runTaskInForkJoinPool(Components.getInstance().getConfiguration().getNbThreads(), () ->
		analyzer.getSymbolsWithoutConstraints().stream()
			.filter(symbol -> !finished.contains(symbol))
			.parallel()
			.forEach(symbol -> {
				int indexMax = currentExplorer.getMaxWIdx(symbol);
				int currentWIdx = currentExplorer.getCurrentWIdx(symbol);
				if(directionUp && currentWIdx < indexMax) {
					WordLengthsExplorer nextExplorer = currentExplorer.copy();
					boolean changed = nextExplorer.setToNextHigherW(symbol);
					Preconditions.checkState(changed, "The Symbol is already at its maximum W: " + symbol + 
							"! This should never happen, there is probably a bug in the explorer.");
					ISolution nextSolution = nextExplorer.createCurrentSolution();
					try {
						evaluateAccuracy(nextSolution);
						evaluateCost(nextSolution);
						synchronized (nextSolutionCandidates) {
							nextSolutionCandidates.add(nextExplorer);
						}
					} catch (AccuracyEvaluationException | CostEvaluationException e) {
						logger.warning("Skipping next solution (direction up) candidate: " + nextSolution.getID() + " cause: " + e.getMessage());
					}
				}
				else if(!directionUp && currentWIdx > 0) {
					WordLengthsExplorer nextExplorer = currentExplorer.copy();
					boolean changed = nextExplorer.setToNextLowerW(symbol);
					Preconditions.checkState(changed, "The Symbol is already at its minimum W: " + symbol + 
							"! This should never happen, there is probably a bug in the explorer.");
					ISolution nextSolution = nextExplorer.createCurrentSolution();
					try {
						evaluateAccuracy(nextSolution);
						evaluateCost(nextSolution);
						synchronized (nextSolutionCandidates) {
							nextSolutionCandidates.add(nextExplorer);
						}
					} catch (AccuracyEvaluationException | CostEvaluationException e) {
						logger.warning("Skipping next solution (direction down) candidate: " + nextSolution.getID() + " cause: " + e.getMessage());
					}
				}
				else {
					synchronized (finished) {
						logger.fine(() -> "Symbol exploration finished; already at " + (directionUp? "max W & direction is up" : "min W & direction is down") + ": " + symbol);
						newlyFinished.add(symbol);						
					}
				}
			}));
		
		finished.addAll(newlyFinished);
		
		if(finished.size() == analyzer.getSymbolsWithoutConstraints().size()) {
			logger.fine(() -> "All Symbols are finished");
			return currentExplorer;
		}
		
		/* select the next Solution that results in the maximum 'accuracy improvement to cost increase' ratio */
		logger.info("Evaluating next candidates ...");
		Stopwatch w = Stopwatch.createStarted();
		WordLengthsExplorer best = selectBestNext(currentExplorer.getLastCreatedSolution(), nextSolutionCandidates, finished);
		logger.info("Evaluating candiates finished in " + w);
		
		/* Once the next best is found, emit all the logs for this iteration */
		for (WordLengthsExplorer wle : nextSolutionCandidates) {
			log(wle.currentSolution);
		}
		
		return best;
	}
	
	private void checkAndUpdateCurrentOptimal(ISolution newSol) {
		if (!userAccuracyConstraint.isValid(newSol)) return;
			
		if (!userAccuracyConstraint.isValid(optimalSol)) {
			optimalSol = newSol;
			return;
		}
		
		double currentCost = getCost(optimalSol).get();
		double currentAcc = getAccuracy(optimalSol).get();

		double newCost = getCost(newSol).get();
		double newAcc = getAccuracy(newSol).get();
		
		if (newCost < currentCost) optimalSol = newSol;
		if (newCost == currentCost && newAcc > currentAcc) optimalSol = newSol;
	}

	private WordLengthsExplorer selectBestNext(ISolution currentSolution, List<WordLengthsExplorer> nextSolutionCandidates, List<Symbol> finished) {
		if(nextSolutionCandidates.isEmpty()) {
			logger.warning("All next solution candidates have failed to evaluate their accuray or cost!");
			return null;
		}
		
		Collections.sort(nextSolutionCandidates, new Comparator<WordLengthsExplorer>() {
			@Override
			public int compare(WordLengthsExplorer o1, WordLengthsExplorer o2) {
				return o1.getLastModifiedSymbol().getName().compareTo(o2.getLastModifiedSymbol().getName());
			}
			
		});
		
		//update optimal solution
		nextSolutionCandidates.stream().forEachOrdered(s->checkAndUpdateCurrentOptimal(s.currentSolution));
		
		List<Integer> lastModifiedWLs = nextSolutionCandidates.stream()
				.map(e -> e.getCurrentW(e.lastModifiedSymbol))
				.collect(toList());
		
		List<Double> accuracyValues = nextSolutionCandidates.stream()
				.map(e -> getAccuracy(e.getLastCreatedSolution()).get())
				.collect(toList());
		
		List<Double> costValues = nextSolutionCandidates.stream()
				.map(e -> getCost(e.getLastCreatedSolution()).get())
				.collect(toList());
		
		ValuesNormalizer accuracyNormalizer = new ValuesNormalizer(accuracyValues);
		ValuesNormalizer costNormalizer = new ValuesNormalizer(costValues);
		//FIXME these values are currently arbitrary
		{
			//max must be obtained before setting min or range, since it is computed from min and range
			final double accMax = accuracyNormalizer.getMax();
			final double costMax = costNormalizer.getMax();
			accuracyNormalizer.setRange(Math.max(accuracyMetric.getNormalizationRangeLB(), accuracyNormalizer.getRange()));
			accuracyNormalizer.setMin(accMax-accuracyNormalizer.getRange());
			costNormalizer.setRange(Math.max(costMetric.getNormalizationRangeLB(), costNormalizer.getRange()));
			costNormalizer.setMin(costMax-costNormalizer.getRange());
		}

		Double normalizedCurrentAccuray = getAccuracy(currentSolution).map(accuracyNormalizer::normalize).orElse(null);
		Double normalizedCurrentCost = getCost(currentSolution).map(costNormalizer::normalize).orElse(null);
		
		List<Double> normalizedAccuracyValues = accuracyNormalizer.normalize(accuracyValues);
		List<Double> normalizedCostValues = costNormalizer.normalize(costValues);

		List<Double> criteria = new LinkedList<>();
		for (int i = 0; i < nextSolutionCandidates.size(); i++) {
			Integer lastModifiedWL = lastModifiedWLs.get(i);
			Double normalizedAccuracyValue = normalizedAccuracyValues.get(i);
			Double normalizedCostValue = normalizedCostValues.get(i);
			Double ratio = (1 + computeImprovement(normalizedCurrentAccuray, normalizedAccuracyValue, accuracyMetric.isHigherBetter())) /
			(1 + computeDegradation(normalizedCurrentCost, normalizedCostValue, costMetric.isHigherBetter()));
			
			//FIXME bias strength is currently not well thought out
			Double WLbias = Math.pow(lastModifiedWL.doubleValue(), 1.2) / (350); //TODO try 250
			
			final Double criterion;
			
			if (directionUp)
				criterion = ratio - WLbias;
			else
				criterion = ratio + WLbias;
			
			WordLengthsExplorer candidate = nextSolutionCandidates.get(i);
			logger.info(String.format("TabuSearchMetric (%s; WL:%d, Acc:%f, Cost:%f, NormAcc:%f, NormCost:%f): %f -> %f", 
					candidate.getLastModifiedSymbol().getName(), lastModifiedWL.intValue(),
					accuracyValues.get(i), costValues.get(i),
					normalizedAccuracyValue, normalizedCostValue,
					ratio, criterion));
			
			criteria.add(criterion);
			nextSolutionCandidates.get(i).currentSolution.setNote("gradient", criterion+"");
			nextSolutionCandidates.get(i).currentSolution.setNote("selected", "F");
		}
		
		final WordLengthsExplorer nextBest;
		if(directionUp) {
			nextBest = criteria.stream()
					.max(Double::compare)
					.map(max -> nextSolutionCandidates.get(criteria.indexOf(max)))
					.orElse(null);
		} else {
			nextBest = criteria.stream()
					.min(Double::compare)
					.map(min -> nextSolutionCandidates.get(criteria.indexOf(min)))
					.orElse(null);
		}
		nextBest.currentSolution.setNote("selected", "T");
		
		return nextBest;
	}

	/**
	 * @param currentNormalized current value normalized (between 0 and 1). If {@code null}, use 0 if higherIsBetter, 1 otherwise.
	 * @param nextNormalized next value normalized (between 0 and 1)
	 * @param higherIsBetter true indicates that the input values are considered better (i.e. improved) if higher. 
	 * @return positive values between 0 and 2. Higher values => more improvement (values between 0 and 1 signify degradation).
	 */
	private double computeImprovement(Double currentNormalized, double nextNormalized, boolean higherIsBetter) {
		if(currentNormalized == null)
			currentNormalized = higherIsBetter? 0.0 : 1.0;

		double diff;
		if (Double.isNaN(currentNormalized) || Double.isNaN(nextNormalized)) {
			diff = 0;
		} else {
			diff = currentNormalized - nextNormalized;
		}
		if(higherIsBetter)
			diff = -diff;
		
		if (Math.abs(diff) < 0.0000001) {
			diff = 0;
		}
		return diff + 1;
	}
	
	/**
	 * @param currentNormalized current value normalized (between 0 and 1)
	 * @param nextNormalized next value normalized (between 0 and 1)
	 * @param higherIsBetter true indicates that the input values are considered better (i.e. improved) if higher. 
	 * @return positive values between 0 and 2. Higher values => more degradation (values between 0 and 1 signify improvement).
	 */
	private double computeDegradation(Double currentNormalized, double nextNormalized, boolean higherIsBetter) {
		double degradation = 2 - computeImprovement(currentNormalized, nextNormalized, higherIsBetter);
		return degradation;
	}
	
}
