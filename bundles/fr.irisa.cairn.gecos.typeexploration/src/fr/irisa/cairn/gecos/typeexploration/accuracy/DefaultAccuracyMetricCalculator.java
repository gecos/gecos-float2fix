package fr.irisa.cairn.gecos.typeexploration.accuracy;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Map;

import fr.irisa.cairn.gecos.core.profiling.backend.ErrorStats;
import gecos.core.Symbol;

public class DefaultAccuracyMetricCalculator implements IAccuracyMetricCalculator {

	public enum MergeMode {
		/**
		 * Take the mean between error signals 
		 *  <br> e.g. consider the signals with the following stats:
		 *  <pre>
		 *  E1 = {nbElements(N)=3, sum(S)=6, sumSquared(S2)=14} and 
		 *  E2 = {N=2, S=9, S2=41} 
		 *  their mean stats = {N=2.5, S=7.5, S2=27.5}
		 *  </pre>
		 * This is as if computing the error stats of a signal obtained as the mean of all error signals.
		 */
		MEAN("Take the mean between error signals i.e. as if computing the error stats of a signal obtained as the mean of all error signals."),
		/**
		 * Take the Concatenation of error signals
		 * <br> e.g. consider the signals:
		 * <pre>
		 *  E1 = {nbElements(N)=3, sum(S)=6, sumSquared(S2)=14} and 
		 *  E2 = {N=2, S=9, S2=41} 
		 *  their concat stats = {N=5, S=15, S2=55}
		 *  </pre>
		 * This is as if computing the error stats of a signal obtained as the concatenation of all error signals.
		 */
		CONCAT("Take the Concatenation of error signals i.e. as if computing the error stats of a signal obtained as the concatenation of all error signals."),
		/**
		 * TODO
		 */
		WORST("Not yet supported!")
		;
		
		private String description;
		private MergeMode(String description) {
			this.description = description;
		}
		public String getDescription() {
			return description;
		}
	}
	
	private MergeMode symbolSnapshotsMergeMode = MergeMode.MEAN; 	// between the snapshots of a given symbol
	private MergeMode symbolsMergeMode = MergeMode.CONCAT; 			// between the merged error stats of symbols
	
	
	/**
	 * @param symbolSnapshotsMergeMode specify how to merge the error stats between all the snapshots of a given symbol
	 * @param symbolsMergeMode specify how to merge the error stats between the merged stats of all symbols
	 * @see MergeMode#CONCAT
	 * @see MergeMode#MEAN
	 * @see MergeMode#WORST
	 */
	public DefaultAccuracyMetricCalculator(MergeMode symbolSnapshotsMergeMode, MergeMode symbolsMergeMode) {
		this.symbolSnapshotsMergeMode = symbolSnapshotsMergeMode;
		this.symbolsMergeMode = symbolsMergeMode;
	}
	
	/**
	 * Shorthand for this({@link MergeMode#MEAN}, {@link MergeMode#CONCAT})
	 */
	public DefaultAccuracyMetricCalculator() {
		this(MergeMode.MEAN, MergeMode.CONCAT);
	}

	@Override
	public ErrorStats compute(Map<Symbol, List<ErrorStats>> snapshotErrorStatsMap) {
		List<ErrorStats> mergedSnapshots = snapshotErrorStatsMap.values().stream()
			.map(snapshotErrorStats -> merge(snapshotErrorStats, symbolSnapshotsMergeMode))
			.collect(toList());
		
		return merge(mergedSnapshots, symbolsMergeMode);
	}

	private ErrorStats merge(List<ErrorStats> snapshotErrorStats, MergeMode mergeMode) {
		switch (mergeMode) {
		case WORST:
			throw new UnsupportedOperationException("Not yet implemented");
		case CONCAT:
			return ErrorStats.merge(snapshotErrorStats, false);
		case MEAN: default:
			return ErrorStats.merge(snapshotErrorStats, true);
		}
	}

}
