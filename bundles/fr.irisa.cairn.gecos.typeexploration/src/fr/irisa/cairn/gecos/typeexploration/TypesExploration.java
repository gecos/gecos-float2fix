package fr.irisa.cairn.gecos.typeexploration;

import static fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION;
import static fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory.pragma;
import static fr.irisa.cairn.gecos.model.utils.misc.LoggingUtils.anonymousLoggerToFile;
import static fr.irisa.cairn.gecos.typeexploration.GlobalParameters.LOG_EXPLORATION;
import static fr.irisa.cairn.gecos.typeexploration.GlobalParameters.LOG_EXPLORATION_STATS;
import static fr.irisa.cairn.gecos.typeexploration.GlobalParameters.LOG_MAIN;
import static fr.irisa.cairn.gecos.typeexploration.GlobalParameters.ROOT_OUTPUT_FOLDER;
import static java.util.Arrays.asList;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jfree.data.statistics.HistogramType;

import com.google.common.base.Stopwatch;
import com.google.common.base.Throwables;
import com.google.common.primitives.Ints;

import fr.irisa.cairn.gecos.core.profiling.backend.NativeProfilingEngineSupport;
import fr.irisa.cairn.gecos.core.profiling.backend.ProfilingInfo;
import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.c.generator.XtendCGenerator;
import fr.irisa.cairn.gecos.model.cdtfrontend.CDTFrontEnd;
import fr.irisa.cairn.gecos.model.utils.misc.LoggingUtils;
import fr.irisa.cairn.gecos.model.utils.misc.LoggingUtils.PlainFormatter;
import fr.irisa.cairn.gecos.model.utils.misc.PathUtils;
import fr.irisa.cairn.gecos.model.utils.misc.ThreadUtils;
import fr.irisa.cairn.gecos.typeexploration.accuracy.AcFixedCtFloatGenerator;
import fr.irisa.cairn.gecos.typeexploration.accuracy.AccuracyEvaluationException;
import fr.irisa.cairn.gecos.typeexploration.accuracy.DefaultAccuracyMetricCalculator;
import fr.irisa.cairn.gecos.typeexploration.accuracy.ITypeParamGenerator;
import fr.irisa.cairn.gecos.typeexploration.accuracy.SimulationAccuracyEvaluator;
import fr.irisa.cairn.gecos.typeexploration.cost.ComputationModelEvaluator;
import fr.irisa.cairn.gecos.typeexploration.cost.DummyCostEvaluator;
import fr.irisa.cairn.gecos.typeexploration.dse.NoSolutionFoundException;
import fr.irisa.cairn.gecos.typeexploration.model.IAccuracyMetric;
import fr.irisa.cairn.gecos.typeexploration.model.IMetric;
import fr.irisa.cairn.gecos.typeexploration.model.ISolution;
import fr.irisa.cairn.gecos.typeexploration.model.IUserConstraint;
import fr.irisa.cairn.gecos.typeexploration.model.UserFactory;
import fr.irisa.cairn.gecos.typeexploration.solutionspace.SolutionSpaceBuilderFromPragma;
import fr.irisa.cairn.gecos.typeexploration.solutionspace.SolutionSpaceConstraintsProcessor;
import fr.irisa.cairn.gecos.typeexploration.utils.BitHistogramChart;
import fr.irisa.cairn.gecos.typeexploration.utils.HistogramChart;
import fr.irisa.cairn.gecos.typeexploration.utils.ImageViewer;
import fr.irisa.r2d2.gecos.framework.GSArg;
import fr.irisa.r2d2.gecos.framework.GSModule;
import fr.irisa.r2d2.gecos.framework.GSModuleConstructor;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import typeexploration.SolutionSpace;
import typeexploration.TypeConfiguration;


/**
 * @author aelmouss
 */
@GSModule("This module provides a framework for exploring custom fixed-point and/or floating-point types.")
public class TypesExploration {

	protected final GecosProject project;
	protected final Configuration configuration;
	
	protected GecosProject originalProjectCopy;
	

	/**
	 * @deprecated: should be moved to UI handler or wizard
	 */
	@Deprecated
	@GSModuleConstructor(value = "Generate the default configuration file and exit", args = {
		@GSArg(name = "defaultConfigurationLocation", info = "Directory where the default configuration file will be generated at")})
	public TypesExploration(final String defaultConfigurationLocation) {
		project = null;
		configuration = null;
		try {
			Configuration.generateDefaultConfigurationFile(Paths.get(defaultConfigurationLocation));
			System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	@GSModuleConstructor(value = "Run the type exploration flow using the default configuration.", args = {
			@GSArg(name = "project", info = "The gecos project containing the sources on which the exploration will be performed. Can be unparsed.")})
	public TypesExploration(final GecosProject unparsedProject) {
		this(unparsedProject, null);
	}
	
	@GSModuleConstructor(value = "Run the type exploration flow using the specified configuration.", args = {
		@GSArg(name = "project", info = "The gecos project containing the sources on which the exploration will be performed. Can be unparsed."),
		@GSArg(name = "configurationFile", info = "The configuration file to be used. (A default config file can be generated, see other constructors)")})
	public TypesExploration(final GecosProject unparsedProject, final String configurationFile) {
		Objects.requireNonNull(unparsedProject);

		if(configurationFile == null) {
			this.configuration = new Configuration();
		} else {
			try {
				this.configuration = new Configuration(Paths.get(configurationFile));
			} catch (IOException e) {
				throw new RuntimeException("Failed to load configuration file: " + configurationFile, e);
			}
		}
		
		this.project = unparsedProject;
		if(!project.isParsed())
			parseProject();
		this.originalProjectCopy = project.copy();
	}
	
	protected void parseProject() {
		CDTFrontEnd parser = new CDTFrontEnd(project);
		CDTFrontEnd.annotateFileLocations(true); //XXX this should not be static!
		parser.compute();
	}

	public void compute() {
		/** Initialize Component */
		Components.setCompoments(createComponents());
		
		Logger logger = Components.getInstance().getMainLogger();
		
		anonymousLoggerToFile(Components.getInstance().getOutputLoactions().getLogsDir().resolve("configuration.rpt"), true).info(configuration.toString());
		
		
		/** Building the solution space */
		logger.info("                - Building Solution Space");
		SolutionSpace solutionSpace = createSolutionSpace();
		
		
		/** Evaluate the reference (original) implementation */
		if(Components.getInstance().getAccuEvaluator() instanceof SimulationAccuracyEvaluator) {
			SimulationAccuracyEvaluator accEval = (SimulationAccuracyEvaluator)Components.getInstance().getAccuEvaluator();
			try {
				accEval.initialize();
				
				//XXX temporary for demo only
				Map<Symbol, ProfilingInfo<? extends Number>> referenceProfilingData = accEval.getReferenceProfilingInfo();
				display(referenceProfilingData); 
			} catch(AccuracyEvaluationException e) {
				logger.severe("Failed to evaluate reference (original) solution! \n" + Throwables.getStackTraceAsString(e));
				System.exit(-1);
			}
		}
		
		waitInput("Press enter to start the exploration..");
		

		/** Solution space Exploration */
		logger.info("                - Start types exploration");
		Stopwatch w = Stopwatch.createStarted();
		try {
			ISolution solution = Components.getInstance().getExplorationAlgorithm().explore(solutionSpace, configuration.getExplorationFlags());
			logger.info("                - Exploration finished in " + w);
			
			generateSolutionCode(logger, solution);
			
		} catch (NoSolutionFoundException e) {
			logger.severe("No solution was found! \n" + Throwables.getStackTraceAsString(e));
//			System.exit(-1);
		} catch (IOException e) {
			logger.severe("Failed to write solution! \n" + Throwables.getStackTraceAsString(e));
		}

		
		waitInput("Press enter to exit..");
	}
	
	protected static IUserConstraint makeConstraint(IAccuracyMetric metric, Double threshold) {
		return metric.isHigherBetter() ?
				sol -> metric.getMetric(sol).get() >= threshold :
				sol -> metric.getMetric(sol).get() <= threshold ;
	}
	
	protected Components createComponents() {
		List<TypeConfiguration> defaultTypeConfigs = asList(
				UserFactory.defaultFixedConfiguration(Ints.asList(configuration.getDefaultFixedW()), Ints.asList(configuration.getDefaultFixedI()), Ints.asList(configuration.getDefaultSigned())),
				UserFactory.defaultFloatConfiguration(Ints.asList(configuration.getDefaultFloatW()), Ints.asList(configuration.getDefaultFloatE()), Ints.asList(configuration.getDefaultSigned())));

		//!! more complex constraints can be used as composition of other constraints
		// You only need to provide a convenient way for the user to specify them..
		// For now we only use Upper or Lower bound on the selected accuracy metric 
		IUserConstraint accuracyConstraint = IUserConstraint.and(configuration.getAccuracyMetrics().entrySet().stream()
				.map(e -> makeConstraint(e.getKey(), e.getValue()))
				.toArray(IUserConstraint[]::new));
				
		
		Components comp = new Components();
		
		comp.configuration = configuration;
		if (configuration.getCostModelFilepath() == null || configuration.getCostModelFilepath().length() == 0) {
			comp.computationModel = null;
		} else {
			try {
				comp.computationModel = ComputationModelIO.loadModel(configuration.getCostModelFilepath());
			} catch (IOException e) {
				throw new RuntimeException("Failed to load computation model file: " + configuration.getCostModelFilepath(), e);
			}
		}
		
		comp.outputLoc = new OutputLocations(ROOT_OUTPUT_FOLDER.resolve(project.getName()), 
				configuration.isTimeTagOutputDir(), configuration.getNbTaggedOutputsTokeep());
		
		Logger logger = Logger.getLogger(LOG_MAIN);
		comp.mainLogger = LoggingUtils.useStdout(logger, configuration.getMainLoggerLevel());

		comp.expLogger = LoggingUtils.loggerToFile(Logger.getLogger(LOG_EXPLORATION), configuration.getExploarationLoggerLevel(),  
				comp.outputLoc.getLogsDir().resolve(LOG_EXPLORATION+".log"), true);

		comp.expStatsLogger = LoggingUtils.loggerToFile(Logger.getLogger(LOG_EXPLORATION_STATS), Level.ALL,  
				comp.outputLoc.getLogsDir().resolve(LOG_EXPLORATION_STATS+".log"), false, new PlainFormatter());
		
		comp.solutionSpaceBuilder = new SolutionSpaceBuilderFromPragma(defaultTypeConfigs);
		
		comp.accuracyConstraint = accuracyConstraint;
		//TODO comp.costConstraint = 
		
		comp.explorationMode = configuration.getExplorationMode();
		
		//TODO should normalize (or scale) metric values before summing
		comp.accuracyMetric = IMetric.mergeMetrics(configuration.getAccuracyMetrics().keySet());
		comp.costMetric = IMetric.mergeMetrics(configuration.getCostMetric());
		
		comp.explorationAlgorithm = configuration.getAlgo();
		
		if (comp.computationModel != null) {
			comp.costEvaluator = new ComputationModelEvaluator(comp.computationModel, configuration.getCostModelFlags());
		} else {
			comp.costEvaluator = new DummyCostEvaluator();
		}

		comp.typesGenerator = new AcFixedCtFloatGenerator(); 
		//comp.typesGenerator = new AcDatatypesGenerator();
		
		comp.accuracyMetricCalculator = new DefaultAccuracyMetricCalculator(configuration.getSnapshotsErrorStatsMergeMode(), 
				configuration.getSymbolsErrorStatsMergeMode());
		
		comp.accuracyEvaluator = new SimulationAccuracyEvaluator(project); 
		

		//Reflect compiler options to profiling engine
		NativeProfilingEngineSupport.CC = comp.configuration.getCC();
		NativeProfilingEngineSupport.EXTRA_CFLAGS = comp.configuration.getExtraCFlags();
		
		return comp;
	}

	protected SolutionSpace createSolutionSpace() {
		Path logsDir = Components.getInstance().getOutputLoactions().getLogsDir();
		
		SolutionSpace solutionSpace = Components.getInstance().getSolutionSpaceBuilder().build(project);
		ThreadUtils.runInNewThread(() -> anonymousLoggerToFile(logsDir.resolve("solution-space-raw.rpt")).info(solutionSpace.toString()));
	
		new SolutionSpaceConstraintsProcessor(solutionSpace).compute();
		
		ThreadUtils.runInNewThread(() -> anonymousLoggerToFile(logsDir.resolve("solution-space-final.rpt")).info(solutionSpace.toString()));
		
		return solutionSpace;
	}
	
	//!!! this only works with SimulationAccuracyEvaluator as accuracyEvaluator component
	protected void generateSolutionCode(Logger logger, ISolution solution) throws IOException {
		ITypeParamGenerator generator = Components.getInstance().getTypesGenerator();
		Function<Symbol, String> typeNameGetter = symbol -> new TypeAnalyzer(symbol.getType()).getBaseLevel().getAlias().getName(); //XXX assumes symbol types have been converted to AliasTypes
		List<String> lines = new ArrayList<>();
		
		lines.add("#ifndef __TYPE_DEFS__");
		lines.add("#define __TYPE_DEFS__");
		lines.add("");
		solution.getSymbols().stream()
			.map(s -> "#define " + String.format("%-30s", typeNameGetter.apply(s)) + "  " + generator.generate(solution.getType(s)))
			.forEach(lines::add);
		lines.add("");
		lines.add("#endif //__TYPE_DEFS__");
		
		Path solutionDir = PathUtils.createDir(Components.getInstance().getOutputLoactions().getExplorationDir(), "solution", true);
		String typedefHeaderName = "typedefs.h";
		logger.info("                - Generating Solution code in: " + solutionDir);
		Files.write(solutionDir.resolve(typedefHeaderName), lines);
		
		//XXX hack
		if(Components.getInstance().getAccuEvaluator() instanceof SimulationAccuracyEvaluator) {
			GecosProject originalNoDirectives = ((SimulationAccuracyEvaluator)Components.getInstance().getAccuEvaluator()).getProjectCopyWithoutDirectives().copy();
			originalNoDirectives.listProcedureSets()
				.forEach(ps -> pragma(ps, CODEGEN_PRINT_ANNOTATION + "#include \"" + typedefHeaderName + "\""));
			
			//TODO need to add header of types libraries being used
			
			new XtendCGenerator(originalNoDirectives, solutionDir.toString()).compute();
		}
	}
	
	
	//XXX these are temporary: for demo only
	public static final boolean SHOW_HISTOGRAMS = false;
	public static final boolean SHOW_IMAGES = false;
	public static final boolean SHOW_EXPLORAION_HISTOGRAMS = false;
	public static final boolean SHOW_EXPLORAION_IMAGES = false;
	
	private void display(Map<Symbol, ProfilingInfo<? extends Number>> referenceProfilingData) {
		Path expDir = Components.getInstance().getOutputLoactions().getExplorationDir();
		
		if(SHOW_HISTOGRAMS) {
			double[] refValues = referenceProfilingData.values().iterator().next().getAllSnapshotValues().mapToDouble(Number::doubleValue).toArray();
			
			try {
				new HistogramChart("Histogram", "reference output", "value", "relative frequency", HistogramType.RELATIVE_FREQUENCY, refValues, refValues.length);
			
				BitHistogramChart bitHisto = BitHistogramChart.ofDoubles("Reference Bit Histogram", "reference values", HistogramType.FREQUENCY, refValues);
				bitHisto.saveAsJpeg(expDir, "bitHist");
				
				BitHistogramChart.ofFixed("Reference Bit Histogram Fixed", "reference values", HistogramType.RELATIVE_FREQUENCY, refValues, 64, 30);
				BitHistogramChart.ofFixed("Reference Bit Histogram Fixed", "reference values", HistogramType.FREQUENCY, refValues, 32, 16);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if(SHOW_IMAGES) {
			ImageViewer imgViewer = new ImageViewer("Image", referenceProfilingData.values().iterator().next().getSnapshotAsImage(0, 128, 128, 255));
			imgViewer.saveAsJpeg(expDir, "ref-output-image");
		}
	}

	private void waitInput(String msg) {
		try {
			System.out.println(msg);
			System.out.flush();
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
