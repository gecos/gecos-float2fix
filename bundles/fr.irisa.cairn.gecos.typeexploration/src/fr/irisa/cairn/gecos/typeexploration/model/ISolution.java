package fr.irisa.cairn.gecos.typeexploration.model;

import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.irisa.cairn.gecos.core.profiling.backend.ErrorStats;
import gecos.core.Symbol;
import typeexploration.SolutionSpace;
import typeexploration.TypeParam;

public interface ISolution {
	
	int getID();
	void setID(int value);
	
	SolutionSpace getSolutionSpace();
	List<Symbol> getSymbols();
	
	void setType(Symbol s, TypeParam t);
	TypeParam getType(Symbol s);
	
	/**
	 * Note is an arbitrary piece of String that is emitted in the exploration log.
	 * It is to be used by exploration algorithms to keep track of important pieces
	 * of info specific to the algorithm.
	 * 
	 * @param note
	 */
	void setNote(String key, String note);
	String getNote(String key);
	Map<String, String> getNotes();
	
	
	void addCost(String costMetricName, String componentName, Double cost);
	void removeCost(String costMetricName, String componentName);
	Set<String> getCostComponentNames(String costMetricName);
	
	/**
	 * @param costMetricName
	 * @param componentName
	 * @return the cost with the specified cost metric name, or {@code null} if no such metric was added.
	 */
	Double getCost(String costMetricName, String componentName);
	
	
	
	void addAccuracy(String accuracyMetricName, Double accuracy);
	void removeAccuracy(String accuracyMetricName);
	Double getAccuracy(String accuracyMetricName);

	
	//XXX These should be optional, since an AccuracyEvaluator may not be able
	// the compute Error stats per Symbol but just an aggregation for example
	Map<Symbol, List<ErrorStats>> getAllErrorStats();
	void setErrorStats(Symbol s, List<ErrorStats> stats);
	
	String metricDump();

}