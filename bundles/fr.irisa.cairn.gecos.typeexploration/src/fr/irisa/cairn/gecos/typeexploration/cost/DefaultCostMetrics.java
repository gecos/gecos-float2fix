package fr.irisa.cairn.gecos.typeexploration.cost;

import java.util.function.Function;

import fr.irisa.cairn.gecos.typeexploration.model.ICostEvaluator;
import fr.irisa.cairn.gecos.typeexploration.model.ICostMetric;
import fr.irisa.cairn.gecos.typeexploration.model.ISolution;

public enum DefaultCostMetrics {

	SUM_W("The sum of wordlengths of all Symbols", false, s -> s.getCost(DummyCostEvaluator.COST_METRIC_NAME, ICostEvaluator.FULL_APPLICATION), 20),
	AREA_MODEL("Simple area cost estimation based on a model of computation", false, s -> s.getCost(ComputationModelEvaluator.AREA_MODEL, ICostEvaluator.FULL_APPLICATION), 500),
	ENERGY_MODEL("Simple energy estimation based on a model of computation", false, s -> s.getCost(ComputationModelEvaluator.ENERGY_MODEL, ICostEvaluator.FULL_APPLICATION), 50),
	;
	
	private ICostMetric metric;
	
	private DefaultCostMetrics(String description, boolean higherIsBetter, Function<ISolution, Double> costMetric, double normalizationRangeLB) {
		metric = ICostMetric.createMetric(description, higherIsBetter, costMetric, normalizationRangeLB);
	}
	
	public ICostMetric getMetric() {
		return metric;
	}
	
	public String describe() {
		return name() + ": " + metric.getDescription() + ". Higher values are " + (metric.isHigherBetter()? "less":"more") + " costly";
	}
}