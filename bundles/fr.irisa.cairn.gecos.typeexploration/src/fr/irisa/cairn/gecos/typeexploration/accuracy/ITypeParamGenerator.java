package fr.irisa.cairn.gecos.typeexploration.accuracy;

import java.nio.file.Path;
import java.util.List;

import typeexploration.TypeParam;

public interface ITypeParamGenerator {

	String generate(TypeParam t);
	
	List<Path> getIncDirs(boolean useFixed, boolean useFloat);
	
	List<String> getHeaders(boolean useFixed, boolean useFloat);

	List<String> getLibs(boolean useFixed, boolean useFloat);
	
	List<Path> getLibDirs(boolean useFixed, boolean useFloat);
}
