# User guide


## C Souce code Preparation

* C++ code is not supported

* If your C project has no main function defined and AND it defines more than 1 function THEN the top-level function MUST 
be annotated with `pragma MAIN_FUNC`, like:
  ```
  #pragma MAIN_FUNC
  void top_level_func(double x) {
    ...
  ```

* The output symbols MUST be saved at the error observation point (usually at the end) using the `$save` directive:
  ```
  $save(<symbol> [, <size_outermost_dim>, ..., <size_innermost_dim>]);
  ```
  - If the symbol is not a scalar or array, its dimension sizes MUST be specified in the `$save` directive.
  Alternatively, you can use `$size` directive prior to $save:
    ```
    $size(<symbol>, <size_outermost_dim>, ..., <size_innermost_dim>);
    ...
    $save(<symbol>);
    ```

* If no main procedure is available, one will be generated automatically, but in order to initialize the values of inputs, 
you need to inject their values using the `$inject` directive:
  ```
  $inject(<symbol>, SOURCE, [, <size_outermost_dim>, ..., <size_innermost_dim>]);
  ```
  - If the symbol is not a scallar or array, its dimension sizes MUST be specified as additional arguments or using
  `$size` (same as `$save`).
  - `SOURCE` can be either:
      1. $random_uniform(min, max [, seed])
      2. $random_normal(mean, stddev [, seed])
      3. $from_var(`<any expression that evaluates as double>`)
      4. $from_file(`<file_path>`)
        Currently, only support txt file in the following format:
         ```
         ND_DIMS
         SIZE DIM 0
         ...
         SIZE DIM ND_DIMS-1
         first value
         second value
         ...
         ```

NOTE: `$inject` directive can be used anywhere even if a main procedure is defined, it will simply override the symbol values at the location it's placed at.


* Your code should not define any function named as any of the directives (`$save`, `$inject` or `$size`)!

* Do NOT use operations between a floating-point symbol and a immediate constant.
  This may cause compilation errors when we later change the type of the symbol
  More generally, keep in mind the fact that the floating-point symbols will be compiled with different types during the    
  exploration. With that in mind, avoid operations that might cause conflicts or ambiguity depending of the backend Types 
  library you select in the tool. Currently the tool can only use *AC_DataTypes*, but this could be easily extended to 
  other libraries.
  For e.g:
  ```
  double x;
  .. = x * 0.5;
  ```
  During the exploration the type of x might be changed to `ac_fixed<..>`, this will result in a compilation error 
  (_ambiguous overload for ‘operator*’_) since the ac_fixed library does not define such behavior.

TIP: To avoid this problem, extract the constant into a separate variable.

* Do NOT use type operations using explicit types, for example:

  ```
  #define TYPE double
  ...
  TYPE *p = (TYPE *)malloc(sizeof(TYPE)*4);
  ```
  
  In this case, the cast and the sizeof operations are not allowed, since they won't change when we explore 
  different types for symbol `p`, thus resulting in compilation errors!

  To avoid this problem, use typedef instead.
  However, one typedef can currently be used by one Symbol only!
  So you can not use the same typedef as type for 2 different symbols, like for 
  For example:
  ```
  typedef double TYPE;
  ...
  TYPE *p = (TYPE *)malloc(sizeof(TYPE)*4); // OK
  TYPE x; // NOT SUPPORTED !
  ```


## Guiding the Exploration

* Annotate a symbol with `pragma EXPLORE_FIX` to define the set of fixed-point configurations to be explored for that symbol:
  ```
  #pragma EXPLORE_FIX W={SET_VALUES} I={SET_VALUES}
  float symbol;
  ```
  `SET_VALUES := (<min>..<max> | <value>)[, (<min>..<max> | <value>)]+`

* Annotate a symbol with `pragma EXPLORE_FLOAT` to define the set of custom floating-point configurations to be explored 
for that symbol:
  ```
  #pragma EXPLORE_FLOAT W={SET_VALUES} E={SET_VALUES}
  float symbol;
  ```
  `SET_VALUES := (<min>..<max> | <value>)[, (<min>..<max> | <value>)]+`

* A symbol can have multiple `pragma EXPLORE` annotations

* A default configuration is used for symbols with no #pragma EXPLORE
  This default configuration can be set in the configuration file.

* Annotate a symbol with `pragma EXPLORE_CONSTRAINT SAME = <other_symbol>` to indicate 
  that it should always have the same type as `<other_symbol>`. For example:
  ```
  #pragma EXPLORE_FIX W={8..12, 16} I={4}
  float x; // explored widths = {8,9,10,11,12,16}, Integer part = {4}
  #pragma EXPLORE_CONSTRAINT SAME = x
  float y; // type of y will mirror that of x
  ```

WARNING: avoid creating cycle with `SAME` constraints. Thought if you do, the tool
will automatically detect them.

* 'SAME' constraints are automatically added for functions parameters (only the functions defined in your C project), 
  as   following:

  - A function parameter is constrained to have the same type as the corresponding argument in each call to the procedure.
  
  - In this case, if the function parameter is annotated by `pragma EXPLORE`, its
  type configurations are moved (added) to the corresponding call argument.
  
  - In case of multiple calls to the same function, multiple SAME constraints are added to the parameter.
    In this case, all the corresponding calls arguments as well as the function parameter 
    are constrained to be the SAME as only one of them. This one also merge in the type configurations of all the others.
    
   - A log file reporting all such operations is generated at `<output_dir>/logs/solution-space-analysis.log` 


## Configuration File

Run a compiler script (.cs) with the `TypesExploration` module  to generate the default configuration file.
The generated file is well documented.
For example, the following script will generate the default configuration file `default.properties` 
in the script's directory.  

```
TypesExploration(".");
```

## Diagnostics

* When you run the tool, you get an error on the console saying something like "cannot find library lprofilingengine" :

  - If you check the console (assuming the logging level is at least `INFO`), this error will appear right after invoking
  a shell command to compile the instrumented code with g++.

  - In this g++ command (on the console) check the -L arguments, you will find the location where the missing `profilingengine` 
  library should be located at (the path should end with something like "resources/ProfilingEngine_double/bin"). Let's call this directory
  `LIB_DIR`.
  
  - Now open a terminal and go to `LIB_DIR`/.. (the parent directory), in their you should find a `build` directory, go to it.
  In this `build` directory run the `cmake_release.sh` shell script (this requires `cmake` to be installed!!), it will generate a Makefile.
  Now simply run `make` while still in the build directory. This should compile and generate the `profilingengine` library.
  
  - Re-run the tool, the error should now disappear.

  
  
















