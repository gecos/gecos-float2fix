/**
 */
package typeexploration;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see typeexploration.TypeexplorationPackage#getTypeConfiguration()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface TypeConfiguration extends EObject {
} // TypeConfiguration
