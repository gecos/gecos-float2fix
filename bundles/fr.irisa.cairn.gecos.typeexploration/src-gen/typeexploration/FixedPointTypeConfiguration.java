/**
 */
package typeexploration;

import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fixed Point Type Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.FixedPointTypeConfiguration#getIntegerWidthValues <em>Integer Width Values</em>}</li>
 *   <li>{@link typeexploration.FixedPointTypeConfiguration#getQuantificationMode <em>Quantification Mode</em>}</li>
 *   <li>{@link typeexploration.FixedPointTypeConfiguration#getOverflowMode <em>Overflow Mode</em>}</li>
 * </ul>
 *
 * @see typeexploration.TypeexplorationPackage#getFixedPointTypeConfiguration()
 * @model
 * @generated
 */
public interface FixedPointTypeConfiguration extends NumberTypeConfiguration {
	/**
	 * Returns the value of the '<em><b>Integer Width Values</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Width Values</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Width Values</em>' attribute list.
	 * @see typeexploration.TypeexplorationPackage#getFixedPointTypeConfiguration_IntegerWidthValues()
	 * @model
	 * @generated
	 */
	EList<Integer> getIntegerWidthValues();

	/**
	 * Returns the value of the '<em><b>Quantification Mode</b></em>' attribute list.
	 * The list contents are of type {@link gecos.types.QuantificationMode}.
	 * The literals are from the enumeration {@link gecos.types.QuantificationMode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantification Mode</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantification Mode</em>' attribute list.
	 * @see gecos.types.QuantificationMode
	 * @see typeexploration.TypeexplorationPackage#getFixedPointTypeConfiguration_QuantificationMode()
	 * @model
	 * @generated
	 */
	EList<QuantificationMode> getQuantificationMode();

	/**
	 * Returns the value of the '<em><b>Overflow Mode</b></em>' attribute list.
	 * The list contents are of type {@link gecos.types.OverflowMode}.
	 * The literals are from the enumeration {@link gecos.types.OverflowMode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overflow Mode</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overflow Mode</em>' attribute list.
	 * @see gecos.types.OverflowMode
	 * @see typeexploration.TypeexplorationPackage#getFixedPointTypeConfiguration_OverflowMode()
	 * @model
	 * @generated
	 */
	EList<OverflowMode> getOverflowMode();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Add the values specified by c to this.
	 * @return this
	 * <!-- end-model-doc -->
	 * @model unique="false" cUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='super.mergeIn(c);\nthis.getIntegerWidthValues().addAll(c.getIntegerWidthValues());\nthis.getQuantificationMode().addAll(c.getQuantificationMode());\nthis.getOverflowMode().addAll(c.getOverflowMode());\nreturn this;'"
	 * @generated
	 */
	FixedPointTypeConfiguration mergeIn(FixedPointTypeConfiguration c);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%java.lang.Boolean%&gt;&gt; _signed = this.getSigned();\n&lt;%java.lang.String%&gt; _plus = (\"Fixed-point: S=\" + _signed);\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \" W=\");\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%java.lang.Integer%&gt;&gt; _totalWidthValues = this.getTotalWidthValues();\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + _totalWidthValues);\n&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + \" I=\");\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%java.lang.Integer%&gt;&gt; _integerWidthValues = this.getIntegerWidthValues();\nreturn (_plus_3 + _integerWidthValues);'"
	 * @generated
	 */
	String toString();

} // FixedPointTypeConfiguration
