/**
 */
package typeexploration;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see typeexploration.TypeexplorationPackage
 * @generated
 */
public interface TypeexplorationFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TypeexplorationFactory eINSTANCE = typeexploration.impl.TypeexplorationFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Fixed Point Type Param</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fixed Point Type Param</em>'.
	 * @generated
	 */
	FixedPointTypeParam createFixedPointTypeParam();

	/**
	 * Returns a new object of class '<em>Float Point Type Param</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Float Point Type Param</em>'.
	 * @generated
	 */
	FloatPointTypeParam createFloatPointTypeParam();

	/**
	 * Returns a new object of class '<em>Generic Type Param</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Generic Type Param</em>'.
	 * @generated
	 */
	GenericTypeParam createGenericTypeParam();

	/**
	 * Returns a new object of class '<em>Same Type Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Same Type Constraint</em>'.
	 * @generated
	 */
	SameTypeConstraint createSameTypeConstraint();

	/**
	 * Returns a new object of class '<em>Fixed Point Type Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fixed Point Type Configuration</em>'.
	 * @generated
	 */
	FixedPointTypeConfiguration createFixedPointTypeConfiguration();

	/**
	 * Returns a new object of class '<em>Float Point Type Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Float Point Type Configuration</em>'.
	 * @generated
	 */
	FloatPointTypeConfiguration createFloatPointTypeConfiguration();

	/**
	 * Returns a new object of class '<em>Type Configuration Space</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Type Configuration Space</em>'.
	 * @generated
	 */
	TypeConfigurationSpace createTypeConfigurationSpace();

	/**
	 * Returns a new object of class '<em>Solution Space</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Solution Space</em>'.
	 * @generated
	 */
	SolutionSpace createSolutionSpace();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TypeexplorationPackage getTypeexplorationPackage();

} //TypeexplorationFactory
