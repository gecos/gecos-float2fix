/**
 */
package typeexploration.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import typeexploration.TypeConstraint;
import typeexploration.TypeexplorationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class TypeConstraintImpl extends MinimalEObjectImpl.Container implements TypeConstraint {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TypeConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypeexplorationPackage.Literals.TYPE_CONSTRAINT;
	}

} //TypeConstraintImpl
