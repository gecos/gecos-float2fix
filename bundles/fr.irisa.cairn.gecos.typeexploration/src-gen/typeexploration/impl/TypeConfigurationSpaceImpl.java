/**
 */
package typeexploration.impl;

import java.lang.CharSequence;

import java.security.InvalidParameterException;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

import typeexploration.FixedPointTypeConfiguration;
import typeexploration.FloatPointTypeConfiguration;
import typeexploration.TypeConfiguration;
import typeexploration.TypeConfigurationSpace;
import typeexploration.TypeConstraint;
import typeexploration.TypeexplorationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type Configuration Space</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.impl.TypeConfigurationSpaceImpl#getFixedPtCongurations <em>Fixed Pt Congurations</em>}</li>
 *   <li>{@link typeexploration.impl.TypeConfigurationSpaceImpl#getFloatPtCongurations <em>Float Pt Congurations</em>}</li>
 *   <li>{@link typeexploration.impl.TypeConfigurationSpaceImpl#getConstraints <em>Constraints</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TypeConfigurationSpaceImpl extends MinimalEObjectImpl.Container implements TypeConfigurationSpace {
	/**
	 * The cached value of the '{@link #getFixedPtCongurations() <em>Fixed Pt Congurations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFixedPtCongurations()
	 * @generated
	 * @ordered
	 */
	protected FixedPointTypeConfiguration fixedPtCongurations;

	/**
	 * The cached value of the '{@link #getFloatPtCongurations() <em>Float Pt Congurations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFloatPtCongurations()
	 * @generated
	 * @ordered
	 */
	protected FloatPointTypeConfiguration floatPtCongurations;

	/**
	 * The cached value of the '{@link #getConstraints() <em>Constraints</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraints()
	 * @generated
	 * @ordered
	 */
	protected EList<TypeConstraint> constraints;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TypeConfigurationSpaceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypeexplorationPackage.Literals.TYPE_CONFIGURATION_SPACE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixedPointTypeConfiguration getFixedPtCongurations() {
		return fixedPtCongurations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFixedPtCongurations(FixedPointTypeConfiguration newFixedPtCongurations, NotificationChain msgs) {
		FixedPointTypeConfiguration oldFixedPtCongurations = fixedPtCongurations;
		fixedPtCongurations = newFixedPtCongurations;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__FIXED_PT_CONGURATIONS, oldFixedPtCongurations, newFixedPtCongurations);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFixedPtCongurations(FixedPointTypeConfiguration newFixedPtCongurations) {
		if (newFixedPtCongurations != fixedPtCongurations) {
			NotificationChain msgs = null;
			if (fixedPtCongurations != null)
				msgs = ((InternalEObject)fixedPtCongurations).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__FIXED_PT_CONGURATIONS, null, msgs);
			if (newFixedPtCongurations != null)
				msgs = ((InternalEObject)newFixedPtCongurations).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__FIXED_PT_CONGURATIONS, null, msgs);
			msgs = basicSetFixedPtCongurations(newFixedPtCongurations, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__FIXED_PT_CONGURATIONS, newFixedPtCongurations, newFixedPtCongurations));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloatPointTypeConfiguration getFloatPtCongurations() {
		return floatPtCongurations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFloatPtCongurations(FloatPointTypeConfiguration newFloatPtCongurations, NotificationChain msgs) {
		FloatPointTypeConfiguration oldFloatPtCongurations = floatPtCongurations;
		floatPtCongurations = newFloatPtCongurations;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__FLOAT_PT_CONGURATIONS, oldFloatPtCongurations, newFloatPtCongurations);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFloatPtCongurations(FloatPointTypeConfiguration newFloatPtCongurations) {
		if (newFloatPtCongurations != floatPtCongurations) {
			NotificationChain msgs = null;
			if (floatPtCongurations != null)
				msgs = ((InternalEObject)floatPtCongurations).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__FLOAT_PT_CONGURATIONS, null, msgs);
			if (newFloatPtCongurations != null)
				msgs = ((InternalEObject)newFloatPtCongurations).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__FLOAT_PT_CONGURATIONS, null, msgs);
			msgs = basicSetFloatPtCongurations(newFloatPtCongurations, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__FLOAT_PT_CONGURATIONS, newFloatPtCongurations, newFloatPtCongurations));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TypeConstraint> getConstraints() {
		if (constraints == null) {
			constraints = new EObjectContainmentEList<TypeConstraint>(TypeConstraint.class, this, TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__CONSTRAINTS);
		}
		return constraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TypeConfiguration> listConfigurations() {
		final BasicEList<TypeConfiguration> list = new BasicEList<TypeConfiguration>();
		FixedPointTypeConfiguration _fixedPtCongurations = this.getFixedPtCongurations();
		boolean _tripleNotEquals = (_fixedPtCongurations != null);
		if (_tripleNotEquals) {
			list.add(this.getFixedPtCongurations());
		}
		FloatPointTypeConfiguration _floatPtCongurations = this.getFloatPtCongurations();
		boolean _tripleNotEquals_1 = (_floatPtCongurations != null);
		if (_tripleNotEquals_1) {
			list.add(this.getFloatPtCongurations());
		}
		return ECollections.<TypeConfiguration>unmodifiableEList(list);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addConfiguration(final TypeConfiguration c) {
		if ((c instanceof FixedPointTypeConfiguration)) {
			FixedPointTypeConfiguration _fixedPtCongurations = this.getFixedPtCongurations();
			boolean _tripleEquals = (_fixedPtCongurations == null);
			if (_tripleEquals) {
				this.setFixedPtCongurations(((FixedPointTypeConfiguration)c));
			}
			else {
				this.getFixedPtCongurations().mergeIn(((FixedPointTypeConfiguration)c));
			}
		}
		else {
			if ((c instanceof FloatPointTypeConfiguration)) {
				FloatPointTypeConfiguration _floatPtCongurations = this.getFloatPtCongurations();
				boolean _tripleEquals_1 = (_floatPtCongurations == null);
				if (_tripleEquals_1) {
					this.setFloatPtCongurations(((FloatPointTypeConfiguration)c));
				}
				else {
					this.getFloatPtCongurations().mergeIn(((FloatPointTypeConfiguration)c));
				}
			}
			else {
				throw new InvalidParameterException(("Unknown parameter type: " + c));
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		final Function1<TypeConfiguration, CharSequence> _function = new Function1<TypeConfiguration, CharSequence>() {
			public CharSequence apply(final TypeConfiguration it) {
				String _string = it.toString();
				return ("\t\t" + _string);
			}
		};
		String _join = IterableExtensions.<TypeConfiguration>join(this.listConfigurations(), "\n", _function);
		final Function1<TypeConstraint, CharSequence> _function_1 = new Function1<TypeConstraint, CharSequence>() {
			public CharSequence apply(final TypeConstraint it) {
				String _string = it.toString();
				return ("\t\t" + _string);
			}
		};
		String _join_1 = IterableExtensions.<TypeConstraint>join(this.getConstraints(), "\n", _function_1);
		return (_join + _join_1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__FIXED_PT_CONGURATIONS:
				return basicSetFixedPtCongurations(null, msgs);
			case TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__FLOAT_PT_CONGURATIONS:
				return basicSetFloatPtCongurations(null, msgs);
			case TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__CONSTRAINTS:
				return ((InternalEList<?>)getConstraints()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__FIXED_PT_CONGURATIONS:
				return getFixedPtCongurations();
			case TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__FLOAT_PT_CONGURATIONS:
				return getFloatPtCongurations();
			case TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__CONSTRAINTS:
				return getConstraints();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__FIXED_PT_CONGURATIONS:
				setFixedPtCongurations((FixedPointTypeConfiguration)newValue);
				return;
			case TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__FLOAT_PT_CONGURATIONS:
				setFloatPtCongurations((FloatPointTypeConfiguration)newValue);
				return;
			case TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__CONSTRAINTS:
				getConstraints().clear();
				getConstraints().addAll((Collection<? extends TypeConstraint>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__FIXED_PT_CONGURATIONS:
				setFixedPtCongurations((FixedPointTypeConfiguration)null);
				return;
			case TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__FLOAT_PT_CONGURATIONS:
				setFloatPtCongurations((FloatPointTypeConfiguration)null);
				return;
			case TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__CONSTRAINTS:
				getConstraints().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__FIXED_PT_CONGURATIONS:
				return fixedPtCongurations != null;
			case TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__FLOAT_PT_CONGURATIONS:
				return floatPtCongurations != null;
			case TypeexplorationPackage.TYPE_CONFIGURATION_SPACE__CONSTRAINTS:
				return constraints != null && !constraints.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TypeConfigurationSpaceImpl
