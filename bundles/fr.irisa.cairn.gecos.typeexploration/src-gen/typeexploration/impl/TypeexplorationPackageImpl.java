/**
 */
package typeexploration.impl;

import fr.irisa.cairn.gecos.typeexploration.solutionspace.SolutionSpaceAnalyzer;

import gecos.annotations.AnnotationsPackage;

import gecos.blocks.BlocksPackage;

import gecos.core.CorePackage;

import gecos.dag.DagPackage;

import gecos.gecosproject.GecosprojectPackage;

import gecos.instrs.InstrsPackage;

import gecos.types.TypesPackage;

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import typeexploration.FixedPointTypeConfiguration;
import typeexploration.FixedPointTypeParam;
import typeexploration.FloatPointTypeConfiguration;
import typeexploration.FloatPointTypeParam;
import typeexploration.GenericTypeParam;
import typeexploration.NumberTypeConfiguration;
import typeexploration.SameTypeConstraint;
import typeexploration.SolutionSpace;
import typeexploration.TypeConfiguration;
import typeexploration.TypeConfigurationSpace;
import typeexploration.TypeConstraint;
import typeexploration.TypeParam;
import typeexploration.TypeexplorationFactory;
import typeexploration.TypeexplorationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TypeexplorationPackageImpl extends EPackageImpl implements TypeexplorationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typeParamEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fixedPointTypeParamEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass floatPointTypeParamEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass genericTypeParamEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typeConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sameTypeConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typeConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass numberTypeConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fixedPointTypeConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass floatPointTypeConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typeConfigurationSpaceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass symbolToTypeConfigurationsEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass solutionSpaceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType solutionSpaceAnalyzerEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see typeexploration.TypeexplorationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TypeexplorationPackageImpl() {
		super(eNS_URI, TypeexplorationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TypeexplorationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TypeexplorationPackage init() {
		if (isInited) return (TypeexplorationPackage)EPackage.Registry.INSTANCE.getEPackage(TypeexplorationPackage.eNS_URI);

		// Obtain or create and register package
		TypeexplorationPackageImpl theTypeexplorationPackage = (TypeexplorationPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TypeexplorationPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TypeexplorationPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		CorePackage.eINSTANCE.eClass();
		GecosprojectPackage.eINSTANCE.eClass();
		InstrsPackage.eINSTANCE.eClass();
		AnnotationsPackage.eINSTANCE.eClass();
		BlocksPackage.eINSTANCE.eClass();
		DagPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theTypeexplorationPackage.createPackageContents();

		// Initialize created meta-data
		theTypeexplorationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTypeexplorationPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TypeexplorationPackage.eNS_URI, theTypeexplorationPackage);
		return theTypeexplorationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTypeParam() {
		return typeParamEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFixedPointTypeParam() {
		return fixedPointTypeParamEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixedPointTypeParam_Signed() {
		return (EAttribute)fixedPointTypeParamEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixedPointTypeParam_TotalWidth() {
		return (EAttribute)fixedPointTypeParamEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixedPointTypeParam_IntegerWidth() {
		return (EAttribute)fixedPointTypeParamEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixedPointTypeParam_QuantificationMode() {
		return (EAttribute)fixedPointTypeParamEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixedPointTypeParam_OverflowMode() {
		return (EAttribute)fixedPointTypeParamEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFloatPointTypeParam() {
		return floatPointTypeParamEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFloatPointTypeParam_Signed() {
		return (EAttribute)floatPointTypeParamEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFloatPointTypeParam_TotalWidth() {
		return (EAttribute)floatPointTypeParamEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFloatPointTypeParam_ExponentWidth() {
		return (EAttribute)floatPointTypeParamEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFloatPointTypeParam_ExponentBias() {
		return (EAttribute)floatPointTypeParamEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGenericTypeParam() {
		return genericTypeParamEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGenericTypeParam_Type() {
		return (EReference)genericTypeParamEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTypeConstraint() {
		return typeConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSameTypeConstraint() {
		return sameTypeConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSameTypeConstraint_Symbol() {
		return (EReference)sameTypeConstraintEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTypeConfiguration() {
		return typeConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNumberTypeConfiguration() {
		return numberTypeConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNumberTypeConfiguration_Signed() {
		return (EAttribute)numberTypeConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNumberTypeConfiguration_TotalWidthValues() {
		return (EAttribute)numberTypeConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFixedPointTypeConfiguration() {
		return fixedPointTypeConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixedPointTypeConfiguration_IntegerWidthValues() {
		return (EAttribute)fixedPointTypeConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixedPointTypeConfiguration_QuantificationMode() {
		return (EAttribute)fixedPointTypeConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixedPointTypeConfiguration_OverflowMode() {
		return (EAttribute)fixedPointTypeConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFloatPointTypeConfiguration() {
		return floatPointTypeConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFloatPointTypeConfiguration_ExponentWidthValues() {
		return (EAttribute)floatPointTypeConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFloatPointTypeConfiguration_BiasValues() {
		return (EAttribute)floatPointTypeConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTypeConfigurationSpace() {
		return typeConfigurationSpaceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTypeConfigurationSpace_FixedPtCongurations() {
		return (EReference)typeConfigurationSpaceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTypeConfigurationSpace_FloatPtCongurations() {
		return (EReference)typeConfigurationSpaceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTypeConfigurationSpace_Constraints() {
		return (EReference)typeConfigurationSpaceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSymbolToTypeConfigurationsEntry() {
		return symbolToTypeConfigurationsEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSymbolToTypeConfigurationsEntry_Key() {
		return (EReference)symbolToTypeConfigurationsEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSymbolToTypeConfigurationsEntry_Value() {
		return (EReference)symbolToTypeConfigurationsEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSolutionSpace() {
		return solutionSpaceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSolutionSpace_Project() {
		return (EReference)solutionSpaceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSolutionSpace_ExploredSymbols() {
		return (EReference)solutionSpaceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSolutionSpace_DefaultTypeConfigs() {
		return (EReference)solutionSpaceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSolutionSpace_Analyzer() {
		return (EAttribute)solutionSpaceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getSolutionSpaceAnalyzer() {
		return solutionSpaceAnalyzerEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeexplorationFactory getTypeexplorationFactory() {
		return (TypeexplorationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		typeParamEClass = createEClass(TYPE_PARAM);

		fixedPointTypeParamEClass = createEClass(FIXED_POINT_TYPE_PARAM);
		createEAttribute(fixedPointTypeParamEClass, FIXED_POINT_TYPE_PARAM__SIGNED);
		createEAttribute(fixedPointTypeParamEClass, FIXED_POINT_TYPE_PARAM__TOTAL_WIDTH);
		createEAttribute(fixedPointTypeParamEClass, FIXED_POINT_TYPE_PARAM__INTEGER_WIDTH);
		createEAttribute(fixedPointTypeParamEClass, FIXED_POINT_TYPE_PARAM__QUANTIFICATION_MODE);
		createEAttribute(fixedPointTypeParamEClass, FIXED_POINT_TYPE_PARAM__OVERFLOW_MODE);

		floatPointTypeParamEClass = createEClass(FLOAT_POINT_TYPE_PARAM);
		createEAttribute(floatPointTypeParamEClass, FLOAT_POINT_TYPE_PARAM__SIGNED);
		createEAttribute(floatPointTypeParamEClass, FLOAT_POINT_TYPE_PARAM__TOTAL_WIDTH);
		createEAttribute(floatPointTypeParamEClass, FLOAT_POINT_TYPE_PARAM__EXPONENT_WIDTH);
		createEAttribute(floatPointTypeParamEClass, FLOAT_POINT_TYPE_PARAM__EXPONENT_BIAS);

		genericTypeParamEClass = createEClass(GENERIC_TYPE_PARAM);
		createEReference(genericTypeParamEClass, GENERIC_TYPE_PARAM__TYPE);

		typeConstraintEClass = createEClass(TYPE_CONSTRAINT);

		sameTypeConstraintEClass = createEClass(SAME_TYPE_CONSTRAINT);
		createEReference(sameTypeConstraintEClass, SAME_TYPE_CONSTRAINT__SYMBOL);

		typeConfigurationEClass = createEClass(TYPE_CONFIGURATION);

		numberTypeConfigurationEClass = createEClass(NUMBER_TYPE_CONFIGURATION);
		createEAttribute(numberTypeConfigurationEClass, NUMBER_TYPE_CONFIGURATION__SIGNED);
		createEAttribute(numberTypeConfigurationEClass, NUMBER_TYPE_CONFIGURATION__TOTAL_WIDTH_VALUES);

		fixedPointTypeConfigurationEClass = createEClass(FIXED_POINT_TYPE_CONFIGURATION);
		createEAttribute(fixedPointTypeConfigurationEClass, FIXED_POINT_TYPE_CONFIGURATION__INTEGER_WIDTH_VALUES);
		createEAttribute(fixedPointTypeConfigurationEClass, FIXED_POINT_TYPE_CONFIGURATION__QUANTIFICATION_MODE);
		createEAttribute(fixedPointTypeConfigurationEClass, FIXED_POINT_TYPE_CONFIGURATION__OVERFLOW_MODE);

		floatPointTypeConfigurationEClass = createEClass(FLOAT_POINT_TYPE_CONFIGURATION);
		createEAttribute(floatPointTypeConfigurationEClass, FLOAT_POINT_TYPE_CONFIGURATION__EXPONENT_WIDTH_VALUES);
		createEAttribute(floatPointTypeConfigurationEClass, FLOAT_POINT_TYPE_CONFIGURATION__BIAS_VALUES);

		typeConfigurationSpaceEClass = createEClass(TYPE_CONFIGURATION_SPACE);
		createEReference(typeConfigurationSpaceEClass, TYPE_CONFIGURATION_SPACE__FIXED_PT_CONGURATIONS);
		createEReference(typeConfigurationSpaceEClass, TYPE_CONFIGURATION_SPACE__FLOAT_PT_CONGURATIONS);
		createEReference(typeConfigurationSpaceEClass, TYPE_CONFIGURATION_SPACE__CONSTRAINTS);

		symbolToTypeConfigurationsEntryEClass = createEClass(SYMBOL_TO_TYPE_CONFIGURATIONS_ENTRY);
		createEReference(symbolToTypeConfigurationsEntryEClass, SYMBOL_TO_TYPE_CONFIGURATIONS_ENTRY__KEY);
		createEReference(symbolToTypeConfigurationsEntryEClass, SYMBOL_TO_TYPE_CONFIGURATIONS_ENTRY__VALUE);

		solutionSpaceEClass = createEClass(SOLUTION_SPACE);
		createEReference(solutionSpaceEClass, SOLUTION_SPACE__PROJECT);
		createEReference(solutionSpaceEClass, SOLUTION_SPACE__EXPLORED_SYMBOLS);
		createEReference(solutionSpaceEClass, SOLUTION_SPACE__DEFAULT_TYPE_CONFIGS);
		createEAttribute(solutionSpaceEClass, SOLUTION_SPACE__ANALYZER);

		// Create data types
		solutionSpaceAnalyzerEDataType = createEDataType(SOLUTION_SPACE_ANALYZER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		GecosprojectPackage theGecosprojectPackage = (GecosprojectPackage)EPackage.Registry.INSTANCE.getEPackage(GecosprojectPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		fixedPointTypeParamEClass.getESuperTypes().add(this.getTypeParam());
		floatPointTypeParamEClass.getESuperTypes().add(this.getTypeParam());
		genericTypeParamEClass.getESuperTypes().add(this.getTypeParam());
		sameTypeConstraintEClass.getESuperTypes().add(this.getTypeConstraint());
		numberTypeConfigurationEClass.getESuperTypes().add(this.getTypeConfiguration());
		fixedPointTypeConfigurationEClass.getESuperTypes().add(this.getNumberTypeConfiguration());
		floatPointTypeConfigurationEClass.getESuperTypes().add(this.getNumberTypeConfiguration());

		// Initialize classes and features; add operations and parameters
		initEClass(typeParamEClass, TypeParam.class, "TypeParam", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(typeParamEClass, theEcorePackage.getEInt(), "getTotalWidth", 0, 1, !IS_UNIQUE, IS_ORDERED);

		EOperation op = addEOperation(typeParamEClass, theEcorePackage.getEBoolean(), "equals", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypeParam(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(fixedPointTypeParamEClass, FixedPointTypeParam.class, "FixedPointTypeParam", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFixedPointTypeParam_Signed(), theEcorePackage.getEBoolean(), "signed", "true", 0, 1, FixedPointTypeParam.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFixedPointTypeParam_TotalWidth(), theEcorePackage.getEInt(), "totalWidth", null, 0, 1, FixedPointTypeParam.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFixedPointTypeParam_IntegerWidth(), theEcorePackage.getEInt(), "integerWidth", null, 0, 1, FixedPointTypeParam.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFixedPointTypeParam_QuantificationMode(), theTypesPackage.getQuantificationMode(), "quantificationMode", null, 0, 1, FixedPointTypeParam.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFixedPointTypeParam_OverflowMode(), theTypesPackage.getOverflowMode(), "overflowMode", null, 0, 1, FixedPointTypeParam.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(fixedPointTypeParamEClass, theEcorePackage.getEBoolean(), "equals", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypeParam(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(fixedPointTypeParamEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(floatPointTypeParamEClass, FloatPointTypeParam.class, "FloatPointTypeParam", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFloatPointTypeParam_Signed(), theEcorePackage.getEBoolean(), "signed", "true", 0, 1, FloatPointTypeParam.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFloatPointTypeParam_TotalWidth(), theEcorePackage.getEInt(), "totalWidth", null, 0, 1, FloatPointTypeParam.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFloatPointTypeParam_ExponentWidth(), theEcorePackage.getEInt(), "exponentWidth", null, 0, 1, FloatPointTypeParam.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFloatPointTypeParam_ExponentBias(), theEcorePackage.getEInt(), "exponentBias", null, 0, 1, FloatPointTypeParam.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(floatPointTypeParamEClass, theEcorePackage.getEBoolean(), "equals", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypeParam(), "other", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(floatPointTypeParamEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(genericTypeParamEClass, GenericTypeParam.class, "GenericTypeParam", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGenericTypeParam_Type(), theTypesPackage.getType(), null, "type", null, 0, 1, GenericTypeParam.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(genericTypeParamEClass, theEcorePackage.getEInt(), "getTotalWidth", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(genericTypeParamEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(typeConstraintEClass, TypeConstraint.class, "TypeConstraint", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(sameTypeConstraintEClass, SameTypeConstraint.class, "SameTypeConstraint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSameTypeConstraint_Symbol(), theCorePackage.getSymbol(), null, "symbol", null, 0, 1, SameTypeConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(sameTypeConstraintEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(typeConfigurationEClass, TypeConfiguration.class, "TypeConfiguration", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(numberTypeConfigurationEClass, NumberTypeConfiguration.class, "NumberTypeConfiguration", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNumberTypeConfiguration_Signed(), theEcorePackage.getEBoolean(), "signed", null, 0, -1, NumberTypeConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNumberTypeConfiguration_TotalWidthValues(), theEcorePackage.getEInt(), "totalWidthValues", null, 0, -1, NumberTypeConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(numberTypeConfigurationEClass, this.getNumberTypeConfiguration(), "mergeIn", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNumberTypeConfiguration(), "c", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(numberTypeConfigurationEClass, this.getNumberTypeConfiguration(), "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(fixedPointTypeConfigurationEClass, FixedPointTypeConfiguration.class, "FixedPointTypeConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFixedPointTypeConfiguration_IntegerWidthValues(), theEcorePackage.getEInt(), "integerWidthValues", null, 0, -1, FixedPointTypeConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFixedPointTypeConfiguration_QuantificationMode(), theTypesPackage.getQuantificationMode(), "quantificationMode", null, 0, -1, FixedPointTypeConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFixedPointTypeConfiguration_OverflowMode(), theTypesPackage.getOverflowMode(), "overflowMode", null, 0, -1, FixedPointTypeConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(fixedPointTypeConfigurationEClass, this.getFixedPointTypeConfiguration(), "mergeIn", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getFixedPointTypeConfiguration(), "c", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(fixedPointTypeConfigurationEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(floatPointTypeConfigurationEClass, FloatPointTypeConfiguration.class, "FloatPointTypeConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFloatPointTypeConfiguration_ExponentWidthValues(), theEcorePackage.getEInt(), "exponentWidthValues", null, 0, -1, FloatPointTypeConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFloatPointTypeConfiguration_BiasValues(), theEcorePackage.getEInt(), "biasValues", null, 0, -1, FloatPointTypeConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(floatPointTypeConfigurationEClass, this.getFloatPointTypeConfiguration(), "mergeIn", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getFloatPointTypeConfiguration(), "c", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(floatPointTypeConfigurationEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(typeConfigurationSpaceEClass, TypeConfigurationSpace.class, "TypeConfigurationSpace", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTypeConfigurationSpace_FixedPtCongurations(), this.getFixedPointTypeConfiguration(), null, "fixedPtCongurations", null, 0, 1, TypeConfigurationSpace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTypeConfigurationSpace_FloatPtCongurations(), this.getFloatPointTypeConfiguration(), null, "floatPtCongurations", null, 0, 1, TypeConfigurationSpace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTypeConfigurationSpace_Constraints(), this.getTypeConstraint(), null, "constraints", null, 0, -1, TypeConfigurationSpace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(typeConfigurationSpaceEClass, this.getTypeConfiguration(), "listConfigurations", 0, -1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(typeConfigurationSpaceEClass, null, "addConfiguration", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypeConfiguration(), "c", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(typeConfigurationSpaceEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(symbolToTypeConfigurationsEntryEClass, Map.Entry.class, "SymbolToTypeConfigurationsEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSymbolToTypeConfigurationsEntry_Key(), theCorePackage.getSymbol(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSymbolToTypeConfigurationsEntry_Value(), this.getTypeConfigurationSpace(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(solutionSpaceEClass, SolutionSpace.class, "SolutionSpace", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSolutionSpace_Project(), theGecosprojectPackage.getGecosProject(), null, "project", null, 0, 1, SolutionSpace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSolutionSpace_ExploredSymbols(), this.getSymbolToTypeConfigurationsEntry(), null, "exploredSymbols", null, 0, -1, SolutionSpace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSolutionSpace_DefaultTypeConfigs(), this.getTypeConfigurationSpace(), null, "defaultTypeConfigs", null, 0, 1, SolutionSpace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSolutionSpace_Analyzer(), this.getSolutionSpaceAnalyzer(), "analyzer", null, 0, 1, SolutionSpace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(solutionSpaceEClass, this.getTypeConfigurationSpace(), "getTypeSpace", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getSymbol(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(solutionSpaceEClass, theEcorePackage.getEBoolean(), "isDefault", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getSymbol(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(solutionSpaceEClass, this.getTypeConstraint(), "getTypeConstraints", 0, -1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getSymbol(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(solutionSpaceEClass, this.getFixedPointTypeConfiguration(), "getTypeFixedPtConfigs", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getSymbol(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(solutionSpaceEClass, this.getFloatPointTypeConfiguration(), "getTypeFloatPtConfigs", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getSymbol(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(solutionSpaceEClass, this.getTypeConfiguration(), "listTypeConfigs", 0, -1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getSymbol(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(solutionSpaceEClass, this.getTypeConfigurationSpace(), "getTypeSpaceOrDefault", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getSymbol(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(solutionSpaceEClass, null, "addTypeConstraint", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getSymbol(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypeConstraint(), "c", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(solutionSpaceEClass, null, "addTypeConfiguration", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getSymbol(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTypeConfiguration(), "c", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(solutionSpaceEClass, null, "addSymbol", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getSymbol(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(solutionSpaceEClass, theCorePackage.getSymbol(), "getSymbols", 0, -1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(solutionSpaceEClass, this.getSolutionSpace(), "copy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(solutionSpaceEClass, theEcorePackage.getEString(), "getUniqueTypeName", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getSymbol(), "s", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(solutionSpaceEClass, theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);

		// Initialize data types
		initEDataType(solutionSpaceAnalyzerEDataType, SolutionSpaceAnalyzer.class, "SolutionSpaceAnalyzer", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
		   });
	}

} //TypeexplorationPackageImpl
