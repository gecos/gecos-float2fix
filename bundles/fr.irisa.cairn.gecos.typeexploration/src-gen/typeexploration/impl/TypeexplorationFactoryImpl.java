/**
 */
package typeexploration.impl;

import fr.irisa.cairn.gecos.typeexploration.solutionspace.SolutionSpaceAnalyzer;

import gecos.core.Symbol;

import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import typeexploration.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TypeexplorationFactoryImpl extends EFactoryImpl implements TypeexplorationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TypeexplorationFactory init() {
		try {
			TypeexplorationFactory theTypeexplorationFactory = (TypeexplorationFactory)EPackage.Registry.INSTANCE.getEFactory(TypeexplorationPackage.eNS_URI);
			if (theTypeexplorationFactory != null) {
				return theTypeexplorationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TypeexplorationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeexplorationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TypeexplorationPackage.FIXED_POINT_TYPE_PARAM: return createFixedPointTypeParam();
			case TypeexplorationPackage.FLOAT_POINT_TYPE_PARAM: return createFloatPointTypeParam();
			case TypeexplorationPackage.GENERIC_TYPE_PARAM: return createGenericTypeParam();
			case TypeexplorationPackage.SAME_TYPE_CONSTRAINT: return createSameTypeConstraint();
			case TypeexplorationPackage.FIXED_POINT_TYPE_CONFIGURATION: return createFixedPointTypeConfiguration();
			case TypeexplorationPackage.FLOAT_POINT_TYPE_CONFIGURATION: return createFloatPointTypeConfiguration();
			case TypeexplorationPackage.TYPE_CONFIGURATION_SPACE: return createTypeConfigurationSpace();
			case TypeexplorationPackage.SYMBOL_TO_TYPE_CONFIGURATIONS_ENTRY: return (EObject)createSymbolToTypeConfigurationsEntry();
			case TypeexplorationPackage.SOLUTION_SPACE: return createSolutionSpace();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case TypeexplorationPackage.SOLUTION_SPACE_ANALYZER:
				return createSolutionSpaceAnalyzerFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case TypeexplorationPackage.SOLUTION_SPACE_ANALYZER:
				return convertSolutionSpaceAnalyzerToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixedPointTypeParam createFixedPointTypeParam() {
		FixedPointTypeParamImpl fixedPointTypeParam = new FixedPointTypeParamImpl();
		return fixedPointTypeParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloatPointTypeParam createFloatPointTypeParam() {
		FloatPointTypeParamImpl floatPointTypeParam = new FloatPointTypeParamImpl();
		return floatPointTypeParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericTypeParam createGenericTypeParam() {
		GenericTypeParamImpl genericTypeParam = new GenericTypeParamImpl();
		return genericTypeParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SameTypeConstraint createSameTypeConstraint() {
		SameTypeConstraintImpl sameTypeConstraint = new SameTypeConstraintImpl();
		return sameTypeConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixedPointTypeConfiguration createFixedPointTypeConfiguration() {
		FixedPointTypeConfigurationImpl fixedPointTypeConfiguration = new FixedPointTypeConfigurationImpl();
		return fixedPointTypeConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloatPointTypeConfiguration createFloatPointTypeConfiguration() {
		FloatPointTypeConfigurationImpl floatPointTypeConfiguration = new FloatPointTypeConfigurationImpl();
		return floatPointTypeConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeConfigurationSpace createTypeConfigurationSpace() {
		TypeConfigurationSpaceImpl typeConfigurationSpace = new TypeConfigurationSpaceImpl();
		return typeConfigurationSpace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Symbol, TypeConfigurationSpace> createSymbolToTypeConfigurationsEntry() {
		SymbolToTypeConfigurationsEntryImpl symbolToTypeConfigurationsEntry = new SymbolToTypeConfigurationsEntryImpl();
		return symbolToTypeConfigurationsEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SolutionSpace createSolutionSpace() {
		SolutionSpaceImpl solutionSpace = new SolutionSpaceImpl();
		return solutionSpace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SolutionSpaceAnalyzer createSolutionSpaceAnalyzerFromString(EDataType eDataType, String initialValue) {
		return (SolutionSpaceAnalyzer)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSolutionSpaceAnalyzerToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeexplorationPackage getTypeexplorationPackage() {
		return (TypeexplorationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TypeexplorationPackage getPackage() {
		return TypeexplorationPackage.eINSTANCE;
	}

} //TypeexplorationFactoryImpl
