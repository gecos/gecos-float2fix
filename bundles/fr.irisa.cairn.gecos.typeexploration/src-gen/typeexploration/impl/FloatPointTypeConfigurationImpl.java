/**
 */
package typeexploration.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import typeexploration.FloatPointTypeConfiguration;
import typeexploration.TypeexplorationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Float Point Type Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.impl.FloatPointTypeConfigurationImpl#getExponentWidthValues <em>Exponent Width Values</em>}</li>
 *   <li>{@link typeexploration.impl.FloatPointTypeConfigurationImpl#getBiasValues <em>Bias Values</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FloatPointTypeConfigurationImpl extends NumberTypeConfigurationImpl implements FloatPointTypeConfiguration {
	/**
	 * The cached value of the '{@link #getExponentWidthValues() <em>Exponent Width Values</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExponentWidthValues()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> exponentWidthValues;

	/**
	 * The cached value of the '{@link #getBiasValues() <em>Bias Values</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBiasValues()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> biasValues;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FloatPointTypeConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypeexplorationPackage.Literals.FLOAT_POINT_TYPE_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getExponentWidthValues() {
		if (exponentWidthValues == null) {
			exponentWidthValues = new EDataTypeUniqueEList<Integer>(Integer.class, this, TypeexplorationPackage.FLOAT_POINT_TYPE_CONFIGURATION__EXPONENT_WIDTH_VALUES);
		}
		return exponentWidthValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getBiasValues() {
		if (biasValues == null) {
			biasValues = new EDataTypeUniqueEList<Integer>(Integer.class, this, TypeexplorationPackage.FLOAT_POINT_TYPE_CONFIGURATION__BIAS_VALUES);
		}
		return biasValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloatPointTypeConfiguration mergeIn(final FloatPointTypeConfiguration c) {
		super.mergeIn(c);
		this.getExponentWidthValues().addAll(c.getExponentWidthValues());
		this.getBiasValues().addAll(c.getBiasValues());
		return this;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		EList<Boolean> _signed = this.getSigned();
		String _plus = ("Floating-point: S=" + _signed);
		String _plus_1 = (_plus + " W=");
		EList<Integer> _totalWidthValues = this.getTotalWidthValues();
		String _plus_2 = (_plus_1 + _totalWidthValues);
		String _plus_3 = (_plus_2 + " E=");
		EList<Integer> _exponentWidthValues = this.getExponentWidthValues();
		return (_plus_3 + _exponentWidthValues);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TypeexplorationPackage.FLOAT_POINT_TYPE_CONFIGURATION__EXPONENT_WIDTH_VALUES:
				return getExponentWidthValues();
			case TypeexplorationPackage.FLOAT_POINT_TYPE_CONFIGURATION__BIAS_VALUES:
				return getBiasValues();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TypeexplorationPackage.FLOAT_POINT_TYPE_CONFIGURATION__EXPONENT_WIDTH_VALUES:
				getExponentWidthValues().clear();
				getExponentWidthValues().addAll((Collection<? extends Integer>)newValue);
				return;
			case TypeexplorationPackage.FLOAT_POINT_TYPE_CONFIGURATION__BIAS_VALUES:
				getBiasValues().clear();
				getBiasValues().addAll((Collection<? extends Integer>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TypeexplorationPackage.FLOAT_POINT_TYPE_CONFIGURATION__EXPONENT_WIDTH_VALUES:
				getExponentWidthValues().clear();
				return;
			case TypeexplorationPackage.FLOAT_POINT_TYPE_CONFIGURATION__BIAS_VALUES:
				getBiasValues().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TypeexplorationPackage.FLOAT_POINT_TYPE_CONFIGURATION__EXPONENT_WIDTH_VALUES:
				return exponentWidthValues != null && !exponentWidthValues.isEmpty();
			case TypeexplorationPackage.FLOAT_POINT_TYPE_CONFIGURATION__BIAS_VALUES:
				return biasValues != null && !biasValues.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //FloatPointTypeConfigurationImpl
