/**
 */
package typeexploration.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EcoreUtil;

import typeexploration.NumberTypeConfiguration;
import typeexploration.TypeexplorationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Number Type Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.impl.NumberTypeConfigurationImpl#getSigned <em>Signed</em>}</li>
 *   <li>{@link typeexploration.impl.NumberTypeConfigurationImpl#getTotalWidthValues <em>Total Width Values</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class NumberTypeConfigurationImpl extends MinimalEObjectImpl.Container implements NumberTypeConfiguration {
	/**
	 * The cached value of the '{@link #getSigned() <em>Signed</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSigned()
	 * @generated
	 * @ordered
	 */
	protected EList<Boolean> signed;

	/**
	 * The cached value of the '{@link #getTotalWidthValues() <em>Total Width Values</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotalWidthValues()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> totalWidthValues;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NumberTypeConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypeexplorationPackage.Literals.NUMBER_TYPE_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Boolean> getSigned() {
		if (signed == null) {
			signed = new EDataTypeUniqueEList<Boolean>(Boolean.class, this, TypeexplorationPackage.NUMBER_TYPE_CONFIGURATION__SIGNED);
		}
		return signed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getTotalWidthValues() {
		if (totalWidthValues == null) {
			totalWidthValues = new EDataTypeUniqueEList<Integer>(Integer.class, this, TypeexplorationPackage.NUMBER_TYPE_CONFIGURATION__TOTAL_WIDTH_VALUES);
		}
		return totalWidthValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NumberTypeConfiguration mergeIn(final NumberTypeConfiguration c) {
		this.getSigned().addAll(c.getSigned());
		this.getTotalWidthValues().addAll(c.getTotalWidthValues());
		return this;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NumberTypeConfiguration copy() {
		return EcoreUtil.<NumberTypeConfiguration>copy(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TypeexplorationPackage.NUMBER_TYPE_CONFIGURATION__SIGNED:
				return getSigned();
			case TypeexplorationPackage.NUMBER_TYPE_CONFIGURATION__TOTAL_WIDTH_VALUES:
				return getTotalWidthValues();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TypeexplorationPackage.NUMBER_TYPE_CONFIGURATION__SIGNED:
				getSigned().clear();
				getSigned().addAll((Collection<? extends Boolean>)newValue);
				return;
			case TypeexplorationPackage.NUMBER_TYPE_CONFIGURATION__TOTAL_WIDTH_VALUES:
				getTotalWidthValues().clear();
				getTotalWidthValues().addAll((Collection<? extends Integer>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TypeexplorationPackage.NUMBER_TYPE_CONFIGURATION__SIGNED:
				getSigned().clear();
				return;
			case TypeexplorationPackage.NUMBER_TYPE_CONFIGURATION__TOTAL_WIDTH_VALUES:
				getTotalWidthValues().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TypeexplorationPackage.NUMBER_TYPE_CONFIGURATION__SIGNED:
				return signed != null && !signed.isEmpty();
			case TypeexplorationPackage.NUMBER_TYPE_CONFIGURATION__TOTAL_WIDTH_VALUES:
				return totalWidthValues != null && !totalWidthValues.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (signed: ");
		result.append(signed);
		result.append(", totalWidthValues: ");
		result.append(totalWidthValues);
		result.append(')');
		return result.toString();
	}

} //NumberTypeConfigurationImpl
