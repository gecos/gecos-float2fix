/**
 */
package typeexploration.impl;

import com.google.common.base.Objects;

import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import typeexploration.FixedPointTypeParam;
import typeexploration.TypeParam;
import typeexploration.TypeexplorationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fixed Point Type Param</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.impl.FixedPointTypeParamImpl#isSigned <em>Signed</em>}</li>
 *   <li>{@link typeexploration.impl.FixedPointTypeParamImpl#getTotalWidth <em>Total Width</em>}</li>
 *   <li>{@link typeexploration.impl.FixedPointTypeParamImpl#getIntegerWidth <em>Integer Width</em>}</li>
 *   <li>{@link typeexploration.impl.FixedPointTypeParamImpl#getQuantificationMode <em>Quantification Mode</em>}</li>
 *   <li>{@link typeexploration.impl.FixedPointTypeParamImpl#getOverflowMode <em>Overflow Mode</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FixedPointTypeParamImpl extends TypeParamImpl implements FixedPointTypeParam {
	/**
	 * The default value of the '{@link #isSigned() <em>Signed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSigned()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SIGNED_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isSigned() <em>Signed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSigned()
	 * @generated
	 * @ordered
	 */
	protected boolean signed = SIGNED_EDEFAULT;

	/**
	 * The default value of the '{@link #getTotalWidth() <em>Total Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotalWidth()
	 * @generated
	 * @ordered
	 */
	protected static final int TOTAL_WIDTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getTotalWidth() <em>Total Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotalWidth()
	 * @generated
	 * @ordered
	 */
	protected int totalWidth = TOTAL_WIDTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getIntegerWidth() <em>Integer Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegerWidth()
	 * @generated
	 * @ordered
	 */
	protected static final int INTEGER_WIDTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getIntegerWidth() <em>Integer Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegerWidth()
	 * @generated
	 * @ordered
	 */
	protected int integerWidth = INTEGER_WIDTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantificationMode() <em>Quantification Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantificationMode()
	 * @generated
	 * @ordered
	 */
	protected static final QuantificationMode QUANTIFICATION_MODE_EDEFAULT = QuantificationMode.AC_TRN;

	/**
	 * The cached value of the '{@link #getQuantificationMode() <em>Quantification Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantificationMode()
	 * @generated
	 * @ordered
	 */
	protected QuantificationMode quantificationMode = QUANTIFICATION_MODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getOverflowMode() <em>Overflow Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverflowMode()
	 * @generated
	 * @ordered
	 */
	protected static final OverflowMode OVERFLOW_MODE_EDEFAULT = OverflowMode.AC_WRAP;

	/**
	 * The cached value of the '{@link #getOverflowMode() <em>Overflow Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverflowMode()
	 * @generated
	 * @ordered
	 */
	protected OverflowMode overflowMode = OVERFLOW_MODE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FixedPointTypeParamImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypeexplorationPackage.Literals.FIXED_POINT_TYPE_PARAM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSigned() {
		return signed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSigned(boolean newSigned) {
		boolean oldSigned = signed;
		signed = newSigned;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__SIGNED, oldSigned, signed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getTotalWidth() {
		return totalWidth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTotalWidth(int newTotalWidth) {
		int oldTotalWidth = totalWidth;
		totalWidth = newTotalWidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__TOTAL_WIDTH, oldTotalWidth, totalWidth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getIntegerWidth() {
		return integerWidth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntegerWidth(int newIntegerWidth) {
		int oldIntegerWidth = integerWidth;
		integerWidth = newIntegerWidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__INTEGER_WIDTH, oldIntegerWidth, integerWidth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QuantificationMode getQuantificationMode() {
		return quantificationMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantificationMode(QuantificationMode newQuantificationMode) {
		QuantificationMode oldQuantificationMode = quantificationMode;
		quantificationMode = newQuantificationMode == null ? QUANTIFICATION_MODE_EDEFAULT : newQuantificationMode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__QUANTIFICATION_MODE, oldQuantificationMode, quantificationMode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OverflowMode getOverflowMode() {
		return overflowMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOverflowMode(OverflowMode newOverflowMode) {
		OverflowMode oldOverflowMode = overflowMode;
		overflowMode = newOverflowMode == null ? OVERFLOW_MODE_EDEFAULT : newOverflowMode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__OVERFLOW_MODE, oldOverflowMode, overflowMode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean equals(final TypeParam other) {
		boolean _xifexpression = false;
		if ((other instanceof FixedPointTypeParam)) {
			_xifexpression = (((((this.isSigned() == ((FixedPointTypeParam)other).isSigned()) && 
				(this.getTotalWidth() == ((FixedPointTypeParam)other).getTotalWidth())) && 
				(this.getIntegerWidth() == ((FixedPointTypeParam)other).getIntegerWidth())) && 
				Objects.equal(this.getQuantificationMode(), ((FixedPointTypeParam)other).getQuantificationMode())) && 
				Objects.equal(this.getOverflowMode(), ((FixedPointTypeParam)other).getOverflowMode()));
		}
		else {
			_xifexpression = false;
		}
		return _xifexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _xifexpression = null;
		boolean _isSigned = this.isSigned();
		boolean _not = (!_isSigned);
		if (_not) {
			_xifexpression = "un";
		}
		else {
			_xifexpression = "";
		}
		String _plus = ("Fixed-point type: " + _xifexpression);
		String _plus_1 = (_plus + "signed W=");
		int _totalWidth = this.getTotalWidth();
		String _plus_2 = (_plus_1 + Integer.valueOf(_totalWidth));
		String _plus_3 = (_plus_2 + " I=");
		int _integerWidth = this.getIntegerWidth();
		String _plus_4 = (_plus_3 + Integer.valueOf(_integerWidth));
		String _plus_5 = (_plus_4 + " Q=");
		QuantificationMode _quantificationMode = this.getQuantificationMode();
		String _plus_6 = (_plus_5 + _quantificationMode);
		String _plus_7 = (_plus_6 + " O=");
		OverflowMode _overflowMode = this.getOverflowMode();
		return (_plus_7 + _overflowMode);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__SIGNED:
				return isSigned();
			case TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__TOTAL_WIDTH:
				return getTotalWidth();
			case TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__INTEGER_WIDTH:
				return getIntegerWidth();
			case TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__QUANTIFICATION_MODE:
				return getQuantificationMode();
			case TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__OVERFLOW_MODE:
				return getOverflowMode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__SIGNED:
				setSigned((Boolean)newValue);
				return;
			case TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__TOTAL_WIDTH:
				setTotalWidth((Integer)newValue);
				return;
			case TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__INTEGER_WIDTH:
				setIntegerWidth((Integer)newValue);
				return;
			case TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__QUANTIFICATION_MODE:
				setQuantificationMode((QuantificationMode)newValue);
				return;
			case TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__OVERFLOW_MODE:
				setOverflowMode((OverflowMode)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__SIGNED:
				setSigned(SIGNED_EDEFAULT);
				return;
			case TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__TOTAL_WIDTH:
				setTotalWidth(TOTAL_WIDTH_EDEFAULT);
				return;
			case TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__INTEGER_WIDTH:
				setIntegerWidth(INTEGER_WIDTH_EDEFAULT);
				return;
			case TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__QUANTIFICATION_MODE:
				setQuantificationMode(QUANTIFICATION_MODE_EDEFAULT);
				return;
			case TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__OVERFLOW_MODE:
				setOverflowMode(OVERFLOW_MODE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__SIGNED:
				return signed != SIGNED_EDEFAULT;
			case TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__TOTAL_WIDTH:
				return totalWidth != TOTAL_WIDTH_EDEFAULT;
			case TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__INTEGER_WIDTH:
				return integerWidth != INTEGER_WIDTH_EDEFAULT;
			case TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__QUANTIFICATION_MODE:
				return quantificationMode != QUANTIFICATION_MODE_EDEFAULT;
			case TypeexplorationPackage.FIXED_POINT_TYPE_PARAM__OVERFLOW_MODE:
				return overflowMode != OVERFLOW_MODE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //FixedPointTypeParamImpl
