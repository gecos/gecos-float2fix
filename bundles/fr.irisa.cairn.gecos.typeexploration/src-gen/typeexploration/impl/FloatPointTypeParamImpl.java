/**
 */
package typeexploration.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import typeexploration.FloatPointTypeParam;
import typeexploration.TypeParam;
import typeexploration.TypeexplorationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Float Point Type Param</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.impl.FloatPointTypeParamImpl#isSigned <em>Signed</em>}</li>
 *   <li>{@link typeexploration.impl.FloatPointTypeParamImpl#getTotalWidth <em>Total Width</em>}</li>
 *   <li>{@link typeexploration.impl.FloatPointTypeParamImpl#getExponentWidth <em>Exponent Width</em>}</li>
 *   <li>{@link typeexploration.impl.FloatPointTypeParamImpl#getExponentBias <em>Exponent Bias</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FloatPointTypeParamImpl extends TypeParamImpl implements FloatPointTypeParam {
	/**
	 * The default value of the '{@link #isSigned() <em>Signed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSigned()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SIGNED_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isSigned() <em>Signed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSigned()
	 * @generated
	 * @ordered
	 */
	protected boolean signed = SIGNED_EDEFAULT;

	/**
	 * The default value of the '{@link #getTotalWidth() <em>Total Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotalWidth()
	 * @generated
	 * @ordered
	 */
	protected static final int TOTAL_WIDTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getTotalWidth() <em>Total Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotalWidth()
	 * @generated
	 * @ordered
	 */
	protected int totalWidth = TOTAL_WIDTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getExponentWidth() <em>Exponent Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExponentWidth()
	 * @generated
	 * @ordered
	 */
	protected static final int EXPONENT_WIDTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getExponentWidth() <em>Exponent Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExponentWidth()
	 * @generated
	 * @ordered
	 */
	protected int exponentWidth = EXPONENT_WIDTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getExponentBias() <em>Exponent Bias</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExponentBias()
	 * @generated
	 * @ordered
	 */
	protected static final int EXPONENT_BIAS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getExponentBias() <em>Exponent Bias</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExponentBias()
	 * @generated
	 * @ordered
	 */
	protected int exponentBias = EXPONENT_BIAS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FloatPointTypeParamImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypeexplorationPackage.Literals.FLOAT_POINT_TYPE_PARAM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSigned() {
		return signed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSigned(boolean newSigned) {
		boolean oldSigned = signed;
		signed = newSigned;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypeexplorationPackage.FLOAT_POINT_TYPE_PARAM__SIGNED, oldSigned, signed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getTotalWidth() {
		return totalWidth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTotalWidth(int newTotalWidth) {
		int oldTotalWidth = totalWidth;
		totalWidth = newTotalWidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypeexplorationPackage.FLOAT_POINT_TYPE_PARAM__TOTAL_WIDTH, oldTotalWidth, totalWidth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getExponentWidth() {
		return exponentWidth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExponentWidth(int newExponentWidth) {
		int oldExponentWidth = exponentWidth;
		exponentWidth = newExponentWidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypeexplorationPackage.FLOAT_POINT_TYPE_PARAM__EXPONENT_WIDTH, oldExponentWidth, exponentWidth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getExponentBias() {
		return exponentBias;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExponentBias(int newExponentBias) {
		int oldExponentBias = exponentBias;
		exponentBias = newExponentBias;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypeexplorationPackage.FLOAT_POINT_TYPE_PARAM__EXPONENT_BIAS, oldExponentBias, exponentBias));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean equals(final TypeParam other) {
		boolean _xifexpression = false;
		if ((other instanceof FloatPointTypeParam)) {
			_xifexpression = ((((this.isSigned() == ((FloatPointTypeParam)other).isSigned()) && 
				(this.getTotalWidth() == ((FloatPointTypeParam)other).getTotalWidth())) && 
				(this.getExponentWidth() == ((FloatPointTypeParam)other).getExponentWidth())) && 
				(this.getExponentBias() == ((FloatPointTypeParam)other).getExponentBias()));
		}
		else {
			_xifexpression = false;
		}
		return _xifexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		String _xifexpression = null;
		boolean _isSigned = this.isSigned();
		boolean _not = (!_isSigned);
		if (_not) {
			_xifexpression = "un";
		}
		else {
			_xifexpression = "";
		}
		String _plus = ("Floating-point type:" + _xifexpression);
		String _plus_1 = (_plus + "signed W=");
		int _totalWidth = this.getTotalWidth();
		String _plus_2 = (_plus_1 + Integer.valueOf(_totalWidth));
		String _plus_3 = (_plus_2 + " E=");
		int _exponentWidth = this.getExponentWidth();
		return (_plus_3 + Integer.valueOf(_exponentWidth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TypeexplorationPackage.FLOAT_POINT_TYPE_PARAM__SIGNED:
				return isSigned();
			case TypeexplorationPackage.FLOAT_POINT_TYPE_PARAM__TOTAL_WIDTH:
				return getTotalWidth();
			case TypeexplorationPackage.FLOAT_POINT_TYPE_PARAM__EXPONENT_WIDTH:
				return getExponentWidth();
			case TypeexplorationPackage.FLOAT_POINT_TYPE_PARAM__EXPONENT_BIAS:
				return getExponentBias();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TypeexplorationPackage.FLOAT_POINT_TYPE_PARAM__SIGNED:
				setSigned((Boolean)newValue);
				return;
			case TypeexplorationPackage.FLOAT_POINT_TYPE_PARAM__TOTAL_WIDTH:
				setTotalWidth((Integer)newValue);
				return;
			case TypeexplorationPackage.FLOAT_POINT_TYPE_PARAM__EXPONENT_WIDTH:
				setExponentWidth((Integer)newValue);
				return;
			case TypeexplorationPackage.FLOAT_POINT_TYPE_PARAM__EXPONENT_BIAS:
				setExponentBias((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TypeexplorationPackage.FLOAT_POINT_TYPE_PARAM__SIGNED:
				setSigned(SIGNED_EDEFAULT);
				return;
			case TypeexplorationPackage.FLOAT_POINT_TYPE_PARAM__TOTAL_WIDTH:
				setTotalWidth(TOTAL_WIDTH_EDEFAULT);
				return;
			case TypeexplorationPackage.FLOAT_POINT_TYPE_PARAM__EXPONENT_WIDTH:
				setExponentWidth(EXPONENT_WIDTH_EDEFAULT);
				return;
			case TypeexplorationPackage.FLOAT_POINT_TYPE_PARAM__EXPONENT_BIAS:
				setExponentBias(EXPONENT_BIAS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TypeexplorationPackage.FLOAT_POINT_TYPE_PARAM__SIGNED:
				return signed != SIGNED_EDEFAULT;
			case TypeexplorationPackage.FLOAT_POINT_TYPE_PARAM__TOTAL_WIDTH:
				return totalWidth != TOTAL_WIDTH_EDEFAULT;
			case TypeexplorationPackage.FLOAT_POINT_TYPE_PARAM__EXPONENT_WIDTH:
				return exponentWidth != EXPONENT_WIDTH_EDEFAULT;
			case TypeexplorationPackage.FLOAT_POINT_TYPE_PARAM__EXPONENT_BIAS:
				return exponentBias != EXPONENT_BIAS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //FloatPointTypeParamImpl
