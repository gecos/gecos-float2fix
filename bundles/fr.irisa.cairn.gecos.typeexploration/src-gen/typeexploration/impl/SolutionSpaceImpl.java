/**
 */
package typeexploration.impl;

import fr.irisa.cairn.gecos.typeexploration.model.UserFactory;

import fr.irisa.cairn.gecos.typeexploration.solutionspace.SolutionSpaceAnalyzer;

import fr.irisa.cairn.gecos.typeexploration.utils.SolutionSpaceUtils;

import gecos.core.Symbol;

import gecos.gecosproject.GecosProject;

import java.lang.CharSequence;

import java.util.Collections;

import java.util.Map.Entry;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions;

import org.eclipse.xtext.xbase.lib.CollectionLiterals;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

import typeexploration.FixedPointTypeConfiguration;
import typeexploration.FloatPointTypeConfiguration;
import typeexploration.SolutionSpace;
import typeexploration.TypeConfiguration;
import typeexploration.TypeConfigurationSpace;
import typeexploration.TypeConstraint;
import typeexploration.TypeexplorationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Solution Space</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.impl.SolutionSpaceImpl#getProject <em>Project</em>}</li>
 *   <li>{@link typeexploration.impl.SolutionSpaceImpl#getExploredSymbols <em>Explored Symbols</em>}</li>
 *   <li>{@link typeexploration.impl.SolutionSpaceImpl#getDefaultTypeConfigs <em>Default Type Configs</em>}</li>
 *   <li>{@link typeexploration.impl.SolutionSpaceImpl#getAnalyzer <em>Analyzer</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SolutionSpaceImpl extends MinimalEObjectImpl.Container implements SolutionSpace {
	/**
	 * The cached value of the '{@link #getProject() <em>Project</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProject()
	 * @generated
	 * @ordered
	 */
	protected GecosProject project;

	/**
	 * The cached value of the '{@link #getExploredSymbols() <em>Explored Symbols</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExploredSymbols()
	 * @generated
	 * @ordered
	 */
	protected EMap<Symbol, TypeConfigurationSpace> exploredSymbols;

	/**
	 * The cached value of the '{@link #getDefaultTypeConfigs() <em>Default Type Configs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultTypeConfigs()
	 * @generated
	 * @ordered
	 */
	protected TypeConfigurationSpace defaultTypeConfigs;

	/**
	 * The default value of the '{@link #getAnalyzer() <em>Analyzer</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnalyzer()
	 * @generated
	 * @ordered
	 */
	protected static final SolutionSpaceAnalyzer ANALYZER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAnalyzer() <em>Analyzer</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnalyzer()
	 * @generated
	 * @ordered
	 */
	protected SolutionSpaceAnalyzer analyzer = ANALYZER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SolutionSpaceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypeexplorationPackage.Literals.SOLUTION_SPACE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GecosProject getProject() {
		if (project != null && project.eIsProxy()) {
			InternalEObject oldProject = (InternalEObject)project;
			project = (GecosProject)eResolveProxy(oldProject);
			if (project != oldProject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TypeexplorationPackage.SOLUTION_SPACE__PROJECT, oldProject, project));
			}
		}
		return project;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GecosProject basicGetProject() {
		return project;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProject(GecosProject newProject) {
		GecosProject oldProject = project;
		project = newProject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypeexplorationPackage.SOLUTION_SPACE__PROJECT, oldProject, project));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Symbol, TypeConfigurationSpace> getExploredSymbols() {
		if (exploredSymbols == null) {
			exploredSymbols = new EcoreEMap<Symbol,TypeConfigurationSpace>(TypeexplorationPackage.Literals.SYMBOL_TO_TYPE_CONFIGURATIONS_ENTRY, SymbolToTypeConfigurationsEntryImpl.class, this, TypeexplorationPackage.SOLUTION_SPACE__EXPLORED_SYMBOLS);
		}
		return exploredSymbols;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeConfigurationSpace getDefaultTypeConfigs() {
		return defaultTypeConfigs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefaultTypeConfigs(TypeConfigurationSpace newDefaultTypeConfigs, NotificationChain msgs) {
		TypeConfigurationSpace oldDefaultTypeConfigs = defaultTypeConfigs;
		defaultTypeConfigs = newDefaultTypeConfigs;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TypeexplorationPackage.SOLUTION_SPACE__DEFAULT_TYPE_CONFIGS, oldDefaultTypeConfigs, newDefaultTypeConfigs);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultTypeConfigs(TypeConfigurationSpace newDefaultTypeConfigs) {
		if (newDefaultTypeConfigs != defaultTypeConfigs) {
			NotificationChain msgs = null;
			if (defaultTypeConfigs != null)
				msgs = ((InternalEObject)defaultTypeConfigs).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TypeexplorationPackage.SOLUTION_SPACE__DEFAULT_TYPE_CONFIGS, null, msgs);
			if (newDefaultTypeConfigs != null)
				msgs = ((InternalEObject)newDefaultTypeConfigs).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TypeexplorationPackage.SOLUTION_SPACE__DEFAULT_TYPE_CONFIGS, null, msgs);
			msgs = basicSetDefaultTypeConfigs(newDefaultTypeConfigs, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypeexplorationPackage.SOLUTION_SPACE__DEFAULT_TYPE_CONFIGS, newDefaultTypeConfigs, newDefaultTypeConfigs));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SolutionSpaceAnalyzer getAnalyzer() {
		return analyzer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnalyzer(SolutionSpaceAnalyzer newAnalyzer) {
		SolutionSpaceAnalyzer oldAnalyzer = analyzer;
		analyzer = newAnalyzer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypeexplorationPackage.SOLUTION_SPACE__ANALYZER, oldAnalyzer, analyzer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeConfigurationSpace getTypeSpace(final Symbol s) {
		return this.getExploredSymbols().get(s);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDefault(final Symbol s) {
		TypeConfigurationSpace _get = this.getExploredSymbols().get(s);
		return (_get == null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TypeConstraint> getTypeConstraints(final Symbol s) {
		TypeConfigurationSpace _get = this.getExploredSymbols().get(s);
		EList<TypeConstraint> _constraints = null;
		if (_get!=null) {
			_constraints=_get.getConstraints();
		}
		return _constraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixedPointTypeConfiguration getTypeFixedPtConfigs(final Symbol s) {
		TypeConfigurationSpace _get = this.getExploredSymbols().get(s);
		FixedPointTypeConfiguration _fixedPtCongurations = null;
		if (_get!=null) {
			_fixedPtCongurations=_get.getFixedPtCongurations();
		}
		return _fixedPtCongurations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloatPointTypeConfiguration getTypeFloatPtConfigs(final Symbol s) {
		TypeConfigurationSpace _get = this.getExploredSymbols().get(s);
		FloatPointTypeConfiguration _floatPtCongurations = null;
		if (_get!=null) {
			_floatPtCongurations=_get.getFloatPtCongurations();
		}
		return _floatPtCongurations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TypeConfiguration> listTypeConfigs(final Symbol s) {
		TypeConfigurationSpace _get = this.getExploredSymbols().get(s);
		EList<TypeConfiguration> _listConfigurations = null;
		if (_get!=null) {
			_listConfigurations=_get.listConfigurations();
		}
		return _listConfigurations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeConfigurationSpace getTypeSpaceOrDefault(final Symbol s) {
		TypeConfigurationSpace _elvis = null;
		TypeConfigurationSpace _get = this.getExploredSymbols().get(s);
		if (_get != null) {
			_elvis = _get;
		} else {
			TypeConfigurationSpace _defaultTypeConfigs = this.getDefaultTypeConfigs();
			_elvis = _defaultTypeConfigs;
		}
		return _elvis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addTypeConstraint(final Symbol s, final TypeConstraint c) {
		final EList<TypeConstraint> constraints = this.getTypeConstraints(s);
		if ((constraints == null)) {
			this.getExploredSymbols().put(s, UserFactory.configurationSpace(CollectionLiterals.<TypeConfiguration>emptyList(), Collections.<TypeConstraint>singletonList(c)));
		}
		else {
			constraints.add(c);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addTypeConfiguration(final Symbol s, final TypeConfiguration c) {
		final TypeConfigurationSpace space = this.getExploredSymbols().get(s);
		if ((space == null)) {
			this.getExploredSymbols().put(s, UserFactory.configurationSpace(Collections.<TypeConfiguration>singletonList(c)));
		}
		else {
			space.addConfiguration(c);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addSymbol(final Symbol s) {
		boolean _containsKey = this.getExploredSymbols().containsKey(s);
		boolean _not = (!_containsKey);
		if (_not) {
			this.getExploredSymbols().put(s, null);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Symbol> getSymbols() {
		final Function1<Entry<Symbol, TypeConfigurationSpace>, Symbol> _function = new Function1<Entry<Symbol, TypeConfigurationSpace>, Symbol>() {
			public Symbol apply(final Entry<Symbol, TypeConfigurationSpace> it) {
				return it.getKey();
			}
		};
		return ECollections.<Symbol>unmodifiableEList(XcoreEListExtensions.<Entry<Symbol, TypeConfigurationSpace>, Symbol>map(ECollections.<Entry<Symbol, TypeConfigurationSpace>>asEList(this.getExploredSymbols()), _function));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SolutionSpace copy() {
		return EcoreUtil.<SolutionSpace>copy(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUniqueTypeName(final Symbol s) {
		String _name = s.getName();
		String _plus = ("TYPE_" + _name);
		String _plus_1 = (_plus + "_");
		int _indexOfKey = this.getExploredSymbols().indexOfKey(s);
		return (_plus_1 + Integer.valueOf(_indexOfKey));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		int _size = this.getExploredSymbols().size();
		String _plus = ("Solution Space (" + Integer.valueOf(_size));
		String _plus_1 = (_plus + "):\n");
		final Function1<Symbol, CharSequence> _function = new Function1<Symbol, CharSequence>() {
			public CharSequence apply(final Symbol it) {
				String _printSymbol = SolutionSpaceUtils.printSymbol(it);
				String _plus = ("\tSymbol \'" + _printSymbol);
				String _plus_1 = (_plus + "\' (");
				String _simpleName = it.getClass().getSimpleName();
				String _plus_2 = (_plus_1 + _simpleName);
				String _plus_3 = (_plus_2 + "):\n");
				TypeConfigurationSpace _typeSpace = SolutionSpaceImpl.this.getTypeSpace(it);
				return (_plus_3 + _typeSpace);
			}
		};
		String _join = IterableExtensions.<Symbol>join(this.getSymbols(), "\n", _function);
		String _plus_2 = (_plus_1 + _join);
		String _plus_3 = (_plus_2 + "\n\tDefault Type Configurations:\n");
		TypeConfigurationSpace _defaultTypeConfigs = this.getDefaultTypeConfigs();
		String _plus_4 = (_plus_3 + _defaultTypeConfigs);
		return (_plus_4 + "\n");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TypeexplorationPackage.SOLUTION_SPACE__EXPLORED_SYMBOLS:
				return ((InternalEList<?>)getExploredSymbols()).basicRemove(otherEnd, msgs);
			case TypeexplorationPackage.SOLUTION_SPACE__DEFAULT_TYPE_CONFIGS:
				return basicSetDefaultTypeConfigs(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TypeexplorationPackage.SOLUTION_SPACE__PROJECT:
				if (resolve) return getProject();
				return basicGetProject();
			case TypeexplorationPackage.SOLUTION_SPACE__EXPLORED_SYMBOLS:
				if (coreType) return getExploredSymbols();
				else return getExploredSymbols().map();
			case TypeexplorationPackage.SOLUTION_SPACE__DEFAULT_TYPE_CONFIGS:
				return getDefaultTypeConfigs();
			case TypeexplorationPackage.SOLUTION_SPACE__ANALYZER:
				return getAnalyzer();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TypeexplorationPackage.SOLUTION_SPACE__PROJECT:
				setProject((GecosProject)newValue);
				return;
			case TypeexplorationPackage.SOLUTION_SPACE__EXPLORED_SYMBOLS:
				((EStructuralFeature.Setting)getExploredSymbols()).set(newValue);
				return;
			case TypeexplorationPackage.SOLUTION_SPACE__DEFAULT_TYPE_CONFIGS:
				setDefaultTypeConfigs((TypeConfigurationSpace)newValue);
				return;
			case TypeexplorationPackage.SOLUTION_SPACE__ANALYZER:
				setAnalyzer((SolutionSpaceAnalyzer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TypeexplorationPackage.SOLUTION_SPACE__PROJECT:
				setProject((GecosProject)null);
				return;
			case TypeexplorationPackage.SOLUTION_SPACE__EXPLORED_SYMBOLS:
				getExploredSymbols().clear();
				return;
			case TypeexplorationPackage.SOLUTION_SPACE__DEFAULT_TYPE_CONFIGS:
				setDefaultTypeConfigs((TypeConfigurationSpace)null);
				return;
			case TypeexplorationPackage.SOLUTION_SPACE__ANALYZER:
				setAnalyzer(ANALYZER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TypeexplorationPackage.SOLUTION_SPACE__PROJECT:
				return project != null;
			case TypeexplorationPackage.SOLUTION_SPACE__EXPLORED_SYMBOLS:
				return exploredSymbols != null && !exploredSymbols.isEmpty();
			case TypeexplorationPackage.SOLUTION_SPACE__DEFAULT_TYPE_CONFIGS:
				return defaultTypeConfigs != null;
			case TypeexplorationPackage.SOLUTION_SPACE__ANALYZER:
				return ANALYZER_EDEFAULT == null ? analyzer != null : !ANALYZER_EDEFAULT.equals(analyzer);
		}
		return super.eIsSet(featureID);
	}

} //SolutionSpaceImpl
