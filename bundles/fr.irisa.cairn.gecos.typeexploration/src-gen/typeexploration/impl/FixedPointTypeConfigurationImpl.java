/**
 */
package typeexploration.impl;

import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import typeexploration.FixedPointTypeConfiguration;
import typeexploration.TypeexplorationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fixed Point Type Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.impl.FixedPointTypeConfigurationImpl#getIntegerWidthValues <em>Integer Width Values</em>}</li>
 *   <li>{@link typeexploration.impl.FixedPointTypeConfigurationImpl#getQuantificationMode <em>Quantification Mode</em>}</li>
 *   <li>{@link typeexploration.impl.FixedPointTypeConfigurationImpl#getOverflowMode <em>Overflow Mode</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FixedPointTypeConfigurationImpl extends NumberTypeConfigurationImpl implements FixedPointTypeConfiguration {
	/**
	 * The cached value of the '{@link #getIntegerWidthValues() <em>Integer Width Values</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegerWidthValues()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> integerWidthValues;

	/**
	 * The cached value of the '{@link #getQuantificationMode() <em>Quantification Mode</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantificationMode()
	 * @generated
	 * @ordered
	 */
	protected EList<QuantificationMode> quantificationMode;

	/**
	 * The cached value of the '{@link #getOverflowMode() <em>Overflow Mode</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverflowMode()
	 * @generated
	 * @ordered
	 */
	protected EList<OverflowMode> overflowMode;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FixedPointTypeConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypeexplorationPackage.Literals.FIXED_POINT_TYPE_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getIntegerWidthValues() {
		if (integerWidthValues == null) {
			integerWidthValues = new EDataTypeUniqueEList<Integer>(Integer.class, this, TypeexplorationPackage.FIXED_POINT_TYPE_CONFIGURATION__INTEGER_WIDTH_VALUES);
		}
		return integerWidthValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QuantificationMode> getQuantificationMode() {
		if (quantificationMode == null) {
			quantificationMode = new EDataTypeUniqueEList<QuantificationMode>(QuantificationMode.class, this, TypeexplorationPackage.FIXED_POINT_TYPE_CONFIGURATION__QUANTIFICATION_MODE);
		}
		return quantificationMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OverflowMode> getOverflowMode() {
		if (overflowMode == null) {
			overflowMode = new EDataTypeUniqueEList<OverflowMode>(OverflowMode.class, this, TypeexplorationPackage.FIXED_POINT_TYPE_CONFIGURATION__OVERFLOW_MODE);
		}
		return overflowMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixedPointTypeConfiguration mergeIn(final FixedPointTypeConfiguration c) {
		super.mergeIn(c);
		this.getIntegerWidthValues().addAll(c.getIntegerWidthValues());
		this.getQuantificationMode().addAll(c.getQuantificationMode());
		this.getOverflowMode().addAll(c.getOverflowMode());
		return this;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		EList<Boolean> _signed = this.getSigned();
		String _plus = ("Fixed-point: S=" + _signed);
		String _plus_1 = (_plus + " W=");
		EList<Integer> _totalWidthValues = this.getTotalWidthValues();
		String _plus_2 = (_plus_1 + _totalWidthValues);
		String _plus_3 = (_plus_2 + " I=");
		EList<Integer> _integerWidthValues = this.getIntegerWidthValues();
		return (_plus_3 + _integerWidthValues);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TypeexplorationPackage.FIXED_POINT_TYPE_CONFIGURATION__INTEGER_WIDTH_VALUES:
				return getIntegerWidthValues();
			case TypeexplorationPackage.FIXED_POINT_TYPE_CONFIGURATION__QUANTIFICATION_MODE:
				return getQuantificationMode();
			case TypeexplorationPackage.FIXED_POINT_TYPE_CONFIGURATION__OVERFLOW_MODE:
				return getOverflowMode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TypeexplorationPackage.FIXED_POINT_TYPE_CONFIGURATION__INTEGER_WIDTH_VALUES:
				getIntegerWidthValues().clear();
				getIntegerWidthValues().addAll((Collection<? extends Integer>)newValue);
				return;
			case TypeexplorationPackage.FIXED_POINT_TYPE_CONFIGURATION__QUANTIFICATION_MODE:
				getQuantificationMode().clear();
				getQuantificationMode().addAll((Collection<? extends QuantificationMode>)newValue);
				return;
			case TypeexplorationPackage.FIXED_POINT_TYPE_CONFIGURATION__OVERFLOW_MODE:
				getOverflowMode().clear();
				getOverflowMode().addAll((Collection<? extends OverflowMode>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TypeexplorationPackage.FIXED_POINT_TYPE_CONFIGURATION__INTEGER_WIDTH_VALUES:
				getIntegerWidthValues().clear();
				return;
			case TypeexplorationPackage.FIXED_POINT_TYPE_CONFIGURATION__QUANTIFICATION_MODE:
				getQuantificationMode().clear();
				return;
			case TypeexplorationPackage.FIXED_POINT_TYPE_CONFIGURATION__OVERFLOW_MODE:
				getOverflowMode().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TypeexplorationPackage.FIXED_POINT_TYPE_CONFIGURATION__INTEGER_WIDTH_VALUES:
				return integerWidthValues != null && !integerWidthValues.isEmpty();
			case TypeexplorationPackage.FIXED_POINT_TYPE_CONFIGURATION__QUANTIFICATION_MODE:
				return quantificationMode != null && !quantificationMode.isEmpty();
			case TypeexplorationPackage.FIXED_POINT_TYPE_CONFIGURATION__OVERFLOW_MODE:
				return overflowMode != null && !overflowMode.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //FixedPointTypeConfigurationImpl
