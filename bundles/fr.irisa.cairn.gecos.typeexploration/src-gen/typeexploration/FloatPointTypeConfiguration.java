/**
 */
package typeexploration;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Float Point Type Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.FloatPointTypeConfiguration#getExponentWidthValues <em>Exponent Width Values</em>}</li>
 *   <li>{@link typeexploration.FloatPointTypeConfiguration#getBiasValues <em>Bias Values</em>}</li>
 * </ul>
 *
 * @see typeexploration.TypeexplorationPackage#getFloatPointTypeConfiguration()
 * @model
 * @generated
 */
public interface FloatPointTypeConfiguration extends NumberTypeConfiguration {
	/**
	 * Returns the value of the '<em><b>Exponent Width Values</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exponent Width Values</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exponent Width Values</em>' attribute list.
	 * @see typeexploration.TypeexplorationPackage#getFloatPointTypeConfiguration_ExponentWidthValues()
	 * @model
	 * @generated
	 */
	EList<Integer> getExponentWidthValues();

	/**
	 * Returns the value of the '<em><b>Bias Values</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bias Values</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bias Values</em>' attribute list.
	 * @see typeexploration.TypeexplorationPackage#getFloatPointTypeConfiguration_BiasValues()
	 * @model
	 * @generated
	 */
	EList<Integer> getBiasValues();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Add the values specified by c to this.
	 * @return this
	 * <!-- end-model-doc -->
	 * @model unique="false" cUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='super.mergeIn(c);\nthis.getExponentWidthValues().addAll(c.getExponentWidthValues());\nthis.getBiasValues().addAll(c.getBiasValues());\nreturn this;'"
	 * @generated
	 */
	FloatPointTypeConfiguration mergeIn(FloatPointTypeConfiguration c);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%java.lang.Boolean%&gt;&gt; _signed = this.getSigned();\n&lt;%java.lang.String%&gt; _plus = (\"Floating-point: S=\" + _signed);\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \" W=\");\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%java.lang.Integer%&gt;&gt; _totalWidthValues = this.getTotalWidthValues();\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + _totalWidthValues);\n&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + \" E=\");\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%java.lang.Integer%&gt;&gt; _exponentWidthValues = this.getExponentWidthValues();\nreturn (_plus_3 + _exponentWidthValues);'"
	 * @generated
	 */
	String toString();

} // FloatPointTypeConfiguration
