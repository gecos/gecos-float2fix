/**
 */
package typeexploration;

import gecos.types.OverflowMode;
import gecos.types.QuantificationMode;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fixed Point Type Param</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.FixedPointTypeParam#isSigned <em>Signed</em>}</li>
 *   <li>{@link typeexploration.FixedPointTypeParam#getTotalWidth <em>Total Width</em>}</li>
 *   <li>{@link typeexploration.FixedPointTypeParam#getIntegerWidth <em>Integer Width</em>}</li>
 *   <li>{@link typeexploration.FixedPointTypeParam#getQuantificationMode <em>Quantification Mode</em>}</li>
 *   <li>{@link typeexploration.FixedPointTypeParam#getOverflowMode <em>Overflow Mode</em>}</li>
 * </ul>
 *
 * @see typeexploration.TypeexplorationPackage#getFixedPointTypeParam()
 * @model
 * @generated
 */
public interface FixedPointTypeParam extends TypeParam {
	/**
	 * Returns the value of the '<em><b>Signed</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signed</em>' attribute.
	 * @see #setSigned(boolean)
	 * @see typeexploration.TypeexplorationPackage#getFixedPointTypeParam_Signed()
	 * @model default="true" unique="false"
	 * @generated
	 */
	boolean isSigned();

	/**
	 * Sets the value of the '{@link typeexploration.FixedPointTypeParam#isSigned <em>Signed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signed</em>' attribute.
	 * @see #isSigned()
	 * @generated
	 */
	void setSigned(boolean value);

	/**
	 * Returns the value of the '<em><b>Total Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Total Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Total Width</em>' attribute.
	 * @see #setTotalWidth(int)
	 * @see typeexploration.TypeexplorationPackage#getFixedPointTypeParam_TotalWidth()
	 * @model unique="false"
	 * @generated
	 */
	int getTotalWidth();

	/**
	 * Sets the value of the '{@link typeexploration.FixedPointTypeParam#getTotalWidth <em>Total Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Total Width</em>' attribute.
	 * @see #getTotalWidth()
	 * @generated
	 */
	void setTotalWidth(int value);

	/**
	 * Returns the value of the '<em><b>Integer Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Width</em>' attribute.
	 * @see #setIntegerWidth(int)
	 * @see typeexploration.TypeexplorationPackage#getFixedPointTypeParam_IntegerWidth()
	 * @model unique="false"
	 * @generated
	 */
	int getIntegerWidth();

	/**
	 * Sets the value of the '{@link typeexploration.FixedPointTypeParam#getIntegerWidth <em>Integer Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integer Width</em>' attribute.
	 * @see #getIntegerWidth()
	 * @generated
	 */
	void setIntegerWidth(int value);

	/**
	 * Returns the value of the '<em><b>Quantification Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link gecos.types.QuantificationMode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantification Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantification Mode</em>' attribute.
	 * @see gecos.types.QuantificationMode
	 * @see #setQuantificationMode(QuantificationMode)
	 * @see typeexploration.TypeexplorationPackage#getFixedPointTypeParam_QuantificationMode()
	 * @model unique="false"
	 * @generated
	 */
	QuantificationMode getQuantificationMode();

	/**
	 * Sets the value of the '{@link typeexploration.FixedPointTypeParam#getQuantificationMode <em>Quantification Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantification Mode</em>' attribute.
	 * @see gecos.types.QuantificationMode
	 * @see #getQuantificationMode()
	 * @generated
	 */
	void setQuantificationMode(QuantificationMode value);

	/**
	 * Returns the value of the '<em><b>Overflow Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link gecos.types.OverflowMode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overflow Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overflow Mode</em>' attribute.
	 * @see gecos.types.OverflowMode
	 * @see #setOverflowMode(OverflowMode)
	 * @see typeexploration.TypeexplorationPackage#getFixedPointTypeParam_OverflowMode()
	 * @model unique="false"
	 * @generated
	 */
	OverflowMode getOverflowMode();

	/**
	 * Sets the value of the '{@link typeexploration.FixedPointTypeParam#getOverflowMode <em>Overflow Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Overflow Mode</em>' attribute.
	 * @see gecos.types.OverflowMode
	 * @see #getOverflowMode()
	 * @generated
	 */
	void setOverflowMode(OverflowMode value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" otherUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _xifexpression = false;\nif ((other instanceof &lt;%typeexploration.FixedPointTypeParam%&gt;))\n{\n\t_xifexpression = (((((this.isSigned() == ((&lt;%typeexploration.FixedPointTypeParam%&gt;)other).isSigned()) &amp;&amp; \n\t\t(this.getTotalWidth() == ((&lt;%typeexploration.FixedPointTypeParam%&gt;)other).getTotalWidth())) &amp;&amp; \n\t\t(this.getIntegerWidth() == ((&lt;%typeexploration.FixedPointTypeParam%&gt;)other).getIntegerWidth())) &amp;&amp; \n\t\t&lt;%com.google.common.base.Objects%&gt;.equal(this.getQuantificationMode(), ((&lt;%typeexploration.FixedPointTypeParam%&gt;)other).getQuantificationMode())) &amp;&amp; \n\t\t&lt;%com.google.common.base.Objects%&gt;.equal(this.getOverflowMode(), ((&lt;%typeexploration.FixedPointTypeParam%&gt;)other).getOverflowMode()));\n}\nelse\n{\n\t_xifexpression = false;\n}\nreturn _xifexpression;'"
	 * @generated
	 */
	boolean equals(TypeParam other);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _xifexpression = null;\nboolean _isSigned = this.isSigned();\nboolean _not = (!_isSigned);\nif (_not)\n{\n\t_xifexpression = \"un\";\n}\nelse\n{\n\t_xifexpression = \"\";\n}\n&lt;%java.lang.String%&gt; _plus = (\"Fixed-point type: \" + _xifexpression);\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \"signed W=\");\nint _totalWidth = this.getTotalWidth();\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + &lt;%java.lang.Integer%&gt;.valueOf(_totalWidth));\n&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + \" I=\");\nint _integerWidth = this.getIntegerWidth();\n&lt;%java.lang.String%&gt; _plus_4 = (_plus_3 + &lt;%java.lang.Integer%&gt;.valueOf(_integerWidth));\n&lt;%java.lang.String%&gt; _plus_5 = (_plus_4 + \" Q=\");\n&lt;%gecos.types.QuantificationMode%&gt; _quantificationMode = this.getQuantificationMode();\n&lt;%java.lang.String%&gt; _plus_6 = (_plus_5 + _quantificationMode);\n&lt;%java.lang.String%&gt; _plus_7 = (_plus_6 + \" O=\");\n&lt;%gecos.types.OverflowMode%&gt; _overflowMode = this.getOverflowMode();\nreturn (_plus_7 + _overflowMode);'"
	 * @generated
	 */
	String toString();

} // FixedPointTypeParam
