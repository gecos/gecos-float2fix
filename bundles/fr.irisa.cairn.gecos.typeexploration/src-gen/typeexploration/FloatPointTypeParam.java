/**
 */
package typeexploration;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Float Point Type Param</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.FloatPointTypeParam#isSigned <em>Signed</em>}</li>
 *   <li>{@link typeexploration.FloatPointTypeParam#getTotalWidth <em>Total Width</em>}</li>
 *   <li>{@link typeexploration.FloatPointTypeParam#getExponentWidth <em>Exponent Width</em>}</li>
 *   <li>{@link typeexploration.FloatPointTypeParam#getExponentBias <em>Exponent Bias</em>}</li>
 * </ul>
 *
 * @see typeexploration.TypeexplorationPackage#getFloatPointTypeParam()
 * @model
 * @generated
 */
public interface FloatPointTypeParam extends TypeParam {
	/**
	 * Returns the value of the '<em><b>Signed</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signed</em>' attribute.
	 * @see #setSigned(boolean)
	 * @see typeexploration.TypeexplorationPackage#getFloatPointTypeParam_Signed()
	 * @model default="true" unique="false"
	 * @generated
	 */
	boolean isSigned();

	/**
	 * Sets the value of the '{@link typeexploration.FloatPointTypeParam#isSigned <em>Signed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signed</em>' attribute.
	 * @see #isSigned()
	 * @generated
	 */
	void setSigned(boolean value);

	/**
	 * Returns the value of the '<em><b>Total Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Total Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Total Width</em>' attribute.
	 * @see #setTotalWidth(int)
	 * @see typeexploration.TypeexplorationPackage#getFloatPointTypeParam_TotalWidth()
	 * @model unique="false"
	 * @generated
	 */
	int getTotalWidth();

	/**
	 * Sets the value of the '{@link typeexploration.FloatPointTypeParam#getTotalWidth <em>Total Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Total Width</em>' attribute.
	 * @see #getTotalWidth()
	 * @generated
	 */
	void setTotalWidth(int value);

	/**
	 * Returns the value of the '<em><b>Exponent Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exponent Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exponent Width</em>' attribute.
	 * @see #setExponentWidth(int)
	 * @see typeexploration.TypeexplorationPackage#getFloatPointTypeParam_ExponentWidth()
	 * @model unique="false"
	 * @generated
	 */
	int getExponentWidth();

	/**
	 * Sets the value of the '{@link typeexploration.FloatPointTypeParam#getExponentWidth <em>Exponent Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exponent Width</em>' attribute.
	 * @see #getExponentWidth()
	 * @generated
	 */
	void setExponentWidth(int value);

	/**
	 * Returns the value of the '<em><b>Exponent Bias</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exponent Bias</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exponent Bias</em>' attribute.
	 * @see #setExponentBias(int)
	 * @see typeexploration.TypeexplorationPackage#getFloatPointTypeParam_ExponentBias()
	 * @model unique="false"
	 * @generated
	 */
	int getExponentBias();

	/**
	 * Sets the value of the '{@link typeexploration.FloatPointTypeParam#getExponentBias <em>Exponent Bias</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exponent Bias</em>' attribute.
	 * @see #getExponentBias()
	 * @generated
	 */
	void setExponentBias(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" otherUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _xifexpression = false;\nif ((other instanceof &lt;%typeexploration.FloatPointTypeParam%&gt;))\n{\n\t_xifexpression = ((((this.isSigned() == ((&lt;%typeexploration.FloatPointTypeParam%&gt;)other).isSigned()) &amp;&amp; \n\t\t(this.getTotalWidth() == ((&lt;%typeexploration.FloatPointTypeParam%&gt;)other).getTotalWidth())) &amp;&amp; \n\t\t(this.getExponentWidth() == ((&lt;%typeexploration.FloatPointTypeParam%&gt;)other).getExponentWidth())) &amp;&amp; \n\t\t(this.getExponentBias() == ((&lt;%typeexploration.FloatPointTypeParam%&gt;)other).getExponentBias()));\n}\nelse\n{\n\t_xifexpression = false;\n}\nreturn _xifexpression;'"
	 * @generated
	 */
	boolean equals(TypeParam other);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _xifexpression = null;\nboolean _isSigned = this.isSigned();\nboolean _not = (!_isSigned);\nif (_not)\n{\n\t_xifexpression = \"un\";\n}\nelse\n{\n\t_xifexpression = \"\";\n}\n&lt;%java.lang.String%&gt; _plus = (\"Floating-point type:\" + _xifexpression);\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \"signed W=\");\nint _totalWidth = this.getTotalWidth();\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + &lt;%java.lang.Integer%&gt;.valueOf(_totalWidth));\n&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + \" E=\");\nint _exponentWidth = this.getExponentWidth();\nreturn (_plus_3 + &lt;%java.lang.Integer%&gt;.valueOf(_exponentWidth));'"
	 * @generated
	 */
	String toString();

} // FloatPointTypeParam
