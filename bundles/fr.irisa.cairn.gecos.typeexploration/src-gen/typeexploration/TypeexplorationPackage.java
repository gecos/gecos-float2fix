/**
 */
package typeexploration;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see typeexploration.TypeexplorationFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel operationReflection='false'"
 * @generated
 */
public interface TypeexplorationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "typeexploration";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.gecos.org/float2fix";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "typeexploration";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TypeexplorationPackage eINSTANCE = typeexploration.impl.TypeexplorationPackageImpl.init();

	/**
	 * The meta object id for the '{@link typeexploration.impl.TypeParamImpl <em>Type Param</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.impl.TypeParamImpl
	 * @see typeexploration.impl.TypeexplorationPackageImpl#getTypeParam()
	 * @generated
	 */
	int TYPE_PARAM = 0;

	/**
	 * The number of structural features of the '<em>Type Param</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_PARAM_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link typeexploration.impl.FixedPointTypeParamImpl <em>Fixed Point Type Param</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.impl.FixedPointTypeParamImpl
	 * @see typeexploration.impl.TypeexplorationPackageImpl#getFixedPointTypeParam()
	 * @generated
	 */
	int FIXED_POINT_TYPE_PARAM = 1;

	/**
	 * The feature id for the '<em><b>Signed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_TYPE_PARAM__SIGNED = TYPE_PARAM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Total Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_TYPE_PARAM__TOTAL_WIDTH = TYPE_PARAM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Integer Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_TYPE_PARAM__INTEGER_WIDTH = TYPE_PARAM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Quantification Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_TYPE_PARAM__QUANTIFICATION_MODE = TYPE_PARAM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Overflow Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_TYPE_PARAM__OVERFLOW_MODE = TYPE_PARAM_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Fixed Point Type Param</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_TYPE_PARAM_FEATURE_COUNT = TYPE_PARAM_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link typeexploration.impl.FloatPointTypeParamImpl <em>Float Point Type Param</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.impl.FloatPointTypeParamImpl
	 * @see typeexploration.impl.TypeexplorationPackageImpl#getFloatPointTypeParam()
	 * @generated
	 */
	int FLOAT_POINT_TYPE_PARAM = 2;

	/**
	 * The feature id for the '<em><b>Signed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_POINT_TYPE_PARAM__SIGNED = TYPE_PARAM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Total Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_POINT_TYPE_PARAM__TOTAL_WIDTH = TYPE_PARAM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Exponent Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_POINT_TYPE_PARAM__EXPONENT_WIDTH = TYPE_PARAM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Exponent Bias</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_POINT_TYPE_PARAM__EXPONENT_BIAS = TYPE_PARAM_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Float Point Type Param</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_POINT_TYPE_PARAM_FEATURE_COUNT = TYPE_PARAM_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link typeexploration.impl.GenericTypeParamImpl <em>Generic Type Param</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.impl.GenericTypeParamImpl
	 * @see typeexploration.impl.TypeexplorationPackageImpl#getGenericTypeParam()
	 * @generated
	 */
	int GENERIC_TYPE_PARAM = 3;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_TYPE_PARAM__TYPE = TYPE_PARAM_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Generic Type Param</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_TYPE_PARAM_FEATURE_COUNT = TYPE_PARAM_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link typeexploration.impl.TypeConstraintImpl <em>Type Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.impl.TypeConstraintImpl
	 * @see typeexploration.impl.TypeexplorationPackageImpl#getTypeConstraint()
	 * @generated
	 */
	int TYPE_CONSTRAINT = 4;

	/**
	 * The number of structural features of the '<em>Type Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_CONSTRAINT_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link typeexploration.impl.SameTypeConstraintImpl <em>Same Type Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.impl.SameTypeConstraintImpl
	 * @see typeexploration.impl.TypeexplorationPackageImpl#getSameTypeConstraint()
	 * @generated
	 */
	int SAME_TYPE_CONSTRAINT = 5;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_TYPE_CONSTRAINT__SYMBOL = TYPE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Same Type Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAME_TYPE_CONSTRAINT_FEATURE_COUNT = TYPE_CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link typeexploration.TypeConfiguration <em>Type Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.TypeConfiguration
	 * @see typeexploration.impl.TypeexplorationPackageImpl#getTypeConfiguration()
	 * @generated
	 */
	int TYPE_CONFIGURATION = 6;

	/**
	 * The number of structural features of the '<em>Type Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_CONFIGURATION_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link typeexploration.impl.NumberTypeConfigurationImpl <em>Number Type Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.impl.NumberTypeConfigurationImpl
	 * @see typeexploration.impl.TypeexplorationPackageImpl#getNumberTypeConfiguration()
	 * @generated
	 */
	int NUMBER_TYPE_CONFIGURATION = 7;

	/**
	 * The feature id for the '<em><b>Signed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_TYPE_CONFIGURATION__SIGNED = TYPE_CONFIGURATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Total Width Values</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_TYPE_CONFIGURATION__TOTAL_WIDTH_VALUES = TYPE_CONFIGURATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Number Type Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_TYPE_CONFIGURATION_FEATURE_COUNT = TYPE_CONFIGURATION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link typeexploration.impl.FixedPointTypeConfigurationImpl <em>Fixed Point Type Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.impl.FixedPointTypeConfigurationImpl
	 * @see typeexploration.impl.TypeexplorationPackageImpl#getFixedPointTypeConfiguration()
	 * @generated
	 */
	int FIXED_POINT_TYPE_CONFIGURATION = 8;

	/**
	 * The feature id for the '<em><b>Signed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_TYPE_CONFIGURATION__SIGNED = NUMBER_TYPE_CONFIGURATION__SIGNED;

	/**
	 * The feature id for the '<em><b>Total Width Values</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_TYPE_CONFIGURATION__TOTAL_WIDTH_VALUES = NUMBER_TYPE_CONFIGURATION__TOTAL_WIDTH_VALUES;

	/**
	 * The feature id for the '<em><b>Integer Width Values</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_TYPE_CONFIGURATION__INTEGER_WIDTH_VALUES = NUMBER_TYPE_CONFIGURATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Quantification Mode</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_TYPE_CONFIGURATION__QUANTIFICATION_MODE = NUMBER_TYPE_CONFIGURATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Overflow Mode</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_TYPE_CONFIGURATION__OVERFLOW_MODE = NUMBER_TYPE_CONFIGURATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Fixed Point Type Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_TYPE_CONFIGURATION_FEATURE_COUNT = NUMBER_TYPE_CONFIGURATION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link typeexploration.impl.FloatPointTypeConfigurationImpl <em>Float Point Type Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.impl.FloatPointTypeConfigurationImpl
	 * @see typeexploration.impl.TypeexplorationPackageImpl#getFloatPointTypeConfiguration()
	 * @generated
	 */
	int FLOAT_POINT_TYPE_CONFIGURATION = 9;

	/**
	 * The feature id for the '<em><b>Signed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_POINT_TYPE_CONFIGURATION__SIGNED = NUMBER_TYPE_CONFIGURATION__SIGNED;

	/**
	 * The feature id for the '<em><b>Total Width Values</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_POINT_TYPE_CONFIGURATION__TOTAL_WIDTH_VALUES = NUMBER_TYPE_CONFIGURATION__TOTAL_WIDTH_VALUES;

	/**
	 * The feature id for the '<em><b>Exponent Width Values</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_POINT_TYPE_CONFIGURATION__EXPONENT_WIDTH_VALUES = NUMBER_TYPE_CONFIGURATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Bias Values</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_POINT_TYPE_CONFIGURATION__BIAS_VALUES = NUMBER_TYPE_CONFIGURATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Float Point Type Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_POINT_TYPE_CONFIGURATION_FEATURE_COUNT = NUMBER_TYPE_CONFIGURATION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link typeexploration.impl.TypeConfigurationSpaceImpl <em>Type Configuration Space</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.impl.TypeConfigurationSpaceImpl
	 * @see typeexploration.impl.TypeexplorationPackageImpl#getTypeConfigurationSpace()
	 * @generated
	 */
	int TYPE_CONFIGURATION_SPACE = 10;

	/**
	 * The feature id for the '<em><b>Fixed Pt Congurations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_CONFIGURATION_SPACE__FIXED_PT_CONGURATIONS = 0;

	/**
	 * The feature id for the '<em><b>Float Pt Congurations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_CONFIGURATION_SPACE__FLOAT_PT_CONGURATIONS = 1;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_CONFIGURATION_SPACE__CONSTRAINTS = 2;

	/**
	 * The number of structural features of the '<em>Type Configuration Space</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_CONFIGURATION_SPACE_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link typeexploration.impl.SymbolToTypeConfigurationsEntryImpl <em>Symbol To Type Configurations Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.impl.SymbolToTypeConfigurationsEntryImpl
	 * @see typeexploration.impl.TypeexplorationPackageImpl#getSymbolToTypeConfigurationsEntry()
	 * @generated
	 */
	int SYMBOL_TO_TYPE_CONFIGURATIONS_ENTRY = 11;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_TO_TYPE_CONFIGURATIONS_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_TO_TYPE_CONFIGURATIONS_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Symbol To Type Configurations Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_TO_TYPE_CONFIGURATIONS_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link typeexploration.impl.SolutionSpaceImpl <em>Solution Space</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.impl.SolutionSpaceImpl
	 * @see typeexploration.impl.TypeexplorationPackageImpl#getSolutionSpace()
	 * @generated
	 */
	int SOLUTION_SPACE = 12;

	/**
	 * The feature id for the '<em><b>Project</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE__PROJECT = 0;

	/**
	 * The feature id for the '<em><b>Explored Symbols</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE__EXPLORED_SYMBOLS = 1;

	/**
	 * The feature id for the '<em><b>Default Type Configs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE__DEFAULT_TYPE_CONFIGS = 2;

	/**
	 * The feature id for the '<em><b>Analyzer</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE__ANALYZER = 3;

	/**
	 * The number of structural features of the '<em>Solution Space</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '<em>Solution Space Analyzer</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.gecos.typeexploration.solutionspace.SolutionSpaceAnalyzer
	 * @see typeexploration.impl.TypeexplorationPackageImpl#getSolutionSpaceAnalyzer()
	 * @generated
	 */
	int SOLUTION_SPACE_ANALYZER = 13;


	/**
	 * Returns the meta object for class '{@link typeexploration.TypeParam <em>Type Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Param</em>'.
	 * @see typeexploration.TypeParam
	 * @generated
	 */
	EClass getTypeParam();

	/**
	 * Returns the meta object for class '{@link typeexploration.FixedPointTypeParam <em>Fixed Point Type Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fixed Point Type Param</em>'.
	 * @see typeexploration.FixedPointTypeParam
	 * @generated
	 */
	EClass getFixedPointTypeParam();

	/**
	 * Returns the meta object for the attribute '{@link typeexploration.FixedPointTypeParam#isSigned <em>Signed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Signed</em>'.
	 * @see typeexploration.FixedPointTypeParam#isSigned()
	 * @see #getFixedPointTypeParam()
	 * @generated
	 */
	EAttribute getFixedPointTypeParam_Signed();

	/**
	 * Returns the meta object for the attribute '{@link typeexploration.FixedPointTypeParam#getTotalWidth <em>Total Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Total Width</em>'.
	 * @see typeexploration.FixedPointTypeParam#getTotalWidth()
	 * @see #getFixedPointTypeParam()
	 * @generated
	 */
	EAttribute getFixedPointTypeParam_TotalWidth();

	/**
	 * Returns the meta object for the attribute '{@link typeexploration.FixedPointTypeParam#getIntegerWidth <em>Integer Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Integer Width</em>'.
	 * @see typeexploration.FixedPointTypeParam#getIntegerWidth()
	 * @see #getFixedPointTypeParam()
	 * @generated
	 */
	EAttribute getFixedPointTypeParam_IntegerWidth();

	/**
	 * Returns the meta object for the attribute '{@link typeexploration.FixedPointTypeParam#getQuantificationMode <em>Quantification Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantification Mode</em>'.
	 * @see typeexploration.FixedPointTypeParam#getQuantificationMode()
	 * @see #getFixedPointTypeParam()
	 * @generated
	 */
	EAttribute getFixedPointTypeParam_QuantificationMode();

	/**
	 * Returns the meta object for the attribute '{@link typeexploration.FixedPointTypeParam#getOverflowMode <em>Overflow Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Overflow Mode</em>'.
	 * @see typeexploration.FixedPointTypeParam#getOverflowMode()
	 * @see #getFixedPointTypeParam()
	 * @generated
	 */
	EAttribute getFixedPointTypeParam_OverflowMode();

	/**
	 * Returns the meta object for class '{@link typeexploration.FloatPointTypeParam <em>Float Point Type Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Float Point Type Param</em>'.
	 * @see typeexploration.FloatPointTypeParam
	 * @generated
	 */
	EClass getFloatPointTypeParam();

	/**
	 * Returns the meta object for the attribute '{@link typeexploration.FloatPointTypeParam#isSigned <em>Signed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Signed</em>'.
	 * @see typeexploration.FloatPointTypeParam#isSigned()
	 * @see #getFloatPointTypeParam()
	 * @generated
	 */
	EAttribute getFloatPointTypeParam_Signed();

	/**
	 * Returns the meta object for the attribute '{@link typeexploration.FloatPointTypeParam#getTotalWidth <em>Total Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Total Width</em>'.
	 * @see typeexploration.FloatPointTypeParam#getTotalWidth()
	 * @see #getFloatPointTypeParam()
	 * @generated
	 */
	EAttribute getFloatPointTypeParam_TotalWidth();

	/**
	 * Returns the meta object for the attribute '{@link typeexploration.FloatPointTypeParam#getExponentWidth <em>Exponent Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Exponent Width</em>'.
	 * @see typeexploration.FloatPointTypeParam#getExponentWidth()
	 * @see #getFloatPointTypeParam()
	 * @generated
	 */
	EAttribute getFloatPointTypeParam_ExponentWidth();

	/**
	 * Returns the meta object for the attribute '{@link typeexploration.FloatPointTypeParam#getExponentBias <em>Exponent Bias</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Exponent Bias</em>'.
	 * @see typeexploration.FloatPointTypeParam#getExponentBias()
	 * @see #getFloatPointTypeParam()
	 * @generated
	 */
	EAttribute getFloatPointTypeParam_ExponentBias();

	/**
	 * Returns the meta object for class '{@link typeexploration.GenericTypeParam <em>Generic Type Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Generic Type Param</em>'.
	 * @see typeexploration.GenericTypeParam
	 * @generated
	 */
	EClass getGenericTypeParam();

	/**
	 * Returns the meta object for the reference '{@link typeexploration.GenericTypeParam#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see typeexploration.GenericTypeParam#getType()
	 * @see #getGenericTypeParam()
	 * @generated
	 */
	EReference getGenericTypeParam_Type();

	/**
	 * Returns the meta object for class '{@link typeexploration.TypeConstraint <em>Type Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Constraint</em>'.
	 * @see typeexploration.TypeConstraint
	 * @generated
	 */
	EClass getTypeConstraint();

	/**
	 * Returns the meta object for class '{@link typeexploration.SameTypeConstraint <em>Same Type Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Same Type Constraint</em>'.
	 * @see typeexploration.SameTypeConstraint
	 * @generated
	 */
	EClass getSameTypeConstraint();

	/**
	 * Returns the meta object for the reference '{@link typeexploration.SameTypeConstraint#getSymbol <em>Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Symbol</em>'.
	 * @see typeexploration.SameTypeConstraint#getSymbol()
	 * @see #getSameTypeConstraint()
	 * @generated
	 */
	EReference getSameTypeConstraint_Symbol();

	/**
	 * Returns the meta object for class '{@link typeexploration.TypeConfiguration <em>Type Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Configuration</em>'.
	 * @see typeexploration.TypeConfiguration
	 * @generated
	 */
	EClass getTypeConfiguration();

	/**
	 * Returns the meta object for class '{@link typeexploration.NumberTypeConfiguration <em>Number Type Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Number Type Configuration</em>'.
	 * @see typeexploration.NumberTypeConfiguration
	 * @generated
	 */
	EClass getNumberTypeConfiguration();

	/**
	 * Returns the meta object for the attribute list '{@link typeexploration.NumberTypeConfiguration#getSigned <em>Signed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Signed</em>'.
	 * @see typeexploration.NumberTypeConfiguration#getSigned()
	 * @see #getNumberTypeConfiguration()
	 * @generated
	 */
	EAttribute getNumberTypeConfiguration_Signed();

	/**
	 * Returns the meta object for the attribute list '{@link typeexploration.NumberTypeConfiguration#getTotalWidthValues <em>Total Width Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Total Width Values</em>'.
	 * @see typeexploration.NumberTypeConfiguration#getTotalWidthValues()
	 * @see #getNumberTypeConfiguration()
	 * @generated
	 */
	EAttribute getNumberTypeConfiguration_TotalWidthValues();

	/**
	 * Returns the meta object for class '{@link typeexploration.FixedPointTypeConfiguration <em>Fixed Point Type Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fixed Point Type Configuration</em>'.
	 * @see typeexploration.FixedPointTypeConfiguration
	 * @generated
	 */
	EClass getFixedPointTypeConfiguration();

	/**
	 * Returns the meta object for the attribute list '{@link typeexploration.FixedPointTypeConfiguration#getIntegerWidthValues <em>Integer Width Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Integer Width Values</em>'.
	 * @see typeexploration.FixedPointTypeConfiguration#getIntegerWidthValues()
	 * @see #getFixedPointTypeConfiguration()
	 * @generated
	 */
	EAttribute getFixedPointTypeConfiguration_IntegerWidthValues();

	/**
	 * Returns the meta object for the attribute list '{@link typeexploration.FixedPointTypeConfiguration#getQuantificationMode <em>Quantification Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Quantification Mode</em>'.
	 * @see typeexploration.FixedPointTypeConfiguration#getQuantificationMode()
	 * @see #getFixedPointTypeConfiguration()
	 * @generated
	 */
	EAttribute getFixedPointTypeConfiguration_QuantificationMode();

	/**
	 * Returns the meta object for the attribute list '{@link typeexploration.FixedPointTypeConfiguration#getOverflowMode <em>Overflow Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Overflow Mode</em>'.
	 * @see typeexploration.FixedPointTypeConfiguration#getOverflowMode()
	 * @see #getFixedPointTypeConfiguration()
	 * @generated
	 */
	EAttribute getFixedPointTypeConfiguration_OverflowMode();

	/**
	 * Returns the meta object for class '{@link typeexploration.FloatPointTypeConfiguration <em>Float Point Type Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Float Point Type Configuration</em>'.
	 * @see typeexploration.FloatPointTypeConfiguration
	 * @generated
	 */
	EClass getFloatPointTypeConfiguration();

	/**
	 * Returns the meta object for the attribute list '{@link typeexploration.FloatPointTypeConfiguration#getExponentWidthValues <em>Exponent Width Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Exponent Width Values</em>'.
	 * @see typeexploration.FloatPointTypeConfiguration#getExponentWidthValues()
	 * @see #getFloatPointTypeConfiguration()
	 * @generated
	 */
	EAttribute getFloatPointTypeConfiguration_ExponentWidthValues();

	/**
	 * Returns the meta object for the attribute list '{@link typeexploration.FloatPointTypeConfiguration#getBiasValues <em>Bias Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Bias Values</em>'.
	 * @see typeexploration.FloatPointTypeConfiguration#getBiasValues()
	 * @see #getFloatPointTypeConfiguration()
	 * @generated
	 */
	EAttribute getFloatPointTypeConfiguration_BiasValues();

	/**
	 * Returns the meta object for class '{@link typeexploration.TypeConfigurationSpace <em>Type Configuration Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Configuration Space</em>'.
	 * @see typeexploration.TypeConfigurationSpace
	 * @generated
	 */
	EClass getTypeConfigurationSpace();

	/**
	 * Returns the meta object for the containment reference '{@link typeexploration.TypeConfigurationSpace#getFixedPtCongurations <em>Fixed Pt Congurations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Fixed Pt Congurations</em>'.
	 * @see typeexploration.TypeConfigurationSpace#getFixedPtCongurations()
	 * @see #getTypeConfigurationSpace()
	 * @generated
	 */
	EReference getTypeConfigurationSpace_FixedPtCongurations();

	/**
	 * Returns the meta object for the containment reference '{@link typeexploration.TypeConfigurationSpace#getFloatPtCongurations <em>Float Pt Congurations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Float Pt Congurations</em>'.
	 * @see typeexploration.TypeConfigurationSpace#getFloatPtCongurations()
	 * @see #getTypeConfigurationSpace()
	 * @generated
	 */
	EReference getTypeConfigurationSpace_FloatPtCongurations();

	/**
	 * Returns the meta object for the containment reference list '{@link typeexploration.TypeConfigurationSpace#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constraints</em>'.
	 * @see typeexploration.TypeConfigurationSpace#getConstraints()
	 * @see #getTypeConfigurationSpace()
	 * @generated
	 */
	EReference getTypeConfigurationSpace_Constraints();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Symbol To Type Configurations Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Symbol To Type Configurations Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="gecos.core.Symbol"
	 *        valueType="typeexploration.TypeConfigurationSpace" valueContainment="true"
	 * @generated
	 */
	EClass getSymbolToTypeConfigurationsEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getSymbolToTypeConfigurationsEntry()
	 * @generated
	 */
	EReference getSymbolToTypeConfigurationsEntry_Key();

	/**
	 * Returns the meta object for the containment reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getSymbolToTypeConfigurationsEntry()
	 * @generated
	 */
	EReference getSymbolToTypeConfigurationsEntry_Value();

	/**
	 * Returns the meta object for class '{@link typeexploration.SolutionSpace <em>Solution Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Solution Space</em>'.
	 * @see typeexploration.SolutionSpace
	 * @generated
	 */
	EClass getSolutionSpace();

	/**
	 * Returns the meta object for the reference '{@link typeexploration.SolutionSpace#getProject <em>Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Project</em>'.
	 * @see typeexploration.SolutionSpace#getProject()
	 * @see #getSolutionSpace()
	 * @generated
	 */
	EReference getSolutionSpace_Project();

	/**
	 * Returns the meta object for the map '{@link typeexploration.SolutionSpace#getExploredSymbols <em>Explored Symbols</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Explored Symbols</em>'.
	 * @see typeexploration.SolutionSpace#getExploredSymbols()
	 * @see #getSolutionSpace()
	 * @generated
	 */
	EReference getSolutionSpace_ExploredSymbols();

	/**
	 * Returns the meta object for the containment reference '{@link typeexploration.SolutionSpace#getDefaultTypeConfigs <em>Default Type Configs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Default Type Configs</em>'.
	 * @see typeexploration.SolutionSpace#getDefaultTypeConfigs()
	 * @see #getSolutionSpace()
	 * @generated
	 */
	EReference getSolutionSpace_DefaultTypeConfigs();

	/**
	 * Returns the meta object for the attribute '{@link typeexploration.SolutionSpace#getAnalyzer <em>Analyzer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Analyzer</em>'.
	 * @see typeexploration.SolutionSpace#getAnalyzer()
	 * @see #getSolutionSpace()
	 * @generated
	 */
	EAttribute getSolutionSpace_Analyzer();

	/**
	 * Returns the meta object for data type '{@link fr.irisa.cairn.gecos.typeexploration.solutionspace.SolutionSpaceAnalyzer <em>Solution Space Analyzer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Solution Space Analyzer</em>'.
	 * @see fr.irisa.cairn.gecos.typeexploration.solutionspace.SolutionSpaceAnalyzer
	 * @model instanceClass="fr.irisa.cairn.gecos.typeexploration.solutionspace.SolutionSpaceAnalyzer"
	 * @generated
	 */
	EDataType getSolutionSpaceAnalyzer();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TypeexplorationFactory getTypeexplorationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link typeexploration.impl.TypeParamImpl <em>Type Param</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.impl.TypeParamImpl
		 * @see typeexploration.impl.TypeexplorationPackageImpl#getTypeParam()
		 * @generated
		 */
		EClass TYPE_PARAM = eINSTANCE.getTypeParam();

		/**
		 * The meta object literal for the '{@link typeexploration.impl.FixedPointTypeParamImpl <em>Fixed Point Type Param</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.impl.FixedPointTypeParamImpl
		 * @see typeexploration.impl.TypeexplorationPackageImpl#getFixedPointTypeParam()
		 * @generated
		 */
		EClass FIXED_POINT_TYPE_PARAM = eINSTANCE.getFixedPointTypeParam();

		/**
		 * The meta object literal for the '<em><b>Signed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_POINT_TYPE_PARAM__SIGNED = eINSTANCE.getFixedPointTypeParam_Signed();

		/**
		 * The meta object literal for the '<em><b>Total Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_POINT_TYPE_PARAM__TOTAL_WIDTH = eINSTANCE.getFixedPointTypeParam_TotalWidth();

		/**
		 * The meta object literal for the '<em><b>Integer Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_POINT_TYPE_PARAM__INTEGER_WIDTH = eINSTANCE.getFixedPointTypeParam_IntegerWidth();

		/**
		 * The meta object literal for the '<em><b>Quantification Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_POINT_TYPE_PARAM__QUANTIFICATION_MODE = eINSTANCE.getFixedPointTypeParam_QuantificationMode();

		/**
		 * The meta object literal for the '<em><b>Overflow Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_POINT_TYPE_PARAM__OVERFLOW_MODE = eINSTANCE.getFixedPointTypeParam_OverflowMode();

		/**
		 * The meta object literal for the '{@link typeexploration.impl.FloatPointTypeParamImpl <em>Float Point Type Param</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.impl.FloatPointTypeParamImpl
		 * @see typeexploration.impl.TypeexplorationPackageImpl#getFloatPointTypeParam()
		 * @generated
		 */
		EClass FLOAT_POINT_TYPE_PARAM = eINSTANCE.getFloatPointTypeParam();

		/**
		 * The meta object literal for the '<em><b>Signed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FLOAT_POINT_TYPE_PARAM__SIGNED = eINSTANCE.getFloatPointTypeParam_Signed();

		/**
		 * The meta object literal for the '<em><b>Total Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FLOAT_POINT_TYPE_PARAM__TOTAL_WIDTH = eINSTANCE.getFloatPointTypeParam_TotalWidth();

		/**
		 * The meta object literal for the '<em><b>Exponent Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FLOAT_POINT_TYPE_PARAM__EXPONENT_WIDTH = eINSTANCE.getFloatPointTypeParam_ExponentWidth();

		/**
		 * The meta object literal for the '<em><b>Exponent Bias</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FLOAT_POINT_TYPE_PARAM__EXPONENT_BIAS = eINSTANCE.getFloatPointTypeParam_ExponentBias();

		/**
		 * The meta object literal for the '{@link typeexploration.impl.GenericTypeParamImpl <em>Generic Type Param</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.impl.GenericTypeParamImpl
		 * @see typeexploration.impl.TypeexplorationPackageImpl#getGenericTypeParam()
		 * @generated
		 */
		EClass GENERIC_TYPE_PARAM = eINSTANCE.getGenericTypeParam();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GENERIC_TYPE_PARAM__TYPE = eINSTANCE.getGenericTypeParam_Type();

		/**
		 * The meta object literal for the '{@link typeexploration.impl.TypeConstraintImpl <em>Type Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.impl.TypeConstraintImpl
		 * @see typeexploration.impl.TypeexplorationPackageImpl#getTypeConstraint()
		 * @generated
		 */
		EClass TYPE_CONSTRAINT = eINSTANCE.getTypeConstraint();

		/**
		 * The meta object literal for the '{@link typeexploration.impl.SameTypeConstraintImpl <em>Same Type Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.impl.SameTypeConstraintImpl
		 * @see typeexploration.impl.TypeexplorationPackageImpl#getSameTypeConstraint()
		 * @generated
		 */
		EClass SAME_TYPE_CONSTRAINT = eINSTANCE.getSameTypeConstraint();

		/**
		 * The meta object literal for the '<em><b>Symbol</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SAME_TYPE_CONSTRAINT__SYMBOL = eINSTANCE.getSameTypeConstraint_Symbol();

		/**
		 * The meta object literal for the '{@link typeexploration.TypeConfiguration <em>Type Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.TypeConfiguration
		 * @see typeexploration.impl.TypeexplorationPackageImpl#getTypeConfiguration()
		 * @generated
		 */
		EClass TYPE_CONFIGURATION = eINSTANCE.getTypeConfiguration();

		/**
		 * The meta object literal for the '{@link typeexploration.impl.NumberTypeConfigurationImpl <em>Number Type Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.impl.NumberTypeConfigurationImpl
		 * @see typeexploration.impl.TypeexplorationPackageImpl#getNumberTypeConfiguration()
		 * @generated
		 */
		EClass NUMBER_TYPE_CONFIGURATION = eINSTANCE.getNumberTypeConfiguration();

		/**
		 * The meta object literal for the '<em><b>Signed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NUMBER_TYPE_CONFIGURATION__SIGNED = eINSTANCE.getNumberTypeConfiguration_Signed();

		/**
		 * The meta object literal for the '<em><b>Total Width Values</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NUMBER_TYPE_CONFIGURATION__TOTAL_WIDTH_VALUES = eINSTANCE.getNumberTypeConfiguration_TotalWidthValues();

		/**
		 * The meta object literal for the '{@link typeexploration.impl.FixedPointTypeConfigurationImpl <em>Fixed Point Type Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.impl.FixedPointTypeConfigurationImpl
		 * @see typeexploration.impl.TypeexplorationPackageImpl#getFixedPointTypeConfiguration()
		 * @generated
		 */
		EClass FIXED_POINT_TYPE_CONFIGURATION = eINSTANCE.getFixedPointTypeConfiguration();

		/**
		 * The meta object literal for the '<em><b>Integer Width Values</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_POINT_TYPE_CONFIGURATION__INTEGER_WIDTH_VALUES = eINSTANCE.getFixedPointTypeConfiguration_IntegerWidthValues();

		/**
		 * The meta object literal for the '<em><b>Quantification Mode</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_POINT_TYPE_CONFIGURATION__QUANTIFICATION_MODE = eINSTANCE.getFixedPointTypeConfiguration_QuantificationMode();

		/**
		 * The meta object literal for the '<em><b>Overflow Mode</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_POINT_TYPE_CONFIGURATION__OVERFLOW_MODE = eINSTANCE.getFixedPointTypeConfiguration_OverflowMode();

		/**
		 * The meta object literal for the '{@link typeexploration.impl.FloatPointTypeConfigurationImpl <em>Float Point Type Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.impl.FloatPointTypeConfigurationImpl
		 * @see typeexploration.impl.TypeexplorationPackageImpl#getFloatPointTypeConfiguration()
		 * @generated
		 */
		EClass FLOAT_POINT_TYPE_CONFIGURATION = eINSTANCE.getFloatPointTypeConfiguration();

		/**
		 * The meta object literal for the '<em><b>Exponent Width Values</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FLOAT_POINT_TYPE_CONFIGURATION__EXPONENT_WIDTH_VALUES = eINSTANCE.getFloatPointTypeConfiguration_ExponentWidthValues();

		/**
		 * The meta object literal for the '<em><b>Bias Values</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FLOAT_POINT_TYPE_CONFIGURATION__BIAS_VALUES = eINSTANCE.getFloatPointTypeConfiguration_BiasValues();

		/**
		 * The meta object literal for the '{@link typeexploration.impl.TypeConfigurationSpaceImpl <em>Type Configuration Space</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.impl.TypeConfigurationSpaceImpl
		 * @see typeexploration.impl.TypeexplorationPackageImpl#getTypeConfigurationSpace()
		 * @generated
		 */
		EClass TYPE_CONFIGURATION_SPACE = eINSTANCE.getTypeConfigurationSpace();

		/**
		 * The meta object literal for the '<em><b>Fixed Pt Congurations</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TYPE_CONFIGURATION_SPACE__FIXED_PT_CONGURATIONS = eINSTANCE.getTypeConfigurationSpace_FixedPtCongurations();

		/**
		 * The meta object literal for the '<em><b>Float Pt Congurations</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TYPE_CONFIGURATION_SPACE__FLOAT_PT_CONGURATIONS = eINSTANCE.getTypeConfigurationSpace_FloatPtCongurations();

		/**
		 * The meta object literal for the '<em><b>Constraints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TYPE_CONFIGURATION_SPACE__CONSTRAINTS = eINSTANCE.getTypeConfigurationSpace_Constraints();

		/**
		 * The meta object literal for the '{@link typeexploration.impl.SymbolToTypeConfigurationsEntryImpl <em>Symbol To Type Configurations Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.impl.SymbolToTypeConfigurationsEntryImpl
		 * @see typeexploration.impl.TypeexplorationPackageImpl#getSymbolToTypeConfigurationsEntry()
		 * @generated
		 */
		EClass SYMBOL_TO_TYPE_CONFIGURATIONS_ENTRY = eINSTANCE.getSymbolToTypeConfigurationsEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYMBOL_TO_TYPE_CONFIGURATIONS_ENTRY__KEY = eINSTANCE.getSymbolToTypeConfigurationsEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYMBOL_TO_TYPE_CONFIGURATIONS_ENTRY__VALUE = eINSTANCE.getSymbolToTypeConfigurationsEntry_Value();

		/**
		 * The meta object literal for the '{@link typeexploration.impl.SolutionSpaceImpl <em>Solution Space</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.impl.SolutionSpaceImpl
		 * @see typeexploration.impl.TypeexplorationPackageImpl#getSolutionSpace()
		 * @generated
		 */
		EClass SOLUTION_SPACE = eINSTANCE.getSolutionSpace();

		/**
		 * The meta object literal for the '<em><b>Project</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOLUTION_SPACE__PROJECT = eINSTANCE.getSolutionSpace_Project();

		/**
		 * The meta object literal for the '<em><b>Explored Symbols</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOLUTION_SPACE__EXPLORED_SYMBOLS = eINSTANCE.getSolutionSpace_ExploredSymbols();

		/**
		 * The meta object literal for the '<em><b>Default Type Configs</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOLUTION_SPACE__DEFAULT_TYPE_CONFIGS = eINSTANCE.getSolutionSpace_DefaultTypeConfigs();

		/**
		 * The meta object literal for the '<em><b>Analyzer</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOLUTION_SPACE__ANALYZER = eINSTANCE.getSolutionSpace_Analyzer();

		/**
		 * The meta object literal for the '<em>Solution Space Analyzer</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.gecos.typeexploration.solutionspace.SolutionSpaceAnalyzer
		 * @see typeexploration.impl.TypeexplorationPackageImpl#getSolutionSpaceAnalyzer()
		 * @generated
		 */
		EDataType SOLUTION_SPACE_ANALYZER = eINSTANCE.getSolutionSpaceAnalyzer();

	}

} //TypeexplorationPackage
