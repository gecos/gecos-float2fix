/**
 */
package typeexploration;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Number Type Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.NumberTypeConfiguration#getSigned <em>Signed</em>}</li>
 *   <li>{@link typeexploration.NumberTypeConfiguration#getTotalWidthValues <em>Total Width Values</em>}</li>
 * </ul>
 *
 * @see typeexploration.TypeexplorationPackage#getNumberTypeConfiguration()
 * @model abstract="true"
 * @generated
 */
public interface NumberTypeConfiguration extends TypeConfiguration {
	/**
	 * Returns the value of the '<em><b>Signed</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Boolean}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signed</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signed</em>' attribute list.
	 * @see typeexploration.TypeexplorationPackage#getNumberTypeConfiguration_Signed()
	 * @model
	 * @generated
	 */
	EList<Boolean> getSigned();

	/**
	 * Returns the value of the '<em><b>Total Width Values</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Total Width Values</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Total Width Values</em>' attribute list.
	 * @see typeexploration.TypeexplorationPackage#getNumberTypeConfiguration_TotalWidthValues()
	 * @model
	 * @generated
	 */
	EList<Integer> getTotalWidthValues();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" cUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getSigned().addAll(c.getSigned());\nthis.getTotalWidthValues().addAll(c.getTotalWidthValues());\nreturn this;'"
	 * @generated
	 */
	NumberTypeConfiguration mergeIn(NumberTypeConfiguration c);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.ecore.util.EcoreUtil%&gt;.&lt;&lt;%typeexploration.NumberTypeConfiguration%&gt;&gt;copy(this);'"
	 * @generated
	 */
	NumberTypeConfiguration copy();

} // NumberTypeConfiguration
