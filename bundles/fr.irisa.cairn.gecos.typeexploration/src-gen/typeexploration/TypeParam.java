/**
 */
package typeexploration;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Param</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see typeexploration.TypeexplorationPackage#getTypeParam()
 * @model abstract="true"
 * @generated
 */
public interface TypeParam extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 * @generated
	 */
	int getTotalWidth();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" otherUnique="false"
	 * @generated
	 */
	boolean equals(TypeParam other);

} // TypeParam
