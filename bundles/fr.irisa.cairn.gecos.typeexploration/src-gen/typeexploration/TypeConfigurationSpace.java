/**
 */
package typeexploration;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Configuration Space</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.TypeConfigurationSpace#getFixedPtCongurations <em>Fixed Pt Congurations</em>}</li>
 *   <li>{@link typeexploration.TypeConfigurationSpace#getFloatPtCongurations <em>Float Pt Congurations</em>}</li>
 *   <li>{@link typeexploration.TypeConfigurationSpace#getConstraints <em>Constraints</em>}</li>
 * </ul>
 *
 * @see typeexploration.TypeexplorationPackage#getTypeConfigurationSpace()
 * @model
 * @generated
 */
public interface TypeConfigurationSpace extends EObject {
	/**
	 * Returns the value of the '<em><b>Fixed Pt Congurations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fixed Pt Congurations</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fixed Pt Congurations</em>' containment reference.
	 * @see #setFixedPtCongurations(FixedPointTypeConfiguration)
	 * @see typeexploration.TypeexplorationPackage#getTypeConfigurationSpace_FixedPtCongurations()
	 * @model containment="true"
	 * @generated
	 */
	FixedPointTypeConfiguration getFixedPtCongurations();

	/**
	 * Sets the value of the '{@link typeexploration.TypeConfigurationSpace#getFixedPtCongurations <em>Fixed Pt Congurations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fixed Pt Congurations</em>' containment reference.
	 * @see #getFixedPtCongurations()
	 * @generated
	 */
	void setFixedPtCongurations(FixedPointTypeConfiguration value);

	/**
	 * Returns the value of the '<em><b>Float Pt Congurations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Float Pt Congurations</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Float Pt Congurations</em>' containment reference.
	 * @see #setFloatPtCongurations(FloatPointTypeConfiguration)
	 * @see typeexploration.TypeexplorationPackage#getTypeConfigurationSpace_FloatPtCongurations()
	 * @model containment="true"
	 * @generated
	 */
	FloatPointTypeConfiguration getFloatPtCongurations();

	/**
	 * Sets the value of the '{@link typeexploration.TypeConfigurationSpace#getFloatPtCongurations <em>Float Pt Congurations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Float Pt Congurations</em>' containment reference.
	 * @see #getFloatPtCongurations()
	 * @generated
	 */
	void setFloatPtCongurations(FloatPointTypeConfiguration value);

	/**
	 * Returns the value of the '<em><b>Constraints</b></em>' containment reference list.
	 * The list contents are of type {@link typeexploration.TypeConstraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraints</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraints</em>' containment reference list.
	 * @see typeexploration.TypeexplorationPackage#getTypeConfigurationSpace_Constraints()
	 * @model containment="true"
	 * @generated
	 */
	EList<TypeConstraint> getConstraints();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 *  @return an unmodifiable list of configurations
	 * <!-- end-model-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%typeexploration.TypeConfiguration%&gt;&gt; list = new &lt;%org.eclipse.emf.common.util.BasicEList%&gt;&lt;&lt;%typeexploration.TypeConfiguration%&gt;&gt;();\n&lt;%typeexploration.FixedPointTypeConfiguration%&gt; _fixedPtCongurations = this.getFixedPtCongurations();\nboolean _tripleNotEquals = (_fixedPtCongurations != null);\nif (_tripleNotEquals)\n{\n\tlist.add(this.getFixedPtCongurations());\n}\n&lt;%typeexploration.FloatPointTypeConfiguration%&gt; _floatPtCongurations = this.getFloatPtCongurations();\nboolean _tripleNotEquals_1 = (_floatPtCongurations != null);\nif (_tripleNotEquals_1)\n{\n\tlist.add(this.getFloatPtCongurations());\n}\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%typeexploration.TypeConfiguration%&gt;&gt;unmodifiableEList(list);'"
	 * @generated
	 */
	EList<TypeConfiguration> listConfigurations();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model cUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if ((c instanceof &lt;%typeexploration.FixedPointTypeConfiguration%&gt;))\n{\n\t&lt;%typeexploration.FixedPointTypeConfiguration%&gt; _fixedPtCongurations = this.getFixedPtCongurations();\n\tboolean _tripleEquals = (_fixedPtCongurations == null);\n\tif (_tripleEquals)\n\t{\n\t\tthis.setFixedPtCongurations(((&lt;%typeexploration.FixedPointTypeConfiguration%&gt;)c));\n\t}\n\telse\n\t{\n\t\tthis.getFixedPtCongurations().mergeIn(((&lt;%typeexploration.FixedPointTypeConfiguration%&gt;)c));\n\t}\n}\nelse\n{\n\tif ((c instanceof &lt;%typeexploration.FloatPointTypeConfiguration%&gt;))\n\t{\n\t\t&lt;%typeexploration.FloatPointTypeConfiguration%&gt; _floatPtCongurations = this.getFloatPtCongurations();\n\t\tboolean _tripleEquals_1 = (_floatPtCongurations == null);\n\t\tif (_tripleEquals_1)\n\t\t{\n\t\t\tthis.setFloatPtCongurations(((&lt;%typeexploration.FloatPointTypeConfiguration%&gt;)c));\n\t\t}\n\t\telse\n\t\t{\n\t\t\tthis.getFloatPtCongurations().mergeIn(((&lt;%typeexploration.FloatPointTypeConfiguration%&gt;)c));\n\t\t}\n\t}\n\telse\n\t{\n\t\tthrow new &lt;%java.security.InvalidParameterException%&gt;((\"Unknown parameter type: \" + c));\n\t}\n}'"
	 * @generated
	 */
	void addConfiguration(TypeConfiguration c);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%typeexploration.TypeConfiguration%&gt;, &lt;%java.lang.CharSequence%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%typeexploration.TypeConfiguration%&gt;, &lt;%java.lang.CharSequence%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.CharSequence%&gt; apply(final &lt;%typeexploration.TypeConfiguration%&gt; it)\n\t{\n\t\t&lt;%java.lang.String%&gt; _string = it.toString();\n\t\treturn (\"\\t\\t\" + _string);\n\t}\n};\n&lt;%java.lang.String%&gt; _join = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%typeexploration.TypeConfiguration%&gt;&gt;join(this.listConfigurations(), \"\\n\", _function);\nfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%typeexploration.TypeConstraint%&gt;, &lt;%java.lang.CharSequence%&gt;&gt; _function_1 = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%typeexploration.TypeConstraint%&gt;, &lt;%java.lang.CharSequence%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.CharSequence%&gt; apply(final &lt;%typeexploration.TypeConstraint%&gt; it)\n\t{\n\t\t&lt;%java.lang.String%&gt; _string = it.toString();\n\t\treturn (\"\\t\\t\" + _string);\n\t}\n};\n&lt;%java.lang.String%&gt; _join_1 = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%typeexploration.TypeConstraint%&gt;&gt;join(this.getConstraints(), \"\\n\", _function_1);\nreturn (_join + _join_1);'"
	 * @generated
	 */
	String toString();

} // TypeConfigurationSpace
