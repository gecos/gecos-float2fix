/**
 */
package typeexploration.util;

import gecos.core.Symbol;

import java.util.Map;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import typeexploration.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see typeexploration.TypeexplorationPackage
 * @generated
 */
public class TypeexplorationAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TypeexplorationPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeexplorationAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = TypeexplorationPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TypeexplorationSwitch<Adapter> modelSwitch =
		new TypeexplorationSwitch<Adapter>() {
			@Override
			public Adapter caseTypeParam(TypeParam object) {
				return createTypeParamAdapter();
			}
			@Override
			public Adapter caseFixedPointTypeParam(FixedPointTypeParam object) {
				return createFixedPointTypeParamAdapter();
			}
			@Override
			public Adapter caseFloatPointTypeParam(FloatPointTypeParam object) {
				return createFloatPointTypeParamAdapter();
			}
			@Override
			public Adapter caseGenericTypeParam(GenericTypeParam object) {
				return createGenericTypeParamAdapter();
			}
			@Override
			public Adapter caseTypeConstraint(TypeConstraint object) {
				return createTypeConstraintAdapter();
			}
			@Override
			public Adapter caseSameTypeConstraint(SameTypeConstraint object) {
				return createSameTypeConstraintAdapter();
			}
			@Override
			public Adapter caseTypeConfiguration(TypeConfiguration object) {
				return createTypeConfigurationAdapter();
			}
			@Override
			public Adapter caseNumberTypeConfiguration(NumberTypeConfiguration object) {
				return createNumberTypeConfigurationAdapter();
			}
			@Override
			public Adapter caseFixedPointTypeConfiguration(FixedPointTypeConfiguration object) {
				return createFixedPointTypeConfigurationAdapter();
			}
			@Override
			public Adapter caseFloatPointTypeConfiguration(FloatPointTypeConfiguration object) {
				return createFloatPointTypeConfigurationAdapter();
			}
			@Override
			public Adapter caseTypeConfigurationSpace(TypeConfigurationSpace object) {
				return createTypeConfigurationSpaceAdapter();
			}
			@Override
			public Adapter caseSymbolToTypeConfigurationsEntry(Map.Entry<Symbol, TypeConfigurationSpace> object) {
				return createSymbolToTypeConfigurationsEntryAdapter();
			}
			@Override
			public Adapter caseSolutionSpace(SolutionSpace object) {
				return createSolutionSpaceAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.TypeParam <em>Type Param</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.TypeParam
	 * @generated
	 */
	public Adapter createTypeParamAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.FixedPointTypeParam <em>Fixed Point Type Param</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.FixedPointTypeParam
	 * @generated
	 */
	public Adapter createFixedPointTypeParamAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.FloatPointTypeParam <em>Float Point Type Param</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.FloatPointTypeParam
	 * @generated
	 */
	public Adapter createFloatPointTypeParamAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.GenericTypeParam <em>Generic Type Param</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.GenericTypeParam
	 * @generated
	 */
	public Adapter createGenericTypeParamAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.TypeConstraint <em>Type Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.TypeConstraint
	 * @generated
	 */
	public Adapter createTypeConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.SameTypeConstraint <em>Same Type Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.SameTypeConstraint
	 * @generated
	 */
	public Adapter createSameTypeConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.TypeConfiguration <em>Type Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.TypeConfiguration
	 * @generated
	 */
	public Adapter createTypeConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.NumberTypeConfiguration <em>Number Type Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.NumberTypeConfiguration
	 * @generated
	 */
	public Adapter createNumberTypeConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.FixedPointTypeConfiguration <em>Fixed Point Type Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.FixedPointTypeConfiguration
	 * @generated
	 */
	public Adapter createFixedPointTypeConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.FloatPointTypeConfiguration <em>Float Point Type Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.FloatPointTypeConfiguration
	 * @generated
	 */
	public Adapter createFloatPointTypeConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.TypeConfigurationSpace <em>Type Configuration Space</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.TypeConfigurationSpace
	 * @generated
	 */
	public Adapter createTypeConfigurationSpaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Symbol To Type Configurations Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createSymbolToTypeConfigurationsEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.SolutionSpace <em>Solution Space</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.SolutionSpace
	 * @generated
	 */
	public Adapter createSolutionSpaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //TypeexplorationAdapterFactory
