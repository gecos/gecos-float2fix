/**
 */
package typeexploration.util;

import gecos.core.Symbol;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import typeexploration.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see typeexploration.TypeexplorationPackage
 * @generated
 */
public class TypeexplorationSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TypeexplorationPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeexplorationSwitch() {
		if (modelPackage == null) {
			modelPackage = TypeexplorationPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case TypeexplorationPackage.TYPE_PARAM: {
				TypeParam typeParam = (TypeParam)theEObject;
				T result = caseTypeParam(typeParam);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypeexplorationPackage.FIXED_POINT_TYPE_PARAM: {
				FixedPointTypeParam fixedPointTypeParam = (FixedPointTypeParam)theEObject;
				T result = caseFixedPointTypeParam(fixedPointTypeParam);
				if (result == null) result = caseTypeParam(fixedPointTypeParam);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypeexplorationPackage.FLOAT_POINT_TYPE_PARAM: {
				FloatPointTypeParam floatPointTypeParam = (FloatPointTypeParam)theEObject;
				T result = caseFloatPointTypeParam(floatPointTypeParam);
				if (result == null) result = caseTypeParam(floatPointTypeParam);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypeexplorationPackage.GENERIC_TYPE_PARAM: {
				GenericTypeParam genericTypeParam = (GenericTypeParam)theEObject;
				T result = caseGenericTypeParam(genericTypeParam);
				if (result == null) result = caseTypeParam(genericTypeParam);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypeexplorationPackage.TYPE_CONSTRAINT: {
				TypeConstraint typeConstraint = (TypeConstraint)theEObject;
				T result = caseTypeConstraint(typeConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypeexplorationPackage.SAME_TYPE_CONSTRAINT: {
				SameTypeConstraint sameTypeConstraint = (SameTypeConstraint)theEObject;
				T result = caseSameTypeConstraint(sameTypeConstraint);
				if (result == null) result = caseTypeConstraint(sameTypeConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypeexplorationPackage.TYPE_CONFIGURATION: {
				TypeConfiguration typeConfiguration = (TypeConfiguration)theEObject;
				T result = caseTypeConfiguration(typeConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypeexplorationPackage.NUMBER_TYPE_CONFIGURATION: {
				NumberTypeConfiguration numberTypeConfiguration = (NumberTypeConfiguration)theEObject;
				T result = caseNumberTypeConfiguration(numberTypeConfiguration);
				if (result == null) result = caseTypeConfiguration(numberTypeConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypeexplorationPackage.FIXED_POINT_TYPE_CONFIGURATION: {
				FixedPointTypeConfiguration fixedPointTypeConfiguration = (FixedPointTypeConfiguration)theEObject;
				T result = caseFixedPointTypeConfiguration(fixedPointTypeConfiguration);
				if (result == null) result = caseNumberTypeConfiguration(fixedPointTypeConfiguration);
				if (result == null) result = caseTypeConfiguration(fixedPointTypeConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypeexplorationPackage.FLOAT_POINT_TYPE_CONFIGURATION: {
				FloatPointTypeConfiguration floatPointTypeConfiguration = (FloatPointTypeConfiguration)theEObject;
				T result = caseFloatPointTypeConfiguration(floatPointTypeConfiguration);
				if (result == null) result = caseNumberTypeConfiguration(floatPointTypeConfiguration);
				if (result == null) result = caseTypeConfiguration(floatPointTypeConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypeexplorationPackage.TYPE_CONFIGURATION_SPACE: {
				TypeConfigurationSpace typeConfigurationSpace = (TypeConfigurationSpace)theEObject;
				T result = caseTypeConfigurationSpace(typeConfigurationSpace);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypeexplorationPackage.SYMBOL_TO_TYPE_CONFIGURATIONS_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Symbol, TypeConfigurationSpace> symbolToTypeConfigurationsEntry = (Map.Entry<Symbol, TypeConfigurationSpace>)theEObject;
				T result = caseSymbolToTypeConfigurationsEntry(symbolToTypeConfigurationsEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypeexplorationPackage.SOLUTION_SPACE: {
				SolutionSpace solutionSpace = (SolutionSpace)theEObject;
				T result = caseSolutionSpace(solutionSpace);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type Param</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Param</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypeParam(TypeParam object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fixed Point Type Param</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fixed Point Type Param</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFixedPointTypeParam(FixedPointTypeParam object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Float Point Type Param</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Float Point Type Param</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFloatPointTypeParam(FloatPointTypeParam object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generic Type Param</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generic Type Param</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGenericTypeParam(GenericTypeParam object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypeConstraint(TypeConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Same Type Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Same Type Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSameTypeConstraint(SameTypeConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypeConfiguration(TypeConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Number Type Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Number Type Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNumberTypeConfiguration(NumberTypeConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fixed Point Type Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fixed Point Type Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFixedPointTypeConfiguration(FixedPointTypeConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Float Point Type Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Float Point Type Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFloatPointTypeConfiguration(FloatPointTypeConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type Configuration Space</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Configuration Space</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypeConfigurationSpace(TypeConfigurationSpace object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Symbol To Type Configurations Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Symbol To Type Configurations Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSymbolToTypeConfigurationsEntry(Map.Entry<Symbol, TypeConfigurationSpace> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Solution Space</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Solution Space</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSolutionSpace(SolutionSpace object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //TypeexplorationSwitch
