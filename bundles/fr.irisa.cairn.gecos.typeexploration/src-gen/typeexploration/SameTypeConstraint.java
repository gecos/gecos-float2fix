/**
 */
package typeexploration;

import gecos.core.Symbol;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Same Type Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.SameTypeConstraint#getSymbol <em>Symbol</em>}</li>
 * </ul>
 *
 * @see typeexploration.TypeexplorationPackage#getSameTypeConstraint()
 * @model
 * @generated
 */
public interface SameTypeConstraint extends TypeConstraint {
	/**
	 * Returns the value of the '<em><b>Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symbol</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbol</em>' reference.
	 * @see #setSymbol(Symbol)
	 * @see typeexploration.TypeexplorationPackage#getSameTypeConstraint_Symbol()
	 * @model
	 * @generated
	 */
	Symbol getSymbol();

	/**
	 * Sets the value of the '{@link typeexploration.SameTypeConstraint#getSymbol <em>Symbol</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Symbol</em>' reference.
	 * @see #getSymbol()
	 * @generated
	 */
	void setSymbol(Symbol value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _printSymbol = &lt;%fr.irisa.cairn.gecos.typeexploration.utils.SolutionSpaceUtils%&gt;.printSymbol(this.getSymbol());\nreturn (\"Constraint: SAME as \" + _printSymbol);'"
	 * @generated
	 */
	String toString();

} // SameTypeConstraint
