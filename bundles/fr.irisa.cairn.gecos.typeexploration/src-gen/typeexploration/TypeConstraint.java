/**
 */
package typeexploration;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see typeexploration.TypeexplorationPackage#getTypeConstraint()
 * @model abstract="true"
 * @generated
 */
public interface TypeConstraint extends EObject {
} // TypeConstraint
