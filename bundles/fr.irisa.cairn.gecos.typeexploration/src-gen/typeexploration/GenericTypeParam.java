/**
 */
package typeexploration;

import gecos.types.Type;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generic Type Param</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Used to represent primitive floating-point types for example.
 * More generally any type that is not covered by the other TypeParam classes.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.GenericTypeParam#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see typeexploration.TypeexplorationPackage#getGenericTypeParam()
 * @model
 * @generated
 */
public interface GenericTypeParam extends TypeParam {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(Type)
	 * @see typeexploration.TypeexplorationPackage#getGenericTypeParam_Type()
	 * @model
	 * @generated
	 */
	Type getType();

	/**
	 * Sets the value of the '{@link typeexploration.GenericTypeParam#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Type value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%gecos.types.Type%&gt; _type = this.getType();\nfinal &lt;%fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer%&gt; ta = new &lt;%fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer%&gt;(_type);\nboolean _isFloatDouble = ta.getBaseLevel().isFloatDouble();\nif (_isFloatDouble)\n{\n\treturn 64;\n}\nboolean _isFloatSingle = ta.getBaseLevel().isFloatSingle();\nif (_isFloatSingle)\n{\n\treturn 32;\n}\n&lt;%gecos.types.Type%&gt; _type_1 = this.getType();\n&lt;%java.lang.String%&gt; _plus = (\"Not yet supported for type: \" + _type_1);\nthrow new &lt;%java.lang.RuntimeException%&gt;(_plus);'"
	 * @generated
	 */
	int getTotalWidth();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _string = this.getType().toString();\nreturn (\"Generic type: \" + _string);'"
	 * @generated
	 */
	String toString();

} // GenericTypeParam
