/**
 */
package typeexploration;

import fr.irisa.cairn.gecos.typeexploration.solutionspace.SolutionSpaceAnalyzer;

import gecos.core.Symbol;

import gecos.gecosproject.GecosProject;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Solution Space</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.SolutionSpace#getProject <em>Project</em>}</li>
 *   <li>{@link typeexploration.SolutionSpace#getExploredSymbols <em>Explored Symbols</em>}</li>
 *   <li>{@link typeexploration.SolutionSpace#getDefaultTypeConfigs <em>Default Type Configs</em>}</li>
 *   <li>{@link typeexploration.SolutionSpace#getAnalyzer <em>Analyzer</em>}</li>
 * </ul>
 *
 * @see typeexploration.TypeexplorationPackage#getSolutionSpace()
 * @model
 * @generated
 */
public interface SolutionSpace extends EObject {
	/**
	 * Returns the value of the '<em><b>Project</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Project</em>' reference.
	 * @see #setProject(GecosProject)
	 * @see typeexploration.TypeexplorationPackage#getSolutionSpace_Project()
	 * @model
	 * @generated
	 */
	GecosProject getProject();

	/**
	 * Sets the value of the '{@link typeexploration.SolutionSpace#getProject <em>Project</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Project</em>' reference.
	 * @see #getProject()
	 * @generated
	 */
	void setProject(GecosProject value);

	/**
	 * Returns the value of the '<em><b>Explored Symbols</b></em>' map.
	 * The key is of type {@link gecos.core.Symbol},
	 * and the value is of type {@link typeexploration.TypeConfigurationSpace},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Explored Symbols</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Explored Symbols</em>' map.
	 * @see typeexploration.TypeexplorationPackage#getSolutionSpace_ExploredSymbols()
	 * @model mapType="typeexploration.SymbolToTypeConfigurationsEntry&lt;gecos.core.Symbol, typeexploration.TypeConfigurationSpace&gt;"
	 * @generated
	 */
	EMap<Symbol, TypeConfigurationSpace> getExploredSymbols();

	/**
	 * Returns the value of the '<em><b>Default Type Configs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Type Configs</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Type Configs</em>' containment reference.
	 * @see #setDefaultTypeConfigs(TypeConfigurationSpace)
	 * @see typeexploration.TypeexplorationPackage#getSolutionSpace_DefaultTypeConfigs()
	 * @model containment="true"
	 * @generated
	 */
	TypeConfigurationSpace getDefaultTypeConfigs();

	/**
	 * Sets the value of the '{@link typeexploration.SolutionSpace#getDefaultTypeConfigs <em>Default Type Configs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Type Configs</em>' containment reference.
	 * @see #getDefaultTypeConfigs()
	 * @generated
	 */
	void setDefaultTypeConfigs(TypeConfigurationSpace value);

	/**
	 * Returns the value of the '<em><b>Analyzer</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Analyzer</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Analyzer</em>' attribute.
	 * @see #setAnalyzer(SolutionSpaceAnalyzer)
	 * @see typeexploration.TypeexplorationPackage#getSolutionSpace_Analyzer()
	 * @model unique="false" dataType="typeexploration.SolutionSpaceAnalyzer"
	 * @generated
	 */
	SolutionSpaceAnalyzer getAnalyzer();

	/**
	 * Sets the value of the '{@link typeexploration.SolutionSpace#getAnalyzer <em>Analyzer</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Analyzer</em>' attribute.
	 * @see #getAnalyzer()
	 * @generated
	 */
	void setAnalyzer(SolutionSpaceAnalyzer value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return the {@link TypeConfigurationSpace} associated with the given Symbol if present, {@code null} otherwise.
	 * Modifications will be reflected in the model.
	 * <!-- end-model-doc -->
	 * @model unique="false" sUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getExploredSymbols().get(s);'"
	 * @generated
	 */
	TypeConfigurationSpace getTypeSpace(Symbol s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 *  @return true if the Symbol does not have a TypeConfigurationSpace (i.e. it should use the default config)
	 * <!-- end-model-doc -->
	 * @model unique="false" sUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%typeexploration.TypeConfigurationSpace%&gt; _get = this.getExploredSymbols().get(s);\nreturn (_get == null);'"
	 * @generated
	 */
	boolean isDefault(Symbol s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return the constraints associated with the given Symbol if present, {@code null} otherwise.
	 * Modifications will be reflected in the model.
	 * <!-- end-model-doc -->
	 * @model unique="false" sUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%typeexploration.TypeConfigurationSpace%&gt; _get = this.getExploredSymbols().get(s);\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%typeexploration.TypeConstraint%&gt;&gt; _constraints = null;\nif (_get!=null)\n{\n\t_constraints=_get.getConstraints();\n}\nreturn _constraints;'"
	 * @generated
	 */
	EList<TypeConstraint> getTypeConstraints(Symbol s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return the fixed-point type configuration associated with the given Symbol if present, {@code null} otherwise.
	 * Modifications will be reflected in the model.
	 * <!-- end-model-doc -->
	 * @model unique="false" sUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%typeexploration.TypeConfigurationSpace%&gt; _get = this.getExploredSymbols().get(s);\n&lt;%typeexploration.FixedPointTypeConfiguration%&gt; _fixedPtCongurations = null;\nif (_get!=null)\n{\n\t_fixedPtCongurations=_get.getFixedPtCongurations();\n}\nreturn _fixedPtCongurations;'"
	 * @generated
	 */
	FixedPointTypeConfiguration getTypeFixedPtConfigs(Symbol s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return the floating-point type configuration associated with the given Symbol if present, {@code null} otherwise.
	 * Modifications will be reflected in the model.
	 * <!-- end-model-doc -->
	 * @model unique="false" sUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%typeexploration.TypeConfigurationSpace%&gt; _get = this.getExploredSymbols().get(s);\n&lt;%typeexploration.FloatPointTypeConfiguration%&gt; _floatPtCongurations = null;\nif (_get!=null)\n{\n\t_floatPtCongurations=_get.getFloatPtCongurations();\n}\nreturn _floatPtCongurations;'"
	 * @generated
	 */
	FloatPointTypeConfiguration getTypeFloatPtConfigs(Symbol s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return an unmodifiable list of all type configurations associated with the given Symbol if present, {@code null} otherwise.
	 * <!-- end-model-doc -->
	 * @model unique="false" sUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%typeexploration.TypeConfigurationSpace%&gt; _get = this.getExploredSymbols().get(s);\n&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%typeexploration.TypeConfiguration%&gt;&gt; _listConfigurations = null;\nif (_get!=null)\n{\n\t_listConfigurations=_get.listConfigurations();\n}\nreturn _listConfigurations;'"
	 * @generated
	 */
	EList<TypeConfiguration> listTypeConfigs(Symbol s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @return the {@link TypeConfigurationSpace} associated with the given Symbol, if not {@code null}.
	 * Otherwise the default configurationSpace is returned. Never {@code null}.
	 * <!-- end-model-doc -->
	 * @model unique="false" sUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%typeexploration.TypeConfigurationSpace%&gt; _elvis = null;\n&lt;%typeexploration.TypeConfigurationSpace%&gt; _get = this.getExploredSymbols().get(s);\nif (_get != null)\n{\n\t_elvis = _get;\n} else\n{\n\t&lt;%typeexploration.TypeConfigurationSpace%&gt; _defaultTypeConfigs = this.getDefaultTypeConfigs();\n\t_elvis = _defaultTypeConfigs;\n}\nreturn _elvis;'"
	 * @generated
	 */
	TypeConfigurationSpace getTypeSpaceOrDefault(Symbol s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model sUnique="false" cUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%typeexploration.TypeConstraint%&gt;&gt; constraints = this.getTypeConstraints(s);\nif ((constraints == null))\n{\n\tthis.getExploredSymbols().put(s, &lt;%fr.irisa.cairn.gecos.typeexploration.model.UserFactory%&gt;.configurationSpace(&lt;%org.eclipse.xtext.xbase.lib.CollectionLiterals%&gt;.&lt;&lt;%typeexploration.TypeConfiguration%&gt;&gt;emptyList(), &lt;%java.util.Collections%&gt;.&lt;&lt;%typeexploration.TypeConstraint%&gt;&gt;singletonList(c)));\n}\nelse\n{\n\tconstraints.add(c);\n}'"
	 * @generated
	 */
	void addTypeConstraint(Symbol s, TypeConstraint c);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model sUnique="false" cUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%typeexploration.TypeConfigurationSpace%&gt; space = this.getExploredSymbols().get(s);\nif ((space == null))\n{\n\tthis.getExploredSymbols().put(s, &lt;%fr.irisa.cairn.gecos.typeexploration.model.UserFactory%&gt;.configurationSpace(&lt;%java.util.Collections%&gt;.&lt;&lt;%typeexploration.TypeConfiguration%&gt;&gt;singletonList(c)));\n}\nelse\n{\n\tspace.addConfiguration(c);\n}'"
	 * @generated
	 */
	void addTypeConfiguration(Symbol s, TypeConfiguration c);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model sUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean _containsKey = this.getExploredSymbols().containsKey(s);\nboolean _not = (!_containsKey);\nif (_not)\n{\n\tthis.getExploredSymbols().put(s, null);\n}'"
	 * @generated
	 */
	void addSymbol(Symbol s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%java.util.Map.Entry%&gt;&lt;&lt;%gecos.core.Symbol%&gt;, &lt;%typeexploration.TypeConfigurationSpace%&gt;&gt;, &lt;%gecos.core.Symbol%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%java.util.Map.Entry%&gt;&lt;&lt;%gecos.core.Symbol%&gt;, &lt;%typeexploration.TypeConfigurationSpace%&gt;&gt;, &lt;%gecos.core.Symbol%&gt;&gt;()\n{\n\tpublic &lt;%gecos.core.Symbol%&gt; apply(final &lt;%java.util.Map.Entry%&gt;&lt;&lt;%gecos.core.Symbol%&gt;, &lt;%typeexploration.TypeConfigurationSpace%&gt;&gt; it)\n\t{\n\t\treturn it.getKey();\n\t}\n};\nreturn &lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%gecos.core.Symbol%&gt;&gt;unmodifiableEList(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%java.util.Map.Entry%&gt;&lt;&lt;%gecos.core.Symbol%&gt;, &lt;%typeexploration.TypeConfigurationSpace%&gt;&gt;, &lt;%gecos.core.Symbol%&gt;&gt;map(&lt;%org.eclipse.emf.common.util.ECollections%&gt;.&lt;&lt;%java.util.Map.Entry%&gt;&lt;&lt;%gecos.core.Symbol%&gt;, &lt;%typeexploration.TypeConfigurationSpace%&gt;&gt;&gt;asEList(this.getExploredSymbols()), _function));'"
	 * @generated
	 */
	EList<Symbol> getSymbols();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return &lt;%org.eclipse.emf.ecore.util.EcoreUtil%&gt;.&lt;&lt;%typeexploration.SolutionSpace%&gt;&gt;copy(this);'"
	 * @generated
	 */
	SolutionSpace copy();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" sUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.String%&gt; _name = s.getName();\n&lt;%java.lang.String%&gt; _plus = (\"TYPE_\" + _name);\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \"_\");\nint _indexOfKey = this.getExploredSymbols().indexOfKey(s);\nreturn (_plus_1 + &lt;%java.lang.Integer%&gt;.valueOf(_indexOfKey));'"
	 * @generated
	 */
	String getUniqueTypeName(Symbol s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int _size = this.getExploredSymbols().size();\n&lt;%java.lang.String%&gt; _plus = (\"Solution Space (\" + &lt;%java.lang.Integer%&gt;.valueOf(_size));\n&lt;%java.lang.String%&gt; _plus_1 = (_plus + \"):\\n\");\nfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.core.Symbol%&gt;, &lt;%java.lang.CharSequence%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%gecos.core.Symbol%&gt;, &lt;%java.lang.CharSequence%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.CharSequence%&gt; apply(final &lt;%gecos.core.Symbol%&gt; it)\n\t{\n\t\t&lt;%java.lang.String%&gt; _printSymbol = &lt;%fr.irisa.cairn.gecos.typeexploration.utils.SolutionSpaceUtils%&gt;.printSymbol(it);\n\t\t&lt;%java.lang.String%&gt; _plus = (\"\\tSymbol \\\'\" + _printSymbol);\n\t\t&lt;%java.lang.String%&gt; _plus_1 = (_plus + \"\\\' (\");\n\t\t&lt;%java.lang.String%&gt; _simpleName = it.getClass().getSimpleName();\n\t\t&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + _simpleName);\n\t\t&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + \"):\\n\");\n\t\t&lt;%typeexploration.TypeConfigurationSpace%&gt; _typeSpace = &lt;%this%&gt;.getTypeSpace(it);\n\t\treturn (_plus_3 + _typeSpace);\n\t}\n};\n&lt;%java.lang.String%&gt; _join = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%gecos.core.Symbol%&gt;&gt;join(this.getSymbols(), \"\\n\", _function);\n&lt;%java.lang.String%&gt; _plus_2 = (_plus_1 + _join);\n&lt;%java.lang.String%&gt; _plus_3 = (_plus_2 + \"\\n\\tDefault Type Configurations:\\n\");\n&lt;%typeexploration.TypeConfigurationSpace%&gt; _defaultTypeConfigs = this.getDefaultTypeConfigs();\n&lt;%java.lang.String%&gt; _plus_4 = (_plus_3 + _defaultTypeConfigs);\nreturn (_plus_4 + \"\\n\");'"
	 * @generated
	 */
	String toString();

} // SolutionSpace
