/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package float2fix.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.float2fix.util.Float2fixResourceFactoryImpl
 * @generated
 */
public class Float2fixResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public Float2fixResourceImpl(URI uri) {
		super(uri);
	}

} //Float2fixResourceImpl
