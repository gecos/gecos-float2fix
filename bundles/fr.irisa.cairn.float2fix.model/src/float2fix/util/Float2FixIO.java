package float2fix.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;

import float2fix.Float2fixPackage;
import float2fix.Graph;


public class Float2FixIO {
	public static Graph load(String fileName) {
		ResourceSet rs = new ResourceSetImpl();
		Resource.Factory.Registry f = rs.getResourceFactoryRegistry();
		Map<String, Object> m = f.getExtensionToFactoryMap();
		m.put(Resource.Factory.Registry.DEFAULT_EXTENSION, new Float2fixResourceFactoryImpl());
		rs.getPackageRegistry().put(Float2fixPackage.eNS_URI, Float2fixPackage.eINSTANCE);
		URI uri = URI.createFileURI(fileName);
		Resource resource = rs.getResource(uri, true);
		Graph myPs = null;
		if (resource.isLoaded() && resource.getContents().size() > 0) {
			myPs = (Graph) resource.getContents().get(0);
		}
		return myPs;
	}

	public static void save(String fileName, Graph ps) {
		ResourceSet rs = new ResourceSetImpl();
		Resource.Factory.Registry f = rs.getResourceFactoryRegistry();
		Map<String, Object> m = f.getExtensionToFactoryMap();
		m.put(Resource.Factory.Registry.DEFAULT_EXTENSION, new Float2fixResourceFactoryImpl());
		rs.getPackageRegistry().put(Float2fixPackage.eNS_URI, Float2fixPackage.eINSTANCE);

		Resource packageResource = rs.createResource(URI.createFileURI(fileName));
		packageResource.getContents().add(Float2fixPackage.eINSTANCE);
		try {
			packageResource.load(null);
		}
		catch (IOException e1) {
			e1.printStackTrace();
		}

		URI uri = URI.createFileURI(fileName);
		Resource resource = rs.createResource(uri);
		resource.getContents().add(ps);
		try {
			HashMap<String, Boolean> options = new HashMap<String, Boolean>();
			options.put(XMIResource.OPTION_SCHEMA_LOCATION, Boolean.TRUE);
			resource.save(options);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
