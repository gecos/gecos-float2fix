/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package float2fix.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import float2fix.Annotations;
import float2fix.Edge;
import float2fix.Float2fixPackage;
import float2fix.Node;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Edge</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link float2fix.impl.EdgeImpl#getAttr <em>Attr</em>}</li>
 *   <li>{@link float2fix.impl.EdgeImpl#getId_pred <em>Id pred</em>}</li>
 *   <li>{@link float2fix.impl.EdgeImpl#getId_succ <em>Id succ</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EdgeImpl extends EObjectImpl implements Edge {
	/**
	 * The cached value of the '{@link #getAttr() <em>Attr</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttr()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotations> attr;

	/**
	 * The cached value of the '{@link #getId_pred() <em>Id pred</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId_pred()
	 * @generated
	 * @ordered
	 */
	protected Node id_pred;

	/**
	 * The cached value of the '{@link #getId_succ() <em>Id succ</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId_succ()
	 * @generated
	 * @ordered
	 */
	protected Node id_succ;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EdgeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Float2fixPackage.Literals.EDGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotations> getAttr() {
		if (attr == null) {
			attr = new EObjectContainmentEList<Annotations>(Annotations.class, this, Float2fixPackage.EDGE__ATTR);
		}
		return attr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node getId_pred() {
		if (id_pred != null && id_pred.eIsProxy()) {
			InternalEObject oldId_pred = (InternalEObject)id_pred;
			id_pred = (Node)eResolveProxy(oldId_pred);
			if (id_pred != oldId_pred) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Float2fixPackage.EDGE__ID_PRED, oldId_pred, id_pred));
			}
		}
		return id_pred;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node basicGetId_pred() {
		return id_pred;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId_pred(Node newId_pred) {
		Node oldId_pred = id_pred;
		id_pred = newId_pred;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Float2fixPackage.EDGE__ID_PRED, oldId_pred, id_pred));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node getId_succ() {
		if (id_succ != null && id_succ.eIsProxy()) {
			InternalEObject oldId_succ = (InternalEObject)id_succ;
			id_succ = (Node)eResolveProxy(oldId_succ);
			if (id_succ != oldId_succ) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Float2fixPackage.EDGE__ID_SUCC, oldId_succ, id_succ));
			}
		}
		return id_succ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node basicGetId_succ() {
		return id_succ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId_succ(Node newId_succ) {
		Node oldId_succ = id_succ;
		id_succ = newId_succ;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Float2fixPackage.EDGE__ID_SUCC, oldId_succ, id_succ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Float2fixPackage.EDGE__ATTR:
				return ((InternalEList<?>)getAttr()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Float2fixPackage.EDGE__ATTR:
				return getAttr();
			case Float2fixPackage.EDGE__ID_PRED:
				if (resolve) return getId_pred();
				return basicGetId_pred();
			case Float2fixPackage.EDGE__ID_SUCC:
				if (resolve) return getId_succ();
				return basicGetId_succ();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Float2fixPackage.EDGE__ATTR:
				getAttr().clear();
				getAttr().addAll((Collection<? extends Annotations>)newValue);
				return;
			case Float2fixPackage.EDGE__ID_PRED:
				setId_pred((Node)newValue);
				return;
			case Float2fixPackage.EDGE__ID_SUCC:
				setId_succ((Node)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Float2fixPackage.EDGE__ATTR:
				getAttr().clear();
				return;
			case Float2fixPackage.EDGE__ID_PRED:
				setId_pred((Node)null);
				return;
			case Float2fixPackage.EDGE__ID_SUCC:
				setId_succ((Node)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Float2fixPackage.EDGE__ATTR:
				return attr != null && !attr.isEmpty();
			case Float2fixPackage.EDGE__ID_PRED:
				return id_pred != null;
			case Float2fixPackage.EDGE__ID_SUCC:
				return id_succ != null;
		}
		return super.eIsSet(featureID);
	}

} //EdgeImpl
