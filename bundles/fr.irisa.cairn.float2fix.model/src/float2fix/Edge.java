/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package float2fix;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link float2fix.Edge#getAttr <em>Attr</em>}</li>
 *   <li>{@link float2fix.Edge#getId_pred <em>Id pred</em>}</li>
 *   <li>{@link float2fix.Edge#getId_succ <em>Id succ</em>}</li>
 * </ul>
 * </p>
 *
 * @see float2fix.Float2fixPackage#getEdge()
 * @model
 * @generated
 */
public interface Edge extends EObject {
	/**
	 * Returns the value of the '<em><b>Attr</b></em>' containment reference list.
	 * The list contents are of type {@link float2fix.Annotations}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attr</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attr</em>' containment reference list.
	 * @see float2fix.Float2fixPackage#getEdge_Attr()
	 * @model containment="true"
	 * @generated
	 */
	EList<Annotations> getAttr();

	/**
	 * Returns the value of the '<em><b>Id pred</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id pred</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id pred</em>' reference.
	 * @see #setId_pred(Node)
	 * @see float2fix.Float2fixPackage#getEdge_Id_pred()
	 * @model
	 * @generated
	 */
	Node getId_pred();

	/**
	 * Sets the value of the '{@link float2fix.Edge#getId_pred <em>Id pred</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id pred</em>' reference.
	 * @see #getId_pred()
	 * @generated
	 */
	void setId_pred(Node value);

	/**
	 * Returns the value of the '<em><b>Id succ</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id succ</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id succ</em>' reference.
	 * @see #setId_succ(Node)
	 * @see float2fix.Float2fixPackage#getEdge_Id_succ()
	 * @model
	 * @generated
	 */
	Node getId_succ();

	/**
	 * Sets the value of the '{@link float2fix.Edge#getId_succ <em>Id succ</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id succ</em>' reference.
	 * @see #getId_succ()
	 * @generated
	 */
	void setId_succ(Node value);

} // Edge
