package fr.irisa.cairn.float2fix.model.extended.c.code.generator.xtend

import gecos.extended.types.SCFixType
import gecos.extended.types.SCFixedType
import gecos.extended.types.SCUFixType
import gecos.extended.types.SCUFixedType
import org.eclipse.emf.ecore.EObject

class GIDFixTypeTemplate extends GIDFixTemplate {
	
	def dispatch generate(SCFixType o){
		afterSymbolInformation.append("("+o.bitWidth+", "+o.integerWidth+", "+o.quantificationMode+", "+o.overflowMode+")");		
		'''sc_fix'''
	}
	
	def dispatch generate(SCUFixType o){
		afterSymbolInformation.append("("+o.bitWidth+", "+o.integerWidth+", "+o.quantificationMode+", "+o.overflowMode+")");		
		'''sc_ufix'''
	}
	
	def dispatch generate(SCFixedType o){
		'''sc_fixed<«o.bitWidth», «o.integerWidth», «o.quantificationMode», «o.overflowMode»>'''
	}
	
	def dispatch generate(SCUFixedType o){
		'''sc_ufixed<«o.bitWidth», «o.integerWidth», «o.quantificationMode», «o.overflowMode»>'''
	}
	
	def dispatch generate(EObject object) {
		null
	}
	
}