package fr.irisa.cairn.float2fix.model.extended.c.code.generator;

import fr.irisa.cairn.float2fix.model.extended.c.code.generator.xtend.GIDFixTypeTemplate;
import fr.irisa.cairn.gecos.model.extensions.generators.ExtendableGenerator;
import fr.irisa.cairn.gecos.model.extensions.generators.IGeneratorDispatchStrategy;

public class GIDFixTypeExtendableCGenerator extends ExtendableGenerator {

	 public static GIDFixTypeExtendableCGenerator eInstance = new GIDFixTypeExtendableCGenerator();

     public GIDFixTypeExtendableCGenerator() {
             super(new GIDFixTypeTemplate());
     }

     public GIDFixTypeExtendableCGenerator(IGeneratorDispatchStrategy strategy) {
             super(strategy,new GIDFixTypeTemplate());
     }
	
}
