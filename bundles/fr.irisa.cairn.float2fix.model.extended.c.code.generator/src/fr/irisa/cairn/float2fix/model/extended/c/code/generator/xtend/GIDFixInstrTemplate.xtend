package fr.irisa.cairn.float2fix.model.extended.c.code.generator.xtend

import gecos.extended.instrs.NewInstruction
import gecos.instrs.Instruction
import fr.irisa.cairn.gecos.model.c.generator.ExtendableInstructionCGenerator
import gecos.extended.instrs.NewArrayInstruction
import fr.irisa.cairn.gecos.model.c.generator.ExtendableTypeCGenerator
import org.eclipse.emf.ecore.EObject

class GIDFixInstrTemplate extends GIDFixTemplate {
	
	def dispatch generate(EObject object) {
		null
	}
	
	def dispatch generate(NewInstruction instr){
		val buffer = new StringBuffer()
		buffer.append('''new «ExtendableTypeCGenerator::eInstance.generate(instr.object)»''')
		buffer.append("(")
		for (Instruction instruction : instr.parameters) {
			buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(instruction)»''')
			if (instr.parameters.indexOf(instruction) < instr.parameters.size - 1)
				buffer.append(", ")
		}
		buffer.append(")")
		'''«buffer»''' 
	}
	
	def dispatch generate(NewArrayInstruction instr){
		
		val buffer = new StringBuffer()
		buffer.append('''new «ExtendableTypeCGenerator::eInstance.generate(instr.object)»''')
		
		val size = instr.index.size;
		if(size > 1){
			for(i : 1..instr.index.size-1){
				buffer.append("*");
			}
		}
		buffer.append("[")
		buffer.append('''«ExtendableInstructionCGenerator::eInstance.generate(instr.index.head)»''')
		buffer.append("]")
		'''«buffer»''' 
	}
	
	
}