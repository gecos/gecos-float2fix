package fr.irisa.cairn.float2fix.model.extended.c.code.generator;

import fr.irisa.cairn.float2fix.model.extended.c.code.generator.xtend.GIDFixInstrTemplate;
import fr.irisa.cairn.gecos.model.extensions.generators.ExtendableGenerator;
import fr.irisa.cairn.gecos.model.extensions.generators.IGeneratorDispatchStrategy;

public class GIDFixInstrExtendableCGenerator extends ExtendableGenerator {

	 public static GIDFixInstrExtendableCGenerator eInstance = new GIDFixInstrExtendableCGenerator();

     public GIDFixInstrExtendableCGenerator() {
             super(new GIDFixInstrTemplate());
     }

     public GIDFixInstrExtendableCGenerator(IGeneratorDispatchStrategy strategy) {
             super(strategy,new GIDFixInstrTemplate());
     }
	
}
