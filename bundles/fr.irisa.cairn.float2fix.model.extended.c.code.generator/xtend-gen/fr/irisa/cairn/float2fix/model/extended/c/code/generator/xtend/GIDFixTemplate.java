package fr.irisa.cairn.float2fix.model.extended.c.code.generator.xtend;

import fr.irisa.cairn.gecos.model.extensions.generators.IGecosCodeGenerator;
import javax.annotation.Generated;
import org.eclipse.emf.ecore.EObject;

@SuppressWarnings("all")
@Generated(value = "org.eclipse.xtend.core.compiler.XtendGenerator", date = "2019-04-08T14:12+0200")
public class GIDFixTemplate implements IGecosCodeGenerator {
  public Object generate(final EObject o) {
    return null;
  }
  
  @Override
  public String generate(final Object o) {
    Object _xblockexpression = null;
    {
      final Object object = this.generate(((EObject) o));
      if ((object != null)) {
        return object.toString();
      }
      _xblockexpression = null;
    }
    return ((String)_xblockexpression);
  }
}
