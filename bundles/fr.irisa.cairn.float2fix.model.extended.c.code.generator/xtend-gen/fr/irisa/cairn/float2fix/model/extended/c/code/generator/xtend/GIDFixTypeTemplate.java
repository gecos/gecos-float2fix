package fr.irisa.cairn.float2fix.model.extended.c.code.generator.xtend;

import fr.irisa.cairn.float2fix.model.extended.c.code.generator.xtend.GIDFixTemplate;
import fr.irisa.cairn.gecos.model.extensions.generators.IGecosCodeGenerator;
import gecos.extended.types.OverflowMode;
import gecos.extended.types.QuantificationMode;
import gecos.extended.types.SCFixType;
import gecos.extended.types.SCFixedType;
import gecos.extended.types.SCUFixType;
import gecos.extended.types.SCUFixedType;
import java.util.Arrays;
import javax.annotation.Generated;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
@Generated(value = "org.eclipse.xtend.core.compiler.XtendGenerator", date = "2019-04-08T14:12+0200")
public class GIDFixTypeTemplate extends GIDFixTemplate {
  protected Object _generate(final SCFixType o) {
    CharSequence _xblockexpression = null;
    {
      String _bitWidth = o.getBitWidth();
      String _plus = ("(" + _bitWidth);
      String _plus_1 = (_plus + ", ");
      String _integerWidth = o.getIntegerWidth();
      String _plus_2 = (_plus_1 + _integerWidth);
      String _plus_3 = (_plus_2 + ", ");
      QuantificationMode _quantificationMode = o.getQuantificationMode();
      String _plus_4 = (_plus_3 + _quantificationMode);
      String _plus_5 = (_plus_4 + ", ");
      OverflowMode _overflowMode = o.getOverflowMode();
      String _plus_6 = (_plus_5 + _overflowMode);
      String _plus_7 = (_plus_6 + ")");
      IGecosCodeGenerator.afterSymbolInformation.append(_plus_7);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("sc_fix");
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final SCUFixType o) {
    CharSequence _xblockexpression = null;
    {
      String _bitWidth = o.getBitWidth();
      String _plus = ("(" + _bitWidth);
      String _plus_1 = (_plus + ", ");
      String _integerWidth = o.getIntegerWidth();
      String _plus_2 = (_plus_1 + _integerWidth);
      String _plus_3 = (_plus_2 + ", ");
      QuantificationMode _quantificationMode = o.getQuantificationMode();
      String _plus_4 = (_plus_3 + _quantificationMode);
      String _plus_5 = (_plus_4 + ", ");
      OverflowMode _overflowMode = o.getOverflowMode();
      String _plus_6 = (_plus_5 + _overflowMode);
      String _plus_7 = (_plus_6 + ")");
      IGecosCodeGenerator.afterSymbolInformation.append(_plus_7);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("sc_ufix");
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final SCFixedType o) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("sc_fixed<");
    int _bitWidth = o.getBitWidth();
    _builder.append(_bitWidth);
    _builder.append(", ");
    int _integerWidth = o.getIntegerWidth();
    _builder.append(_integerWidth);
    _builder.append(", ");
    QuantificationMode _quantificationMode = o.getQuantificationMode();
    _builder.append(_quantificationMode);
    _builder.append(", ");
    OverflowMode _overflowMode = o.getOverflowMode();
    _builder.append(_overflowMode);
    _builder.append(">");
    return _builder;
  }
  
  protected Object _generate(final SCUFixedType o) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("sc_ufixed<");
    int _bitWidth = o.getBitWidth();
    _builder.append(_bitWidth);
    _builder.append(", ");
    int _integerWidth = o.getIntegerWidth();
    _builder.append(_integerWidth);
    _builder.append(", ");
    QuantificationMode _quantificationMode = o.getQuantificationMode();
    _builder.append(_quantificationMode);
    _builder.append(", ");
    OverflowMode _overflowMode = o.getOverflowMode();
    _builder.append(_overflowMode);
    _builder.append(">");
    return _builder;
  }
  
  protected Object _generate(final EObject object) {
    return null;
  }
  
  public Object generate(final EObject o) {
    if (o instanceof SCFixType) {
      return _generate((SCFixType)o);
    } else if (o instanceof SCFixedType) {
      return _generate((SCFixedType)o);
    } else if (o instanceof SCUFixType) {
      return _generate((SCUFixType)o);
    } else if (o instanceof SCUFixedType) {
      return _generate((SCUFixedType)o);
    } else if (o != null) {
      return _generate(o);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(o).toString());
    }
  }
}
