package fr.irisa.cairn.float2fix.model.extended.c.code.generator.xtend;

import fr.irisa.cairn.float2fix.model.extended.c.code.generator.xtend.GIDFixTemplate;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableInstructionCGenerator;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableTypeCGenerator;
import gecos.extended.instrs.NewArrayInstruction;
import gecos.extended.instrs.NewInstruction;
import gecos.instrs.Instruction;
import java.util.Arrays;
import javax.annotation.Generated;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.IntegerRange;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
@Generated(value = "org.eclipse.xtend.core.compiler.XtendGenerator", date = "2019-04-08T14:12+0200")
public class GIDFixInstrTemplate extends GIDFixTemplate {
  protected Object _generate(final EObject object) {
    return null;
  }
  
  protected Object _generate(final NewInstruction instr) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("new ");
      String _generate = ExtendableTypeCGenerator.eInstance.generate(instr.getObject());
      _builder.append(_generate);
      buffer.append(_builder);
      buffer.append("(");
      EList<Instruction> _parameters = instr.getParameters();
      for (final Instruction instruction : _parameters) {
        {
          StringConcatenation _builder_1 = new StringConcatenation();
          String _generate_1 = ExtendableInstructionCGenerator.eInstance.generate(instruction);
          _builder_1.append(_generate_1);
          buffer.append(_builder_1);
          int _indexOf = instr.getParameters().indexOf(instruction);
          int _size = instr.getParameters().size();
          int _minus = (_size - 1);
          boolean _lessThan = (_indexOf < _minus);
          if (_lessThan) {
            buffer.append(", ");
          }
        }
      }
      buffer.append(")");
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append(buffer);
      _xblockexpression = _builder_1;
    }
    return _xblockexpression;
  }
  
  protected Object _generate(final NewArrayInstruction instr) {
    CharSequence _xblockexpression = null;
    {
      final StringBuffer buffer = new StringBuffer();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("new ");
      String _generate = ExtendableTypeCGenerator.eInstance.generate(instr.getObject());
      _builder.append(_generate);
      buffer.append(_builder);
      final int size = instr.getIndex().size();
      if ((size > 1)) {
        int _size = instr.getIndex().size();
        int _minus = (_size - 1);
        IntegerRange _upTo = new IntegerRange(1, _minus);
        for (final Integer i : _upTo) {
          buffer.append("*");
        }
      }
      buffer.append("[");
      StringConcatenation _builder_1 = new StringConcatenation();
      String _generate_1 = ExtendableInstructionCGenerator.eInstance.generate(IterableExtensions.<Instruction>head(instr.getIndex()));
      _builder_1.append(_generate_1);
      buffer.append(_builder_1);
      buffer.append("]");
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append(buffer);
      _xblockexpression = _builder_2;
    }
    return _xblockexpression;
  }
  
  public Object generate(final EObject instr) {
    if (instr instanceof NewArrayInstruction) {
      return _generate((NewArrayInstruction)instr);
    } else if (instr instanceof NewInstruction) {
      return _generate((NewInstruction)instr);
    } else if (instr != null) {
      return _generate(instr);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(instr).toString());
    }
  }
}
