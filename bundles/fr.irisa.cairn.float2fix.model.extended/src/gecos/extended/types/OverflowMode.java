/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package gecos.extended.types;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Overflow Mode</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see gecos.extended.types.TypesPackage#getOverflowMode()
 * @model
 * @generated
 */
public enum OverflowMode implements Enumerator {
	/**
	 * The '<em><b>SC SAT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SC_SAT_VALUE
	 * @generated
	 * @ordered
	 */
	SC_SAT(4, "SC_SAT", "SC_SAT"),

	/**
	 * The '<em><b>SC SAT ZERO</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SC_SAT_ZERO_VALUE
	 * @generated
	 * @ordered
	 */
	SC_SAT_ZERO(5, "SC_SAT_ZERO", "SC_SAT_ZERO"),

	/**
	 * The '<em><b>SC SAT SYM</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SC_SAT_SYM_VALUE
	 * @generated
	 * @ordered
	 */
	SC_SAT_SYM(6, "SC_SAT_SYM", "SC_SAT_SYM"),

	/**
	 * The '<em><b>SC WRAP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SC_WRAP_VALUE
	 * @generated
	 * @ordered
	 */
	SC_WRAP(7, "SC_WRAP", "SC_WRAP");

	/**
	 * The '<em><b>SC SAT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SC SAT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SC_SAT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SC_SAT_VALUE = 4;

	/**
	 * The '<em><b>SC SAT ZERO</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SC SAT ZERO</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SC_SAT_ZERO
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SC_SAT_ZERO_VALUE = 5;

	/**
	 * The '<em><b>SC SAT SYM</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SC SAT SYM</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SC_SAT_SYM
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SC_SAT_SYM_VALUE = 6;

	/**
	 * The '<em><b>SC WRAP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SC WRAP</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SC_WRAP
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SC_WRAP_VALUE = 7;

	/**
	 * An array of all the '<em><b>Overflow Mode</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final OverflowMode[] VALUES_ARRAY =
		new OverflowMode[] {
			SC_SAT,
			SC_SAT_ZERO,
			SC_SAT_SYM,
			SC_WRAP,
		};

	/**
	 * A public read-only list of all the '<em><b>Overflow Mode</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<OverflowMode> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Overflow Mode</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static OverflowMode get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			OverflowMode result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Overflow Mode</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static OverflowMode getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			OverflowMode result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Overflow Mode</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static OverflowMode get(int value) {
		switch (value) {
			case SC_SAT_VALUE: return SC_SAT;
			case SC_SAT_ZERO_VALUE: return SC_SAT_ZERO;
			case SC_SAT_SYM_VALUE: return SC_SAT_SYM;
			case SC_WRAP_VALUE: return SC_WRAP;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private OverflowMode(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //OverflowMode
