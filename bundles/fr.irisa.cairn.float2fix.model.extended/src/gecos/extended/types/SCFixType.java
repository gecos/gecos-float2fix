/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package gecos.extended.types;

import gecos.types.TypesVisitable;
import gecos.types.TypesVisitor;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SC Fix Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.extended.types.SCFixType#getBitWidth <em>Bit Width</em>}</li>
 *   <li>{@link gecos.extended.types.SCFixType#getIntegerWidth <em>Integer Width</em>}</li>
 *   <li>{@link gecos.extended.types.SCFixType#getQuantificationMode <em>Quantification Mode</em>}</li>
 *   <li>{@link gecos.extended.types.SCFixType#getOverflowMode <em>Overflow Mode</em>}</li>
 * </ul>
 *
 * @see gecos.extended.types.TypesPackage#getSCFixType()
 * @model
 * @generated
 */
public interface SCFixType extends SCType, TypesVisitable {
	/**
	 * Returns the value of the '<em><b>Bit Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bit Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bit Width</em>' attribute.
	 * @see #setBitWidth(String)
	 * @see gecos.extended.types.TypesPackage#getSCFixType_BitWidth()
	 * @model
	 * @generated
	 */
	String getBitWidth();

	/**
	 * Sets the value of the '{@link gecos.extended.types.SCFixType#getBitWidth <em>Bit Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bit Width</em>' attribute.
	 * @see #getBitWidth()
	 * @generated
	 */
	void setBitWidth(String value);

	/**
	 * Returns the value of the '<em><b>Integer Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Width</em>' attribute.
	 * @see #setIntegerWidth(String)
	 * @see gecos.extended.types.TypesPackage#getSCFixType_IntegerWidth()
	 * @model
	 * @generated
	 */
	String getIntegerWidth();

	/**
	 * Sets the value of the '{@link gecos.extended.types.SCFixType#getIntegerWidth <em>Integer Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integer Width</em>' attribute.
	 * @see #getIntegerWidth()
	 * @generated
	 */
	void setIntegerWidth(String value);

	/**
	 * Returns the value of the '<em><b>Quantification Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link gecos.extended.types.QuantificationMode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantification Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantification Mode</em>' attribute.
	 * @see gecos.extended.types.QuantificationMode
	 * @see #setQuantificationMode(QuantificationMode)
	 * @see gecos.extended.types.TypesPackage#getSCFixType_QuantificationMode()
	 * @model
	 * @generated
	 */
	QuantificationMode getQuantificationMode();

	/**
	 * Sets the value of the '{@link gecos.extended.types.SCFixType#getQuantificationMode <em>Quantification Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantification Mode</em>' attribute.
	 * @see gecos.extended.types.QuantificationMode
	 * @see #getQuantificationMode()
	 * @generated
	 */
	void setQuantificationMode(QuantificationMode value);

	/**
	 * Returns the value of the '<em><b>Overflow Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link gecos.extended.types.OverflowMode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overflow Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overflow Mode</em>' attribute.
	 * @see gecos.extended.types.OverflowMode
	 * @see #setOverflowMode(OverflowMode)
	 * @see gecos.extended.types.TypesPackage#getSCFixType_OverflowMode()
	 * @model
	 * @generated
	 */
	OverflowMode getOverflowMode();

	/**
	 * Sets the value of the '{@link gecos.extended.types.SCFixType#getOverflowMode <em>Overflow Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Overflow Mode</em>' attribute.
	 * @see gecos.extended.types.OverflowMode
	 * @see #getOverflowMode()
	 * @generated
	 */
	void setOverflowMode(OverflowMode value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void accept(TypesVisitor visitor);
} // SCFixType
