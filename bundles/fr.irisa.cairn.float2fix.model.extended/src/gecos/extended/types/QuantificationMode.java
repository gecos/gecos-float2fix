/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package gecos.extended.types;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Quantification Mode</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see gecos.extended.types.TypesPackage#getQuantificationMode()
 * @model
 * @generated
 */
public enum QuantificationMode implements Enumerator {
	/**
	 * The '<em><b>SC TRN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SC_TRN_VALUE
	 * @generated
	 * @ordered
	 */
	SC_TRN(7, "SC_TRN", "SC_TRN"),

	/**
	 * The '<em><b>SC TRN ZERO</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SC_TRN_ZERO_VALUE
	 * @generated
	 * @ordered
	 */
	SC_TRN_ZERO(8, "SC_TRN_ZERO", "SC_TRN_ZERO"),

	/**
	 * The '<em><b>SC RND</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SC_RND_VALUE
	 * @generated
	 * @ordered
	 */
	SC_RND(9, "SC_RND", "SC_RND"),

	/**
	 * The '<em><b>SC RND ZERO</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SC_RND_ZERO_VALUE
	 * @generated
	 * @ordered
	 */
	SC_RND_ZERO(10, "SC_RND_ZERO", "SC_RND_ZERO"),

	/**
	 * The '<em><b>SC RND INF</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SC_RND_INF_VALUE
	 * @generated
	 * @ordered
	 */
	SC_RND_INF(11, "SC_RND_INF", "SC_RND_INF"),

	/**
	 * The '<em><b>SC RND MIN INF</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SC_RND_MIN_INF_VALUE
	 * @generated
	 * @ordered
	 */
	SC_RND_MIN_INF(12, "SC_RND_MIN_INF", "SC_RND_MIN_INF"),

	/**
	 * The '<em><b>SC RND CONV</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SC_RND_CONV_VALUE
	 * @generated
	 * @ordered
	 */
	SC_RND_CONV(13, "SC_RND_CONV", "SC_RND_CONV");

	/**
	 * The '<em><b>SC TRN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SC TRN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SC_TRN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SC_TRN_VALUE = 7;

	/**
	 * The '<em><b>SC TRN ZERO</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SC TRN ZERO</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SC_TRN_ZERO
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SC_TRN_ZERO_VALUE = 8;

	/**
	 * The '<em><b>SC RND</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SC RND</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SC_RND
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SC_RND_VALUE = 9;

	/**
	 * The '<em><b>SC RND ZERO</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SC RND ZERO</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SC_RND_ZERO
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SC_RND_ZERO_VALUE = 10;

	/**
	 * The '<em><b>SC RND INF</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SC RND INF</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SC_RND_INF
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SC_RND_INF_VALUE = 11;

	/**
	 * The '<em><b>SC RND MIN INF</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SC RND MIN INF</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SC_RND_MIN_INF
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SC_RND_MIN_INF_VALUE = 12;

	/**
	 * The '<em><b>SC RND CONV</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SC RND CONV</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SC_RND_CONV
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SC_RND_CONV_VALUE = 13;

	/**
	 * An array of all the '<em><b>Quantification Mode</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final QuantificationMode[] VALUES_ARRAY =
		new QuantificationMode[] {
			SC_TRN,
			SC_TRN_ZERO,
			SC_RND,
			SC_RND_ZERO,
			SC_RND_INF,
			SC_RND_MIN_INF,
			SC_RND_CONV,
		};

	/**
	 * A public read-only list of all the '<em><b>Quantification Mode</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<QuantificationMode> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Quantification Mode</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static QuantificationMode get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			QuantificationMode result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Quantification Mode</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static QuantificationMode getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			QuantificationMode result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Quantification Mode</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static QuantificationMode get(int value) {
		switch (value) {
			case SC_TRN_VALUE: return SC_TRN;
			case SC_TRN_ZERO_VALUE: return SC_TRN_ZERO;
			case SC_RND_VALUE: return SC_RND;
			case SC_RND_ZERO_VALUE: return SC_RND_ZERO;
			case SC_RND_INF_VALUE: return SC_RND_INF;
			case SC_RND_MIN_INF_VALUE: return SC_RND_MIN_INF;
			case SC_RND_CONV_VALUE: return SC_RND_CONV;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private QuantificationMode(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //QuantificationMode
