/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package gecos.extended.types;

import gecos.types.BaseType;
import gecos.types.TypesVisitable;
import gecos.types.TypesVisitor;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SC Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see gecos.extended.types.TypesPackage#getSCType()
 * @model
 * @generated
 */
public interface SCType extends BaseType, TypesVisitable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void accept(TypesVisitor visitor);

} // SCType
