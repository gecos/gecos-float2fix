/**
 */
package gecos.extended.types;

import gecos.types.TypesVisitor;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SCU Fixed Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.extended.types.SCUFixedType#getBitWidth <em>Bit Width</em>}</li>
 *   <li>{@link gecos.extended.types.SCUFixedType#getIntegerWidth <em>Integer Width</em>}</li>
 *   <li>{@link gecos.extended.types.SCUFixedType#getQuantificationMode <em>Quantification Mode</em>}</li>
 *   <li>{@link gecos.extended.types.SCUFixedType#getOverflowMode <em>Overflow Mode</em>}</li>
 * </ul>
 *
 * @see gecos.extended.types.TypesPackage#getSCUFixedType()
 * @model
 * @generated
 */
public interface SCUFixedType extends SCType {

	/**
	 * Returns the value of the '<em><b>Bit Width</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bit Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bit Width</em>' attribute.
	 * @see #setBitWidth(int)
	 * @see gecos.extended.types.TypesPackage#getSCUFixedType_BitWidth()
	 * @model default="0"
	 * @generated
	 */
	int getBitWidth();

	/**
	 * Sets the value of the '{@link gecos.extended.types.SCUFixedType#getBitWidth <em>Bit Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bit Width</em>' attribute.
	 * @see #getBitWidth()
	 * @generated
	 */
	void setBitWidth(int value);

	/**
	 * Returns the value of the '<em><b>Integer Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Width</em>' attribute.
	 * @see #setIntegerWidth(int)
	 * @see gecos.extended.types.TypesPackage#getSCUFixedType_IntegerWidth()
	 * @model
	 * @generated
	 */
	int getIntegerWidth();

	/**
	 * Sets the value of the '{@link gecos.extended.types.SCUFixedType#getIntegerWidth <em>Integer Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integer Width</em>' attribute.
	 * @see #getIntegerWidth()
	 * @generated
	 */
	void setIntegerWidth(int value);

	/**
	 * Returns the value of the '<em><b>Quantification Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link gecos.extended.types.QuantificationMode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantification Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantification Mode</em>' attribute.
	 * @see gecos.extended.types.QuantificationMode
	 * @see #setQuantificationMode(QuantificationMode)
	 * @see gecos.extended.types.TypesPackage#getSCUFixedType_QuantificationMode()
	 * @model
	 * @generated
	 */
	QuantificationMode getQuantificationMode();

	/**
	 * Sets the value of the '{@link gecos.extended.types.SCUFixedType#getQuantificationMode <em>Quantification Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantification Mode</em>' attribute.
	 * @see gecos.extended.types.QuantificationMode
	 * @see #getQuantificationMode()
	 * @generated
	 */
	void setQuantificationMode(QuantificationMode value);

	/**
	 * Returns the value of the '<em><b>Overflow Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link gecos.extended.types.OverflowMode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overflow Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overflow Mode</em>' attribute.
	 * @see gecos.extended.types.OverflowMode
	 * @see #setOverflowMode(OverflowMode)
	 * @see gecos.extended.types.TypesPackage#getSCUFixedType_OverflowMode()
	 * @model
	 * @generated
	 */
	OverflowMode getOverflowMode();

	/**
	 * Sets the value of the '{@link gecos.extended.types.SCUFixedType#getOverflowMode <em>Overflow Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Overflow Mode</em>' attribute.
	 * @see gecos.extended.types.OverflowMode
	 * @see #getOverflowMode()
	 * @generated
	 */
	void setOverflowMode(OverflowMode value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void accept(TypesVisitor visitor);
} // SCUFixedType
