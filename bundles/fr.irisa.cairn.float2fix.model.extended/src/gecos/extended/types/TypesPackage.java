package gecos.extended.types;


import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see gecos.extended.types.TypesFactory
 * @model kind="package"
 * @generated
 */
public interface TypesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "types";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://http://idfix.gforge.inria.fr/types";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "fr.irisa.cairn.float2fix.model.types";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TypesPackage eINSTANCE = gecos.extended.types.impl.TypesPackageImpl.init();

	/**
	 * The meta object id for the '{@link gecos.extended.types.impl.SCTypeImpl <em>SC Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.extended.types.impl.SCTypeImpl
	 * @see gecos.extended.types.impl.TypesPackageImpl#getSCType()
	 * @generated
	 */
	int SC_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_TYPE__ANNOTATIONS = gecos.types.TypesPackage.BASE_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_TYPE__CONSTANT = gecos.types.TypesPackage.BASE_TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_TYPE__VOLATILE = gecos.types.TypesPackage.BASE_TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_TYPE__STORAGE_CLASS = gecos.types.TypesPackage.BASE_TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_TYPE__SIZE = gecos.types.TypesPackage.BASE_TYPE__SIZE;

	/**
	 * The number of structural features of the '<em>SC Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_TYPE_FEATURE_COUNT = gecos.types.TypesPackage.BASE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link gecos.extended.types.impl.SCFixTypeImpl <em>SC Fix Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.extended.types.impl.SCFixTypeImpl
	 * @see gecos.extended.types.impl.TypesPackageImpl#getSCFixType()
	 * @generated
	 */
	int SC_FIX_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_FIX_TYPE__ANNOTATIONS = SC_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_FIX_TYPE__CONSTANT = SC_TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_FIX_TYPE__VOLATILE = SC_TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_FIX_TYPE__STORAGE_CLASS = SC_TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_FIX_TYPE__SIZE = SC_TYPE__SIZE;

	/**
	 * The feature id for the '<em><b>Bit Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_FIX_TYPE__BIT_WIDTH = SC_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Integer Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_FIX_TYPE__INTEGER_WIDTH = SC_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Quantification Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_FIX_TYPE__QUANTIFICATION_MODE = SC_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Overflow Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_FIX_TYPE__OVERFLOW_MODE = SC_TYPE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>SC Fix Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_FIX_TYPE_FEATURE_COUNT = SC_TYPE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link gecos.extended.types.impl.SCUFixTypeImpl <em>SCU Fix Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.extended.types.impl.SCUFixTypeImpl
	 * @see gecos.extended.types.impl.TypesPackageImpl#getSCUFixType()
	 * @generated
	 */
	int SCU_FIX_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCU_FIX_TYPE__ANNOTATIONS = SC_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCU_FIX_TYPE__CONSTANT = SC_TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCU_FIX_TYPE__VOLATILE = SC_TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCU_FIX_TYPE__STORAGE_CLASS = SC_TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCU_FIX_TYPE__SIZE = SC_TYPE__SIZE;

	/**
	 * The feature id for the '<em><b>Bit Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCU_FIX_TYPE__BIT_WIDTH = SC_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Integer Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCU_FIX_TYPE__INTEGER_WIDTH = SC_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Quantification Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCU_FIX_TYPE__QUANTIFICATION_MODE = SC_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Overflow Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCU_FIX_TYPE__OVERFLOW_MODE = SC_TYPE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>SCU Fix Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCU_FIX_TYPE_FEATURE_COUNT = SC_TYPE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link gecos.extended.types.impl.SCFixedTypeImpl <em>SC Fixed Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.extended.types.impl.SCFixedTypeImpl
	 * @see gecos.extended.types.impl.TypesPackageImpl#getSCFixedType()
	 * @generated
	 */
	int SC_FIXED_TYPE = 3;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_FIXED_TYPE__ANNOTATIONS = SC_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_FIXED_TYPE__CONSTANT = SC_TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_FIXED_TYPE__VOLATILE = SC_TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_FIXED_TYPE__STORAGE_CLASS = SC_TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_FIXED_TYPE__SIZE = SC_TYPE__SIZE;

	/**
	 * The feature id for the '<em><b>Bit Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_FIXED_TYPE__BIT_WIDTH = SC_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Integer Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_FIXED_TYPE__INTEGER_WIDTH = SC_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Quantification Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_FIXED_TYPE__QUANTIFICATION_MODE = SC_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Overflow Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_FIXED_TYPE__OVERFLOW_MODE = SC_TYPE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>SC Fixed Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_FIXED_TYPE_FEATURE_COUNT = SC_TYPE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link gecos.extended.types.impl.SCUFixedTypeImpl <em>SCU Fixed Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.extended.types.impl.SCUFixedTypeImpl
	 * @see gecos.extended.types.impl.TypesPackageImpl#getSCUFixedType()
	 * @generated
	 */
	int SCU_FIXED_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCU_FIXED_TYPE__ANNOTATIONS = SC_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCU_FIXED_TYPE__CONSTANT = SC_TYPE__CONSTANT;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCU_FIXED_TYPE__VOLATILE = SC_TYPE__VOLATILE;

	/**
	 * The feature id for the '<em><b>Storage Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCU_FIXED_TYPE__STORAGE_CLASS = SC_TYPE__STORAGE_CLASS;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCU_FIXED_TYPE__SIZE = SC_TYPE__SIZE;

	/**
	 * The feature id for the '<em><b>Bit Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCU_FIXED_TYPE__BIT_WIDTH = SC_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Integer Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCU_FIXED_TYPE__INTEGER_WIDTH = SC_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Quantification Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCU_FIXED_TYPE__QUANTIFICATION_MODE = SC_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Overflow Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCU_FIXED_TYPE__OVERFLOW_MODE = SC_TYPE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>SCU Fixed Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCU_FIXED_TYPE_FEATURE_COUNT = SC_TYPE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link gecos.extended.types.OverflowMode <em>Overflow Mode</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.extended.types.OverflowMode
	 * @see gecos.extended.types.impl.TypesPackageImpl#getOverflowMode()
	 * @generated
	 */
	int OVERFLOW_MODE = 5;

	/**
	 * The meta object id for the '{@link gecos.extended.types.QuantificationMode <em>Quantification Mode</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.extended.types.QuantificationMode
	 * @see gecos.extended.types.impl.TypesPackageImpl#getQuantificationMode()
	 * @generated
	 */
	int QUANTIFICATION_MODE = 6;


	/**
	 * Returns the meta object for class '{@link gecos.extended.types.SCType <em>SC Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SC Type</em>'.
	 * @see gecos.extended.types.SCType
	 * @generated
	 */
	EClass getSCType();

	/**
	 * Returns the meta object for class '{@link gecos.extended.types.SCFixType <em>SC Fix Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SC Fix Type</em>'.
	 * @see gecos.extended.types.SCFixType
	 * @generated
	 */
	EClass getSCFixType();

	/**
	 * Returns the meta object for the attribute '{@link gecos.extended.types.SCFixType#getBitWidth <em>Bit Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bit Width</em>'.
	 * @see gecos.extended.types.SCFixType#getBitWidth()
	 * @see #getSCFixType()
	 * @generated
	 */
	EAttribute getSCFixType_BitWidth();

	/**
	 * Returns the meta object for the attribute '{@link gecos.extended.types.SCFixType#getIntegerWidth <em>Integer Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Integer Width</em>'.
	 * @see gecos.extended.types.SCFixType#getIntegerWidth()
	 * @see #getSCFixType()
	 * @generated
	 */
	EAttribute getSCFixType_IntegerWidth();

	/**
	 * Returns the meta object for the attribute '{@link gecos.extended.types.SCFixType#getQuantificationMode <em>Quantification Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantification Mode</em>'.
	 * @see gecos.extended.types.SCFixType#getQuantificationMode()
	 * @see #getSCFixType()
	 * @generated
	 */
	EAttribute getSCFixType_QuantificationMode();

	/**
	 * Returns the meta object for the attribute '{@link gecos.extended.types.SCFixType#getOverflowMode <em>Overflow Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Overflow Mode</em>'.
	 * @see gecos.extended.types.SCFixType#getOverflowMode()
	 * @see #getSCFixType()
	 * @generated
	 */
	EAttribute getSCFixType_OverflowMode();

	/**
	 * Returns the meta object for class '{@link gecos.extended.types.SCUFixType <em>SCU Fix Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SCU Fix Type</em>'.
	 * @see gecos.extended.types.SCUFixType
	 * @generated
	 */
	EClass getSCUFixType();

	/**
	 * Returns the meta object for the attribute '{@link gecos.extended.types.SCUFixType#getBitWidth <em>Bit Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bit Width</em>'.
	 * @see gecos.extended.types.SCUFixType#getBitWidth()
	 * @see #getSCUFixType()
	 * @generated
	 */
	EAttribute getSCUFixType_BitWidth();

	/**
	 * Returns the meta object for the attribute '{@link gecos.extended.types.SCUFixType#getIntegerWidth <em>Integer Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Integer Width</em>'.
	 * @see gecos.extended.types.SCUFixType#getIntegerWidth()
	 * @see #getSCUFixType()
	 * @generated
	 */
	EAttribute getSCUFixType_IntegerWidth();

	/**
	 * Returns the meta object for the attribute '{@link gecos.extended.types.SCUFixType#getQuantificationMode <em>Quantification Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantification Mode</em>'.
	 * @see gecos.extended.types.SCUFixType#getQuantificationMode()
	 * @see #getSCUFixType()
	 * @generated
	 */
	EAttribute getSCUFixType_QuantificationMode();

	/**
	 * Returns the meta object for the attribute '{@link gecos.extended.types.SCUFixType#getOverflowMode <em>Overflow Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Overflow Mode</em>'.
	 * @see gecos.extended.types.SCUFixType#getOverflowMode()
	 * @see #getSCUFixType()
	 * @generated
	 */
	EAttribute getSCUFixType_OverflowMode();

	/**
	 * Returns the meta object for class '{@link gecos.extended.types.SCFixedType <em>SC Fixed Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SC Fixed Type</em>'.
	 * @see gecos.extended.types.SCFixedType
	 * @generated
	 */
	EClass getSCFixedType();

	/**
	 * Returns the meta object for the attribute '{@link gecos.extended.types.SCFixedType#getBitWidth <em>Bit Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bit Width</em>'.
	 * @see gecos.extended.types.SCFixedType#getBitWidth()
	 * @see #getSCFixedType()
	 * @generated
	 */
	EAttribute getSCFixedType_BitWidth();

	/**
	 * Returns the meta object for the attribute '{@link gecos.extended.types.SCFixedType#getIntegerWidth <em>Integer Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Integer Width</em>'.
	 * @see gecos.extended.types.SCFixedType#getIntegerWidth()
	 * @see #getSCFixedType()
	 * @generated
	 */
	EAttribute getSCFixedType_IntegerWidth();

	/**
	 * Returns the meta object for the attribute '{@link gecos.extended.types.SCFixedType#getQuantificationMode <em>Quantification Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantification Mode</em>'.
	 * @see gecos.extended.types.SCFixedType#getQuantificationMode()
	 * @see #getSCFixedType()
	 * @generated
	 */
	EAttribute getSCFixedType_QuantificationMode();

	/**
	 * Returns the meta object for the attribute '{@link gecos.extended.types.SCFixedType#getOverflowMode <em>Overflow Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Overflow Mode</em>'.
	 * @see gecos.extended.types.SCFixedType#getOverflowMode()
	 * @see #getSCFixedType()
	 * @generated
	 */
	EAttribute getSCFixedType_OverflowMode();

	/**
	 * Returns the meta object for class '{@link gecos.extended.types.SCUFixedType <em>SCU Fixed Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SCU Fixed Type</em>'.
	 * @see gecos.extended.types.SCUFixedType
	 * @generated
	 */
	EClass getSCUFixedType();

	/**
	 * Returns the meta object for the attribute '{@link gecos.extended.types.SCUFixedType#getBitWidth <em>Bit Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bit Width</em>'.
	 * @see gecos.extended.types.SCUFixedType#getBitWidth()
	 * @see #getSCUFixedType()
	 * @generated
	 */
	EAttribute getSCUFixedType_BitWidth();

	/**
	 * Returns the meta object for the attribute '{@link gecos.extended.types.SCUFixedType#getIntegerWidth <em>Integer Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Integer Width</em>'.
	 * @see gecos.extended.types.SCUFixedType#getIntegerWidth()
	 * @see #getSCUFixedType()
	 * @generated
	 */
	EAttribute getSCUFixedType_IntegerWidth();

	/**
	 * Returns the meta object for the attribute '{@link gecos.extended.types.SCUFixedType#getQuantificationMode <em>Quantification Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantification Mode</em>'.
	 * @see gecos.extended.types.SCUFixedType#getQuantificationMode()
	 * @see #getSCUFixedType()
	 * @generated
	 */
	EAttribute getSCUFixedType_QuantificationMode();

	/**
	 * Returns the meta object for the attribute '{@link gecos.extended.types.SCUFixedType#getOverflowMode <em>Overflow Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Overflow Mode</em>'.
	 * @see gecos.extended.types.SCUFixedType#getOverflowMode()
	 * @see #getSCUFixedType()
	 * @generated
	 */
	EAttribute getSCUFixedType_OverflowMode();

	/**
	 * Returns the meta object for enum '{@link gecos.extended.types.OverflowMode <em>Overflow Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Overflow Mode</em>'.
	 * @see gecos.extended.types.OverflowMode
	 * @generated
	 */
	EEnum getOverflowMode();

	/**
	 * Returns the meta object for enum '{@link gecos.extended.types.QuantificationMode <em>Quantification Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Quantification Mode</em>'.
	 * @see gecos.extended.types.QuantificationMode
	 * @generated
	 */
	EEnum getQuantificationMode();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TypesFactory getTypesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link gecos.extended.types.impl.SCTypeImpl <em>SC Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.extended.types.impl.SCTypeImpl
		 * @see gecos.extended.types.impl.TypesPackageImpl#getSCType()
		 * @generated
		 */
		EClass SC_TYPE = eINSTANCE.getSCType();

		/**
		 * The meta object literal for the '{@link gecos.extended.types.impl.SCFixTypeImpl <em>SC Fix Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.extended.types.impl.SCFixTypeImpl
		 * @see gecos.extended.types.impl.TypesPackageImpl#getSCFixType()
		 * @generated
		 */
		EClass SC_FIX_TYPE = eINSTANCE.getSCFixType();

		/**
		 * The meta object literal for the '<em><b>Bit Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SC_FIX_TYPE__BIT_WIDTH = eINSTANCE.getSCFixType_BitWidth();

		/**
		 * The meta object literal for the '<em><b>Integer Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SC_FIX_TYPE__INTEGER_WIDTH = eINSTANCE.getSCFixType_IntegerWidth();

		/**
		 * The meta object literal for the '<em><b>Quantification Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SC_FIX_TYPE__QUANTIFICATION_MODE = eINSTANCE.getSCFixType_QuantificationMode();

		/**
		 * The meta object literal for the '<em><b>Overflow Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SC_FIX_TYPE__OVERFLOW_MODE = eINSTANCE.getSCFixType_OverflowMode();

		/**
		 * The meta object literal for the '{@link gecos.extended.types.impl.SCUFixTypeImpl <em>SCU Fix Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.extended.types.impl.SCUFixTypeImpl
		 * @see gecos.extended.types.impl.TypesPackageImpl#getSCUFixType()
		 * @generated
		 */
		EClass SCU_FIX_TYPE = eINSTANCE.getSCUFixType();

		/**
		 * The meta object literal for the '<em><b>Bit Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCU_FIX_TYPE__BIT_WIDTH = eINSTANCE.getSCUFixType_BitWidth();

		/**
		 * The meta object literal for the '<em><b>Integer Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCU_FIX_TYPE__INTEGER_WIDTH = eINSTANCE.getSCUFixType_IntegerWidth();

		/**
		 * The meta object literal for the '<em><b>Quantification Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCU_FIX_TYPE__QUANTIFICATION_MODE = eINSTANCE.getSCUFixType_QuantificationMode();

		/**
		 * The meta object literal for the '<em><b>Overflow Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCU_FIX_TYPE__OVERFLOW_MODE = eINSTANCE.getSCUFixType_OverflowMode();

		/**
		 * The meta object literal for the '{@link gecos.extended.types.impl.SCFixedTypeImpl <em>SC Fixed Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.extended.types.impl.SCFixedTypeImpl
		 * @see gecos.extended.types.impl.TypesPackageImpl#getSCFixedType()
		 * @generated
		 */
		EClass SC_FIXED_TYPE = eINSTANCE.getSCFixedType();

		/**
		 * The meta object literal for the '<em><b>Bit Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SC_FIXED_TYPE__BIT_WIDTH = eINSTANCE.getSCFixedType_BitWidth();

		/**
		 * The meta object literal for the '<em><b>Integer Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SC_FIXED_TYPE__INTEGER_WIDTH = eINSTANCE.getSCFixedType_IntegerWidth();

		/**
		 * The meta object literal for the '<em><b>Quantification Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SC_FIXED_TYPE__QUANTIFICATION_MODE = eINSTANCE.getSCFixedType_QuantificationMode();

		/**
		 * The meta object literal for the '<em><b>Overflow Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SC_FIXED_TYPE__OVERFLOW_MODE = eINSTANCE.getSCFixedType_OverflowMode();

		/**
		 * The meta object literal for the '{@link gecos.extended.types.impl.SCUFixedTypeImpl <em>SCU Fixed Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.extended.types.impl.SCUFixedTypeImpl
		 * @see gecos.extended.types.impl.TypesPackageImpl#getSCUFixedType()
		 * @generated
		 */
		EClass SCU_FIXED_TYPE = eINSTANCE.getSCUFixedType();

		/**
		 * The meta object literal for the '<em><b>Bit Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCU_FIXED_TYPE__BIT_WIDTH = eINSTANCE.getSCUFixedType_BitWidth();

		/**
		 * The meta object literal for the '<em><b>Integer Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCU_FIXED_TYPE__INTEGER_WIDTH = eINSTANCE.getSCUFixedType_IntegerWidth();

		/**
		 * The meta object literal for the '<em><b>Quantification Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCU_FIXED_TYPE__QUANTIFICATION_MODE = eINSTANCE.getSCUFixedType_QuantificationMode();

		/**
		 * The meta object literal for the '<em><b>Overflow Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCU_FIXED_TYPE__OVERFLOW_MODE = eINSTANCE.getSCUFixedType_OverflowMode();

		/**
		 * The meta object literal for the '{@link gecos.extended.types.OverflowMode <em>Overflow Mode</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.extended.types.OverflowMode
		 * @see gecos.extended.types.impl.TypesPackageImpl#getOverflowMode()
		 * @generated
		 */
		EEnum OVERFLOW_MODE = eINSTANCE.getOverflowMode();

		/**
		 * The meta object literal for the '{@link gecos.extended.types.QuantificationMode <em>Quantification Mode</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.extended.types.QuantificationMode
		 * @see gecos.extended.types.impl.TypesPackageImpl#getQuantificationMode()
		 * @generated
		 */
		EEnum QUANTIFICATION_MODE = eINSTANCE.getQuantificationMode();

	}

} //TypesPackage