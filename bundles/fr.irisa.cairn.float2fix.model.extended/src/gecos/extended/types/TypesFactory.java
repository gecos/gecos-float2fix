package gecos.extended.types;


import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see gecos.extended.types.TypesPackage
 * @generated
 */
public interface TypesFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TypesFactory eINSTANCE = gecos.extended.types.impl.TypesFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>SC Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SC Type</em>'.
	 * @generated
	 */
	SCType createSCType();

	/**
	 * Returns a new object of class '<em>SC Fix Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SC Fix Type</em>'.
	 * @generated
	 */
	SCFixType createSCFixType();

	/**
	 * Returns a new object of class '<em>SCU Fix Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SCU Fix Type</em>'.
	 * @generated
	 */
	SCUFixType createSCUFixType();

	/**
	 * Returns a new object of class '<em>SC Fixed Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SC Fixed Type</em>'.
	 * @generated
	 */
	SCFixedType createSCFixedType();

	/**
	 * Returns a new object of class '<em>SCU Fixed Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SCU Fixed Type</em>'.
	 * @generated
	 */
	SCUFixedType createSCUFixedType();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TypesPackage getTypesPackage();

} //TypesFactory