/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package gecos.extended.types.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

import gecos.annotations.AnnotatedElement;
import gecos.core.GecosNode;
import gecos.extended.types.*;
import gecos.extended.types.SCFixType;
import gecos.extended.types.SCFixedType;
import gecos.extended.types.SCType;
import gecos.extended.types.SCUFixType;
import gecos.extended.types.SCUFixedType;
import gecos.extended.types.TypesPackage;
import gecos.types.BaseType;
import gecos.types.ScalarType;
import gecos.types.Type;
import gecos.types.TypesVisitable;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see gecos.extended.types.TypesPackage
 * @generated
 */
public class TypesSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TypesPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypesSwitch() {
		if (modelPackage == null) {
			modelPackage = TypesPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case TypesPackage.SC_TYPE: {
				SCType scType = (SCType)theEObject;
				T result = caseSCType(scType);
				if (result == null) result = caseBaseType(scType);
				if (result == null) result = caseScalarType(scType);
				if (result == null) result = caseType(scType);
				if (result == null) result = caseAnnotatedElement(scType);
				if (result == null) result = caseTypesVisitable(scType);
				if (result == null) result = caseGecosNode(scType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.SC_FIX_TYPE: {
				SCFixType scFixType = (SCFixType)theEObject;
				T result = caseSCFixType(scFixType);
				if (result == null) result = caseSCType(scFixType);
				if (result == null) result = caseBaseType(scFixType);
				if (result == null) result = caseScalarType(scFixType);
				if (result == null) result = caseType(scFixType);
				if (result == null) result = caseAnnotatedElement(scFixType);
				if (result == null) result = caseTypesVisitable(scFixType);
				if (result == null) result = caseGecosNode(scFixType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.SCU_FIX_TYPE: {
				SCUFixType scuFixType = (SCUFixType)theEObject;
				T result = caseSCUFixType(scuFixType);
				if (result == null) result = caseSCType(scuFixType);
				if (result == null) result = caseBaseType(scuFixType);
				if (result == null) result = caseScalarType(scuFixType);
				if (result == null) result = caseType(scuFixType);
				if (result == null) result = caseAnnotatedElement(scuFixType);
				if (result == null) result = caseTypesVisitable(scuFixType);
				if (result == null) result = caseGecosNode(scuFixType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.SC_FIXED_TYPE: {
				SCFixedType scFixedType = (SCFixedType)theEObject;
				T result = caseSCFixedType(scFixedType);
				if (result == null) result = caseSCType(scFixedType);
				if (result == null) result = caseBaseType(scFixedType);
				if (result == null) result = caseScalarType(scFixedType);
				if (result == null) result = caseType(scFixedType);
				if (result == null) result = caseAnnotatedElement(scFixedType);
				if (result == null) result = caseTypesVisitable(scFixedType);
				if (result == null) result = caseGecosNode(scFixedType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TypesPackage.SCU_FIXED_TYPE: {
				SCUFixedType scuFixedType = (SCUFixedType)theEObject;
				T result = caseSCUFixedType(scuFixedType);
				if (result == null) result = caseSCType(scuFixedType);
				if (result == null) result = caseBaseType(scuFixedType);
				if (result == null) result = caseScalarType(scuFixedType);
				if (result == null) result = caseType(scuFixedType);
				if (result == null) result = caseAnnotatedElement(scuFixedType);
				if (result == null) result = caseTypesVisitable(scuFixedType);
				if (result == null) result = caseGecosNode(scuFixedType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SC Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SC Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSCType(SCType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SC Fix Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SC Fix Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSCFixType(SCFixType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SCU Fix Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SCU Fix Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSCUFixType(SCUFixType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SC Fixed Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SC Fixed Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSCFixedType(SCFixedType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SCU Fixed Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SCU Fixed Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSCUFixedType(SCUFixedType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Gecos Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gecos Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGecosNode(GecosNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotatedElement(AnnotatedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visitable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypesVisitable(TypesVisitable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseType(Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Scalar Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Scalar Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScalarType(ScalarType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseType(BaseType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //TypesSwitch
