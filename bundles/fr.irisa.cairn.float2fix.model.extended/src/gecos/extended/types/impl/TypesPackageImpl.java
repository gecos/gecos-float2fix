/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package gecos.extended.types.impl;

import gecos.annotations.AnnotationsPackage;
import gecos.blocks.BlocksPackage;
import gecos.core.CorePackage;
import gecos.dag.DagPackage;
import gecos.extended.instrs.impl.InstrsPackageImpl;
import gecos.extended.types.OverflowMode;
import gecos.extended.types.QuantificationMode;
import gecos.extended.types.SCFixType;
import gecos.extended.types.SCFixedType;
import gecos.extended.types.SCType;
import gecos.extended.types.SCUFixType;
import gecos.extended.types.SCUFixedType;
import gecos.extended.types.TypesFactory;
import gecos.extended.types.TypesPackage;
import gecos.gecosproject.GecosprojectPackage;
import gecos.instrs.InstrsPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TypesPackageImpl extends EPackageImpl implements TypesPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scFixTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scuFixTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scFixedTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scuFixedTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum overflowModeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum quantificationModeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see gecos.extended.types.TypesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TypesPackageImpl() {
		super(eNS_URI, TypesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TypesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TypesPackage init() {
		if (isInited) return (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

		// Obtain or create and register package
		TypesPackageImpl theTypesPackage = (TypesPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TypesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TypesPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		AnnotationsPackage.eINSTANCE.eClass();
		BlocksPackage.eINSTANCE.eClass();
		CorePackage.eINSTANCE.eClass();
		DagPackage.eINSTANCE.eClass();
		InstrsPackage.eINSTANCE.eClass();
		gecos.types.TypesPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		InstrsPackageImpl theInstrsPackage_1 = (InstrsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(gecos.extended.instrs.InstrsPackage.eNS_URI) instanceof InstrsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(gecos.extended.instrs.InstrsPackage.eNS_URI) : gecos.extended.instrs.InstrsPackage.eINSTANCE);

		// Create package meta-data objects
		theTypesPackage.createPackageContents();
		theInstrsPackage_1.createPackageContents();

		// Initialize created meta-data
		theTypesPackage.initializePackageContents();
		theInstrsPackage_1.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTypesPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TypesPackage.eNS_URI, theTypesPackage);
		return theTypesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSCType() {
		return scTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSCFixType() {
		return scFixTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSCFixType_BitWidth() {
		return (EAttribute)scFixTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSCFixType_IntegerWidth() {
		return (EAttribute)scFixTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSCFixType_QuantificationMode() {
		return (EAttribute)scFixTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSCFixType_OverflowMode() {
		return (EAttribute)scFixTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSCUFixType() {
		return scuFixTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSCUFixType_BitWidth() {
		return (EAttribute)scuFixTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSCUFixType_IntegerWidth() {
		return (EAttribute)scuFixTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSCUFixType_QuantificationMode() {
		return (EAttribute)scuFixTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSCUFixType_OverflowMode() {
		return (EAttribute)scuFixTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSCFixedType() {
		return scFixedTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSCFixedType_BitWidth() {
		return (EAttribute)scFixedTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSCFixedType_IntegerWidth() {
		return (EAttribute)scFixedTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSCFixedType_QuantificationMode() {
		return (EAttribute)scFixedTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSCFixedType_OverflowMode() {
		return (EAttribute)scFixedTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSCUFixedType() {
		return scuFixedTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSCUFixedType_BitWidth() {
		return (EAttribute)scuFixedTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSCUFixedType_IntegerWidth() {
		return (EAttribute)scuFixedTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSCUFixedType_QuantificationMode() {
		return (EAttribute)scuFixedTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSCUFixedType_OverflowMode() {
		return (EAttribute)scuFixedTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getOverflowMode() {
		return overflowModeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getQuantificationMode() {
		return quantificationModeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypesFactory getTypesFactory() {
		return (TypesFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		scTypeEClass = createEClass(SC_TYPE);

		scFixTypeEClass = createEClass(SC_FIX_TYPE);
		createEAttribute(scFixTypeEClass, SC_FIX_TYPE__BIT_WIDTH);
		createEAttribute(scFixTypeEClass, SC_FIX_TYPE__INTEGER_WIDTH);
		createEAttribute(scFixTypeEClass, SC_FIX_TYPE__QUANTIFICATION_MODE);
		createEAttribute(scFixTypeEClass, SC_FIX_TYPE__OVERFLOW_MODE);

		scuFixTypeEClass = createEClass(SCU_FIX_TYPE);
		createEAttribute(scuFixTypeEClass, SCU_FIX_TYPE__BIT_WIDTH);
		createEAttribute(scuFixTypeEClass, SCU_FIX_TYPE__INTEGER_WIDTH);
		createEAttribute(scuFixTypeEClass, SCU_FIX_TYPE__QUANTIFICATION_MODE);
		createEAttribute(scuFixTypeEClass, SCU_FIX_TYPE__OVERFLOW_MODE);

		scFixedTypeEClass = createEClass(SC_FIXED_TYPE);
		createEAttribute(scFixedTypeEClass, SC_FIXED_TYPE__BIT_WIDTH);
		createEAttribute(scFixedTypeEClass, SC_FIXED_TYPE__INTEGER_WIDTH);
		createEAttribute(scFixedTypeEClass, SC_FIXED_TYPE__QUANTIFICATION_MODE);
		createEAttribute(scFixedTypeEClass, SC_FIXED_TYPE__OVERFLOW_MODE);

		scuFixedTypeEClass = createEClass(SCU_FIXED_TYPE);
		createEAttribute(scuFixedTypeEClass, SCU_FIXED_TYPE__BIT_WIDTH);
		createEAttribute(scuFixedTypeEClass, SCU_FIXED_TYPE__INTEGER_WIDTH);
		createEAttribute(scuFixedTypeEClass, SCU_FIXED_TYPE__QUANTIFICATION_MODE);
		createEAttribute(scuFixedTypeEClass, SCU_FIXED_TYPE__OVERFLOW_MODE);

		// Create enums
		overflowModeEEnum = createEEnum(OVERFLOW_MODE);
		quantificationModeEEnum = createEEnum(QUANTIFICATION_MODE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		gecos.types.TypesPackage theTypesPackage_1 = (gecos.types.TypesPackage)EPackage.Registry.INSTANCE.getEPackage(gecos.types.TypesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		scTypeEClass.getESuperTypes().add(theTypesPackage_1.getBaseType());
		scTypeEClass.getESuperTypes().add(theTypesPackage_1.getTypesVisitable());
		scFixTypeEClass.getESuperTypes().add(this.getSCType());
		scFixTypeEClass.getESuperTypes().add(theTypesPackage_1.getTypesVisitable());
		scuFixTypeEClass.getESuperTypes().add(this.getSCType());
		scuFixTypeEClass.getESuperTypes().add(theTypesPackage_1.getTypesVisitable());
		scFixedTypeEClass.getESuperTypes().add(this.getSCType());
		scuFixedTypeEClass.getESuperTypes().add(this.getSCType());

		// Initialize classes and features; add operations and parameters
		initEClass(scTypeEClass, SCType.class, "SCType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = addEOperation(scTypeEClass, null, "accept", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTypesPackage_1.getTypesVisitor(), "visitor", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(scFixTypeEClass, SCFixType.class, "SCFixType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSCFixType_BitWidth(), ecorePackage.getEString(), "bitWidth", null, 0, 1, SCFixType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSCFixType_IntegerWidth(), ecorePackage.getEString(), "integerWidth", null, 0, 1, SCFixType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSCFixType_QuantificationMode(), this.getQuantificationMode(), "quantificationMode", null, 0, 1, SCFixType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSCFixType_OverflowMode(), this.getOverflowMode(), "overflowMode", null, 0, 1, SCFixType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(scFixTypeEClass, null, "accept", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTypesPackage_1.getTypesVisitor(), "visitor", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(scuFixTypeEClass, SCUFixType.class, "SCUFixType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSCUFixType_BitWidth(), ecorePackage.getEString(), "bitWidth", null, 0, 1, SCUFixType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSCUFixType_IntegerWidth(), ecorePackage.getEString(), "integerWidth", null, 0, 1, SCUFixType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSCUFixType_QuantificationMode(), this.getQuantificationMode(), "quantificationMode", null, 0, 1, SCUFixType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSCUFixType_OverflowMode(), this.getOverflowMode(), "overflowMode", null, 0, 1, SCUFixType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(scuFixTypeEClass, null, "accept", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTypesPackage_1.getTypesVisitor(), "visitor", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(scFixedTypeEClass, SCFixedType.class, "SCFixedType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSCFixedType_BitWidth(), ecorePackage.getEInt(), "bitWidth", "0", 0, 1, SCFixedType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSCFixedType_IntegerWidth(), ecorePackage.getEInt(), "integerWidth", null, 0, 1, SCFixedType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSCFixedType_QuantificationMode(), this.getQuantificationMode(), "quantificationMode", null, 0, 1, SCFixedType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSCFixedType_OverflowMode(), this.getOverflowMode(), "overflowMode", null, 0, 1, SCFixedType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(scFixedTypeEClass, null, "accept", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTypesPackage_1.getTypesVisitor(), "visitor", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(scuFixedTypeEClass, SCUFixedType.class, "SCUFixedType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSCUFixedType_BitWidth(), ecorePackage.getEInt(), "bitWidth", "0", 0, 1, SCUFixedType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSCUFixedType_IntegerWidth(), ecorePackage.getEInt(), "integerWidth", null, 0, 1, SCUFixedType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSCUFixedType_QuantificationMode(), this.getQuantificationMode(), "quantificationMode", null, 0, 1, SCUFixedType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSCUFixedType_OverflowMode(), this.getOverflowMode(), "overflowMode", null, 0, 1, SCUFixedType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(scuFixedTypeEClass, null, "accept", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theTypesPackage_1.getTypesVisitor(), "visitor", 0, 1, IS_UNIQUE, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(overflowModeEEnum, OverflowMode.class, "OverflowMode");
		addEEnumLiteral(overflowModeEEnum, OverflowMode.SC_SAT);
		addEEnumLiteral(overflowModeEEnum, OverflowMode.SC_SAT_ZERO);
		addEEnumLiteral(overflowModeEEnum, OverflowMode.SC_SAT_SYM);
		addEEnumLiteral(overflowModeEEnum, OverflowMode.SC_WRAP);

		initEEnum(quantificationModeEEnum, QuantificationMode.class, "QuantificationMode");
		addEEnumLiteral(quantificationModeEEnum, QuantificationMode.SC_TRN);
		addEEnumLiteral(quantificationModeEEnum, QuantificationMode.SC_TRN_ZERO);
		addEEnumLiteral(quantificationModeEEnum, QuantificationMode.SC_RND);
		addEEnumLiteral(quantificationModeEEnum, QuantificationMode.SC_RND_ZERO);
		addEEnumLiteral(quantificationModeEEnum, QuantificationMode.SC_RND_INF);
		addEEnumLiteral(quantificationModeEEnum, QuantificationMode.SC_RND_MIN_INF);
		addEEnumLiteral(quantificationModeEEnum, QuantificationMode.SC_RND_CONV);

		// Create resource
		createResource(eNS_URI);
	}

} //TypesPackageImpl
