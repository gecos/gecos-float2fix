/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package gecos.extended.types.impl;

import gecos.extended.types.SCType;
import gecos.extended.types.TypesPackage;
import gecos.types.impl.BaseTypeImpl;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SC Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SCTypeImpl extends BaseTypeImpl implements SCType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SCTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypesPackage.Literals.SC_TYPE;
	}

} //SCTypeImpl
