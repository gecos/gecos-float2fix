/**
 */
package gecos.extended.types.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import gecos.extended.types.OverflowMode;
import gecos.extended.types.QuantificationMode;
import gecos.extended.types.SCFixedType;
import gecos.extended.types.TypesPackage;
import gecos.types.Type;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SC Fixed Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link gecos.extended.types.impl.SCFixedTypeImpl#getBitWidth <em>Bit Width</em>}</li>
 *   <li>{@link gecos.extended.types.impl.SCFixedTypeImpl#getIntegerWidth <em>Integer Width</em>}</li>
 *   <li>{@link gecos.extended.types.impl.SCFixedTypeImpl#getQuantificationMode <em>Quantification Mode</em>}</li>
 *   <li>{@link gecos.extended.types.impl.SCFixedTypeImpl#getOverflowMode <em>Overflow Mode</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SCFixedTypeImpl extends SCTypeImpl implements SCFixedType {
	/**
	 * The default value of the '{@link #getBitWidth() <em>Bit Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBitWidth()
	 * @generated
	 * @ordered
	 */
	protected static final int BIT_WIDTH_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getBitWidth() <em>Bit Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBitWidth()
	 * @generated
	 * @ordered
	 */
	protected int bitWidth = BIT_WIDTH_EDEFAULT;
	/**
	 * The default value of the '{@link #getIntegerWidth() <em>Integer Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegerWidth()
	 * @generated
	 * @ordered
	 */
	protected static final int INTEGER_WIDTH_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getIntegerWidth() <em>Integer Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegerWidth()
	 * @generated
	 * @ordered
	 */
	protected int integerWidth = INTEGER_WIDTH_EDEFAULT;
	/**
	 * The default value of the '{@link #getQuantificationMode() <em>Quantification Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantificationMode()
	 * @generated
	 * @ordered
	 */
	protected static final QuantificationMode QUANTIFICATION_MODE_EDEFAULT = QuantificationMode.SC_TRN;
	/**
	 * The cached value of the '{@link #getQuantificationMode() <em>Quantification Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantificationMode()
	 * @generated
	 * @ordered
	 */
	protected QuantificationMode quantificationMode = QUANTIFICATION_MODE_EDEFAULT;
	/**
	 * The default value of the '{@link #getOverflowMode() <em>Overflow Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverflowMode()
	 * @generated
	 * @ordered
	 */
	protected static final OverflowMode OVERFLOW_MODE_EDEFAULT = OverflowMode.SC_SAT;
	/**
	 * The cached value of the '{@link #getOverflowMode() <em>Overflow Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverflowMode()
	 * @generated
	 * @ordered
	 */
	protected OverflowMode overflowMode = OVERFLOW_MODE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SCFixedTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypesPackage.Literals.SC_FIXED_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBitWidth() {
		return bitWidth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBitWidth(int newBitWidth) {
		int oldBitWidth = bitWidth;
		bitWidth = newBitWidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.SC_FIXED_TYPE__BIT_WIDTH, oldBitWidth, bitWidth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getIntegerWidth() {
		return integerWidth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntegerWidth(int newIntegerWidth) {
		int oldIntegerWidth = integerWidth;
		integerWidth = newIntegerWidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.SC_FIXED_TYPE__INTEGER_WIDTH, oldIntegerWidth, integerWidth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QuantificationMode getQuantificationMode() {
		return quantificationMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantificationMode(QuantificationMode newQuantificationMode) {
		QuantificationMode oldQuantificationMode = quantificationMode;
		quantificationMode = newQuantificationMode == null ? QUANTIFICATION_MODE_EDEFAULT : newQuantificationMode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.SC_FIXED_TYPE__QUANTIFICATION_MODE, oldQuantificationMode, quantificationMode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OverflowMode getOverflowMode() {
		return overflowMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOverflowMode(OverflowMode newOverflowMode) {
		OverflowMode oldOverflowMode = overflowMode;
		overflowMode = newOverflowMode == null ? OVERFLOW_MODE_EDEFAULT : newOverflowMode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.SC_FIXED_TYPE__OVERFLOW_MODE, oldOverflowMode, overflowMode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TypesPackage.SC_FIXED_TYPE__BIT_WIDTH:
				return getBitWidth();
			case TypesPackage.SC_FIXED_TYPE__INTEGER_WIDTH:
				return getIntegerWidth();
			case TypesPackage.SC_FIXED_TYPE__QUANTIFICATION_MODE:
				return getQuantificationMode();
			case TypesPackage.SC_FIXED_TYPE__OVERFLOW_MODE:
				return getOverflowMode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TypesPackage.SC_FIXED_TYPE__BIT_WIDTH:
				setBitWidth((Integer)newValue);
				return;
			case TypesPackage.SC_FIXED_TYPE__INTEGER_WIDTH:
				setIntegerWidth((Integer)newValue);
				return;
			case TypesPackage.SC_FIXED_TYPE__QUANTIFICATION_MODE:
				setQuantificationMode((QuantificationMode)newValue);
				return;
			case TypesPackage.SC_FIXED_TYPE__OVERFLOW_MODE:
				setOverflowMode((OverflowMode)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TypesPackage.SC_FIXED_TYPE__BIT_WIDTH:
				setBitWidth(BIT_WIDTH_EDEFAULT);
				return;
			case TypesPackage.SC_FIXED_TYPE__INTEGER_WIDTH:
				setIntegerWidth(INTEGER_WIDTH_EDEFAULT);
				return;
			case TypesPackage.SC_FIXED_TYPE__QUANTIFICATION_MODE:
				setQuantificationMode(QUANTIFICATION_MODE_EDEFAULT);
				return;
			case TypesPackage.SC_FIXED_TYPE__OVERFLOW_MODE:
				setOverflowMode(OVERFLOW_MODE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TypesPackage.SC_FIXED_TYPE__BIT_WIDTH:
				return bitWidth != BIT_WIDTH_EDEFAULT;
			case TypesPackage.SC_FIXED_TYPE__INTEGER_WIDTH:
				return integerWidth != INTEGER_WIDTH_EDEFAULT;
			case TypesPackage.SC_FIXED_TYPE__QUANTIFICATION_MODE:
				return quantificationMode != QUANTIFICATION_MODE_EDEFAULT;
			case TypesPackage.SC_FIXED_TYPE__OVERFLOW_MODE:
				return overflowMode != OVERFLOW_MODE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public String toString() {
		return "sc_fixed<" + this.bitWidth +"," + this.integerWidth + ", " + this.quantificationMode + ", " + this.overflowMode + ">";
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public boolean isEqual(Type other){
		if (other instanceof SCFixedType) {
			SCFixedType scfixed= (SCFixedType) other;
			boolean isSameBW = (scfixed.getBitWidth()==getBitWidth());
			boolean isSameFrac = (scfixed.getIntegerWidth()==getIntegerWidth());
			boolean isSameOvf = (scfixed.getOverflowMode()==getOverflowMode());
			boolean isSameQuantification = (scfixed.getQuantificationMode()==getQuantificationMode());
			return isSameBW && isSameOvf && isSameFrac && isSameQuantification;
		}
		return false;
	}
} //SCFixedTypeImpl
