/**
 */
package gecos.extended.instrs;

import gecos.instrs.Instruction;
import gecos.types.Type;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>New Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.extended.instrs.NewInstruction#getObject <em>Object</em>}</li>
 *   <li>{@link gecos.extended.instrs.NewInstruction#getParameters <em>Parameters</em>}</li>
 * </ul>
 *
 * @see gecos.extended.instrs.InstrsPackage#getNewInstruction()
 * @model
 * @generated
 */
public interface NewInstruction extends Instruction {
	/**
	 * Returns the value of the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object</em>' reference.
	 * @see #setObject(Type)
	 * @see gecos.extended.instrs.InstrsPackage#getNewInstruction_Object()
	 * @model
	 * @generated
	 */
	Type getObject();

	/**
	 * Sets the value of the '{@link gecos.extended.instrs.NewInstruction#getObject <em>Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object</em>' reference.
	 * @see #getObject()
	 * @generated
	 */
	void setObject(Type value);

	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.instrs.Instruction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference list.
	 * @see gecos.extended.instrs.InstrsPackage#getNewInstruction_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<Instruction> getParameters();

} // NewInstruction
