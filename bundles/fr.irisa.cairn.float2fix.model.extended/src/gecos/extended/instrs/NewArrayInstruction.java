/**
 */
package gecos.extended.instrs;

import gecos.instrs.Instruction;
import gecos.types.Type;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>New Array Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link gecos.extended.instrs.NewArrayInstruction#getObject <em>Object</em>}</li>
 *   <li>{@link gecos.extended.instrs.NewArrayInstruction#getIndex <em>Index</em>}</li>
 * </ul>
 *
 * @see gecos.extended.instrs.InstrsPackage#getNewArrayInstruction()
 * @model
 * @generated
 */
public interface NewArrayInstruction extends Instruction {
	/**
	 * Returns the value of the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object</em>' reference.
	 * @see #setObject(Type)
	 * @see gecos.extended.instrs.InstrsPackage#getNewArrayInstruction_Object()
	 * @model
	 * @generated
	 */
	Type getObject();

	/**
	 * Sets the value of the '{@link gecos.extended.instrs.NewArrayInstruction#getObject <em>Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object</em>' reference.
	 * @see #getObject()
	 * @generated
	 */
	void setObject(Type value);

	/**
	 * Returns the value of the '<em><b>Index</b></em>' containment reference list.
	 * The list contents are of type {@link gecos.instrs.Instruction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Index</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Index</em>' containment reference list.
	 * @see gecos.extended.instrs.InstrsPackage#getNewArrayInstruction_Index()
	 * @model containment="true"
	 * @generated
	 */
	EList<Instruction> getIndex();

} // NewArrayInstruction
