/**
 */
package gecos.extended.instrs;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see gecos.extended.instrs.InstrsPackage
 * @generated
 */
public interface InstrsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	InstrsFactory eINSTANCE = gecos.extended.instrs.impl.InstrsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>New Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>New Instruction</em>'.
	 * @generated
	 */
	NewInstruction createNewInstruction();

	/**
	 * Returns a new object of class '<em>New Array Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>New Array Instruction</em>'.
	 * @generated
	 */
	NewArrayInstruction createNewArrayInstruction();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	InstrsPackage getInstrsPackage();

} //InstrsFactory
