/**
 */
package gecos.extended.instrs.impl;

import gecos.annotations.AnnotationsPackage;
import gecos.blocks.BlocksPackage;
import gecos.core.CorePackage;
import gecos.dag.DagPackage;
import gecos.extended.instrs.InstrsFactory;
import gecos.extended.instrs.InstrsPackage;
import gecos.extended.instrs.NewArrayInstruction;
import gecos.extended.instrs.NewInstruction;
import gecos.extended.types.impl.TypesPackageImpl;
import gecos.gecosproject.GecosprojectPackage;
import gecos.types.TypesPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class InstrsPackageImpl extends EPackageImpl implements InstrsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass newInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass newArrayInstructionEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see gecos.extended.instrs.InstrsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private InstrsPackageImpl() {
		super(eNS_URI, InstrsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link InstrsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static InstrsPackage init() {
		if (isInited) return (InstrsPackage)EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI);

		// Obtain or create and register package
		InstrsPackageImpl theInstrsPackage = (InstrsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof InstrsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new InstrsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		AnnotationsPackage.eINSTANCE.eClass();
		BlocksPackage.eINSTANCE.eClass();
		CorePackage.eINSTANCE.eClass();
		DagPackage.eINSTANCE.eClass();
		gecos.instrs.InstrsPackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		TypesPackageImpl theTypesPackage_1 = (TypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(gecos.extended.types.TypesPackage.eNS_URI) instanceof TypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(gecos.extended.types.TypesPackage.eNS_URI) : gecos.extended.types.TypesPackage.eINSTANCE);

		// Create package meta-data objects
		theInstrsPackage.createPackageContents();
		theTypesPackage_1.createPackageContents();

		// Initialize created meta-data
		theInstrsPackage.initializePackageContents();
		theTypesPackage_1.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theInstrsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(InstrsPackage.eNS_URI, theInstrsPackage);
		return theInstrsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNewInstruction() {
		return newInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNewInstruction_Object() {
		return (EReference)newInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNewInstruction_Parameters() {
		return (EReference)newInstructionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNewArrayInstruction() {
		return newArrayInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNewArrayInstruction_Object() {
		return (EReference)newArrayInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNewArrayInstruction_Index() {
		return (EReference)newArrayInstructionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InstrsFactory getInstrsFactory() {
		return (InstrsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		newInstructionEClass = createEClass(NEW_INSTRUCTION);
		createEReference(newInstructionEClass, NEW_INSTRUCTION__OBJECT);
		createEReference(newInstructionEClass, NEW_INSTRUCTION__PARAMETERS);

		newArrayInstructionEClass = createEClass(NEW_ARRAY_INSTRUCTION);
		createEReference(newArrayInstructionEClass, NEW_ARRAY_INSTRUCTION__OBJECT);
		createEReference(newArrayInstructionEClass, NEW_ARRAY_INSTRUCTION__INDEX);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		gecos.instrs.InstrsPackage theInstrsPackage_1 = (gecos.instrs.InstrsPackage)EPackage.Registry.INSTANCE.getEPackage(gecos.instrs.InstrsPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		newInstructionEClass.getESuperTypes().add(theInstrsPackage_1.getInstruction());
		newArrayInstructionEClass.getESuperTypes().add(theInstrsPackage_1.getInstruction());

		// Initialize classes and features; add operations and parameters
		initEClass(newInstructionEClass, NewInstruction.class, "NewInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNewInstruction_Object(), theTypesPackage.getType(), null, "object", null, 0, 1, NewInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNewInstruction_Parameters(), theInstrsPackage_1.getInstruction(), null, "parameters", null, 0, -1, NewInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(newArrayInstructionEClass, NewArrayInstruction.class, "NewArrayInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNewArrayInstruction_Object(), theTypesPackage.getType(), null, "object", null, 0, 1, NewArrayInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNewArrayInstruction_Index(), theInstrsPackage_1.getInstruction(), null, "index", null, 0, -1, NewArrayInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //InstrsPackageImpl
