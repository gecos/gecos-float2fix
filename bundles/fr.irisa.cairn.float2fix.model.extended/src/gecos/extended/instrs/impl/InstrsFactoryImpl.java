/**
 */
package gecos.extended.instrs.impl;

import gecos.extended.instrs.InstrsFactory;
import gecos.extended.instrs.InstrsPackage;
import gecos.extended.instrs.NewArrayInstruction;
import gecos.extended.instrs.NewInstruction;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class InstrsFactoryImpl extends EFactoryImpl implements InstrsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static InstrsFactory init() {
		try {
			InstrsFactory theInstrsFactory = (InstrsFactory)EPackage.Registry.INSTANCE.getEFactory(InstrsPackage.eNS_URI);
			if (theInstrsFactory != null) {
				return theInstrsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new InstrsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InstrsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case InstrsPackage.NEW_INSTRUCTION: return createNewInstruction();
			case InstrsPackage.NEW_ARRAY_INSTRUCTION: return createNewArrayInstruction();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NewInstruction createNewInstruction() {
		NewInstructionImpl newInstruction = new NewInstructionImpl();
		return newInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NewArrayInstruction createNewArrayInstruction() {
		NewArrayInstructionImpl newArrayInstruction = new NewArrayInstructionImpl();
		return newArrayInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InstrsPackage getInstrsPackage() {
		return (InstrsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static InstrsPackage getPackage() {
		return InstrsPackage.eINSTANCE;
	}

} //InstrsFactoryImpl
