/**
 */
package gecos.extended.instrs;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see gecos.extended.instrs.InstrsFactory
 * @model kind="package"
 * @generated
 */
public interface InstrsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "instrs";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://idfix.gforge.inria.fr/extended";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "instrs";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	InstrsPackage eINSTANCE = gecos.extended.instrs.impl.InstrsPackageImpl.init();

	/**
	 * The meta object id for the '{@link gecos.extended.instrs.impl.NewInstructionImpl <em>New Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.extended.instrs.impl.NewInstructionImpl
	 * @see gecos.extended.instrs.impl.InstrsPackageImpl#getNewInstruction()
	 * @generated
	 */
	int NEW_INSTRUCTION = 0;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_INSTRUCTION__ANNOTATIONS = gecos.instrs.InstrsPackage.INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_INSTRUCTION__INTERNAL_CACHED_TYPE = gecos.instrs.InstrsPackage.INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_INSTRUCTION__OBJECT = gecos.instrs.InstrsPackage.INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_INSTRUCTION__PARAMETERS = gecos.instrs.InstrsPackage.INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>New Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_INSTRUCTION_FEATURE_COUNT = gecos.instrs.InstrsPackage.INSTRUCTION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link gecos.extended.instrs.impl.NewArrayInstructionImpl <em>New Array Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gecos.extended.instrs.impl.NewArrayInstructionImpl
	 * @see gecos.extended.instrs.impl.InstrsPackageImpl#getNewArrayInstruction()
	 * @generated
	 */
	int NEW_ARRAY_INSTRUCTION = 1;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_ARRAY_INSTRUCTION__ANNOTATIONS = gecos.instrs.InstrsPackage.INSTRUCTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>internal cached type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_ARRAY_INSTRUCTION__INTERNAL_CACHED_TYPE = gecos.instrs.InstrsPackage.INSTRUCTION__INTERNAL_CACHED_TYPE;

	/**
	 * The feature id for the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_ARRAY_INSTRUCTION__OBJECT = gecos.instrs.InstrsPackage.INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Index</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_ARRAY_INSTRUCTION__INDEX = gecos.instrs.InstrsPackage.INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>New Array Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_ARRAY_INSTRUCTION_FEATURE_COUNT = gecos.instrs.InstrsPackage.INSTRUCTION_FEATURE_COUNT + 2;


	/**
	 * Returns the meta object for class '{@link gecos.extended.instrs.NewInstruction <em>New Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>New Instruction</em>'.
	 * @see gecos.extended.instrs.NewInstruction
	 * @generated
	 */
	EClass getNewInstruction();

	/**
	 * Returns the meta object for the reference '{@link gecos.extended.instrs.NewInstruction#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object</em>'.
	 * @see gecos.extended.instrs.NewInstruction#getObject()
	 * @see #getNewInstruction()
	 * @generated
	 */
	EReference getNewInstruction_Object();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.extended.instrs.NewInstruction#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see gecos.extended.instrs.NewInstruction#getParameters()
	 * @see #getNewInstruction()
	 * @generated
	 */
	EReference getNewInstruction_Parameters();

	/**
	 * Returns the meta object for class '{@link gecos.extended.instrs.NewArrayInstruction <em>New Array Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>New Array Instruction</em>'.
	 * @see gecos.extended.instrs.NewArrayInstruction
	 * @generated
	 */
	EClass getNewArrayInstruction();

	/**
	 * Returns the meta object for the reference '{@link gecos.extended.instrs.NewArrayInstruction#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object</em>'.
	 * @see gecos.extended.instrs.NewArrayInstruction#getObject()
	 * @see #getNewArrayInstruction()
	 * @generated
	 */
	EReference getNewArrayInstruction_Object();

	/**
	 * Returns the meta object for the containment reference list '{@link gecos.extended.instrs.NewArrayInstruction#getIndex <em>Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Index</em>'.
	 * @see gecos.extended.instrs.NewArrayInstruction#getIndex()
	 * @see #getNewArrayInstruction()
	 * @generated
	 */
	EReference getNewArrayInstruction_Index();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	InstrsFactory getInstrsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link gecos.extended.instrs.impl.NewInstructionImpl <em>New Instruction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.extended.instrs.impl.NewInstructionImpl
		 * @see gecos.extended.instrs.impl.InstrsPackageImpl#getNewInstruction()
		 * @generated
		 */
		EClass NEW_INSTRUCTION = eINSTANCE.getNewInstruction();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEW_INSTRUCTION__OBJECT = eINSTANCE.getNewInstruction_Object();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEW_INSTRUCTION__PARAMETERS = eINSTANCE.getNewInstruction_Parameters();

		/**
		 * The meta object literal for the '{@link gecos.extended.instrs.impl.NewArrayInstructionImpl <em>New Array Instruction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gecos.extended.instrs.impl.NewArrayInstructionImpl
		 * @see gecos.extended.instrs.impl.InstrsPackageImpl#getNewArrayInstruction()
		 * @generated
		 */
		EClass NEW_ARRAY_INSTRUCTION = eINSTANCE.getNewArrayInstruction();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEW_ARRAY_INSTRUCTION__OBJECT = eINSTANCE.getNewArrayInstruction_Object();

		/**
		 * The meta object literal for the '<em><b>Index</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEW_ARRAY_INSTRUCTION__INDEX = eINSTANCE.getNewArrayInstruction_Index();

	}

} //InstrsPackage
