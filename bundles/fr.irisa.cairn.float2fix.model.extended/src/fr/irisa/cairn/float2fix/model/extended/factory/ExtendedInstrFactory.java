package fr.irisa.cairn.float2fix.model.extended.factory;

import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import gecos.core.Symbol;
import gecos.extended.instrs.NewArrayInstruction;
import gecos.extended.instrs.NewInstruction;
import gecos.extended.instrs.impl.InstrsFactoryImpl;
import gecos.instrs.Instruction;
import gecos.types.BaseType;

public class ExtendedInstrFactory extends InstrsFactoryImpl {

	private static ExtendedInstrFactory _factory = new ExtendedInstrFactory();
	
	public static NewInstruction New(BaseType object){
		NewInstruction ret = _factory.createNewInstruction();
		ret.setObject(object);
		ret.setType(object);
		return ret;
	}
	
	public static NewInstruction New(BaseType object, Instruction... params){
		NewInstruction ret = New(object);
		for(Instruction param : params){
			ret.getParameters().add(param);
		}
		return ret;
	}
	
	public static NewArrayInstruction NewArray(BaseType object, Instruction... index){
		NewArrayInstruction ret = _factory.createNewArrayInstruction();
		ret.setObject(object);
		ret.setType(object);
		for(Instruction param : index){
			ret.getIndex().add(param);
		}
		return ret;
	}
	
	public static NewArrayInstruction NewArray(BaseType object, Symbol... index){
		NewArrayInstruction ret = _factory.createNewArrayInstruction();
		ret.setObject(object);
		ret.setType(object);
		for(Symbol param : index){
			ret.getIndex().add(GecosUserInstructionFactory.symbref(param));
		}
		return ret;
	}
}
