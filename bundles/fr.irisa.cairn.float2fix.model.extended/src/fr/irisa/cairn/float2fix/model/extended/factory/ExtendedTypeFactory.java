package fr.irisa.cairn.float2fix.model.extended.factory;

import org.eclipse.emf.common.util.EList;

import gecos.core.Scope;
import gecos.extended.types.OverflowMode;
import gecos.extended.types.QuantificationMode;
import gecos.extended.types.SCFixType;
import gecos.extended.types.SCFixedType;
import gecos.extended.types.SCUFixType;
import gecos.extended.types.SCUFixedType;
import gecos.extended.types.impl.TypesFactoryImpl;
import gecos.types.Type;


public class ExtendedTypeFactory extends TypesFactoryImpl {

	private static final boolean VERBOSE = true;
	
	static private ExtendedTypeFactory factory = new ExtendedTypeFactory();
	static private Scope scope;
	
	static public void setScope(Scope _scope) {
		scope = _scope;
	}

	@SuppressWarnings("unchecked")
	private static <T extends Type> T addTypeToScope(T t) {
		if(scope!=null) {
			EList<Type> types = scope.getTypes();
			for (int i = 0; i < types.size(); i++)
				if (types.get(i).isEqual(t))
					return (T)types.get(i);
			scope.getTypes().add(t);
		} else {
			//FIXME Use exception instead ?
			if (VERBOSE) System.err.println("Scope is not provided in the factory \"fr.irisa.cairn.gecos.model.TypeFactory\"");
//			throw new UnsupportedOperationException("Scope is not provided in the factory \"fr.irisa.cairn.gecos.model.TypeFactory\"");
		}
		return t;
	}
	
	public static Type SCFIX(String width, String integerWidth){
		SCFixType res = factory.createSCFixType();
		res.setIntegerWidth(integerWidth);
		res.setBitWidth(width);
		res.setQuantificationMode(QuantificationMode.SC_TRN);
		res.setOverflowMode(OverflowMode.SC_SAT);
		return addTypeToScope(res);
	}	
	
	public static Type SCUFIX(String width, String integerWidth){
		SCUFixType res = factory.createSCUFixType();
		res.setIntegerWidth(integerWidth);
		res.setBitWidth(width);
		res.setQuantificationMode(QuantificationMode.SC_TRN);
		res.setOverflowMode(OverflowMode.SC_SAT);
		return addTypeToScope(res);
	}	
	
	public static Type SCFIXED(int width, int integerWidth, QuantificationMode quantification, OverflowMode overflow){
		SCFixedType res = factory.createSCFixedType();
		res.setIntegerWidth(integerWidth);
		res.setBitWidth(width);
		res.setQuantificationMode(quantification);
		res.setOverflowMode(overflow);
		return addTypeToScope(res);
	}
	
	public static Type SCFIXED(int width, int integerWidth){
		return SCFIXED(width, integerWidth, QuantificationMode.SC_TRN, OverflowMode.SC_SAT);
	}	
	
	public static Type SCUFIXED(int width, int integerWidth, QuantificationMode quantification, OverflowMode overflow){
		SCUFixedType res = factory.createSCUFixedType();
		res.setIntegerWidth(integerWidth);
		res.setBitWidth(width);
		res.setQuantificationMode(quantification);
		res.setOverflowMode(overflow);
		return addTypeToScope(res);
	}	
	
	public static Type SCUFIXED(int width, int integerWidth){
		return SCUFIXED(width, integerWidth, QuantificationMode.SC_TRN, OverflowMode.SC_SAT);
	}	
	
}
