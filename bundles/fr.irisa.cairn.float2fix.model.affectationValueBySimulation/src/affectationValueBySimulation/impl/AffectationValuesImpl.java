/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package affectationValueBySimulation.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeEList;

import affectationValueBySimulation.AffectationValueBySimulationPackage;
import affectationValueBySimulation.AffectationValues;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Affectation Values</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link affectationValueBySimulation.impl.AffectationValuesImpl#getAffectationNumber <em>Affectation Number</em>}</li>
 *   <li>{@link affectationValueBySimulation.impl.AffectationValuesImpl#getValues <em>Values</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AffectationValuesImpl extends EObjectImpl implements AffectationValues {
	/**
	 * The default value of the '{@link #getAffectationNumber() <em>Affectation Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAffectationNumber()
	 * @generated
	 * @ordered
	 */
	protected static final int AFFECTATION_NUMBER_EDEFAULT = -1;

	/**
	 * The cached value of the '{@link #getAffectationNumber() <em>Affectation Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAffectationNumber()
	 * @generated
	 * @ordered
	 */
	protected int affectationNumber = AFFECTATION_NUMBER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getValues() <em>Values</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValues()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> values;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AffectationValuesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AffectationValueBySimulationPackage.Literals.AFFECTATION_VALUES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getAffectationNumber() {
		return affectationNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAffectationNumber(int newAffectationNumber) {
		int oldAffectationNumber = affectationNumber;
		affectationNumber = newAffectationNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AffectationValueBySimulationPackage.AFFECTATION_VALUES__AFFECTATION_NUMBER, oldAffectationNumber, affectationNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getValues() {
		if (values == null) {
			values = new EDataTypeEList<Double>(Double.class, this, AffectationValueBySimulationPackage.AFFECTATION_VALUES__VALUES);
		}
		return values;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AffectationValueBySimulationPackage.AFFECTATION_VALUES__AFFECTATION_NUMBER:
				return getAffectationNumber();
			case AffectationValueBySimulationPackage.AFFECTATION_VALUES__VALUES:
				return getValues();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AffectationValueBySimulationPackage.AFFECTATION_VALUES__AFFECTATION_NUMBER:
				setAffectationNumber((Integer)newValue);
				return;
			case AffectationValueBySimulationPackage.AFFECTATION_VALUES__VALUES:
				getValues().clear();
				getValues().addAll((Collection<? extends Double>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AffectationValueBySimulationPackage.AFFECTATION_VALUES__AFFECTATION_NUMBER:
				setAffectationNumber(AFFECTATION_NUMBER_EDEFAULT);
				return;
			case AffectationValueBySimulationPackage.AFFECTATION_VALUES__VALUES:
				getValues().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AffectationValueBySimulationPackage.AFFECTATION_VALUES__AFFECTATION_NUMBER:
				return affectationNumber != AFFECTATION_NUMBER_EDEFAULT;
			case AffectationValueBySimulationPackage.AFFECTATION_VALUES__VALUES:
				return values != null && !values.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (affectationNumber: ");
		result.append(affectationNumber);
		result.append(", values: ");
		result.append(values);
		result.append(')');
		return result.toString();
	}

} //AffectationValuesImpl
