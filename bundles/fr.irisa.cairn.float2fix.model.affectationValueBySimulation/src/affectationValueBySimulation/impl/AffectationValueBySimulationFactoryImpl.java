/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package affectationValueBySimulation.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import affectationValueBySimulation.AffectationValueBySimulationFactory;
import affectationValueBySimulation.AffectationValueBySimulationPackage;
import affectationValueBySimulation.AffectationValues;
import affectationValueBySimulation.ValuesList;
import affectationValueBySimulation.VariableValues;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AffectationValueBySimulationFactoryImpl extends EFactoryImpl implements AffectationValueBySimulationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AffectationValueBySimulationFactory init() {
		try {
			AffectationValueBySimulationFactory theAffectationValueBySimulationFactory = (AffectationValueBySimulationFactory)EPackage.Registry.INSTANCE.getEFactory(AffectationValueBySimulationPackage.eNS_URI);
			if (theAffectationValueBySimulationFactory != null) {
				return theAffectationValueBySimulationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new AffectationValueBySimulationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AffectationValueBySimulationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case AffectationValueBySimulationPackage.VALUES_LIST: return createValuesList();
			case AffectationValueBySimulationPackage.AFFECTATION_VALUES: return createAffectationValues();
			case AffectationValueBySimulationPackage.VARIABLE_VALUES: return createVariableValues();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValuesList createValuesList() {
		ValuesListImpl valuesList = new ValuesListImpl();
		return valuesList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AffectationValues createAffectationValues() {
		AffectationValuesImpl affectationValues = new AffectationValuesImpl();
		return affectationValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableValues createVariableValues() {
		VariableValuesImpl variableValues = new VariableValuesImpl();
		return variableValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AffectationValueBySimulationPackage getAffectationValueBySimulationPackage() {
		return (AffectationValueBySimulationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static AffectationValueBySimulationPackage getPackage() {
		return AffectationValueBySimulationPackage.eINSTANCE;
	}

} //AffectationValueBySimulationFactoryImpl
