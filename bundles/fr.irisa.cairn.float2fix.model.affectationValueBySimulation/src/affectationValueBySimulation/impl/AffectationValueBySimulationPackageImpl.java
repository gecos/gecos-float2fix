/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package affectationValueBySimulation.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import affectationValueBySimulation.AffectationValueBySimulationFactory;
import affectationValueBySimulation.AffectationValueBySimulationPackage;
import affectationValueBySimulation.AffectationValues;
import affectationValueBySimulation.ValuesList;
import affectationValueBySimulation.VariableValues;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AffectationValueBySimulationPackageImpl extends EPackageImpl implements AffectationValueBySimulationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valuesListEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass affectationValuesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variableValuesEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see affectationValueBySimulation.AffectationValueBySimulationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AffectationValueBySimulationPackageImpl() {
		super(eNS_URI, AffectationValueBySimulationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link AffectationValueBySimulationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AffectationValueBySimulationPackage init() {
		if (isInited) return (AffectationValueBySimulationPackage)EPackage.Registry.INSTANCE.getEPackage(AffectationValueBySimulationPackage.eNS_URI);

		// Obtain or create and register package
		AffectationValueBySimulationPackageImpl theAffectationValueBySimulationPackage = (AffectationValueBySimulationPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof AffectationValueBySimulationPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new AffectationValueBySimulationPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theAffectationValueBySimulationPackage.createPackageContents();

		// Initialize created meta-data
		theAffectationValueBySimulationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAffectationValueBySimulationPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AffectationValueBySimulationPackage.eNS_URI, theAffectationValueBySimulationPackage);
		return theAffectationValueBySimulationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getValuesList() {
		return valuesListEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getValuesList_AffectationList() {
		return (EReference)valuesListEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getValuesList_VariableList() {
		return (EReference)valuesListEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAffectationValues() {
		return affectationValuesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAffectationValues_AffectationNumber() {
		return (EAttribute)affectationValuesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAffectationValues_Values() {
		return (EAttribute)affectationValuesEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVariableValues() {
		return variableValuesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVariableValues_NumDataCDFG() {
		return (EAttribute)variableValuesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVariableValues_Index() {
		return (EAttribute)variableValuesEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVariableValues_Values() {
		return (EAttribute)variableValuesEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AffectationValueBySimulationFactory getAffectationValueBySimulationFactory() {
		return (AffectationValueBySimulationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		valuesListEClass = createEClass(VALUES_LIST);
		createEReference(valuesListEClass, VALUES_LIST__AFFECTATION_LIST);
		createEReference(valuesListEClass, VALUES_LIST__VARIABLE_LIST);

		affectationValuesEClass = createEClass(AFFECTATION_VALUES);
		createEAttribute(affectationValuesEClass, AFFECTATION_VALUES__AFFECTATION_NUMBER);
		createEAttribute(affectationValuesEClass, AFFECTATION_VALUES__VALUES);

		variableValuesEClass = createEClass(VARIABLE_VALUES);
		createEAttribute(variableValuesEClass, VARIABLE_VALUES__NUM_DATA_CDFG);
		createEAttribute(variableValuesEClass, VARIABLE_VALUES__INDEX);
		createEAttribute(variableValuesEClass, VARIABLE_VALUES__VALUES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(valuesListEClass, ValuesList.class, "ValuesList", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getValuesList_AffectationList(), this.getAffectationValues(), null, "affectationList", null, 0, -1, ValuesList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getValuesList_VariableList(), this.getVariableValues(), null, "variableList", null, 0, -1, ValuesList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(affectationValuesEClass, AffectationValues.class, "AffectationValues", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAffectationValues_AffectationNumber(), ecorePackage.getEInt(), "affectationNumber", "-1", 1, 1, AffectationValues.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAffectationValues_Values(), ecorePackage.getEDouble(), "values", "0.0", 0, -1, AffectationValues.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(variableValuesEClass, VariableValues.class, "VariableValues", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getVariableValues_NumDataCDFG(), ecorePackage.getEInt(), "numDataCDFG", null, 1, 1, VariableValues.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVariableValues_Index(), ecorePackage.getEString(), "index", null, 0, 1, VariableValues.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVariableValues_Values(), ecorePackage.getEDouble(), "values", null, 0, -1, VariableValues.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //AffectationValueBySimulationPackageImpl
