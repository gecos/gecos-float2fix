/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package affectationValueBySimulation.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import affectationValueBySimulation.AffectationValueBySimulationPackage;
import affectationValueBySimulation.AffectationValues;
import affectationValueBySimulation.ValuesList;
import affectationValueBySimulation.VariableValues;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Values List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link affectationValueBySimulation.impl.ValuesListImpl#getAffectationList <em>Affectation List</em>}</li>
 *   <li>{@link affectationValueBySimulation.impl.ValuesListImpl#getVariableList <em>Variable List</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ValuesListImpl extends EObjectImpl implements ValuesList {
	/**
	 * The cached value of the '{@link #getAffectationList() <em>Affectation List</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAffectationList()
	 * @generated
	 * @ordered
	 */
	protected EList<AffectationValues> affectationList;

	/**
	 * The cached value of the '{@link #getVariableList() <em>Variable List</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariableList()
	 * @generated
	 * @ordered
	 */
	protected EList<VariableValues> variableList;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ValuesListImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AffectationValueBySimulationPackage.Literals.VALUES_LIST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AffectationValues> getAffectationList() {
		if (affectationList == null) {
			affectationList = new EObjectContainmentEList<AffectationValues>(AffectationValues.class, this, AffectationValueBySimulationPackage.VALUES_LIST__AFFECTATION_LIST);
		}
		return affectationList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VariableValues> getVariableList() {
		if (variableList == null) {
			variableList = new EObjectContainmentEList<VariableValues>(VariableValues.class, this, AffectationValueBySimulationPackage.VALUES_LIST__VARIABLE_LIST);
		}
		return variableList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AffectationValueBySimulationPackage.VALUES_LIST__AFFECTATION_LIST:
				return ((InternalEList<?>)getAffectationList()).basicRemove(otherEnd, msgs);
			case AffectationValueBySimulationPackage.VALUES_LIST__VARIABLE_LIST:
				return ((InternalEList<?>)getVariableList()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AffectationValueBySimulationPackage.VALUES_LIST__AFFECTATION_LIST:
				return getAffectationList();
			case AffectationValueBySimulationPackage.VALUES_LIST__VARIABLE_LIST:
				return getVariableList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AffectationValueBySimulationPackage.VALUES_LIST__AFFECTATION_LIST:
				getAffectationList().clear();
				getAffectationList().addAll((Collection<? extends AffectationValues>)newValue);
				return;
			case AffectationValueBySimulationPackage.VALUES_LIST__VARIABLE_LIST:
				getVariableList().clear();
				getVariableList().addAll((Collection<? extends VariableValues>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AffectationValueBySimulationPackage.VALUES_LIST__AFFECTATION_LIST:
				getAffectationList().clear();
				return;
			case AffectationValueBySimulationPackage.VALUES_LIST__VARIABLE_LIST:
				getVariableList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AffectationValueBySimulationPackage.VALUES_LIST__AFFECTATION_LIST:
				return affectationList != null && !affectationList.isEmpty();
			case AffectationValueBySimulationPackage.VALUES_LIST__VARIABLE_LIST:
				return variableList != null && !variableList.isEmpty();
		}
		return super.eIsSet(featureID);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer();
		result.append(" (affectationList: ");
		result.append(affectationList);
		result.append(", variableList: ");
		result.append(variableList);
		result.append(')');
		return result.toString();
	}

} //ValuesListImpl
