/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package affectationValueBySimulation.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeEList;

import affectationValueBySimulation.AffectationValueBySimulationPackage;
import affectationValueBySimulation.VariableValues;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variable Values</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link affectationValueBySimulation.impl.VariableValuesImpl#getNumDataCDFG <em>Num Data CDFG</em>}</li>
 *   <li>{@link affectationValueBySimulation.impl.VariableValuesImpl#getIndex <em>Index</em>}</li>
 *   <li>{@link affectationValueBySimulation.impl.VariableValuesImpl#getValues <em>Values</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VariableValuesImpl extends EObjectImpl implements VariableValues {
	/**
	 * The default value of the '{@link #getNumDataCDFG() <em>Num Data CDFG</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumDataCDFG()
	 * @generated
	 * @ordered
	 */
	protected static final int NUM_DATA_CDFG_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumDataCDFG() <em>Num Data CDFG</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumDataCDFG()
	 * @generated
	 * @ordered
	 */
	protected int numDataCDFG = NUM_DATA_CDFG_EDEFAULT;

	/**
	 * The default value of the '{@link #getIndex() <em>Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndex()
	 * @generated
	 * @ordered
	 */
	protected static final String INDEX_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIndex() <em>Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndex()
	 * @generated
	 * @ordered
	 */
	protected String index = INDEX_EDEFAULT;

	/**
	 * The cached value of the '{@link #getValues() <em>Values</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValues()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> values;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VariableValuesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AffectationValueBySimulationPackage.Literals.VARIABLE_VALUES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumDataCDFG() {
		return numDataCDFG;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumDataCDFG(int newNumDataCDFG) {
		int oldNumDataCDFG = numDataCDFG;
		numDataCDFG = newNumDataCDFG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AffectationValueBySimulationPackage.VARIABLE_VALUES__NUM_DATA_CDFG, oldNumDataCDFG, numDataCDFG));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getIndex() {
		return index;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndex(String newIndex) {
		String oldIndex = index;
		index = newIndex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AffectationValueBySimulationPackage.VARIABLE_VALUES__INDEX, oldIndex, index));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getValues() {
		if (values == null) {
			values = new EDataTypeEList<Double>(Double.class, this, AffectationValueBySimulationPackage.VARIABLE_VALUES__VALUES);
		}
		return values;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AffectationValueBySimulationPackage.VARIABLE_VALUES__NUM_DATA_CDFG:
				return getNumDataCDFG();
			case AffectationValueBySimulationPackage.VARIABLE_VALUES__INDEX:
				return getIndex();
			case AffectationValueBySimulationPackage.VARIABLE_VALUES__VALUES:
				return getValues();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AffectationValueBySimulationPackage.VARIABLE_VALUES__NUM_DATA_CDFG:
				setNumDataCDFG((Integer)newValue);
				return;
			case AffectationValueBySimulationPackage.VARIABLE_VALUES__INDEX:
				setIndex((String)newValue);
				return;
			case AffectationValueBySimulationPackage.VARIABLE_VALUES__VALUES:
				getValues().clear();
				getValues().addAll((Collection<? extends Double>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AffectationValueBySimulationPackage.VARIABLE_VALUES__NUM_DATA_CDFG:
				setNumDataCDFG(NUM_DATA_CDFG_EDEFAULT);
				return;
			case AffectationValueBySimulationPackage.VARIABLE_VALUES__INDEX:
				setIndex(INDEX_EDEFAULT);
				return;
			case AffectationValueBySimulationPackage.VARIABLE_VALUES__VALUES:
				getValues().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AffectationValueBySimulationPackage.VARIABLE_VALUES__NUM_DATA_CDFG:
				return numDataCDFG != NUM_DATA_CDFG_EDEFAULT;
			case AffectationValueBySimulationPackage.VARIABLE_VALUES__INDEX:
				return INDEX_EDEFAULT == null ? index != null : !INDEX_EDEFAULT.equals(index);
			case AffectationValueBySimulationPackage.VARIABLE_VALUES__VALUES:
				return values != null && !values.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (numDataCDFG: ");
		result.append(numDataCDFG);
		result.append(", index: ");
		result.append(index);
		result.append(", values: ");
		result.append(values);
		result.append(')');
		return result.toString();
	}

} //VariableValuesImpl
