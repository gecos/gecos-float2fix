/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package affectationValueBySimulation;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Affectation Values</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link affectationValueBySimulation.AffectationValues#getAffectationNumber <em>Affectation Number</em>}</li>
 *   <li>{@link affectationValueBySimulation.AffectationValues#getValues <em>Values</em>}</li>
 * </ul>
 * </p>
 *
 * @see affectationValueBySimulation.AffectationValueBySimulationPackage#getAffectationValues()
 * @model
 * @generated
 */
public interface AffectationValues extends EObject {
	/**
	 * Returns the value of the '<em><b>Affectation Number</b></em>' attribute.
	 * The default value is <code>"-1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Affectation Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Affectation Number</em>' attribute.
	 * @see #setAffectationNumber(int)
	 * @see affectationValueBySimulation.AffectationValueBySimulationPackage#getAffectationValues_AffectationNumber()
	 * @model default="-1" required="true"
	 * @generated
	 */
	int getAffectationNumber();

	/**
	 * Sets the value of the '{@link affectationValueBySimulation.AffectationValues#getAffectationNumber <em>Affectation Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Affectation Number</em>' attribute.
	 * @see #getAffectationNumber()
	 * @generated
	 */
	void setAffectationNumber(int value);

	/**
	 * Returns the value of the '<em><b>Values</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Values</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Values</em>' attribute list.
	 * @see affectationValueBySimulation.AffectationValueBySimulationPackage#getAffectationValues_Values()
	 * @model default="0.0" unique="false"
	 * @generated
	 */
	EList<Double> getValues();

} // AffectationValues
