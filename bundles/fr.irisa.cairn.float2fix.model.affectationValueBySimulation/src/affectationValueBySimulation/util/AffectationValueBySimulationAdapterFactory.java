/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package affectationValueBySimulation.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

import affectationValueBySimulation.AffectationValueBySimulationPackage;
import affectationValueBySimulation.AffectationValues;
import affectationValueBySimulation.ValuesList;
import affectationValueBySimulation.VariableValues;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see affectationValueBySimulation.AffectationValueBySimulationPackage
 * @generated
 */
public class AffectationValueBySimulationAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static AffectationValueBySimulationPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AffectationValueBySimulationAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = AffectationValueBySimulationPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AffectationValueBySimulationSwitch<Adapter> modelSwitch =
		new AffectationValueBySimulationSwitch<Adapter>() {
			@Override
			public Adapter caseValuesList(ValuesList object) {
				return createValuesListAdapter();
			}
			@Override
			public Adapter caseAffectationValues(AffectationValues object) {
				return createAffectationValuesAdapter();
			}
			@Override
			public Adapter caseVariableValues(VariableValues object) {
				return createVariableValuesAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link affectationValueBySimulation.ValuesList <em>Values List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see affectationValueBySimulation.ValuesList
	 * @generated
	 */
	public Adapter createValuesListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link affectationValueBySimulation.AffectationValues <em>Affectation Values</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see affectationValueBySimulation.AffectationValues
	 * @generated
	 */
	public Adapter createAffectationValuesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link affectationValueBySimulation.VariableValues <em>Variable Values</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see affectationValueBySimulation.VariableValues
	 * @generated
	 */
	public Adapter createVariableValuesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //AffectationValueBySimulationAdapterFactory
