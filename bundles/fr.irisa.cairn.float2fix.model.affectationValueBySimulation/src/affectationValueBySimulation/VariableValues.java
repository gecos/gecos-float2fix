/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package affectationValueBySimulation;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable Values</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link affectationValueBySimulation.VariableValues#getNumDataCDFG <em>Num Data CDFG</em>}</li>
 *   <li>{@link affectationValueBySimulation.VariableValues#getIndex <em>Index</em>}</li>
 *   <li>{@link affectationValueBySimulation.VariableValues#getValues <em>Values</em>}</li>
 * </ul>
 * </p>
 *
 * @see affectationValueBySimulation.AffectationValueBySimulationPackage#getVariableValues()
 * @model
 * @generated
 */
public interface VariableValues extends EObject {
	/**
	 * Returns the value of the '<em><b>Num Data CDFG</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Num Data CDFG</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Num Data CDFG</em>' attribute.
	 * @see #setNumDataCDFG(int)
	 * @see affectationValueBySimulation.AffectationValueBySimulationPackage#getVariableValues_NumDataCDFG()
	 * @model required="true"
	 * @generated
	 */
	int getNumDataCDFG();

	/**
	 * Sets the value of the '{@link affectationValueBySimulation.VariableValues#getNumDataCDFG <em>Num Data CDFG</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Num Data CDFG</em>' attribute.
	 * @see #getNumDataCDFG()
	 * @generated
	 */
	void setNumDataCDFG(int value);

	/**
	 * Returns the value of the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Index</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Index</em>' attribute.
	 * @see #setIndex(String)
	 * @see affectationValueBySimulation.AffectationValueBySimulationPackage#getVariableValues_Index()
	 * @model
	 * @generated
	 */
	String getIndex();

	/**
	 * Sets the value of the '{@link affectationValueBySimulation.VariableValues#getIndex <em>Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Index</em>' attribute.
	 * @see #getIndex()
	 * @generated
	 */
	void setIndex(String value);

	/**
	 * Returns the value of the '<em><b>Values</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Values</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Values</em>' attribute list.
	 * @see affectationValueBySimulation.AffectationValueBySimulationPackage#getVariableValues_Values()
	 * @model unique="false"
	 * @generated
	 */
	EList<Double> getValues();

} // VariableValues
