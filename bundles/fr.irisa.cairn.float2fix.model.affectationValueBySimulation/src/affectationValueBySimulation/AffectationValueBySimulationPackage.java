/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package affectationValueBySimulation;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see affectationValueBySimulation.AffectationValueBySimulationFactory
 * @model kind="package"
 * @generated
 */
public interface AffectationValueBySimulationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "affectationValueBySimulation";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "fr.irisa.cairn.float2fix.model.affectationValueBySimulation";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "fr.irisa.cairn.float2fix.model.affectationValueBySimulation";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AffectationValueBySimulationPackage eINSTANCE = affectationValueBySimulation.impl.AffectationValueBySimulationPackageImpl.init();

	/**
	 * The meta object id for the '{@link affectationValueBySimulation.impl.ValuesListImpl <em>Values List</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see affectationValueBySimulation.impl.ValuesListImpl
	 * @see affectationValueBySimulation.impl.AffectationValueBySimulationPackageImpl#getValuesList()
	 * @generated
	 */
	int VALUES_LIST = 0;

	/**
	 * The feature id for the '<em><b>Affectation List</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUES_LIST__AFFECTATION_LIST = 0;

	/**
	 * The feature id for the '<em><b>Variable List</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUES_LIST__VARIABLE_LIST = 1;

	/**
	 * The number of structural features of the '<em>Values List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUES_LIST_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link affectationValueBySimulation.impl.AffectationValuesImpl <em>Affectation Values</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see affectationValueBySimulation.impl.AffectationValuesImpl
	 * @see affectationValueBySimulation.impl.AffectationValueBySimulationPackageImpl#getAffectationValues()
	 * @generated
	 */
	int AFFECTATION_VALUES = 1;

	/**
	 * The feature id for the '<em><b>Affectation Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFFECTATION_VALUES__AFFECTATION_NUMBER = 0;

	/**
	 * The feature id for the '<em><b>Values</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFFECTATION_VALUES__VALUES = 1;

	/**
	 * The number of structural features of the '<em>Affectation Values</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AFFECTATION_VALUES_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link affectationValueBySimulation.impl.VariableValuesImpl <em>Variable Values</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see affectationValueBySimulation.impl.VariableValuesImpl
	 * @see affectationValueBySimulation.impl.AffectationValueBySimulationPackageImpl#getVariableValues()
	 * @generated
	 */
	int VARIABLE_VALUES = 2;

	/**
	 * The feature id for the '<em><b>Num Data CDFG</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_VALUES__NUM_DATA_CDFG = 0;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_VALUES__INDEX = 1;

	/**
	 * The feature id for the '<em><b>Values</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_VALUES__VALUES = 2;

	/**
	 * The number of structural features of the '<em>Variable Values</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_VALUES_FEATURE_COUNT = 3;


	/**
	 * Returns the meta object for class '{@link affectationValueBySimulation.ValuesList <em>Values List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Values List</em>'.
	 * @see affectationValueBySimulation.ValuesList
	 * @generated
	 */
	EClass getValuesList();

	/**
	 * Returns the meta object for the containment reference list '{@link affectationValueBySimulation.ValuesList#getAffectationList <em>Affectation List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Affectation List</em>'.
	 * @see affectationValueBySimulation.ValuesList#getAffectationList()
	 * @see #getValuesList()
	 * @generated
	 */
	EReference getValuesList_AffectationList();

	/**
	 * Returns the meta object for the containment reference list '{@link affectationValueBySimulation.ValuesList#getVariableList <em>Variable List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Variable List</em>'.
	 * @see affectationValueBySimulation.ValuesList#getVariableList()
	 * @see #getValuesList()
	 * @generated
	 */
	EReference getValuesList_VariableList();

	/**
	 * Returns the meta object for class '{@link affectationValueBySimulation.AffectationValues <em>Affectation Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Affectation Values</em>'.
	 * @see affectationValueBySimulation.AffectationValues
	 * @generated
	 */
	EClass getAffectationValues();

	/**
	 * Returns the meta object for the attribute '{@link affectationValueBySimulation.AffectationValues#getAffectationNumber <em>Affectation Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Affectation Number</em>'.
	 * @see affectationValueBySimulation.AffectationValues#getAffectationNumber()
	 * @see #getAffectationValues()
	 * @generated
	 */
	EAttribute getAffectationValues_AffectationNumber();

	/**
	 * Returns the meta object for the attribute list '{@link affectationValueBySimulation.AffectationValues#getValues <em>Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Values</em>'.
	 * @see affectationValueBySimulation.AffectationValues#getValues()
	 * @see #getAffectationValues()
	 * @generated
	 */
	EAttribute getAffectationValues_Values();

	/**
	 * Returns the meta object for class '{@link affectationValueBySimulation.VariableValues <em>Variable Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable Values</em>'.
	 * @see affectationValueBySimulation.VariableValues
	 * @generated
	 */
	EClass getVariableValues();

	/**
	 * Returns the meta object for the attribute '{@link affectationValueBySimulation.VariableValues#getNumDataCDFG <em>Num Data CDFG</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Data CDFG</em>'.
	 * @see affectationValueBySimulation.VariableValues#getNumDataCDFG()
	 * @see #getVariableValues()
	 * @generated
	 */
	EAttribute getVariableValues_NumDataCDFG();

	/**
	 * Returns the meta object for the attribute '{@link affectationValueBySimulation.VariableValues#getIndex <em>Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Index</em>'.
	 * @see affectationValueBySimulation.VariableValues#getIndex()
	 * @see #getVariableValues()
	 * @generated
	 */
	EAttribute getVariableValues_Index();

	/**
	 * Returns the meta object for the attribute list '{@link affectationValueBySimulation.VariableValues#getValues <em>Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Values</em>'.
	 * @see affectationValueBySimulation.VariableValues#getValues()
	 * @see #getVariableValues()
	 * @generated
	 */
	EAttribute getVariableValues_Values();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AffectationValueBySimulationFactory getAffectationValueBySimulationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link affectationValueBySimulation.impl.ValuesListImpl <em>Values List</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see affectationValueBySimulation.impl.ValuesListImpl
		 * @see affectationValueBySimulation.impl.AffectationValueBySimulationPackageImpl#getValuesList()
		 * @generated
		 */
		EClass VALUES_LIST = eINSTANCE.getValuesList();

		/**
		 * The meta object literal for the '<em><b>Affectation List</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VALUES_LIST__AFFECTATION_LIST = eINSTANCE.getValuesList_AffectationList();

		/**
		 * The meta object literal for the '<em><b>Variable List</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VALUES_LIST__VARIABLE_LIST = eINSTANCE.getValuesList_VariableList();

		/**
		 * The meta object literal for the '{@link affectationValueBySimulation.impl.AffectationValuesImpl <em>Affectation Values</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see affectationValueBySimulation.impl.AffectationValuesImpl
		 * @see affectationValueBySimulation.impl.AffectationValueBySimulationPackageImpl#getAffectationValues()
		 * @generated
		 */
		EClass AFFECTATION_VALUES = eINSTANCE.getAffectationValues();

		/**
		 * The meta object literal for the '<em><b>Affectation Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AFFECTATION_VALUES__AFFECTATION_NUMBER = eINSTANCE.getAffectationValues_AffectationNumber();

		/**
		 * The meta object literal for the '<em><b>Values</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AFFECTATION_VALUES__VALUES = eINSTANCE.getAffectationValues_Values();

		/**
		 * The meta object literal for the '{@link affectationValueBySimulation.impl.VariableValuesImpl <em>Variable Values</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see affectationValueBySimulation.impl.VariableValuesImpl
		 * @see affectationValueBySimulation.impl.AffectationValueBySimulationPackageImpl#getVariableValues()
		 * @generated
		 */
		EClass VARIABLE_VALUES = eINSTANCE.getVariableValues();

		/**
		 * The meta object literal for the '<em><b>Num Data CDFG</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VARIABLE_VALUES__NUM_DATA_CDFG = eINSTANCE.getVariableValues_NumDataCDFG();

		/**
		 * The meta object literal for the '<em><b>Index</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VARIABLE_VALUES__INDEX = eINSTANCE.getVariableValues_Index();

		/**
		 * The meta object literal for the '<em><b>Values</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VARIABLE_VALUES__VALUES = eINSTANCE.getVariableValues_Values();

	}

} //AffectationValueBySimulationPackage
