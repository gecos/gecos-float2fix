/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package affectationValueBySimulation;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see affectationValueBySimulation.AffectationValueBySimulationPackage
 * @generated
 */
public interface AffectationValueBySimulationFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AffectationValueBySimulationFactory eINSTANCE = affectationValueBySimulation.impl.AffectationValueBySimulationFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Values List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Values List</em>'.
	 * @generated
	 */
	ValuesList createValuesList();

	/**
	 * Returns a new object of class '<em>Affectation Values</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Affectation Values</em>'.
	 * @generated
	 */
	AffectationValues createAffectationValues();

	/**
	 * Returns a new object of class '<em>Variable Values</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Variable Values</em>'.
	 * @generated
	 */
	VariableValues createVariableValues();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	AffectationValueBySimulationPackage getAffectationValueBySimulationPackage();

} //AffectationValueBySimulationFactory
