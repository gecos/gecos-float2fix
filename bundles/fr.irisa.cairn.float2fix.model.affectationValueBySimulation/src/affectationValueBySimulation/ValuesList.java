/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package affectationValueBySimulation;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Values List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link affectationValueBySimulation.ValuesList#getAffectationList <em>Affectation List</em>}</li>
 *   <li>{@link affectationValueBySimulation.ValuesList#getVariableList <em>Variable List</em>}</li>
 * </ul>
 * </p>
 *
 * @see affectationValueBySimulation.AffectationValueBySimulationPackage#getValuesList()
 * @model
 * @generated
 */
public interface ValuesList extends EObject {
	/**
	 * Returns the value of the '<em><b>Affectation List</b></em>' containment reference list.
	 * The list contents are of type {@link affectationValueBySimulation.AffectationValues}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Affectation List</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Affectation List</em>' containment reference list.
	 * @see affectationValueBySimulation.AffectationValueBySimulationPackage#getValuesList_AffectationList()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<AffectationValues> getAffectationList();

	/**
	 * Returns the value of the '<em><b>Variable List</b></em>' containment reference list.
	 * The list contents are of type {@link affectationValueBySimulation.VariableValues}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable List</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable List</em>' containment reference list.
	 * @see affectationValueBySimulation.AffectationValueBySimulationPackage#getValuesList_VariableList()
	 * @model containment="true"
	 * @generated
	 */
	EList<VariableValues> getVariableList();

} // ValuesList
