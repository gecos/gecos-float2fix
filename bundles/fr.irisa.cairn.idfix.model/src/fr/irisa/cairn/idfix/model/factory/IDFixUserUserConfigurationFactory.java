package fr.irisa.cairn.idfix.model.factory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;

import fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE;
import fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE;
import fr.irisa.cairn.idfix.model.userconfiguration.DATA_TYPE_REGENERATION;
import fr.irisa.cairn.idfix.model.userconfiguration.ENGINE;
import fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_LOADER;
import fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_OUTPUT_MODE;
import fr.irisa.cairn.idfix.model.userconfiguration.RUNTIME_MODE;
import fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration;
import fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationFactory;
import fr.irisa.cairn.idfix.utils.exceptions.EnvironmentSetupException;

public class IDFixUserUserConfigurationFactory {
	static private UserconfigurationFactory _factory = UserconfigurationFactory.eINSTANCE;
	static private final String _SEPARATOR = "=";
	
	// GLOBAL SPECIF
	static private final String _KEY_ENGINE = "engine";
	static private final String _KEY_TREAD_NUMBER = "thread_number";
	static private final String _KEY_RUNTIME_MODE = "runtime_mode";
	static private final String _KEY_MAXIMUM_FOLDER = "maximum_folder";
	static private final String _KEY_TAG_FOLDER = "date_tag_folder"; //if false MAXIMUM_FOLDER is ignored and only 1 folder with no Data tag is used 
	static private final String _KEY_OPERATOR_CHARACTERISTIC = "operator_characteristic";
	
	// NOISE FUNCTION TWEAKS
	static private final String _KEY_NOISE_FUNCTION_OUTPUT_MODE = "noise_function_output_mode";
	static private final String _KEY_OPERATOR_QUANTIFICATION_MODE = "operator_quantification_mode";
	static private final String _KEY_OPERATOR_OVERFLOW_MODE = "operator_overflow_mode";
	static private final String _KEY_NOISE_FUNCTION_LOADER = "noise_function_loader";
	
	// SIMULATION
	static private final String _KEY_BACKEND_SIMULATOR_SAMPLE_NUMBER = "backend_simulator_sample_number";
	static private final String _KEY_OPTIMIZATION_SIMULATOR_SAMPLE_NUMBER = "optimization_simulator_sample_number";
	static private final String _KEY_OPTIMIZATION_SIMULATOR_FREQUENCY = "optimization_simulator_frequency";
	static private final String _KEY_OPTIMIZATION_SIMULATOR_SUPERIOR_LIMIT = "optimization_simulator_superior_limit";
	
	// REGENERATION
	static private final String _KEY_REGENERATION_DATA_TYPE_USE = "regenerator_data_type";
	
	// OTHER
	static private final String _KEY_OPTMIZATION_GRAPH_DISPLAY = "display_optimization_graph";
	static private final String _KEY_SUMMARY_GENERATION = "generate_summary";
	static private final String _KEY_CORRELATION_MODE = "correlation_mode";
	
	static public UserConfiguration USERCONFIGURATION() {
		UserConfiguration configuration = _factory.createUserConfiguration();
		return configuration;
	}
	
	static public UserConfiguration USERCONFIGURATION(Path configurationfile) throws EnvironmentSetupException{
		UserConfiguration configuration = _factory.createUserConfiguration();
		readConfigurationFile(configurationfile, configuration);
		return configuration;
	}
	
	/*************************************************************************
	 *				               UTILITARY METHOD                          *
	 *                                                                       * 
	 ************************************************************************/
	/**
	 * Read the configuration file and update the option in the {@link UserConfiguration}
	 * @param configurationFile
	 * @param configuration
	 * @throws EnvironmentSetupException
	 */
	private static void readConfigurationFile(Path configurationFile, UserConfiguration configuration) throws EnvironmentSetupException{
		try{
			BufferedReader reader = new BufferedReader(new FileReader(configurationFile.toFile()));
			String line, values[];
			while((line = reader.readLine()) != null){
				// skip comments and empty lines
				if(line.startsWith("#") || line.trim().isEmpty())
					continue;
				
				values = line.split(_SEPARATOR);
				if(values.length != 2){
					reader.close();
					throw new EnvironmentSetupException("Invalid option line contained in the user configuration file " + values);
				}
				
				switch (values[0]) {
				case _KEY_ENGINE:
					configuration.setEngine(manageEngineOption(values[1]));
					break;
				case _KEY_TREAD_NUMBER:
					configuration.setTheadNumber(manageThreadNumber(values[1]));
					break;
				case _KEY_OPTMIZATION_GRAPH_DISPLAY:
					configuration.setOptimizationGraphDisplay(manageBooleanOption(values[1]));
					break;
				case _KEY_SUMMARY_GENERATION:
					configuration.setGenerateSummary(manageBooleanOption(values[1]));
					break;
				case _KEY_NOISE_FUNCTION_LOADER:
					configuration.setNoiseFunctionLoader(manageNoiseFunctionLoaderOption(values[1]));
					break;
				case _KEY_RUNTIME_MODE:
					configuration.setRuntimeMode(manageRuntimeModeOption(values[1]));
					break;
				case _KEY_TAG_FOLDER:
					configuration.setTagFolder(manageBooleanOption(values[1]));
					break;
				case _KEY_MAXIMUM_FOLDER:
					configuration.setMaximumFolder(Integer.parseInt(values[1]));;
					break;
				case _KEY_BACKEND_SIMULATOR_SAMPLE_NUMBER:
					configuration.setBackendSimulatorSampleNumber(manageSimulatorNumber(values[1]));
					break;
				case _KEY_NOISE_FUNCTION_OUTPUT_MODE:
					configuration.setNoiseFunctionOutputMode(manageNoiseFunctionOutputMode(values[1]));
					break;
				case _KEY_OPERATOR_QUANTIFICATION_MODE:
					configuration.setOperatorQuantificationType(manageOperatorQuantificationType(values[1]));
					break;
				case _KEY_OPERATOR_OVERFLOW_MODE:
					configuration.setOperatorOverflowType(manageOperatorOverflowType(values[1]));
					break;
				case _KEY_OPTIMIZATION_SIMULATOR_SAMPLE_NUMBER:
					configuration.setOptimizationSimulatorSampleNumber(manageSimulatorNumber(values[1]));
					break;
				case _KEY_OPTIMIZATION_SIMULATOR_FREQUENCY:
					configuration.setOptimizationSimulatorFrequency(Integer.parseInt(values[1]));
					break;
				case _KEY_OPTIMIZATION_SIMULATOR_SUPERIOR_LIMIT:
					configuration.setOptimizationSimulatorSuperiorLimit(manageSimulatorSuperiorLimit(values[1]));
					break;
				case _KEY_REGENERATION_DATA_TYPE_USE:
					configuration.setDataTypeRegneration(manageDataTypeRegeneration(values[1]));
					break;
				case _KEY_OPERATOR_CHARACTERISTIC:
					configuration.setOperatorCharacteristic(values[1]);
					break;
				case _KEY_CORRELATION_MODE:
					configuration.setCorrelationMode(manageBooleanOption(values[1]));
					break;
					
				default:
					break;
				}
			}
			
			reader.close();
		} catch (IOException e){
			throw new EnvironmentSetupException(e);
		}
	}
	
	private static float manageSimulatorSuperiorLimit(String value) throws EnvironmentSetupException {
		float ret = Float.parseFloat(value);
		if(ret >= 0.0)
			throw new EnvironmentSetupException("Incorrect value for the superior limit of the optimization simulator, need to have a superior limit < 0");
		
		return ret;
	}
	
	private static int manageThreadNumber(String value) throws EnvironmentSetupException{
		int ret = Integer.parseInt(value);
		if(ret < 1)
			throw new EnvironmentSetupException("Incorrect value for thread number option, need to have a thread number > 0");
		
		return ret;
	}
	
	private static int manageSimulatorNumber(String value) throws EnvironmentSetupException{
		int ret = Integer.parseInt(value);
		if(ret < 0)
			throw new EnvironmentSetupException("Incorrect value for simulator sample number option, need to have a positive number");
		
		return ret;
	}
	
	private static ENGINE manageEngineOption(String value) throws EnvironmentSetupException{
		switch (value) {
		case "matlab":
			return ENGINE.MATLAB;
		case "octave":
			return ENGINE.OCTAVE;

		default:
			throw new EnvironmentSetupException("Incorrect value for engine option " + value);
		}
	}
	
	private static NOISE_FUNCTION_LOADER manageNoiseFunctionLoaderOption(String value) throws EnvironmentSetupException{
		switch (value) {
		case "jni":
			return NOISE_FUNCTION_LOADER.JNI;
		case "jna":
			return NOISE_FUNCTION_LOADER.JNA;

		default:
			throw new EnvironmentSetupException("Incorrect value for noise function loader option " + value);
		}
	}
	
	private static RUNTIME_MODE manageRuntimeModeOption(String value) throws EnvironmentSetupException{
		switch (value) {
		case "release":
			return RUNTIME_MODE.RELEASE;
		case "debug":
			return RUNTIME_MODE.DEBUG;
		case "trace":
			return RUNTIME_MODE.TRACE;

		default:
			throw new EnvironmentSetupException("Incorrect value for runtime mode option " + value);
		}
	}
	
	private static NOISE_FUNCTION_OUTPUT_MODE manageNoiseFunctionOutputMode(String value) throws EnvironmentSetupException{
		switch (value) {
		case "worst":
			return NOISE_FUNCTION_OUTPUT_MODE.WORST;
		case "mean":
			return NOISE_FUNCTION_OUTPUT_MODE.MEAN;
		default:
			throw new EnvironmentSetupException("Incorrect value for noise function output mode option " + value);
		}
	}
	
	private static QUANTIFICATION_TYPE manageOperatorQuantificationType(String value) throws EnvironmentSetupException{
		switch (value) {
		case "truncation":
			return QUANTIFICATION_TYPE.TRUNCATION;
		case "rounding":
			return QUANTIFICATION_TYPE.ROUNDING;
		default:
			throw new EnvironmentSetupException("Incorrect value for noise function output mode option " + value);
		}
	}
	
	private static OVERFLOW_TYPE manageOperatorOverflowType(String value) throws EnvironmentSetupException{
		switch (value) {
		case "saturation":
			return OVERFLOW_TYPE.SATURATION;
		case "wrap":
			return OVERFLOW_TYPE.WRAP;
		default:
			throw new EnvironmentSetupException("Incorrect value for noise function output mode option " + value);
		}
	}
	
	private static boolean manageBooleanOption(String value) throws EnvironmentSetupException{
		if(value.equals("true") || value.equals("1")){
			return true;
		}
		else if(value.equals("false") || value.equals("0")){
			return false;
		}
		else{
			throw new EnvironmentSetupException("Incorrect value for optimization graph display option " + value);
		}
	}
	
	private static DATA_TYPE_REGENERATION manageDataTypeRegeneration(String value) throws EnvironmentSetupException{
		switch (value.toLowerCase()) {
		case "sc_fixed":
			return DATA_TYPE_REGENERATION.SC_FIXED;
		case "ac_fixed":
			return DATA_TYPE_REGENERATION.AC_FIXED;
		case "c_inline":
			return DATA_TYPE_REGENERATION.CINLINE;
		case "c_macros":
			return DATA_TYPE_REGENERATION.CMACROS;
		default:
			throw new EnvironmentSetupException("Incorrect value for data type regeneration option " + value);
		}
	}
}
