package fr.irisa.cairn.idfix.model.factory;

import fr.irisa.cairn.idfix.model.informationandtiming.AnalyticInformations;
import fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingFactory;
import fr.irisa.cairn.idfix.model.informationandtiming.Informations;
import fr.irisa.cairn.idfix.model.informationandtiming.Section;
import fr.irisa.cairn.idfix.model.informationandtiming.SimulationInformations;
import fr.irisa.cairn.idfix.model.informationandtiming.TimeElement;
import fr.irisa.cairn.idfix.model.informationandtiming.Timing;

public class IDFixUserInformationAndTimingFactory {
	private static InformationandtimingFactory _factory = InformationandtimingFactory.eINSTANCE;
	
	public static TimeElement TIMEELEMENT(String description, float time){
		TimeElement element = _factory.createTimeElement();
		element.setDescription(description);
		element.setTime(time);
		
		return element;
	}
	
	public static Timing TIMING(){
		return _factory.createTiming();
	}
	
	public static Timing TIMING(Section... sections){
		Timing timing = _factory.createTiming();
		for(Section section : sections){
			timing.getSections().add(section);
		}
		
		return timing;
	}
	
	public static Section SECTION(String name){
		Section section = _factory.createSection();
		section.setName(name);
		
		return section;
	}
	
	public static AnalyticInformations ANALYTICINFORMATION(){
		return _factory.createAnalyticInformations();
	}
	
	public static SimulationInformations SIMULATIONINFORMATION(){
		return _factory.createSimulationInformations();
	}
	
	public static Informations INFORMATIONS(){
		Informations informations = _factory.createInformations();
		informations.setTiming(TIMING());
		informations.setAnalyticInformations(ANALYTICINFORMATION());
		informations.setSimulationInformations(SIMULATIONINFORMATION());
		
		return informations;
	}
}
