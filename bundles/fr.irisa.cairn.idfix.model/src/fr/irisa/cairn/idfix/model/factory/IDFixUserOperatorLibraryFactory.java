package fr.irisa.cairn.idfix.model.factory;

import java.util.List;

import fr.irisa.cairn.idfix.model.operatorlibrary.Characteristic;
import fr.irisa.cairn.idfix.model.operatorlibrary.Instance;
import fr.irisa.cairn.idfix.model.operatorlibrary.OPERATOR_KIND;
import fr.irisa.cairn.idfix.model.operatorlibrary.Operator;
import fr.irisa.cairn.idfix.model.operatorlibrary.OperatorLibrary;
import fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryFactory;
import fr.irisa.cairn.idfix.model.operatorlibrary.Triplet;

public class IDFixUserOperatorLibraryFactory {

	static private OperatorlibraryFactory _factory = OperatorlibraryFactory.eINSTANCE;
	
	static public OperatorLibrary OPERATORLIBRARY(){
		return _factory.createOperatorLibrary();
	}
	
	static public Operator OPERATOR(OPERATOR_KIND kind, List<Instance> instances){
		Operator ret = _factory.createOperator();
		ret.setKind(kind);
		for(Instance i : instances)
			ret.getIntances().add(i);
		
		return ret;		
	}
	
	static public Instance INSTANCE(Triplet triplet, List<Characteristic> characteristics){
		Instance ret = _factory.createInstance();
		ret.setOperands(triplet);
		for(Characteristic c : characteristics)
			ret.getCharacteristics().add(c);
		
		return ret;
	}
	
	static public Triplet TRIPLET(int first, int second, int third){
		Triplet ret = _factory.createTriplet();
		ret.setFirst(first);
		ret.setSecond(second);
		ret.setThird(third);
		
		return ret;
	}
	
	static public Characteristic CHARACTERISTIC(String name, float value){
		Characteristic ret = _factory.createCharacteristic();
		ret.setName(name);
		ret.setValue(value);
		
		return ret;
	}	
}
