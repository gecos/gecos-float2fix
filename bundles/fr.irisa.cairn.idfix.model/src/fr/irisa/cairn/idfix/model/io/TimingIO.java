package fr.irisa.cairn.idfix.model.io;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage;
import fr.irisa.cairn.idfix.model.informationandtiming.Timing;

public class TimingIO {
	public static Timing loadTimingFile(String fileName) {
		ResourceSet rs = new ResourceSetImpl();
		Resource.Factory.Registry f = rs.getResourceFactoryRegistry();
		Map<String, Object> m = f.getExtensionToFactoryMap();
		m.put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		rs.getPackageRegistry().put(InformationandtimingPackage.eNS_URI, InformationandtimingPackage.eINSTANCE);
		
		URI uri = URI.createFileURI(fileName);
		//System.out.println(uri);
		Resource resource = rs.getResource(uri, true);
		Timing timing = null;
		if (resource.isLoaded() && resource.getContents().size() > 0) {
			timing = (Timing) resource.getContents().get(0);
		}
		return timing;
	}

	public static void saveTimingFile(String fileName, Timing fixSpecif) {
		ResourceSet rs = new ResourceSetImpl();
		Resource.Factory.Registry f = rs.getResourceFactoryRegistry();
		Map<String, Object> m = f.getExtensionToFactoryMap();
		m.put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		rs.getPackageRegistry().put(InformationandtimingPackage.eNS_URI, InformationandtimingPackage.eINSTANCE);

		Resource packageResource = rs.createResource(URI.createFileURI(fileName));
		packageResource.getContents().add(InformationandtimingPackage.eINSTANCE);
		try {
			packageResource.load(null);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		URI uri = URI.createFileURI(fileName);
		Resource resource = rs.createResource(uri);
		resource.getContents().add(fixSpecif);
		try {
			HashMap<String, Boolean> options = new HashMap<String, Boolean>();
			options.put(XMIResource.OPTION_SCHEMA_LOCATION, Boolean.TRUE);
			resource.save(options);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
