package fr.irisa.cairn.idfix.model.factory;

import org.eclipse.emf.common.util.BasicEList;

import fr.irisa.cairn.idfix.model.fixedpointspecification.Data;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Operation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixProject;
import fr.irisa.cairn.idfix.model.operatorlibrary.Characteristic;
import fr.irisa.cairn.idfix.model.operatorlibrary.Instance;
import fr.irisa.cairn.idfix.model.operatorlibrary.OPERATOR_KIND;
import fr.irisa.cairn.idfix.model.operatorlibrary.OperatorLibrary;
import fr.irisa.cairn.idfix.model.solutionspace.LibraryElement;
import fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace;
import fr.irisa.cairn.idfix.model.solutionspace.SolutionspaceFactory;
import fr.irisa.cairn.idfix.model.solutionspace.Triplet;


public class IDFixUserSolutionSpaceFactory {

	static private SolutionspaceFactory _factory = SolutionspaceFactory.eINSTANCE;
	
	static private OPERATOR_KIND resolveNewLibraryOperatorKind(OperatorKind operatorKind){
		OPERATOR_KIND ret;
		switch (operatorKind) {
		case ADD:
			ret = OPERATOR_KIND.ADD;
			break;

		case SUB:
			ret = OPERATOR_KIND.SUB;
			break;
			
		case MUL:
			ret = OPERATOR_KIND.MUL;
			break;
			
		case DIV:
			ret = OPERATOR_KIND.DIV;
			break;
	
		case SET:
			ret = OPERATOR_KIND.SET;
			break;
		default:
			throw new RuntimeException("Error during the creation of the optimization solution space. Unknown operator get from the operator library");
		}
		
		return ret;
	}
		
	static private Float getBackCostValue(Instance instance, String cost){
		for(Characteristic c : instance.getCharacteristics()){
			if(c.getName().equals(cost)){
				return c.getValue();
			}
		}
		
		return null;
	}

	static private void buildNewLibrary(SolutionSpace solution, OperatorLibrary library, String characteristic){
		OPERATOR_KIND libraryOperatorKind;
		for(OperatorKind opKind : solution.getLibrary().keySet()){
			libraryOperatorKind = resolveNewLibraryOperatorKind(opKind);
			fr.irisa.cairn.idfix.model.operatorlibrary.Operator operatorLibrary = library.getOperator(libraryOperatorKind);
			if(operatorLibrary == null)
				throw new RuntimeException("Operator detected in the solution space not describe in the operator library (" + opKind + ")");
			
			for(Instance instance : operatorLibrary.getIntances()){
				fr.irisa.cairn.idfix.model.operatorlibrary.Triplet tripletLibrary = instance.getOperands();
				Float costValue =getBackCostValue(instance, characteristic);
				if(costValue == null)
					throw new RuntimeException("The operator library not contains the characteristic " + characteristic + " for the instance [" 
						+ instance.getOperands().getFirst() + ", " + instance.getOperands().getSecond() + ", " + instance.getOperands().getThird()
						+ "] of the operator (" + libraryOperatorKind + ")");
				
				Triplet triplet = TRIPLET(tripletLibrary.getFirst(), tripletLibrary.getSecond(), tripletLibrary.getThird());
				solution.getLibrary().get(opKind).add(LIBELEMENT(triplet, costValue));
			}
		}		
	}
	
	static public SolutionSpace SOLUTIONSPACE(IdfixProject project, OperatorLibrary library){
		SolutionSpace solution = _factory.createSolutionSpace();
		FixedPointSpecification fixedSpecif = project.getFixedPointSpecification();
		
		for(Data data : fixedSpecif.getDatas()){
			solution.getDatas().add(data);
		}
		
		// TODO sorted to have same result of the old version for the test. To delete old version have been deleted
		Operation operatorSorted[] = new Operation[fixedSpecif.getOperations().size()];
		for(Operation op : fixedSpecif.getOperations()){
			operatorSorted[op.getEval_cdfg_number()] = op;
		}
		for(Operation op : operatorSorted){
			solution.getOperators().add(op);
			if(!solution.getLibrary().contains(op.getKind())){
				solution.getLibrary().put(op.getKind(), new BasicEList<LibraryElement>());
			}
		}
		
		buildNewLibrary(solution, library, project.getUserConfiguration().getOperatorCharacteristic());
		solution.setAllOperatorToMin();
		
		return solution;
	}
	
	static public LibraryElement LIBELEMENT(Triplet triplet, float cost){
		LibraryElement element = _factory.createLibraryElement();
		element.setOperand(triplet);
		element.setCost(cost);
		return element;
	}
	
	static public Triplet TRIPLET(int first, int second, int thrid){
		Triplet triplet = _factory.createTriplet();
		triplet.setFirst(first);
		triplet.setSecond(second);
		triplet.setThird(thrid);
		
		return triplet;
	}
	
}
