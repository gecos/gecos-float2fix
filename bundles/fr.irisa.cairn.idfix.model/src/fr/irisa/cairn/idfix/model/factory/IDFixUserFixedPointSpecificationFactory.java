package fr.irisa.cairn.idfix.model.factory;

import fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge;
import fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Data;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationFactory;
import fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Operation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind;
import fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE;
import gecos.core.Symbol;
import gecos.instrs.Instruction;

public class IDFixUserFixedPointSpecificationFactory {

	static private FixedpointspecificationFactory _factory = FixedpointspecificationFactory.eINSTANCE;
	static public QUANTIFICATION_TYPE DEFAULTQUANTIFICATIONTYPE = QUANTIFICATION_TYPE.TRUNCATION;
	static public OVERFLOW_TYPE DEFAULTOVERFLOWTYPE = OVERFLOW_TYPE.SATURATION;
	
	static public Dynamic DYNAMIC(double min, double max){
		Dynamic dynamic = _factory.createDynamic();
		dynamic.setMin(min);
		dynamic.setMax(max);
		return dynamic;
	}
	
	static public FixedPointInformation FIXEDINFORMATION(int bitWidth){
		FixedPointInformation fixed = _factory.createFixedPointInformation();
		fixed.setFixedBitWidth(true);
		fixed.setBitWidth(bitWidth);
		fixed.setQuantification(DEFAULTQUANTIFICATIONTYPE);
		fixed.setOverflow(DEFAULTOVERFLOWTYPE);
		return fixed;
	}
	
	static public FixedPointInformation FIXEDINFORMATION(Dynamic dynamic){
		FixedPointInformation fixed = _factory.createFixedPointInformation();		
		fixed.setFixedBitWidth(false);
		fixed.setDynamic(dynamic);
		fixed.setQuantification(DEFAULTQUANTIFICATIONTYPE);
		fixed.setOverflow(DEFAULTOVERFLOWTYPE);
		return fixed;
	}
	
	
	static public FixedPointInformation FIXEDINFORMATION(int integerWidth, int bitWidth, boolean signed){
		FixedPointInformation fixed = _factory.createFixedPointInformation();		
		fixed.setFixedBitWidth(false); //IMPORTANT need to affect width information before the dynamic!!
		fixed.setIntegerWidth(integerWidth);
		fixed.setBitWidth(bitWidth);
		fixed.setSigned(signed);
		fixed.setQuantification(DEFAULTQUANTIFICATIONTYPE);
		fixed.setOverflow(DEFAULTOVERFLOWTYPE);
		return fixed;
	}
	
	static public FixedPointInformation FIXEDINFORMATION(Dynamic dynamic ,int bitWidth){
		FixedPointInformation fixed = _factory.createFixedPointInformation();		
		fixed.setFixedBitWidth(true); //IMPORTANT need to affect width information before the dynamic!!
		fixed.setBitWidth(bitWidth);
		fixed.setDynamic(dynamic);
		fixed.setQuantification(DEFAULTQUANTIFICATIONTYPE);
		fixed.setOverflow(DEFAULTOVERFLOWTYPE);
		return fixed;
	}
	
	static public Operation OPERATOR(int conv_cdfg, int eval_cdfg, OperatorKind kind, Instruction... instruction){
		Operation op = _factory.createOperator();
		op.setConv_cdfg_number(conv_cdfg);
		op.setEval_cdfg_number(eval_cdfg);
		op.setKind(kind);
		for(Instruction inst : instruction)
			op.getCorrespondingInstructions().add(inst);
		return op;
	}
	
	static public Data DATA(Symbol s, int cdfg_number){
		Data data = _factory.createData();
		data.setCorrespondingSymbol(s);
		data.setCDFGNumber(cdfg_number);
		return data;
	}
	
	static public Data DATA(Symbol s, int cdfg_number, FixedPointInformation fixedInfo){
		Data data = DATA(s, cdfg_number);
		data.setFixedInformation(fixedInfo);
		return data;
	}
	
	static public FixedPointSpecification FIXEDSPECIFICATION(){
		return _factory.createFixedPointSpecification();
	}
	
	static public CDFGEdge EDGE(CDFGNode pred, CDFGNode succ) {
		CDFGEdge e = _factory.createCDFGEdge();
		e.setPredecessor(pred);
		e.setSuccessor(succ);
		return e;
	}
}
