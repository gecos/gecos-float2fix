package fr.irisa.cairn.idfix.model.factory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.SortedMap;
import java.util.TreeMap;

import fr.irisa.cairn.idfix.model.idfixproject.IdfixProject;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixprojectFactory;
import fr.irisa.cairn.idfix.model.userconfiguration.RUNTIME_MODE;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.IDFixUtils;
import fr.irisa.cairn.idfix.utils.exceptions.EnvironmentSetupException;
import fr.irisa.cairn.idfix.utils.logging.IDFixLogger;
import gecos.gecosproject.GecosProject;

public class IDFixUserIDFixProjectFactory {
	static private IdfixprojectFactory _factory = IdfixprojectFactory.eINSTANCE;
	
	/**
	 * Create and initialize a IDFix project.
	 * 	- Default user configuration
	 * @param gecosProject
	 * @param outputFolderPath
	 * @return a IDFix project initialized
	 */
	static public IdfixProject IDFIXPROJECT(GecosProject gecosProject, String outputFolderPath){		
		return IDFIXPROJECT(gecosProject, outputFolderPath, null);
	}
	
	/**
	 * Create and initialize a IDFix project 
	 * @param gecosProject
	 * @param outputFolderPath
	 * @param configurationFile
	 * @return a IDFix project initialized
	 */
	public static IdfixProject IDFIXPROJECT(GecosProject gecosProject, String outputFolderPath, Path configurationFile){		
		if(gecosProject.getSources().size() != 1)
			throw new RuntimeException("IDFix is only compatible to work on just one procedure set (1 file)");
		IdfixProject project = _factory.createIdfixProject();
		
		if(configurationFile != null){
			try{
				project.setUserConfiguration(IDFixUserUserConfigurationFactory.USERCONFIGURATION(configurationFile));
			} catch(EnvironmentSetupException e){
				IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_GLOBAL).error(e);
				throw new RuntimeException(e);
			}
		}
		else{
			project.setUserConfiguration(IDFixUserUserConfigurationFactory.USERCONFIGURATION()); // Default one
		}
		
		project.setOutputFolderPath(buildOutputDir(outputFolderPath, gecosProject.getSources().get(0).getName(), 
				project.getUserConfiguration().getMaximumFolder(), project.getUserConfiguration().isTagFolder()));
		if(project.getUserConfiguration().getRuntimeMode() == RUNTIME_MODE.RELEASE)
			IDFixLogger.initializeReleaseLogging(project.getOutputFolderPath() + ConstantPathAndName.LOG_FODLER_NAME);
		else if(project.getUserConfiguration().getRuntimeMode() == RUNTIME_MODE.DEBUG)
			IDFixLogger.initializeDebugLogging(project.getOutputFolderPath() + ConstantPathAndName.LOG_FODLER_NAME);
		else if(project.getUserConfiguration().getRuntimeMode() == RUNTIME_MODE.TRACE)
			IDFixLogger.initializeTraceLogging(project.getOutputFolderPath() + ConstantPathAndName.LOG_FODLER_NAME);
		else {
			IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_GLOBAL).error("Impossible to initialize logger for the current IDFix project");
			throw new RuntimeException("Impossible to initialize logger for the current IDFix project");
		}
			
		project.setGecosProject(gecosProject);
		project.setFixedPointSpecification(IDFixUserFixedPointSpecificationFactory.FIXEDSPECIFICATION());
		project.setProcessInformations(IDFixUserInformationAndTimingFactory.INFORMATIONS());
		project.getProcessInformations().setUseLastResultModeEnable(checkIfReusingLastRunResult(project));
		if(project.getProcessInformations().isUseLastResultModeEnable()){
			IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_GLOBAL).info("Use last process result compatibility detected (Use last result mode activated)");
			IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_GLOBAL).info("   Dynamic file path : " + project.getDynamicFilePath());
			IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_GLOBAL).info("   Noise function library file path : " + project.getNoiseLibraryFilePath() + "\n");
		}
		
		IDFixUserFixedPointSpecificationFactory.DEFAULTQUANTIFICATIONTYPE = project.getUserConfiguration().getOperatorQuantificationType();
		IDFixUserFixedPointSpecificationFactory.DEFAULTOVERFLOWTYPE = project.getUserConfiguration().getOperatorOverflowType();
		
		return project;
	}	
	
	/*************************************************************************
	 *				               UTILITARY METHOD                          *
	 *                                                                       * 
	 ************************************************************************/
	/**
	 * Create a new sub folder into the path given and generate into it the folder hierarchy used by the tool
	 * @param outputDir
	 * @param cFilePath
	 * @param maximumDatedFolder the maximum dated folder needed into the file folder (delete the old one)
	 * @param tagFolder 
	 * @return new output directory path
	 */
	private static String buildOutputDir(String outputDir, String cFilePath, int maximumDatedFolder, boolean tagFolder){
		File file = new File(cFilePath);
		if(!file.isFile())
			throw new RuntimeException("File " + cFilePath + " have been not found");
		
		File directory;
		String newOutputDir;
		newOutputDir = outputDir.endsWith("/")?outputDir:outputDir + '/';
		newOutputDir += file.getName() +"_folder/";
		
		if(tagFolder) {
			// Delete if necessary some dated folder
			if(new File(newOutputDir).isDirectory() && maximumDatedFolder > 0)
				checkMaximumDatedFolder(newOutputDir, maximumDatedFolder - 1); // -1 because of the new folder have not been created
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
			
			File lastRunDirectory = new File(newOutputDir, ConstantPathAndName.GEN_LAST_RUN_FOLDER_NAME);
			if(lastRunDirectory.isDirectory() && checkIfReusingLastRunResult(file, lastRunDirectory)){
				newOutputDir += dateFormat.format(new Date()) + "/";	
			}
			else{
				newOutputDir += dateFormat.format(new Date()) + "-based/";	
			}
		}
		else {
//			File lastRunDirectory = new File(newOutputDir, ConstantPathAndName.GEN_LAST_RUN_FOLDER_NAME);
//			if(checkCSourceFile(file, lastRunDirectory))
			newOutputDir += "current/";
		}
		
		// Folder creation needed by the IDFix flow
		directory = new File(new File(newOutputDir).getParentFile(), ConstantPathAndName.GEN_LAST_RUN_FOLDER_NAME);
		if(!directory.isDirectory())
			if(!directory.mkdirs())
				throw new RuntimeException("The folder "+ newOutputDir + ConstantPathAndName.GEN_LAST_RUN_FOLDER_NAME +" doesn't exist and have not been created");
		
		directory = new File(newOutputDir, ConstantPathAndName.IDFIX_CONV_OUTPUT_FOLDER_NAME);
		if(!directory.exists())
			if(!directory.mkdirs())
				throw new RuntimeException("The folder "+ newOutputDir + ConstantPathAndName.IDFIX_CONV_OUTPUT_FOLDER_NAME +" doesn't exist and have not been created");
		
		directory = new File(newOutputDir, ConstantPathAndName.IDFIX_EVAL_OUTPUT_FOLDER_NAME);
		if(!directory.isDirectory())
			if(!directory.mkdirs())
				throw new RuntimeException("The folder "+ newOutputDir + ConstantPathAndName.IDFIX_EVAL_OUTPUT_FOLDER_NAME +" doesn't exist and have not been created");
		
		directory = new File(newOutputDir, ConstantPathAndName.LOG_FODLER_NAME);
		if(!directory.isDirectory())
			if(!directory.mkdirs())
				throw new RuntimeException("The folder "+ newOutputDir + ConstantPathAndName.LOG_FODLER_NAME +" doesn't exist and have not been created");
		
		return newOutputDir;
	}
	
	/**
	 * Check if we need to delete dated folder
	 *  - Keep the current based folder if possible
	 * @param outputFolderPath
	 * @param maximumDatedFolder
	 */
	private static void checkMaximumDatedFolder(String outputFolderPath, int maximumDatedFolder){
		Comparator<Date> dateComparator = new Comparator<Date>() {
			   @Override
		        public int compare(Date  d1, Date  d2)
		        {
		            return  d1.compareTo(d2);
		        }
			};
			
		File outputFolder = new File(outputFolderPath);
		SortedMap<Date, File> validFolderToDeletion = new TreeMap<Date, File>(dateComparator);
		SortedMap<Date, File> basedFolder = new TreeMap<Date, File>(dateComparator);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		Date date;

		for(File f : outputFolder.listFiles()){
			if(f.isDirectory()){ 
				String filename = f.getName();
				try {
					date = formatter.parse(filename);
					if(!filename.endsWith("-based")){		
						validFolderToDeletion.put(date, f);
					}
					else{
						basedFolder.put(date, f);
					}
				} catch (ParseException e) {
					System.out.println("ignore " + f.getName());
				}
			}
		}
		
		int totalFolder = basedFolder.size() + validFolderToDeletion.size();
		// Add if there are more than one based folder the older one.
		if(!basedFolder.isEmpty()){
			validFolderToDeletion.putAll(basedFolder.subMap(basedFolder.firstKey(), basedFolder.lastKey())); // add old based folder to valid folder to deletion
		}
		
		if(totalFolder > maximumDatedFolder){
			int needToDelete = totalFolder - maximumDatedFolder;
			int i = 0;
			for(Date d : validFolderToDeletion.keySet()){
				IDFixUtils.deleteDirectory(validFolderToDeletion.get(d));
				i++;
				
				if(i >= needToDelete)
					break;
			}
			
			if(!basedFolder.isEmpty() && i < needToDelete){ //  if we weed to delete more folder, we need the current based one
				IDFixUtils.deleteDirectory(basedFolder.get(basedFolder.lastKey()));
			}
		}
	}
	
	/**
	 * Check if all the current project is compatible to reuse result of the last project run
	 * 	- Check if last run result or information are available
	 * 	- Check if last run result is compatible
	 * @param project
	 * @return true is the current project can reuse result and information of the last project process, false otherwise
	 */
	private static boolean checkIfReusingLastRunResult(IdfixProject project){
		File projectCSourceFile = new File(project.getGecosProject().getSources().get(0).getName());
		File lastRunDirectory = new File(new File(project.getOutputFolderPath()).getParentFile(), ConstantPathAndName.GEN_LAST_RUN_FOLDER_NAME);
		return checkIfReusingLastRunResult(projectCSourceFile, lastRunDirectory);
	}
	
	private static boolean checkIfReusingLastRunResult(File projectCSourceFile, File lastRunDirectory){
		if(lastRunDirectory.isDirectory()){
			try {
				
				return checkCSourceFile(projectCSourceFile, lastRunDirectory)
						&& checkDynamicSpecificationInformation(lastRunDirectory)
						&& checkNoiseFunctionLibrary(lastRunDirectory)
						&& checkKLMMatrix(lastRunDirectory);
			} catch (IOException e) { // Default mode
				return false;
			}			
		}
		else{
			return false; // Save folder not exist
		}
	}

	/**
	 * Check if the C source file of the current project have the same content of the last run C source file 
	 * @param project
	 * @param lastRunDirectory
	 * @return true if the current project C source file have the same content of the last run C source file 
	 * @throws IOException if the last run C source file have been not found
	 */
	private static boolean checkCSourceFile(File projectCSourceFile, File lastRunDirectory) throws IOException {
		if(!projectCSourceFile.isFile()){
			throw new RuntimeException("The original C source code doesn't exist or have been not found");
		}
		
		File lastCSourceFile = new File(lastRunDirectory, ConstantPathAndName.GEN_LAST_RUN_C_SOURCE_FILE_NAME);
		return IDFixUtils.areFilesEquals(projectCSourceFile, lastCSourceFile);
	}
	
	/**
	 * Check if the dynamic specification file is present in the directory parameter.
	 * @param lastRunDirectory
	 * @return true if the dynamic specification file is present in the directory parameter, false otherwise
	 */
	private static boolean checkDynamicSpecificationInformation(File lastRunDirectory){		
		File dynamicSpecification = new File(lastRunDirectory, ConstantPathAndName.GEN_DYNAMIC_SPECIFICATION_FILE_NAME);		
		return dynamicSpecification.isFile();
	}
	
	/**
	 * Check if the noise function library file is present in the directory parameter.
	 * @param lastRunDirectory
	 * @return true if the noise function library file is present in the directory parameter, false otherwise
	 */
	private static boolean checkNoiseFunctionLibrary(File lastRunDirectory){
		File noiseFunctionLibrary = new File(lastRunDirectory, ConstantPathAndName.GEN_NOISE_LIBRARY_FILE_NAME);
		return noiseFunctionLibrary.isFile();
	}
	
	/**
	 * Check if KLM matrix is present in the directory parameter.
	 * @param lastRunDirectory
	 * @return true if KLM matrix file is present in the directory parameter, false otherwise
	 */
	private static boolean checkKLMMatrix(File lastRunDirectory){
		File KLMMatrix = new File(lastRunDirectory, ConstantPathAndName.GEN_KLM_MATRIX_FILE_NAME);
		return KLMMatrix.isFile();
	}
}