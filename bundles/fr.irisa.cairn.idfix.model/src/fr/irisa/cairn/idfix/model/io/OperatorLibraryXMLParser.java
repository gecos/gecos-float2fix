package fr.irisa.cairn.idfix.model.io;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import fr.irisa.cairn.idfix.model.factory.IDFixUserOperatorLibraryFactory;
import fr.irisa.cairn.idfix.model.operatorlibrary.Characteristic;
import fr.irisa.cairn.idfix.model.operatorlibrary.Instance;
import fr.irisa.cairn.idfix.model.operatorlibrary.OPERATOR_KIND;
import fr.irisa.cairn.idfix.model.operatorlibrary.Operator;
import fr.irisa.cairn.idfix.model.operatorlibrary.OperatorLibrary;
import fr.irisa.cairn.idfix.model.operatorlibrary.Triplet;

public class OperatorLibraryXMLParser extends DefaultHandler {
	private OperatorLibrary _operatorLibrary;
	private List<Operator> _currentOperator;
	private List<Instance> _currentInstance;
	private List<Characteristic> _currentCharacteristics;
	private Triplet _currentTriplet;
	
	private OPERATOR_KIND _currentOperatorKind;
	
	// XML ELEMENTS
	static private final String _ELEMENT_ARCHITECTURE = "architecture";
	static private final String _ELEMENT_OPERATOR = "operator";
	static private final String _ELEMENT_INSTANCE = "instance";
	static private final String _ELEMENT_TRIPLET = "triplet";
	static private final String _ELEMENT_CHARACTERISTIC = "characteristic";
	
	//XML ATTRIBUTE
	static private final String _ATTRIBUTE_KIND = "kind";
	static private final String _ATTRIBUTE_FIRST = "first";	
	static private final String _ATTRIBUTE_SECOND = "second";	
	static private final String _ATTRIBUTE_THIRD = "third";	
	static private final String _ATTRIBUTE_NAME = "name";
	static private final String _ATTRIBUTE_VALUE = "value";
	
	public OperatorLibrary load(String operatorLibrary){
		if(!new File(operatorLibrary).isFile())
			throw new RuntimeException("Operator library file not found or doesn't exist");
		
		_operatorLibrary = IDFixUserOperatorLibraryFactory.OPERATORLIBRARY();
		try {
			SAXParserFactory spf = SAXParserFactory.newInstance();
		    spf.setNamespaceAware(true);
		    spf.setValidating(false);
		    SAXParser saxParser = spf.newSAXParser();
		    XMLReader xmlReader = saxParser.getXMLReader();
		    xmlReader.setErrorHandler(this);
		    xmlReader.setContentHandler(this);
		    xmlReader.parse(new InputSource(operatorLibrary));
		} catch(SAXException | ParserConfigurationException | IOException e){
			throw new RuntimeException("Error during the loading of the operator library file" + e);
		}
	    
	    return _operatorLibrary;
	}
	
	@Override
	public void startDocument() throws SAXException {
		
	}
		
	@Override
	public void endDocument() throws SAXException {
		
	}

	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
		switch (localName) {
		case _ELEMENT_ARCHITECTURE:
			_currentOperator = new LinkedList<Operator>();
			break;

		case _ELEMENT_OPERATOR:
			manageElementOperator(atts);
			_currentInstance = new LinkedList<Instance>();
			break;
			
		case _ELEMENT_INSTANCE:
			_currentCharacteristics = new LinkedList<Characteristic>();
			break;
			
		case _ELEMENT_TRIPLET:
			_currentTriplet = manageElementTriplet(atts);
			break;
		
		case _ELEMENT_CHARACTERISTIC:
			_currentCharacteristics.add(manageElementCharacteristic(atts));
			break;
		
		default:
			throw new RuntimeException("Error in the format of the architecture XML file. An unknown element is detected");
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		switch (localName) {
		case _ELEMENT_ARCHITECTURE:
			_operatorLibrary.getOperators().addAll(_currentOperator);
			break;

		case _ELEMENT_OPERATOR:
			_currentOperator.add(IDFixUserOperatorLibraryFactory.OPERATOR(_currentOperatorKind, _currentInstance));
			break;
			
		case _ELEMENT_INSTANCE:
			_currentInstance.add(IDFixUserOperatorLibraryFactory.INSTANCE(_currentTriplet, _currentCharacteristics));
			break;
			
		case _ELEMENT_TRIPLET: // Already save
			break;
		
		case _ELEMENT_CHARACTERISTIC: // Already save
			break;
		
		default:
			throw new RuntimeException("Error in the format of the architecture XML file. An unknown element is detected");
		}
	}
	
	private void manageElementOperator(Attributes atts){
		String kind = atts.getValue(_ATTRIBUTE_KIND);
		if(kind == null)
			throw new RuntimeException("Error in the format of the architecture XML file. Missing kind attribute in operator element");
		_currentOperatorKind = revolveOperatorKind(kind);
	}
	
	private OPERATOR_KIND revolveOperatorKind(String kind){
		switch (kind) {
		case "mul":
			return OPERATOR_KIND.MUL;
		case "sub":
			return OPERATOR_KIND.SUB;
		case "add":
			return OPERATOR_KIND.ADD;
		case "div":
			return OPERATOR_KIND.DIV;
		case "set":
			return OPERATOR_KIND.SET;

		default:
			throw new RuntimeException("Unknown operator kind detected in the operator element detected");
		}
	}
	
	private Triplet manageElementTriplet(Attributes atts){
		String sFirst = atts.getValue(_ATTRIBUTE_FIRST);
		String sSecond = atts.getValue(_ATTRIBUTE_SECOND);
		String sThird = atts.getValue(_ATTRIBUTE_THIRD);
		if(sFirst == null || sSecond == null || sThird == null)
			throw new RuntimeException("Error in the format of the architecture XML file. Missing attribute in triplet element");
		
		int first = Integer.parseInt(sFirst);
		int second = Integer.parseInt(sSecond);
		int third = Integer.parseInt(sThird);
		
		return IDFixUserOperatorLibraryFactory.TRIPLET(first, second, third);
	}
	
	private Characteristic manageElementCharacteristic(Attributes atts){
		String name = atts.getValue(_ATTRIBUTE_NAME);
		String sValue = atts.getValue(_ATTRIBUTE_VALUE);
		if(name == null || sValue == null)
			throw new RuntimeException("Error in the format of the architecture XML file. Missing attribute in characterictic element");
		
		float value = Float.parseFloat(sValue);
		
		return IDFixUserOperatorLibraryFactory.CHARACTERISTIC(name, value);
	}
	
	
	@Override
	public void error(SAXParseException e) throws SAXException {
		String message = "Error: " + e.getMessage();
		throw new SAXException(message);
	}

	@Override
	public void fatalError(SAXParseException e) throws SAXException {
		String message = "Error: " + e.getMessage();
		throw new SAXException(message);
	}

	@Override
	public void warning(SAXParseException e) throws SAXException {
		System.out.println("Warning: " + e.getMessage());
	}
	
}
