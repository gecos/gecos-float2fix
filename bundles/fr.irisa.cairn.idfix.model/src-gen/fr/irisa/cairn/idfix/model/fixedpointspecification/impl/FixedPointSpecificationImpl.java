/**
 */
package fr.irisa.cairn.idfix.model.fixedpointspecification.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import FixPtSpecification.DataId;
import FixPtSpecification.FixPtOpsSpecif;
import FixPtSpecification.FixPtSpecif;
import FixPtSpecification.FixPtType;
import float2fix.Node;
import fr.irisa.cairn.idfix.model.factory.IDFixUserFixedPointSpecificationFactory;
import fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Data;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Operation;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.IDFixUtils;
import fr.irisa.cairn.idfix.utils.io.FixPtSpecifIO;
import gecos.core.Symbol;
import gecos.instrs.Instruction;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fixed Point Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedPointSpecificationImpl#getEdges <em>Edges</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedPointSpecificationImpl#getDatas <em>Datas</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedPointSpecificationImpl#getOperators <em>Operators</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FixedPointSpecificationImpl extends MinimalEObjectImpl.Container implements FixedPointSpecification {
	/**
	 * The cached value of the '{@link #getEdges() <em>Edges</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<CDFGEdge> edges;

	/**
	 * The cached value of the '{@link #getDatas() <em>Datas</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatas()
	 * @generated
	 * @ordered
	 */
	protected EList<Data> datas;

	/**
	 * The cached value of the '{@link #getOperations() <em>Operators</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperations()
	 * @generated
	 * @ordered
	 */
	protected EList<Operation> operators;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FixedPointSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FixedpointspecificationPackage.Literals.FIXED_POINT_SPECIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CDFGEdge> getEdges() {
		if (edges == null) {
			edges = new EObjectContainmentEList<CDFGEdge>(CDFGEdge.class, this, FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION__EDGES);
		}
		return edges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Data> getDatas() {
		if (datas == null) {
			datas = new EObjectContainmentEList<Data>(Data.class, this, FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION__DATAS);
		}
		return datas;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Operation> getOperations() {
		if (operators == null) {
			operators = new EObjectContainmentEList<Operation>(Operation.class, this, FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION__OPERATORS);
		}
		return operators;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void updateDynamicInformation(String dynamicInformation) {
		FixPtType[] operandOps;
		int eval_cdfg;
		Operation operator;
		Data data;
		Double lowerBound;
		Double upperBound;
		
		FixPtSpecif dynInfo = FixPtSpecifIO.loadFixPtSpecif(dynamicInformation);
		
		for (FixPtOpsSpecif opsSpecif : dynInfo.getListFixPtOpsSpecif()) {
			eval_cdfg = Integer.parseInt(opsSpecif.getNumOp());
			operator = findOperatorFromEvalCDFG(eval_cdfg);
			if(operator == null){
				System.err.println("eval_cdfg_op : " + eval_cdfg);
				throw new RuntimeException("Information contained in the dynamic file provide by IDFixEval module not matched with the fixed point specification generated");
			}
			
			operandOps = new FixPtType[3];
			operandOps[0] = opsSpecif.getInput1();
			operandOps[1] = opsSpecif.getInput2();
			operandOps[2] = opsSpecif.getOutput();
			
			for (int i = 0; i < 3; i++) {
				lowerBound = operandOps[i].getDyn().getLowerBound();
				upperBound = operandOps[i].getDyn().getUpperBound();
				if(i >= operator.getOperands().size()){
					operator.getOperands().add(IDFixUserFixedPointSpecificationFactory.FIXEDINFORMATION(
							IDFixUserFixedPointSpecificationFactory.DYNAMIC(lowerBound, upperBound)));
				}
				else{
					operator.getOperands().get(i).setDynamic(IDFixUserFixedPointSpecificationFactory.DYNAMIC(lowerBound, upperBound));
				}
			}
		}
		
		for(DataId d : dynInfo.getListFixPtType().keySet()){
			eval_cdfg = Integer.parseInt(d.getNum());
			data = findDataFromCDFG(eval_cdfg);
			if(data == null){
				System.err.println("eval_cdfg_op : " + eval_cdfg);
				throw new RuntimeException("Information contained in the dynamic file provide by IDFixEval module not matched with the fixed point specification generated");
			}
			
			lowerBound = dynInfo.getListFixPtType().get(d).getDyn().getLowerBound();
			upperBound = dynInfo.getListFixPtType().get(d).getDyn().getUpperBound();
			if(data.getFixedInformation() != null){
				data.getFixedInformation().setDynamic(IDFixUserFixedPointSpecificationFactory.DYNAMIC(lowerBound, upperBound));
			}
			else{
				data.setFixedInformation(IDFixUserFixedPointSpecificationFactory.FIXEDINFORMATION(
							IDFixUserFixedPointSpecificationFactory.DYNAMIC(lowerBound, upperBound)));
			}
		}
		
		checkValidity();
	}

	/**
	 * <!-- begin-user-doc -->
	 * Return the operator defined by the cdfg number of IDFixEval
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Operation findOperatorFromEvalCDFG(int eval_cdfg) {
		for(Operation op : operators){
			if(op.getEval_cdfg_number() == eval_cdfg)
				return op;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Return the operator defined by the cdfg number of IDFixConv
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Operation findOperatorFromConvCDFG(int conv_cdfg) {
		for(Operation op : operators){
			if(op.getConv_cdfg_number() == conv_cdfg)
				return op;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Return the operator which represent the instruction parameter
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Operation findOperatorFromInstruction(Instruction instruction) {
		// We not check the equality between instructions contained in the operator and the parameter given because we have two procedure set contained
		// in the project. Symbol and instruction are not the same. We need to check the CDFG number which are the same in the two procedure set
		// and represent the same data or operator
		Integer conv_cdfg = IDFixUtils.getCDFGInstructionNumber(instruction);
		if(conv_cdfg == null) {
			return null;
		}
		return this.findOperatorFromConvCDFG(conv_cdfg);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Return the data which represented by the CDFG number given as parameter
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Data findDataFromCDFG(int cdfg) {
		for(Data data : datas){
			if(data.getCDFGNumber() == cdfg)
				return data;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Return the data corresponding to the symbol
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Data findDataFromSymbol(Symbol symbol) {
		// We not check the equality between the symbol contained in the data and the parameter given because we have two procedure set contained
		// in the project. Symbol and instruction are not the same. We need to check the CDFG number which are the same in the two procedure set
		// and represent the same data or operator
		Integer numDataCDFG = IDFixUtils.getSymbolAnnotationNumber(symbol);
		if(numDataCDFG == null){
			return null;
		}
		return this.findDataFromCDFG(numDataCDFG);	
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void updateDataFixedInformation() {
		int maxBitWidth, maxIntegerWidth, minIntegerWidth;
		boolean isSystemInput;
		for(Data data : getDatas()){
			maxBitWidth = Integer.MIN_VALUE;
			maxIntegerWidth = Integer.MIN_VALUE;
			minIntegerWidth = Integer.MAX_VALUE;
			
			for(Operation op : data.getPredecessorOperators()){
				if(op.getOperands().get(2).getBitWidth() > maxBitWidth)
					maxBitWidth = op.getOperands().get(2).getBitWidth();
				if(op.getOperands().get(2).getIntegerWidth() > maxIntegerWidth)
					maxIntegerWidth = op.getOperands().get(2).getIntegerWidth();
				if(op.getOperands().get(2).getIntegerWidth() < minIntegerWidth)
					minIntegerWidth = op.getOperands().get(2).getIntegerWidth();
			}
			
			isSystemInput = false;
			for(Node node : data.getSfgNodeInstances()){
				if(node.getPredecessors().isEmpty()){
					isSystemInput = true;
					break;
				}
			}
			if(isSystemInput){ //XXX check if correct (case of DELAY ?)
				for(Operation op : data.getSuccessorOperators()){ 
					int operandIndex = -1;
					for(CDFGEdge edg : op.getPredecessors()) {
						if(edg.getPredecessor().equals(data)) {
							operandIndex = edg.getPredIndex();
							break;
						}
					}
					if(operandIndex < 0 || operandIndex > 1)
						throw new RuntimeException("Predecessor index is not valid: " + data + "\n" + op);
					
					if(op.getOperands().get(operandIndex).getBitWidth() > maxBitWidth)
						maxBitWidth = op.getOperands().get(operandIndex).getBitWidth();
					if(op.getOperands().get(operandIndex).getIntegerWidth() > maxIntegerWidth)
						maxIntegerWidth = op.getOperands().get(operandIndex).getIntegerWidth();
					if(op.getOperands().get(operandIndex).getIntegerWidth() < minIntegerWidth)
						minIntegerWidth = op.getOperands().get(operandIndex).getIntegerWidth();
				}
			}
			
			if(!data.getFixedInformation().isFixedBitWidth() || data.getFixedInformation().getBitWidth() == ConstantPathAndName.MAX_DATA_BITWIDTH){
				/* set bitWidth */
				data.getFixedInformation().setBitWidth(maxBitWidth);
			}
			
			/* set integer width */ //TODO check if correct
//				data.getFixedInformation().setIntegerWidth(maxIntegerWidth); 
			data.getFixedInformation().setIntegerWidth(Math.max(data.getFixedInformation().getDynamicIntegerWidth(), minIntegerWidth)); //max(max(dynamicIntegerWidth), min(IntegerWidth)))
		}		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void checkValidity() {
		for(Data data : getDatas()){
			if(data.getFixedInformation() == null)
				throw new RuntimeException("Data " + data.getCorrespondingSymbol().getName()  + " (CDFG: " + data.getCDFGNumber() + ") contained in the fixed point specification have not fixed point information after the update of the dynamic information get from the idfix-eval process");
			
			if(data.getFixedInformation().getDynamic() == null){
				throw new RuntimeException("Data " + data.getCorrespondingSymbol().getName() + " (CDFG: " + data.getCDFGNumber() + ") contained in the fixed point specification have not dynamic information after the update of the dynamic information get from the idfix-eval process");
			}
		}
		
		for(Operation operator : getOperations()){
			if(operator.getOperands().size() != 3){
				throw new RuntimeException("Operator contained in the fixed point specification have not fixed point information for each of these operands after the update of the dynamic information get from the idfix-eval process");
			}
			
			for(FixedPointInformation fixedInfo : operator.getOperands()){
				if(fixedInfo.getDynamic() == null)
					throw new RuntimeException("Operator contained in the fixed point specification have not dynamic information for each of these operands after the update of the dynamic information get from the idfix-eval process");
			}	
		}		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION__EDGES:
				return ((InternalEList<?>)getEdges()).basicRemove(otherEnd, msgs);
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION__DATAS:
				return ((InternalEList<?>)getDatas()).basicRemove(otherEnd, msgs);
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION__OPERATORS:
				return ((InternalEList<?>)getOperations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION__EDGES:
				return getEdges();
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION__DATAS:
				return getDatas();
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION__OPERATORS:
				return getOperations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION__EDGES:
				getEdges().clear();
				getEdges().addAll((Collection<? extends CDFGEdge>)newValue);
				return;
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION__DATAS:
				getDatas().clear();
				getDatas().addAll((Collection<? extends Data>)newValue);
				return;
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION__OPERATORS:
				getOperations().clear();
				getOperations().addAll((Collection<? extends Operation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION__EDGES:
				getEdges().clear();
				return;
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION__DATAS:
				getDatas().clear();
				return;
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION__OPERATORS:
				getOperations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION__EDGES:
				return edges != null && !edges.isEmpty();
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION__DATAS:
				return datas != null && !datas.isEmpty();
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION__OPERATORS:
				return operators != null && !operators.isEmpty();
		}
		return super.eIsSet(featureID);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION___UPDATE_DYNAMIC_INFORMATION__STRING:
				updateDynamicInformation((String)arguments.get(0));
				return null;
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION___FIND_OPERATOR_FROM_EVAL_CDFG__INT:
				return findOperatorFromEvalCDFG((Integer)arguments.get(0));
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION___FIND_OPERATOR_FROM_CONV_CDFG__INT:
				return findOperatorFromConvCDFG((Integer)arguments.get(0));
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION___FIND_OPERATOR_FROM_INSTRUCTION__INSTRUCTION:
				return findOperatorFromInstruction((Instruction)arguments.get(0));
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION___FIND_DATA_FROM_CDFG__INT:
				return findDataFromCDFG((Integer)arguments.get(0));
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION___FIND_DATA_FROM_SYMBOL__SYMBOL:
				return findDataFromSymbol((Symbol)arguments.get(0));
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION___UPDATE_DATA_FIXED_INFORMATION:
				updateDataFixedInformation();
				return null;
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION___CHECK_VALIDITY:
				checkValidity();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public String toString(){
		StringBuffer result = new StringBuffer("FixedPointSpecification:\n");
		if(operators != null){
			for(Operation op : operators){
				result.append(op.toString() + "\n");
			}
			result.append("\n");
		}
		
		if(datas != null){
			for(Data data : datas){
				result.append(data.toString() + "\n");
			}
		}
		return result.toString();
	}

} //FixedPointSpecificationImpl
