/**
 */
package fr.irisa.cairn.idfix.model.idfixproject;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixprojectPackage
 * @generated
 */
public interface IdfixprojectFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	IdfixprojectFactory eINSTANCE = fr.irisa.cairn.idfix.model.idfixproject.impl.IdfixprojectFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Idfix Project</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Idfix Project</em>'.
	 * @generated
	 */
	IdfixProject createIdfixProject();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	IdfixprojectPackage getIdfixprojectPackage();

} //IdfixprojectFactory
