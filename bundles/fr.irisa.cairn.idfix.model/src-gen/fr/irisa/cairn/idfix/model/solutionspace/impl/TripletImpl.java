/**
 */
package fr.irisa.cairn.idfix.model.solutionspace.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import fr.irisa.cairn.idfix.model.solutionspace.SolutionspacePackage;
import fr.irisa.cairn.idfix.model.solutionspace.Triplet;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Triplet</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.solutionspace.impl.TripletImpl#getFirst <em>First</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.solutionspace.impl.TripletImpl#getSecond <em>Second</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.solutionspace.impl.TripletImpl#getThird <em>Third</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TripletImpl extends MinimalEObjectImpl.Container implements Triplet {
	/**
	 * The default value of the '{@link #getFirst() <em>First</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirst()
	 * @generated
	 * @ordered
	 */
	protected static final int FIRST_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getFirst() <em>First</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirst()
	 * @generated
	 * @ordered
	 */
	protected int first = FIRST_EDEFAULT;

	/**
	 * The default value of the '{@link #getSecond() <em>Second</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSecond()
	 * @generated
	 * @ordered
	 */
	protected static final int SECOND_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getSecond() <em>Second</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSecond()
	 * @generated
	 * @ordered
	 */
	protected int second = SECOND_EDEFAULT;

	/**
	 * The default value of the '{@link #getThird() <em>Third</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThird()
	 * @generated
	 * @ordered
	 */
	protected static final int THIRD_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getThird() <em>Third</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThird()
	 * @generated
	 * @ordered
	 */
	protected int third = THIRD_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TripletImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SolutionspacePackage.Literals.TRIPLET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getFirst() {
		return first;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirst(int newFirst) {
		int oldFirst = first;
		first = newFirst;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SolutionspacePackage.TRIPLET__FIRST, oldFirst, first));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSecond() {
		return second;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSecond(int newSecond) {
		int oldSecond = second;
		second = newSecond;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SolutionspacePackage.TRIPLET__SECOND, oldSecond, second));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getThird() {
		return third;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setThird(int newThird) {
		int oldThird = third;
		third = newThird;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SolutionspacePackage.TRIPLET__THIRD, oldThird, third));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SolutionspacePackage.TRIPLET__FIRST:
				return getFirst();
			case SolutionspacePackage.TRIPLET__SECOND:
				return getSecond();
			case SolutionspacePackage.TRIPLET__THIRD:
				return getThird();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SolutionspacePackage.TRIPLET__FIRST:
				setFirst((Integer)newValue);
				return;
			case SolutionspacePackage.TRIPLET__SECOND:
				setSecond((Integer)newValue);
				return;
			case SolutionspacePackage.TRIPLET__THIRD:
				setThird((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SolutionspacePackage.TRIPLET__FIRST:
				setFirst(FIRST_EDEFAULT);
				return;
			case SolutionspacePackage.TRIPLET__SECOND:
				setSecond(SECOND_EDEFAULT);
				return;
			case SolutionspacePackage.TRIPLET__THIRD:
				setThird(THIRD_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SolutionspacePackage.TRIPLET__FIRST:
				return first != FIRST_EDEFAULT;
			case SolutionspacePackage.TRIPLET__SECOND:
				return second != SECOND_EDEFAULT;
			case SolutionspacePackage.TRIPLET__THIRD:
				return third != THIRD_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append("[");
		result.append(first);
		result.append(", ");
		result.append(second);
		result.append(", ");
		result.append(third);
		result.append(']');
		return result.toString();
	}

} //TripletImpl
