/**
 */
package fr.irisa.cairn.idfix.model.fixedpointspecification;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage
 * @generated
 */
public interface FixedpointspecificationFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FixedpointspecificationFactory eINSTANCE = fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Fixed Point Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fixed Point Specification</em>'.
	 * @generated
	 */
	FixedPointSpecification createFixedPointSpecification();

	/**
	 * Returns a new object of class '<em>Data</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data</em>'.
	 * @generated
	 */
	Data createData();

	/**
	 * Returns a new object of class '<em>Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operator</em>'.
	 * @generated
	 */
	Operation createOperator();

	/**
	 * Returns a new object of class '<em>Fixed Point Information</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fixed Point Information</em>'.
	 * @generated
	 */
	FixedPointInformation createFixedPointInformation();

	/**
	 * Returns a new object of class '<em>Dynamic</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dynamic</em>'.
	 * @generated
	 */
	Dynamic createDynamic();

	/**
	 * Returns a new object of class '<em>CDFG Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CDFG Edge</em>'.
	 * @generated
	 */
	CDFGEdge createCDFGEdge();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	FixedpointspecificationPackage getFixedpointspecificationPackage();

} //FixedpointspecificationFactory
