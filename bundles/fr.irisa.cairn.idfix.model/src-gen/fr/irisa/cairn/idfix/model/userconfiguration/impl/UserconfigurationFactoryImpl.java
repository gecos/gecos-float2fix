/**
 */
package fr.irisa.cairn.idfix.model.userconfiguration.impl;

import fr.irisa.cairn.idfix.model.userconfiguration.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UserconfigurationFactoryImpl extends EFactoryImpl implements UserconfigurationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static UserconfigurationFactory init() {
		try {
			UserconfigurationFactory theUserconfigurationFactory = (UserconfigurationFactory)EPackage.Registry.INSTANCE.getEFactory(UserconfigurationPackage.eNS_URI);
			if (theUserconfigurationFactory != null) {
				return theUserconfigurationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new UserconfigurationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserconfigurationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case UserconfigurationPackage.USER_CONFIGURATION: return createUserConfiguration();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case UserconfigurationPackage.ENGINE:
				return createENGINEFromString(eDataType, initialValue);
			case UserconfigurationPackage.NOISE_FUNCTION_LOADER:
				return createNOISE_FUNCTION_LOADERFromString(eDataType, initialValue);
			case UserconfigurationPackage.RUNTIME_MODE:
				return createRUNTIME_MODEFromString(eDataType, initialValue);
			case UserconfigurationPackage.NOISE_FUNCTION_OUTPUT_MODE:
				return createNOISE_FUNCTION_OUTPUT_MODEFromString(eDataType, initialValue);
			case UserconfigurationPackage.DATA_TYPE_REGENERATION:
				return createDATA_TYPE_REGENERATIONFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case UserconfigurationPackage.ENGINE:
				return convertENGINEToString(eDataType, instanceValue);
			case UserconfigurationPackage.NOISE_FUNCTION_LOADER:
				return convertNOISE_FUNCTION_LOADERToString(eDataType, instanceValue);
			case UserconfigurationPackage.RUNTIME_MODE:
				return convertRUNTIME_MODEToString(eDataType, instanceValue);
			case UserconfigurationPackage.NOISE_FUNCTION_OUTPUT_MODE:
				return convertNOISE_FUNCTION_OUTPUT_MODEToString(eDataType, instanceValue);
			case UserconfigurationPackage.DATA_TYPE_REGENERATION:
				return convertDATA_TYPE_REGENERATIONToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserConfiguration createUserConfiguration() {
		UserConfigurationImpl userConfiguration = new UserConfigurationImpl();
		return userConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ENGINE createENGINEFromString(EDataType eDataType, String initialValue) {
		ENGINE result = ENGINE.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertENGINEToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NOISE_FUNCTION_LOADER createNOISE_FUNCTION_LOADERFromString(EDataType eDataType, String initialValue) {
		NOISE_FUNCTION_LOADER result = NOISE_FUNCTION_LOADER.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertNOISE_FUNCTION_LOADERToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RUNTIME_MODE createRUNTIME_MODEFromString(EDataType eDataType, String initialValue) {
		RUNTIME_MODE result = RUNTIME_MODE.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRUNTIME_MODEToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NOISE_FUNCTION_OUTPUT_MODE createNOISE_FUNCTION_OUTPUT_MODEFromString(EDataType eDataType, String initialValue) {
		NOISE_FUNCTION_OUTPUT_MODE result = NOISE_FUNCTION_OUTPUT_MODE.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertNOISE_FUNCTION_OUTPUT_MODEToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DATA_TYPE_REGENERATION createDATA_TYPE_REGENERATIONFromString(EDataType eDataType, String initialValue) {
		DATA_TYPE_REGENERATION result = DATA_TYPE_REGENERATION.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDATA_TYPE_REGENERATIONToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserconfigurationPackage getUserconfigurationPackage() {
		return (UserconfigurationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static UserconfigurationPackage getPackage() {
		return UserconfigurationPackage.eINSTANCE;
	}

} //UserconfigurationFactoryImpl
