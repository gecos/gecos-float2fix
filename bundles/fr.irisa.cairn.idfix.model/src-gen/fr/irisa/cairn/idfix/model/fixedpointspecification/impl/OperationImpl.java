/**
 */
package fr.irisa.cairn.idfix.model.fixedpointspecification.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Operation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind;
import fr.irisa.cairn.idfix.model.operatorlibrary.Instance;
import gecos.instrs.Instruction;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.OperationImpl#getOperands <em>Operands</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.OperationImpl#getEval_cdfg_number <em>Eval cdfg number</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.OperationImpl#getConv_cdfg_number <em>Conv cdfg number</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.OperationImpl#getKind <em>Kind</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.OperationImpl#getCost <em>Cost</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.OperationImpl#getCorrespondingInstructions <em>Corresponding Instructions</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.OperationImpl#getUsageNumber <em>Usage Number</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.OperationImpl#getInstances <em>Instances</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationImpl extends CDFGNodeImpl implements Operation {
	/**
	 * The cached value of the '{@link #getOperands() <em>Operands</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperands()
	 * @generated
	 * @ordered
	 */
	protected EList<FixedPointInformation> operands;

	/**
	 * The default value of the '{@link #getEval_cdfg_number() <em>Eval cdfg number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEval_cdfg_number()
	 * @generated
	 * @ordered
	 */
	protected static final int EVAL_CDFG_NUMBER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getEval_cdfg_number() <em>Eval cdfg number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEval_cdfg_number()
	 * @generated
	 * @ordered
	 */
	protected int eval_cdfg_number = EVAL_CDFG_NUMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #getConv_cdfg_number() <em>Conv cdfg number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConv_cdfg_number()
	 * @generated
	 * @ordered
	 */
	protected static final int CONV_CDFG_NUMBER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getConv_cdfg_number() <em>Conv cdfg number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConv_cdfg_number()
	 * @generated
	 * @ordered
	 */
	protected int conv_cdfg_number = CONV_CDFG_NUMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected static final OperatorKind KIND_EDEFAULT = OperatorKind.ADD;

	/**
	 * The cached value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected OperatorKind kind = KIND_EDEFAULT;

	/**
	 * The default value of the '{@link #getCost() <em>Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCost()
	 * @generated
	 * @ordered
	 */
	protected static final float COST_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getCost() <em>Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCost()
	 * @generated
	 * @ordered
	 */
	protected float cost = COST_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCorrespondingInstructions() <em>Corresponding Instructions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrespondingInstructions()
	 * @generated
	 * @ordered
	 */
	protected EList<Instruction> correspondingInstructions;

	/**
	 * The default value of the '{@link #getUsageNumber() <em>Usage Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsageNumber()
	 * @generated
	 * @ordered
	 */
	protected static final int USAGE_NUMBER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getUsageNumber() <em>Usage Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsageNumber()
	 * @generated
	 * @ordered
	 */
	protected int usageNumber = USAGE_NUMBER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInstances() <em>Instances</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstances()
	 * @generated
	 * @ordered
	 */
	protected EList<Instance> instances;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FixedpointspecificationPackage.Literals.OPERATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FixedPointInformation> getOperands() {
		if (operands == null) {
			operands = new EObjectContainmentEList<FixedPointInformation>(FixedPointInformation.class, this, FixedpointspecificationPackage.OPERATOR__OPERANDS);
		}
		return operands;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getEval_cdfg_number() {
		return eval_cdfg_number;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEval_cdfg_number(int newEval_cdfg_number) {
		int oldEval_cdfg_number = eval_cdfg_number;
		eval_cdfg_number = newEval_cdfg_number;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixedpointspecificationPackage.OPERATOR__EVAL_CDFG_NUMBER, oldEval_cdfg_number, eval_cdfg_number));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getConv_cdfg_number() {
		return conv_cdfg_number;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConv_cdfg_number(int newConv_cdfg_number) {
		int oldConv_cdfg_number = conv_cdfg_number;
		conv_cdfg_number = newConv_cdfg_number;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixedpointspecificationPackage.OPERATOR__CONV_CDFG_NUMBER, oldConv_cdfg_number, conv_cdfg_number));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatorKind getKind() {
		return kind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKind(OperatorKind newKind) {
		OperatorKind oldKind = kind;
		kind = newKind == null ? KIND_EDEFAULT : newKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixedpointspecificationPackage.OPERATOR__KIND, oldKind, kind));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getCost() {
		return cost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCost(float newCost) {
		float oldCost = cost;
		cost = newCost;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixedpointspecificationPackage.OPERATOR__COST, oldCost, cost));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instruction> getCorrespondingInstructions() {
		if (correspondingInstructions == null) {
			correspondingInstructions = new EObjectResolvingEList<Instruction>(Instruction.class, this, FixedpointspecificationPackage.OPERATOR__CORRESPONDING_INSTRUCTIONS);
		}
		return correspondingInstructions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getUsageNumber() {
		return usageNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUsageNumber(int newUsageNumber) {
		int oldUsageNumber = usageNumber;
		usageNumber = newUsageNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixedpointspecificationPackage.OPERATOR__USAGE_NUMBER, oldUsageNumber, usageNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instance> getInstances() {
		if (instances == null) {
			instances = new EObjectResolvingEList<Instance>(Instance.class, this, FixedpointspecificationPackage.OPERATOR__INSTANCES);
		}
		return instances;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInstance(int index) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean haveNextInstance() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMaxInstance() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getCurrentInstance() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void nextInstance() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void previousInstance() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FixedpointspecificationPackage.OPERATOR__OPERANDS:
				return ((InternalEList<?>)getOperands()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FixedpointspecificationPackage.OPERATOR__OPERANDS:
				return getOperands();
			case FixedpointspecificationPackage.OPERATOR__EVAL_CDFG_NUMBER:
				return getEval_cdfg_number();
			case FixedpointspecificationPackage.OPERATOR__CONV_CDFG_NUMBER:
				return getConv_cdfg_number();
			case FixedpointspecificationPackage.OPERATOR__KIND:
				return getKind();
			case FixedpointspecificationPackage.OPERATOR__COST:
				return getCost();
			case FixedpointspecificationPackage.OPERATOR__CORRESPONDING_INSTRUCTIONS:
				return getCorrespondingInstructions();
			case FixedpointspecificationPackage.OPERATOR__USAGE_NUMBER:
				return getUsageNumber();
			case FixedpointspecificationPackage.OPERATOR__INSTANCES:
				return getInstances();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FixedpointspecificationPackage.OPERATOR__OPERANDS:
				getOperands().clear();
				getOperands().addAll((Collection<? extends FixedPointInformation>)newValue);
				return;
			case FixedpointspecificationPackage.OPERATOR__EVAL_CDFG_NUMBER:
				setEval_cdfg_number((Integer)newValue);
				return;
			case FixedpointspecificationPackage.OPERATOR__CONV_CDFG_NUMBER:
				setConv_cdfg_number((Integer)newValue);
				return;
			case FixedpointspecificationPackage.OPERATOR__KIND:
				setKind((OperatorKind)newValue);
				return;
			case FixedpointspecificationPackage.OPERATOR__COST:
				setCost((Float)newValue);
				return;
			case FixedpointspecificationPackage.OPERATOR__CORRESPONDING_INSTRUCTIONS:
				getCorrespondingInstructions().clear();
				getCorrespondingInstructions().addAll((Collection<? extends Instruction>)newValue);
				return;
			case FixedpointspecificationPackage.OPERATOR__USAGE_NUMBER:
				setUsageNumber((Integer)newValue);
				return;
			case FixedpointspecificationPackage.OPERATOR__INSTANCES:
				getInstances().clear();
				getInstances().addAll((Collection<? extends Instance>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FixedpointspecificationPackage.OPERATOR__OPERANDS:
				getOperands().clear();
				return;
			case FixedpointspecificationPackage.OPERATOR__EVAL_CDFG_NUMBER:
				setEval_cdfg_number(EVAL_CDFG_NUMBER_EDEFAULT);
				return;
			case FixedpointspecificationPackage.OPERATOR__CONV_CDFG_NUMBER:
				setConv_cdfg_number(CONV_CDFG_NUMBER_EDEFAULT);
				return;
			case FixedpointspecificationPackage.OPERATOR__KIND:
				setKind(KIND_EDEFAULT);
				return;
			case FixedpointspecificationPackage.OPERATOR__COST:
				setCost(COST_EDEFAULT);
				return;
			case FixedpointspecificationPackage.OPERATOR__CORRESPONDING_INSTRUCTIONS:
				getCorrespondingInstructions().clear();
				return;
			case FixedpointspecificationPackage.OPERATOR__USAGE_NUMBER:
				setUsageNumber(USAGE_NUMBER_EDEFAULT);
				return;
			case FixedpointspecificationPackage.OPERATOR__INSTANCES:
				getInstances().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FixedpointspecificationPackage.OPERATOR__OPERANDS:
				return operands != null && !operands.isEmpty();
			case FixedpointspecificationPackage.OPERATOR__EVAL_CDFG_NUMBER:
				return eval_cdfg_number != EVAL_CDFG_NUMBER_EDEFAULT;
			case FixedpointspecificationPackage.OPERATOR__CONV_CDFG_NUMBER:
				return conv_cdfg_number != CONV_CDFG_NUMBER_EDEFAULT;
			case FixedpointspecificationPackage.OPERATOR__KIND:
				return kind != KIND_EDEFAULT;
			case FixedpointspecificationPackage.OPERATOR__COST:
				return cost != COST_EDEFAULT;
			case FixedpointspecificationPackage.OPERATOR__CORRESPONDING_INSTRUCTIONS:
				return correspondingInstructions != null && !correspondingInstructions.isEmpty();
			case FixedpointspecificationPackage.OPERATOR__USAGE_NUMBER:
				return usageNumber != USAGE_NUMBER_EDEFAULT;
			case FixedpointspecificationPackage.OPERATOR__INSTANCES:
				return instances != null && !instances.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case FixedpointspecificationPackage.OPERATOR___SET_INSTANCE__INT:
				setInstance((Integer)arguments.get(0));
				return null;
			case FixedpointspecificationPackage.OPERATOR___HAVE_NEXT_INSTANCE:
				return haveNextInstance();
			case FixedpointspecificationPackage.OPERATOR___GET_MAX_INSTANCE:
				return getMaxInstance();
			case FixedpointspecificationPackage.OPERATOR___GET_CURRENT_INSTANCE:
				return getCurrentInstance();
			case FixedpointspecificationPackage.OPERATOR___NEXT_INSTANCE:
				nextInstance();
				return null;
			case FixedpointspecificationPackage.OPERATOR___PREVIOUS_INSTANCE:
				previousInstance();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}
	
	@Override
	public FixedPointInformation getOutputInfo() {
		return getOperands().get(2);
	}

	@Override
	public String getName() {
		return kind + "_" + eval_cdfg_number + "_" + conv_cdfg_number;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
//		result.append("Operator -> eval_cdfg_number: ");
//		result.append(eval_cdfg_number);
//		result.append(", conv_cdfg_number: ");
//		result.append(conv_cdfg_number);
//		result.append(", kind: ");
//		result.append(kind.getLiteral());

		result.append(getName()).append(": ");
		for(FixedPointInformation info : getOperands()){
			result.append(" " + info);
		}
		
		return result.toString();
	}

} //OperatorImpl
