/**
 */
package fr.irisa.cairn.idfix.model.informationandtiming.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import float2fix.Float2fixPackage;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage;
import fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixprojectPackage;
import fr.irisa.cairn.idfix.model.idfixproject.impl.IdfixprojectPackageImpl;
import fr.irisa.cairn.idfix.model.informationandtiming.AnalyticInformations;
import fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingFactory;
import fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage;
import fr.irisa.cairn.idfix.model.informationandtiming.Informations;
import fr.irisa.cairn.idfix.model.informationandtiming.Section;
import fr.irisa.cairn.idfix.model.informationandtiming.SimulationInformations;
import fr.irisa.cairn.idfix.model.informationandtiming.TimeElement;
import fr.irisa.cairn.idfix.model.informationandtiming.Timing;
import fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryPackage;
import fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorlibraryPackageImpl;
import fr.irisa.cairn.idfix.model.solutionspace.SolutionspacePackage;
import fr.irisa.cairn.idfix.model.solutionspace.impl.SolutionspacePackageImpl;
import fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage;
import fr.irisa.cairn.idfix.model.userconfiguration.impl.UserconfigurationPackageImpl;
import gecos.annotations.AnnotationsPackage;
import gecos.blocks.BlocksPackage;
import gecos.core.CorePackage;
import gecos.dag.DagPackage;
import gecos.gecosproject.GecosprojectPackage;
import gecos.instrs.InstrsPackage;
import gecos.types.TypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class InformationandtimingPackageImpl extends EPackageImpl implements InformationandtimingPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass informationsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timeElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass analyticInformationsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simulationInformationsEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private InformationandtimingPackageImpl() {
		super(eNS_URI, InformationandtimingFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link InformationandtimingPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static InformationandtimingPackage init() {
		if (isInited) return (InformationandtimingPackage)EPackage.Registry.INSTANCE.getEPackage(InformationandtimingPackage.eNS_URI);

		// Obtain or create and register package
		InformationandtimingPackageImpl theInformationandtimingPackage = (InformationandtimingPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof InformationandtimingPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new InformationandtimingPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		Float2fixPackage.eINSTANCE.eClass();
		AnnotationsPackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		InstrsPackage.eINSTANCE.eClass();
		BlocksPackage.eINSTANCE.eClass();
		CorePackage.eINSTANCE.eClass();
		DagPackage.eINSTANCE.eClass();
		GecosprojectPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		SolutionspacePackageImpl theSolutionspacePackage = (SolutionspacePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SolutionspacePackage.eNS_URI) instanceof SolutionspacePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SolutionspacePackage.eNS_URI) : SolutionspacePackage.eINSTANCE);
		FixedpointspecificationPackageImpl theFixedpointspecificationPackage = (FixedpointspecificationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FixedpointspecificationPackage.eNS_URI) instanceof FixedpointspecificationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FixedpointspecificationPackage.eNS_URI) : FixedpointspecificationPackage.eINSTANCE);
		IdfixprojectPackageImpl theIdfixprojectPackage = (IdfixprojectPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(IdfixprojectPackage.eNS_URI) instanceof IdfixprojectPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(IdfixprojectPackage.eNS_URI) : IdfixprojectPackage.eINSTANCE);
		UserconfigurationPackageImpl theUserconfigurationPackage = (UserconfigurationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UserconfigurationPackage.eNS_URI) instanceof UserconfigurationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UserconfigurationPackage.eNS_URI) : UserconfigurationPackage.eINSTANCE);
		OperatorlibraryPackageImpl theOperatorlibraryPackage = (OperatorlibraryPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OperatorlibraryPackage.eNS_URI) instanceof OperatorlibraryPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OperatorlibraryPackage.eNS_URI) : OperatorlibraryPackage.eINSTANCE);

		// Create package meta-data objects
		theInformationandtimingPackage.createPackageContents();
		theSolutionspacePackage.createPackageContents();
		theFixedpointspecificationPackage.createPackageContents();
		theIdfixprojectPackage.createPackageContents();
		theUserconfigurationPackage.createPackageContents();
		theOperatorlibraryPackage.createPackageContents();

		// Initialize created meta-data
		theInformationandtimingPackage.initializePackageContents();
		theSolutionspacePackage.initializePackageContents();
		theFixedpointspecificationPackage.initializePackageContents();
		theIdfixprojectPackage.initializePackageContents();
		theUserconfigurationPackage.initializePackageContents();
		theOperatorlibraryPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theInformationandtimingPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(InformationandtimingPackage.eNS_URI, theInformationandtimingPackage);
		return theInformationandtimingPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInformations() {
		return informationsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInformations_OptimizationAlgorithmUsed() {
		return (EAttribute)informationsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInformations_UserNoiseConstraint() {
		return (EAttribute)informationsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInformations_Timing() {
		return (EReference)informationsEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInformations_UseLastResultModeEnable() {
		return (EAttribute)informationsEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInformations_AnalyticInformations() {
		return (EReference)informationsEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInformations_SimulationInformations() {
		return (EReference)informationsEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTiming() {
		return timingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTiming_Sections() {
		return (EReference)timingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimeElement() {
		return timeElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeElement_Description() {
		return (EAttribute)timeElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeElement_Time() {
		return (EAttribute)timeElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSection() {
		return sectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSection_Name() {
		return (EAttribute)sectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSection_Time() {
		return (EAttribute)sectionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSection_Elements() {
		return (EReference)sectionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSection__AddTimeLog__String_long_long() {
		return sectionEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSection__AddTimeLog__String_float() {
		return sectionEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnalyticInformations() {
		return analyticInformationsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnalyticInformations_FinalNoiseConstraint() {
		return (EAttribute)analyticInformationsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnalyticInformations_FinalCostConstraint() {
		return (EAttribute)analyticInformationsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnalyticInformations_NoiseEvalutationNumber() {
		return (EAttribute)analyticInformationsEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnalyticInformations_CostEvaluationNumber() {
		return (EAttribute)analyticInformationsEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimulationInformations() {
		return simulationInformationsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulationInformations_SampleNumber() {
		return (EAttribute)simulationInformationsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulationInformations_NoisePowerResult() {
		return (EAttribute)simulationInformationsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InformationandtimingFactory getInformationandtimingFactory() {
		return (InformationandtimingFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		informationsEClass = createEClass(INFORMATIONS);
		createEAttribute(informationsEClass, INFORMATIONS__OPTIMIZATION_ALGORITHM_USED);
		createEAttribute(informationsEClass, INFORMATIONS__USER_NOISE_CONSTRAINT);
		createEReference(informationsEClass, INFORMATIONS__TIMING);
		createEAttribute(informationsEClass, INFORMATIONS__USE_LAST_RESULT_MODE_ENABLE);
		createEReference(informationsEClass, INFORMATIONS__ANALYTIC_INFORMATIONS);
		createEReference(informationsEClass, INFORMATIONS__SIMULATION_INFORMATIONS);

		timingEClass = createEClass(TIMING);
		createEReference(timingEClass, TIMING__SECTIONS);

		timeElementEClass = createEClass(TIME_ELEMENT);
		createEAttribute(timeElementEClass, TIME_ELEMENT__DESCRIPTION);
		createEAttribute(timeElementEClass, TIME_ELEMENT__TIME);

		sectionEClass = createEClass(SECTION);
		createEAttribute(sectionEClass, SECTION__NAME);
		createEAttribute(sectionEClass, SECTION__TIME);
		createEReference(sectionEClass, SECTION__ELEMENTS);
		createEOperation(sectionEClass, SECTION___ADD_TIME_LOG__STRING_LONG_LONG);
		createEOperation(sectionEClass, SECTION___ADD_TIME_LOG__STRING_FLOAT);

		analyticInformationsEClass = createEClass(ANALYTIC_INFORMATIONS);
		createEAttribute(analyticInformationsEClass, ANALYTIC_INFORMATIONS__FINAL_NOISE_CONSTRAINT);
		createEAttribute(analyticInformationsEClass, ANALYTIC_INFORMATIONS__FINAL_COST_CONSTRAINT);
		createEAttribute(analyticInformationsEClass, ANALYTIC_INFORMATIONS__NOISE_EVALUTATION_NUMBER);
		createEAttribute(analyticInformationsEClass, ANALYTIC_INFORMATIONS__COST_EVALUATION_NUMBER);

		simulationInformationsEClass = createEClass(SIMULATION_INFORMATIONS);
		createEAttribute(simulationInformationsEClass, SIMULATION_INFORMATIONS__SAMPLE_NUMBER);
		createEAttribute(simulationInformationsEClass, SIMULATION_INFORMATIONS__NOISE_POWER_RESULT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(informationsEClass, Informations.class, "Informations", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getInformations_OptimizationAlgorithmUsed(), ecorePackage.getEString(), "optimizationAlgorithmUsed", null, 0, 1, Informations.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInformations_UserNoiseConstraint(), ecorePackage.getEFloat(), "userNoiseConstraint", null, 0, 1, Informations.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInformations_Timing(), this.getTiming(), null, "timing", null, 0, 1, Informations.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInformations_UseLastResultModeEnable(), ecorePackage.getEBoolean(), "useLastResultModeEnable", "false", 0, 1, Informations.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInformations_AnalyticInformations(), this.getAnalyticInformations(), null, "analyticInformations", null, 0, 1, Informations.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInformations_SimulationInformations(), this.getSimulationInformations(), null, "simulationInformations", null, 0, 1, Informations.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(timingEClass, Timing.class, "Timing", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTiming_Sections(), this.getSection(), null, "sections", null, 0, -1, Timing.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(timeElementEClass, TimeElement.class, "TimeElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTimeElement_Description(), ecorePackage.getEString(), "description", null, 0, 1, TimeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimeElement_Time(), ecorePackage.getEFloat(), "time", null, 0, 1, TimeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sectionEClass, Section.class, "Section", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSection_Name(), ecorePackage.getEString(), "name", null, 0, 1, Section.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSection_Time(), ecorePackage.getEFloat(), "time", "-1", 0, 1, Section.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSection_Elements(), this.getTimeElement(), null, "elements", null, 0, -1, Section.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getSection__AddTimeLog__String_long_long(), null, "addTimeLog", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "description", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getELong(), "start", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getELong(), "end", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getSection__AddTimeLog__String_float(), null, "addTimeLog", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "description", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEFloat(), "time", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(analyticInformationsEClass, AnalyticInformations.class, "AnalyticInformations", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAnalyticInformations_FinalNoiseConstraint(), ecorePackage.getEDouble(), "finalNoiseConstraint", null, 0, 1, AnalyticInformations.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAnalyticInformations_FinalCostConstraint(), ecorePackage.getEFloat(), "finalCostConstraint", null, 0, 1, AnalyticInformations.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAnalyticInformations_NoiseEvalutationNumber(), ecorePackage.getEInt(), "noiseEvalutationNumber", null, 0, 1, AnalyticInformations.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAnalyticInformations_CostEvaluationNumber(), ecorePackage.getEInt(), "costEvaluationNumber", null, 0, 1, AnalyticInformations.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(simulationInformationsEClass, SimulationInformations.class, "SimulationInformations", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSimulationInformations_SampleNumber(), ecorePackage.getEInt(), "sampleNumber", null, 0, 1, SimulationInformations.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulationInformations_NoisePowerResult(), ecorePackage.getEDouble(), "noisePowerResult", null, 0, 1, SimulationInformations.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //InformationandtimingPackageImpl
