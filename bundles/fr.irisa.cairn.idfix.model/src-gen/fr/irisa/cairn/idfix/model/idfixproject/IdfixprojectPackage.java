/**
 */
package fr.irisa.cairn.idfix.model.idfixproject;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixprojectFactory
 * @model kind="package"
 * @generated
 */
public interface IdfixprojectPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "idfixproject";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://idfix.fr/fr/irisa/cairn/idfix/model/idfixproject";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "idfixproject";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	IdfixprojectPackage eINSTANCE = fr.irisa.cairn.idfix.model.idfixproject.impl.IdfixprojectPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.idfixproject.impl.IdfixProjectImpl <em>Idfix Project</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.idfixproject.impl.IdfixProjectImpl
	 * @see fr.irisa.cairn.idfix.model.idfixproject.impl.IdfixprojectPackageImpl#getIdfixProject()
	 * @generated
	 */
	int IDFIX_PROJECT = 0;

	/**
	 * The feature id for the '<em><b>Gecos Project</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDFIX_PROJECT__GECOS_PROJECT = 0;

	/**
	 * The feature id for the '<em><b>Gecos Project Annoted And Not Unrolled</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDFIX_PROJECT__GECOS_PROJECT_ANNOTED_AND_NOT_UNROLLED = 1;

	/**
	 * The feature id for the '<em><b>Fixed Point Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDFIX_PROJECT__FIXED_POINT_SPECIFICATION = 2;

	/**
	 * The feature id for the '<em><b>Output Folder Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDFIX_PROJECT__OUTPUT_FOLDER_PATH = 3;

	/**
	 * The feature id for the '<em><b>Process Informations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDFIX_PROJECT__PROCESS_INFORMATIONS = 4;

	/**
	 * The feature id for the '<em><b>User Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDFIX_PROJECT__USER_CONFIGURATION = 5;

	/**
	 * The number of structural features of the '<em>Idfix Project</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDFIX_PROJECT_FEATURE_COUNT = 6;

	/**
	 * The operation id for the '<em>Get ID Fix Eval Output Folder Path</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDFIX_PROJECT___GET_ID_FIX_EVAL_OUTPUT_FOLDER_PATH = 0;

	/**
	 * The operation id for the '<em>Get ID Fix Conv Output Folder Path</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDFIX_PROJECT___GET_ID_FIX_CONV_OUTPUT_FOLDER_PATH = 1;

	/**
	 * The operation id for the '<em>Get SFG File Path</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDFIX_PROJECT___GET_SFG_FILE_PATH = 2;

	/**
	 * The operation id for the '<em>Get Simulated Values File Path</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDFIX_PROJECT___GET_SIMULATED_VALUES_FILE_PATH = 3;

	/**
	 * The operation id for the '<em>Get Dynamic File Path</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDFIX_PROJECT___GET_DYNAMIC_FILE_PATH = 4;

	/**
	 * The operation id for the '<em>Get Noise Library File Path</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDFIX_PROJECT___GET_NOISE_LIBRARY_FILE_PATH = 5;

	/**
	 * The operation id for the '<em>Generate Log Informations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDFIX_PROJECT___GENERATE_LOG_INFORMATIONS = 6;

	/**
	 * The operation id for the '<em>Finalize</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDFIX_PROJECT___FINALIZE = 7;

	/**
	 * The operation id for the '<em>Plot Save Informations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDFIX_PROJECT___PLOT_SAVE_INFORMATIONS = 8;

	/**
	 * The number of operations of the '<em>Idfix Project</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDFIX_PROJECT_OPERATION_COUNT = 9;


	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject <em>Idfix Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Idfix Project</em>'.
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixProject
	 * @generated
	 */
	EClass getIdfixProject();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getGecosProject <em>Gecos Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Gecos Project</em>'.
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getGecosProject()
	 * @see #getIdfixProject()
	 * @generated
	 */
	EReference getIdfixProject_GecosProject();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getGecosProjectAnnotedAndNotUnrolled <em>Gecos Project Annoted And Not Unrolled</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Gecos Project Annoted And Not Unrolled</em>'.
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getGecosProjectAnnotedAndNotUnrolled()
	 * @see #getIdfixProject()
	 * @generated
	 */
	EReference getIdfixProject_GecosProjectAnnotedAndNotUnrolled();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getFixedPointSpecification <em>Fixed Point Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Fixed Point Specification</em>'.
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getFixedPointSpecification()
	 * @see #getIdfixProject()
	 * @generated
	 */
	EReference getIdfixProject_FixedPointSpecification();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getOutputFolderPath <em>Output Folder Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output Folder Path</em>'.
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getOutputFolderPath()
	 * @see #getIdfixProject()
	 * @generated
	 */
	EAttribute getIdfixProject_OutputFolderPath();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getProcessInformations <em>Process Informations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Process Informations</em>'.
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getProcessInformations()
	 * @see #getIdfixProject()
	 * @generated
	 */
	EReference getIdfixProject_ProcessInformations();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getUserConfiguration <em>User Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>User Configuration</em>'.
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getUserConfiguration()
	 * @see #getIdfixProject()
	 * @generated
	 */
	EReference getIdfixProject_UserConfiguration();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getIDFixEvalOutputFolderPath() <em>Get ID Fix Eval Output Folder Path</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get ID Fix Eval Output Folder Path</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getIDFixEvalOutputFolderPath()
	 * @generated
	 */
	EOperation getIdfixProject__GetIDFixEvalOutputFolderPath();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getIDFixConvOutputFolderPath() <em>Get ID Fix Conv Output Folder Path</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get ID Fix Conv Output Folder Path</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getIDFixConvOutputFolderPath()
	 * @generated
	 */
	EOperation getIdfixProject__GetIDFixConvOutputFolderPath();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getSFGFilePath() <em>Get SFG File Path</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get SFG File Path</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getSFGFilePath()
	 * @generated
	 */
	EOperation getIdfixProject__GetSFGFilePath();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getSimulatedValuesFilePath() <em>Get Simulated Values File Path</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Simulated Values File Path</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getSimulatedValuesFilePath()
	 * @generated
	 */
	EOperation getIdfixProject__GetSimulatedValuesFilePath();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getDynamicFilePath() <em>Get Dynamic File Path</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Dynamic File Path</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getDynamicFilePath()
	 * @generated
	 */
	EOperation getIdfixProject__GetDynamicFilePath();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getNoiseLibraryFilePath() <em>Get Noise Library File Path</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Noise Library File Path</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getNoiseLibraryFilePath()
	 * @generated
	 */
	EOperation getIdfixProject__GetNoiseLibraryFilePath();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#generateLogInformations() <em>Generate Log Informations</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Generate Log Informations</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#generateLogInformations()
	 * @generated
	 */
	EOperation getIdfixProject__GenerateLogInformations();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#finalize() <em>Finalize</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Finalize</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#finalize()
	 * @generated
	 */
	EOperation getIdfixProject__Finalize();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#plotSaveInformations() <em>Plot Save Informations</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Plot Save Informations</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#plotSaveInformations()
	 * @generated
	 */
	EOperation getIdfixProject__PlotSaveInformations();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	IdfixprojectFactory getIdfixprojectFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.idfixproject.impl.IdfixProjectImpl <em>Idfix Project</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.idfixproject.impl.IdfixProjectImpl
		 * @see fr.irisa.cairn.idfix.model.idfixproject.impl.IdfixprojectPackageImpl#getIdfixProject()
		 * @generated
		 */
		EClass IDFIX_PROJECT = eINSTANCE.getIdfixProject();

		/**
		 * The meta object literal for the '<em><b>Gecos Project</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IDFIX_PROJECT__GECOS_PROJECT = eINSTANCE.getIdfixProject_GecosProject();

		/**
		 * The meta object literal for the '<em><b>Gecos Project Annoted And Not Unrolled</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IDFIX_PROJECT__GECOS_PROJECT_ANNOTED_AND_NOT_UNROLLED = eINSTANCE.getIdfixProject_GecosProjectAnnotedAndNotUnrolled();

		/**
		 * The meta object literal for the '<em><b>Fixed Point Specification</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IDFIX_PROJECT__FIXED_POINT_SPECIFICATION = eINSTANCE.getIdfixProject_FixedPointSpecification();

		/**
		 * The meta object literal for the '<em><b>Output Folder Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDFIX_PROJECT__OUTPUT_FOLDER_PATH = eINSTANCE.getIdfixProject_OutputFolderPath();

		/**
		 * The meta object literal for the '<em><b>Process Informations</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IDFIX_PROJECT__PROCESS_INFORMATIONS = eINSTANCE.getIdfixProject_ProcessInformations();

		/**
		 * The meta object literal for the '<em><b>User Configuration</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IDFIX_PROJECT__USER_CONFIGURATION = eINSTANCE.getIdfixProject_UserConfiguration();

		/**
		 * The meta object literal for the '<em><b>Get ID Fix Eval Output Folder Path</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IDFIX_PROJECT___GET_ID_FIX_EVAL_OUTPUT_FOLDER_PATH = eINSTANCE.getIdfixProject__GetIDFixEvalOutputFolderPath();

		/**
		 * The meta object literal for the '<em><b>Get ID Fix Conv Output Folder Path</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IDFIX_PROJECT___GET_ID_FIX_CONV_OUTPUT_FOLDER_PATH = eINSTANCE.getIdfixProject__GetIDFixConvOutputFolderPath();

		/**
		 * The meta object literal for the '<em><b>Get SFG File Path</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IDFIX_PROJECT___GET_SFG_FILE_PATH = eINSTANCE.getIdfixProject__GetSFGFilePath();

		/**
		 * The meta object literal for the '<em><b>Get Simulated Values File Path</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IDFIX_PROJECT___GET_SIMULATED_VALUES_FILE_PATH = eINSTANCE.getIdfixProject__GetSimulatedValuesFilePath();

		/**
		 * The meta object literal for the '<em><b>Get Dynamic File Path</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IDFIX_PROJECT___GET_DYNAMIC_FILE_PATH = eINSTANCE.getIdfixProject__GetDynamicFilePath();

		/**
		 * The meta object literal for the '<em><b>Get Noise Library File Path</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IDFIX_PROJECT___GET_NOISE_LIBRARY_FILE_PATH = eINSTANCE.getIdfixProject__GetNoiseLibraryFilePath();

		/**
		 * The meta object literal for the '<em><b>Generate Log Informations</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IDFIX_PROJECT___GENERATE_LOG_INFORMATIONS = eINSTANCE.getIdfixProject__GenerateLogInformations();

		/**
		 * The meta object literal for the '<em><b>Finalize</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IDFIX_PROJECT___FINALIZE = eINSTANCE.getIdfixProject__Finalize();

		/**
		 * The meta object literal for the '<em><b>Plot Save Informations</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IDFIX_PROJECT___PLOT_SAVE_INFORMATIONS = eINSTANCE.getIdfixProject__PlotSaveInformations();

	}

} //IdfixprojectPackage
