/**
 */
package fr.irisa.cairn.idfix.model.informationandtiming.impl;

import fr.irisa.cairn.idfix.model.informationandtiming.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingFactory;
import fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage;
import fr.irisa.cairn.idfix.model.informationandtiming.Informations;
import fr.irisa.cairn.idfix.model.informationandtiming.Section;
import fr.irisa.cairn.idfix.model.informationandtiming.TimeElement;
import fr.irisa.cairn.idfix.model.informationandtiming.Timing;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class InformationandtimingFactoryImpl extends EFactoryImpl implements InformationandtimingFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static InformationandtimingFactory init() {
		try {
			InformationandtimingFactory theInformationandtimingFactory = (InformationandtimingFactory)EPackage.Registry.INSTANCE.getEFactory(InformationandtimingPackage.eNS_URI);
			if (theInformationandtimingFactory != null) {
				return theInformationandtimingFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new InformationandtimingFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InformationandtimingFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case InformationandtimingPackage.INFORMATIONS: return createInformations();
			case InformationandtimingPackage.TIMING: return createTiming();
			case InformationandtimingPackage.TIME_ELEMENT: return createTimeElement();
			case InformationandtimingPackage.SECTION: return createSection();
			case InformationandtimingPackage.ANALYTIC_INFORMATIONS: return createAnalyticInformations();
			case InformationandtimingPackage.SIMULATION_INFORMATIONS: return createSimulationInformations();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Informations createInformations() {
		InformationsImpl informations = new InformationsImpl();
		return informations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Timing createTiming() {
		TimingImpl timing = new TimingImpl();
		return timing;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeElement createTimeElement() {
		TimeElementImpl timeElement = new TimeElementImpl();
		return timeElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Section createSection() {
		SectionImpl section = new SectionImpl();
		return section;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnalyticInformations createAnalyticInformations() {
		AnalyticInformationsImpl analyticInformations = new AnalyticInformationsImpl();
		return analyticInformations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationInformations createSimulationInformations() {
		SimulationInformationsImpl simulationInformations = new SimulationInformationsImpl();
		return simulationInformations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InformationandtimingPackage getInformationandtimingPackage() {
		return (InformationandtimingPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static InformationandtimingPackage getPackage() {
		return InformationandtimingPackage.eINSTANCE;
	}

} //InformationandtimingFactoryImpl
