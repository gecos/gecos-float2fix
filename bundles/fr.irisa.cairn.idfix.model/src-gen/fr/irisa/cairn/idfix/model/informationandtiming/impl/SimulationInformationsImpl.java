/**
 */
package fr.irisa.cairn.idfix.model.informationandtiming.impl;

import fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage;
import fr.irisa.cairn.idfix.model.informationandtiming.SimulationInformations;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simulation Informations</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.SimulationInformationsImpl#getSampleNumber <em>Sample Number</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.SimulationInformationsImpl#getNoisePowerResult <em>Noise Power Result</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SimulationInformationsImpl extends MinimalEObjectImpl.Container implements SimulationInformations {
	/**
	 * The default value of the '{@link #getSampleNumber() <em>Sample Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSampleNumber()
	 * @generated
	 * @ordered
	 */
	protected static final int SAMPLE_NUMBER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getSampleNumber() <em>Sample Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSampleNumber()
	 * @generated
	 * @ordered
	 */
	protected int sampleNumber = SAMPLE_NUMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #getNoisePowerResult() <em>Noise Power Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoisePowerResult()
	 * @generated
	 * @ordered
	 */
	protected static final double NOISE_POWER_RESULT_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getNoisePowerResult() <em>Noise Power Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoisePowerResult()
	 * @generated
	 * @ordered
	 */
	protected double noisePowerResult = NOISE_POWER_RESULT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimulationInformationsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InformationandtimingPackage.Literals.SIMULATION_INFORMATIONS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSampleNumber() {
		return sampleNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSampleNumber(int newSampleNumber) {
		int oldSampleNumber = sampleNumber;
		sampleNumber = newSampleNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InformationandtimingPackage.SIMULATION_INFORMATIONS__SAMPLE_NUMBER, oldSampleNumber, sampleNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getNoisePowerResult() {
		return noisePowerResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNoisePowerResult(double newNoisePowerResult) {
		double oldNoisePowerResult = noisePowerResult;
		noisePowerResult = newNoisePowerResult;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InformationandtimingPackage.SIMULATION_INFORMATIONS__NOISE_POWER_RESULT, oldNoisePowerResult, noisePowerResult));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InformationandtimingPackage.SIMULATION_INFORMATIONS__SAMPLE_NUMBER:
				return getSampleNumber();
			case InformationandtimingPackage.SIMULATION_INFORMATIONS__NOISE_POWER_RESULT:
				return getNoisePowerResult();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InformationandtimingPackage.SIMULATION_INFORMATIONS__SAMPLE_NUMBER:
				setSampleNumber((Integer)newValue);
				return;
			case InformationandtimingPackage.SIMULATION_INFORMATIONS__NOISE_POWER_RESULT:
				setNoisePowerResult((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InformationandtimingPackage.SIMULATION_INFORMATIONS__SAMPLE_NUMBER:
				setSampleNumber(SAMPLE_NUMBER_EDEFAULT);
				return;
			case InformationandtimingPackage.SIMULATION_INFORMATIONS__NOISE_POWER_RESULT:
				setNoisePowerResult(NOISE_POWER_RESULT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InformationandtimingPackage.SIMULATION_INFORMATIONS__SAMPLE_NUMBER:
				return sampleNumber != SAMPLE_NUMBER_EDEFAULT;
			case InformationandtimingPackage.SIMULATION_INFORMATIONS__NOISE_POWER_RESULT:
				return noisePowerResult != NOISE_POWER_RESULT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (sampleNumber: ");
		result.append(sampleNumber);
		result.append(", noisePowerResult: ");
		result.append(noisePowerResult);
		result.append(')');
		return result.toString();
	}

} //SimulationInformationsImpl
