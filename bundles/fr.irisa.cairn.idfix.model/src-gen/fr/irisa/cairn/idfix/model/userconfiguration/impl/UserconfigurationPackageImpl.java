/**
 */
package fr.irisa.cairn.idfix.model.userconfiguration.impl;

import float2fix.Float2fixPackage;

import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage;

import fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl;

import fr.irisa.cairn.idfix.model.idfixproject.IdfixprojectPackage;

import fr.irisa.cairn.idfix.model.idfixproject.impl.IdfixprojectPackageImpl;

import fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage;

import fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationandtimingPackageImpl;

import fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryPackage;
import fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorlibraryPackageImpl;
import fr.irisa.cairn.idfix.model.solutionspace.SolutionspacePackage;

import fr.irisa.cairn.idfix.model.solutionspace.impl.SolutionspacePackageImpl;

import fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration;
import fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationFactory;
import fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage;

import gecos.annotations.AnnotationsPackage;

import gecos.blocks.BlocksPackage;

import gecos.core.CorePackage;

import gecos.dag.DagPackage;

import gecos.gecosproject.GecosprojectPackage;

import gecos.instrs.InstrsPackage;

import gecos.types.TypesPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UserconfigurationPackageImpl extends EPackageImpl implements UserconfigurationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass userConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum engineEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum noisE_FUNCTION_LOADEREEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum runtimE_MODEEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum noisE_FUNCTION_OUTPUT_MODEEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum datA_TYPE_REGENERATIONEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private UserconfigurationPackageImpl() {
		super(eNS_URI, UserconfigurationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link UserconfigurationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static UserconfigurationPackage init() {
		if (isInited) return (UserconfigurationPackage)EPackage.Registry.INSTANCE.getEPackage(UserconfigurationPackage.eNS_URI);

		// Obtain or create and register package
		UserconfigurationPackageImpl theUserconfigurationPackage = (UserconfigurationPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof UserconfigurationPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new UserconfigurationPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		Float2fixPackage.eINSTANCE.eClass();
		AnnotationsPackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		InstrsPackage.eINSTANCE.eClass();
		BlocksPackage.eINSTANCE.eClass();
		CorePackage.eINSTANCE.eClass();
		DagPackage.eINSTANCE.eClass();
		GecosprojectPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		SolutionspacePackageImpl theSolutionspacePackage = (SolutionspacePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SolutionspacePackage.eNS_URI) instanceof SolutionspacePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SolutionspacePackage.eNS_URI) : SolutionspacePackage.eINSTANCE);
		FixedpointspecificationPackageImpl theFixedpointspecificationPackage = (FixedpointspecificationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FixedpointspecificationPackage.eNS_URI) instanceof FixedpointspecificationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FixedpointspecificationPackage.eNS_URI) : FixedpointspecificationPackage.eINSTANCE);
		IdfixprojectPackageImpl theIdfixprojectPackage = (IdfixprojectPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(IdfixprojectPackage.eNS_URI) instanceof IdfixprojectPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(IdfixprojectPackage.eNS_URI) : IdfixprojectPackage.eINSTANCE);
		InformationandtimingPackageImpl theInformationandtimingPackage = (InformationandtimingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(InformationandtimingPackage.eNS_URI) instanceof InformationandtimingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(InformationandtimingPackage.eNS_URI) : InformationandtimingPackage.eINSTANCE);
		OperatorlibraryPackageImpl theOperatorlibraryPackage = (OperatorlibraryPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OperatorlibraryPackage.eNS_URI) instanceof OperatorlibraryPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OperatorlibraryPackage.eNS_URI) : OperatorlibraryPackage.eINSTANCE);

		// Create package meta-data objects
		theUserconfigurationPackage.createPackageContents();
		theSolutionspacePackage.createPackageContents();
		theFixedpointspecificationPackage.createPackageContents();
		theIdfixprojectPackage.createPackageContents();
		theInformationandtimingPackage.createPackageContents();
		theOperatorlibraryPackage.createPackageContents();

		// Initialize created meta-data
		theUserconfigurationPackage.initializePackageContents();
		theSolutionspacePackage.initializePackageContents();
		theFixedpointspecificationPackage.initializePackageContents();
		theIdfixprojectPackage.initializePackageContents();
		theInformationandtimingPackage.initializePackageContents();
		theOperatorlibraryPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theUserconfigurationPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(UserconfigurationPackage.eNS_URI, theUserconfigurationPackage);
		return theUserconfigurationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUserConfiguration() {
		return userConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserConfiguration_TheadNumber() {
		return (EAttribute)userConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserConfiguration_OptimizationGraphDisplay() {
		return (EAttribute)userConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserConfiguration_GenerateSummary() {
		return (EAttribute)userConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserConfiguration_Engine() {
		return (EAttribute)userConfigurationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserConfiguration_NoiseFunctionLoader() {
		return (EAttribute)userConfigurationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserConfiguration_RuntimeMode() {
		return (EAttribute)userConfigurationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserConfiguration_MaximumFolder() {
		return (EAttribute)userConfigurationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserConfiguration_BackendSimulatorSampleNumber() {
		return (EAttribute)userConfigurationEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserConfiguration_NoiseFunctionOutputMode() {
		return (EAttribute)userConfigurationEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserConfiguration_OperatorQuantificationType() {
		return (EAttribute)userConfigurationEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserConfiguration_OperatorOverflowType() {
		return (EAttribute)userConfigurationEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserConfiguration_OptimizationSimulatorSampleNumber() {
		return (EAttribute)userConfigurationEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserConfiguration_OptimizationSimulatorFrequency() {
		return (EAttribute)userConfigurationEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserConfiguration_OptimizationSimulatorSuperiorLimit() {
		return (EAttribute)userConfigurationEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserConfiguration_DataTypeRegneration() {
		return (EAttribute)userConfigurationEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserConfiguration_OperatorCharacteristic() {
		return (EAttribute)userConfigurationEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserConfiguration_TagFolder() {
		return (EAttribute)userConfigurationEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserConfiguration_CorrelationMode() {
		return (EAttribute)userConfigurationEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getUserConfiguration__IsReleaseMode() {
		return userConfigurationEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getUserConfiguration__IsDebugMode() {
		return userConfigurationEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getUserConfiguration__IsTraceMode() {
		return userConfigurationEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getUserConfiguration__BackendSimulationIsActivated() {
		return userConfigurationEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getUserConfiguration__OptimizationSimulationIsActivated() {
		return userConfigurationEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getENGINE() {
		return engineEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getNOISE_FUNCTION_LOADER() {
		return noisE_FUNCTION_LOADEREEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRUNTIME_MODE() {
		return runtimE_MODEEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getNOISE_FUNCTION_OUTPUT_MODE() {
		return noisE_FUNCTION_OUTPUT_MODEEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDATA_TYPE_REGENERATION() {
		return datA_TYPE_REGENERATIONEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserconfigurationFactory getUserconfigurationFactory() {
		return (UserconfigurationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		userConfigurationEClass = createEClass(USER_CONFIGURATION);
		createEAttribute(userConfigurationEClass, USER_CONFIGURATION__THEAD_NUMBER);
		createEAttribute(userConfigurationEClass, USER_CONFIGURATION__OPTIMIZATION_GRAPH_DISPLAY);
		createEAttribute(userConfigurationEClass, USER_CONFIGURATION__GENERATE_SUMMARY);
		createEAttribute(userConfigurationEClass, USER_CONFIGURATION__ENGINE);
		createEAttribute(userConfigurationEClass, USER_CONFIGURATION__NOISE_FUNCTION_LOADER);
		createEAttribute(userConfigurationEClass, USER_CONFIGURATION__RUNTIME_MODE);
		createEAttribute(userConfigurationEClass, USER_CONFIGURATION__MAXIMUM_FOLDER);
		createEAttribute(userConfigurationEClass, USER_CONFIGURATION__BACKEND_SIMULATOR_SAMPLE_NUMBER);
		createEAttribute(userConfigurationEClass, USER_CONFIGURATION__NOISE_FUNCTION_OUTPUT_MODE);
		createEAttribute(userConfigurationEClass, USER_CONFIGURATION__OPERATOR_QUANTIFICATION_TYPE);
		createEAttribute(userConfigurationEClass, USER_CONFIGURATION__OPERATOR_OVERFLOW_TYPE);
		createEAttribute(userConfigurationEClass, USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_SAMPLE_NUMBER);
		createEAttribute(userConfigurationEClass, USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_FREQUENCY);
		createEAttribute(userConfigurationEClass, USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_SUPERIOR_LIMIT);
		createEAttribute(userConfigurationEClass, USER_CONFIGURATION__DATA_TYPE_REGNERATION);
		createEAttribute(userConfigurationEClass, USER_CONFIGURATION__OPERATOR_CHARACTERISTIC);
		createEAttribute(userConfigurationEClass, USER_CONFIGURATION__TAG_FOLDER);
		createEAttribute(userConfigurationEClass, USER_CONFIGURATION__CORRELATION_MODE);
		createEOperation(userConfigurationEClass, USER_CONFIGURATION___IS_RELEASE_MODE);
		createEOperation(userConfigurationEClass, USER_CONFIGURATION___IS_DEBUG_MODE);
		createEOperation(userConfigurationEClass, USER_CONFIGURATION___IS_TRACE_MODE);
		createEOperation(userConfigurationEClass, USER_CONFIGURATION___BACKEND_SIMULATION_IS_ACTIVATED);
		createEOperation(userConfigurationEClass, USER_CONFIGURATION___OPTIMIZATION_SIMULATION_IS_ACTIVATED);

		// Create enums
		engineEEnum = createEEnum(ENGINE);
		noisE_FUNCTION_LOADEREEnum = createEEnum(NOISE_FUNCTION_LOADER);
		runtimE_MODEEEnum = createEEnum(RUNTIME_MODE);
		noisE_FUNCTION_OUTPUT_MODEEEnum = createEEnum(NOISE_FUNCTION_OUTPUT_MODE);
		datA_TYPE_REGENERATIONEEnum = createEEnum(DATA_TYPE_REGENERATION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		FixedpointspecificationPackage theFixedpointspecificationPackage = (FixedpointspecificationPackage)EPackage.Registry.INSTANCE.getEPackage(FixedpointspecificationPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(userConfigurationEClass, UserConfiguration.class, "UserConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUserConfiguration_TheadNumber(), ecorePackage.getEInt(), "theadNumber", "1", 0, 1, UserConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserConfiguration_OptimizationGraphDisplay(), ecorePackage.getEBoolean(), "optimizationGraphDisplay", "false", 0, 1, UserConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserConfiguration_GenerateSummary(), ecorePackage.getEBoolean(), "generateSummary", "false", 0, 1, UserConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserConfiguration_Engine(), this.getENGINE(), "engine", "MATLAB", 0, 1, UserConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserConfiguration_NoiseFunctionLoader(), this.getNOISE_FUNCTION_LOADER(), "noiseFunctionLoader", "JNA", 0, 1, UserConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserConfiguration_RuntimeMode(), this.getRUNTIME_MODE(), "runtimeMode", "RELEASE", 0, 1, UserConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserConfiguration_MaximumFolder(), ecorePackage.getEInt(), "maximumFolder", "0", 0, 1, UserConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserConfiguration_BackendSimulatorSampleNumber(), ecorePackage.getEInt(), "backendSimulatorSampleNumber", "0", 0, 1, UserConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserConfiguration_NoiseFunctionOutputMode(), this.getNOISE_FUNCTION_OUTPUT_MODE(), "noiseFunctionOutputMode", "WORST", 0, 1, UserConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserConfiguration_OperatorQuantificationType(), theFixedpointspecificationPackage.getQUANTIFICATION_TYPE(), "operatorQuantificationType", "TRUNCATION", 0, 1, UserConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserConfiguration_OperatorOverflowType(), theFixedpointspecificationPackage.getOVERFLOW_TYPE(), "operatorOverflowType", "SATURATION", 0, 1, UserConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserConfiguration_OptimizationSimulatorSampleNumber(), ecorePackage.getEInt(), "optimizationSimulatorSampleNumber", null, 0, 1, UserConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserConfiguration_OptimizationSimulatorFrequency(), ecorePackage.getEInt(), "optimizationSimulatorFrequency", null, 0, 1, UserConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserConfiguration_OptimizationSimulatorSuperiorLimit(), ecorePackage.getEFloat(), "optimizationSimulatorSuperiorLimit", null, 0, 1, UserConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserConfiguration_DataTypeRegneration(), this.getDATA_TYPE_REGENERATION(), "dataTypeRegneration", "SC_FIXED", 0, 1, UserConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserConfiguration_OperatorCharacteristic(), ecorePackage.getEString(), "operatorCharacteristic", "energy", 0, 1, UserConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserConfiguration_TagFolder(), ecorePackage.getEBoolean(), "tagFolder", "true", 0, 1, UserConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUserConfiguration_CorrelationMode(), ecorePackage.getEBoolean(), "correlationMode", "false", 0, 1, UserConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getUserConfiguration__IsReleaseMode(), ecorePackage.getEBoolean(), "isReleaseMode", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getUserConfiguration__IsDebugMode(), ecorePackage.getEBoolean(), "isDebugMode", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getUserConfiguration__IsTraceMode(), ecorePackage.getEBoolean(), "isTraceMode", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getUserConfiguration__BackendSimulationIsActivated(), ecorePackage.getEBoolean(), "backendSimulationIsActivated", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getUserConfiguration__OptimizationSimulationIsActivated(), ecorePackage.getEBoolean(), "optimizationSimulationIsActivated", 0, 1, IS_UNIQUE, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(engineEEnum, fr.irisa.cairn.idfix.model.userconfiguration.ENGINE.class, "ENGINE");
		addEEnumLiteral(engineEEnum, fr.irisa.cairn.idfix.model.userconfiguration.ENGINE.MATLAB);
		addEEnumLiteral(engineEEnum, fr.irisa.cairn.idfix.model.userconfiguration.ENGINE.OCTAVE);

		initEEnum(noisE_FUNCTION_LOADEREEnum, fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_LOADER.class, "NOISE_FUNCTION_LOADER");
		addEEnumLiteral(noisE_FUNCTION_LOADEREEnum, fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_LOADER.JNA);
		addEEnumLiteral(noisE_FUNCTION_LOADEREEnum, fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_LOADER.JNI);

		initEEnum(runtimE_MODEEEnum, fr.irisa.cairn.idfix.model.userconfiguration.RUNTIME_MODE.class, "RUNTIME_MODE");
		addEEnumLiteral(runtimE_MODEEEnum, fr.irisa.cairn.idfix.model.userconfiguration.RUNTIME_MODE.RELEASE);
		addEEnumLiteral(runtimE_MODEEEnum, fr.irisa.cairn.idfix.model.userconfiguration.RUNTIME_MODE.DEBUG);
		addEEnumLiteral(runtimE_MODEEEnum, fr.irisa.cairn.idfix.model.userconfiguration.RUNTIME_MODE.TRACE);

		initEEnum(noisE_FUNCTION_OUTPUT_MODEEEnum, fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_OUTPUT_MODE.class, "NOISE_FUNCTION_OUTPUT_MODE");
		addEEnumLiteral(noisE_FUNCTION_OUTPUT_MODEEEnum, fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_OUTPUT_MODE.WORST);
		addEEnumLiteral(noisE_FUNCTION_OUTPUT_MODEEEnum, fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_OUTPUT_MODE.MEAN);

		initEEnum(datA_TYPE_REGENERATIONEEnum, fr.irisa.cairn.idfix.model.userconfiguration.DATA_TYPE_REGENERATION.class, "DATA_TYPE_REGENERATION");
		addEEnumLiteral(datA_TYPE_REGENERATIONEEnum, fr.irisa.cairn.idfix.model.userconfiguration.DATA_TYPE_REGENERATION.SC_FIXED);
		addEEnumLiteral(datA_TYPE_REGENERATIONEEnum, fr.irisa.cairn.idfix.model.userconfiguration.DATA_TYPE_REGENERATION.AC_FIXED);
		addEEnumLiteral(datA_TYPE_REGENERATIONEEnum, fr.irisa.cairn.idfix.model.userconfiguration.DATA_TYPE_REGENERATION.CINLINE);
		addEEnumLiteral(datA_TYPE_REGENERATIONEEnum, fr.irisa.cairn.idfix.model.userconfiguration.DATA_TYPE_REGENERATION.CMACROS);

		// Create resource
		createResource(eNS_URI);
	}

} //UserconfigurationPackageImpl
