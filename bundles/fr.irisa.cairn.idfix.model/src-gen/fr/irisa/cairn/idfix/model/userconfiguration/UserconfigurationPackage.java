/**
 */
package fr.irisa.cairn.idfix.model.userconfiguration;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationFactory
 * @model kind="package"
 * @generated
 */
public interface UserconfigurationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "userconfiguration";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://idfix.fr/fr/irisa/cairn/idfix/model/userconfiguration";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "userconfiguration";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UserconfigurationPackage eINSTANCE = fr.irisa.cairn.idfix.model.userconfiguration.impl.UserconfigurationPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.userconfiguration.impl.UserConfigurationImpl <em>User Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.impl.UserConfigurationImpl
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.impl.UserconfigurationPackageImpl#getUserConfiguration()
	 * @generated
	 */
	int USER_CONFIGURATION = 0;

	/**
	 * The feature id for the '<em><b>Thead Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION__THEAD_NUMBER = 0;

	/**
	 * The feature id for the '<em><b>Optimization Graph Display</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION__OPTIMIZATION_GRAPH_DISPLAY = 1;

	/**
	 * The feature id for the '<em><b>Generate Summary</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION__GENERATE_SUMMARY = 2;

	/**
	 * The feature id for the '<em><b>Engine</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION__ENGINE = 3;

	/**
	 * The feature id for the '<em><b>Noise Function Loader</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION__NOISE_FUNCTION_LOADER = 4;

	/**
	 * The feature id for the '<em><b>Runtime Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION__RUNTIME_MODE = 5;

	/**
	 * The feature id for the '<em><b>Maximum Folder</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION__MAXIMUM_FOLDER = 6;

	/**
	 * The feature id for the '<em><b>Backend Simulator Sample Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION__BACKEND_SIMULATOR_SAMPLE_NUMBER = 7;

	/**
	 * The feature id for the '<em><b>Noise Function Output Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION__NOISE_FUNCTION_OUTPUT_MODE = 8;

	/**
	 * The feature id for the '<em><b>Operator Quantification Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION__OPERATOR_QUANTIFICATION_TYPE = 9;

	/**
	 * The feature id for the '<em><b>Operator Overflow Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION__OPERATOR_OVERFLOW_TYPE = 10;

	/**
	 * The feature id for the '<em><b>Optimization Simulator Sample Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_SAMPLE_NUMBER = 11;

	/**
	 * The feature id for the '<em><b>Optimization Simulator Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_FREQUENCY = 12;

	/**
	 * The feature id for the '<em><b>Optimization Simulator Superior Limit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_SUPERIOR_LIMIT = 13;

	/**
	 * The feature id for the '<em><b>Data Type Regneration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION__DATA_TYPE_REGNERATION = 14;

	/**
	 * The feature id for the '<em><b>Operator Characteristic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION__OPERATOR_CHARACTERISTIC = 15;

	/**
	 * The feature id for the '<em><b>Tag Folder</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION__TAG_FOLDER = 16;

	/**
	 * The feature id for the '<em><b>Correlation Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION__CORRELATION_MODE = 17;

	/**
	 * The number of structural features of the '<em>User Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION_FEATURE_COUNT = 18;

	/**
	 * The operation id for the '<em>Is Release Mode</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION___IS_RELEASE_MODE = 0;

	/**
	 * The operation id for the '<em>Is Debug Mode</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION___IS_DEBUG_MODE = 1;

	/**
	 * The operation id for the '<em>Is Trace Mode</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION___IS_TRACE_MODE = 2;

	/**
	 * The operation id for the '<em>Backend Simulation Is Activated</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION___BACKEND_SIMULATION_IS_ACTIVATED = 3;

	/**
	 * The operation id for the '<em>Optimization Simulation Is Activated</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION___OPTIMIZATION_SIMULATION_IS_ACTIVATED = 4;

	/**
	 * The number of operations of the '<em>User Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_CONFIGURATION_OPERATION_COUNT = 5;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.userconfiguration.ENGINE <em>ENGINE</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.ENGINE
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.impl.UserconfigurationPackageImpl#getENGINE()
	 * @generated
	 */
	int ENGINE = 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_LOADER <em>NOISE FUNCTION LOADER</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_LOADER
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.impl.UserconfigurationPackageImpl#getNOISE_FUNCTION_LOADER()
	 * @generated
	 */
	int NOISE_FUNCTION_LOADER = 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.userconfiguration.RUNTIME_MODE <em>RUNTIME MODE</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.RUNTIME_MODE
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.impl.UserconfigurationPackageImpl#getRUNTIME_MODE()
	 * @generated
	 */
	int RUNTIME_MODE = 3;


	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_OUTPUT_MODE <em>NOISE FUNCTION OUTPUT MODE</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_OUTPUT_MODE
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.impl.UserconfigurationPackageImpl#getNOISE_FUNCTION_OUTPUT_MODE()
	 * @generated
	 */
	int NOISE_FUNCTION_OUTPUT_MODE = 4;


	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.userconfiguration.DATA_TYPE_REGENERATION <em>DATA TYPE REGENERATION</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.DATA_TYPE_REGENERATION
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.impl.UserconfigurationPackageImpl#getDATA_TYPE_REGENERATION()
	 * @generated
	 */
	int DATA_TYPE_REGENERATION = 5;


	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration <em>User Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User Configuration</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration
	 * @generated
	 */
	EClass getUserConfiguration();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getTheadNumber <em>Thead Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Thead Number</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getTheadNumber()
	 * @see #getUserConfiguration()
	 * @generated
	 */
	EAttribute getUserConfiguration_TheadNumber();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#isOptimizationGraphDisplay <em>Optimization Graph Display</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Optimization Graph Display</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#isOptimizationGraphDisplay()
	 * @see #getUserConfiguration()
	 * @generated
	 */
	EAttribute getUserConfiguration_OptimizationGraphDisplay();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#isGenerateSummary <em>Generate Summary</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Generate Summary</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#isGenerateSummary()
	 * @see #getUserConfiguration()
	 * @generated
	 */
	EAttribute getUserConfiguration_GenerateSummary();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getEngine <em>Engine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Engine</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getEngine()
	 * @see #getUserConfiguration()
	 * @generated
	 */
	EAttribute getUserConfiguration_Engine();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getNoiseFunctionLoader <em>Noise Function Loader</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Noise Function Loader</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getNoiseFunctionLoader()
	 * @see #getUserConfiguration()
	 * @generated
	 */
	EAttribute getUserConfiguration_NoiseFunctionLoader();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getRuntimeMode <em>Runtime Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Runtime Mode</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getRuntimeMode()
	 * @see #getUserConfiguration()
	 * @generated
	 */
	EAttribute getUserConfiguration_RuntimeMode();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getMaximumFolder <em>Maximum Folder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Maximum Folder</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getMaximumFolder()
	 * @see #getUserConfiguration()
	 * @generated
	 */
	EAttribute getUserConfiguration_MaximumFolder();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getBackendSimulatorSampleNumber <em>Backend Simulator Sample Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Backend Simulator Sample Number</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getBackendSimulatorSampleNumber()
	 * @see #getUserConfiguration()
	 * @generated
	 */
	EAttribute getUserConfiguration_BackendSimulatorSampleNumber();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getNoiseFunctionOutputMode <em>Noise Function Output Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Noise Function Output Mode</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getNoiseFunctionOutputMode()
	 * @see #getUserConfiguration()
	 * @generated
	 */
	EAttribute getUserConfiguration_NoiseFunctionOutputMode();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOperatorQuantificationType <em>Operator Quantification Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator Quantification Type</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOperatorQuantificationType()
	 * @see #getUserConfiguration()
	 * @generated
	 */
	EAttribute getUserConfiguration_OperatorQuantificationType();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOperatorOverflowType <em>Operator Overflow Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator Overflow Type</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOperatorOverflowType()
	 * @see #getUserConfiguration()
	 * @generated
	 */
	EAttribute getUserConfiguration_OperatorOverflowType();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOptimizationSimulatorSampleNumber <em>Optimization Simulator Sample Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Optimization Simulator Sample Number</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOptimizationSimulatorSampleNumber()
	 * @see #getUserConfiguration()
	 * @generated
	 */
	EAttribute getUserConfiguration_OptimizationSimulatorSampleNumber();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOptimizationSimulatorFrequency <em>Optimization Simulator Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Optimization Simulator Frequency</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOptimizationSimulatorFrequency()
	 * @see #getUserConfiguration()
	 * @generated
	 */
	EAttribute getUserConfiguration_OptimizationSimulatorFrequency();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOptimizationSimulatorSuperiorLimit <em>Optimization Simulator Superior Limit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Optimization Simulator Superior Limit</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOptimizationSimulatorSuperiorLimit()
	 * @see #getUserConfiguration()
	 * @generated
	 */
	EAttribute getUserConfiguration_OptimizationSimulatorSuperiorLimit();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getDataTypeRegneration <em>Data Type Regneration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Data Type Regneration</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getDataTypeRegneration()
	 * @see #getUserConfiguration()
	 * @generated
	 */
	EAttribute getUserConfiguration_DataTypeRegneration();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOperatorCharacteristic <em>Operator Characteristic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator Characteristic</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOperatorCharacteristic()
	 * @see #getUserConfiguration()
	 * @generated
	 */
	EAttribute getUserConfiguration_OperatorCharacteristic();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#isTagFolder <em>Tag Folder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tag Folder</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#isTagFolder()
	 * @see #getUserConfiguration()
	 * @generated
	 */
	EAttribute getUserConfiguration_TagFolder();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#isCorrelationMode <em>Correlation Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Correlation Mode</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#isCorrelationMode()
	 * @see #getUserConfiguration()
	 * @generated
	 */
	EAttribute getUserConfiguration_CorrelationMode();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#isReleaseMode() <em>Is Release Mode</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Release Mode</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#isReleaseMode()
	 * @generated
	 */
	EOperation getUserConfiguration__IsReleaseMode();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#isDebugMode() <em>Is Debug Mode</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Debug Mode</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#isDebugMode()
	 * @generated
	 */
	EOperation getUserConfiguration__IsDebugMode();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#isTraceMode() <em>Is Trace Mode</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Trace Mode</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#isTraceMode()
	 * @generated
	 */
	EOperation getUserConfiguration__IsTraceMode();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#backendSimulationIsActivated() <em>Backend Simulation Is Activated</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Backend Simulation Is Activated</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#backendSimulationIsActivated()
	 * @generated
	 */
	EOperation getUserConfiguration__BackendSimulationIsActivated();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#optimizationSimulationIsActivated() <em>Optimization Simulation Is Activated</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Optimization Simulation Is Activated</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#optimizationSimulationIsActivated()
	 * @generated
	 */
	EOperation getUserConfiguration__OptimizationSimulationIsActivated();

	/**
	 * Returns the meta object for enum '{@link fr.irisa.cairn.idfix.model.userconfiguration.ENGINE <em>ENGINE</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>ENGINE</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.ENGINE
	 * @generated
	 */
	EEnum getENGINE();

	/**
	 * Returns the meta object for enum '{@link fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_LOADER <em>NOISE FUNCTION LOADER</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>NOISE FUNCTION LOADER</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_LOADER
	 * @generated
	 */
	EEnum getNOISE_FUNCTION_LOADER();

	/**
	 * Returns the meta object for enum '{@link fr.irisa.cairn.idfix.model.userconfiguration.RUNTIME_MODE <em>RUNTIME MODE</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>RUNTIME MODE</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.RUNTIME_MODE
	 * @generated
	 */
	EEnum getRUNTIME_MODE();

	/**
	 * Returns the meta object for enum '{@link fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_OUTPUT_MODE <em>NOISE FUNCTION OUTPUT MODE</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>NOISE FUNCTION OUTPUT MODE</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_OUTPUT_MODE
	 * @generated
	 */
	EEnum getNOISE_FUNCTION_OUTPUT_MODE();

	/**
	 * Returns the meta object for enum '{@link fr.irisa.cairn.idfix.model.userconfiguration.DATA_TYPE_REGENERATION <em>DATA TYPE REGENERATION</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>DATA TYPE REGENERATION</em>'.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.DATA_TYPE_REGENERATION
	 * @generated
	 */
	EEnum getDATA_TYPE_REGENERATION();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	UserconfigurationFactory getUserconfigurationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.userconfiguration.impl.UserConfigurationImpl <em>User Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.userconfiguration.impl.UserConfigurationImpl
		 * @see fr.irisa.cairn.idfix.model.userconfiguration.impl.UserconfigurationPackageImpl#getUserConfiguration()
		 * @generated
		 */
		EClass USER_CONFIGURATION = eINSTANCE.getUserConfiguration();

		/**
		 * The meta object literal for the '<em><b>Thead Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_CONFIGURATION__THEAD_NUMBER = eINSTANCE.getUserConfiguration_TheadNumber();

		/**
		 * The meta object literal for the '<em><b>Optimization Graph Display</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_CONFIGURATION__OPTIMIZATION_GRAPH_DISPLAY = eINSTANCE.getUserConfiguration_OptimizationGraphDisplay();

		/**
		 * The meta object literal for the '<em><b>Generate Summary</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_CONFIGURATION__GENERATE_SUMMARY = eINSTANCE.getUserConfiguration_GenerateSummary();

		/**
		 * The meta object literal for the '<em><b>Engine</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_CONFIGURATION__ENGINE = eINSTANCE.getUserConfiguration_Engine();

		/**
		 * The meta object literal for the '<em><b>Noise Function Loader</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_CONFIGURATION__NOISE_FUNCTION_LOADER = eINSTANCE.getUserConfiguration_NoiseFunctionLoader();

		/**
		 * The meta object literal for the '<em><b>Runtime Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_CONFIGURATION__RUNTIME_MODE = eINSTANCE.getUserConfiguration_RuntimeMode();

		/**
		 * The meta object literal for the '<em><b>Maximum Folder</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_CONFIGURATION__MAXIMUM_FOLDER = eINSTANCE.getUserConfiguration_MaximumFolder();

		/**
		 * The meta object literal for the '<em><b>Backend Simulator Sample Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_CONFIGURATION__BACKEND_SIMULATOR_SAMPLE_NUMBER = eINSTANCE.getUserConfiguration_BackendSimulatorSampleNumber();

		/**
		 * The meta object literal for the '<em><b>Noise Function Output Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_CONFIGURATION__NOISE_FUNCTION_OUTPUT_MODE = eINSTANCE.getUserConfiguration_NoiseFunctionOutputMode();

		/**
		 * The meta object literal for the '<em><b>Operator Quantification Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_CONFIGURATION__OPERATOR_QUANTIFICATION_TYPE = eINSTANCE.getUserConfiguration_OperatorQuantificationType();

		/**
		 * The meta object literal for the '<em><b>Operator Overflow Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_CONFIGURATION__OPERATOR_OVERFLOW_TYPE = eINSTANCE.getUserConfiguration_OperatorOverflowType();

		/**
		 * The meta object literal for the '<em><b>Optimization Simulator Sample Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_SAMPLE_NUMBER = eINSTANCE.getUserConfiguration_OptimizationSimulatorSampleNumber();

		/**
		 * The meta object literal for the '<em><b>Optimization Simulator Frequency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_FREQUENCY = eINSTANCE.getUserConfiguration_OptimizationSimulatorFrequency();

		/**
		 * The meta object literal for the '<em><b>Optimization Simulator Superior Limit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_SUPERIOR_LIMIT = eINSTANCE.getUserConfiguration_OptimizationSimulatorSuperiorLimit();

		/**
		 * The meta object literal for the '<em><b>Data Type Regneration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_CONFIGURATION__DATA_TYPE_REGNERATION = eINSTANCE.getUserConfiguration_DataTypeRegneration();

		/**
		 * The meta object literal for the '<em><b>Operator Characteristic</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_CONFIGURATION__OPERATOR_CHARACTERISTIC = eINSTANCE.getUserConfiguration_OperatorCharacteristic();

		/**
		 * The meta object literal for the '<em><b>Tag Folder</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_CONFIGURATION__TAG_FOLDER = eINSTANCE.getUserConfiguration_TagFolder();

		/**
		 * The meta object literal for the '<em><b>Correlation Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_CONFIGURATION__CORRELATION_MODE = eINSTANCE.getUserConfiguration_CorrelationMode();

		/**
		 * The meta object literal for the '<em><b>Is Release Mode</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation USER_CONFIGURATION___IS_RELEASE_MODE = eINSTANCE.getUserConfiguration__IsReleaseMode();

		/**
		 * The meta object literal for the '<em><b>Is Debug Mode</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation USER_CONFIGURATION___IS_DEBUG_MODE = eINSTANCE.getUserConfiguration__IsDebugMode();

		/**
		 * The meta object literal for the '<em><b>Is Trace Mode</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation USER_CONFIGURATION___IS_TRACE_MODE = eINSTANCE.getUserConfiguration__IsTraceMode();

		/**
		 * The meta object literal for the '<em><b>Backend Simulation Is Activated</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation USER_CONFIGURATION___BACKEND_SIMULATION_IS_ACTIVATED = eINSTANCE.getUserConfiguration__BackendSimulationIsActivated();

		/**
		 * The meta object literal for the '<em><b>Optimization Simulation Is Activated</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation USER_CONFIGURATION___OPTIMIZATION_SIMULATION_IS_ACTIVATED = eINSTANCE.getUserConfiguration__OptimizationSimulationIsActivated();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.userconfiguration.ENGINE <em>ENGINE</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.userconfiguration.ENGINE
		 * @see fr.irisa.cairn.idfix.model.userconfiguration.impl.UserconfigurationPackageImpl#getENGINE()
		 * @generated
		 */
		EEnum ENGINE = eINSTANCE.getENGINE();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_LOADER <em>NOISE FUNCTION LOADER</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_LOADER
		 * @see fr.irisa.cairn.idfix.model.userconfiguration.impl.UserconfigurationPackageImpl#getNOISE_FUNCTION_LOADER()
		 * @generated
		 */
		EEnum NOISE_FUNCTION_LOADER = eINSTANCE.getNOISE_FUNCTION_LOADER();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.userconfiguration.RUNTIME_MODE <em>RUNTIME MODE</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.userconfiguration.RUNTIME_MODE
		 * @see fr.irisa.cairn.idfix.model.userconfiguration.impl.UserconfigurationPackageImpl#getRUNTIME_MODE()
		 * @generated
		 */
		EEnum RUNTIME_MODE = eINSTANCE.getRUNTIME_MODE();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_OUTPUT_MODE <em>NOISE FUNCTION OUTPUT MODE</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_OUTPUT_MODE
		 * @see fr.irisa.cairn.idfix.model.userconfiguration.impl.UserconfigurationPackageImpl#getNOISE_FUNCTION_OUTPUT_MODE()
		 * @generated
		 */
		EEnum NOISE_FUNCTION_OUTPUT_MODE = eINSTANCE.getNOISE_FUNCTION_OUTPUT_MODE();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.userconfiguration.DATA_TYPE_REGENERATION <em>DATA TYPE REGENERATION</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.userconfiguration.DATA_TYPE_REGENERATION
		 * @see fr.irisa.cairn.idfix.model.userconfiguration.impl.UserconfigurationPackageImpl#getDATA_TYPE_REGENERATION()
		 * @generated
		 */
		EEnum DATA_TYPE_REGENERATION = eINSTANCE.getDATA_TYPE_REGENERATION();

	}

} //UserconfigurationPackage
