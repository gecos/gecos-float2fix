/**
 */
package fr.irisa.cairn.idfix.model.fixedpointspecification.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge;
import fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Data;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Operation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CDFG Edge</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.CDFGEdgeImpl#getPredecessor <em>Predecessor</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.CDFGEdgeImpl#getSuccessor <em>Successor</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.CDFGEdgeImpl#getPredIndex <em>Pred Index</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CDFGEdgeImpl extends MinimalEObjectImpl.Container implements CDFGEdge {
	/**
	 * The cached value of the '{@link #getPredecessor() <em>Predecessor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPredecessor()
	 * @generated
	 * @ordered
	 */
	protected CDFGNode predecessor;

	/**
	 * The cached value of the '{@link #getSuccessor() <em>Successor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuccessor()
	 * @generated
	 * @ordered
	 */
	protected CDFGNode successor;

	/**
	 * The default value of the '{@link #getPredIndex() <em>Pred Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPredIndex()
	 * @generated
	 * @ordered
	 */
	protected static final int PRED_INDEX_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getPredIndex() <em>Pred Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPredIndex()
	 * @generated
	 * @ordered
	 */
	protected int predIndex = PRED_INDEX_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CDFGEdgeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FixedpointspecificationPackage.Literals.CDFG_EDGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CDFGNode getPredecessor() {
		if (predecessor != null && predecessor.eIsProxy()) {
			InternalEObject oldPredecessor = (InternalEObject)predecessor;
			predecessor = (CDFGNode)eResolveProxy(oldPredecessor);
			if (predecessor != oldPredecessor) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FixedpointspecificationPackage.CDFG_EDGE__PREDECESSOR, oldPredecessor, predecessor));
			}
		}
		return predecessor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CDFGNode basicGetPredecessor() {
		return predecessor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPredecessor(CDFGNode newPredecessor, NotificationChain msgs) {
		CDFGNode oldPredecessor = predecessor;
		predecessor = newPredecessor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FixedpointspecificationPackage.CDFG_EDGE__PREDECESSOR, oldPredecessor, newPredecessor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPredecessor(CDFGNode newPredecessor) {
		if (newPredecessor != predecessor) {
			NotificationChain msgs = null;
			if (predecessor != null)
				msgs = ((InternalEObject)predecessor).eInverseRemove(this, FixedpointspecificationPackage.CDFG_NODE__SUCCESSORS, CDFGNode.class, msgs);
			if (newPredecessor != null)
				msgs = ((InternalEObject)newPredecessor).eInverseAdd(this, FixedpointspecificationPackage.CDFG_NODE__SUCCESSORS, CDFGNode.class, msgs);
			msgs = basicSetPredecessor(newPredecessor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixedpointspecificationPackage.CDFG_EDGE__PREDECESSOR, newPredecessor, newPredecessor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CDFGNode getSuccessor() {
		if (successor != null && successor.eIsProxy()) {
			InternalEObject oldSuccessor = (InternalEObject)successor;
			successor = (CDFGNode)eResolveProxy(oldSuccessor);
			if (successor != oldSuccessor) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FixedpointspecificationPackage.CDFG_EDGE__SUCCESSOR, oldSuccessor, successor));
			}
		}
		return successor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CDFGNode basicGetSuccessor() {
		return successor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSuccessor(CDFGNode newSuccessor, NotificationChain msgs) {
		CDFGNode oldSuccessor = successor;
		successor = newSuccessor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FixedpointspecificationPackage.CDFG_EDGE__SUCCESSOR, oldSuccessor, newSuccessor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuccessor(CDFGNode newSuccessor) {
		if (newSuccessor != successor) {
			NotificationChain msgs = null;
			if (successor != null)
				msgs = ((InternalEObject)successor).eInverseRemove(this, FixedpointspecificationPackage.CDFG_NODE__PREDECESSORS, CDFGNode.class, msgs);
			if (newSuccessor != null)
				msgs = ((InternalEObject)newSuccessor).eInverseAdd(this, FixedpointspecificationPackage.CDFG_NODE__PREDECESSORS, CDFGNode.class, msgs);
			msgs = basicSetSuccessor(newSuccessor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixedpointspecificationPackage.CDFG_EDGE__SUCCESSOR, newSuccessor, newSuccessor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPredIndex() {
		return predIndex;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPredIndex(int newPredIndex) {
		int oldPredIndex = predIndex;
		predIndex = newPredIndex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixedpointspecificationPackage.CDFG_EDGE__PRED_INDEX, oldPredIndex, predIndex));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public FixedPointInformation getPredecessorInfo() {
		if(predecessor == null)
			throw new RuntimeException("ERROR: predecessor not found. Invalid edge!");
		return predecessor.getOutputInfo();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public FixedPointInformation getSuccessorInfo() {
		if(successor == null)
			throw new RuntimeException("ERROR: predecessor not found. Invalid edge!");
		if(successor instanceof Data)
			return ((Data) successor).getFixedInformation();
		else if(successor instanceof Operation)
			return ((Operation) successor).getOperands().get(predIndex); //XXX???
		throw new RuntimeException("ERROR: unsupported Node type");	
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FixedpointspecificationPackage.CDFG_EDGE__PREDECESSOR:
				if (predecessor != null)
					msgs = ((InternalEObject)predecessor).eInverseRemove(this, FixedpointspecificationPackage.CDFG_NODE__SUCCESSORS, CDFGNode.class, msgs);
				return basicSetPredecessor((CDFGNode)otherEnd, msgs);
			case FixedpointspecificationPackage.CDFG_EDGE__SUCCESSOR:
				if (successor != null)
					msgs = ((InternalEObject)successor).eInverseRemove(this, FixedpointspecificationPackage.CDFG_NODE__PREDECESSORS, CDFGNode.class, msgs);
				return basicSetSuccessor((CDFGNode)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FixedpointspecificationPackage.CDFG_EDGE__PREDECESSOR:
				return basicSetPredecessor(null, msgs);
			case FixedpointspecificationPackage.CDFG_EDGE__SUCCESSOR:
				return basicSetSuccessor(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FixedpointspecificationPackage.CDFG_EDGE__PREDECESSOR:
				if (resolve) return getPredecessor();
				return basicGetPredecessor();
			case FixedpointspecificationPackage.CDFG_EDGE__SUCCESSOR:
				if (resolve) return getSuccessor();
				return basicGetSuccessor();
			case FixedpointspecificationPackage.CDFG_EDGE__PRED_INDEX:
				return getPredIndex();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FixedpointspecificationPackage.CDFG_EDGE__PREDECESSOR:
				setPredecessor((CDFGNode)newValue);
				return;
			case FixedpointspecificationPackage.CDFG_EDGE__SUCCESSOR:
				setSuccessor((CDFGNode)newValue);
				return;
			case FixedpointspecificationPackage.CDFG_EDGE__PRED_INDEX:
				setPredIndex((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FixedpointspecificationPackage.CDFG_EDGE__PREDECESSOR:
				setPredecessor((CDFGNode)null);
				return;
			case FixedpointspecificationPackage.CDFG_EDGE__SUCCESSOR:
				setSuccessor((CDFGNode)null);
				return;
			case FixedpointspecificationPackage.CDFG_EDGE__PRED_INDEX:
				setPredIndex(PRED_INDEX_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FixedpointspecificationPackage.CDFG_EDGE__PREDECESSOR:
				return predecessor != null;
			case FixedpointspecificationPackage.CDFG_EDGE__SUCCESSOR:
				return successor != null;
			case FixedpointspecificationPackage.CDFG_EDGE__PRED_INDEX:
				return predIndex != PRED_INDEX_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case FixedpointspecificationPackage.CDFG_EDGE___GET_PREDECESSOR_INFO:
				return getPredecessorInfo();
			case FixedpointspecificationPackage.CDFG_EDGE___GET_SUCCESSOR_INFO:
				return getSuccessorInfo();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
//		if (eIsProxy()) return super.toString();
//
//		StringBuffer result = new StringBuffer(super.toString());
//		result.append(" (predIndex: ");
//		result.append(predIndex);
//		result.append(')');
//		return result.toString();
		
		return predecessor + "-(" + predIndex + ")->" + successor;
	}

} //CDFGEdgeImpl
