/**
 */
package fr.irisa.cairn.idfix.model.operatorlibrary.impl;

import fr.irisa.cairn.idfix.model.operatorlibrary.OPERATOR_KIND;
import java.util.Collection;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import fr.irisa.cairn.idfix.model.operatorlibrary.Operator;
import fr.irisa.cairn.idfix.model.operatorlibrary.OperatorLibrary;
import fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryPackage;
import java.lang.reflect.InvocationTargetException;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operator Library</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorLibraryImpl#getOperators <em>Operators</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperatorLibraryImpl extends MinimalEObjectImpl.Container implements OperatorLibrary {
	/**
	 * The cached value of the '{@link #getOperators() <em>Operators</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperators()
	 * @generated
	 * @ordered
	 */
	protected EList<Operator> operators;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperatorLibraryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OperatorlibraryPackage.Literals.OPERATOR_LIBRARY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Operator> getOperators() {
		if (operators == null) {
			operators = new EObjectResolvingEList<Operator>(Operator.class, this, OperatorlibraryPackage.OPERATOR_LIBRARY__OPERATORS);
		}
		return operators;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Return the operator corresponding with the operator kind, null otherwise
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Operator getOperator(OPERATOR_KIND kind) {
		for(Operator op : this.getOperators()){
			if(op.getKind() == kind)
				return op;
		}
		
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OperatorlibraryPackage.OPERATOR_LIBRARY__OPERATORS:
				return getOperators();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OperatorlibraryPackage.OPERATOR_LIBRARY__OPERATORS:
				getOperators().clear();
				getOperators().addAll((Collection<? extends Operator>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OperatorlibraryPackage.OPERATOR_LIBRARY__OPERATORS:
				getOperators().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OperatorlibraryPackage.OPERATOR_LIBRARY__OPERATORS:
				return operators != null && !operators.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case OperatorlibraryPackage.OPERATOR_LIBRARY___GET_OPERATOR__OPERATOR_KIND:
				return getOperator((OPERATOR_KIND)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer("OperatorLibrary -> ");
		result.append("\n");
		for(Operator c : this.getOperators()){
			result.append("   " + c.toString());
			result.append("\n");
		}
		return result.toString();
	}
	
} //OperatorLibraryImpl
