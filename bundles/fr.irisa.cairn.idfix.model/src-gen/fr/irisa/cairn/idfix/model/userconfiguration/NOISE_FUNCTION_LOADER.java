/**
 */
package fr.irisa.cairn.idfix.model.userconfiguration;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>NOISE FUNCTION LOADER</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage#getNOISE_FUNCTION_LOADER()
 * @model
 * @generated
 */
public enum NOISE_FUNCTION_LOADER implements Enumerator {
	/**
	 * The '<em><b>JNA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #JNA_VALUE
	 * @generated
	 * @ordered
	 */
	JNA(0, "JNA", "JNA"), /**
	 * The '<em><b>JNI</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #JNI_VALUE
	 * @generated
	 * @ordered
	 */
	JNI(1, "JNI", "JNI");

	/**
	 * The '<em><b>JNA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>JNA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #JNA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int JNA_VALUE = 0;

	/**
	 * The '<em><b>JNI</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>JNI</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #JNI
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int JNI_VALUE = 1;

	/**
	 * An array of all the '<em><b>NOISE FUNCTION LOADER</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final NOISE_FUNCTION_LOADER[] VALUES_ARRAY =
		new NOISE_FUNCTION_LOADER[] {
			JNA,
			JNI,
		};

	/**
	 * A public read-only list of all the '<em><b>NOISE FUNCTION LOADER</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<NOISE_FUNCTION_LOADER> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>NOISE FUNCTION LOADER</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static NOISE_FUNCTION_LOADER get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			NOISE_FUNCTION_LOADER result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>NOISE FUNCTION LOADER</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static NOISE_FUNCTION_LOADER getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			NOISE_FUNCTION_LOADER result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>NOISE FUNCTION LOADER</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static NOISE_FUNCTION_LOADER get(int value) {
		switch (value) {
			case JNA_VALUE: return JNA;
			case JNI_VALUE: return JNI;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private NOISE_FUNCTION_LOADER(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //NOISE_FUNCTION_LOADER
