/**
 */
package fr.irisa.cairn.idfix.model.solutionspace.impl;

import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind;
import fr.irisa.cairn.idfix.model.solutionspace.LibraryElement;
import fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace;
import fr.irisa.cairn.idfix.model.solutionspace.SolutionspaceFactory;
import fr.irisa.cairn.idfix.model.solutionspace.SolutionspacePackage;
import fr.irisa.cairn.idfix.model.solutionspace.Triplet;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SolutionspaceFactoryImpl extends EFactoryImpl implements SolutionspaceFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SolutionspaceFactory init() {
		try {
			SolutionspaceFactory theSolutionspaceFactory = (SolutionspaceFactory)EPackage.Registry.INSTANCE.getEFactory(SolutionspacePackage.eNS_URI);
			if (theSolutionspaceFactory != null) {
				return theSolutionspaceFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SolutionspaceFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SolutionspaceFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case SolutionspacePackage.TRIPLET: return createTriplet();
			case SolutionspacePackage.SOLUTION_SPACE: return createSolutionSpace();
			case SolutionspacePackage.OPERATOR_TO_LIST_ELEMENT: return (EObject)createOperatorToListElement();
			case SolutionspacePackage.LIBRARY_ELEMENT: return createLibraryElement();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Triplet createTriplet() {
		TripletImpl triplet = new TripletImpl();
		return triplet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SolutionSpace createSolutionSpace() {
		SolutionSpaceImpl solutionSpace = new SolutionSpaceImpl();
		return solutionSpace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<OperatorKind, EList<LibraryElement>> createOperatorToListElement() {
		OperatorToListElementImpl operatorToListElement = new OperatorToListElementImpl();
		return operatorToListElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LibraryElement createLibraryElement() {
		LibraryElementImpl libraryElement = new LibraryElementImpl();
		return libraryElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SolutionspacePackage getSolutionspacePackage() {
		return (SolutionspacePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SolutionspacePackage getPackage() {
		return SolutionspacePackage.eINSTANCE;
	}

} //SolutionspaceFactoryImpl
