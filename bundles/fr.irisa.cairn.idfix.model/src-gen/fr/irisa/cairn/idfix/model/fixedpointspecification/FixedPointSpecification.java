/**
 */
package fr.irisa.cairn.idfix.model.fixedpointspecification;

import gecos.core.Symbol;

import gecos.instrs.Instruction;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fixed Point Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#getEdges <em>Edges</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#getDatas <em>Datas</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#getOperators <em>Operators</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getFixedPointSpecification()
 * @model
 * @generated
 */
public interface FixedPointSpecification extends EObject {
	/**
	 * Returns the value of the '<em><b>Edges</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edges</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edges</em>' containment reference list.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getFixedPointSpecification_Edges()
	 * @model containment="true"
	 * @generated
	 */
	EList<CDFGEdge> getEdges();

	/**
	 * Returns the value of the '<em><b>Datas</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.idfix.model.fixedpointspecification.Data}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datas</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datas</em>' containment reference list.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getFixedPointSpecification_Datas()
	 * @model containment="true"
	 * @generated
	 */
	EList<Data> getDatas();

	/**
	 * Returns the value of the '<em><b>Operators</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operators</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operators</em>' containment reference list.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getFixedPointSpecification_Operators()
	 * @model containment="true"
	 * @generated
	 */
	EList<Operation> getOperations();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void updateDynamicInformation(String dynamicInformation);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Operation findOperatorFromEvalCDFG(int eval_cdfg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Operation findOperatorFromConvCDFG(int conv_cdfg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Operation findOperatorFromInstruction(Instruction instruction);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Data findDataFromCDFG(int cdfg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Data findDataFromSymbol(Symbol symbol);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void updateDataFixedInformation();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void checkValidity();

} // FixedPointSpecification
