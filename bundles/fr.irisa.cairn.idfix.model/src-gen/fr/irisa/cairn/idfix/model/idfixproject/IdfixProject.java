/**
 */
package fr.irisa.cairn.idfix.model.idfixproject;

import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification;
import fr.irisa.cairn.idfix.model.informationandtiming.Informations;
import fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration;
import gecos.gecosproject.GecosProject;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Idfix Project</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getGecosProject <em>Gecos Project</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getGecosProjectAnnotedAndNotUnrolled <em>Gecos Project Annoted And Not Unrolled</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getFixedPointSpecification <em>Fixed Point Specification</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getOutputFolderPath <em>Output Folder Path</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getProcessInformations <em>Process Informations</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getUserConfiguration <em>User Configuration</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixprojectPackage#getIdfixProject()
 * @model
 * @generated
 */
public interface IdfixProject extends EObject {
	/**
	 * Returns the value of the '<em><b>Gecos Project</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Gecos Project</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Gecos Project</em>' reference.
	 * @see #setGecosProject(GecosProject)
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixprojectPackage#getIdfixProject_GecosProject()
	 * @model
	 * @generated
	 */
	GecosProject getGecosProject();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getGecosProject <em>Gecos Project</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Gecos Project</em>' reference.
	 * @see #getGecosProject()
	 * @generated
	 */
	void setGecosProject(GecosProject value);

	/**
	 * Returns the value of the '<em><b>Gecos Project Annoted And Not Unrolled</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Gecos Project Annoted And Not Unrolled</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Gecos Project Annoted And Not Unrolled</em>' reference.
	 * @see #setGecosProjectAnnotedAndNotUnrolled(GecosProject)
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixprojectPackage#getIdfixProject_GecosProjectAnnotedAndNotUnrolled()
	 * @model
	 * @generated
	 */
	GecosProject getGecosProjectAnnotedAndNotUnrolled();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getGecosProjectAnnotedAndNotUnrolled <em>Gecos Project Annoted And Not Unrolled</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Gecos Project Annoted And Not Unrolled</em>' reference.
	 * @see #getGecosProjectAnnotedAndNotUnrolled()
	 * @generated
	 */
	void setGecosProjectAnnotedAndNotUnrolled(GecosProject value);

	/**
	 * Returns the value of the '<em><b>Fixed Point Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fixed Point Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fixed Point Specification</em>' containment reference.
	 * @see #setFixedPointSpecification(FixedPointSpecification)
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixprojectPackage#getIdfixProject_FixedPointSpecification()
	 * @model containment="true"
	 * @generated
	 */
	FixedPointSpecification getFixedPointSpecification();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getFixedPointSpecification <em>Fixed Point Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fixed Point Specification</em>' containment reference.
	 * @see #getFixedPointSpecification()
	 * @generated
	 */
	void setFixedPointSpecification(FixedPointSpecification value);

	/**
	 * Returns the value of the '<em><b>Output Folder Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Folder Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Folder Path</em>' attribute.
	 * @see #setOutputFolderPath(String)
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixprojectPackage#getIdfixProject_OutputFolderPath()
	 * @model
	 * @generated
	 */
	String getOutputFolderPath();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getOutputFolderPath <em>Output Folder Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Folder Path</em>' attribute.
	 * @see #getOutputFolderPath()
	 * @generated
	 */
	void setOutputFolderPath(String value);

	/**
	 * Returns the value of the '<em><b>Debug</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Debug</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Debug</em>' attribute.
	 * @see #setDebug(boolean)
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixprojectPackage#getIdfixProject_Debug()
	 * @model
	 * @generated
	 */
//	boolean isDebug();

	/**
	 * Returns the value of the '<em><b>Process Informations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Process Informations</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Informations</em>' containment reference.
	 * @see #setProcessInformations(Informations)
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixprojectPackage#getIdfixProject_ProcessInformations()
	 * @model containment="true"
	 * @generated
	 */
	Informations getProcessInformations();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getProcessInformations <em>Process Informations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Process Informations</em>' containment reference.
	 * @see #getProcessInformations()
	 * @generated
	 */
	void setProcessInformations(Informations value);

	/**
	 * Returns the value of the '<em><b>User Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>User Configuration</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User Configuration</em>' reference.
	 * @see #setUserConfiguration(UserConfiguration)
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixprojectPackage#getIdfixProject_UserConfiguration()
	 * @model
	 * @generated
	 */
	UserConfiguration getUserConfiguration();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.idfixproject.IdfixProject#getUserConfiguration <em>User Configuration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User Configuration</em>' reference.
	 * @see #getUserConfiguration()
	 * @generated
	 */
	void setUserConfiguration(UserConfiguration value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getIDFixEvalOutputFolderPath();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getIDFixConvOutputFolderPath();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getSFGFilePath();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getSimulatedValuesFilePath();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getDynamicFilePath();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getNoiseLibraryFilePath();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void generateLogInformations();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void finalize();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void plotSaveInformations();

} // IdfixProject
