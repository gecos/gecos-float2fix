/**
 */
package fr.irisa.cairn.idfix.model.solutionspace;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.idfix.model.fixedpointspecification.Data;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Operation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Solution Space</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#getOperators <em>Operators</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#getLibrary <em>Library</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#getDatas <em>Datas</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionspacePackage#getSolutionSpace()
 * @model
 * @generated
 */
public interface SolutionSpace extends EObject {
	/**
	 * Returns the value of the '<em><b>Operators</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operators</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operators</em>' reference list.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionspacePackage#getSolutionSpace_Operators()
	 * @model
	 * @generated
	 */
	EList<Operation> getOperators();

	/**
	 * Returns the value of the '<em><b>Library</b></em>' map.
	 * The key is of type {@link fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind},
	 * and the value is of type list of {@link fr.irisa.cairn.idfix.model.solutionspace.LibraryElement},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Library</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Library</em>' map.
	 * @see #isSetLibrary()
	 * @see #unsetLibrary()
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionspacePackage#getSolutionSpace_Library()
	 * @model mapType="fr.irisa.cairn.idfix.model.solutionspace.OperatorToListElement<fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind, fr.irisa.cairn.idfix.model.solutionspace.LibraryElement>"
	 * @generated
	 */
	EMap<OperatorKind, EList<LibraryElement>> getLibrary();

	/**
	 * Unsets the value of the '{@link fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#getLibrary <em>Library</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLibrary()
	 * @see #getLibrary()
	 * @generated
	 */
	void unsetLibrary();

	/**
	 * Returns whether the value of the '{@link fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#getLibrary <em>Library</em>}' map is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Library</em>' map is set.
	 * @see #unsetLibrary()
	 * @see #getLibrary()
	 * @generated
	 */
	boolean isSetLibrary();

	/**
	 * Returns the value of the '<em><b>Datas</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.cairn.idfix.model.fixedpointspecification.Data}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datas</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datas</em>' reference list.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionspacePackage#getSolutionSpace_Datas()
	 * @model
	 * @generated
	 */
	EList<Data> getDatas();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void setAllOperatorToMax();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void setAllOperatorToMin();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void setOperatorTo(Operation operator, int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	int getMaxIndexOf(Operation operator);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	int getCurrentIndexOf(Operation operator);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void indexPlusOneTo(Operation operator);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void indexMinusOneTo(Operation operator);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void saveCurrentSolutionSpace();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void useSolutionSpaceSaved();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	int getIndexMaxOf(OperatorKind operatorKind);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void setOperatorToMax(Operation op);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void setAllOperatorsTo(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isMaxIndexOf(Operation operator);

} // SolutionSpace
