/**
 */
package fr.irisa.cairn.idfix.model.userconfiguration;

import fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE;
import fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getTheadNumber <em>Thead Number</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#isOptimizationGraphDisplay <em>Optimization Graph Display</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#isGenerateSummary <em>Generate Summary</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getEngine <em>Engine</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getNoiseFunctionLoader <em>Noise Function Loader</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getRuntimeMode <em>Runtime Mode</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getMaximumFolder <em>Maximum Folder</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getBackendSimulatorSampleNumber <em>Backend Simulator Sample Number</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getNoiseFunctionOutputMode <em>Noise Function Output Mode</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOperatorQuantificationType <em>Operator Quantification Type</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOperatorOverflowType <em>Operator Overflow Type</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOptimizationSimulatorSampleNumber <em>Optimization Simulator Sample Number</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOptimizationSimulatorFrequency <em>Optimization Simulator Frequency</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOptimizationSimulatorSuperiorLimit <em>Optimization Simulator Superior Limit</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getDataTypeRegneration <em>Data Type Regneration</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOperatorCharacteristic <em>Operator Characteristic</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#isTagFolder <em>Tag Folder</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#isCorrelationMode <em>Correlation Mode</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage#getUserConfiguration()
 * @model
 * @generated
 */
public interface UserConfiguration extends EObject {
	/**
	 * Returns the value of the '<em><b>Thead Number</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Thead Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Thead Number</em>' attribute.
	 * @see #setTheadNumber(int)
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage#getUserConfiguration_TheadNumber()
	 * @model default="1"
	 * @generated
	 */
	int getTheadNumber();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getTheadNumber <em>Thead Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Thead Number</em>' attribute.
	 * @see #getTheadNumber()
	 * @generated
	 */
	void setTheadNumber(int value);

	/**
	 * Returns the value of the '<em><b>Optimization Graph Display</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Optimization Graph Display</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optimization Graph Display</em>' attribute.
	 * @see #setOptimizationGraphDisplay(boolean)
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage#getUserConfiguration_OptimizationGraphDisplay()
	 * @model default="false"
	 * @generated
	 */
	boolean isOptimizationGraphDisplay();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#isOptimizationGraphDisplay <em>Optimization Graph Display</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Optimization Graph Display</em>' attribute.
	 * @see #isOptimizationGraphDisplay()
	 * @generated
	 */
	void setOptimizationGraphDisplay(boolean value);

	/**
	 * Returns the value of the '<em><b>Generate Summary</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generate Summary</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generate Summary</em>' attribute.
	 * @see #setGenerateSummary(boolean)
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage#getUserConfiguration_GenerateSummary()
	 * @model default="false"
	 * @generated
	 */
	boolean isGenerateSummary();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#isGenerateSummary <em>Generate Summary</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generate Summary</em>' attribute.
	 * @see #isGenerateSummary()
	 * @generated
	 */
	void setGenerateSummary(boolean value);

	/**
	 * Returns the value of the '<em><b>Engine</b></em>' attribute.
	 * The default value is <code>"MATLAB"</code>.
	 * The literals are from the enumeration {@link fr.irisa.cairn.idfix.model.userconfiguration.ENGINE}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Engine</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Engine</em>' attribute.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.ENGINE
	 * @see #setEngine(ENGINE)
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage#getUserConfiguration_Engine()
	 * @model default="MATLAB"
	 * @generated
	 */
	ENGINE getEngine();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getEngine <em>Engine</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Engine</em>' attribute.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.ENGINE
	 * @see #getEngine()
	 * @generated
	 */
	void setEngine(ENGINE value);

	/**
	 * Returns the value of the '<em><b>Noise Function Loader</b></em>' attribute.
	 * The default value is <code>"JNA"</code>.
	 * The literals are from the enumeration {@link fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_LOADER}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Noise Function Loader</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Noise Function Loader</em>' attribute.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_LOADER
	 * @see #setNoiseFunctionLoader(NOISE_FUNCTION_LOADER)
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage#getUserConfiguration_NoiseFunctionLoader()
	 * @model default="JNA"
	 * @generated
	 */
	NOISE_FUNCTION_LOADER getNoiseFunctionLoader();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getNoiseFunctionLoader <em>Noise Function Loader</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Noise Function Loader</em>' attribute.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_LOADER
	 * @see #getNoiseFunctionLoader()
	 * @generated
	 */
	void setNoiseFunctionLoader(NOISE_FUNCTION_LOADER value);

	/**
	 * Returns the value of the '<em><b>Runtime Mode</b></em>' attribute.
	 * The default value is <code>"RELEASE"</code>.
	 * The literals are from the enumeration {@link fr.irisa.cairn.idfix.model.userconfiguration.RUNTIME_MODE}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Runtime Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runtime Mode</em>' attribute.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.RUNTIME_MODE
	 * @see #setRuntimeMode(RUNTIME_MODE)
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage#getUserConfiguration_RuntimeMode()
	 * @model default="RELEASE"
	 * @generated
	 */
	RUNTIME_MODE getRuntimeMode();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getRuntimeMode <em>Runtime Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Runtime Mode</em>' attribute.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.RUNTIME_MODE
	 * @see #getRuntimeMode()
	 * @generated
	 */
	void setRuntimeMode(RUNTIME_MODE value);

	/**
	 * Returns the value of the '<em><b>Maximum Folder</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Maximum Folder</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Maximum Folder</em>' attribute.
	 * @see #setMaximumFolder(int)
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage#getUserConfiguration_MaximumFolder()
	 * @model default="0"
	 * @generated
	 */
	int getMaximumFolder();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getMaximumFolder <em>Maximum Folder</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Maximum Folder</em>' attribute.
	 * @see #getMaximumFolder()
	 * @generated
	 */
	void setMaximumFolder(int value);

	/**
	 * Returns the value of the '<em><b>Backend Simulator Sample Number</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Backend Simulator Sample Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Backend Simulator Sample Number</em>' attribute.
	 * @see #setBackendSimulatorSampleNumber(int)
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage#getUserConfiguration_BackendSimulatorSampleNumber()
	 * @model default="0"
	 * @generated
	 */
	int getBackendSimulatorSampleNumber();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getBackendSimulatorSampleNumber <em>Backend Simulator Sample Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Backend Simulator Sample Number</em>' attribute.
	 * @see #getBackendSimulatorSampleNumber()
	 * @generated
	 */
	void setBackendSimulatorSampleNumber(int value);

	/**
	 * Returns the value of the '<em><b>Noise Function Output Mode</b></em>' attribute.
	 * The default value is <code>"WORST"</code>.
	 * The literals are from the enumeration {@link fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_OUTPUT_MODE}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Noise Function Output Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Noise Function Output Mode</em>' attribute.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_OUTPUT_MODE
	 * @see #setNoiseFunctionOutputMode(NOISE_FUNCTION_OUTPUT_MODE)
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage#getUserConfiguration_NoiseFunctionOutputMode()
	 * @model default="WORST"
	 * @generated
	 */
	NOISE_FUNCTION_OUTPUT_MODE getNoiseFunctionOutputMode();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getNoiseFunctionOutputMode <em>Noise Function Output Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Noise Function Output Mode</em>' attribute.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_OUTPUT_MODE
	 * @see #getNoiseFunctionOutputMode()
	 * @generated
	 */
	void setNoiseFunctionOutputMode(NOISE_FUNCTION_OUTPUT_MODE value);

	/**
	 * Returns the value of the '<em><b>Operator Quantification Type</b></em>' attribute.
	 * The default value is <code>"TRUNCATION"</code>.
	 * The literals are from the enumeration {@link fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator Quantification Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator Quantification Type</em>' attribute.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE
	 * @see #setOperatorQuantificationType(QUANTIFICATION_TYPE)
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage#getUserConfiguration_OperatorQuantificationType()
	 * @model default="TRUNCATION"
	 * @generated
	 */
	QUANTIFICATION_TYPE getOperatorQuantificationType();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOperatorQuantificationType <em>Operator Quantification Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator Quantification Type</em>' attribute.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE
	 * @see #getOperatorQuantificationType()
	 * @generated
	 */
	void setOperatorQuantificationType(QUANTIFICATION_TYPE value);

	/**
	 * Returns the value of the '<em><b>Operator Overflow Type</b></em>' attribute.
	 * The default value is <code>"SATURATION"</code>.
	 * The literals are from the enumeration {@link fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator Overflow Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator Overflow Type</em>' attribute.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE
	 * @see #setOperatorOverflowType(OVERFLOW_TYPE)
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage#getUserConfiguration_OperatorOverflowType()
	 * @model default="SATURATION"
	 * @generated
	 */
	OVERFLOW_TYPE getOperatorOverflowType();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOperatorOverflowType <em>Operator Overflow Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator Overflow Type</em>' attribute.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE
	 * @see #getOperatorOverflowType()
	 * @generated
	 */
	void setOperatorOverflowType(OVERFLOW_TYPE value);

	/**
	 * Returns the value of the '<em><b>Optimization Simulator Sample Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Optimization Simulator Sample Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optimization Simulator Sample Number</em>' attribute.
	 * @see #setOptimizationSimulatorSampleNumber(int)
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage#getUserConfiguration_OptimizationSimulatorSampleNumber()
	 * @model
	 * @generated
	 */
	int getOptimizationSimulatorSampleNumber();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOptimizationSimulatorSampleNumber <em>Optimization Simulator Sample Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Optimization Simulator Sample Number</em>' attribute.
	 * @see #getOptimizationSimulatorSampleNumber()
	 * @generated
	 */
	void setOptimizationSimulatorSampleNumber(int value);

	/**
	 * Returns the value of the '<em><b>Optimization Simulator Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Optimization Simulator Frequency</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optimization Simulator Frequency</em>' attribute.
	 * @see #setOptimizationSimulatorFrequency(int)
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage#getUserConfiguration_OptimizationSimulatorFrequency()
	 * @model
	 * @generated
	 */
	int getOptimizationSimulatorFrequency();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOptimizationSimulatorFrequency <em>Optimization Simulator Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Optimization Simulator Frequency</em>' attribute.
	 * @see #getOptimizationSimulatorFrequency()
	 * @generated
	 */
	void setOptimizationSimulatorFrequency(int value);

	/**
	 * Returns the value of the '<em><b>Optimization Simulator Superior Limit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Optimization Simulator Superior Limit</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optimization Simulator Superior Limit</em>' attribute.
	 * @see #setOptimizationSimulatorSuperiorLimit(float)
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage#getUserConfiguration_OptimizationSimulatorSuperiorLimit()
	 * @model
	 * @generated
	 */
	float getOptimizationSimulatorSuperiorLimit();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOptimizationSimulatorSuperiorLimit <em>Optimization Simulator Superior Limit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Optimization Simulator Superior Limit</em>' attribute.
	 * @see #getOptimizationSimulatorSuperiorLimit()
	 * @generated
	 */
	void setOptimizationSimulatorSuperiorLimit(float value);

	/**
	 * Returns the value of the '<em><b>Data Type Regneration</b></em>' attribute.
	 * The default value is <code>"SC_FIXED"</code>.
	 * The literals are from the enumeration {@link fr.irisa.cairn.idfix.model.userconfiguration.DATA_TYPE_REGENERATION}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Type Regneration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Type Regneration</em>' attribute.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.DATA_TYPE_REGENERATION
	 * @see #setDataTypeRegneration(DATA_TYPE_REGENERATION)
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage#getUserConfiguration_DataTypeRegneration()
	 * @model default="SC_FIXED"
	 * @generated
	 */
	DATA_TYPE_REGENERATION getDataTypeRegneration();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getDataTypeRegneration <em>Data Type Regneration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Type Regneration</em>' attribute.
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.DATA_TYPE_REGENERATION
	 * @see #getDataTypeRegneration()
	 * @generated
	 */
	void setDataTypeRegneration(DATA_TYPE_REGENERATION value);

	/**
	 * Returns the value of the '<em><b>Operator Characteristic</b></em>' attribute.
	 * The default value is <code>"energy"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator Characteristic</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator Characteristic</em>' attribute.
	 * @see #setOperatorCharacteristic(String)
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage#getUserConfiguration_OperatorCharacteristic()
	 * @model default="energy"
	 * @generated
	 */
	String getOperatorCharacteristic();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#getOperatorCharacteristic <em>Operator Characteristic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator Characteristic</em>' attribute.
	 * @see #getOperatorCharacteristic()
	 * @generated
	 */
	void setOperatorCharacteristic(String value);

	/**
	 * Returns the value of the '<em><b>Tag Folder</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tag Folder</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tag Folder</em>' attribute.
	 * @see #setTagFolder(boolean)
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage#getUserConfiguration_TagFolder()
	 * @model default="true"
	 * @generated
	 */
	boolean isTagFolder();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#isTagFolder <em>Tag Folder</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tag Folder</em>' attribute.
	 * @see #isTagFolder()
	 * @generated
	 */
	void setTagFolder(boolean value);

	/**
	 * Returns the value of the '<em><b>Correlation Mode</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Correlation Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Correlation Mode</em>' attribute.
	 * @see #setCorrelationMode(boolean)
	 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage#getUserConfiguration_CorrelationMode()
	 * @model default="false"
	 * @generated
	 */
	boolean isCorrelationMode();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration#isCorrelationMode <em>Correlation Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Correlation Mode</em>' attribute.
	 * @see #isCorrelationMode()
	 * @generated
	 */
	void setCorrelationMode(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isReleaseMode();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isDebugMode();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isTraceMode();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean backendSimulationIsActivated();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean optimizationSimulationIsActivated();

} // UserConfiguration
