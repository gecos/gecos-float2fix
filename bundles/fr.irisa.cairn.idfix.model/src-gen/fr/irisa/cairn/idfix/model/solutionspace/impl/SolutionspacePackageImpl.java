/**
 */
package fr.irisa.cairn.idfix.model.solutionspace.impl;

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import float2fix.Float2fixPackage;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage;
import fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixprojectPackage;
import fr.irisa.cairn.idfix.model.idfixproject.impl.IdfixprojectPackageImpl;
import fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage;
import fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationandtimingPackageImpl;
import fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryPackage;
import fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorlibraryPackageImpl;
import fr.irisa.cairn.idfix.model.solutionspace.LibraryElement;
import fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace;
import fr.irisa.cairn.idfix.model.solutionspace.SolutionspaceFactory;
import fr.irisa.cairn.idfix.model.solutionspace.SolutionspacePackage;
import fr.irisa.cairn.idfix.model.solutionspace.Triplet;
import fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage;
import fr.irisa.cairn.idfix.model.userconfiguration.impl.UserconfigurationPackageImpl;
import gecos.annotations.AnnotationsPackage;
import gecos.blocks.BlocksPackage;
import gecos.core.CorePackage;
import gecos.dag.DagPackage;
import gecos.gecosproject.GecosprojectPackage;
import gecos.instrs.InstrsPackage;
import gecos.types.TypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SolutionspacePackageImpl extends EPackageImpl implements SolutionspacePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tripletEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass solutionSpaceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operatorToListElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass libraryElementEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionspacePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SolutionspacePackageImpl() {
		super(eNS_URI, SolutionspaceFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link SolutionspacePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SolutionspacePackage init() {
		if (isInited) return (SolutionspacePackage)EPackage.Registry.INSTANCE.getEPackage(SolutionspacePackage.eNS_URI);

		// Obtain or create and register package
		SolutionspacePackageImpl theSolutionspacePackage = (SolutionspacePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof SolutionspacePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new SolutionspacePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		Float2fixPackage.eINSTANCE.eClass();
		AnnotationsPackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		InstrsPackage.eINSTANCE.eClass();
		BlocksPackage.eINSTANCE.eClass();
		CorePackage.eINSTANCE.eClass();
		DagPackage.eINSTANCE.eClass();
		GecosprojectPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		FixedpointspecificationPackageImpl theFixedpointspecificationPackage = (FixedpointspecificationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FixedpointspecificationPackage.eNS_URI) instanceof FixedpointspecificationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FixedpointspecificationPackage.eNS_URI) : FixedpointspecificationPackage.eINSTANCE);
		IdfixprojectPackageImpl theIdfixprojectPackage = (IdfixprojectPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(IdfixprojectPackage.eNS_URI) instanceof IdfixprojectPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(IdfixprojectPackage.eNS_URI) : IdfixprojectPackage.eINSTANCE);
		InformationandtimingPackageImpl theInformationandtimingPackage = (InformationandtimingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(InformationandtimingPackage.eNS_URI) instanceof InformationandtimingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(InformationandtimingPackage.eNS_URI) : InformationandtimingPackage.eINSTANCE);
		UserconfigurationPackageImpl theUserconfigurationPackage = (UserconfigurationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UserconfigurationPackage.eNS_URI) instanceof UserconfigurationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UserconfigurationPackage.eNS_URI) : UserconfigurationPackage.eINSTANCE);
		OperatorlibraryPackageImpl theOperatorlibraryPackage = (OperatorlibraryPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OperatorlibraryPackage.eNS_URI) instanceof OperatorlibraryPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OperatorlibraryPackage.eNS_URI) : OperatorlibraryPackage.eINSTANCE);

		// Create package meta-data objects
		theSolutionspacePackage.createPackageContents();
		theFixedpointspecificationPackage.createPackageContents();
		theIdfixprojectPackage.createPackageContents();
		theInformationandtimingPackage.createPackageContents();
		theUserconfigurationPackage.createPackageContents();
		theOperatorlibraryPackage.createPackageContents();

		// Initialize created meta-data
		theSolutionspacePackage.initializePackageContents();
		theFixedpointspecificationPackage.initializePackageContents();
		theIdfixprojectPackage.initializePackageContents();
		theInformationandtimingPackage.initializePackageContents();
		theUserconfigurationPackage.initializePackageContents();
		theOperatorlibraryPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSolutionspacePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(SolutionspacePackage.eNS_URI, theSolutionspacePackage);
		return theSolutionspacePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTriplet() {
		return tripletEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTriplet_First() {
		return (EAttribute)tripletEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTriplet_Second() {
		return (EAttribute)tripletEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTriplet_Third() {
		return (EAttribute)tripletEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSolutionSpace() {
		return solutionSpaceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSolutionSpace_Operators() {
		return (EReference)solutionSpaceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSolutionSpace_Library() {
		return (EReference)solutionSpaceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSolutionSpace_Datas() {
		return (EReference)solutionSpaceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSolutionSpace__SetAllOperatorToMax() {
		return solutionSpaceEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSolutionSpace__SetAllOperatorToMin() {
		return solutionSpaceEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSolutionSpace__SetOperatorTo__Operator_int() {
		return solutionSpaceEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSolutionSpace__GetMaxIndexOf__Operator() {
		return solutionSpaceEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSolutionSpace__GetCurrentIndexOf__Operator() {
		return solutionSpaceEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSolutionSpace__IndexPlusOneTo__Operator() {
		return solutionSpaceEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSolutionSpace__IndexMinusOneTo__Operator() {
		return solutionSpaceEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSolutionSpace__SaveCurrentSolutionSpace() {
		return solutionSpaceEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSolutionSpace__UseSolutionSpaceSaved() {
		return solutionSpaceEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSolutionSpace__GetIndexMaxOf__OperatorKind() {
		return solutionSpaceEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSolutionSpace__SetOperatorToMax__Operator() {
		return solutionSpaceEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSolutionSpace__SetAllOperatorsTo__int() {
		return solutionSpaceEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSolutionSpace__IsMaxIndexOf__Operator() {
		return solutionSpaceEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperatorToListElement() {
		return operatorToListElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperatorToListElement_Key() {
		return (EAttribute)operatorToListElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperatorToListElement_Value() {
		return (EReference)operatorToListElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLibraryElement() {
		return libraryElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLibraryElement_Operand() {
		return (EReference)libraryElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLibraryElement_Cost() {
		return (EAttribute)libraryElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SolutionspaceFactory getSolutionspaceFactory() {
		return (SolutionspaceFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		tripletEClass = createEClass(TRIPLET);
		createEAttribute(tripletEClass, TRIPLET__FIRST);
		createEAttribute(tripletEClass, TRIPLET__SECOND);
		createEAttribute(tripletEClass, TRIPLET__THIRD);

		solutionSpaceEClass = createEClass(SOLUTION_SPACE);
		createEReference(solutionSpaceEClass, SOLUTION_SPACE__OPERATORS);
		createEReference(solutionSpaceEClass, SOLUTION_SPACE__LIBRARY);
		createEReference(solutionSpaceEClass, SOLUTION_SPACE__DATAS);
		createEOperation(solutionSpaceEClass, SOLUTION_SPACE___SET_ALL_OPERATOR_TO_MAX);
		createEOperation(solutionSpaceEClass, SOLUTION_SPACE___SET_ALL_OPERATOR_TO_MIN);
		createEOperation(solutionSpaceEClass, SOLUTION_SPACE___SET_OPERATOR_TO__OPERATOR_INT);
		createEOperation(solutionSpaceEClass, SOLUTION_SPACE___IS_MAX_INDEX_OF__OPERATOR);
		createEOperation(solutionSpaceEClass, SOLUTION_SPACE___GET_MAX_INDEX_OF__OPERATOR);
		createEOperation(solutionSpaceEClass, SOLUTION_SPACE___GET_CURRENT_INDEX_OF__OPERATOR);
		createEOperation(solutionSpaceEClass, SOLUTION_SPACE___INDEX_PLUS_ONE_TO__OPERATOR);
		createEOperation(solutionSpaceEClass, SOLUTION_SPACE___INDEX_MINUS_ONE_TO__OPERATOR);
		createEOperation(solutionSpaceEClass, SOLUTION_SPACE___SAVE_CURRENT_SOLUTION_SPACE);
		createEOperation(solutionSpaceEClass, SOLUTION_SPACE___USE_SOLUTION_SPACE_SAVED);
		createEOperation(solutionSpaceEClass, SOLUTION_SPACE___GET_INDEX_MAX_OF__OPERATORKIND);
		createEOperation(solutionSpaceEClass, SOLUTION_SPACE___SET_OPERATOR_TO_MAX__OPERATOR);
		createEOperation(solutionSpaceEClass, SOLUTION_SPACE___SET_ALL_OPERATORS_TO__INT);

		operatorToListElementEClass = createEClass(OPERATOR_TO_LIST_ELEMENT);
		createEAttribute(operatorToListElementEClass, OPERATOR_TO_LIST_ELEMENT__KEY);
		createEReference(operatorToListElementEClass, OPERATOR_TO_LIST_ELEMENT__VALUE);

		libraryElementEClass = createEClass(LIBRARY_ELEMENT);
		createEReference(libraryElementEClass, LIBRARY_ELEMENT__OPERAND);
		createEAttribute(libraryElementEClass, LIBRARY_ELEMENT__COST);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		FixedpointspecificationPackage theFixedpointspecificationPackage = (FixedpointspecificationPackage)EPackage.Registry.INSTANCE.getEPackage(FixedpointspecificationPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(tripletEClass, Triplet.class, "Triplet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTriplet_First(), ecorePackage.getEInt(), "first", null, 0, 1, Triplet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTriplet_Second(), ecorePackage.getEInt(), "second", null, 0, 1, Triplet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTriplet_Third(), ecorePackage.getEInt(), "third", null, 0, 1, Triplet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(solutionSpaceEClass, SolutionSpace.class, "SolutionSpace", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSolutionSpace_Operators(), theFixedpointspecificationPackage.getOperator(), null, "operators", null, 0, -1, SolutionSpace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSolutionSpace_Library(), this.getOperatorToListElement(), null, "library", null, 0, -1, SolutionSpace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSolutionSpace_Datas(), theFixedpointspecificationPackage.getData(), null, "datas", null, 0, -1, SolutionSpace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getSolutionSpace__SetAllOperatorToMax(), null, "setAllOperatorToMax", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getSolutionSpace__SetAllOperatorToMin(), null, "setAllOperatorToMin", 0, 1, IS_UNIQUE, IS_ORDERED);

		EOperation op = initEOperation(getSolutionSpace__SetOperatorTo__Operator_int(), null, "setOperatorTo", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theFixedpointspecificationPackage.getOperator(), "operator", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "index", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getSolutionSpace__IsMaxIndexOf__Operator(), ecorePackage.getEBoolean(), "isMaxIndexOf", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theFixedpointspecificationPackage.getOperator(), "operator", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getSolutionSpace__GetMaxIndexOf__Operator(), ecorePackage.getEInt(), "getMaxIndexOf", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theFixedpointspecificationPackage.getOperator(), "operator", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getSolutionSpace__GetCurrentIndexOf__Operator(), ecorePackage.getEInt(), "getCurrentIndexOf", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theFixedpointspecificationPackage.getOperator(), "operator", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getSolutionSpace__IndexPlusOneTo__Operator(), null, "indexPlusOneTo", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theFixedpointspecificationPackage.getOperator(), "operator", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getSolutionSpace__IndexMinusOneTo__Operator(), null, "indexMinusOneTo", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theFixedpointspecificationPackage.getOperator(), "operator", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getSolutionSpace__SaveCurrentSolutionSpace(), null, "saveCurrentSolutionSpace", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getSolutionSpace__UseSolutionSpaceSaved(), null, "useSolutionSpaceSaved", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getSolutionSpace__GetIndexMaxOf__OperatorKind(), ecorePackage.getEInt(), "getIndexMaxOf", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theFixedpointspecificationPackage.getOperatorKind(), "operatorKind", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getSolutionSpace__SetOperatorToMax__Operator(), null, "setOperatorToMax", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theFixedpointspecificationPackage.getOperator(), "op", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getSolutionSpace__SetAllOperatorsTo__int(), null, "setAllOperatorsTo", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "index", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(operatorToListElementEClass, Map.Entry.class, "OperatorToListElement", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOperatorToListElement_Key(), theFixedpointspecificationPackage.getOperatorKind(), "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperatorToListElement_Value(), this.getLibraryElement(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(libraryElementEClass, LibraryElement.class, "LibraryElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLibraryElement_Operand(), this.getTriplet(), null, "operand", null, 0, 1, LibraryElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLibraryElement_Cost(), ecorePackage.getEFloat(), "cost", null, 0, 1, LibraryElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //SolutionspacePackageImpl
