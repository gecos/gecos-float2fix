/**
 */
package fr.irisa.cairn.idfix.model.fixedpointspecification;

import gecos.core.Symbol;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Data#getFixedInformation <em>Fixed Information</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Data#getCorrespondingSymbol <em>Corresponding Symbol</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Data#getCDFGNumber <em>CDFG Number</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getData()
 * @model
 * @generated
 */
public interface Data extends CDFGNode {
	/**
	 * Returns the value of the '<em><b>Fixed Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fixed Information</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fixed Information</em>' containment reference.
	 * @see #setFixedInformation(FixedPointInformation)
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getData_FixedInformation()
	 * @model containment="true"
	 * @generated
	 */
	FixedPointInformation getFixedInformation();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Data#getFixedInformation <em>Fixed Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fixed Information</em>' containment reference.
	 * @see #getFixedInformation()
	 * @generated
	 */
	void setFixedInformation(FixedPointInformation value);

	/**
	 * Returns the value of the '<em><b>Corresponding Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Corresponding Symbol</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Corresponding Symbol</em>' reference.
	 * @see #setCorrespondingSymbol(Symbol)
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getData_CorrespondingSymbol()
	 * @model
	 * @generated
	 */
	Symbol getCorrespondingSymbol();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Data#getCorrespondingSymbol <em>Corresponding Symbol</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Corresponding Symbol</em>' reference.
	 * @see #getCorrespondingSymbol()
	 * @generated
	 */
	void setCorrespondingSymbol(Symbol value);

	/**
	 * Returns the value of the '<em><b>CDFG Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>CDFG Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CDFG Number</em>' attribute.
	 * @see #setCDFGNumber(int)
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getData_CDFGNumber()
	 * @model
	 * @generated
	 */
	int getCDFGNumber();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Data#getCDFGNumber <em>CDFG Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CDFG Number</em>' attribute.
	 * @see #getCDFGNumber()
	 * @generated
	 */
	void setCDFGNumber(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<Operation> getSuccessorOperators();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<Operation> getPredecessorOperators();

} // Data
