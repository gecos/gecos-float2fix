/**
 */
package fr.irisa.cairn.idfix.model.operatorlibrary;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operator Library</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.operatorlibrary.OperatorLibrary#getOperators <em>Operators</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryPackage#getOperatorLibrary()
 * @model
 * @generated
 */
public interface OperatorLibrary extends EObject {
	/**
	 * Returns the value of the '<em><b>Operators</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.cairn.idfix.model.operatorlibrary.Operator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operators</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operators</em>' reference list.
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryPackage#getOperatorLibrary_Operators()
	 * @model
	 * @generated
	 */
	EList<Operator> getOperators();

	/**
	 * <!-- begin-user-doc -->
	 * Return the operator corresponding with the operator kind, null otherwise
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Operator getOperator(OPERATOR_KIND kind);

} // OperatorLibrary
