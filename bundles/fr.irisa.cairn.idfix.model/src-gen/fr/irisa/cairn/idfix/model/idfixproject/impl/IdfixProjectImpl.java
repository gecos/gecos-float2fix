/**
 */
package fr.irisa.cairn.idfix.model.idfixproject.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import fr.irisa.cairn.idfix.model.fixedpointspecification.Data;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixProject;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixprojectPackage;
import fr.irisa.cairn.idfix.model.informationandtiming.Informations;
import fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.IDFixUtils;
import fr.irisa.cairn.idfix.utils.exceptions.SFGModelCreationException;
import fr.irisa.cairn.idfix.utils.logging.IDFixLogger;
import gecos.gecosproject.GecosProject;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Idfix Project</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.idfixproject.impl.IdfixProjectImpl#getGecosProject <em>Gecos Project</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.idfixproject.impl.IdfixProjectImpl#getGecosProjectAnnotedAndNotUnrolled <em>Gecos Project Annoted And Not Unrolled</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.idfixproject.impl.IdfixProjectImpl#getFixedPointSpecification <em>Fixed Point Specification</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.idfixproject.impl.IdfixProjectImpl#getOutputFolderPath <em>Output Folder Path</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.idfixproject.impl.IdfixProjectImpl#getProcessInformations <em>Process Informations</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.idfixproject.impl.IdfixProjectImpl#getUserConfiguration <em>User Configuration</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IdfixProjectImpl extends MinimalEObjectImpl.Container implements IdfixProject {
	/**
	 * The cached value of the '{@link #getGecosProject() <em>Gecos Project</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGecosProject()
	 * @generated
	 * @ordered
	 */
	protected GecosProject gecosProject;

	/**
	 * The cached value of the '{@link #getGecosProjectAnnotedAndNotUnrolled() <em>Gecos Project Annoted And Not Unrolled</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGecosProjectAnnotedAndNotUnrolled()
	 * @generated
	 * @ordered
	 */
	protected GecosProject gecosProjectAnnotedAndNotUnrolled;

	/**
	 * The cached value of the '{@link #getFixedPointSpecification() <em>Fixed Point Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFixedPointSpecification()
	 * @generated
	 * @ordered
	 */
	protected FixedPointSpecification fixedPointSpecification;

	/**
	 * The default value of the '{@link #getOutputFolderPath() <em>Output Folder Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputFolderPath()
	 * @generated
	 * @ordered
	 */
	protected static final String OUTPUT_FOLDER_PATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOutputFolderPath() <em>Output Folder Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputFolderPath()
	 * @generated
	 * @ordered
	 */
	protected String outputFolderPath = OUTPUT_FOLDER_PATH_EDEFAULT;

	/**
	 * The cached value of the '{@link #getProcessInformations() <em>Process Informations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessInformations()
	 * @generated
	 * @ordered
	 */
	protected Informations processInformations;

	/**
	 * The cached value of the '{@link #getUserConfiguration() <em>User Configuration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserConfiguration()
	 * @generated
	 * @ordered
	 */
	protected UserConfiguration userConfiguration;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IdfixProjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IdfixprojectPackage.Literals.IDFIX_PROJECT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GecosProject getGecosProject() {
		if (gecosProject != null && gecosProject.eIsProxy()) {
			InternalEObject oldGecosProject = (InternalEObject)gecosProject;
			gecosProject = (GecosProject)eResolveProxy(oldGecosProject);
			if (gecosProject != oldGecosProject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IdfixprojectPackage.IDFIX_PROJECT__GECOS_PROJECT, oldGecosProject, gecosProject));
			}
		}
		return gecosProject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GecosProject basicGetGecosProject() {
		return gecosProject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGecosProject(GecosProject newGecosProject) {
		GecosProject oldGecosProject = gecosProject;
		gecosProject = newGecosProject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IdfixprojectPackage.IDFIX_PROJECT__GECOS_PROJECT, oldGecosProject, gecosProject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GecosProject getGecosProjectAnnotedAndNotUnrolled() {
		if (gecosProjectAnnotedAndNotUnrolled != null && gecosProjectAnnotedAndNotUnrolled.eIsProxy()) {
			InternalEObject oldGecosProjectAnnotedAndNotUnrolled = (InternalEObject)gecosProjectAnnotedAndNotUnrolled;
			gecosProjectAnnotedAndNotUnrolled = (GecosProject)eResolveProxy(oldGecosProjectAnnotedAndNotUnrolled);
			if (gecosProjectAnnotedAndNotUnrolled != oldGecosProjectAnnotedAndNotUnrolled) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IdfixprojectPackage.IDFIX_PROJECT__GECOS_PROJECT_ANNOTED_AND_NOT_UNROLLED, oldGecosProjectAnnotedAndNotUnrolled, gecosProjectAnnotedAndNotUnrolled));
			}
		}
		return gecosProjectAnnotedAndNotUnrolled;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GecosProject basicGetGecosProjectAnnotedAndNotUnrolled() {
		return gecosProjectAnnotedAndNotUnrolled;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGecosProjectAnnotedAndNotUnrolled(GecosProject newGecosProjectAnnotedAndNotUnrolled) {
		GecosProject oldGecosProjectAnnotedAndNotUnrolled = gecosProjectAnnotedAndNotUnrolled;
		gecosProjectAnnotedAndNotUnrolled = newGecosProjectAnnotedAndNotUnrolled;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IdfixprojectPackage.IDFIX_PROJECT__GECOS_PROJECT_ANNOTED_AND_NOT_UNROLLED, oldGecosProjectAnnotedAndNotUnrolled, gecosProjectAnnotedAndNotUnrolled));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixedPointSpecification getFixedPointSpecification() {
		return fixedPointSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFixedPointSpecification(FixedPointSpecification newFixedPointSpecification, NotificationChain msgs) {
		FixedPointSpecification oldFixedPointSpecification = fixedPointSpecification;
		fixedPointSpecification = newFixedPointSpecification;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, IdfixprojectPackage.IDFIX_PROJECT__FIXED_POINT_SPECIFICATION, oldFixedPointSpecification, newFixedPointSpecification);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFixedPointSpecification(FixedPointSpecification newFixedPointSpecification) {
		if (newFixedPointSpecification != fixedPointSpecification) {
			NotificationChain msgs = null;
			if (fixedPointSpecification != null)
				msgs = ((InternalEObject)fixedPointSpecification).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - IdfixprojectPackage.IDFIX_PROJECT__FIXED_POINT_SPECIFICATION, null, msgs);
			if (newFixedPointSpecification != null)
				msgs = ((InternalEObject)newFixedPointSpecification).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - IdfixprojectPackage.IDFIX_PROJECT__FIXED_POINT_SPECIFICATION, null, msgs);
			msgs = basicSetFixedPointSpecification(newFixedPointSpecification, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IdfixprojectPackage.IDFIX_PROJECT__FIXED_POINT_SPECIFICATION, newFixedPointSpecification, newFixedPointSpecification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOutputFolderPath() {
		return outputFolderPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutputFolderPath(String newOutputFolderPath) {
		String oldOutputFolderPath = outputFolderPath;
		outputFolderPath = newOutputFolderPath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IdfixprojectPackage.IDFIX_PROJECT__OUTPUT_FOLDER_PATH, oldOutputFolderPath, outputFolderPath));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Informations getProcessInformations() {
		return processInformations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessInformations(Informations newProcessInformations, NotificationChain msgs) {
		Informations oldProcessInformations = processInformations;
		processInformations = newProcessInformations;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, IdfixprojectPackage.IDFIX_PROJECT__PROCESS_INFORMATIONS, oldProcessInformations, newProcessInformations);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessInformations(Informations newProcessInformations) {
		if (newProcessInformations != processInformations) {
			NotificationChain msgs = null;
			if (processInformations != null)
				msgs = ((InternalEObject)processInformations).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - IdfixprojectPackage.IDFIX_PROJECT__PROCESS_INFORMATIONS, null, msgs);
			if (newProcessInformations != null)
				msgs = ((InternalEObject)newProcessInformations).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - IdfixprojectPackage.IDFIX_PROJECT__PROCESS_INFORMATIONS, null, msgs);
			msgs = basicSetProcessInformations(newProcessInformations, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IdfixprojectPackage.IDFIX_PROJECT__PROCESS_INFORMATIONS, newProcessInformations, newProcessInformations));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserConfiguration getUserConfiguration() {
		if (userConfiguration != null && userConfiguration.eIsProxy()) {
			InternalEObject oldUserConfiguration = (InternalEObject)userConfiguration;
			userConfiguration = (UserConfiguration)eResolveProxy(oldUserConfiguration);
			if (userConfiguration != oldUserConfiguration) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IdfixprojectPackage.IDFIX_PROJECT__USER_CONFIGURATION, oldUserConfiguration, userConfiguration));
			}
		}
		return userConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserConfiguration basicGetUserConfiguration() {
		return userConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUserConfiguration(UserConfiguration newUserConfiguration) {
		UserConfiguration oldUserConfiguration = userConfiguration;
		userConfiguration = newUserConfiguration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IdfixprojectPackage.IDFIX_PROJECT__USER_CONFIGURATION, oldUserConfiguration, userConfiguration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getIDFixEvalOutputFolderPath() {
		return outputFolderPath + ConstantPathAndName.IDFIX_EVAL_OUTPUT_FOLDER_NAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getIDFixConvOutputFolderPath() {
		return outputFolderPath + ConstantPathAndName.IDFIX_CONV_OUTPUT_FOLDER_NAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getSFGFilePath() {
		return getIDFixConvOutputFolderPath() + ConstantPathAndName.GEN_SFG_FILE_NAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getSimulatedValuesFilePath() {
		return getIDFixConvOutputFolderPath() + ConstantPathAndName.SIMU_VALUES_FILE_NAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Return the path of the dynamic specification file generated by IDFixEval.
	 * If the project have detected the reuse last process result compatibility, 
	 * it will be return the dynamic specification contained in the last run result save folder. 
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getDynamicFilePath() {
		if(!getProcessInformations().isUseLastResultModeEnable())
			return getIDFixEvalOutputFolderPath() + ConstantPathAndName.GEN_DYNAMIC_SPECIFICATION_FILE_NAME;
		else
			return getResuseLastResutAndInformationFolderPath() + ConstantPathAndName.GEN_DYNAMIC_SPECIFICATION_FILE_NAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Return the path of the noise function library file generated by IDFixEval.
	 * If the project have detected the reuse last process result compatibility, 
	 * it will be return the noise function library contained in the last run result save folder. 
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getNoiseLibraryFilePath() {
		if(!getProcessInformations().isUseLastResultModeEnable())
			return getIDFixEvalOutputFolderPath() + ConstantPathAndName.GEN_NOISE_LIBRARY_FILE_NAME;
		else
			return getResuseLastResutAndInformationFolderPath() + ConstantPathAndName.GEN_NOISE_LIBRARY_FILE_NAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void generateLogInformations() {
		try {
			PrintWriter printer = new PrintWriter(new File(this.getOutputFolderPath() + ConstantPathAndName.LOG_FODLER_NAME + ConstantPathAndName.LOG_FILE_NAME));
			
			if(this.getUserConfiguration().isDebugMode()){
				printer.println("Debugging on");
				printer.println("Output folder path : " + this.getOutputFolderPath());
				printer.println("IDFix conv output folder path : " + this.getIDFixConvOutputFolderPath());
				printer.println("IDFix eval output folder path : " + this.getIDFixEvalOutputFolderPath());
				printer.println("Signal Flow Graph file path : " + this.getSFGFilePath());
				printer.println("Simulated values file path : " + this.getSimulatedValuesFilePath());
				printer.println("Dynamic file path : " + this.getDynamicFilePath());
				printer.println("Noise library path : " + this.getNoiseLibraryFilePath());
				printer.println("");
			}
			
			printer.println(this.getProcessInformations());
			printer.close();
		} catch (FileNotFoundException e) {
			IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_GLOBAL).warn("Impossible to create the log information file.");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * If the reuse mode function is disable. The back up of the result to the last run result save folder is done. 
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	//TODO ajouter la création du lien symbolique des matrices KLM
	public void finalize() {
		// if we reuse information, we don't need to copy the current result
		if(!getProcessInformations().isUseLastResultModeEnable()){
			GecosProject gecosProject = this.getGecosProject();
			File projectCSourceFile = new File(gecosProject.getSources().get(0).getName());
			if(!projectCSourceFile.isFile()){
				throw new RuntimeException("The original C source code doesn't exist or have been not found");
			}			
			
			try {
				File dynamicFile = new File(getResuseLastResutAndInformationFolderPath(), ConstantPathAndName.GEN_DYNAMIC_SPECIFICATION_FILE_NAME);
				File noiseLibraryFunctionFile = new File(getResuseLastResutAndInformationFolderPath(), ConstantPathAndName.GEN_NOISE_LIBRARY_FILE_NAME);
				File KLMMatrixFile = new File(getResuseLastResutAndInformationFolderPath(), ConstantPathAndName.GEN_KLM_MATRIX_FILE_NAME);
				
				// Need to delete all symbolic link before create them
				if(Files.isSymbolicLink(dynamicFile.toPath()) && !dynamicFile.delete()){
						throw new IOException("Error during removal the dynamic file symbolic link");
				}
				if(Files.isSymbolicLink(noiseLibraryFunctionFile.toPath()) && !noiseLibraryFunctionFile.delete()){
						throw new IOException("Error during removal the dynamic file symbolic link");
				}
				if(Files.isSymbolicLink(KLMMatrixFile.toPath()) && !KLMMatrixFile.delete()){
					throw new IOException("Error during removal the KLM matrix file symbolic link");
				}
				
				// Dynamic file symbolic link creation
				Path dynamicFilePath = Paths.get(new File(this.getDynamicFilePath()).getAbsolutePath());
				Path dynamicFileTargetPath = Paths.get(dynamicFile.getAbsolutePath());
				Files.createSymbolicLink(dynamicFileTargetPath, dynamicFilePath);
				
				// Noise function library symbolic link creation
				Path noiseLibraryFunctionPath = Paths.get(new File(this.getNoiseLibraryFilePath()).getAbsolutePath());
				Path noiseLibraryFunctionTargetPath = Paths.get(noiseLibraryFunctionFile.getAbsolutePath());
				Files.createSymbolicLink(noiseLibraryFunctionTargetPath, noiseLibraryFunctionPath);
				
				// KLM matrix symbolic link creation
				Path KLMMatrixFilePath = Paths.get(new File(this.getIDFixEvalOutputFolderPath()
						+ ConstantPathAndName.IDFIX_EVAL_GENERATED_FODLER_NAME + ConstantPathAndName.GEN_KLM_MATRIX_FILE_NAME).getAbsolutePath());
				Path KLMMatrixTargetPath = Paths.get(KLMMatrixFile.getAbsolutePath());
				Files.createSymbolicLink(KLMMatrixTargetPath, KLMMatrixFilePath);
				
				// Project C source file copy 
				IDFixUtils.copyFile(projectCSourceFile, new File(getResuseLastResutAndInformationFolderPath(), ConstantPathAndName.GEN_LAST_RUN_C_SOURCE_FILE_NAME));
			} catch (IOException e) {
				IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_GLOBAL).warn("Error during the copy of the process result and information to the last run process save folder");
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * Write in file some information which make it possible to generate graph
	 * The information is written in a gnu plot matrix format
	 * Column 1: User noise constraint
	 * Column 2: Input Signal range
	 * Column 3: Analytic noise power - simulation noise power
	 * Column 4: Analytic noise power
	 * Column 5: Simulation noise power 
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */	
	public void plotSaveInformations() {
		FixedPointSpecification fixedSpecification = this.getFixedPointSpecification();
		File matrix = new File(new File(this.getOutputFolderPath()).getParentFile(), "matrix.value"); 
		try{
			if(!matrix.isFile()){
				matrix.createNewFile();
			}
			PrintWriter writermatrix = new PrintWriter(new FileWriter(matrix, true));
			
			// User noise constraint 
			writermatrix.print(this.getProcessInformations().getUserNoiseConstraint() + " ");
			
			// Input signal range
			for(Data data : fixedSpecification.getDatas()){
				if(IDFixUtils.isInputVar(data.getCorrespondingSymbol())){
					writermatrix.print(IDFixUtils.getVarMax(data.getCorrespondingSymbol())
							- IDFixUtils.getVarMin(data.getCorrespondingSymbol()) + " ");
					break;
				}				
			}
			
			// Analytic noise power - simulation noise power
			writermatrix.print(this.getProcessInformations().getAnalyticInformations().getFinalNoiseConstraint()
					- this.getProcessInformations().getSimulationInformations().getNoisePowerResult());
			writermatrix.print(" ");
			
			// Analytic noise power
			writermatrix.print(this.getProcessInformations().getAnalyticInformations().getFinalNoiseConstraint());
			writermatrix.print(" ");
			
			// Simulation noise power 
			writermatrix.println(this.getProcessInformations().getSimulationInformations().getNoisePowerResult());
			
			writermatrix.close();
		} catch (IOException | SFGModelCreationException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case IdfixprojectPackage.IDFIX_PROJECT__FIXED_POINT_SPECIFICATION:
				return basicSetFixedPointSpecification(null, msgs);
			case IdfixprojectPackage.IDFIX_PROJECT__PROCESS_INFORMATIONS:
				return basicSetProcessInformations(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case IdfixprojectPackage.IDFIX_PROJECT__GECOS_PROJECT:
				if (resolve) return getGecosProject();
				return basicGetGecosProject();
			case IdfixprojectPackage.IDFIX_PROJECT__GECOS_PROJECT_ANNOTED_AND_NOT_UNROLLED:
				if (resolve) return getGecosProjectAnnotedAndNotUnrolled();
				return basicGetGecosProjectAnnotedAndNotUnrolled();
			case IdfixprojectPackage.IDFIX_PROJECT__FIXED_POINT_SPECIFICATION:
				return getFixedPointSpecification();
			case IdfixprojectPackage.IDFIX_PROJECT__OUTPUT_FOLDER_PATH:
				return getOutputFolderPath();
			case IdfixprojectPackage.IDFIX_PROJECT__PROCESS_INFORMATIONS:
				return getProcessInformations();
			case IdfixprojectPackage.IDFIX_PROJECT__USER_CONFIGURATION:
				if (resolve) return getUserConfiguration();
				return basicGetUserConfiguration();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case IdfixprojectPackage.IDFIX_PROJECT__GECOS_PROJECT:
				setGecosProject((GecosProject)newValue);
				return;
			case IdfixprojectPackage.IDFIX_PROJECT__GECOS_PROJECT_ANNOTED_AND_NOT_UNROLLED:
				setGecosProjectAnnotedAndNotUnrolled((GecosProject)newValue);
				return;
			case IdfixprojectPackage.IDFIX_PROJECT__FIXED_POINT_SPECIFICATION:
				setFixedPointSpecification((FixedPointSpecification)newValue);
				return;
			case IdfixprojectPackage.IDFIX_PROJECT__OUTPUT_FOLDER_PATH:
				setOutputFolderPath((String)newValue);
				return;
			case IdfixprojectPackage.IDFIX_PROJECT__PROCESS_INFORMATIONS:
				setProcessInformations((Informations)newValue);
				return;
			case IdfixprojectPackage.IDFIX_PROJECT__USER_CONFIGURATION:
				setUserConfiguration((UserConfiguration)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case IdfixprojectPackage.IDFIX_PROJECT__GECOS_PROJECT:
				setGecosProject((GecosProject)null);
				return;
			case IdfixprojectPackage.IDFIX_PROJECT__GECOS_PROJECT_ANNOTED_AND_NOT_UNROLLED:
				setGecosProjectAnnotedAndNotUnrolled((GecosProject)null);
				return;
			case IdfixprojectPackage.IDFIX_PROJECT__FIXED_POINT_SPECIFICATION:
				setFixedPointSpecification((FixedPointSpecification)null);
				return;
			case IdfixprojectPackage.IDFIX_PROJECT__OUTPUT_FOLDER_PATH:
				setOutputFolderPath(OUTPUT_FOLDER_PATH_EDEFAULT);
				return;
			case IdfixprojectPackage.IDFIX_PROJECT__PROCESS_INFORMATIONS:
				setProcessInformations((Informations)null);
				return;
			case IdfixprojectPackage.IDFIX_PROJECT__USER_CONFIGURATION:
				setUserConfiguration((UserConfiguration)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case IdfixprojectPackage.IDFIX_PROJECT__GECOS_PROJECT:
				return gecosProject != null;
			case IdfixprojectPackage.IDFIX_PROJECT__GECOS_PROJECT_ANNOTED_AND_NOT_UNROLLED:
				return gecosProjectAnnotedAndNotUnrolled != null;
			case IdfixprojectPackage.IDFIX_PROJECT__FIXED_POINT_SPECIFICATION:
				return fixedPointSpecification != null;
			case IdfixprojectPackage.IDFIX_PROJECT__OUTPUT_FOLDER_PATH:
				return OUTPUT_FOLDER_PATH_EDEFAULT == null ? outputFolderPath != null : !OUTPUT_FOLDER_PATH_EDEFAULT.equals(outputFolderPath);
			case IdfixprojectPackage.IDFIX_PROJECT__PROCESS_INFORMATIONS:
				return processInformations != null;
			case IdfixprojectPackage.IDFIX_PROJECT__USER_CONFIGURATION:
				return userConfiguration != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case IdfixprojectPackage.IDFIX_PROJECT___GET_ID_FIX_EVAL_OUTPUT_FOLDER_PATH:
				return getIDFixEvalOutputFolderPath();
			case IdfixprojectPackage.IDFIX_PROJECT___GET_ID_FIX_CONV_OUTPUT_FOLDER_PATH:
				return getIDFixConvOutputFolderPath();
			case IdfixprojectPackage.IDFIX_PROJECT___GET_SFG_FILE_PATH:
				return getSFGFilePath();
			case IdfixprojectPackage.IDFIX_PROJECT___GET_SIMULATED_VALUES_FILE_PATH:
				return getSimulatedValuesFilePath();
			case IdfixprojectPackage.IDFIX_PROJECT___GET_DYNAMIC_FILE_PATH:
				return getDynamicFilePath();
			case IdfixprojectPackage.IDFIX_PROJECT___GET_NOISE_LIBRARY_FILE_PATH:
				return getNoiseLibraryFilePath();
			case IdfixprojectPackage.IDFIX_PROJECT___GENERATE_LOG_INFORMATIONS:
				generateLogInformations();
				return null;
			case IdfixprojectPackage.IDFIX_PROJECT___FINALIZE:
				finalize();
				return null;
			case IdfixprojectPackage.IDFIX_PROJECT___PLOT_SAVE_INFORMATIONS:
				plotSaveInformations();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (outputFolderPath: ");
		result.append(outputFolderPath);
		result.append(')');
		return result.toString();
	}
	
	/**
	 * 
	 * @generated NOT
	 */
	private String getResuseLastResutAndInformationFolderPath(){
		return new File(getOutputFolderPath()).getParent() + "/" + ConstantPathAndName.GEN_LAST_RUN_FOLDER_NAME;
	}

} //IdfixProjectImpl
