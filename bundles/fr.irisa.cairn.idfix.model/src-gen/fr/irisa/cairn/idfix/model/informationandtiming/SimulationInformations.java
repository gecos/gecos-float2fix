/**
 */
package fr.irisa.cairn.idfix.model.informationandtiming;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simulation Informations</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.SimulationInformations#getSampleNumber <em>Sample Number</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.SimulationInformations#getNoisePowerResult <em>Noise Power Result</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage#getSimulationInformations()
 * @model
 * @generated
 */
public interface SimulationInformations extends EObject {
	/**
	 * Returns the value of the '<em><b>Sample Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sample Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sample Number</em>' attribute.
	 * @see #setSampleNumber(int)
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage#getSimulationInformations_SampleNumber()
	 * @model
	 * @generated
	 */
	int getSampleNumber();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.informationandtiming.SimulationInformations#getSampleNumber <em>Sample Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sample Number</em>' attribute.
	 * @see #getSampleNumber()
	 * @generated
	 */
	void setSampleNumber(int value);

	/**
	 * Returns the value of the '<em><b>Noise Power Result</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Noise Power Result</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Noise Power Result</em>' attribute.
	 * @see #setNoisePowerResult(double)
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage#getSimulationInformations_NoisePowerResult()
	 * @model
	 * @generated
	 */
	double getNoisePowerResult();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.informationandtiming.SimulationInformations#getNoisePowerResult <em>Noise Power Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Noise Power Result</em>' attribute.
	 * @see #getNoisePowerResult()
	 * @generated
	 */
	void setNoisePowerResult(double value);

} // SimulationInformations
