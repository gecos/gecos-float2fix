/**
 */
package fr.irisa.cairn.idfix.model.fixedpointspecification.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge;
import fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Data;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Operation;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage
 * @generated
 */
public class FixedpointspecificationAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static FixedpointspecificationPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixedpointspecificationAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = FixedpointspecificationPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FixedpointspecificationSwitch<Adapter> modelSwitch =
		new FixedpointspecificationSwitch<Adapter>() {
			@Override
			public Adapter caseFixedPointSpecification(FixedPointSpecification object) {
				return createFixedPointSpecificationAdapter();
			}
			@Override
			public Adapter caseData(Data object) {
				return createDataAdapter();
			}
			@Override
			public Adapter caseOperator(Operation object) {
				return createOperatorAdapter();
			}
			@Override
			public Adapter caseFixedPointInformation(FixedPointInformation object) {
				return createFixedPointInformationAdapter();
			}
			@Override
			public Adapter caseDynamic(Dynamic object) {
				return createDynamicAdapter();
			}
			@Override
			public Adapter caseCDFGNode(CDFGNode object) {
				return createCDFGNodeAdapter();
			}
			@Override
			public Adapter caseCDFGEdge(CDFGEdge object) {
				return createCDFGEdgeAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification <em>Fixed Point Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification
	 * @generated
	 */
	public Adapter createFixedPointSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Data <em>Data</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Data
	 * @generated
	 */
	public Adapter createDataAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Operation
	 * @generated
	 */
	public Adapter createOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation <em>Fixed Point Information</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation
	 * @generated
	 */
	public Adapter createFixedPointInformationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic <em>Dynamic</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic
	 * @generated
	 */
	public Adapter createDynamicAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode <em>CDFG Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode
	 * @generated
	 */
	public Adapter createCDFGNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge <em>CDFG Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge
	 * @generated
	 */
	public Adapter createCDFGEdgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //FixedpointspecificationAdapterFactory
