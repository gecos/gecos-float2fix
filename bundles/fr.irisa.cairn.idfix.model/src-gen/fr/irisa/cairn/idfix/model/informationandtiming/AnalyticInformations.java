/**
 */
package fr.irisa.cairn.idfix.model.informationandtiming;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Analytic Informations</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.AnalyticInformations#getFinalNoiseConstraint <em>Final Noise Constraint</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.AnalyticInformations#getFinalCostConstraint <em>Final Cost Constraint</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.AnalyticInformations#getNoiseEvalutationNumber <em>Noise Evalutation Number</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.AnalyticInformations#getCostEvaluationNumber <em>Cost Evaluation Number</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage#getAnalyticInformations()
 * @model
 * @generated
 */
public interface AnalyticInformations extends EObject {
	/**
	 * Returns the value of the '<em><b>Final Noise Constraint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final Noise Constraint</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Noise Constraint</em>' attribute.
	 * @see #setFinalNoiseConstraint(double)
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage#getAnalyticInformations_FinalNoiseConstraint()
	 * @model
	 * @generated
	 */
	double getFinalNoiseConstraint();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.informationandtiming.AnalyticInformations#getFinalNoiseConstraint <em>Final Noise Constraint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final Noise Constraint</em>' attribute.
	 * @see #getFinalNoiseConstraint()
	 * @generated
	 */
	void setFinalNoiseConstraint(double value);

	/**
	 * Returns the value of the '<em><b>Final Cost Constraint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final Cost Constraint</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Cost Constraint</em>' attribute.
	 * @see #setFinalCostConstraint(float)
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage#getAnalyticInformations_FinalCostConstraint()
	 * @model
	 * @generated
	 */
	float getFinalCostConstraint();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.informationandtiming.AnalyticInformations#getFinalCostConstraint <em>Final Cost Constraint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final Cost Constraint</em>' attribute.
	 * @see #getFinalCostConstraint()
	 * @generated
	 */
	void setFinalCostConstraint(float value);

	/**
	 * Returns the value of the '<em><b>Noise Evalutation Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Noise Evalutation Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Noise Evalutation Number</em>' attribute.
	 * @see #setNoiseEvalutationNumber(int)
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage#getAnalyticInformations_NoiseEvalutationNumber()
	 * @model
	 * @generated
	 */
	int getNoiseEvalutationNumber();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.informationandtiming.AnalyticInformations#getNoiseEvalutationNumber <em>Noise Evalutation Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Noise Evalutation Number</em>' attribute.
	 * @see #getNoiseEvalutationNumber()
	 * @generated
	 */
	void setNoiseEvalutationNumber(int value);

	/**
	 * Returns the value of the '<em><b>Cost Evaluation Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cost Evaluation Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cost Evaluation Number</em>' attribute.
	 * @see #setCostEvaluationNumber(int)
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage#getAnalyticInformations_CostEvaluationNumber()
	 * @model
	 * @generated
	 */
	int getCostEvaluationNumber();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.informationandtiming.AnalyticInformations#getCostEvaluationNumber <em>Cost Evaluation Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cost Evaluation Number</em>' attribute.
	 * @see #getCostEvaluationNumber()
	 * @generated
	 */
	void setCostEvaluationNumber(int value);

} // AnalyticInformations
