/**
 */
package fr.irisa.cairn.idfix.model.userconfiguration;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>DATA TYPE REGENERATION</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage#getDATA_TYPE_REGENERATION()
 * @model
 * @generated
 */
public enum DATA_TYPE_REGENERATION implements Enumerator {
	/**
	 * The '<em><b>SC FIXED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SC_FIXED_VALUE
	 * @generated
	 * @ordered
	 */
	SC_FIXED(0, "SC_FIXED", "SC_FIXED"),

	/**
	 * The '<em><b>AC FIXED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AC_FIXED_VALUE
	 * @generated
	 * @ordered
	 */
	AC_FIXED(1, "AC_FIXED", "AC_FIXED"), /**
	 * The '<em><b>CINLINE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CINLINE_VALUE
	 * @generated
	 * @ordered
	 */
	CINLINE(2, "C_INLINE", "C_INLINE"), /**
	 * The '<em><b>CMACROS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CMACROS_VALUE
	 * @generated
	 * @ordered
	 */
	CMACROS(3, "C_MACROS", "C_MACROS");

	/**
	 * The '<em><b>SC FIXED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SC FIXED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SC_FIXED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SC_FIXED_VALUE = 0;

	/**
	 * The '<em><b>AC FIXED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>AC FIXED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AC_FIXED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int AC_FIXED_VALUE = 1;

	/**
	 * The '<em><b>CINLINE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>CINLINE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CINLINE
	 * @model name="C_INLINE"
	 * @generated
	 * @ordered
	 */
	public static final int CINLINE_VALUE = 2;

	/**
	 * The '<em><b>CMACROS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>CMACROS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CMACROS
	 * @model name="C_MACROS"
	 * @generated
	 * @ordered
	 */
	public static final int CMACROS_VALUE = 3;

	/**
	 * An array of all the '<em><b>DATA TYPE REGENERATION</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final DATA_TYPE_REGENERATION[] VALUES_ARRAY =
		new DATA_TYPE_REGENERATION[] {
			SC_FIXED,
			AC_FIXED,
			CINLINE,
			CMACROS,
		};

	/**
	 * A public read-only list of all the '<em><b>DATA TYPE REGENERATION</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<DATA_TYPE_REGENERATION> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>DATA TYPE REGENERATION</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DATA_TYPE_REGENERATION get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DATA_TYPE_REGENERATION result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>DATA TYPE REGENERATION</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DATA_TYPE_REGENERATION getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DATA_TYPE_REGENERATION result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>DATA TYPE REGENERATION</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DATA_TYPE_REGENERATION get(int value) {
		switch (value) {
			case SC_FIXED_VALUE: return SC_FIXED;
			case AC_FIXED_VALUE: return AC_FIXED;
			case CINLINE_VALUE: return CINLINE;
			case CMACROS_VALUE: return CMACROS;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DATA_TYPE_REGENERATION(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //DATA_TYPE_REGENERATION
