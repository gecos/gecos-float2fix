/**
 */
package fr.irisa.cairn.idfix.model.fixedpointspecification.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Data;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationFactory;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage;
import fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Operation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind;
import fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FixedpointspecificationFactoryImpl extends EFactoryImpl implements FixedpointspecificationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FixedpointspecificationFactory init() {
		try {
			FixedpointspecificationFactory theFixedpointspecificationFactory = (FixedpointspecificationFactory)EPackage.Registry.INSTANCE.getEFactory(FixedpointspecificationPackage.eNS_URI);
			if (theFixedpointspecificationFactory != null) {
				return theFixedpointspecificationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new FixedpointspecificationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixedpointspecificationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case FixedpointspecificationPackage.FIXED_POINT_SPECIFICATION: return createFixedPointSpecification();
			case FixedpointspecificationPackage.DATA: return createData();
			case FixedpointspecificationPackage.OPERATOR: return createOperator();
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION: return createFixedPointInformation();
			case FixedpointspecificationPackage.DYNAMIC: return createDynamic();
			case FixedpointspecificationPackage.CDFG_EDGE: return createCDFGEdge();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case FixedpointspecificationPackage.OVERFLOW_TYPE:
				return createOVERFLOW_TYPEFromString(eDataType, initialValue);
			case FixedpointspecificationPackage.QUANTIFICATION_TYPE:
				return createQUANTIFICATION_TYPEFromString(eDataType, initialValue);
			case FixedpointspecificationPackage.OPERATOR_KIND:
				return createOperatorKindFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case FixedpointspecificationPackage.OVERFLOW_TYPE:
				return convertOVERFLOW_TYPEToString(eDataType, instanceValue);
			case FixedpointspecificationPackage.QUANTIFICATION_TYPE:
				return convertQUANTIFICATION_TYPEToString(eDataType, instanceValue);
			case FixedpointspecificationPackage.OPERATOR_KIND:
				return convertOperatorKindToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixedPointSpecification createFixedPointSpecification() {
		FixedPointSpecificationImpl fixedPointSpecification = new FixedPointSpecificationImpl();
		return fixedPointSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Data createData() {
		DataImpl data = new DataImpl();
		return data;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation createOperator() {
		OperationImpl operator = new OperationImpl();
		return operator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixedPointInformation createFixedPointInformation() {
		FixedPointInformationImpl fixedPointInformation = new FixedPointInformationImpl();
		return fixedPointInformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic createDynamic() {
		DynamicImpl dynamic = new DynamicImpl();
		return dynamic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CDFGEdge createCDFGEdge() {
		CDFGEdgeImpl cdfgEdge = new CDFGEdgeImpl();
		return cdfgEdge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OVERFLOW_TYPE createOVERFLOW_TYPEFromString(EDataType eDataType, String initialValue) {
		OVERFLOW_TYPE result = OVERFLOW_TYPE.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOVERFLOW_TYPEToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QUANTIFICATION_TYPE createQUANTIFICATION_TYPEFromString(EDataType eDataType, String initialValue) {
		QUANTIFICATION_TYPE result = QUANTIFICATION_TYPE.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertQUANTIFICATION_TYPEToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatorKind createOperatorKindFromString(EDataType eDataType, String initialValue) {
		OperatorKind result = OperatorKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOperatorKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixedpointspecificationPackage getFixedpointspecificationPackage() {
		return (FixedpointspecificationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static FixedpointspecificationPackage getPackage() {
		return FixedpointspecificationPackage.eINSTANCE;
	}

} //FixedpointspecificationFactoryImpl
