/**
 */
package fr.irisa.cairn.idfix.model.fixedpointspecification.impl;

import float2fix.Float2fixPackage;
import fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge;
import fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Data;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationFactory;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Operation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixprojectPackage;
import fr.irisa.cairn.idfix.model.idfixproject.impl.IdfixprojectPackageImpl;
import fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage;
import fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationandtimingPackageImpl;
import fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryPackage;
import fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorlibraryPackageImpl;
import fr.irisa.cairn.idfix.model.solutionspace.SolutionspacePackage;
import fr.irisa.cairn.idfix.model.solutionspace.impl.SolutionspacePackageImpl;
import fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage;
import fr.irisa.cairn.idfix.model.userconfiguration.impl.UserconfigurationPackageImpl;
import gecos.annotations.AnnotationsPackage;
import gecos.blocks.BlocksPackage;
import gecos.core.CorePackage;
import gecos.dag.DagPackage;
import gecos.gecosproject.GecosprojectPackage;
import gecos.instrs.InstrsPackage;
import gecos.types.TypesPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FixedpointspecificationPackageImpl extends EPackageImpl implements FixedpointspecificationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fixedPointSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fixedPointInformationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dynamicEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cdfgNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cdfgEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum overfloW_TYPEEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum quantificatioN_TYPEEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum operatorKindEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private FixedpointspecificationPackageImpl() {
		super(eNS_URI, FixedpointspecificationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link FixedpointspecificationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static FixedpointspecificationPackage init() {
		if (isInited) return (FixedpointspecificationPackage)EPackage.Registry.INSTANCE.getEPackage(FixedpointspecificationPackage.eNS_URI);

		// Obtain or create and register package
		FixedpointspecificationPackageImpl theFixedpointspecificationPackage = (FixedpointspecificationPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof FixedpointspecificationPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new FixedpointspecificationPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		Float2fixPackage.eINSTANCE.eClass();
		AnnotationsPackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		InstrsPackage.eINSTANCE.eClass();
		BlocksPackage.eINSTANCE.eClass();
		CorePackage.eINSTANCE.eClass();
		DagPackage.eINSTANCE.eClass();
		GecosprojectPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		SolutionspacePackageImpl theSolutionspacePackage = (SolutionspacePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SolutionspacePackage.eNS_URI) instanceof SolutionspacePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SolutionspacePackage.eNS_URI) : SolutionspacePackage.eINSTANCE);
		IdfixprojectPackageImpl theIdfixprojectPackage = (IdfixprojectPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(IdfixprojectPackage.eNS_URI) instanceof IdfixprojectPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(IdfixprojectPackage.eNS_URI) : IdfixprojectPackage.eINSTANCE);
		InformationandtimingPackageImpl theInformationandtimingPackage = (InformationandtimingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(InformationandtimingPackage.eNS_URI) instanceof InformationandtimingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(InformationandtimingPackage.eNS_URI) : InformationandtimingPackage.eINSTANCE);
		UserconfigurationPackageImpl theUserconfigurationPackage = (UserconfigurationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UserconfigurationPackage.eNS_URI) instanceof UserconfigurationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UserconfigurationPackage.eNS_URI) : UserconfigurationPackage.eINSTANCE);
		OperatorlibraryPackageImpl theOperatorlibraryPackage = (OperatorlibraryPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OperatorlibraryPackage.eNS_URI) instanceof OperatorlibraryPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OperatorlibraryPackage.eNS_URI) : OperatorlibraryPackage.eINSTANCE);

		// Create package meta-data objects
		theFixedpointspecificationPackage.createPackageContents();
		theSolutionspacePackage.createPackageContents();
		theIdfixprojectPackage.createPackageContents();
		theInformationandtimingPackage.createPackageContents();
		theUserconfigurationPackage.createPackageContents();
		theOperatorlibraryPackage.createPackageContents();

		// Initialize created meta-data
		theFixedpointspecificationPackage.initializePackageContents();
		theSolutionspacePackage.initializePackageContents();
		theIdfixprojectPackage.initializePackageContents();
		theInformationandtimingPackage.initializePackageContents();
		theUserconfigurationPackage.initializePackageContents();
		theOperatorlibraryPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theFixedpointspecificationPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(FixedpointspecificationPackage.eNS_URI, theFixedpointspecificationPackage);
		return theFixedpointspecificationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFixedPointSpecification() {
		return fixedPointSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFixedPointSpecification_Edges() {
		return (EReference)fixedPointSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFixedPointSpecification_Datas() {
		return (EReference)fixedPointSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFixedPointSpecification_Operators() {
		return (EReference)fixedPointSpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFixedPointSpecification__UpdateDynamicInformation__String() {
		return fixedPointSpecificationEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFixedPointSpecification__FindOperatorFromEvalCDFG__int() {
		return fixedPointSpecificationEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFixedPointSpecification__FindOperatorFromConvCDFG__int() {
		return fixedPointSpecificationEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFixedPointSpecification__FindOperatorFromInstruction__Instruction() {
		return fixedPointSpecificationEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFixedPointSpecification__FindDataFromCDFG__int() {
		return fixedPointSpecificationEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFixedPointSpecification__FindDataFromSymbol__Symbol() {
		return fixedPointSpecificationEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFixedPointSpecification__UpdateDataFixedInformation() {
		return fixedPointSpecificationEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFixedPointSpecification__CheckValidity() {
		return fixedPointSpecificationEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getData() {
		return dataEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getData_FixedInformation() {
		return (EReference)dataEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getData_CorrespondingSymbol() {
		return (EReference)dataEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getData_CDFGNumber() {
		return (EAttribute)dataEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getData__GetSuccessorOperators() {
		return dataEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getData__GetPredecessorOperators() {
		return dataEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperator() {
		return operatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperator_Operands() {
		return (EReference)operatorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperator_Eval_cdfg_number() {
		return (EAttribute)operatorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperator_Conv_cdfg_number() {
		return (EAttribute)operatorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperator_Kind() {
		return (EAttribute)operatorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperator_Cost() {
		return (EAttribute)operatorEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperator_CorrespondingInstructions() {
		return (EReference)operatorEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperator_UsageNumber() {
		return (EAttribute)operatorEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperator_Instances() {
		return (EReference)operatorEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getOperator__SetInstance__int() {
		return operatorEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getOperator__HaveNextInstance() {
		return operatorEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getOperator__GetMaxInstance() {
		return operatorEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getOperator__GetCurrentInstance() {
		return operatorEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getOperator__NextInstance() {
		return operatorEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getOperator__PreviousInstance() {
		return operatorEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFixedPointInformation() {
		return fixedPointInformationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixedPointInformation_FixedBitWidth() {
		return (EAttribute)fixedPointInformationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixedPointInformation_BitWidth() {
		return (EAttribute)fixedPointInformationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixedPointInformation_IntegerWidth() {
		return (EAttribute)fixedPointInformationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixedPointInformation_Signed() {
		return (EAttribute)fixedPointInformationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixedPointInformation_Overflow() {
		return (EAttribute)fixedPointInformationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixedPointInformation_Quantification() {
		return (EAttribute)fixedPointInformationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFixedPointInformation_Dynamic() {
		return (EReference)fixedPointInformationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixedPointInformation_DynamicIntegerWidth() {
		return (EAttribute)fixedPointInformationEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFixedPointInformation__GetFractionalWidth() {
		return fixedPointInformationEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDynamic() {
		return dynamicEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDynamic_Min() {
		return (EAttribute)dynamicEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDynamic_Max() {
		return (EAttribute)dynamicEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCDFGNode() {
		return cdfgNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCDFGNode_Successors() {
		return (EReference)cdfgNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCDFGNode_Predecessors() {
		return (EReference)cdfgNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCDFGNode_SfgNodeInstances() {
		return (EReference)cdfgNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCDFGNode__GetOutputInfo() {
		return cdfgNodeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCDFGNode__GetName() {
		return cdfgNodeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCDFGEdge() {
		return cdfgEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCDFGEdge_Predecessor() {
		return (EReference)cdfgEdgeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCDFGEdge_Successor() {
		return (EReference)cdfgEdgeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCDFGEdge_PredIndex() {
		return (EAttribute)cdfgEdgeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCDFGEdge__GetPredecessorInfo() {
		return cdfgEdgeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCDFGEdge__GetSuccessorInfo() {
		return cdfgEdgeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getOVERFLOW_TYPE() {
		return overfloW_TYPEEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getQUANTIFICATION_TYPE() {
		return quantificatioN_TYPEEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getOperatorKind() {
		return operatorKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixedpointspecificationFactory getFixedpointspecificationFactory() {
		return (FixedpointspecificationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		fixedPointSpecificationEClass = createEClass(FIXED_POINT_SPECIFICATION);
		createEReference(fixedPointSpecificationEClass, FIXED_POINT_SPECIFICATION__EDGES);
		createEReference(fixedPointSpecificationEClass, FIXED_POINT_SPECIFICATION__DATAS);
		createEReference(fixedPointSpecificationEClass, FIXED_POINT_SPECIFICATION__OPERATORS);
		createEOperation(fixedPointSpecificationEClass, FIXED_POINT_SPECIFICATION___UPDATE_DYNAMIC_INFORMATION__STRING);
		createEOperation(fixedPointSpecificationEClass, FIXED_POINT_SPECIFICATION___FIND_OPERATOR_FROM_EVAL_CDFG__INT);
		createEOperation(fixedPointSpecificationEClass, FIXED_POINT_SPECIFICATION___FIND_OPERATOR_FROM_CONV_CDFG__INT);
		createEOperation(fixedPointSpecificationEClass, FIXED_POINT_SPECIFICATION___FIND_OPERATOR_FROM_INSTRUCTION__INSTRUCTION);
		createEOperation(fixedPointSpecificationEClass, FIXED_POINT_SPECIFICATION___FIND_DATA_FROM_CDFG__INT);
		createEOperation(fixedPointSpecificationEClass, FIXED_POINT_SPECIFICATION___FIND_DATA_FROM_SYMBOL__SYMBOL);
		createEOperation(fixedPointSpecificationEClass, FIXED_POINT_SPECIFICATION___UPDATE_DATA_FIXED_INFORMATION);
		createEOperation(fixedPointSpecificationEClass, FIXED_POINT_SPECIFICATION___CHECK_VALIDITY);

		dataEClass = createEClass(DATA);
		createEReference(dataEClass, DATA__FIXED_INFORMATION);
		createEReference(dataEClass, DATA__CORRESPONDING_SYMBOL);
		createEAttribute(dataEClass, DATA__CDFG_NUMBER);
		createEOperation(dataEClass, DATA___GET_SUCCESSOR_OPERATORS);
		createEOperation(dataEClass, DATA___GET_PREDECESSOR_OPERATORS);

		operatorEClass = createEClass(OPERATOR);
		createEReference(operatorEClass, OPERATOR__OPERANDS);
		createEAttribute(operatorEClass, OPERATOR__EVAL_CDFG_NUMBER);
		createEAttribute(operatorEClass, OPERATOR__CONV_CDFG_NUMBER);
		createEAttribute(operatorEClass, OPERATOR__KIND);
		createEAttribute(operatorEClass, OPERATOR__COST);
		createEReference(operatorEClass, OPERATOR__CORRESPONDING_INSTRUCTIONS);
		createEAttribute(operatorEClass, OPERATOR__USAGE_NUMBER);
		createEReference(operatorEClass, OPERATOR__INSTANCES);
		createEOperation(operatorEClass, OPERATOR___SET_INSTANCE__INT);
		createEOperation(operatorEClass, OPERATOR___HAVE_NEXT_INSTANCE);
		createEOperation(operatorEClass, OPERATOR___GET_MAX_INSTANCE);
		createEOperation(operatorEClass, OPERATOR___GET_CURRENT_INSTANCE);
		createEOperation(operatorEClass, OPERATOR___NEXT_INSTANCE);
		createEOperation(operatorEClass, OPERATOR___PREVIOUS_INSTANCE);

		fixedPointInformationEClass = createEClass(FIXED_POINT_INFORMATION);
		createEAttribute(fixedPointInformationEClass, FIXED_POINT_INFORMATION__FIXED_BIT_WIDTH);
		createEAttribute(fixedPointInformationEClass, FIXED_POINT_INFORMATION__SIGNED);
		createEAttribute(fixedPointInformationEClass, FIXED_POINT_INFORMATION__BIT_WIDTH);
		createEAttribute(fixedPointInformationEClass, FIXED_POINT_INFORMATION__INTEGER_WIDTH);
		createEAttribute(fixedPointInformationEClass, FIXED_POINT_INFORMATION__OVERFLOW);
		createEAttribute(fixedPointInformationEClass, FIXED_POINT_INFORMATION__QUANTIFICATION);
		createEReference(fixedPointInformationEClass, FIXED_POINT_INFORMATION__DYNAMIC);
		createEAttribute(fixedPointInformationEClass, FIXED_POINT_INFORMATION__DYNAMIC_INTEGER_WIDTH);
		createEOperation(fixedPointInformationEClass, FIXED_POINT_INFORMATION___GET_FRACTIONAL_WIDTH);

		dynamicEClass = createEClass(DYNAMIC);
		createEAttribute(dynamicEClass, DYNAMIC__MIN);
		createEAttribute(dynamicEClass, DYNAMIC__MAX);

		cdfgNodeEClass = createEClass(CDFG_NODE);
		createEReference(cdfgNodeEClass, CDFG_NODE__SUCCESSORS);
		createEReference(cdfgNodeEClass, CDFG_NODE__PREDECESSORS);
		createEReference(cdfgNodeEClass, CDFG_NODE__SFG_NODE_INSTANCES);
		createEOperation(cdfgNodeEClass, CDFG_NODE___GET_OUTPUT_INFO);
		createEOperation(cdfgNodeEClass, CDFG_NODE___GET_NAME);

		cdfgEdgeEClass = createEClass(CDFG_EDGE);
		createEReference(cdfgEdgeEClass, CDFG_EDGE__PREDECESSOR);
		createEReference(cdfgEdgeEClass, CDFG_EDGE__SUCCESSOR);
		createEAttribute(cdfgEdgeEClass, CDFG_EDGE__PRED_INDEX);
		createEOperation(cdfgEdgeEClass, CDFG_EDGE___GET_PREDECESSOR_INFO);
		createEOperation(cdfgEdgeEClass, CDFG_EDGE___GET_SUCCESSOR_INFO);

		// Create enums
		overfloW_TYPEEEnum = createEEnum(OVERFLOW_TYPE);
		quantificatioN_TYPEEEnum = createEEnum(QUANTIFICATION_TYPE);
		operatorKindEEnum = createEEnum(OPERATOR_KIND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		InstrsPackage theInstrsPackage = (InstrsPackage)EPackage.Registry.INSTANCE.getEPackage(InstrsPackage.eNS_URI);
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		OperatorlibraryPackage theOperatorlibraryPackage = (OperatorlibraryPackage)EPackage.Registry.INSTANCE.getEPackage(OperatorlibraryPackage.eNS_URI);
		Float2fixPackage theFloat2fixPackage = (Float2fixPackage)EPackage.Registry.INSTANCE.getEPackage(Float2fixPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		dataEClass.getESuperTypes().add(this.getCDFGNode());
		operatorEClass.getESuperTypes().add(this.getCDFGNode());

		// Initialize classes, features, and operations; add parameters
		initEClass(fixedPointSpecificationEClass, FixedPointSpecification.class, "FixedPointSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFixedPointSpecification_Edges(), this.getCDFGEdge(), null, "edges", null, 0, -1, FixedPointSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFixedPointSpecification_Datas(), this.getData(), null, "datas", null, 0, -1, FixedPointSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFixedPointSpecification_Operators(), this.getOperator(), null, "operators", null, 0, -1, FixedPointSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getFixedPointSpecification__UpdateDynamicInformation__String(), null, "updateDynamicInformation", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "dynamicInformation", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getFixedPointSpecification__FindOperatorFromEvalCDFG__int(), this.getOperator(), "findOperatorFromEvalCDFG", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "eval_cdfg", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getFixedPointSpecification__FindOperatorFromConvCDFG__int(), this.getOperator(), "findOperatorFromConvCDFG", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "conv_cdfg", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getFixedPointSpecification__FindOperatorFromInstruction__Instruction(), this.getOperator(), "findOperatorFromInstruction", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theInstrsPackage.getInstruction(), "instruction", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getFixedPointSpecification__FindDataFromCDFG__int(), this.getData(), "findDataFromCDFG", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "cdfg", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getFixedPointSpecification__FindDataFromSymbol__Symbol(), this.getData(), "findDataFromSymbol", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCorePackage.getSymbol(), "symbol", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getFixedPointSpecification__UpdateDataFixedInformation(), null, "updateDataFixedInformation", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getFixedPointSpecification__CheckValidity(), null, "checkValidity", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(dataEClass, Data.class, "Data", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getData_FixedInformation(), this.getFixedPointInformation(), null, "fixedInformation", null, 0, 1, Data.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getData_CorrespondingSymbol(), theCorePackage.getSymbol(), null, "correspondingSymbol", null, 0, 1, Data.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getData_CDFGNumber(), ecorePackage.getEInt(), "CDFGNumber", null, 0, 1, Data.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getData__GetSuccessorOperators(), this.getOperator(), "getSuccessorOperators", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getData__GetPredecessorOperators(), this.getOperator(), "getPredecessorOperators", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEClass(operatorEClass, Operation.class, "Operator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperator_Operands(), this.getFixedPointInformation(), null, "operands", null, 0, 3, Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperator_Eval_cdfg_number(), ecorePackage.getEInt(), "eval_cdfg_number", null, 0, 1, Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperator_Conv_cdfg_number(), ecorePackage.getEInt(), "conv_cdfg_number", null, 0, 1, Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperator_Kind(), this.getOperatorKind(), "kind", null, 0, 1, Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperator_Cost(), ecorePackage.getEFloat(), "cost", null, 0, 1, Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperator_CorrespondingInstructions(), theInstrsPackage.getInstruction(), null, "correspondingInstructions", null, 0, -1, Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperator_UsageNumber(), ecorePackage.getEInt(), "usageNumber", null, 0, 1, Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperator_Instances(), theOperatorlibraryPackage.getInstance(), null, "instances", null, 0, -1, Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getOperator__SetInstance__int(), null, "setInstance", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "index", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getOperator__HaveNextInstance(), ecorePackage.getEBoolean(), "haveNextInstance", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getOperator__GetMaxInstance(), ecorePackage.getEInt(), "getMaxInstance", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getOperator__GetCurrentInstance(), ecorePackage.getEInt(), "getCurrentInstance", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getOperator__NextInstance(), null, "nextInstance", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getOperator__PreviousInstance(), null, "previousInstance", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(fixedPointInformationEClass, FixedPointInformation.class, "FixedPointInformation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFixedPointInformation_FixedBitWidth(), ecorePackage.getEBoolean(), "fixedBitWidth", null, 0, 1, FixedPointInformation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFixedPointInformation_Signed(), ecorePackage.getEBoolean(), "signed", null, 0, 1, FixedPointInformation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFixedPointInformation_BitWidth(), ecorePackage.getEInt(), "bitWidth", null, 0, 1, FixedPointInformation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFixedPointInformation_IntegerWidth(), ecorePackage.getEInt(), "integerWidth", null, 0, 1, FixedPointInformation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFixedPointInformation_Overflow(), this.getOVERFLOW_TYPE(), "overflow", null, 0, 1, FixedPointInformation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFixedPointInformation_Quantification(), this.getQUANTIFICATION_TYPE(), "quantification", null, 0, 1, FixedPointInformation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFixedPointInformation_Dynamic(), this.getDynamic(), null, "dynamic", null, 0, 1, FixedPointInformation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFixedPointInformation_DynamicIntegerWidth(), ecorePackage.getEInt(), "dynamicIntegerWidth", null, 0, 1, FixedPointInformation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getFixedPointInformation__GetFractionalWidth(), ecorePackage.getEInt(), "getFractionalWidth", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(dynamicEClass, fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic.class, "Dynamic", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDynamic_Min(), ecorePackage.getEDouble(), "min", null, 0, 1, fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDynamic_Max(), ecorePackage.getEDouble(), "max", null, 0, 1, fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cdfgNodeEClass, CDFGNode.class, "CDFGNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCDFGNode_Successors(), this.getCDFGEdge(), this.getCDFGEdge_Predecessor(), "successors", null, 0, -1, CDFGNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCDFGNode_Predecessors(), this.getCDFGEdge(), this.getCDFGEdge_Successor(), "predecessors", null, 0, -1, CDFGNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCDFGNode_SfgNodeInstances(), theFloat2fixPackage.getNode(), null, "sfgNodeInstances", null, 0, -1, CDFGNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getCDFGNode__GetOutputInfo(), this.getFixedPointInformation(), "getOutputInfo", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getCDFGNode__GetName(), ecorePackage.getEString(), "getName", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(cdfgEdgeEClass, CDFGEdge.class, "CDFGEdge", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCDFGEdge_Predecessor(), this.getCDFGNode(), this.getCDFGNode_Successors(), "predecessor", null, 0, 1, CDFGEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCDFGEdge_Successor(), this.getCDFGNode(), this.getCDFGNode_Predecessors(), "successor", null, 0, 1, CDFGEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCDFGEdge_PredIndex(), ecorePackage.getEInt(), "predIndex", null, 0, 1, CDFGEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getCDFGEdge__GetPredecessorInfo(), this.getFixedPointInformation(), "getPredecessorInfo", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getCDFGEdge__GetSuccessorInfo(), this.getFixedPointInformation(), "getSuccessorInfo", 0, 1, IS_UNIQUE, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(overfloW_TYPEEEnum, fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE.class, "OVERFLOW_TYPE");
		addEEnumLiteral(overfloW_TYPEEEnum, fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE.SATURATION);
		addEEnumLiteral(overfloW_TYPEEEnum, fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE.WRAP);

		initEEnum(quantificatioN_TYPEEEnum, fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE.class, "QUANTIFICATION_TYPE");
		addEEnumLiteral(quantificatioN_TYPEEEnum, fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE.TRUNCATION);
		addEEnumLiteral(quantificatioN_TYPEEEnum, fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE.ROUNDING);

		initEEnum(operatorKindEEnum, OperatorKind.class, "OperatorKind");
		addEEnumLiteral(operatorKindEEnum, OperatorKind.ADD);
		addEEnumLiteral(operatorKindEEnum, OperatorKind.SUB);
		addEEnumLiteral(operatorKindEEnum, OperatorKind.MUL);
		addEEnumLiteral(operatorKindEEnum, OperatorKind.DIV);
		addEEnumLiteral(operatorKindEEnum, OperatorKind.SET);
		addEEnumLiteral(operatorKindEEnum, OperatorKind.NEG);
		addEEnumLiteral(operatorKindEEnum, OperatorKind.SQRT);
		addEEnumLiteral(operatorKindEEnum, OperatorKind.SHR);
		addEEnumLiteral(operatorKindEEnum, OperatorKind.SHL);

		// Create resource
		createResource(eNS_URI);
	}

} //FixedpointspecificationPackageImpl
