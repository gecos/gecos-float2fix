/**
 */
package fr.irisa.cairn.idfix.model.solutionspace;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionspacePackage
 * @generated
 */
public interface SolutionspaceFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SolutionspaceFactory eINSTANCE = fr.irisa.cairn.idfix.model.solutionspace.impl.SolutionspaceFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Triplet</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Triplet</em>'.
	 * @generated
	 */
	Triplet createTriplet();

	/**
	 * Returns a new object of class '<em>Solution Space</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Solution Space</em>'.
	 * @generated
	 */
	SolutionSpace createSolutionSpace();

	/**
	 * Returns a new object of class '<em>Library Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Library Element</em>'.
	 * @generated
	 */
	LibraryElement createLibraryElement();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	SolutionspacePackage getSolutionspacePackage();

} //SolutionspaceFactory
