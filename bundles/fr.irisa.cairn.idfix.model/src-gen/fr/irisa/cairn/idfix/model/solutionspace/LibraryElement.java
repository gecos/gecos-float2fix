/**
 */
package fr.irisa.cairn.idfix.model.solutionspace;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Library Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.solutionspace.LibraryElement#getOperand <em>Operand</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.solutionspace.LibraryElement#getCost <em>Cost</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionspacePackage#getLibraryElement()
 * @model
 * @generated
 */
public interface LibraryElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operand</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operand</em>' containment reference.
	 * @see #setOperand(Triplet)
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionspacePackage#getLibraryElement_Operand()
	 * @model containment="true"
	 * @generated
	 */
	Triplet getOperand();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.solutionspace.LibraryElement#getOperand <em>Operand</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operand</em>' containment reference.
	 * @see #getOperand()
	 * @generated
	 */
	void setOperand(Triplet value);

	/**
	 * Returns the value of the '<em><b>Cost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cost</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cost</em>' attribute.
	 * @see #setCost(float)
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionspacePackage#getLibraryElement_Cost()
	 * @model
	 * @generated
	 */
	float getCost();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.solutionspace.LibraryElement#getCost <em>Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cost</em>' attribute.
	 * @see #getCost()
	 * @generated
	 */
	void setCost(float value);

} // LibraryElement
