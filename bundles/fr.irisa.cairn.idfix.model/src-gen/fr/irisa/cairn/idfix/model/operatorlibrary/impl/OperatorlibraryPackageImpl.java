/**
 */
package fr.irisa.cairn.idfix.model.operatorlibrary.impl;

import float2fix.Float2fixPackage;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage;

import fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl;

import fr.irisa.cairn.idfix.model.idfixproject.IdfixprojectPackage;

import fr.irisa.cairn.idfix.model.idfixproject.impl.IdfixprojectPackageImpl;

import fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage;

import fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationandtimingPackageImpl;

import fr.irisa.cairn.idfix.model.operatorlibrary.Characteristic;
import fr.irisa.cairn.idfix.model.operatorlibrary.Instance;
import fr.irisa.cairn.idfix.model.operatorlibrary.Operator;
import fr.irisa.cairn.idfix.model.operatorlibrary.OperatorLibrary;
import fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryFactory;
import fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryPackage;
import fr.irisa.cairn.idfix.model.operatorlibrary.Triplet;

import fr.irisa.cairn.idfix.model.solutionspace.SolutionspacePackage;

import fr.irisa.cairn.idfix.model.solutionspace.impl.SolutionspacePackageImpl;

import fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage;

import fr.irisa.cairn.idfix.model.userconfiguration.impl.UserconfigurationPackageImpl;

import gecos.annotations.AnnotationsPackage;
import gecos.blocks.BlocksPackage;
import gecos.core.CorePackage;
import gecos.dag.DagPackage;
import gecos.gecosproject.GecosprojectPackage;
import gecos.instrs.InstrsPackage;
import gecos.types.TypesPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OperatorlibraryPackageImpl extends EPackageImpl implements OperatorlibraryPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operatorLibraryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass instanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tripletEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass characteristicEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum operatoR_KINDEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OperatorlibraryPackageImpl() {
		super(eNS_URI, OperatorlibraryFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link OperatorlibraryPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OperatorlibraryPackage init() {
		if (isInited) return (OperatorlibraryPackage)EPackage.Registry.INSTANCE.getEPackage(OperatorlibraryPackage.eNS_URI);

		// Obtain or create and register package
		OperatorlibraryPackageImpl theOperatorlibraryPackage = (OperatorlibraryPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof OperatorlibraryPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new OperatorlibraryPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		Float2fixPackage.eINSTANCE.eClass();
		AnnotationsPackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		InstrsPackage.eINSTANCE.eClass();
		BlocksPackage.eINSTANCE.eClass();
		CorePackage.eINSTANCE.eClass();
		DagPackage.eINSTANCE.eClass();
		GecosprojectPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		SolutionspacePackageImpl theSolutionspacePackage = (SolutionspacePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SolutionspacePackage.eNS_URI) instanceof SolutionspacePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SolutionspacePackage.eNS_URI) : SolutionspacePackage.eINSTANCE);
		FixedpointspecificationPackageImpl theFixedpointspecificationPackage = (FixedpointspecificationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FixedpointspecificationPackage.eNS_URI) instanceof FixedpointspecificationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FixedpointspecificationPackage.eNS_URI) : FixedpointspecificationPackage.eINSTANCE);
		IdfixprojectPackageImpl theIdfixprojectPackage = (IdfixprojectPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(IdfixprojectPackage.eNS_URI) instanceof IdfixprojectPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(IdfixprojectPackage.eNS_URI) : IdfixprojectPackage.eINSTANCE);
		InformationandtimingPackageImpl theInformationandtimingPackage = (InformationandtimingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(InformationandtimingPackage.eNS_URI) instanceof InformationandtimingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(InformationandtimingPackage.eNS_URI) : InformationandtimingPackage.eINSTANCE);
		UserconfigurationPackageImpl theUserconfigurationPackage = (UserconfigurationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UserconfigurationPackage.eNS_URI) instanceof UserconfigurationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UserconfigurationPackage.eNS_URI) : UserconfigurationPackage.eINSTANCE);

		// Create package meta-data objects
		theOperatorlibraryPackage.createPackageContents();
		theSolutionspacePackage.createPackageContents();
		theFixedpointspecificationPackage.createPackageContents();
		theIdfixprojectPackage.createPackageContents();
		theInformationandtimingPackage.createPackageContents();
		theUserconfigurationPackage.createPackageContents();

		// Initialize created meta-data
		theOperatorlibraryPackage.initializePackageContents();
		theSolutionspacePackage.initializePackageContents();
		theFixedpointspecificationPackage.initializePackageContents();
		theIdfixprojectPackage.initializePackageContents();
		theInformationandtimingPackage.initializePackageContents();
		theUserconfigurationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOperatorlibraryPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OperatorlibraryPackage.eNS_URI, theOperatorlibraryPackage);
		return theOperatorlibraryPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperatorLibrary() {
		return operatorLibraryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperatorLibrary_Operators() {
		return (EReference)operatorLibraryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getOperatorLibrary__GetOperator__OPERATOR_KIND() {
		return operatorLibraryEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperator() {
		return operatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperator_Kind() {
		return (EAttribute)operatorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperator_Intances() {
		return (EReference)operatorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInstance() {
		return instanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInstance_Operands() {
		return (EReference)instanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInstance_Characteristics() {
		return (EReference)instanceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTriplet() {
		return tripletEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTriplet_First() {
		return (EAttribute)tripletEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTriplet_Second() {
		return (EAttribute)tripletEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTriplet_Third() {
		return (EAttribute)tripletEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCharacteristic() {
		return characteristicEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCharacteristic_Name() {
		return (EAttribute)characteristicEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCharacteristic_Value() {
		return (EAttribute)characteristicEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getOPERATOR_KIND() {
		return operatoR_KINDEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatorlibraryFactory getOperatorlibraryFactory() {
		return (OperatorlibraryFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		operatorLibraryEClass = createEClass(OPERATOR_LIBRARY);
		createEReference(operatorLibraryEClass, OPERATOR_LIBRARY__OPERATORS);
		createEOperation(operatorLibraryEClass, OPERATOR_LIBRARY___GET_OPERATOR__OPERATOR_KIND);

		operatorEClass = createEClass(OPERATOR);
		createEAttribute(operatorEClass, OPERATOR__KIND);
		createEReference(operatorEClass, OPERATOR__INTANCES);

		instanceEClass = createEClass(INSTANCE);
		createEReference(instanceEClass, INSTANCE__OPERANDS);
		createEReference(instanceEClass, INSTANCE__CHARACTERISTICS);

		tripletEClass = createEClass(TRIPLET);
		createEAttribute(tripletEClass, TRIPLET__FIRST);
		createEAttribute(tripletEClass, TRIPLET__SECOND);
		createEAttribute(tripletEClass, TRIPLET__THIRD);

		characteristicEClass = createEClass(CHARACTERISTIC);
		createEAttribute(characteristicEClass, CHARACTERISTIC__NAME);
		createEAttribute(characteristicEClass, CHARACTERISTIC__VALUE);

		// Create enums
		operatoR_KINDEEnum = createEEnum(OPERATOR_KIND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(operatorLibraryEClass, OperatorLibrary.class, "OperatorLibrary", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperatorLibrary_Operators(), this.getOperator(), null, "operators", null, 0, -1, OperatorLibrary.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getOperatorLibrary__GetOperator__OPERATOR_KIND(), this.getOperator(), "getOperator", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getOPERATOR_KIND(), "kind", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(operatorEClass, Operator.class, "Operator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOperator_Kind(), this.getOPERATOR_KIND(), "kind", null, 0, 1, Operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperator_Intances(), this.getInstance(), null, "intances", null, 0, -1, Operator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(instanceEClass, Instance.class, "Instance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInstance_Operands(), this.getTriplet(), null, "operands", null, 0, 1, Instance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInstance_Characteristics(), this.getCharacteristic(), null, "characteristics", null, 0, -1, Instance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tripletEClass, Triplet.class, "Triplet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTriplet_First(), ecorePackage.getEInt(), "first", null, 0, 1, Triplet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTriplet_Second(), ecorePackage.getEInt(), "second", null, 0, 1, Triplet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTriplet_Third(), ecorePackage.getEInt(), "third", null, 0, 1, Triplet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(characteristicEClass, Characteristic.class, "Characteristic", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCharacteristic_Name(), ecorePackage.getEString(), "name", null, 0, 1, Characteristic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCharacteristic_Value(), ecorePackage.getEFloat(), "value", null, 0, 1, Characteristic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(operatoR_KINDEEnum, fr.irisa.cairn.idfix.model.operatorlibrary.OPERATOR_KIND.class, "OPERATOR_KIND");
		addEEnumLiteral(operatoR_KINDEEnum, fr.irisa.cairn.idfix.model.operatorlibrary.OPERATOR_KIND.ADD);
		addEEnumLiteral(operatoR_KINDEEnum, fr.irisa.cairn.idfix.model.operatorlibrary.OPERATOR_KIND.SUB);
		addEEnumLiteral(operatoR_KINDEEnum, fr.irisa.cairn.idfix.model.operatorlibrary.OPERATOR_KIND.MUL);
		addEEnumLiteral(operatoR_KINDEEnum, fr.irisa.cairn.idfix.model.operatorlibrary.OPERATOR_KIND.DIV);
		addEEnumLiteral(operatoR_KINDEEnum, fr.irisa.cairn.idfix.model.operatorlibrary.OPERATOR_KIND.SET);

		// Create resource
		createResource(eNS_URI);
	}

} //OperatorlibraryPackageImpl
