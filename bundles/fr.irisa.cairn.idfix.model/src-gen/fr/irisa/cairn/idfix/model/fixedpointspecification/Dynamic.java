/**
 */
package fr.irisa.cairn.idfix.model.fixedpointspecification;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dynamic</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic#getMin <em>Min</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic#getMax <em>Max</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getDynamic()
 * @model
 * @generated
 */
public interface Dynamic extends EObject {
	/**
	 * Returns the value of the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min</em>' attribute.
	 * @see #setMin(double)
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getDynamic_Min()
	 * @model
	 * @generated
	 */
	double getMin();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic#getMin <em>Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min</em>' attribute.
	 * @see #getMin()
	 * @generated
	 */
	void setMin(double value);

	/**
	 * Returns the value of the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max</em>' attribute.
	 * @see #setMax(double)
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getDynamic_Max()
	 * @model
	 * @generated
	 */
	double getMax();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic#getMax <em>Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max</em>' attribute.
	 * @see #getMax()
	 * @generated
	 */
	void setMax(double value);

} // Dynamic
