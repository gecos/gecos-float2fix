/**
 */
package fr.irisa.cairn.idfix.model.operatorlibrary.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import fr.irisa.cairn.idfix.model.operatorlibrary.Instance;
import fr.irisa.cairn.idfix.model.operatorlibrary.OPERATOR_KIND;
import fr.irisa.cairn.idfix.model.operatorlibrary.Operator;
import fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorImpl#getKind <em>Kind</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorImpl#getIntances <em>Intances</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperatorImpl extends MinimalEObjectImpl.Container implements Operator {
	/**
	 * The default value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected static final OPERATOR_KIND KIND_EDEFAULT = OPERATOR_KIND.ADD;

	/**
	 * The cached value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected OPERATOR_KIND kind = KIND_EDEFAULT;

	/**
	 * The cached value of the '{@link #getIntances() <em>Intances</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntances()
	 * @generated
	 * @ordered
	 */
	protected EList<Instance> intances;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OperatorlibraryPackage.Literals.OPERATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPERATOR_KIND getKind() {
		return kind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKind(OPERATOR_KIND newKind) {
		OPERATOR_KIND oldKind = kind;
		kind = newKind == null ? KIND_EDEFAULT : newKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperatorlibraryPackage.OPERATOR__KIND, oldKind, kind));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instance> getIntances() {
		if (intances == null) {
			intances = new EObjectResolvingEList<Instance>(Instance.class, this, OperatorlibraryPackage.OPERATOR__INTANCES);
		}
		return intances;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OperatorlibraryPackage.OPERATOR__KIND:
				return getKind();
			case OperatorlibraryPackage.OPERATOR__INTANCES:
				return getIntances();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OperatorlibraryPackage.OPERATOR__KIND:
				setKind((OPERATOR_KIND)newValue);
				return;
			case OperatorlibraryPackage.OPERATOR__INTANCES:
				getIntances().clear();
				getIntances().addAll((Collection<? extends Instance>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OperatorlibraryPackage.OPERATOR__KIND:
				setKind(KIND_EDEFAULT);
				return;
			case OperatorlibraryPackage.OPERATOR__INTANCES:
				getIntances().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OperatorlibraryPackage.OPERATOR__KIND:
				return kind != KIND_EDEFAULT;
			case OperatorlibraryPackage.OPERATOR__INTANCES:
				return intances != null && !intances.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer("Operator ("
				+ kind + ") -> ");
		result.append("\n");
		for(Instance c : this.getIntances()){
			result.append("   " + c.toString());
			result.append("\n");
		}
		return result.toString();
	}

} //OperatorImpl
