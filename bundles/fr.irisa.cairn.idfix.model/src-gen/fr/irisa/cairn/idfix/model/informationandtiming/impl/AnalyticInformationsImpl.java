/**
 */
package fr.irisa.cairn.idfix.model.informationandtiming.impl;

import fr.irisa.cairn.idfix.model.informationandtiming.AnalyticInformations;
import fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Analytic Informations</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.AnalyticInformationsImpl#getFinalNoiseConstraint <em>Final Noise Constraint</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.AnalyticInformationsImpl#getFinalCostConstraint <em>Final Cost Constraint</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.AnalyticInformationsImpl#getNoiseEvalutationNumber <em>Noise Evalutation Number</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.AnalyticInformationsImpl#getCostEvaluationNumber <em>Cost Evaluation Number</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AnalyticInformationsImpl extends MinimalEObjectImpl.Container implements AnalyticInformations {
	/**
	 * The default value of the '{@link #getFinalNoiseConstraint() <em>Final Noise Constraint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalNoiseConstraint()
	 * @generated
	 * @ordered
	 */
	protected static final double FINAL_NOISE_CONSTRAINT_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getFinalNoiseConstraint() <em>Final Noise Constraint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalNoiseConstraint()
	 * @generated
	 * @ordered
	 */
	protected double finalNoiseConstraint = FINAL_NOISE_CONSTRAINT_EDEFAULT;

	/**
	 * The default value of the '{@link #getFinalCostConstraint() <em>Final Cost Constraint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalCostConstraint()
	 * @generated
	 * @ordered
	 */
	protected static final float FINAL_COST_CONSTRAINT_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getFinalCostConstraint() <em>Final Cost Constraint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalCostConstraint()
	 * @generated
	 * @ordered
	 */
	protected float finalCostConstraint = FINAL_COST_CONSTRAINT_EDEFAULT;

	/**
	 * The default value of the '{@link #getNoiseEvalutationNumber() <em>Noise Evalutation Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoiseEvalutationNumber()
	 * @generated
	 * @ordered
	 */
	protected static final int NOISE_EVALUTATION_NUMBER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNoiseEvalutationNumber() <em>Noise Evalutation Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoiseEvalutationNumber()
	 * @generated
	 * @ordered
	 */
	protected int noiseEvalutationNumber = NOISE_EVALUTATION_NUMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #getCostEvaluationNumber() <em>Cost Evaluation Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCostEvaluationNumber()
	 * @generated
	 * @ordered
	 */
	protected static final int COST_EVALUATION_NUMBER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCostEvaluationNumber() <em>Cost Evaluation Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCostEvaluationNumber()
	 * @generated
	 * @ordered
	 */
	protected int costEvaluationNumber = COST_EVALUATION_NUMBER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnalyticInformationsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InformationandtimingPackage.Literals.ANALYTIC_INFORMATIONS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getFinalNoiseConstraint() {
		return finalNoiseConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFinalNoiseConstraint(double newFinalNoiseConstraint) {
		double oldFinalNoiseConstraint = finalNoiseConstraint;
		finalNoiseConstraint = newFinalNoiseConstraint;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InformationandtimingPackage.ANALYTIC_INFORMATIONS__FINAL_NOISE_CONSTRAINT, oldFinalNoiseConstraint, finalNoiseConstraint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getFinalCostConstraint() {
		return finalCostConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFinalCostConstraint(float newFinalCostConstraint) {
		float oldFinalCostConstraint = finalCostConstraint;
		finalCostConstraint = newFinalCostConstraint;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InformationandtimingPackage.ANALYTIC_INFORMATIONS__FINAL_COST_CONSTRAINT, oldFinalCostConstraint, finalCostConstraint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNoiseEvalutationNumber() {
		return noiseEvalutationNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNoiseEvalutationNumber(int newNoiseEvalutationNumber) {
		int oldNoiseEvalutationNumber = noiseEvalutationNumber;
		noiseEvalutationNumber = newNoiseEvalutationNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InformationandtimingPackage.ANALYTIC_INFORMATIONS__NOISE_EVALUTATION_NUMBER, oldNoiseEvalutationNumber, noiseEvalutationNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getCostEvaluationNumber() {
		return costEvaluationNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCostEvaluationNumber(int newCostEvaluationNumber) {
		int oldCostEvaluationNumber = costEvaluationNumber;
		costEvaluationNumber = newCostEvaluationNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InformationandtimingPackage.ANALYTIC_INFORMATIONS__COST_EVALUATION_NUMBER, oldCostEvaluationNumber, costEvaluationNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InformationandtimingPackage.ANALYTIC_INFORMATIONS__FINAL_NOISE_CONSTRAINT:
				return getFinalNoiseConstraint();
			case InformationandtimingPackage.ANALYTIC_INFORMATIONS__FINAL_COST_CONSTRAINT:
				return getFinalCostConstraint();
			case InformationandtimingPackage.ANALYTIC_INFORMATIONS__NOISE_EVALUTATION_NUMBER:
				return getNoiseEvalutationNumber();
			case InformationandtimingPackage.ANALYTIC_INFORMATIONS__COST_EVALUATION_NUMBER:
				return getCostEvaluationNumber();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InformationandtimingPackage.ANALYTIC_INFORMATIONS__FINAL_NOISE_CONSTRAINT:
				setFinalNoiseConstraint((Double)newValue);
				return;
			case InformationandtimingPackage.ANALYTIC_INFORMATIONS__FINAL_COST_CONSTRAINT:
				setFinalCostConstraint((Float)newValue);
				return;
			case InformationandtimingPackage.ANALYTIC_INFORMATIONS__NOISE_EVALUTATION_NUMBER:
				setNoiseEvalutationNumber((Integer)newValue);
				return;
			case InformationandtimingPackage.ANALYTIC_INFORMATIONS__COST_EVALUATION_NUMBER:
				setCostEvaluationNumber((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InformationandtimingPackage.ANALYTIC_INFORMATIONS__FINAL_NOISE_CONSTRAINT:
				setFinalNoiseConstraint(FINAL_NOISE_CONSTRAINT_EDEFAULT);
				return;
			case InformationandtimingPackage.ANALYTIC_INFORMATIONS__FINAL_COST_CONSTRAINT:
				setFinalCostConstraint(FINAL_COST_CONSTRAINT_EDEFAULT);
				return;
			case InformationandtimingPackage.ANALYTIC_INFORMATIONS__NOISE_EVALUTATION_NUMBER:
				setNoiseEvalutationNumber(NOISE_EVALUTATION_NUMBER_EDEFAULT);
				return;
			case InformationandtimingPackage.ANALYTIC_INFORMATIONS__COST_EVALUATION_NUMBER:
				setCostEvaluationNumber(COST_EVALUATION_NUMBER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InformationandtimingPackage.ANALYTIC_INFORMATIONS__FINAL_NOISE_CONSTRAINT:
				return finalNoiseConstraint != FINAL_NOISE_CONSTRAINT_EDEFAULT;
			case InformationandtimingPackage.ANALYTIC_INFORMATIONS__FINAL_COST_CONSTRAINT:
				return finalCostConstraint != FINAL_COST_CONSTRAINT_EDEFAULT;
			case InformationandtimingPackage.ANALYTIC_INFORMATIONS__NOISE_EVALUTATION_NUMBER:
				return noiseEvalutationNumber != NOISE_EVALUTATION_NUMBER_EDEFAULT;
			case InformationandtimingPackage.ANALYTIC_INFORMATIONS__COST_EVALUATION_NUMBER:
				return costEvaluationNumber != COST_EVALUATION_NUMBER_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (finalNoiseConstraint: ");
		result.append(finalNoiseConstraint);
		result.append(", finalCostConstraint: ");
		result.append(finalCostConstraint);
		result.append(", noiseEvalutationNumber: ");
		result.append(noiseEvalutationNumber);
		result.append(", costEvaluationNumber: ");
		result.append(costEvaluationNumber);
		result.append(')');
		return result.toString();
	}

} //AnalyticInformationsImpl
