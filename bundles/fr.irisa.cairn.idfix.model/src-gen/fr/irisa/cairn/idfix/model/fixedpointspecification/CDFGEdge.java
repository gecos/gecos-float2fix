/**
 */
package fr.irisa.cairn.idfix.model.fixedpointspecification;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CDFG Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge#getPredecessor <em>Predecessor</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge#getSuccessor <em>Successor</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge#getPredIndex <em>Pred Index</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getCDFGEdge()
 * @model
 * @generated
 */
public interface CDFGEdge extends EObject {
	/**
	 * Returns the value of the '<em><b>Predecessor</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode#getSuccessors <em>Successors</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Predecessor</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Predecessor</em>' reference.
	 * @see #setPredecessor(CDFGNode)
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getCDFGEdge_Predecessor()
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode#getSuccessors
	 * @model opposite="successors"
	 * @generated
	 */
	CDFGNode getPredecessor();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge#getPredecessor <em>Predecessor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Predecessor</em>' reference.
	 * @see #getPredecessor()
	 * @generated
	 */
	void setPredecessor(CDFGNode value);

	/**
	 * Returns the value of the '<em><b>Successor</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode#getPredecessors <em>Predecessors</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Successor</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Successor</em>' reference.
	 * @see #setSuccessor(CDFGNode)
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getCDFGEdge_Successor()
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode#getPredecessors
	 * @model opposite="predecessors"
	 * @generated
	 */
	CDFGNode getSuccessor();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge#getSuccessor <em>Successor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Successor</em>' reference.
	 * @see #getSuccessor()
	 * @generated
	 */
	void setSuccessor(CDFGNode value);

	/**
	 * Returns the value of the '<em><b>Pred Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pred Index</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pred Index</em>' attribute.
	 * @see #setPredIndex(int)
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getCDFGEdge_PredIndex()
	 * @model
	 * @generated
	 */
	int getPredIndex();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge#getPredIndex <em>Pred Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pred Index</em>' attribute.
	 * @see #getPredIndex()
	 * @generated
	 */
	void setPredIndex(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	FixedPointInformation getPredecessorInfo();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	FixedPointInformation getSuccessorInfo();

} // CDFGEdge
