/**
 */
package fr.irisa.cairn.idfix.model.operatorlibrary.impl;

import fr.irisa.cairn.idfix.model.operatorlibrary.Characteristic;
import fr.irisa.cairn.idfix.model.operatorlibrary.Instance;
import fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryPackage;
import fr.irisa.cairn.idfix.model.operatorlibrary.Triplet;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.operatorlibrary.impl.InstanceImpl#getOperands <em>Operands</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.operatorlibrary.impl.InstanceImpl#getCharacteristics <em>Characteristics</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InstanceImpl extends MinimalEObjectImpl.Container implements Instance {
	/**
	 * The cached value of the '{@link #getOperands() <em>Operands</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperands()
	 * @generated
	 * @ordered
	 */
	protected Triplet operands;

	/**
	 * The cached value of the '{@link #getCharacteristics() <em>Characteristics</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCharacteristics()
	 * @generated
	 * @ordered
	 */
	protected EList<Characteristic> characteristics;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OperatorlibraryPackage.Literals.INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Triplet getOperands() {
		if (operands != null && operands.eIsProxy()) {
			InternalEObject oldOperands = (InternalEObject)operands;
			operands = (Triplet)eResolveProxy(oldOperands);
			if (operands != oldOperands) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OperatorlibraryPackage.INSTANCE__OPERANDS, oldOperands, operands));
			}
		}
		return operands;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Triplet basicGetOperands() {
		return operands;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperands(Triplet newOperands) {
		Triplet oldOperands = operands;
		operands = newOperands;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperatorlibraryPackage.INSTANCE__OPERANDS, oldOperands, operands));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Characteristic> getCharacteristics() {
		if (characteristics == null) {
			characteristics = new EObjectResolvingEList<Characteristic>(Characteristic.class, this, OperatorlibraryPackage.INSTANCE__CHARACTERISTICS);
		}
		return characteristics;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OperatorlibraryPackage.INSTANCE__OPERANDS:
				if (resolve) return getOperands();
				return basicGetOperands();
			case OperatorlibraryPackage.INSTANCE__CHARACTERISTICS:
				return getCharacteristics();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OperatorlibraryPackage.INSTANCE__OPERANDS:
				setOperands((Triplet)newValue);
				return;
			case OperatorlibraryPackage.INSTANCE__CHARACTERISTICS:
				getCharacteristics().clear();
				getCharacteristics().addAll((Collection<? extends Characteristic>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OperatorlibraryPackage.INSTANCE__OPERANDS:
				setOperands((Triplet)null);
				return;
			case OperatorlibraryPackage.INSTANCE__CHARACTERISTICS:
				getCharacteristics().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OperatorlibraryPackage.INSTANCE__OPERANDS:
				return operands != null;
			case OperatorlibraryPackage.INSTANCE__CHARACTERISTICS:
				return characteristics != null && !characteristics.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer("Instance -> ");
		result.append("\n");
		result.append("   " + this.getOperands().toString());
		result.append("\n");
		for(Characteristic c : this.getCharacteristics()){
			result.append("   " + c.toString());
			result.append("\n");
		}
		return result.toString();
	}
} //InstanceImpl
