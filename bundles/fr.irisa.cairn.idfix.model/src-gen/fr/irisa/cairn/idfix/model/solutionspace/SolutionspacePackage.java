/**
 */
package fr.irisa.cairn.idfix.model.solutionspace;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionspaceFactory
 * @model kind="package"
 * @generated
 */
public interface SolutionspacePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "solutionspace";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://idfix.fr/fr/irisa/cairn/idfix/model/solutionspace";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "solutionspace";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SolutionspacePackage eINSTANCE = fr.irisa.cairn.idfix.model.solutionspace.impl.SolutionspacePackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.solutionspace.impl.TripletImpl <em>Triplet</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.solutionspace.impl.TripletImpl
	 * @see fr.irisa.cairn.idfix.model.solutionspace.impl.SolutionspacePackageImpl#getTriplet()
	 * @generated
	 */
	int TRIPLET = 0;

	/**
	 * The feature id for the '<em><b>First</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLET__FIRST = 0;

	/**
	 * The feature id for the '<em><b>Second</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLET__SECOND = 1;

	/**
	 * The feature id for the '<em><b>Third</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLET__THIRD = 2;

	/**
	 * The number of structural features of the '<em>Triplet</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLET_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Triplet</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.solutionspace.impl.SolutionSpaceImpl <em>Solution Space</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.solutionspace.impl.SolutionSpaceImpl
	 * @see fr.irisa.cairn.idfix.model.solutionspace.impl.SolutionspacePackageImpl#getSolutionSpace()
	 * @generated
	 */
	int SOLUTION_SPACE = 1;

	/**
	 * The feature id for the '<em><b>Operators</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE__OPERATORS = 0;

	/**
	 * The feature id for the '<em><b>Library</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE__LIBRARY = 1;

	/**
	 * The feature id for the '<em><b>Datas</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE__DATAS = 2;

	/**
	 * The number of structural features of the '<em>Solution Space</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Set All Operator To Max</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE___SET_ALL_OPERATOR_TO_MAX = 0;

	/**
	 * The operation id for the '<em>Set All Operator To Min</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE___SET_ALL_OPERATOR_TO_MIN = 1;

	/**
	 * The operation id for the '<em>Set Operator To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE___SET_OPERATOR_TO__OPERATOR_INT = 2;

	/**
	 * The operation id for the '<em>Is Max Index Of</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE___IS_MAX_INDEX_OF__OPERATOR = 3;

	/**
	 * The operation id for the '<em>Get Max Index Of</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE___GET_MAX_INDEX_OF__OPERATOR = 4;

	/**
	 * The operation id for the '<em>Get Current Index Of</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE___GET_CURRENT_INDEX_OF__OPERATOR = 5;

	/**
	 * The operation id for the '<em>Index Plus One To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE___INDEX_PLUS_ONE_TO__OPERATOR = 6;

	/**
	 * The operation id for the '<em>Index Minus One To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE___INDEX_MINUS_ONE_TO__OPERATOR = 7;

	/**
	 * The operation id for the '<em>Save Current Solution Space</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE___SAVE_CURRENT_SOLUTION_SPACE = 8;

	/**
	 * The operation id for the '<em>Use Solution Space Saved</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE___USE_SOLUTION_SPACE_SAVED = 9;

	/**
	 * The operation id for the '<em>Get Index Max Of</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE___GET_INDEX_MAX_OF__OPERATORKIND = 10;

	/**
	 * The operation id for the '<em>Set Operator To Max</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE___SET_OPERATOR_TO_MAX__OPERATOR = 11;

	/**
	 * The operation id for the '<em>Set All Operators To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE___SET_ALL_OPERATORS_TO__INT = 12;

	/**
	 * The number of operations of the '<em>Solution Space</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_SPACE_OPERATION_COUNT = 13;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.solutionspace.impl.OperatorToListElementImpl <em>Operator To List Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.solutionspace.impl.OperatorToListElementImpl
	 * @see fr.irisa.cairn.idfix.model.solutionspace.impl.SolutionspacePackageImpl#getOperatorToListElement()
	 * @generated
	 */
	int OPERATOR_TO_LIST_ELEMENT = 2;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_TO_LIST_ELEMENT__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_TO_LIST_ELEMENT__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Operator To List Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_TO_LIST_ELEMENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Operator To List Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_TO_LIST_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.solutionspace.impl.LibraryElementImpl <em>Library Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.solutionspace.impl.LibraryElementImpl
	 * @see fr.irisa.cairn.idfix.model.solutionspace.impl.SolutionspacePackageImpl#getLibraryElement()
	 * @generated
	 */
	int LIBRARY_ELEMENT = 3;

	/**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_ELEMENT__OPERAND = 0;

	/**
	 * The feature id for the '<em><b>Cost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_ELEMENT__COST = 1;

	/**
	 * The number of structural features of the '<em>Library Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_ELEMENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Library Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.solutionspace.Triplet <em>Triplet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Triplet</em>'.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.Triplet
	 * @generated
	 */
	EClass getTriplet();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.solutionspace.Triplet#getFirst <em>First</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>First</em>'.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.Triplet#getFirst()
	 * @see #getTriplet()
	 * @generated
	 */
	EAttribute getTriplet_First();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.solutionspace.Triplet#getSecond <em>Second</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Second</em>'.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.Triplet#getSecond()
	 * @see #getTriplet()
	 * @generated
	 */
	EAttribute getTriplet_Second();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.solutionspace.Triplet#getThird <em>Third</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Third</em>'.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.Triplet#getThird()
	 * @see #getTriplet()
	 * @generated
	 */
	EAttribute getTriplet_Third();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace <em>Solution Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Solution Space</em>'.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace
	 * @generated
	 */
	EClass getSolutionSpace();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#getOperators <em>Operators</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Operators</em>'.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#getOperators()
	 * @see #getSolutionSpace()
	 * @generated
	 */
	EReference getSolutionSpace_Operators();

	/**
	 * Returns the meta object for the map '{@link fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#getLibrary <em>Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Library</em>'.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#getLibrary()
	 * @see #getSolutionSpace()
	 * @generated
	 */
	EReference getSolutionSpace_Library();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#getDatas <em>Datas</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Datas</em>'.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#getDatas()
	 * @see #getSolutionSpace()
	 * @generated
	 */
	EReference getSolutionSpace_Datas();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#setAllOperatorToMax() <em>Set All Operator To Max</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set All Operator To Max</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#setAllOperatorToMax()
	 * @generated
	 */
	EOperation getSolutionSpace__SetAllOperatorToMax();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#setAllOperatorToMin() <em>Set All Operator To Min</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set All Operator To Min</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#setAllOperatorToMin()
	 * @generated
	 */
	EOperation getSolutionSpace__SetAllOperatorToMin();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#setOperatorTo(fr.irisa.cairn.idfix.model.fixedpointspecification.Operator, int) <em>Set Operator To</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Operator To</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#setOperatorTo(fr.irisa.cairn.idfix.model.fixedpointspecification.Operator, int)
	 * @generated
	 */
	EOperation getSolutionSpace__SetOperatorTo__Operator_int();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#getMaxIndexOf(fr.irisa.cairn.idfix.model.fixedpointspecification.Operator) <em>Get Max Index Of</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Max Index Of</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#getMaxIndexOf(fr.irisa.cairn.idfix.model.fixedpointspecification.Operator)
	 * @generated
	 */
	EOperation getSolutionSpace__GetMaxIndexOf__Operator();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#getCurrentIndexOf(fr.irisa.cairn.idfix.model.fixedpointspecification.Operator) <em>Get Current Index Of</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Current Index Of</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#getCurrentIndexOf(fr.irisa.cairn.idfix.model.fixedpointspecification.Operator)
	 * @generated
	 */
	EOperation getSolutionSpace__GetCurrentIndexOf__Operator();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#indexPlusOneTo(fr.irisa.cairn.idfix.model.fixedpointspecification.Operator) <em>Index Plus One To</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Index Plus One To</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#indexPlusOneTo(fr.irisa.cairn.idfix.model.fixedpointspecification.Operator)
	 * @generated
	 */
	EOperation getSolutionSpace__IndexPlusOneTo__Operator();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#indexMinusOneTo(fr.irisa.cairn.idfix.model.fixedpointspecification.Operator) <em>Index Minus One To</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Index Minus One To</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#indexMinusOneTo(fr.irisa.cairn.idfix.model.fixedpointspecification.Operator)
	 * @generated
	 */
	EOperation getSolutionSpace__IndexMinusOneTo__Operator();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#saveCurrentSolutionSpace() <em>Save Current Solution Space</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Save Current Solution Space</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#saveCurrentSolutionSpace()
	 * @generated
	 */
	EOperation getSolutionSpace__SaveCurrentSolutionSpace();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#useSolutionSpaceSaved() <em>Use Solution Space Saved</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Use Solution Space Saved</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#useSolutionSpaceSaved()
	 * @generated
	 */
	EOperation getSolutionSpace__UseSolutionSpaceSaved();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#getIndexMaxOf(fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind) <em>Get Index Max Of</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Index Max Of</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#getIndexMaxOf(fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind)
	 * @generated
	 */
	EOperation getSolutionSpace__GetIndexMaxOf__OperatorKind();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#setOperatorToMax(fr.irisa.cairn.idfix.model.fixedpointspecification.Operator) <em>Set Operator To Max</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Operator To Max</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#setOperatorToMax(fr.irisa.cairn.idfix.model.fixedpointspecification.Operator)
	 * @generated
	 */
	EOperation getSolutionSpace__SetOperatorToMax__Operator();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#setAllOperatorsTo(int) <em>Set All Operators To</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set All Operators To</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#setAllOperatorsTo(int)
	 * @generated
	 */
	EOperation getSolutionSpace__SetAllOperatorsTo__int();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#isMaxIndexOf(fr.irisa.cairn.idfix.model.fixedpointspecification.Operator) <em>Is Max Index Of</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Max Index Of</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace#isMaxIndexOf(fr.irisa.cairn.idfix.model.fixedpointspecification.Operator)
	 * @generated
	 */
	EOperation getSolutionSpace__IsMaxIndexOf__Operator();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Operator To List Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operator To List Element</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind"
	 *        valueType="fr.irisa.cairn.idfix.model.solutionspace.LibraryElement" valueMany="true"
	 * @generated
	 */
	EClass getOperatorToListElement();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getOperatorToListElement()
	 * @generated
	 */
	EAttribute getOperatorToListElement_Key();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getOperatorToListElement()
	 * @generated
	 */
	EReference getOperatorToListElement_Value();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.solutionspace.LibraryElement <em>Library Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Library Element</em>'.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.LibraryElement
	 * @generated
	 */
	EClass getLibraryElement();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.idfix.model.solutionspace.LibraryElement#getOperand <em>Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operand</em>'.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.LibraryElement#getOperand()
	 * @see #getLibraryElement()
	 * @generated
	 */
	EReference getLibraryElement_Operand();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.solutionspace.LibraryElement#getCost <em>Cost</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cost</em>'.
	 * @see fr.irisa.cairn.idfix.model.solutionspace.LibraryElement#getCost()
	 * @see #getLibraryElement()
	 * @generated
	 */
	EAttribute getLibraryElement_Cost();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SolutionspaceFactory getSolutionspaceFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.solutionspace.impl.TripletImpl <em>Triplet</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.solutionspace.impl.TripletImpl
		 * @see fr.irisa.cairn.idfix.model.solutionspace.impl.SolutionspacePackageImpl#getTriplet()
		 * @generated
		 */
		EClass TRIPLET = eINSTANCE.getTriplet();

		/**
		 * The meta object literal for the '<em><b>First</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRIPLET__FIRST = eINSTANCE.getTriplet_First();

		/**
		 * The meta object literal for the '<em><b>Second</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRIPLET__SECOND = eINSTANCE.getTriplet_Second();

		/**
		 * The meta object literal for the '<em><b>Third</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRIPLET__THIRD = eINSTANCE.getTriplet_Third();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.solutionspace.impl.SolutionSpaceImpl <em>Solution Space</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.solutionspace.impl.SolutionSpaceImpl
		 * @see fr.irisa.cairn.idfix.model.solutionspace.impl.SolutionspacePackageImpl#getSolutionSpace()
		 * @generated
		 */
		EClass SOLUTION_SPACE = eINSTANCE.getSolutionSpace();

		/**
		 * The meta object literal for the '<em><b>Operators</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOLUTION_SPACE__OPERATORS = eINSTANCE.getSolutionSpace_Operators();

		/**
		 * The meta object literal for the '<em><b>Library</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOLUTION_SPACE__LIBRARY = eINSTANCE.getSolutionSpace_Library();

		/**
		 * The meta object literal for the '<em><b>Datas</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOLUTION_SPACE__DATAS = eINSTANCE.getSolutionSpace_Datas();

		/**
		 * The meta object literal for the '<em><b>Set All Operator To Max</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SOLUTION_SPACE___SET_ALL_OPERATOR_TO_MAX = eINSTANCE.getSolutionSpace__SetAllOperatorToMax();

		/**
		 * The meta object literal for the '<em><b>Set All Operator To Min</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SOLUTION_SPACE___SET_ALL_OPERATOR_TO_MIN = eINSTANCE.getSolutionSpace__SetAllOperatorToMin();

		/**
		 * The meta object literal for the '<em><b>Set Operator To</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SOLUTION_SPACE___SET_OPERATOR_TO__OPERATOR_INT = eINSTANCE.getSolutionSpace__SetOperatorTo__Operator_int();

		/**
		 * The meta object literal for the '<em><b>Get Max Index Of</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SOLUTION_SPACE___GET_MAX_INDEX_OF__OPERATOR = eINSTANCE.getSolutionSpace__GetMaxIndexOf__Operator();

		/**
		 * The meta object literal for the '<em><b>Get Current Index Of</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SOLUTION_SPACE___GET_CURRENT_INDEX_OF__OPERATOR = eINSTANCE.getSolutionSpace__GetCurrentIndexOf__Operator();

		/**
		 * The meta object literal for the '<em><b>Index Plus One To</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SOLUTION_SPACE___INDEX_PLUS_ONE_TO__OPERATOR = eINSTANCE.getSolutionSpace__IndexPlusOneTo__Operator();

		/**
		 * The meta object literal for the '<em><b>Index Minus One To</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SOLUTION_SPACE___INDEX_MINUS_ONE_TO__OPERATOR = eINSTANCE.getSolutionSpace__IndexMinusOneTo__Operator();

		/**
		 * The meta object literal for the '<em><b>Save Current Solution Space</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SOLUTION_SPACE___SAVE_CURRENT_SOLUTION_SPACE = eINSTANCE.getSolutionSpace__SaveCurrentSolutionSpace();

		/**
		 * The meta object literal for the '<em><b>Use Solution Space Saved</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SOLUTION_SPACE___USE_SOLUTION_SPACE_SAVED = eINSTANCE.getSolutionSpace__UseSolutionSpaceSaved();

		/**
		 * The meta object literal for the '<em><b>Get Index Max Of</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SOLUTION_SPACE___GET_INDEX_MAX_OF__OPERATORKIND = eINSTANCE.getSolutionSpace__GetIndexMaxOf__OperatorKind();

		/**
		 * The meta object literal for the '<em><b>Set Operator To Max</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SOLUTION_SPACE___SET_OPERATOR_TO_MAX__OPERATOR = eINSTANCE.getSolutionSpace__SetOperatorToMax__Operator();

		/**
		 * The meta object literal for the '<em><b>Set All Operators To</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SOLUTION_SPACE___SET_ALL_OPERATORS_TO__INT = eINSTANCE.getSolutionSpace__SetAllOperatorsTo__int();

		/**
		 * The meta object literal for the '<em><b>Is Max Index Of</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SOLUTION_SPACE___IS_MAX_INDEX_OF__OPERATOR = eINSTANCE.getSolutionSpace__IsMaxIndexOf__Operator();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.solutionspace.impl.OperatorToListElementImpl <em>Operator To List Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.solutionspace.impl.OperatorToListElementImpl
		 * @see fr.irisa.cairn.idfix.model.solutionspace.impl.SolutionspacePackageImpl#getOperatorToListElement()
		 * @generated
		 */
		EClass OPERATOR_TO_LIST_ELEMENT = eINSTANCE.getOperatorToListElement();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATOR_TO_LIST_ELEMENT__KEY = eINSTANCE.getOperatorToListElement_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATOR_TO_LIST_ELEMENT__VALUE = eINSTANCE.getOperatorToListElement_Value();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.solutionspace.impl.LibraryElementImpl <em>Library Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.solutionspace.impl.LibraryElementImpl
		 * @see fr.irisa.cairn.idfix.model.solutionspace.impl.SolutionspacePackageImpl#getLibraryElement()
		 * @generated
		 */
		EClass LIBRARY_ELEMENT = eINSTANCE.getLibraryElement();

		/**
		 * The meta object literal for the '<em><b>Operand</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIBRARY_ELEMENT__OPERAND = eINSTANCE.getLibraryElement_Operand();

		/**
		 * The meta object literal for the '<em><b>Cost</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LIBRARY_ELEMENT__COST = eINSTANCE.getLibraryElement_Cost();

	}

} //SolutionspacePackage
