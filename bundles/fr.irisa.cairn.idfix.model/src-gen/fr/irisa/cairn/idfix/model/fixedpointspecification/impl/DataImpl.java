/**
 */
package fr.irisa.cairn.idfix.model.fixedpointspecification.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Data;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Operation;
import gecos.core.Symbol;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.DataImpl#getFixedInformation <em>Fixed Information</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.DataImpl#getCorrespondingSymbol <em>Corresponding Symbol</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.DataImpl#getCDFGNumber <em>CDFG Number</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataImpl extends CDFGNodeImpl implements Data {
	/**
	 * The cached value of the '{@link #getFixedInformation() <em>Fixed Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFixedInformation()
	 * @generated
	 * @ordered
	 */
	protected FixedPointInformation fixedInformation;

	/**
	 * The cached value of the '{@link #getCorrespondingSymbol() <em>Corresponding Symbol</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrespondingSymbol()
	 * @generated
	 * @ordered
	 */
	protected Symbol correspondingSymbol;

	/**
	 * The default value of the '{@link #getCDFGNumber() <em>CDFG Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCDFGNumber()
	 * @generated
	 * @ordered
	 */
	protected static final int CDFG_NUMBER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCDFGNumber() <em>CDFG Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCDFGNumber()
	 * @generated
	 * @ordered
	 */
	protected int cdfgNumber = CDFG_NUMBER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FixedpointspecificationPackage.Literals.DATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixedPointInformation getFixedInformation() {
		return fixedInformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFixedInformation(FixedPointInformation newFixedInformation, NotificationChain msgs) {
		FixedPointInformation oldFixedInformation = fixedInformation;
		fixedInformation = newFixedInformation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FixedpointspecificationPackage.DATA__FIXED_INFORMATION, oldFixedInformation, newFixedInformation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFixedInformation(FixedPointInformation newFixedInformation) {
		if (newFixedInformation != fixedInformation) {
			NotificationChain msgs = null;
			if (fixedInformation != null)
				msgs = ((InternalEObject)fixedInformation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FixedpointspecificationPackage.DATA__FIXED_INFORMATION, null, msgs);
			if (newFixedInformation != null)
				msgs = ((InternalEObject)newFixedInformation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FixedpointspecificationPackage.DATA__FIXED_INFORMATION, null, msgs);
			msgs = basicSetFixedInformation(newFixedInformation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixedpointspecificationPackage.DATA__FIXED_INFORMATION, newFixedInformation, newFixedInformation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol getCorrespondingSymbol() {
		if (correspondingSymbol != null && correspondingSymbol.eIsProxy()) {
			InternalEObject oldCorrespondingSymbol = (InternalEObject)correspondingSymbol;
			correspondingSymbol = (Symbol)eResolveProxy(oldCorrespondingSymbol);
			if (correspondingSymbol != oldCorrespondingSymbol) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FixedpointspecificationPackage.DATA__CORRESPONDING_SYMBOL, oldCorrespondingSymbol, correspondingSymbol));
			}
		}
		return correspondingSymbol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Symbol basicGetCorrespondingSymbol() {
		return correspondingSymbol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCorrespondingSymbol(Symbol newCorrespondingSymbol) {
		Symbol oldCorrespondingSymbol = correspondingSymbol;
		correspondingSymbol = newCorrespondingSymbol;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixedpointspecificationPackage.DATA__CORRESPONDING_SYMBOL, oldCorrespondingSymbol, correspondingSymbol));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getCDFGNumber() {
		return cdfgNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCDFGNumber(int newCDFGNumber) {
		int oldCDFGNumber = cdfgNumber;
		cdfgNumber = newCDFGNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixedpointspecificationPackage.DATA__CDFG_NUMBER, oldCDFGNumber, cdfgNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Operation> getPredecessorOperators() {
		EList<Operation> ret = new BasicEList<Operation>();
		for(CDFGEdge edge : getPredecessors()){
			if(edge.getPredecessor() instanceof Operation)
				ret.add((Operation) edge.getPredecessor());
			else
				throw new RuntimeException("Data cannot be connected to other node than operator");
		}
		
		return ret;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Operation> getSuccessorOperators() {
		EList<Operation> ret = new BasicEList<Operation>();
		for(CDFGEdge edge : getSuccessors()){
			if(edge.getSuccessor() instanceof Operation)
				ret.add((Operation) edge.getSuccessor());
			else
				throw new RuntimeException("Data cannot be connected to other node than operator");
		}
		
		return ret;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FixedpointspecificationPackage.DATA__FIXED_INFORMATION:
				return basicSetFixedInformation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FixedpointspecificationPackage.DATA__FIXED_INFORMATION:
				return getFixedInformation();
			case FixedpointspecificationPackage.DATA__CORRESPONDING_SYMBOL:
				if (resolve) return getCorrespondingSymbol();
				return basicGetCorrespondingSymbol();
			case FixedpointspecificationPackage.DATA__CDFG_NUMBER:
				return getCDFGNumber();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FixedpointspecificationPackage.DATA__FIXED_INFORMATION:
				setFixedInformation((FixedPointInformation)newValue);
				return;
			case FixedpointspecificationPackage.DATA__CORRESPONDING_SYMBOL:
				setCorrespondingSymbol((Symbol)newValue);
				return;
			case FixedpointspecificationPackage.DATA__CDFG_NUMBER:
				setCDFGNumber((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FixedpointspecificationPackage.DATA__FIXED_INFORMATION:
				setFixedInformation((FixedPointInformation)null);
				return;
			case FixedpointspecificationPackage.DATA__CORRESPONDING_SYMBOL:
				setCorrespondingSymbol((Symbol)null);
				return;
			case FixedpointspecificationPackage.DATA__CDFG_NUMBER:
				setCDFGNumber(CDFG_NUMBER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FixedpointspecificationPackage.DATA__FIXED_INFORMATION:
				return fixedInformation != null;
			case FixedpointspecificationPackage.DATA__CORRESPONDING_SYMBOL:
				return correspondingSymbol != null;
			case FixedpointspecificationPackage.DATA__CDFG_NUMBER:
				return cdfgNumber != CDFG_NUMBER_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case FixedpointspecificationPackage.DATA___GET_SUCCESSOR_OPERATORS:
				return getSuccessorOperators();
			case FixedpointspecificationPackage.DATA___GET_PREDECESSOR_OPERATORS:
				return getPredecessorOperators();
		}
		return super.eInvoke(operationID, arguments);
	}

	@Override
	public FixedPointInformation getOutputInfo() {
		return getFixedInformation();
	}
	
	@Override
	public String getName() {
		return correspondingSymbol.getName() + "_" + cdfgNumber;
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public String toString(){
//		StringBuffer result = new StringBuffer();
//		result.append("Data -> " + correspondingSymbol.getName());
//		result.append(", cdfg: " + cdfgNumber);
//		if(fixedInformation != null){
//			result.append(", " + fixedInformation.toString());
//		}
//	
//		if(predecessors != null){
//			result.append("\n\tInput: ");
//			for(CDFGEdge edge : predecessors){
//				result.append("\t" + edge.getPredecessor());
//			}
//		}
//		
//		if(successors != null){
//			result.append("\n\tOutput: ");
//			for(CDFGEdge edge : successors){
//				result.append("\t" + edge.getSuccessor());
//			}
//		}
//		return result.toString();
		
		return getName() + ": " + fixedInformation;
	}

} //DataImpl
