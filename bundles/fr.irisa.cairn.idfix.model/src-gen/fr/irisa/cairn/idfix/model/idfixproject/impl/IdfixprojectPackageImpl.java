/**
 */
package fr.irisa.cairn.idfix.model.idfixproject.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import float2fix.Float2fixPackage;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage;
import fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixProject;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixprojectFactory;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixprojectPackage;
import fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage;
import fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationandtimingPackageImpl;
import fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryPackage;
import fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorlibraryPackageImpl;
import fr.irisa.cairn.idfix.model.solutionspace.SolutionspacePackage;
import fr.irisa.cairn.idfix.model.solutionspace.impl.SolutionspacePackageImpl;
import fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage;
import fr.irisa.cairn.idfix.model.userconfiguration.impl.UserconfigurationPackageImpl;
import gecos.annotations.AnnotationsPackage;
import gecos.blocks.BlocksPackage;
import gecos.core.CorePackage;
import gecos.dag.DagPackage;
import gecos.gecosproject.GecosprojectPackage;
import gecos.instrs.InstrsPackage;
import gecos.types.TypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class IdfixprojectPackageImpl extends EPackageImpl implements IdfixprojectPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass idfixProjectEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.irisa.cairn.idfix.model.idfixproject.IdfixprojectPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private IdfixprojectPackageImpl() {
		super(eNS_URI, IdfixprojectFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link IdfixprojectPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static IdfixprojectPackage init() {
		if (isInited) return (IdfixprojectPackage)EPackage.Registry.INSTANCE.getEPackage(IdfixprojectPackage.eNS_URI);

		// Obtain or create and register package
		IdfixprojectPackageImpl theIdfixprojectPackage = (IdfixprojectPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof IdfixprojectPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new IdfixprojectPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		Float2fixPackage.eINSTANCE.eClass();
		AnnotationsPackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		InstrsPackage.eINSTANCE.eClass();
		BlocksPackage.eINSTANCE.eClass();
		CorePackage.eINSTANCE.eClass();
		DagPackage.eINSTANCE.eClass();
		GecosprojectPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		SolutionspacePackageImpl theSolutionspacePackage = (SolutionspacePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SolutionspacePackage.eNS_URI) instanceof SolutionspacePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SolutionspacePackage.eNS_URI) : SolutionspacePackage.eINSTANCE);
		FixedpointspecificationPackageImpl theFixedpointspecificationPackage = (FixedpointspecificationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FixedpointspecificationPackage.eNS_URI) instanceof FixedpointspecificationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FixedpointspecificationPackage.eNS_URI) : FixedpointspecificationPackage.eINSTANCE);
		InformationandtimingPackageImpl theInformationandtimingPackage = (InformationandtimingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(InformationandtimingPackage.eNS_URI) instanceof InformationandtimingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(InformationandtimingPackage.eNS_URI) : InformationandtimingPackage.eINSTANCE);
		UserconfigurationPackageImpl theUserconfigurationPackage = (UserconfigurationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UserconfigurationPackage.eNS_URI) instanceof UserconfigurationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UserconfigurationPackage.eNS_URI) : UserconfigurationPackage.eINSTANCE);
		OperatorlibraryPackageImpl theOperatorlibraryPackage = (OperatorlibraryPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OperatorlibraryPackage.eNS_URI) instanceof OperatorlibraryPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OperatorlibraryPackage.eNS_URI) : OperatorlibraryPackage.eINSTANCE);

		// Create package meta-data objects
		theIdfixprojectPackage.createPackageContents();
		theSolutionspacePackage.createPackageContents();
		theFixedpointspecificationPackage.createPackageContents();
		theInformationandtimingPackage.createPackageContents();
		theUserconfigurationPackage.createPackageContents();
		theOperatorlibraryPackage.createPackageContents();

		// Initialize created meta-data
		theIdfixprojectPackage.initializePackageContents();
		theSolutionspacePackage.initializePackageContents();
		theFixedpointspecificationPackage.initializePackageContents();
		theInformationandtimingPackage.initializePackageContents();
		theUserconfigurationPackage.initializePackageContents();
		theOperatorlibraryPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theIdfixprojectPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(IdfixprojectPackage.eNS_URI, theIdfixprojectPackage);
		return theIdfixprojectPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIdfixProject() {
		return idfixProjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIdfixProject_GecosProject() {
		return (EReference)idfixProjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIdfixProject_GecosProjectAnnotedAndNotUnrolled() {
		return (EReference)idfixProjectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIdfixProject_FixedPointSpecification() {
		return (EReference)idfixProjectEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdfixProject_OutputFolderPath() {
		return (EAttribute)idfixProjectEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIdfixProject_ProcessInformations() {
		return (EReference)idfixProjectEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIdfixProject_UserConfiguration() {
		return (EReference)idfixProjectEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIdfixProject__GetIDFixEvalOutputFolderPath() {
		return idfixProjectEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIdfixProject__GetIDFixConvOutputFolderPath() {
		return idfixProjectEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIdfixProject__GetSFGFilePath() {
		return idfixProjectEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIdfixProject__GetSimulatedValuesFilePath() {
		return idfixProjectEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIdfixProject__GetDynamicFilePath() {
		return idfixProjectEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIdfixProject__GetNoiseLibraryFilePath() {
		return idfixProjectEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIdfixProject__GenerateLogInformations() {
		return idfixProjectEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIdfixProject__Finalize() {
		return idfixProjectEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIdfixProject__PlotSaveInformations() {
		return idfixProjectEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdfixprojectFactory getIdfixprojectFactory() {
		return (IdfixprojectFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		idfixProjectEClass = createEClass(IDFIX_PROJECT);
		createEReference(idfixProjectEClass, IDFIX_PROJECT__GECOS_PROJECT);
		createEReference(idfixProjectEClass, IDFIX_PROJECT__GECOS_PROJECT_ANNOTED_AND_NOT_UNROLLED);
		createEReference(idfixProjectEClass, IDFIX_PROJECT__FIXED_POINT_SPECIFICATION);
		createEAttribute(idfixProjectEClass, IDFIX_PROJECT__OUTPUT_FOLDER_PATH);
		createEReference(idfixProjectEClass, IDFIX_PROJECT__PROCESS_INFORMATIONS);
		createEReference(idfixProjectEClass, IDFIX_PROJECT__USER_CONFIGURATION);
		createEOperation(idfixProjectEClass, IDFIX_PROJECT___GET_ID_FIX_EVAL_OUTPUT_FOLDER_PATH);
		createEOperation(idfixProjectEClass, IDFIX_PROJECT___GET_ID_FIX_CONV_OUTPUT_FOLDER_PATH);
		createEOperation(idfixProjectEClass, IDFIX_PROJECT___GET_SFG_FILE_PATH);
		createEOperation(idfixProjectEClass, IDFIX_PROJECT___GET_SIMULATED_VALUES_FILE_PATH);
		createEOperation(idfixProjectEClass, IDFIX_PROJECT___GET_DYNAMIC_FILE_PATH);
		createEOperation(idfixProjectEClass, IDFIX_PROJECT___GET_NOISE_LIBRARY_FILE_PATH);
		createEOperation(idfixProjectEClass, IDFIX_PROJECT___GENERATE_LOG_INFORMATIONS);
		createEOperation(idfixProjectEClass, IDFIX_PROJECT___FINALIZE);
		createEOperation(idfixProjectEClass, IDFIX_PROJECT___PLOT_SAVE_INFORMATIONS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		GecosprojectPackage theGecosprojectPackage = (GecosprojectPackage)EPackage.Registry.INSTANCE.getEPackage(GecosprojectPackage.eNS_URI);
		FixedpointspecificationPackage theFixedpointspecificationPackage = (FixedpointspecificationPackage)EPackage.Registry.INSTANCE.getEPackage(FixedpointspecificationPackage.eNS_URI);
		InformationandtimingPackage theInformationandtimingPackage = (InformationandtimingPackage)EPackage.Registry.INSTANCE.getEPackage(InformationandtimingPackage.eNS_URI);
		UserconfigurationPackage theUserconfigurationPackage = (UserconfigurationPackage)EPackage.Registry.INSTANCE.getEPackage(UserconfigurationPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(idfixProjectEClass, IdfixProject.class, "IdfixProject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIdfixProject_GecosProject(), theGecosprojectPackage.getGecosProject(), null, "gecosProject", null, 0, 1, IdfixProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIdfixProject_GecosProjectAnnotedAndNotUnrolled(), theGecosprojectPackage.getGecosProject(), null, "gecosProjectAnnotedAndNotUnrolled", null, 0, 1, IdfixProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIdfixProject_FixedPointSpecification(), theFixedpointspecificationPackage.getFixedPointSpecification(), null, "fixedPointSpecification", null, 0, 1, IdfixProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIdfixProject_OutputFolderPath(), ecorePackage.getEString(), "outputFolderPath", null, 0, 1, IdfixProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIdfixProject_ProcessInformations(), theInformationandtimingPackage.getInformations(), null, "processInformations", null, 0, 1, IdfixProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIdfixProject_UserConfiguration(), theUserconfigurationPackage.getUserConfiguration(), null, "userConfiguration", null, 0, 1, IdfixProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getIdfixProject__GetIDFixEvalOutputFolderPath(), ecorePackage.getEString(), "getIDFixEvalOutputFolderPath", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getIdfixProject__GetIDFixConvOutputFolderPath(), ecorePackage.getEString(), "getIDFixConvOutputFolderPath", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getIdfixProject__GetSFGFilePath(), ecorePackage.getEString(), "getSFGFilePath", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getIdfixProject__GetSimulatedValuesFilePath(), ecorePackage.getEString(), "getSimulatedValuesFilePath", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getIdfixProject__GetDynamicFilePath(), ecorePackage.getEString(), "getDynamicFilePath", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getIdfixProject__GetNoiseLibraryFilePath(), ecorePackage.getEString(), "getNoiseLibraryFilePath", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getIdfixProject__GenerateLogInformations(), null, "generateLogInformations", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getIdfixProject__Finalize(), null, "finalize", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getIdfixProject__PlotSaveInformations(), null, "plotSaveInformations", 0, 1, IS_UNIQUE, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //IdfixprojectPackageImpl
