/**
 */
package fr.irisa.cairn.idfix.model.operatorlibrary;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Triplet</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.operatorlibrary.Triplet#getFirst <em>First</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.operatorlibrary.Triplet#getSecond <em>Second</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.operatorlibrary.Triplet#getThird <em>Third</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryPackage#getTriplet()
 * @model
 * @generated
 */
public interface Triplet extends EObject {
	/**
	 * Returns the value of the '<em><b>First</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First</em>' attribute.
	 * @see #setFirst(int)
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryPackage#getTriplet_First()
	 * @model
	 * @generated
	 */
	int getFirst();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.operatorlibrary.Triplet#getFirst <em>First</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First</em>' attribute.
	 * @see #getFirst()
	 * @generated
	 */
	void setFirst(int value);

	/**
	 * Returns the value of the '<em><b>Second</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Second</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Second</em>' attribute.
	 * @see #setSecond(int)
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryPackage#getTriplet_Second()
	 * @model
	 * @generated
	 */
	int getSecond();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.operatorlibrary.Triplet#getSecond <em>Second</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Second</em>' attribute.
	 * @see #getSecond()
	 * @generated
	 */
	void setSecond(int value);

	/**
	 * Returns the value of the '<em><b>Third</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Third</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Third</em>' attribute.
	 * @see #setThird(int)
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryPackage#getTriplet_Third()
	 * @model
	 * @generated
	 */
	int getThird();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.operatorlibrary.Triplet#getThird <em>Third</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Third</em>' attribute.
	 * @see #getThird()
	 * @generated
	 */
	void setThird(int value);

} // Triplet
