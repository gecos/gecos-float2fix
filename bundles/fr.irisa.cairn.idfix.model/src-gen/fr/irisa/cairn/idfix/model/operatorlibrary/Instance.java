/**
 */
package fr.irisa.cairn.idfix.model.operatorlibrary;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.operatorlibrary.Instance#getOperands <em>Operands</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.operatorlibrary.Instance#getCharacteristics <em>Characteristics</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryPackage#getInstance()
 * @model
 * @generated
 */
public interface Instance extends EObject {
	/**
	 * Returns the value of the '<em><b>Operands</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operands</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operands</em>' reference.
	 * @see #setOperands(Triplet)
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryPackage#getInstance_Operands()
	 * @model
	 * @generated
	 */
	Triplet getOperands();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.operatorlibrary.Instance#getOperands <em>Operands</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operands</em>' reference.
	 * @see #getOperands()
	 * @generated
	 */
	void setOperands(Triplet value);

	/**
	 * Returns the value of the '<em><b>Characteristics</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.cairn.idfix.model.operatorlibrary.Characteristic}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Characteristics</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Characteristics</em>' reference list.
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryPackage#getInstance_Characteristics()
	 * @model
	 * @generated
	 */
	EList<Characteristic> getCharacteristics();

} // Instance
