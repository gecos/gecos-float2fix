/**
 */
package fr.irisa.cairn.idfix.model.fixedpointspecification.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage;
import fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE;
import fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fixed Point Information</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedPointInformationImpl#isFixedBitWidth <em>Fixed Bit Width</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedPointInformationImpl#isSigned <em>Signed</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedPointInformationImpl#getBitWidth <em>Bit Width</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedPointInformationImpl#getIntegerWidth <em>Integer Width</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedPointInformationImpl#getOverflow <em>Overflow</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedPointInformationImpl#getQuantification <em>Quantification</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedPointInformationImpl#getDynamic <em>Dynamic</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedPointInformationImpl#getDynamicIntegerWidth <em>Dynamic Integer Width</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FixedPointInformationImpl extends MinimalEObjectImpl.Container implements FixedPointInformation {
	/**
	 * The default value of the '{@link #isFixedBitWidth() <em>Fixed Bit Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFixedBitWidth()
	 * @generated
	 * @ordered
	 */
	protected static final boolean FIXED_BIT_WIDTH_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isFixedBitWidth() <em>Fixed Bit Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFixedBitWidth()
	 * @generated
	 * @ordered
	 */
	protected boolean fixedBitWidth = FIXED_BIT_WIDTH_EDEFAULT;

	/**
	 * The default value of the '{@link #isSigned() <em>Signed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSigned()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SIGNED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSigned() <em>Signed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSigned()
	 * @generated
	 * @ordered
	 */
	protected boolean signed = SIGNED_EDEFAULT;

	/**
	 * The default value of the '{@link #getBitWidth() <em>Bit Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBitWidth()
	 * @generated
	 * @ordered
	 */
	protected static final int BIT_WIDTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getBitWidth() <em>Bit Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBitWidth()
	 * @generated
	 * @ordered
	 */
	protected int bitWidth = BIT_WIDTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getIntegerWidth() <em>Integer Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegerWidth()
	 * @generated
	 * @ordered
	 */
	protected static final int INTEGER_WIDTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getIntegerWidth() <em>Integer Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegerWidth()
	 * @generated
	 * @ordered
	 */
	protected int integerWidth = INTEGER_WIDTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getOverflow() <em>Overflow</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverflow()
	 * @generated
	 * @ordered
	 */
	protected static final OVERFLOW_TYPE OVERFLOW_EDEFAULT = OVERFLOW_TYPE.SATURATION;

	/**
	 * The cached value of the '{@link #getOverflow() <em>Overflow</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverflow()
	 * @generated
	 * @ordered
	 */
	protected OVERFLOW_TYPE overflow = OVERFLOW_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuantification() <em>Quantification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantification()
	 * @generated
	 * @ordered
	 */
	protected static final QUANTIFICATION_TYPE QUANTIFICATION_EDEFAULT = QUANTIFICATION_TYPE.TRUNCATION;

	/**
	 * The cached value of the '{@link #getQuantification() <em>Quantification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantification()
	 * @generated
	 * @ordered
	 */
	protected QUANTIFICATION_TYPE quantification = QUANTIFICATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDynamic() <em>Dynamic</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDynamic()
	 * @generated
	 * @ordered
	 */
	protected fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic dynamic;

	/**
	 * The default value of the '{@link #getDynamicIntegerWidth() <em>Dynamic Integer Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDynamicIntegerWidth()
	 * @generated
	 * @ordered
	 */
	protected static final int DYNAMIC_INTEGER_WIDTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDynamicIntegerWidth() <em>Dynamic Integer Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDynamicIntegerWidth()
	 * @generated
	 * @ordered
	 */
	protected int dynamicIntegerWidth = DYNAMIC_INTEGER_WIDTH_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FixedPointInformationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FixedpointspecificationPackage.Literals.FIXED_POINT_INFORMATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isFixedBitWidth() {
		return fixedBitWidth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFixedBitWidth(boolean newFixedBitWidth) {
		boolean oldFixedBitWidth = fixedBitWidth;
		fixedBitWidth = newFixedBitWidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixedpointspecificationPackage.FIXED_POINT_INFORMATION__FIXED_BIT_WIDTH, oldFixedBitWidth, fixedBitWidth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBitWidth() {
		return bitWidth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBitWidth(int newBitWidth) {
		int oldBitWidth = bitWidth;
		bitWidth = newBitWidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixedpointspecificationPackage.FIXED_POINT_INFORMATION__BIT_WIDTH, oldBitWidth, bitWidth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getIntegerWidth() {
		return integerWidth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntegerWidth(int newIntegerWidth) {
		int oldIntegerWidth = integerWidth;
		integerWidth = newIntegerWidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixedpointspecificationPackage.FIXED_POINT_INFORMATION__INTEGER_WIDTH, oldIntegerWidth, integerWidth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSigned() {
		return signed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSigned(boolean newSigned) {
		boolean oldSigned = signed;
		signed = newSigned;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixedpointspecificationPackage.FIXED_POINT_INFORMATION__SIGNED, oldSigned, signed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OVERFLOW_TYPE getOverflow() {
		return overflow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOverflow(OVERFLOW_TYPE newOverflow) {
		OVERFLOW_TYPE oldOverflow = overflow;
		overflow = newOverflow == null ? OVERFLOW_EDEFAULT : newOverflow;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixedpointspecificationPackage.FIXED_POINT_INFORMATION__OVERFLOW, oldOverflow, overflow));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QUANTIFICATION_TYPE getQuantification() {
		return quantification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantification(QUANTIFICATION_TYPE newQuantification) {
		QUANTIFICATION_TYPE oldQuantification = quantification;
		quantification = newQuantification == null ? QUANTIFICATION_EDEFAULT : newQuantification;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixedpointspecificationPackage.FIXED_POINT_INFORMATION__QUANTIFICATION, oldQuantification, quantification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic getDynamic() {
		if (dynamic != null && dynamic.eIsProxy()) {
			InternalEObject oldDynamic = (InternalEObject)dynamic;
			dynamic = (fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic)eResolveProxy(oldDynamic);
			if (dynamic != oldDynamic) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FixedpointspecificationPackage.FIXED_POINT_INFORMATION__DYNAMIC, oldDynamic, dynamic));
			}
		}
		return dynamic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic basicGetDynamic() {
		return dynamic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setDynamic(fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic newDynamic) {
		fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic oldDynamic = dynamic;
		dynamic = newDynamic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixedpointspecificationPackage.FIXED_POINT_INFORMATION__DYNAMIC, oldDynamic, dynamic));
		
		int nbBitsMin;
		int nbBitsMax;
		int nbBits;
		boolean signed = false;
		double lowerBound = newDynamic.getMin();
		double upperBound = newDynamic.getMax();
		if (lowerBound == 0 && upperBound == 0) { // Si une opérande à une dynamique [0,0], wip = 9999 (non pris en compte par la suite dans le computePb)
			//XXX check test: it may not be correct 
			this.setIntegerWidth(0);
			this.setSigned(false);
		}
		else {
			if (lowerBound < 0) {
				nbBitsMin = (int) Math.ceil(Math.log(Math.abs(lowerBound)) / Math.log(2));
				signed = true;
			} else if (lowerBound > 0) {
				nbBitsMin = (int) Math.floor(Math.log(lowerBound)/Math.log(2)) + 1;
			} else {
				nbBitsMin = Integer.MIN_VALUE;
			}
		
			if (upperBound < 0) {
				nbBitsMax = (int) Math.ceil(Math.log(Math.abs(upperBound)) / Math.log(2));
			} else if (upperBound > 0) {
				nbBitsMax = (int) Math.floor(Math.log(upperBound)/ Math.log(2)) + 1;
			} else {
				nbBitsMax = Integer.MIN_VALUE;
			}
		
			this.setSigned(signed);
			if (signed) {
				nbBits = Math.max(nbBitsMin, nbBitsMax) + 1;	
			} else {				
				nbBits = Math.max(nbBitsMin, nbBitsMax);
			}
			
			if(fixedBitWidth && nbBits > bitWidth) {
				this.setDynamicIntegerWidth(bitWidth);
				this.setIntegerWidth(bitWidth); //XXX
			}
			else {
				this.setDynamicIntegerWidth(nbBits);
				this.setIntegerWidth(nbBits); //XXX
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getDynamicIntegerWidth() {
		return dynamicIntegerWidth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDynamicIntegerWidth(int newDynamicIntegerWidth) {
		int oldDynamicIntegerWidth = dynamicIntegerWidth;
		dynamicIntegerWidth = newDynamicIntegerWidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixedpointspecificationPackage.FIXED_POINT_INFORMATION__DYNAMIC_INTEGER_WIDTH, oldDynamicIntegerWidth, dynamicIntegerWidth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getFractionalWidth() {
		return bitWidth - integerWidth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__FIXED_BIT_WIDTH:
				return isFixedBitWidth();
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__SIGNED:
				return isSigned();
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__BIT_WIDTH:
				return getBitWidth();
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__INTEGER_WIDTH:
				return getIntegerWidth();
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__OVERFLOW:
				return getOverflow();
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__QUANTIFICATION:
				return getQuantification();
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__DYNAMIC:
				if (resolve) return getDynamic();
				return basicGetDynamic();
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__DYNAMIC_INTEGER_WIDTH:
				return getDynamicIntegerWidth();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__FIXED_BIT_WIDTH:
				setFixedBitWidth((Boolean)newValue);
				return;
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__SIGNED:
				setSigned((Boolean)newValue);
				return;
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__BIT_WIDTH:
				setBitWidth((Integer)newValue);
				return;
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__INTEGER_WIDTH:
				setIntegerWidth((Integer)newValue);
				return;
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__OVERFLOW:
				setOverflow((OVERFLOW_TYPE)newValue);
				return;
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__QUANTIFICATION:
				setQuantification((QUANTIFICATION_TYPE)newValue);
				return;
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__DYNAMIC:
				setDynamic((fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic)newValue);
				return;
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__DYNAMIC_INTEGER_WIDTH:
				setDynamicIntegerWidth((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__FIXED_BIT_WIDTH:
				setFixedBitWidth(FIXED_BIT_WIDTH_EDEFAULT);
				return;
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__SIGNED:
				setSigned(SIGNED_EDEFAULT);
				return;
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__BIT_WIDTH:
				setBitWidth(BIT_WIDTH_EDEFAULT);
				return;
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__INTEGER_WIDTH:
				setIntegerWidth(INTEGER_WIDTH_EDEFAULT);
				return;
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__OVERFLOW:
				setOverflow(OVERFLOW_EDEFAULT);
				return;
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__QUANTIFICATION:
				setQuantification(QUANTIFICATION_EDEFAULT);
				return;
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__DYNAMIC:
				setDynamic((fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic)null);
				return;
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__DYNAMIC_INTEGER_WIDTH:
				setDynamicIntegerWidth(DYNAMIC_INTEGER_WIDTH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__FIXED_BIT_WIDTH:
				return fixedBitWidth != FIXED_BIT_WIDTH_EDEFAULT;
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__SIGNED:
				return signed != SIGNED_EDEFAULT;
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__BIT_WIDTH:
				return bitWidth != BIT_WIDTH_EDEFAULT;
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__INTEGER_WIDTH:
				return integerWidth != INTEGER_WIDTH_EDEFAULT;
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__OVERFLOW:
				return overflow != OVERFLOW_EDEFAULT;
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__QUANTIFICATION:
				return quantification != QUANTIFICATION_EDEFAULT;
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__DYNAMIC:
				return dynamic != null;
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION__DYNAMIC_INTEGER_WIDTH:
				return dynamicIntegerWidth != DYNAMIC_INTEGER_WIDTH_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case FixedpointspecificationPackage.FIXED_POINT_INFORMATION___GET_FRACTIONAL_WIDTH:
				return getFractionalWidth();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append("<");
		result.append(bitWidth);
		result.append(",");
		result.append(integerWidth);
		result.append("(").append(dynamicIntegerWidth).append(')');
		result.append(",");
		result.append(getFractionalWidth());
//		result.append(",");
//		result.append(signed ? "s" : "u");
//		result.append(", ");
//		result.append(overflow);
//		result.append(", ");
//		result.append(quantification);
//		result.append(">, (");
//		result.append(dynamic);
		result.append(">");
		
		return result.toString();
	}

} //FixedPointInformationImpl
