/**
 */
package fr.irisa.cairn.idfix.model.userconfiguration;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage
 * @generated
 */
public interface UserconfigurationFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UserconfigurationFactory eINSTANCE = fr.irisa.cairn.idfix.model.userconfiguration.impl.UserconfigurationFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>User Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>User Configuration</em>'.
	 * @generated
	 */
	UserConfiguration createUserConfiguration();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	UserconfigurationPackage getUserconfigurationPackage();

} //UserconfigurationFactory
