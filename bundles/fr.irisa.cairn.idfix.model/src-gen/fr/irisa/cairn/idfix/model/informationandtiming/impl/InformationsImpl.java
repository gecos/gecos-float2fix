/**
 */
package fr.irisa.cairn.idfix.model.informationandtiming.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import fr.irisa.cairn.idfix.model.informationandtiming.AnalyticInformations;
import fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage;
import fr.irisa.cairn.idfix.model.informationandtiming.Informations;
import fr.irisa.cairn.idfix.model.informationandtiming.SimulationInformations;
import fr.irisa.cairn.idfix.model.informationandtiming.Timing;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Informations</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationsImpl#getOptimizationAlgorithmUsed <em>Optimization Algorithm Used</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationsImpl#getUserNoiseConstraint <em>User Noise Constraint</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationsImpl#getTiming <em>Timing</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationsImpl#isUseLastResultModeEnable <em>Use Last Result Mode Enable</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationsImpl#getAnalyticInformations <em>Analytic Informations</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationsImpl#getSimulationInformations <em>Simulation Informations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InformationsImpl extends MinimalEObjectImpl.Container implements Informations {
	/**
	 * The default value of the '{@link #getOptimizationAlgorithmUsed() <em>Optimization Algorithm Used</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOptimizationAlgorithmUsed()
	 * @generated
	 * @ordered
	 */
	protected static final String OPTIMIZATION_ALGORITHM_USED_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOptimizationAlgorithmUsed() <em>Optimization Algorithm Used</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOptimizationAlgorithmUsed()
	 * @generated
	 * @ordered
	 */
	protected String optimizationAlgorithmUsed = OPTIMIZATION_ALGORITHM_USED_EDEFAULT;

	/**
	 * The default value of the '{@link #getUserNoiseConstraint() <em>User Noise Constraint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserNoiseConstraint()
	 * @generated
	 * @ordered
	 */
	protected static final float USER_NOISE_CONSTRAINT_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getUserNoiseConstraint() <em>User Noise Constraint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserNoiseConstraint()
	 * @generated
	 * @ordered
	 */
	protected float userNoiseConstraint = USER_NOISE_CONSTRAINT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTiming() <em>Timing</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTiming()
	 * @generated
	 * @ordered
	 */
	protected Timing timing;

	/**
	 * The default value of the '{@link #isUseLastResultModeEnable() <em>Use Last Result Mode Enable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUseLastResultModeEnable()
	 * @generated
	 * @ordered
	 */
	protected static final boolean USE_LAST_RESULT_MODE_ENABLE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isUseLastResultModeEnable() <em>Use Last Result Mode Enable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUseLastResultModeEnable()
	 * @generated
	 * @ordered
	 */
	protected boolean useLastResultModeEnable = USE_LAST_RESULT_MODE_ENABLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAnalyticInformations() <em>Analytic Informations</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnalyticInformations()
	 * @generated
	 * @ordered
	 */
	protected AnalyticInformations analyticInformations;

	/**
	 * The cached value of the '{@link #getSimulationInformations() <em>Simulation Informations</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimulationInformations()
	 * @generated
	 * @ordered
	 */
	protected SimulationInformations simulationInformations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InformationsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InformationandtimingPackage.Literals.INFORMATIONS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOptimizationAlgorithmUsed() {
		return optimizationAlgorithmUsed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOptimizationAlgorithmUsed(String newOptimizationAlgorithmUsed) {
		String oldOptimizationAlgorithmUsed = optimizationAlgorithmUsed;
		optimizationAlgorithmUsed = newOptimizationAlgorithmUsed;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InformationandtimingPackage.INFORMATIONS__OPTIMIZATION_ALGORITHM_USED, oldOptimizationAlgorithmUsed, optimizationAlgorithmUsed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getUserNoiseConstraint() {
		return userNoiseConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUserNoiseConstraint(float newUserNoiseConstraint) {
		float oldUserNoiseConstraint = userNoiseConstraint;
		userNoiseConstraint = newUserNoiseConstraint;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InformationandtimingPackage.INFORMATIONS__USER_NOISE_CONSTRAINT, oldUserNoiseConstraint, userNoiseConstraint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Timing getTiming() {
		if (timing != null && timing.eIsProxy()) {
			InternalEObject oldTiming = (InternalEObject)timing;
			timing = (Timing)eResolveProxy(oldTiming);
			if (timing != oldTiming) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InformationandtimingPackage.INFORMATIONS__TIMING, oldTiming, timing));
			}
		}
		return timing;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Timing basicGetTiming() {
		return timing;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTiming(Timing newTiming) {
		Timing oldTiming = timing;
		timing = newTiming;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InformationandtimingPackage.INFORMATIONS__TIMING, oldTiming, timing));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isUseLastResultModeEnable() {
		return useLastResultModeEnable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUseLastResultModeEnable(boolean newUseLastResultModeEnable) {
		boolean oldUseLastResultModeEnable = useLastResultModeEnable;
		useLastResultModeEnable = newUseLastResultModeEnable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InformationandtimingPackage.INFORMATIONS__USE_LAST_RESULT_MODE_ENABLE, oldUseLastResultModeEnable, useLastResultModeEnable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnalyticInformations getAnalyticInformations() {
		if (analyticInformations != null && analyticInformations.eIsProxy()) {
			InternalEObject oldAnalyticInformations = (InternalEObject)analyticInformations;
			analyticInformations = (AnalyticInformations)eResolveProxy(oldAnalyticInformations);
			if (analyticInformations != oldAnalyticInformations) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InformationandtimingPackage.INFORMATIONS__ANALYTIC_INFORMATIONS, oldAnalyticInformations, analyticInformations));
			}
		}
		return analyticInformations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnalyticInformations basicGetAnalyticInformations() {
		return analyticInformations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnalyticInformations(AnalyticInformations newAnalyticInformations) {
		AnalyticInformations oldAnalyticInformations = analyticInformations;
		analyticInformations = newAnalyticInformations;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InformationandtimingPackage.INFORMATIONS__ANALYTIC_INFORMATIONS, oldAnalyticInformations, analyticInformations));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationInformations getSimulationInformations() {
		if (simulationInformations != null && simulationInformations.eIsProxy()) {
			InternalEObject oldSimulationInformations = (InternalEObject)simulationInformations;
			simulationInformations = (SimulationInformations)eResolveProxy(oldSimulationInformations);
			if (simulationInformations != oldSimulationInformations) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, InformationandtimingPackage.INFORMATIONS__SIMULATION_INFORMATIONS, oldSimulationInformations, simulationInformations));
			}
		}
		return simulationInformations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationInformations basicGetSimulationInformations() {
		return simulationInformations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSimulationInformations(SimulationInformations newSimulationInformations) {
		SimulationInformations oldSimulationInformations = simulationInformations;
		simulationInformations = newSimulationInformations;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, InformationandtimingPackage.INFORMATIONS__SIMULATION_INFORMATIONS, oldSimulationInformations, simulationInformations));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case InformationandtimingPackage.INFORMATIONS__OPTIMIZATION_ALGORITHM_USED:
				return getOptimizationAlgorithmUsed();
			case InformationandtimingPackage.INFORMATIONS__USER_NOISE_CONSTRAINT:
				return getUserNoiseConstraint();
			case InformationandtimingPackage.INFORMATIONS__TIMING:
				if (resolve) return getTiming();
				return basicGetTiming();
			case InformationandtimingPackage.INFORMATIONS__USE_LAST_RESULT_MODE_ENABLE:
				return isUseLastResultModeEnable();
			case InformationandtimingPackage.INFORMATIONS__ANALYTIC_INFORMATIONS:
				if (resolve) return getAnalyticInformations();
				return basicGetAnalyticInformations();
			case InformationandtimingPackage.INFORMATIONS__SIMULATION_INFORMATIONS:
				if (resolve) return getSimulationInformations();
				return basicGetSimulationInformations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case InformationandtimingPackage.INFORMATIONS__OPTIMIZATION_ALGORITHM_USED:
				setOptimizationAlgorithmUsed((String)newValue);
				return;
			case InformationandtimingPackage.INFORMATIONS__USER_NOISE_CONSTRAINT:
				setUserNoiseConstraint((Float)newValue);
				return;
			case InformationandtimingPackage.INFORMATIONS__TIMING:
				setTiming((Timing)newValue);
				return;
			case InformationandtimingPackage.INFORMATIONS__USE_LAST_RESULT_MODE_ENABLE:
				setUseLastResultModeEnable((Boolean)newValue);
				return;
			case InformationandtimingPackage.INFORMATIONS__ANALYTIC_INFORMATIONS:
				setAnalyticInformations((AnalyticInformations)newValue);
				return;
			case InformationandtimingPackage.INFORMATIONS__SIMULATION_INFORMATIONS:
				setSimulationInformations((SimulationInformations)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case InformationandtimingPackage.INFORMATIONS__OPTIMIZATION_ALGORITHM_USED:
				setOptimizationAlgorithmUsed(OPTIMIZATION_ALGORITHM_USED_EDEFAULT);
				return;
			case InformationandtimingPackage.INFORMATIONS__USER_NOISE_CONSTRAINT:
				setUserNoiseConstraint(USER_NOISE_CONSTRAINT_EDEFAULT);
				return;
			case InformationandtimingPackage.INFORMATIONS__TIMING:
				setTiming((Timing)null);
				return;
			case InformationandtimingPackage.INFORMATIONS__USE_LAST_RESULT_MODE_ENABLE:
				setUseLastResultModeEnable(USE_LAST_RESULT_MODE_ENABLE_EDEFAULT);
				return;
			case InformationandtimingPackage.INFORMATIONS__ANALYTIC_INFORMATIONS:
				setAnalyticInformations((AnalyticInformations)null);
				return;
			case InformationandtimingPackage.INFORMATIONS__SIMULATION_INFORMATIONS:
				setSimulationInformations((SimulationInformations)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case InformationandtimingPackage.INFORMATIONS__OPTIMIZATION_ALGORITHM_USED:
				return OPTIMIZATION_ALGORITHM_USED_EDEFAULT == null ? optimizationAlgorithmUsed != null : !OPTIMIZATION_ALGORITHM_USED_EDEFAULT.equals(optimizationAlgorithmUsed);
			case InformationandtimingPackage.INFORMATIONS__USER_NOISE_CONSTRAINT:
				return userNoiseConstraint != USER_NOISE_CONSTRAINT_EDEFAULT;
			case InformationandtimingPackage.INFORMATIONS__TIMING:
				return timing != null;
			case InformationandtimingPackage.INFORMATIONS__USE_LAST_RESULT_MODE_ENABLE:
				return useLastResultModeEnable != USE_LAST_RESULT_MODE_ENABLE_EDEFAULT;
			case InformationandtimingPackage.INFORMATIONS__ANALYTIC_INFORMATIONS:
				return analyticInformations != null;
			case InformationandtimingPackage.INFORMATIONS__SIMULATION_INFORMATIONS:
				return simulationInformations != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append("Optimization algorithm used: " + optimizationAlgorithmUsed + "\n");
		result.append("User noise constraint: " + userNoiseConstraint + "\n");
		result.append("Final noise constraint: " + getAnalyticInformations().getFinalNoiseConstraint() + "\n");
		result.append("Final cost constraint: " + getAnalyticInformations().getFinalCostConstraint() + "\n");
		result.append("Noise evaluation number: " + getAnalyticInformations().getNoiseEvalutationNumber() + "\n");
		result.append("Cost evaluation number: " + getAnalyticInformations().getCostEvaluationNumber() + "\n");
		
		result.append("\n");
		result.append(this.getTiming());
		return result.toString();
	}

} //InformationsImpl
