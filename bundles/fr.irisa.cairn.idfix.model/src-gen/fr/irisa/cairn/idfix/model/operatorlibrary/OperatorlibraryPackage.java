/**
 */
package fr.irisa.cairn.idfix.model.operatorlibrary;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryFactory
 * @model kind="package"
 * @generated
 */
public interface OperatorlibraryPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "operatorlibrary";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://idfix.fr/fr/irisa/cairn/idfix/model/operatorlibrary";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "operatorlibrary";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OperatorlibraryPackage eINSTANCE = fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorlibraryPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorLibraryImpl <em>Operator Library</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorLibraryImpl
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorlibraryPackageImpl#getOperatorLibrary()
	 * @generated
	 */
	int OPERATOR_LIBRARY = 0;

	/**
	 * The feature id for the '<em><b>Operators</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_LIBRARY__OPERATORS = 0;

	/**
	 * The number of structural features of the '<em>Operator Library</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_LIBRARY_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Get Operator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_LIBRARY___GET_OPERATOR__OPERATOR_KIND = 0;

	/**
	 * The number of operations of the '<em>Operator Library</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_LIBRARY_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorImpl <em>Operator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorImpl
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorlibraryPackageImpl#getOperator()
	 * @generated
	 */
	int OPERATOR = 1;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__KIND = 0;

	/**
	 * The feature id for the '<em><b>Intances</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__INTANCES = 1;

	/**
	 * The number of structural features of the '<em>Operator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Operator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.operatorlibrary.impl.InstanceImpl <em>Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.impl.InstanceImpl
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorlibraryPackageImpl#getInstance()
	 * @generated
	 */
	int INSTANCE = 2;

	/**
	 * The feature id for the '<em><b>Operands</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE__OPERANDS = 0;

	/**
	 * The feature id for the '<em><b>Characteristics</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE__CHARACTERISTICS = 1;

	/**
	 * The number of structural features of the '<em>Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.operatorlibrary.impl.TripletImpl <em>Triplet</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.impl.TripletImpl
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorlibraryPackageImpl#getTriplet()
	 * @generated
	 */
	int TRIPLET = 3;

	/**
	 * The feature id for the '<em><b>First</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLET__FIRST = 0;

	/**
	 * The feature id for the '<em><b>Second</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLET__SECOND = 1;

	/**
	 * The feature id for the '<em><b>Third</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLET__THIRD = 2;

	/**
	 * The number of structural features of the '<em>Triplet</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLET_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Triplet</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIPLET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.operatorlibrary.impl.CharacteristicImpl <em>Characteristic</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.impl.CharacteristicImpl
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorlibraryPackageImpl#getCharacteristic()
	 * @generated
	 */
	int CHARACTERISTIC = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTERISTIC__NAME = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTERISTIC__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Characteristic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTERISTIC_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Characteristic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTERISTIC_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.operatorlibrary.OPERATOR_KIND <em>OPERATOR KIND</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.OPERATOR_KIND
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorlibraryPackageImpl#getOPERATOR_KIND()
	 * @generated
	 */
	int OPERATOR_KIND = 5;


	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.operatorlibrary.OperatorLibrary <em>Operator Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operator Library</em>'.
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.OperatorLibrary
	 * @generated
	 */
	EClass getOperatorLibrary();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.idfix.model.operatorlibrary.OperatorLibrary#getOperators <em>Operators</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Operators</em>'.
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.OperatorLibrary#getOperators()
	 * @see #getOperatorLibrary()
	 * @generated
	 */
	EReference getOperatorLibrary_Operators();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.operatorlibrary.OperatorLibrary#getOperator(fr.irisa.cairn.idfix.model.operatorlibrary.OPERATOR_KIND) <em>Get Operator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Operator</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.OperatorLibrary#getOperator(fr.irisa.cairn.idfix.model.operatorlibrary.OPERATOR_KIND)
	 * @generated
	 */
	EOperation getOperatorLibrary__GetOperator__OPERATOR_KIND();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.operatorlibrary.Operator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operator</em>'.
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.Operator
	 * @generated
	 */
	EClass getOperator();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.operatorlibrary.Operator#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.Operator#getKind()
	 * @see #getOperator()
	 * @generated
	 */
	EAttribute getOperator_Kind();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.idfix.model.operatorlibrary.Operator#getIntances <em>Intances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Intances</em>'.
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.Operator#getIntances()
	 * @see #getOperator()
	 * @generated
	 */
	EReference getOperator_Intances();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.operatorlibrary.Instance <em>Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Instance</em>'.
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.Instance
	 * @generated
	 */
	EClass getInstance();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.idfix.model.operatorlibrary.Instance#getOperands <em>Operands</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Operands</em>'.
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.Instance#getOperands()
	 * @see #getInstance()
	 * @generated
	 */
	EReference getInstance_Operands();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.idfix.model.operatorlibrary.Instance#getCharacteristics <em>Characteristics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Characteristics</em>'.
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.Instance#getCharacteristics()
	 * @see #getInstance()
	 * @generated
	 */
	EReference getInstance_Characteristics();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.operatorlibrary.Triplet <em>Triplet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Triplet</em>'.
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.Triplet
	 * @generated
	 */
	EClass getTriplet();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.operatorlibrary.Triplet#getFirst <em>First</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>First</em>'.
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.Triplet#getFirst()
	 * @see #getTriplet()
	 * @generated
	 */
	EAttribute getTriplet_First();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.operatorlibrary.Triplet#getSecond <em>Second</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Second</em>'.
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.Triplet#getSecond()
	 * @see #getTriplet()
	 * @generated
	 */
	EAttribute getTriplet_Second();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.operatorlibrary.Triplet#getThird <em>Third</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Third</em>'.
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.Triplet#getThird()
	 * @see #getTriplet()
	 * @generated
	 */
	EAttribute getTriplet_Third();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.operatorlibrary.Characteristic <em>Characteristic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Characteristic</em>'.
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.Characteristic
	 * @generated
	 */
	EClass getCharacteristic();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.operatorlibrary.Characteristic#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.Characteristic#getName()
	 * @see #getCharacteristic()
	 * @generated
	 */
	EAttribute getCharacteristic_Name();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.operatorlibrary.Characteristic#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.Characteristic#getValue()
	 * @see #getCharacteristic()
	 * @generated
	 */
	EAttribute getCharacteristic_Value();

	/**
	 * Returns the meta object for enum '{@link fr.irisa.cairn.idfix.model.operatorlibrary.OPERATOR_KIND <em>OPERATOR KIND</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>OPERATOR KIND</em>'.
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.OPERATOR_KIND
	 * @generated
	 */
	EEnum getOPERATOR_KIND();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OperatorlibraryFactory getOperatorlibraryFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorLibraryImpl <em>Operator Library</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorLibraryImpl
		 * @see fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorlibraryPackageImpl#getOperatorLibrary()
		 * @generated
		 */
		EClass OPERATOR_LIBRARY = eINSTANCE.getOperatorLibrary();

		/**
		 * The meta object literal for the '<em><b>Operators</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATOR_LIBRARY__OPERATORS = eINSTANCE.getOperatorLibrary_Operators();

		/**
		 * The meta object literal for the '<em><b>Get Operator</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OPERATOR_LIBRARY___GET_OPERATOR__OPERATOR_KIND = eINSTANCE.getOperatorLibrary__GetOperator__OPERATOR_KIND();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorImpl <em>Operator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorImpl
		 * @see fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorlibraryPackageImpl#getOperator()
		 * @generated
		 */
		EClass OPERATOR = eINSTANCE.getOperator();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATOR__KIND = eINSTANCE.getOperator_Kind();

		/**
		 * The meta object literal for the '<em><b>Intances</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATOR__INTANCES = eINSTANCE.getOperator_Intances();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.operatorlibrary.impl.InstanceImpl <em>Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.operatorlibrary.impl.InstanceImpl
		 * @see fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorlibraryPackageImpl#getInstance()
		 * @generated
		 */
		EClass INSTANCE = eINSTANCE.getInstance();

		/**
		 * The meta object literal for the '<em><b>Operands</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INSTANCE__OPERANDS = eINSTANCE.getInstance_Operands();

		/**
		 * The meta object literal for the '<em><b>Characteristics</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INSTANCE__CHARACTERISTICS = eINSTANCE.getInstance_Characteristics();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.operatorlibrary.impl.TripletImpl <em>Triplet</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.operatorlibrary.impl.TripletImpl
		 * @see fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorlibraryPackageImpl#getTriplet()
		 * @generated
		 */
		EClass TRIPLET = eINSTANCE.getTriplet();

		/**
		 * The meta object literal for the '<em><b>First</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRIPLET__FIRST = eINSTANCE.getTriplet_First();

		/**
		 * The meta object literal for the '<em><b>Second</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRIPLET__SECOND = eINSTANCE.getTriplet_Second();

		/**
		 * The meta object literal for the '<em><b>Third</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRIPLET__THIRD = eINSTANCE.getTriplet_Third();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.operatorlibrary.impl.CharacteristicImpl <em>Characteristic</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.operatorlibrary.impl.CharacteristicImpl
		 * @see fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorlibraryPackageImpl#getCharacteristic()
		 * @generated
		 */
		EClass CHARACTERISTIC = eINSTANCE.getCharacteristic();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHARACTERISTIC__NAME = eINSTANCE.getCharacteristic_Name();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHARACTERISTIC__VALUE = eINSTANCE.getCharacteristic_Value();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.operatorlibrary.OPERATOR_KIND <em>OPERATOR KIND</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.operatorlibrary.OPERATOR_KIND
		 * @see fr.irisa.cairn.idfix.model.operatorlibrary.impl.OperatorlibraryPackageImpl#getOPERATOR_KIND()
		 * @generated
		 */
		EEnum OPERATOR_KIND = eINSTANCE.getOPERATOR_KIND();

	}

} //OperatorlibraryPackage
