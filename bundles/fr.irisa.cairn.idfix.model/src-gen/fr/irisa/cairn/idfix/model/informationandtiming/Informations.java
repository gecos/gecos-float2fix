/**
 */
package fr.irisa.cairn.idfix.model.informationandtiming;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Informations</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.Informations#getOptimizationAlgorithmUsed <em>Optimization Algorithm Used</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.Informations#getUserNoiseConstraint <em>User Noise Constraint</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.Informations#getTiming <em>Timing</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.Informations#isUseLastResultModeEnable <em>Use Last Result Mode Enable</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.Informations#getAnalyticInformations <em>Analytic Informations</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.Informations#getSimulationInformations <em>Simulation Informations</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage#getInformations()
 * @model
 * @generated
 */
public interface Informations extends EObject {
	/**
	 * Returns the value of the '<em><b>Optimization Algorithm Used</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Optimization Algorithm Used</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optimization Algorithm Used</em>' attribute.
	 * @see #setOptimizationAlgorithmUsed(String)
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage#getInformations_OptimizationAlgorithmUsed()
	 * @model
	 * @generated
	 */
	String getOptimizationAlgorithmUsed();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.informationandtiming.Informations#getOptimizationAlgorithmUsed <em>Optimization Algorithm Used</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Optimization Algorithm Used</em>' attribute.
	 * @see #getOptimizationAlgorithmUsed()
	 * @generated
	 */
	void setOptimizationAlgorithmUsed(String value);

	/**
	 * Returns the value of the '<em><b>User Noise Constraint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>User Noise Constraint</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User Noise Constraint</em>' attribute.
	 * @see #setUserNoiseConstraint(float)
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage#getInformations_UserNoiseConstraint()
	 * @model
	 * @generated
	 */
	float getUserNoiseConstraint();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.informationandtiming.Informations#getUserNoiseConstraint <em>User Noise Constraint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User Noise Constraint</em>' attribute.
	 * @see #getUserNoiseConstraint()
	 * @generated
	 */
	void setUserNoiseConstraint(float value);

	/**
	 * Returns the value of the '<em><b>Timing</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timing</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timing</em>' reference.
	 * @see #setTiming(Timing)
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage#getInformations_Timing()
	 * @model
	 * @generated
	 */
	Timing getTiming();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.informationandtiming.Informations#getTiming <em>Timing</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timing</em>' reference.
	 * @see #getTiming()
	 * @generated
	 */
	void setTiming(Timing value);

	/**
	 * Returns the value of the '<em><b>Use Last Result Mode Enable</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Last Result Mode Enable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Last Result Mode Enable</em>' attribute.
	 * @see #setUseLastResultModeEnable(boolean)
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage#getInformations_UseLastResultModeEnable()
	 * @model default="false"
	 * @generated
	 */
	boolean isUseLastResultModeEnable();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.informationandtiming.Informations#isUseLastResultModeEnable <em>Use Last Result Mode Enable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use Last Result Mode Enable</em>' attribute.
	 * @see #isUseLastResultModeEnable()
	 * @generated
	 */
	void setUseLastResultModeEnable(boolean value);

	/**
	 * Returns the value of the '<em><b>Analytic Informations</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Analytic Informations</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Analytic Informations</em>' reference.
	 * @see #setAnalyticInformations(AnalyticInformations)
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage#getInformations_AnalyticInformations()
	 * @model
	 * @generated
	 */
	AnalyticInformations getAnalyticInformations();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.informationandtiming.Informations#getAnalyticInformations <em>Analytic Informations</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Analytic Informations</em>' reference.
	 * @see #getAnalyticInformations()
	 * @generated
	 */
	void setAnalyticInformations(AnalyticInformations value);

	/**
	 * Returns the value of the '<em><b>Simulation Informations</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simulation Informations</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simulation Informations</em>' reference.
	 * @see #setSimulationInformations(SimulationInformations)
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage#getInformations_SimulationInformations()
	 * @model
	 * @generated
	 */
	SimulationInformations getSimulationInformations();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.informationandtiming.Informations#getSimulationInformations <em>Simulation Informations</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Simulation Informations</em>' reference.
	 * @see #getSimulationInformations()
	 * @generated
	 */
	void setSimulationInformations(SimulationInformations value);

} // Informations
