/**
 */
package fr.irisa.cairn.idfix.model.fixedpointspecification;

import fr.irisa.cairn.idfix.model.operatorlibrary.Instance;
import gecos.instrs.Instruction;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getOperands <em>Operands</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getEval_cdfg_number <em>Eval cdfg number</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getConv_cdfg_number <em>Conv cdfg number</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getKind <em>Kind</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getCost <em>Cost</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getCorrespondingInstructions <em>Corresponding Instructions</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getUsageNumber <em>Usage Number</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getInstances <em>Instances</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getOperator()
 * @model
 * @generated
 */
public interface Operation extends CDFGNode {
	/**
	 * Returns the value of the '<em><b>Operands</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operands</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operands</em>' containment reference list.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getOperator_Operands()
	 * @model containment="true" upper="3"
	 * @generated
	 */
	EList<FixedPointInformation> getOperands();

	/**
	 * Returns the value of the '<em><b>Eval cdfg number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Eval cdfg number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Eval cdfg number</em>' attribute.
	 * @see #setEval_cdfg_number(int)
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getOperator_Eval_cdfg_number()
	 * @model
	 * @generated
	 */
	int getEval_cdfg_number();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getEval_cdfg_number <em>Eval cdfg number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Eval cdfg number</em>' attribute.
	 * @see #getEval_cdfg_number()
	 * @generated
	 */
	void setEval_cdfg_number(int value);

	/**
	 * Returns the value of the '<em><b>Conv cdfg number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conv cdfg number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conv cdfg number</em>' attribute.
	 * @see #setConv_cdfg_number(int)
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getOperator_Conv_cdfg_number()
	 * @model
	 * @generated
	 */
	int getConv_cdfg_number();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getConv_cdfg_number <em>Conv cdfg number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Conv cdfg number</em>' attribute.
	 * @see #getConv_cdfg_number()
	 * @generated
	 */
	void setConv_cdfg_number(int value);

	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind
	 * @see #setKind(OperatorKind)
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getOperator_Kind()
	 * @model
	 * @generated
	 */
	OperatorKind getKind();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind
	 * @see #getKind()
	 * @generated
	 */
	void setKind(OperatorKind value);

	/**
	 * Returns the value of the '<em><b>Cost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cost</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cost</em>' attribute.
	 * @see #setCost(float)
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getOperator_Cost()
	 * @model
	 * @generated
	 */
	float getCost();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getCost <em>Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cost</em>' attribute.
	 * @see #getCost()
	 * @generated
	 */
	void setCost(float value);

	/**
	 * Returns the value of the '<em><b>Corresponding Instructions</b></em>' reference list.
	 * The list contents are of type {@link gecos.instrs.Instruction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Corresponding Instructions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Corresponding Instructions</em>' reference list.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getOperator_CorrespondingInstructions()
	 * @model
	 * @generated
	 */
	EList<Instruction> getCorrespondingInstructions();

	/**
	 * Returns the value of the '<em><b>Usage Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Usage Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Usage Number</em>' attribute.
	 * @see #setUsageNumber(int)
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getOperator_UsageNumber()
	 * @model
	 * @generated
	 */
	int getUsageNumber();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getUsageNumber <em>Usage Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Usage Number</em>' attribute.
	 * @see #getUsageNumber()
	 * @generated
	 */
	void setUsageNumber(int value);

	/**
	 * Returns the value of the '<em><b>Instances</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.cairn.idfix.model.operatorlibrary.Instance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instances</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instances</em>' reference list.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getOperator_Instances()
	 * @model
	 * @generated
	 */
	EList<Instance> getInstances();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void setInstance(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean haveNextInstance();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	int getMaxInstance();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	int getCurrentInstance();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void nextInstance();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void previousInstance();

} // Operator
