/**
 */
package fr.irisa.cairn.idfix.model.fixedpointspecification;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationFactory
 * @model kind="package"
 * @generated
 */
public interface FixedpointspecificationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "fixedpointspecification";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://idfix.fr/fr/irisa/cairn/idfix/model/fixedpointspecification";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "fixedpointspecification";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FixedpointspecificationPackage eINSTANCE = fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedPointSpecificationImpl <em>Fixed Point Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedPointSpecificationImpl
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl#getFixedPointSpecification()
	 * @generated
	 */
	int FIXED_POINT_SPECIFICATION = 0;

	/**
	 * The feature id for the '<em><b>Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_SPECIFICATION__EDGES = 0;

	/**
	 * The feature id for the '<em><b>Datas</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_SPECIFICATION__DATAS = 1;

	/**
	 * The feature id for the '<em><b>Operators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_SPECIFICATION__OPERATORS = 2;

	/**
	 * The number of structural features of the '<em>Fixed Point Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_SPECIFICATION_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Update Dynamic Information</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_SPECIFICATION___UPDATE_DYNAMIC_INFORMATION__STRING = 0;

	/**
	 * The operation id for the '<em>Find Operator From Eval CDFG</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_SPECIFICATION___FIND_OPERATOR_FROM_EVAL_CDFG__INT = 1;

	/**
	 * The operation id for the '<em>Find Operator From Conv CDFG</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_SPECIFICATION___FIND_OPERATOR_FROM_CONV_CDFG__INT = 2;

	/**
	 * The operation id for the '<em>Find Operator From Instruction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_SPECIFICATION___FIND_OPERATOR_FROM_INSTRUCTION__INSTRUCTION = 3;

	/**
	 * The operation id for the '<em>Find Data From CDFG</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_SPECIFICATION___FIND_DATA_FROM_CDFG__INT = 4;

	/**
	 * The operation id for the '<em>Find Data From Symbol</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_SPECIFICATION___FIND_DATA_FROM_SYMBOL__SYMBOL = 5;

	/**
	 * The operation id for the '<em>Update Data Fixed Information</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_SPECIFICATION___UPDATE_DATA_FIXED_INFORMATION = 6;

	/**
	 * The operation id for the '<em>Check Validity</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_SPECIFICATION___CHECK_VALIDITY = 7;

	/**
	 * The number of operations of the '<em>Fixed Point Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_SPECIFICATION_OPERATION_COUNT = 8;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.CDFGNodeImpl <em>CDFG Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.CDFGNodeImpl
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl#getCDFGNode()
	 * @generated
	 */
	int CDFG_NODE = 5;

	/**
	 * The feature id for the '<em><b>Successors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDFG_NODE__SUCCESSORS = 0;

	/**
	 * The feature id for the '<em><b>Predecessors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDFG_NODE__PREDECESSORS = 1;

	/**
	 * The feature id for the '<em><b>Sfg Node Instances</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDFG_NODE__SFG_NODE_INSTANCES = 2;

	/**
	 * The number of structural features of the '<em>CDFG Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDFG_NODE_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Get Output Info</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDFG_NODE___GET_OUTPUT_INFO = 0;

	/**
	 * The operation id for the '<em>Get Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDFG_NODE___GET_NAME = 1;

	/**
	 * The number of operations of the '<em>CDFG Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDFG_NODE_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.DataImpl <em>Data</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.DataImpl
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl#getData()
	 * @generated
	 */
	int DATA = 1;

	/**
	 * The feature id for the '<em><b>Successors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA__SUCCESSORS = CDFG_NODE__SUCCESSORS;

	/**
	 * The feature id for the '<em><b>Predecessors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA__PREDECESSORS = CDFG_NODE__PREDECESSORS;

	/**
	 * The feature id for the '<em><b>Sfg Node Instances</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA__SFG_NODE_INSTANCES = CDFG_NODE__SFG_NODE_INSTANCES;

	/**
	 * The feature id for the '<em><b>Fixed Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA__FIXED_INFORMATION = CDFG_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Corresponding Symbol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA__CORRESPONDING_SYMBOL = CDFG_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CDFG Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA__CDFG_NUMBER = CDFG_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FEATURE_COUNT = CDFG_NODE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Output Info</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA___GET_OUTPUT_INFO = CDFG_NODE___GET_OUTPUT_INFO;

	/**
	 * The operation id for the '<em>Get Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA___GET_NAME = CDFG_NODE___GET_NAME;

	/**
	 * The operation id for the '<em>Get Successor Operators</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA___GET_SUCCESSOR_OPERATORS = CDFG_NODE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Predecessor Operators</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA___GET_PREDECESSOR_OPERATORS = CDFG_NODE_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_OPERATION_COUNT = CDFG_NODE_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.OperationImpl <em>Operator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.OperationImpl
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl#getOperator()
	 * @generated
	 */
	int OPERATOR = 2;

	/**
	 * The feature id for the '<em><b>Successors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__SUCCESSORS = CDFG_NODE__SUCCESSORS;

	/**
	 * The feature id for the '<em><b>Predecessors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__PREDECESSORS = CDFG_NODE__PREDECESSORS;

	/**
	 * The feature id for the '<em><b>Sfg Node Instances</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__SFG_NODE_INSTANCES = CDFG_NODE__SFG_NODE_INSTANCES;

	/**
	 * The feature id for the '<em><b>Operands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__OPERANDS = CDFG_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Eval cdfg number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__EVAL_CDFG_NUMBER = CDFG_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Conv cdfg number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__CONV_CDFG_NUMBER = CDFG_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__KIND = CDFG_NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Cost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__COST = CDFG_NODE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Corresponding Instructions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__CORRESPONDING_INSTRUCTIONS = CDFG_NODE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Usage Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__USAGE_NUMBER = CDFG_NODE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Instances</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__INSTANCES = CDFG_NODE_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Operator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_FEATURE_COUNT = CDFG_NODE_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>Get Output Info</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR___GET_OUTPUT_INFO = CDFG_NODE___GET_OUTPUT_INFO;

	/**
	 * The operation id for the '<em>Get Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR___GET_NAME = CDFG_NODE___GET_NAME;

	/**
	 * The operation id for the '<em>Set Instance</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR___SET_INSTANCE__INT = CDFG_NODE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Have Next Instance</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR___HAVE_NEXT_INSTANCE = CDFG_NODE_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Max Instance</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR___GET_MAX_INSTANCE = CDFG_NODE_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Current Instance</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR___GET_CURRENT_INSTANCE = CDFG_NODE_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Next Instance</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR___NEXT_INSTANCE = CDFG_NODE_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Previous Instance</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR___PREVIOUS_INSTANCE = CDFG_NODE_OPERATION_COUNT + 5;

	/**
	 * The number of operations of the '<em>Operator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_OPERATION_COUNT = CDFG_NODE_OPERATION_COUNT + 6;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedPointInformationImpl <em>Fixed Point Information</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedPointInformationImpl
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl#getFixedPointInformation()
	 * @generated
	 */
	int FIXED_POINT_INFORMATION = 3;

	/**
	 * The feature id for the '<em><b>Fixed Bit Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_INFORMATION__FIXED_BIT_WIDTH = 0;

	/**
	 * The feature id for the '<em><b>Signed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_INFORMATION__SIGNED = 1;

	/**
	 * The feature id for the '<em><b>Bit Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_INFORMATION__BIT_WIDTH = 2;

	/**
	 * The feature id for the '<em><b>Integer Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_INFORMATION__INTEGER_WIDTH = 3;

	/**
	 * The feature id for the '<em><b>Overflow</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_INFORMATION__OVERFLOW = 4;

	/**
	 * The feature id for the '<em><b>Quantification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_INFORMATION__QUANTIFICATION = 5;

	/**
	 * The feature id for the '<em><b>Dynamic</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_INFORMATION__DYNAMIC = 6;

	/**
	 * The feature id for the '<em><b>Dynamic Integer Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_INFORMATION__DYNAMIC_INTEGER_WIDTH = 7;

	/**
	 * The number of structural features of the '<em>Fixed Point Information</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_INFORMATION_FEATURE_COUNT = 8;

	/**
	 * The operation id for the '<em>Get Fractional Width</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_INFORMATION___GET_FRACTIONAL_WIDTH = 0;

	/**
	 * The number of operations of the '<em>Fixed Point Information</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_POINT_INFORMATION_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.DynamicImpl <em>Dynamic</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.DynamicImpl
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl#getDynamic()
	 * @generated
	 */
	int DYNAMIC = 4;

	/**
	 * The feature id for the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DYNAMIC__MIN = 0;

	/**
	 * The feature id for the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DYNAMIC__MAX = 1;

	/**
	 * The number of structural features of the '<em>Dynamic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DYNAMIC_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Dynamic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DYNAMIC_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.CDFGEdgeImpl <em>CDFG Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.CDFGEdgeImpl
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl#getCDFGEdge()
	 * @generated
	 */
	int CDFG_EDGE = 6;

	/**
	 * The feature id for the '<em><b>Predecessor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDFG_EDGE__PREDECESSOR = 0;

	/**
	 * The feature id for the '<em><b>Successor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDFG_EDGE__SUCCESSOR = 1;

	/**
	 * The feature id for the '<em><b>Pred Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDFG_EDGE__PRED_INDEX = 2;

	/**
	 * The number of structural features of the '<em>CDFG Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDFG_EDGE_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Get Predecessor Info</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDFG_EDGE___GET_PREDECESSOR_INFO = 0;

	/**
	 * The operation id for the '<em>Get Successor Info</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDFG_EDGE___GET_SUCCESSOR_INFO = 1;

	/**
	 * The number of operations of the '<em>CDFG Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDFG_EDGE_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE <em>OVERFLOW TYPE</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl#getOVERFLOW_TYPE()
	 * @generated
	 */
	int OVERFLOW_TYPE = 7;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE <em>QUANTIFICATION TYPE</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl#getQUANTIFICATION_TYPE()
	 * @generated
	 */
	int QUANTIFICATION_TYPE = 8;


	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind <em>Operator Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl#getOperatorKind()
	 * @generated
	 */
	int OPERATOR_KIND = 9;


	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification <em>Fixed Point Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fixed Point Specification</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification
	 * @generated
	 */
	EClass getFixedPointSpecification();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#getEdges <em>Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Edges</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#getEdges()
	 * @see #getFixedPointSpecification()
	 * @generated
	 */
	EReference getFixedPointSpecification_Edges();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#getDatas <em>Datas</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Datas</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#getDatas()
	 * @see #getFixedPointSpecification()
	 * @generated
	 */
	EReference getFixedPointSpecification_Datas();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#getOperators <em>Operators</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operators</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#getOperations()
	 * @see #getFixedPointSpecification()
	 * @generated
	 */
	EReference getFixedPointSpecification_Operators();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#updateDynamicInformation(java.lang.String) <em>Update Dynamic Information</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Dynamic Information</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#updateDynamicInformation(java.lang.String)
	 * @generated
	 */
	EOperation getFixedPointSpecification__UpdateDynamicInformation__String();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#findOperatorFromEvalCDFG(int) <em>Find Operator From Eval CDFG</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Find Operator From Eval CDFG</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#findOperatorFromEvalCDFG(int)
	 * @generated
	 */
	EOperation getFixedPointSpecification__FindOperatorFromEvalCDFG__int();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#findOperatorFromConvCDFG(int) <em>Find Operator From Conv CDFG</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Find Operator From Conv CDFG</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#findOperatorFromConvCDFG(int)
	 * @generated
	 */
	EOperation getFixedPointSpecification__FindOperatorFromConvCDFG__int();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#findOperatorFromInstruction(gecos.instrs.Instruction) <em>Find Operator From Instruction</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Find Operator From Instruction</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#findOperatorFromInstruction(gecos.instrs.Instruction)
	 * @generated
	 */
	EOperation getFixedPointSpecification__FindOperatorFromInstruction__Instruction();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#findDataFromCDFG(int) <em>Find Data From CDFG</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Find Data From CDFG</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#findDataFromCDFG(int)
	 * @generated
	 */
	EOperation getFixedPointSpecification__FindDataFromCDFG__int();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#findDataFromSymbol(gecos.core.Symbol) <em>Find Data From Symbol</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Find Data From Symbol</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#findDataFromSymbol(gecos.core.Symbol)
	 * @generated
	 */
	EOperation getFixedPointSpecification__FindDataFromSymbol__Symbol();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#updateDataFixedInformation() <em>Update Data Fixed Information</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Data Fixed Information</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#updateDataFixedInformation()
	 * @generated
	 */
	EOperation getFixedPointSpecification__UpdateDataFixedInformation();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#checkValidity() <em>Check Validity</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Validity</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification#checkValidity()
	 * @generated
	 */
	EOperation getFixedPointSpecification__CheckValidity();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Data <em>Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Data
	 * @generated
	 */
	EClass getData();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Data#getFixedInformation <em>Fixed Information</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Fixed Information</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Data#getFixedInformation()
	 * @see #getData()
	 * @generated
	 */
	EReference getData_FixedInformation();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Data#getCorrespondingSymbol <em>Corresponding Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Corresponding Symbol</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Data#getCorrespondingSymbol()
	 * @see #getData()
	 * @generated
	 */
	EReference getData_CorrespondingSymbol();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Data#getCDFGNumber <em>CDFG Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>CDFG Number</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Data#getCDFGNumber()
	 * @see #getData()
	 * @generated
	 */
	EAttribute getData_CDFGNumber();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Data#getSuccessorOperators() <em>Get Successor Operators</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Successor Operators</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Data#getSuccessorOperators()
	 * @generated
	 */
	EOperation getData__GetSuccessorOperators();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Data#getPredecessorOperators() <em>Get Predecessor Operators</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Predecessor Operators</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Data#getPredecessorOperators()
	 * @generated
	 */
	EOperation getData__GetPredecessorOperators();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operator</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Operation
	 * @generated
	 */
	EClass getOperator();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getOperands <em>Operands</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operands</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getOperands()
	 * @see #getOperator()
	 * @generated
	 */
	EReference getOperator_Operands();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getEval_cdfg_number <em>Eval cdfg number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Eval cdfg number</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getEval_cdfg_number()
	 * @see #getOperator()
	 * @generated
	 */
	EAttribute getOperator_Eval_cdfg_number();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getConv_cdfg_number <em>Conv cdfg number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Conv cdfg number</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getConv_cdfg_number()
	 * @see #getOperator()
	 * @generated
	 */
	EAttribute getOperator_Conv_cdfg_number();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getKind()
	 * @see #getOperator()
	 * @generated
	 */
	EAttribute getOperator_Kind();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getCost <em>Cost</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cost</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getCost()
	 * @see #getOperator()
	 * @generated
	 */
	EAttribute getOperator_Cost();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getCorrespondingInstructions <em>Corresponding Instructions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Corresponding Instructions</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getCorrespondingInstructions()
	 * @see #getOperator()
	 * @generated
	 */
	EReference getOperator_CorrespondingInstructions();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getUsageNumber <em>Usage Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Usage Number</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getUsageNumber()
	 * @see #getOperator()
	 * @generated
	 */
	EAttribute getOperator_UsageNumber();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getInstances <em>Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Instances</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getInstances()
	 * @see #getOperator()
	 * @generated
	 */
	EReference getOperator_Instances();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#setInstance(int) <em>Set Instance</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Instance</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#setInstance(int)
	 * @generated
	 */
	EOperation getOperator__SetInstance__int();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#haveNextInstance() <em>Have Next Instance</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Have Next Instance</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#haveNextInstance()
	 * @generated
	 */
	EOperation getOperator__HaveNextInstance();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getMaxInstance() <em>Get Max Instance</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Max Instance</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getMaxInstance()
	 * @generated
	 */
	EOperation getOperator__GetMaxInstance();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getCurrentInstance() <em>Get Current Instance</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Current Instance</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#getCurrentInstance()
	 * @generated
	 */
	EOperation getOperator__GetCurrentInstance();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#nextInstance() <em>Next Instance</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Next Instance</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#nextInstance()
	 * @generated
	 */
	EOperation getOperator__NextInstance();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#previousInstance() <em>Previous Instance</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Previous Instance</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Operation#previousInstance()
	 * @generated
	 */
	EOperation getOperator__PreviousInstance();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation <em>Fixed Point Information</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fixed Point Information</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation
	 * @generated
	 */
	EClass getFixedPointInformation();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#isFixedBitWidth <em>Fixed Bit Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fixed Bit Width</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#isFixedBitWidth()
	 * @see #getFixedPointInformation()
	 * @generated
	 */
	EAttribute getFixedPointInformation_FixedBitWidth();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getBitWidth <em>Bit Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bit Width</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getBitWidth()
	 * @see #getFixedPointInformation()
	 * @generated
	 */
	EAttribute getFixedPointInformation_BitWidth();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getIntegerWidth <em>Integer Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Integer Width</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getIntegerWidth()
	 * @see #getFixedPointInformation()
	 * @generated
	 */
	EAttribute getFixedPointInformation_IntegerWidth();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#isSigned <em>Signed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Signed</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#isSigned()
	 * @see #getFixedPointInformation()
	 * @generated
	 */
	EAttribute getFixedPointInformation_Signed();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getOverflow <em>Overflow</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Overflow</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getOverflow()
	 * @see #getFixedPointInformation()
	 * @generated
	 */
	EAttribute getFixedPointInformation_Overflow();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getQuantification <em>Quantification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantification</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getQuantification()
	 * @see #getFixedPointInformation()
	 * @generated
	 */
	EAttribute getFixedPointInformation_Quantification();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getDynamic <em>Dynamic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dynamic</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getDynamic()
	 * @see #getFixedPointInformation()
	 * @generated
	 */
	EReference getFixedPointInformation_Dynamic();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getDynamicIntegerWidth <em>Dynamic Integer Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dynamic Integer Width</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getDynamicIntegerWidth()
	 * @see #getFixedPointInformation()
	 * @generated
	 */
	EAttribute getFixedPointInformation_DynamicIntegerWidth();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getFractionalWidth() <em>Get Fractional Width</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Fractional Width</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getFractionalWidth()
	 * @generated
	 */
	EOperation getFixedPointInformation__GetFractionalWidth();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic <em>Dynamic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dynamic</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic
	 * @generated
	 */
	EClass getDynamic();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic#getMin()
	 * @see #getDynamic()
	 * @generated
	 */
	EAttribute getDynamic_Min();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.Dynamic#getMax()
	 * @see #getDynamic()
	 * @generated
	 */
	EAttribute getDynamic_Max();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode <em>CDFG Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CDFG Node</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode
	 * @generated
	 */
	EClass getCDFGNode();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode#getSuccessors <em>Successors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Successors</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode#getSuccessors()
	 * @see #getCDFGNode()
	 * @generated
	 */
	EReference getCDFGNode_Successors();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode#getPredecessors <em>Predecessors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Predecessors</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode#getPredecessors()
	 * @see #getCDFGNode()
	 * @generated
	 */
	EReference getCDFGNode_Predecessors();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode#getSfgNodeInstances <em>Sfg Node Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sfg Node Instances</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode#getSfgNodeInstances()
	 * @see #getCDFGNode()
	 * @generated
	 */
	EReference getCDFGNode_SfgNodeInstances();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode#getOutputInfo() <em>Get Output Info</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Output Info</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode#getOutputInfo()
	 * @generated
	 */
	EOperation getCDFGNode__GetOutputInfo();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode#getName() <em>Get Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Name</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode#getName()
	 * @generated
	 */
	EOperation getCDFGNode__GetName();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge <em>CDFG Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CDFG Edge</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge
	 * @generated
	 */
	EClass getCDFGEdge();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge#getPredecessor <em>Predecessor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Predecessor</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge#getPredecessor()
	 * @see #getCDFGEdge()
	 * @generated
	 */
	EReference getCDFGEdge_Predecessor();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge#getSuccessor <em>Successor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Successor</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge#getSuccessor()
	 * @see #getCDFGEdge()
	 * @generated
	 */
	EReference getCDFGEdge_Successor();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge#getPredIndex <em>Pred Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pred Index</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge#getPredIndex()
	 * @see #getCDFGEdge()
	 * @generated
	 */
	EAttribute getCDFGEdge_PredIndex();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge#getPredecessorInfo() <em>Get Predecessor Info</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Predecessor Info</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge#getPredecessorInfo()
	 * @generated
	 */
	EOperation getCDFGEdge__GetPredecessorInfo();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge#getSuccessorInfo() <em>Get Successor Info</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Successor Info</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge#getSuccessorInfo()
	 * @generated
	 */
	EOperation getCDFGEdge__GetSuccessorInfo();

	/**
	 * Returns the meta object for enum '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE <em>OVERFLOW TYPE</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>OVERFLOW TYPE</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE
	 * @generated
	 */
	EEnum getOVERFLOW_TYPE();

	/**
	 * Returns the meta object for enum '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE <em>QUANTIFICATION TYPE</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>QUANTIFICATION TYPE</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE
	 * @generated
	 */
	EEnum getQUANTIFICATION_TYPE();

	/**
	 * Returns the meta object for enum '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind <em>Operator Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Operator Kind</em>'.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind
	 * @generated
	 */
	EEnum getOperatorKind();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FixedpointspecificationFactory getFixedpointspecificationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedPointSpecificationImpl <em>Fixed Point Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedPointSpecificationImpl
		 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl#getFixedPointSpecification()
		 * @generated
		 */
		EClass FIXED_POINT_SPECIFICATION = eINSTANCE.getFixedPointSpecification();

		/**
		 * The meta object literal for the '<em><b>Edges</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIXED_POINT_SPECIFICATION__EDGES = eINSTANCE.getFixedPointSpecification_Edges();

		/**
		 * The meta object literal for the '<em><b>Datas</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIXED_POINT_SPECIFICATION__DATAS = eINSTANCE.getFixedPointSpecification_Datas();

		/**
		 * The meta object literal for the '<em><b>Operators</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIXED_POINT_SPECIFICATION__OPERATORS = eINSTANCE.getFixedPointSpecification_Operators();

		/**
		 * The meta object literal for the '<em><b>Update Dynamic Information</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FIXED_POINT_SPECIFICATION___UPDATE_DYNAMIC_INFORMATION__STRING = eINSTANCE.getFixedPointSpecification__UpdateDynamicInformation__String();

		/**
		 * The meta object literal for the '<em><b>Find Operator From Eval CDFG</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FIXED_POINT_SPECIFICATION___FIND_OPERATOR_FROM_EVAL_CDFG__INT = eINSTANCE.getFixedPointSpecification__FindOperatorFromEvalCDFG__int();

		/**
		 * The meta object literal for the '<em><b>Find Operator From Conv CDFG</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FIXED_POINT_SPECIFICATION___FIND_OPERATOR_FROM_CONV_CDFG__INT = eINSTANCE.getFixedPointSpecification__FindOperatorFromConvCDFG__int();

		/**
		 * The meta object literal for the '<em><b>Find Operator From Instruction</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FIXED_POINT_SPECIFICATION___FIND_OPERATOR_FROM_INSTRUCTION__INSTRUCTION = eINSTANCE.getFixedPointSpecification__FindOperatorFromInstruction__Instruction();

		/**
		 * The meta object literal for the '<em><b>Find Data From CDFG</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FIXED_POINT_SPECIFICATION___FIND_DATA_FROM_CDFG__INT = eINSTANCE.getFixedPointSpecification__FindDataFromCDFG__int();

		/**
		 * The meta object literal for the '<em><b>Find Data From Symbol</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FIXED_POINT_SPECIFICATION___FIND_DATA_FROM_SYMBOL__SYMBOL = eINSTANCE.getFixedPointSpecification__FindDataFromSymbol__Symbol();

		/**
		 * The meta object literal for the '<em><b>Update Data Fixed Information</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FIXED_POINT_SPECIFICATION___UPDATE_DATA_FIXED_INFORMATION = eINSTANCE.getFixedPointSpecification__UpdateDataFixedInformation();

		/**
		 * The meta object literal for the '<em><b>Check Validity</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FIXED_POINT_SPECIFICATION___CHECK_VALIDITY = eINSTANCE.getFixedPointSpecification__CheckValidity();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.DataImpl <em>Data</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.DataImpl
		 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl#getData()
		 * @generated
		 */
		EClass DATA = eINSTANCE.getData();

		/**
		 * The meta object literal for the '<em><b>Fixed Information</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA__FIXED_INFORMATION = eINSTANCE.getData_FixedInformation();

		/**
		 * The meta object literal for the '<em><b>Corresponding Symbol</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA__CORRESPONDING_SYMBOL = eINSTANCE.getData_CorrespondingSymbol();

		/**
		 * The meta object literal for the '<em><b>CDFG Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA__CDFG_NUMBER = eINSTANCE.getData_CDFGNumber();

		/**
		 * The meta object literal for the '<em><b>Get Successor Operators</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DATA___GET_SUCCESSOR_OPERATORS = eINSTANCE.getData__GetSuccessorOperators();

		/**
		 * The meta object literal for the '<em><b>Get Predecessor Operators</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DATA___GET_PREDECESSOR_OPERATORS = eINSTANCE.getData__GetPredecessorOperators();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.OperationImpl <em>Operator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.OperationImpl
		 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl#getOperator()
		 * @generated
		 */
		EClass OPERATOR = eINSTANCE.getOperator();

		/**
		 * The meta object literal for the '<em><b>Operands</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATOR__OPERANDS = eINSTANCE.getOperator_Operands();

		/**
		 * The meta object literal for the '<em><b>Eval cdfg number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATOR__EVAL_CDFG_NUMBER = eINSTANCE.getOperator_Eval_cdfg_number();

		/**
		 * The meta object literal for the '<em><b>Conv cdfg number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATOR__CONV_CDFG_NUMBER = eINSTANCE.getOperator_Conv_cdfg_number();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATOR__KIND = eINSTANCE.getOperator_Kind();

		/**
		 * The meta object literal for the '<em><b>Cost</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATOR__COST = eINSTANCE.getOperator_Cost();

		/**
		 * The meta object literal for the '<em><b>Corresponding Instructions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATOR__CORRESPONDING_INSTRUCTIONS = eINSTANCE.getOperator_CorrespondingInstructions();

		/**
		 * The meta object literal for the '<em><b>Usage Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATOR__USAGE_NUMBER = eINSTANCE.getOperator_UsageNumber();

		/**
		 * The meta object literal for the '<em><b>Instances</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATOR__INSTANCES = eINSTANCE.getOperator_Instances();

		/**
		 * The meta object literal for the '<em><b>Set Instance</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OPERATOR___SET_INSTANCE__INT = eINSTANCE.getOperator__SetInstance__int();

		/**
		 * The meta object literal for the '<em><b>Have Next Instance</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OPERATOR___HAVE_NEXT_INSTANCE = eINSTANCE.getOperator__HaveNextInstance();

		/**
		 * The meta object literal for the '<em><b>Get Max Instance</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OPERATOR___GET_MAX_INSTANCE = eINSTANCE.getOperator__GetMaxInstance();

		/**
		 * The meta object literal for the '<em><b>Get Current Instance</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OPERATOR___GET_CURRENT_INSTANCE = eINSTANCE.getOperator__GetCurrentInstance();

		/**
		 * The meta object literal for the '<em><b>Next Instance</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OPERATOR___NEXT_INSTANCE = eINSTANCE.getOperator__NextInstance();

		/**
		 * The meta object literal for the '<em><b>Previous Instance</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OPERATOR___PREVIOUS_INSTANCE = eINSTANCE.getOperator__PreviousInstance();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedPointInformationImpl <em>Fixed Point Information</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedPointInformationImpl
		 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl#getFixedPointInformation()
		 * @generated
		 */
		EClass FIXED_POINT_INFORMATION = eINSTANCE.getFixedPointInformation();

		/**
		 * The meta object literal for the '<em><b>Fixed Bit Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_POINT_INFORMATION__FIXED_BIT_WIDTH = eINSTANCE.getFixedPointInformation_FixedBitWidth();

		/**
		 * The meta object literal for the '<em><b>Bit Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_POINT_INFORMATION__BIT_WIDTH = eINSTANCE.getFixedPointInformation_BitWidth();

		/**
		 * The meta object literal for the '<em><b>Integer Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_POINT_INFORMATION__INTEGER_WIDTH = eINSTANCE.getFixedPointInformation_IntegerWidth();

		/**
		 * The meta object literal for the '<em><b>Signed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_POINT_INFORMATION__SIGNED = eINSTANCE.getFixedPointInformation_Signed();

		/**
		 * The meta object literal for the '<em><b>Overflow</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_POINT_INFORMATION__OVERFLOW = eINSTANCE.getFixedPointInformation_Overflow();

		/**
		 * The meta object literal for the '<em><b>Quantification</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_POINT_INFORMATION__QUANTIFICATION = eINSTANCE.getFixedPointInformation_Quantification();

		/**
		 * The meta object literal for the '<em><b>Dynamic</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIXED_POINT_INFORMATION__DYNAMIC = eINSTANCE.getFixedPointInformation_Dynamic();

		/**
		 * The meta object literal for the '<em><b>Dynamic Integer Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_POINT_INFORMATION__DYNAMIC_INTEGER_WIDTH = eINSTANCE.getFixedPointInformation_DynamicIntegerWidth();

		/**
		 * The meta object literal for the '<em><b>Get Fractional Width</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FIXED_POINT_INFORMATION___GET_FRACTIONAL_WIDTH = eINSTANCE.getFixedPointInformation__GetFractionalWidth();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.DynamicImpl <em>Dynamic</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.DynamicImpl
		 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl#getDynamic()
		 * @generated
		 */
		EClass DYNAMIC = eINSTANCE.getDynamic();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DYNAMIC__MIN = eINSTANCE.getDynamic_Min();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DYNAMIC__MAX = eINSTANCE.getDynamic_Max();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.CDFGNodeImpl <em>CDFG Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.CDFGNodeImpl
		 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl#getCDFGNode()
		 * @generated
		 */
		EClass CDFG_NODE = eINSTANCE.getCDFGNode();

		/**
		 * The meta object literal for the '<em><b>Successors</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CDFG_NODE__SUCCESSORS = eINSTANCE.getCDFGNode_Successors();

		/**
		 * The meta object literal for the '<em><b>Predecessors</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CDFG_NODE__PREDECESSORS = eINSTANCE.getCDFGNode_Predecessors();

		/**
		 * The meta object literal for the '<em><b>Sfg Node Instances</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CDFG_NODE__SFG_NODE_INSTANCES = eINSTANCE.getCDFGNode_SfgNodeInstances();

		/**
		 * The meta object literal for the '<em><b>Get Output Info</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CDFG_NODE___GET_OUTPUT_INFO = eINSTANCE.getCDFGNode__GetOutputInfo();

		/**
		 * The meta object literal for the '<em><b>Get Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CDFG_NODE___GET_NAME = eINSTANCE.getCDFGNode__GetName();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.CDFGEdgeImpl <em>CDFG Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.CDFGEdgeImpl
		 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl#getCDFGEdge()
		 * @generated
		 */
		EClass CDFG_EDGE = eINSTANCE.getCDFGEdge();

		/**
		 * The meta object literal for the '<em><b>Predecessor</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CDFG_EDGE__PREDECESSOR = eINSTANCE.getCDFGEdge_Predecessor();

		/**
		 * The meta object literal for the '<em><b>Successor</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CDFG_EDGE__SUCCESSOR = eINSTANCE.getCDFGEdge_Successor();

		/**
		 * The meta object literal for the '<em><b>Pred Index</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CDFG_EDGE__PRED_INDEX = eINSTANCE.getCDFGEdge_PredIndex();

		/**
		 * The meta object literal for the '<em><b>Get Predecessor Info</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CDFG_EDGE___GET_PREDECESSOR_INFO = eINSTANCE.getCDFGEdge__GetPredecessorInfo();

		/**
		 * The meta object literal for the '<em><b>Get Successor Info</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CDFG_EDGE___GET_SUCCESSOR_INFO = eINSTANCE.getCDFGEdge__GetSuccessorInfo();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE <em>OVERFLOW TYPE</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE
		 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl#getOVERFLOW_TYPE()
		 * @generated
		 */
		EEnum OVERFLOW_TYPE = eINSTANCE.getOVERFLOW_TYPE();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE <em>QUANTIFICATION TYPE</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE
		 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl#getQUANTIFICATION_TYPE()
		 * @generated
		 */
		EEnum QUANTIFICATION_TYPE = eINSTANCE.getQUANTIFICATION_TYPE();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind <em>Operator Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind
		 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.impl.FixedpointspecificationPackageImpl#getOperatorKind()
		 * @generated
		 */
		EEnum OPERATOR_KIND = eINSTANCE.getOperatorKind();

	}

} //FixedpointspecificationPackage
