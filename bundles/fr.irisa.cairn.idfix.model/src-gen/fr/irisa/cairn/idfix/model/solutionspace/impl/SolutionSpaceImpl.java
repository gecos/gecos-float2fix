/**
 */
package fr.irisa.cairn.idfix.model.solutionspace.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.irisa.cairn.idfix.model.fixedpointspecification.Data;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Operation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind;
import fr.irisa.cairn.idfix.model.solutionspace.LibraryElement;
import fr.irisa.cairn.idfix.model.solutionspace.SolutionSpace;
import fr.irisa.cairn.idfix.model.solutionspace.SolutionspacePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Solution Space</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.solutionspace.impl.SolutionSpaceImpl#getOperators <em>Operators</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.solutionspace.impl.SolutionSpaceImpl#getLibrary <em>Library</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.solutionspace.impl.SolutionSpaceImpl#getDatas <em>Datas</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SolutionSpaceImpl extends MinimalEObjectImpl.Container implements SolutionSpace {
	/**
	 * The cached value of the '{@link #getOperators() <em>Operators</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperators()
	 * @generated
	 * @ordered
	 */
	protected EList<Operation> operators;
	
	/**
	 * @generated NOT
	 */
	protected Map<Operation, Integer> savedOperators;

	/**
	 * The cached value of the '{@link #getLibrary() <em>Library</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLibrary()
	 * @generated
	 * @ordered
	 */
	protected EMap<OperatorKind, EList<LibraryElement>> library;

	/**
	 * The cached value of the '{@link #getDatas() <em>Datas</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatas()
	 * @generated
	 * @ordered
	 */
	protected EList<Data> datas;

	/**
	 * @generated NOT 
	 */
	protected Map<Operation, Integer> operatorToLibraryElementIndex;
	
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected SolutionSpaceImpl() {
		super();
		operatorToLibraryElementIndex = new HashMap<Operation, Integer>();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SolutionspacePackage.Literals.SOLUTION_SPACE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Operation> getOperators() {
		if (operators == null) {
			operators = new EObjectResolvingEList<Operation>(Operation.class, this, SolutionspacePackage.SOLUTION_SPACE__OPERATORS);
		}
		return operators;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<OperatorKind, EList<LibraryElement>> getLibrary() {
		if (library == null) {
			library = new EcoreEMap.Unsettable<OperatorKind,EList<LibraryElement>>(SolutionspacePackage.Literals.OPERATOR_TO_LIST_ELEMENT, OperatorToListElementImpl.class, this, SolutionspacePackage.SOLUTION_SPACE__LIBRARY);
		}
		return library;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetLibrary() {
		if (library != null) ((InternalEList.Unsettable<?>)library).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetLibrary() {
		return library != null && ((InternalEList.Unsettable<?>)library).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Data> getDatas() {
		if (datas == null) {
			datas = new EObjectResolvingEList<Data>(Data.class, this, SolutionspacePackage.SOLUTION_SPACE__DATAS);
		}
		return datas;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setAllOperatorToMax() {
		OperatorKind kind;
		LibraryElement elem;
		int maxIndex;
		for(Operation op : operators){
			kind = op.getKind();
			maxIndex = getMaxIndexOf(op);
			elem = library.get(kind).get(maxIndex);
			op.getOperands().get(0).setBitWidth(elem.getOperand().getFirst());
			op.getOperands().get(1).setBitWidth(elem.getOperand().getSecond());
			op.getOperands().get(2).setBitWidth(elem.getOperand().getThird());
			op.setCost(elem.getCost());
			operatorToLibraryElementIndex.put(op, maxIndex);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setAllOperatorToMin() {
		OperatorKind kind;
		LibraryElement elem;
		for(Operation op : operators){
			kind = op.getKind();
			elem = library.get(kind).get(0);
			op.getOperands().get(0).setBitWidth(elem.getOperand().getFirst());
			op.getOperands().get(1).setBitWidth(elem.getOperand().getSecond());
			op.getOperands().get(2).setBitWidth(elem.getOperand().getThird());
			op.setCost(elem.getCost());
			operatorToLibraryElementIndex.put(op, 0);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setOperatorTo(Operation operator, int index) {
		OperatorKind kind;
		LibraryElement elem;
		if(operators.contains(operator)){
			kind = operator.getKind();
			if(index <= getMaxIndexOf(operator) && index >= 0){
				elem = library.get(kind).get(index);
				operator.getOperands().get(0).setBitWidth(elem.getOperand().getFirst());
				operator.getOperands().get(1).setBitWidth(elem.getOperand().getSecond());
				operator.getOperands().get(2).setBitWidth(elem.getOperand().getThird());
				operator.setCost(elem.getCost());
				operatorToLibraryElementIndex.put(operator, index);
			}
			else{
				throw new RuntimeException("Impossible to use the index " + index + " because of the number of element in the operator library for this kind is " + getMaxIndexOf(operator));
			}
		}
		else{
			throw new RuntimeException("The operator " + operator + " is not contained in the solution space");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getMaxIndexOf(Operation operator) {
		OperatorKind kind;
		if(operators.contains(operator)){
			kind = operator.getKind();
			return  library.get(kind).size() - 1;
		}
		else{
			throw new RuntimeException("The operator " + operator + " is not contained in the solution space");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getCurrentIndexOf(Operation operator) {
		if(operators.contains(operator)){
			return operatorToLibraryElementIndex.get(operator);
		}
		else{
			throw new RuntimeException("The operator " + operator + " is not contained in the solution space");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void indexPlusOneTo(Operation operator) {
		if(operators.contains(operator)){
			int index = operatorToLibraryElementIndex.get(operator);
			setOperatorTo(operator, index + 1);
		}
		else{
			throw new RuntimeException("The operator " + operator + " is not contained in the solution space");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void indexMinusOneTo(Operation operator) {
		if(operators.contains(operator)){
			int index = operatorToLibraryElementIndex.get(operator);
			setOperatorTo(operator, index - 1);
		}
		else{
			throw new RuntimeException("The operator " + operator + " is not contained in the solution space");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void saveCurrentSolutionSpace() {
		savedOperators = new HashMap<Operation, Integer>();
		for(Operation op : operators){
			savedOperators.put(op, getCurrentIndexOf(op));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void useSolutionSpaceSaved() {
		if(savedOperators != null){
			for(Operation op : operators){
				setOperatorTo(op, savedOperators.get(op));
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getIndexMaxOf(OperatorKind operatorKind) {
		return library.get(operatorKind).size() - 1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setOperatorToMax(Operation op) {
		OperatorKind kind = op.getKind();
		int maxIndex = getMaxIndexOf(op);
		LibraryElement elem = library.get(kind).get(maxIndex);
		op.getOperands().get(0).setBitWidth(elem.getOperand().getFirst());
		op.getOperands().get(1).setBitWidth(elem.getOperand().getSecond());
		op.getOperands().get(2).setBitWidth(elem.getOperand().getThird());
		op.setCost(elem.getCost());
		operatorToLibraryElementIndex.put(op, maxIndex);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setAllOperatorsTo(int index) {
		OperatorKind kind;
		LibraryElement elem;
		for(Operation op : operators){
			if(index <= getMaxIndexOf(op) && index >= 0){
				kind = op.getKind();
				elem = library.get(kind).get(index);
				op.getOperands().get(0).setBitWidth(elem.getOperand().getFirst());
				op.getOperands().get(1).setBitWidth(elem.getOperand().getSecond());
				op.getOperands().get(2).setBitWidth(elem.getOperand().getThird());
				op.setCost(elem.getCost());
				operatorToLibraryElementIndex.put(op, index);
			}
			else{
				throw new RuntimeException("Impossible to use the index " + index + " because of the number of element in the operator library for this kind is " + getMaxIndexOf(op));
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isMaxIndexOf(Operation operator) {
		return getCurrentIndexOf(operator) == getMaxIndexOf(operator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SolutionspacePackage.SOLUTION_SPACE__LIBRARY:
				return ((InternalEList<?>)getLibrary()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SolutionspacePackage.SOLUTION_SPACE__OPERATORS:
				return getOperators();
			case SolutionspacePackage.SOLUTION_SPACE__LIBRARY:
				if (coreType) return getLibrary();
				else return getLibrary().map();
			case SolutionspacePackage.SOLUTION_SPACE__DATAS:
				return getDatas();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SolutionspacePackage.SOLUTION_SPACE__OPERATORS:
				getOperators().clear();
				getOperators().addAll((Collection<? extends Operation>)newValue);
				return;
			case SolutionspacePackage.SOLUTION_SPACE__LIBRARY:
				((EStructuralFeature.Setting)getLibrary()).set(newValue);
				return;
			case SolutionspacePackage.SOLUTION_SPACE__DATAS:
				getDatas().clear();
				getDatas().addAll((Collection<? extends Data>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SolutionspacePackage.SOLUTION_SPACE__OPERATORS:
				getOperators().clear();
				return;
			case SolutionspacePackage.SOLUTION_SPACE__LIBRARY:
				unsetLibrary();
				return;
			case SolutionspacePackage.SOLUTION_SPACE__DATAS:
				getDatas().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SolutionspacePackage.SOLUTION_SPACE__OPERATORS:
				return operators != null && !operators.isEmpty();
			case SolutionspacePackage.SOLUTION_SPACE__LIBRARY:
				return isSetLibrary();
			case SolutionspacePackage.SOLUTION_SPACE__DATAS:
				return datas != null && !datas.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case SolutionspacePackage.SOLUTION_SPACE___SET_ALL_OPERATOR_TO_MAX:
				setAllOperatorToMax();
				return null;
			case SolutionspacePackage.SOLUTION_SPACE___SET_ALL_OPERATOR_TO_MIN:
				setAllOperatorToMin();
				return null;
			case SolutionspacePackage.SOLUTION_SPACE___SET_OPERATOR_TO__OPERATOR_INT:
				setOperatorTo((Operation)arguments.get(0), (Integer)arguments.get(1));
				return null;
			case SolutionspacePackage.SOLUTION_SPACE___IS_MAX_INDEX_OF__OPERATOR:
				return isMaxIndexOf((Operation)arguments.get(0));
			case SolutionspacePackage.SOLUTION_SPACE___GET_MAX_INDEX_OF__OPERATOR:
				return getMaxIndexOf((Operation)arguments.get(0));
			case SolutionspacePackage.SOLUTION_SPACE___GET_CURRENT_INDEX_OF__OPERATOR:
				return getCurrentIndexOf((Operation)arguments.get(0));
			case SolutionspacePackage.SOLUTION_SPACE___INDEX_PLUS_ONE_TO__OPERATOR:
				indexPlusOneTo((Operation)arguments.get(0));
				return null;
			case SolutionspacePackage.SOLUTION_SPACE___INDEX_MINUS_ONE_TO__OPERATOR:
				indexMinusOneTo((Operation)arguments.get(0));
				return null;
			case SolutionspacePackage.SOLUTION_SPACE___SAVE_CURRENT_SOLUTION_SPACE:
				saveCurrentSolutionSpace();
				return null;
			case SolutionspacePackage.SOLUTION_SPACE___USE_SOLUTION_SPACE_SAVED:
				useSolutionSpaceSaved();
				return null;
			case SolutionspacePackage.SOLUTION_SPACE___GET_INDEX_MAX_OF__OPERATORKIND:
				return getIndexMaxOf((OperatorKind)arguments.get(0));
			case SolutionspacePackage.SOLUTION_SPACE___SET_OPERATOR_TO_MAX__OPERATOR:
				setOperatorToMax((Operation)arguments.get(0));
				return null;
			case SolutionspacePackage.SOLUTION_SPACE___SET_ALL_OPERATORS_TO__INT:
				setAllOperatorsTo((Integer)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public String toString(){
		StringBuffer result = new StringBuffer("SolutionSpace (\n");
		
		if(operators != null){
			for(Operation op : operators){
				result.append(op.toString());
				result.append("\n");
			}
			result.append("\n");
		}
		
		for(OperatorKind kind : library.keySet()){
			result.append(kind.getLiteral());
			result.append(" ");
			result.append(library.get(kind));
			result.append("\n");
		}
		
		result.append(')');
		return result.toString();
	}
} //SolutionSpaceImpl
