/**
 */
package fr.irisa.cairn.idfix.model.informationandtiming;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timing</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.informationandtiming.Timing#getSections <em>Sections</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage#getTiming()
 * @model
 * @generated
 */
public interface Timing extends EObject {
	/**
	 * Returns the value of the '<em><b>Sections</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.idfix.model.informationandtiming.Section}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sections</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sections</em>' containment reference list.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage#getTiming_Sections()
	 * @model containment="true"
	 * @generated
	 */
	EList<Section> getSections();

} // Timing
