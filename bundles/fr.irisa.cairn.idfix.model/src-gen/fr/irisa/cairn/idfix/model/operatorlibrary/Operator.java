/**
 */
package fr.irisa.cairn.idfix.model.operatorlibrary;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.operatorlibrary.Operator#getKind <em>Kind</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.operatorlibrary.Operator#getIntances <em>Intances</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryPackage#getOperator()
 * @model
 * @generated
 */
public interface Operator extends EObject {
	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.irisa.cairn.idfix.model.operatorlibrary.OPERATOR_KIND}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.OPERATOR_KIND
	 * @see #setKind(OPERATOR_KIND)
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryPackage#getOperator_Kind()
	 * @model
	 * @generated
	 */
	OPERATOR_KIND getKind();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.operatorlibrary.Operator#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.OPERATOR_KIND
	 * @see #getKind()
	 * @generated
	 */
	void setKind(OPERATOR_KIND value);

	/**
	 * Returns the value of the '<em><b>Intances</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.cairn.idfix.model.operatorlibrary.Instance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Intances</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Intances</em>' reference list.
	 * @see fr.irisa.cairn.idfix.model.operatorlibrary.OperatorlibraryPackage#getOperator_Intances()
	 * @model
	 * @generated
	 */
	EList<Instance> getIntances();

} // Operator
