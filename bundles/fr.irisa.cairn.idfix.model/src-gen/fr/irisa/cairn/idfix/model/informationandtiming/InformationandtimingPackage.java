/**
 */
package fr.irisa.cairn.idfix.model.informationandtiming;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingFactory
 * @model kind="package"
 * @generated
 */
public interface InformationandtimingPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "informationandtiming";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://idfix.fr/fr/irisa/cairn/idfix/model/informationandtiming";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "informationandtiming";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	InformationandtimingPackage eINSTANCE = fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationandtimingPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationsImpl <em>Informations</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationsImpl
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationandtimingPackageImpl#getInformations()
	 * @generated
	 */
	int INFORMATIONS = 0;

	/**
	 * The feature id for the '<em><b>Optimization Algorithm Used</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMATIONS__OPTIMIZATION_ALGORITHM_USED = 0;

	/**
	 * The feature id for the '<em><b>User Noise Constraint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMATIONS__USER_NOISE_CONSTRAINT = 1;

	/**
	 * The feature id for the '<em><b>Timing</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMATIONS__TIMING = 2;

	/**
	 * The feature id for the '<em><b>Use Last Result Mode Enable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMATIONS__USE_LAST_RESULT_MODE_ENABLE = 3;

	/**
	 * The feature id for the '<em><b>Analytic Informations</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMATIONS__ANALYTIC_INFORMATIONS = 4;

	/**
	 * The feature id for the '<em><b>Simulation Informations</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMATIONS__SIMULATION_INFORMATIONS = 5;

	/**
	 * The number of structural features of the '<em>Informations</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMATIONS_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Informations</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFORMATIONS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.TimingImpl <em>Timing</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.TimingImpl
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationandtimingPackageImpl#getTiming()
	 * @generated
	 */
	int TIMING = 1;

	/**
	 * The feature id for the '<em><b>Sections</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING__SECTIONS = 0;

	/**
	 * The number of structural features of the '<em>Timing</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Timing</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.TimeElementImpl <em>Time Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.TimeElementImpl
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationandtimingPackageImpl#getTimeElement()
	 * @generated
	 */
	int TIME_ELEMENT = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_ELEMENT__DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_ELEMENT__TIME = 1;

	/**
	 * The number of structural features of the '<em>Time Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_ELEMENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Time Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_ELEMENT_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.SectionImpl <em>Section</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.SectionImpl
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationandtimingPackageImpl#getSection()
	 * @generated
	 */
	int SECTION = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECTION__NAME = 0;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECTION__TIME = 1;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECTION__ELEMENTS = 2;

	/**
	 * The number of structural features of the '<em>Section</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECTION_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Add Time Log</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECTION___ADD_TIME_LOG__STRING_LONG_LONG = 0;

	/**
	 * The operation id for the '<em>Add Time Log</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECTION___ADD_TIME_LOG__STRING_FLOAT = 1;

	/**
	 * The number of operations of the '<em>Section</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECTION_OPERATION_COUNT = 2;


	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.AnalyticInformationsImpl <em>Analytic Informations</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.AnalyticInformationsImpl
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationandtimingPackageImpl#getAnalyticInformations()
	 * @generated
	 */
	int ANALYTIC_INFORMATIONS = 4;

	/**
	 * The feature id for the '<em><b>Final Noise Constraint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYTIC_INFORMATIONS__FINAL_NOISE_CONSTRAINT = 0;

	/**
	 * The feature id for the '<em><b>Final Cost Constraint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYTIC_INFORMATIONS__FINAL_COST_CONSTRAINT = 1;

	/**
	 * The feature id for the '<em><b>Noise Evalutation Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYTIC_INFORMATIONS__NOISE_EVALUTATION_NUMBER = 2;

	/**
	 * The feature id for the '<em><b>Cost Evaluation Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYTIC_INFORMATIONS__COST_EVALUATION_NUMBER = 3;

	/**
	 * The number of structural features of the '<em>Analytic Informations</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYTIC_INFORMATIONS_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Analytic Informations</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYTIC_INFORMATIONS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.SimulationInformationsImpl <em>Simulation Informations</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.SimulationInformationsImpl
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationandtimingPackageImpl#getSimulationInformations()
	 * @generated
	 */
	int SIMULATION_INFORMATIONS = 5;

	/**
	 * The feature id for the '<em><b>Sample Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_INFORMATIONS__SAMPLE_NUMBER = 0;

	/**
	 * The feature id for the '<em><b>Noise Power Result</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_INFORMATIONS__NOISE_POWER_RESULT = 1;

	/**
	 * The number of structural features of the '<em>Simulation Informations</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_INFORMATIONS_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Simulation Informations</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_INFORMATIONS_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.informationandtiming.Informations <em>Informations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Informations</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.Informations
	 * @generated
	 */
	EClass getInformations();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.informationandtiming.Informations#getOptimizationAlgorithmUsed <em>Optimization Algorithm Used</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Optimization Algorithm Used</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.Informations#getOptimizationAlgorithmUsed()
	 * @see #getInformations()
	 * @generated
	 */
	EAttribute getInformations_OptimizationAlgorithmUsed();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.informationandtiming.Informations#getUserNoiseConstraint <em>User Noise Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>User Noise Constraint</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.Informations#getUserNoiseConstraint()
	 * @see #getInformations()
	 * @generated
	 */
	EAttribute getInformations_UserNoiseConstraint();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.idfix.model.informationandtiming.Informations#getTiming <em>Timing</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Timing</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.Informations#getTiming()
	 * @see #getInformations()
	 * @generated
	 */
	EReference getInformations_Timing();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.informationandtiming.Informations#isUseLastResultModeEnable <em>Use Last Result Mode Enable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Use Last Result Mode Enable</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.Informations#isUseLastResultModeEnable()
	 * @see #getInformations()
	 * @generated
	 */
	EAttribute getInformations_UseLastResultModeEnable();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.idfix.model.informationandtiming.Informations#getAnalyticInformations <em>Analytic Informations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Analytic Informations</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.Informations#getAnalyticInformations()
	 * @see #getInformations()
	 * @generated
	 */
	EReference getInformations_AnalyticInformations();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.idfix.model.informationandtiming.Informations#getSimulationInformations <em>Simulation Informations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Simulation Informations</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.Informations#getSimulationInformations()
	 * @see #getInformations()
	 * @generated
	 */
	EReference getInformations_SimulationInformations();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.informationandtiming.Timing <em>Timing</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Timing</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.Timing
	 * @generated
	 */
	EClass getTiming();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.idfix.model.informationandtiming.Timing#getSections <em>Sections</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sections</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.Timing#getSections()
	 * @see #getTiming()
	 * @generated
	 */
	EReference getTiming_Sections();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.informationandtiming.TimeElement <em>Time Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Time Element</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.TimeElement
	 * @generated
	 */
	EClass getTimeElement();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.informationandtiming.TimeElement#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.TimeElement#getDescription()
	 * @see #getTimeElement()
	 * @generated
	 */
	EAttribute getTimeElement_Description();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.informationandtiming.TimeElement#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.TimeElement#getTime()
	 * @see #getTimeElement()
	 * @generated
	 */
	EAttribute getTimeElement_Time();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.informationandtiming.Section <em>Section</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Section</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.Section
	 * @generated
	 */
	EClass getSection();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.informationandtiming.Section#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.Section#getName()
	 * @see #getSection()
	 * @generated
	 */
	EAttribute getSection_Name();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.informationandtiming.Section#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.Section#getTime()
	 * @see #getSection()
	 * @generated
	 */
	EAttribute getSection_Time();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.idfix.model.informationandtiming.Section#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.Section#getElements()
	 * @see #getSection()
	 * @generated
	 */
	EReference getSection_Elements();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.informationandtiming.Section#addTimeLog(java.lang.String, long, long) <em>Add Time Log</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Time Log</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.Section#addTimeLog(java.lang.String, long, long)
	 * @generated
	 */
	EOperation getSection__AddTimeLog__String_long_long();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.idfix.model.informationandtiming.Section#addTimeLog(java.lang.String, float) <em>Add Time Log</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Time Log</em>' operation.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.Section#addTimeLog(java.lang.String, float)
	 * @generated
	 */
	EOperation getSection__AddTimeLog__String_float();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.informationandtiming.AnalyticInformations <em>Analytic Informations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Analytic Informations</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.AnalyticInformations
	 * @generated
	 */
	EClass getAnalyticInformations();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.informationandtiming.AnalyticInformations#getFinalNoiseConstraint <em>Final Noise Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final Noise Constraint</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.AnalyticInformations#getFinalNoiseConstraint()
	 * @see #getAnalyticInformations()
	 * @generated
	 */
	EAttribute getAnalyticInformations_FinalNoiseConstraint();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.informationandtiming.AnalyticInformations#getFinalCostConstraint <em>Final Cost Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final Cost Constraint</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.AnalyticInformations#getFinalCostConstraint()
	 * @see #getAnalyticInformations()
	 * @generated
	 */
	EAttribute getAnalyticInformations_FinalCostConstraint();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.informationandtiming.AnalyticInformations#getNoiseEvalutationNumber <em>Noise Evalutation Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Noise Evalutation Number</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.AnalyticInformations#getNoiseEvalutationNumber()
	 * @see #getAnalyticInformations()
	 * @generated
	 */
	EAttribute getAnalyticInformations_NoiseEvalutationNumber();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.informationandtiming.AnalyticInformations#getCostEvaluationNumber <em>Cost Evaluation Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cost Evaluation Number</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.AnalyticInformations#getCostEvaluationNumber()
	 * @see #getAnalyticInformations()
	 * @generated
	 */
	EAttribute getAnalyticInformations_CostEvaluationNumber();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.idfix.model.informationandtiming.SimulationInformations <em>Simulation Informations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simulation Informations</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.SimulationInformations
	 * @generated
	 */
	EClass getSimulationInformations();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.informationandtiming.SimulationInformations#getSampleNumber <em>Sample Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sample Number</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.SimulationInformations#getSampleNumber()
	 * @see #getSimulationInformations()
	 * @generated
	 */
	EAttribute getSimulationInformations_SampleNumber();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.idfix.model.informationandtiming.SimulationInformations#getNoisePowerResult <em>Noise Power Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Noise Power Result</em>'.
	 * @see fr.irisa.cairn.idfix.model.informationandtiming.SimulationInformations#getNoisePowerResult()
	 * @see #getSimulationInformations()
	 * @generated
	 */
	EAttribute getSimulationInformations_NoisePowerResult();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	InformationandtimingFactory getInformationandtimingFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationsImpl <em>Informations</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationsImpl
		 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationandtimingPackageImpl#getInformations()
		 * @generated
		 */
		EClass INFORMATIONS = eINSTANCE.getInformations();

		/**
		 * The meta object literal for the '<em><b>Optimization Algorithm Used</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INFORMATIONS__OPTIMIZATION_ALGORITHM_USED = eINSTANCE.getInformations_OptimizationAlgorithmUsed();

		/**
		 * The meta object literal for the '<em><b>User Noise Constraint</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INFORMATIONS__USER_NOISE_CONSTRAINT = eINSTANCE.getInformations_UserNoiseConstraint();

		/**
		 * The meta object literal for the '<em><b>Timing</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INFORMATIONS__TIMING = eINSTANCE.getInformations_Timing();

		/**
		 * The meta object literal for the '<em><b>Use Last Result Mode Enable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INFORMATIONS__USE_LAST_RESULT_MODE_ENABLE = eINSTANCE.getInformations_UseLastResultModeEnable();

		/**
		 * The meta object literal for the '<em><b>Analytic Informations</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INFORMATIONS__ANALYTIC_INFORMATIONS = eINSTANCE.getInformations_AnalyticInformations();

		/**
		 * The meta object literal for the '<em><b>Simulation Informations</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INFORMATIONS__SIMULATION_INFORMATIONS = eINSTANCE.getInformations_SimulationInformations();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.TimingImpl <em>Timing</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.TimingImpl
		 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationandtimingPackageImpl#getTiming()
		 * @generated
		 */
		EClass TIMING = eINSTANCE.getTiming();

		/**
		 * The meta object literal for the '<em><b>Sections</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMING__SECTIONS = eINSTANCE.getTiming_Sections();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.TimeElementImpl <em>Time Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.TimeElementImpl
		 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationandtimingPackageImpl#getTimeElement()
		 * @generated
		 */
		EClass TIME_ELEMENT = eINSTANCE.getTimeElement();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_ELEMENT__DESCRIPTION = eINSTANCE.getTimeElement_Description();

		/**
		 * The meta object literal for the '<em><b>Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_ELEMENT__TIME = eINSTANCE.getTimeElement_Time();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.SectionImpl <em>Section</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.SectionImpl
		 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationandtimingPackageImpl#getSection()
		 * @generated
		 */
		EClass SECTION = eINSTANCE.getSection();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SECTION__NAME = eINSTANCE.getSection_Name();

		/**
		 * The meta object literal for the '<em><b>Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SECTION__TIME = eINSTANCE.getSection_Time();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECTION__ELEMENTS = eINSTANCE.getSection_Elements();

		/**
		 * The meta object literal for the '<em><b>Add Time Log</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SECTION___ADD_TIME_LOG__STRING_LONG_LONG = eINSTANCE.getSection__AddTimeLog__String_long_long();

		/**
		 * The meta object literal for the '<em><b>Add Time Log</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SECTION___ADD_TIME_LOG__STRING_FLOAT = eINSTANCE.getSection__AddTimeLog__String_float();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.AnalyticInformationsImpl <em>Analytic Informations</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.AnalyticInformationsImpl
		 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationandtimingPackageImpl#getAnalyticInformations()
		 * @generated
		 */
		EClass ANALYTIC_INFORMATIONS = eINSTANCE.getAnalyticInformations();

		/**
		 * The meta object literal for the '<em><b>Final Noise Constraint</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANALYTIC_INFORMATIONS__FINAL_NOISE_CONSTRAINT = eINSTANCE.getAnalyticInformations_FinalNoiseConstraint();

		/**
		 * The meta object literal for the '<em><b>Final Cost Constraint</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANALYTIC_INFORMATIONS__FINAL_COST_CONSTRAINT = eINSTANCE.getAnalyticInformations_FinalCostConstraint();

		/**
		 * The meta object literal for the '<em><b>Noise Evalutation Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANALYTIC_INFORMATIONS__NOISE_EVALUTATION_NUMBER = eINSTANCE.getAnalyticInformations_NoiseEvalutationNumber();

		/**
		 * The meta object literal for the '<em><b>Cost Evaluation Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANALYTIC_INFORMATIONS__COST_EVALUATION_NUMBER = eINSTANCE.getAnalyticInformations_CostEvaluationNumber();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.idfix.model.informationandtiming.impl.SimulationInformationsImpl <em>Simulation Informations</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.SimulationInformationsImpl
		 * @see fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationandtimingPackageImpl#getSimulationInformations()
		 * @generated
		 */
		EClass SIMULATION_INFORMATIONS = eINSTANCE.getSimulationInformations();

		/**
		 * The meta object literal for the '<em><b>Sample Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_INFORMATIONS__SAMPLE_NUMBER = eINSTANCE.getSimulationInformations_SampleNumber();

		/**
		 * The meta object literal for the '<em><b>Noise Power Result</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_INFORMATIONS__NOISE_POWER_RESULT = eINSTANCE.getSimulationInformations_NoisePowerResult();

	}

} //InformationandtimingPackage
