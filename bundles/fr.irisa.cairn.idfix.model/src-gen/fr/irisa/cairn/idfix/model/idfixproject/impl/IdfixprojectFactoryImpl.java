/**
 */
package fr.irisa.cairn.idfix.model.idfixproject.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixProject;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixprojectFactory;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixprojectPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class IdfixprojectFactoryImpl extends EFactoryImpl implements IdfixprojectFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static IdfixprojectFactory init() {
		try {
			IdfixprojectFactory theIdfixprojectFactory = (IdfixprojectFactory)EPackage.Registry.INSTANCE.getEFactory(IdfixprojectPackage.eNS_URI);
			if (theIdfixprojectFactory != null) {
				return theIdfixprojectFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new IdfixprojectFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdfixprojectFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case IdfixprojectPackage.IDFIX_PROJECT: return createIdfixProject();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdfixProject createIdfixProject() {
		IdfixProjectImpl idfixProject = new IdfixProjectImpl();
		return idfixProject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdfixprojectPackage getIdfixprojectPackage() {
		return (IdfixprojectPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static IdfixprojectPackage getPackage() {
		return IdfixprojectPackage.eINSTANCE;
	}

} //IdfixprojectFactoryImpl
