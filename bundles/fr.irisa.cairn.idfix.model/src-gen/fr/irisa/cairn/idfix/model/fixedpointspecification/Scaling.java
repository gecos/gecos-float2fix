/**
 */
package fr.irisa.cairn.idfix.model.fixedpointspecification;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scaling</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Scaling#getEdge <em>Edge</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getScaling()
 * @model
 * @generated
 */
public interface Scaling extends EObject {
	/**
	 * Returns the value of the '<em><b>Edge</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge#getScaling <em>Scaling</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edge</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edge</em>' container reference.
	 * @see #setEdge(CDFGEdge)
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getScaling_Edge()
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge#getScaling
	 * @model opposite="scaling" transient="false"
	 * @generated
	 */
	CDFGEdge getEdge();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.Scaling#getEdge <em>Edge</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Edge</em>' container reference.
	 * @see #getEdge()
	 * @generated
	 */
	void setEdge(CDFGEdge value);

	/**
	 * <!-- begin-user-doc -->
	 * @return (from.FW - to.FW) > 0 if shr 
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	int computeScalingAmount();

	boolean isShr();
	boolean isShl();

} // Scaling
