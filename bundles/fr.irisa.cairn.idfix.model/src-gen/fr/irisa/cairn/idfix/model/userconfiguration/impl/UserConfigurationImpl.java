/**
 */
package fr.irisa.cairn.idfix.model.userconfiguration.impl;

import fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE;
import fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE;
import fr.irisa.cairn.idfix.model.userconfiguration.DATA_TYPE_REGENERATION;
import fr.irisa.cairn.idfix.model.userconfiguration.ENGINE;
import fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_LOADER;
import fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_OUTPUT_MODE;
import fr.irisa.cairn.idfix.model.userconfiguration.RUNTIME_MODE;
import fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration;
import fr.irisa.cairn.idfix.model.userconfiguration.UserconfigurationPackage;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>User Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.impl.UserConfigurationImpl#getTheadNumber <em>Thead Number</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.impl.UserConfigurationImpl#isOptimizationGraphDisplay <em>Optimization Graph Display</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.impl.UserConfigurationImpl#isGenerateSummary <em>Generate Summary</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.impl.UserConfigurationImpl#getEngine <em>Engine</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.impl.UserConfigurationImpl#getNoiseFunctionLoader <em>Noise Function Loader</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.impl.UserConfigurationImpl#getRuntimeMode <em>Runtime Mode</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.impl.UserConfigurationImpl#getMaximumFolder <em>Maximum Folder</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.impl.UserConfigurationImpl#getBackendSimulatorSampleNumber <em>Backend Simulator Sample Number</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.impl.UserConfigurationImpl#getNoiseFunctionOutputMode <em>Noise Function Output Mode</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.impl.UserConfigurationImpl#getOperatorQuantificationType <em>Operator Quantification Type</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.impl.UserConfigurationImpl#getOperatorOverflowType <em>Operator Overflow Type</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.impl.UserConfigurationImpl#getOptimizationSimulatorSampleNumber <em>Optimization Simulator Sample Number</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.impl.UserConfigurationImpl#getOptimizationSimulatorFrequency <em>Optimization Simulator Frequency</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.impl.UserConfigurationImpl#getOptimizationSimulatorSuperiorLimit <em>Optimization Simulator Superior Limit</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.impl.UserConfigurationImpl#getDataTypeRegneration <em>Data Type Regneration</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.impl.UserConfigurationImpl#getOperatorCharacteristic <em>Operator Characteristic</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.impl.UserConfigurationImpl#isTagFolder <em>Tag Folder</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.userconfiguration.impl.UserConfigurationImpl#isCorrelationMode <em>Correlation Mode</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UserConfigurationImpl extends MinimalEObjectImpl.Container implements UserConfiguration {
	/**
	 * The default value of the '{@link #getTheadNumber() <em>Thead Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTheadNumber()
	 * @generated
	 * @ordered
	 */
	protected static final int THEAD_NUMBER_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getTheadNumber() <em>Thead Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTheadNumber()
	 * @generated
	 * @ordered
	 */
	protected int theadNumber = THEAD_NUMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #isOptimizationGraphDisplay() <em>Optimization Graph Display</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOptimizationGraphDisplay()
	 * @generated
	 * @ordered
	 */
	protected static final boolean OPTIMIZATION_GRAPH_DISPLAY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isOptimizationGraphDisplay() <em>Optimization Graph Display</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOptimizationGraphDisplay()
	 * @generated
	 * @ordered
	 */
	protected boolean optimizationGraphDisplay = OPTIMIZATION_GRAPH_DISPLAY_EDEFAULT;

	/**
	 * The default value of the '{@link #isGenerateSummary() <em>Generate Summary</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isGenerateSummary()
	 * @generated
	 * @ordered
	 */
	protected static final boolean GENERATE_SUMMARY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isGenerateSummary() <em>Generate Summary</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isGenerateSummary()
	 * @generated
	 * @ordered
	 */
	protected boolean generateSummary = GENERATE_SUMMARY_EDEFAULT;

	/**
	 * The default value of the '{@link #getEngine() <em>Engine</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEngine()
	 * @generated
	 * @ordered
	 */
	protected static final ENGINE ENGINE_EDEFAULT = ENGINE.MATLAB;

	/**
	 * The cached value of the '{@link #getEngine() <em>Engine</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEngine()
	 * @generated
	 * @ordered
	 */
	protected ENGINE engine = ENGINE_EDEFAULT;

	/**
	 * The default value of the '{@link #getNoiseFunctionLoader() <em>Noise Function Loader</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoiseFunctionLoader()
	 * @generated
	 * @ordered
	 */
	protected static final NOISE_FUNCTION_LOADER NOISE_FUNCTION_LOADER_EDEFAULT = NOISE_FUNCTION_LOADER.JNA;

	/**
	 * The cached value of the '{@link #getNoiseFunctionLoader() <em>Noise Function Loader</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoiseFunctionLoader()
	 * @generated
	 * @ordered
	 */
	protected NOISE_FUNCTION_LOADER noiseFunctionLoader = NOISE_FUNCTION_LOADER_EDEFAULT;

	/**
	 * The default value of the '{@link #getRuntimeMode() <em>Runtime Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuntimeMode()
	 * @generated
	 * @ordered
	 */
	protected static final RUNTIME_MODE RUNTIME_MODE_EDEFAULT = RUNTIME_MODE.RELEASE;

	/**
	 * The cached value of the '{@link #getRuntimeMode() <em>Runtime Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuntimeMode()
	 * @generated
	 * @ordered
	 */
	protected RUNTIME_MODE runtimeMode = RUNTIME_MODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaximumFolder() <em>Maximum Folder</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaximumFolder()
	 * @generated
	 * @ordered
	 */
	protected static final int MAXIMUM_FOLDER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMaximumFolder() <em>Maximum Folder</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaximumFolder()
	 * @generated
	 * @ordered
	 */
	protected int maximumFolder = MAXIMUM_FOLDER_EDEFAULT;

	/**
	 * The default value of the '{@link #getBackendSimulatorSampleNumber() <em>Backend Simulator Sample Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBackendSimulatorSampleNumber()
	 * @generated
	 * @ordered
	 */
	protected static final int BACKEND_SIMULATOR_SAMPLE_NUMBER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getBackendSimulatorSampleNumber() <em>Backend Simulator Sample Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBackendSimulatorSampleNumber()
	 * @generated
	 * @ordered
	 */
	protected int backendSimulatorSampleNumber = BACKEND_SIMULATOR_SAMPLE_NUMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #getNoiseFunctionOutputMode() <em>Noise Function Output Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoiseFunctionOutputMode()
	 * @generated
	 * @ordered
	 */
	protected static final NOISE_FUNCTION_OUTPUT_MODE NOISE_FUNCTION_OUTPUT_MODE_EDEFAULT = NOISE_FUNCTION_OUTPUT_MODE.WORST;

	/**
	 * The cached value of the '{@link #getNoiseFunctionOutputMode() <em>Noise Function Output Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoiseFunctionOutputMode()
	 * @generated
	 * @ordered
	 */
	protected NOISE_FUNCTION_OUTPUT_MODE noiseFunctionOutputMode = NOISE_FUNCTION_OUTPUT_MODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getOperatorQuantificationType() <em>Operator Quantification Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperatorQuantificationType()
	 * @generated
	 * @ordered
	 */
	protected static final QUANTIFICATION_TYPE OPERATOR_QUANTIFICATION_TYPE_EDEFAULT = QUANTIFICATION_TYPE.TRUNCATION;

	/**
	 * The cached value of the '{@link #getOperatorQuantificationType() <em>Operator Quantification Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperatorQuantificationType()
	 * @generated
	 * @ordered
	 */
	protected QUANTIFICATION_TYPE operatorQuantificationType = OPERATOR_QUANTIFICATION_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getOperatorOverflowType() <em>Operator Overflow Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperatorOverflowType()
	 * @generated
	 * @ordered
	 */
	protected static final OVERFLOW_TYPE OPERATOR_OVERFLOW_TYPE_EDEFAULT = OVERFLOW_TYPE.SATURATION;

	/**
	 * The cached value of the '{@link #getOperatorOverflowType() <em>Operator Overflow Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperatorOverflowType()
	 * @generated
	 * @ordered
	 */
	protected OVERFLOW_TYPE operatorOverflowType = OPERATOR_OVERFLOW_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getOptimizationSimulatorSampleNumber() <em>Optimization Simulator Sample Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOptimizationSimulatorSampleNumber()
	 * @generated
	 * @ordered
	 */
	protected static final int OPTIMIZATION_SIMULATOR_SAMPLE_NUMBER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getOptimizationSimulatorSampleNumber() <em>Optimization Simulator Sample Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOptimizationSimulatorSampleNumber()
	 * @generated
	 * @ordered
	 */
	protected int optimizationSimulatorSampleNumber = OPTIMIZATION_SIMULATOR_SAMPLE_NUMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #getOptimizationSimulatorFrequency() <em>Optimization Simulator Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOptimizationSimulatorFrequency()
	 * @generated
	 * @ordered
	 */
	protected static final int OPTIMIZATION_SIMULATOR_FREQUENCY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getOptimizationSimulatorFrequency() <em>Optimization Simulator Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOptimizationSimulatorFrequency()
	 * @generated
	 * @ordered
	 */
	protected int optimizationSimulatorFrequency = OPTIMIZATION_SIMULATOR_FREQUENCY_EDEFAULT;

	/**
	 * The default value of the '{@link #getOptimizationSimulatorSuperiorLimit() <em>Optimization Simulator Superior Limit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOptimizationSimulatorSuperiorLimit()
	 * @generated
	 * @ordered
	 */
	protected static final float OPTIMIZATION_SIMULATOR_SUPERIOR_LIMIT_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getOptimizationSimulatorSuperiorLimit() <em>Optimization Simulator Superior Limit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOptimizationSimulatorSuperiorLimit()
	 * @generated
	 * @ordered
	 */
	protected float optimizationSimulatorSuperiorLimit = OPTIMIZATION_SIMULATOR_SUPERIOR_LIMIT_EDEFAULT;

	/**
	 * The default value of the '{@link #getDataTypeRegneration() <em>Data Type Regneration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataTypeRegneration()
	 * @generated
	 * @ordered
	 */
	protected static final DATA_TYPE_REGENERATION DATA_TYPE_REGNERATION_EDEFAULT = DATA_TYPE_REGENERATION.SC_FIXED;

	/**
	 * The cached value of the '{@link #getDataTypeRegneration() <em>Data Type Regneration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataTypeRegneration()
	 * @generated
	 * @ordered
	 */
	protected DATA_TYPE_REGENERATION dataTypeRegneration = DATA_TYPE_REGNERATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getOperatorCharacteristic() <em>Operator Characteristic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperatorCharacteristic()
	 * @generated
	 * @ordered
	 */
	protected static final String OPERATOR_CHARACTERISTIC_EDEFAULT = "energy";

	/**
	 * The cached value of the '{@link #getOperatorCharacteristic() <em>Operator Characteristic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperatorCharacteristic()
	 * @generated
	 * @ordered
	 */
	protected String operatorCharacteristic = OPERATOR_CHARACTERISTIC_EDEFAULT;

	/**
	 * The default value of the '{@link #isTagFolder() <em>Tag Folder</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTagFolder()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TAG_FOLDER_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isTagFolder() <em>Tag Folder</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTagFolder()
	 * @generated
	 * @ordered
	 */
	protected boolean tagFolder = TAG_FOLDER_EDEFAULT;

	/**
	 * The default value of the '{@link #isCorrelationMode() <em>Correlation Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCorrelationMode()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CORRELATION_MODE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isCorrelationMode() <em>Correlation Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCorrelationMode()
	 * @generated
	 * @ordered
	 */
	protected boolean correlationMode = CORRELATION_MODE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UserConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UserconfigurationPackage.Literals.USER_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getTheadNumber() {
		return theadNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTheadNumber(int newTheadNumber) {
		int oldTheadNumber = theadNumber;
		theadNumber = newTheadNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UserconfigurationPackage.USER_CONFIGURATION__THEAD_NUMBER, oldTheadNumber, theadNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isOptimizationGraphDisplay() {
		return optimizationGraphDisplay;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOptimizationGraphDisplay(boolean newOptimizationGraphDisplay) {
		boolean oldOptimizationGraphDisplay = optimizationGraphDisplay;
		optimizationGraphDisplay = newOptimizationGraphDisplay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UserconfigurationPackage.USER_CONFIGURATION__OPTIMIZATION_GRAPH_DISPLAY, oldOptimizationGraphDisplay, optimizationGraphDisplay));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isGenerateSummary() {
		return generateSummary;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenerateSummary(boolean newGenerateSummary) {
		boolean oldGenerateSummary = generateSummary;
		generateSummary = newGenerateSummary;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UserconfigurationPackage.USER_CONFIGURATION__GENERATE_SUMMARY, oldGenerateSummary, generateSummary));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ENGINE getEngine() {
		return engine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEngine(ENGINE newEngine) {
		ENGINE oldEngine = engine;
		engine = newEngine == null ? ENGINE_EDEFAULT : newEngine;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UserconfigurationPackage.USER_CONFIGURATION__ENGINE, oldEngine, engine));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NOISE_FUNCTION_LOADER getNoiseFunctionLoader() {
		return noiseFunctionLoader;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNoiseFunctionLoader(NOISE_FUNCTION_LOADER newNoiseFunctionLoader) {
		NOISE_FUNCTION_LOADER oldNoiseFunctionLoader = noiseFunctionLoader;
		noiseFunctionLoader = newNoiseFunctionLoader == null ? NOISE_FUNCTION_LOADER_EDEFAULT : newNoiseFunctionLoader;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UserconfigurationPackage.USER_CONFIGURATION__NOISE_FUNCTION_LOADER, oldNoiseFunctionLoader, noiseFunctionLoader));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RUNTIME_MODE getRuntimeMode() {
		return runtimeMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuntimeMode(RUNTIME_MODE newRuntimeMode) {
		RUNTIME_MODE oldRuntimeMode = runtimeMode;
		runtimeMode = newRuntimeMode == null ? RUNTIME_MODE_EDEFAULT : newRuntimeMode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UserconfigurationPackage.USER_CONFIGURATION__RUNTIME_MODE, oldRuntimeMode, runtimeMode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMaximumFolder() {
		return maximumFolder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaximumFolder(int newMaximumFolder) {
		int oldMaximumFolder = maximumFolder;
		maximumFolder = newMaximumFolder;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UserconfigurationPackage.USER_CONFIGURATION__MAXIMUM_FOLDER, oldMaximumFolder, maximumFolder));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBackendSimulatorSampleNumber() {
		return backendSimulatorSampleNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBackendSimulatorSampleNumber(int newBackendSimulatorSampleNumber) {
		int oldBackendSimulatorSampleNumber = backendSimulatorSampleNumber;
		backendSimulatorSampleNumber = newBackendSimulatorSampleNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UserconfigurationPackage.USER_CONFIGURATION__BACKEND_SIMULATOR_SAMPLE_NUMBER, oldBackendSimulatorSampleNumber, backendSimulatorSampleNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NOISE_FUNCTION_OUTPUT_MODE getNoiseFunctionOutputMode() {
		return noiseFunctionOutputMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNoiseFunctionOutputMode(NOISE_FUNCTION_OUTPUT_MODE newNoiseFunctionOutputMode) {
		NOISE_FUNCTION_OUTPUT_MODE oldNoiseFunctionOutputMode = noiseFunctionOutputMode;
		noiseFunctionOutputMode = newNoiseFunctionOutputMode == null ? NOISE_FUNCTION_OUTPUT_MODE_EDEFAULT : newNoiseFunctionOutputMode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UserconfigurationPackage.USER_CONFIGURATION__NOISE_FUNCTION_OUTPUT_MODE, oldNoiseFunctionOutputMode, noiseFunctionOutputMode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QUANTIFICATION_TYPE getOperatorQuantificationType() {
		return operatorQuantificationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperatorQuantificationType(QUANTIFICATION_TYPE newOperatorQuantificationType) {
		QUANTIFICATION_TYPE oldOperatorQuantificationType = operatorQuantificationType;
		operatorQuantificationType = newOperatorQuantificationType == null ? OPERATOR_QUANTIFICATION_TYPE_EDEFAULT : newOperatorQuantificationType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UserconfigurationPackage.USER_CONFIGURATION__OPERATOR_QUANTIFICATION_TYPE, oldOperatorQuantificationType, operatorQuantificationType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OVERFLOW_TYPE getOperatorOverflowType() {
		return operatorOverflowType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperatorOverflowType(OVERFLOW_TYPE newOperatorOverflowType) {
		OVERFLOW_TYPE oldOperatorOverflowType = operatorOverflowType;
		operatorOverflowType = newOperatorOverflowType == null ? OPERATOR_OVERFLOW_TYPE_EDEFAULT : newOperatorOverflowType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UserconfigurationPackage.USER_CONFIGURATION__OPERATOR_OVERFLOW_TYPE, oldOperatorOverflowType, operatorOverflowType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getOptimizationSimulatorSampleNumber() {
		return optimizationSimulatorSampleNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOptimizationSimulatorSampleNumber(int newOptimizationSimulatorSampleNumber) {
		int oldOptimizationSimulatorSampleNumber = optimizationSimulatorSampleNumber;
		optimizationSimulatorSampleNumber = newOptimizationSimulatorSampleNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UserconfigurationPackage.USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_SAMPLE_NUMBER, oldOptimizationSimulatorSampleNumber, optimizationSimulatorSampleNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getOptimizationSimulatorFrequency() {
		return optimizationSimulatorFrequency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOptimizationSimulatorFrequency(int newOptimizationSimulatorFrequency) {
		int oldOptimizationSimulatorFrequency = optimizationSimulatorFrequency;
		optimizationSimulatorFrequency = newOptimizationSimulatorFrequency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UserconfigurationPackage.USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_FREQUENCY, oldOptimizationSimulatorFrequency, optimizationSimulatorFrequency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getOptimizationSimulatorSuperiorLimit() {
		return optimizationSimulatorSuperiorLimit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOptimizationSimulatorSuperiorLimit(float newOptimizationSimulatorSuperiorLimit) {
		float oldOptimizationSimulatorSuperiorLimit = optimizationSimulatorSuperiorLimit;
		optimizationSimulatorSuperiorLimit = newOptimizationSimulatorSuperiorLimit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UserconfigurationPackage.USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_SUPERIOR_LIMIT, oldOptimizationSimulatorSuperiorLimit, optimizationSimulatorSuperiorLimit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DATA_TYPE_REGENERATION getDataTypeRegneration() {
		return dataTypeRegneration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataTypeRegneration(DATA_TYPE_REGENERATION newDataTypeRegneration) {
		DATA_TYPE_REGENERATION oldDataTypeRegneration = dataTypeRegneration;
		dataTypeRegneration = newDataTypeRegneration == null ? DATA_TYPE_REGNERATION_EDEFAULT : newDataTypeRegneration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UserconfigurationPackage.USER_CONFIGURATION__DATA_TYPE_REGNERATION, oldDataTypeRegneration, dataTypeRegneration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOperatorCharacteristic() {
		return operatorCharacteristic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperatorCharacteristic(String newOperatorCharacteristic) {
		String oldOperatorCharacteristic = operatorCharacteristic;
		operatorCharacteristic = newOperatorCharacteristic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UserconfigurationPackage.USER_CONFIGURATION__OPERATOR_CHARACTERISTIC, oldOperatorCharacteristic, operatorCharacteristic));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTagFolder() {
		return tagFolder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTagFolder(boolean newTagFolder) {
		boolean oldTagFolder = tagFolder;
		tagFolder = newTagFolder;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UserconfigurationPackage.USER_CONFIGURATION__TAG_FOLDER, oldTagFolder, tagFolder));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCorrelationMode() {
		return correlationMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCorrelationMode(boolean newCorrelationMode) {
		boolean oldCorrelationMode = correlationMode;
		correlationMode = newCorrelationMode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UserconfigurationPackage.USER_CONFIGURATION__CORRELATION_MODE, oldCorrelationMode, correlationMode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isReleaseMode() {
		return getRuntimeMode() == RUNTIME_MODE.RELEASE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isDebugMode() {
		return getRuntimeMode() == RUNTIME_MODE.DEBUG || getRuntimeMode() == RUNTIME_MODE.TRACE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isTraceMode() {
		return getRuntimeMode() == RUNTIME_MODE.TRACE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean backendSimulationIsActivated() {
		return this.getBackendSimulatorSampleNumber() > 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean optimizationSimulationIsActivated() {
		return this.getOptimizationSimulatorSampleNumber() > 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UserconfigurationPackage.USER_CONFIGURATION__THEAD_NUMBER:
				return getTheadNumber();
			case UserconfigurationPackage.USER_CONFIGURATION__OPTIMIZATION_GRAPH_DISPLAY:
				return isOptimizationGraphDisplay();
			case UserconfigurationPackage.USER_CONFIGURATION__GENERATE_SUMMARY:
				return isGenerateSummary();
			case UserconfigurationPackage.USER_CONFIGURATION__ENGINE:
				return getEngine();
			case UserconfigurationPackage.USER_CONFIGURATION__NOISE_FUNCTION_LOADER:
				return getNoiseFunctionLoader();
			case UserconfigurationPackage.USER_CONFIGURATION__RUNTIME_MODE:
				return getRuntimeMode();
			case UserconfigurationPackage.USER_CONFIGURATION__MAXIMUM_FOLDER:
				return getMaximumFolder();
			case UserconfigurationPackage.USER_CONFIGURATION__BACKEND_SIMULATOR_SAMPLE_NUMBER:
				return getBackendSimulatorSampleNumber();
			case UserconfigurationPackage.USER_CONFIGURATION__NOISE_FUNCTION_OUTPUT_MODE:
				return getNoiseFunctionOutputMode();
			case UserconfigurationPackage.USER_CONFIGURATION__OPERATOR_QUANTIFICATION_TYPE:
				return getOperatorQuantificationType();
			case UserconfigurationPackage.USER_CONFIGURATION__OPERATOR_OVERFLOW_TYPE:
				return getOperatorOverflowType();
			case UserconfigurationPackage.USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_SAMPLE_NUMBER:
				return getOptimizationSimulatorSampleNumber();
			case UserconfigurationPackage.USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_FREQUENCY:
				return getOptimizationSimulatorFrequency();
			case UserconfigurationPackage.USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_SUPERIOR_LIMIT:
				return getOptimizationSimulatorSuperiorLimit();
			case UserconfigurationPackage.USER_CONFIGURATION__DATA_TYPE_REGNERATION:
				return getDataTypeRegneration();
			case UserconfigurationPackage.USER_CONFIGURATION__OPERATOR_CHARACTERISTIC:
				return getOperatorCharacteristic();
			case UserconfigurationPackage.USER_CONFIGURATION__TAG_FOLDER:
				return isTagFolder();
			case UserconfigurationPackage.USER_CONFIGURATION__CORRELATION_MODE:
				return isCorrelationMode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UserconfigurationPackage.USER_CONFIGURATION__THEAD_NUMBER:
				setTheadNumber((Integer)newValue);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__OPTIMIZATION_GRAPH_DISPLAY:
				setOptimizationGraphDisplay((Boolean)newValue);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__GENERATE_SUMMARY:
				setGenerateSummary((Boolean)newValue);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__ENGINE:
				setEngine((ENGINE)newValue);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__NOISE_FUNCTION_LOADER:
				setNoiseFunctionLoader((NOISE_FUNCTION_LOADER)newValue);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__RUNTIME_MODE:
				setRuntimeMode((RUNTIME_MODE)newValue);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__MAXIMUM_FOLDER:
				setMaximumFolder((Integer)newValue);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__BACKEND_SIMULATOR_SAMPLE_NUMBER:
				setBackendSimulatorSampleNumber((Integer)newValue);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__NOISE_FUNCTION_OUTPUT_MODE:
				setNoiseFunctionOutputMode((NOISE_FUNCTION_OUTPUT_MODE)newValue);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__OPERATOR_QUANTIFICATION_TYPE:
				setOperatorQuantificationType((QUANTIFICATION_TYPE)newValue);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__OPERATOR_OVERFLOW_TYPE:
				setOperatorOverflowType((OVERFLOW_TYPE)newValue);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_SAMPLE_NUMBER:
				setOptimizationSimulatorSampleNumber((Integer)newValue);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_FREQUENCY:
				setOptimizationSimulatorFrequency((Integer)newValue);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_SUPERIOR_LIMIT:
				setOptimizationSimulatorSuperiorLimit((Float)newValue);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__DATA_TYPE_REGNERATION:
				setDataTypeRegneration((DATA_TYPE_REGENERATION)newValue);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__OPERATOR_CHARACTERISTIC:
				setOperatorCharacteristic((String)newValue);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__TAG_FOLDER:
				setTagFolder((Boolean)newValue);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__CORRELATION_MODE:
				setCorrelationMode((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UserconfigurationPackage.USER_CONFIGURATION__THEAD_NUMBER:
				setTheadNumber(THEAD_NUMBER_EDEFAULT);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__OPTIMIZATION_GRAPH_DISPLAY:
				setOptimizationGraphDisplay(OPTIMIZATION_GRAPH_DISPLAY_EDEFAULT);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__GENERATE_SUMMARY:
				setGenerateSummary(GENERATE_SUMMARY_EDEFAULT);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__ENGINE:
				setEngine(ENGINE_EDEFAULT);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__NOISE_FUNCTION_LOADER:
				setNoiseFunctionLoader(NOISE_FUNCTION_LOADER_EDEFAULT);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__RUNTIME_MODE:
				setRuntimeMode(RUNTIME_MODE_EDEFAULT);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__MAXIMUM_FOLDER:
				setMaximumFolder(MAXIMUM_FOLDER_EDEFAULT);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__BACKEND_SIMULATOR_SAMPLE_NUMBER:
				setBackendSimulatorSampleNumber(BACKEND_SIMULATOR_SAMPLE_NUMBER_EDEFAULT);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__NOISE_FUNCTION_OUTPUT_MODE:
				setNoiseFunctionOutputMode(NOISE_FUNCTION_OUTPUT_MODE_EDEFAULT);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__OPERATOR_QUANTIFICATION_TYPE:
				setOperatorQuantificationType(OPERATOR_QUANTIFICATION_TYPE_EDEFAULT);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__OPERATOR_OVERFLOW_TYPE:
				setOperatorOverflowType(OPERATOR_OVERFLOW_TYPE_EDEFAULT);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_SAMPLE_NUMBER:
				setOptimizationSimulatorSampleNumber(OPTIMIZATION_SIMULATOR_SAMPLE_NUMBER_EDEFAULT);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_FREQUENCY:
				setOptimizationSimulatorFrequency(OPTIMIZATION_SIMULATOR_FREQUENCY_EDEFAULT);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_SUPERIOR_LIMIT:
				setOptimizationSimulatorSuperiorLimit(OPTIMIZATION_SIMULATOR_SUPERIOR_LIMIT_EDEFAULT);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__DATA_TYPE_REGNERATION:
				setDataTypeRegneration(DATA_TYPE_REGNERATION_EDEFAULT);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__OPERATOR_CHARACTERISTIC:
				setOperatorCharacteristic(OPERATOR_CHARACTERISTIC_EDEFAULT);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__TAG_FOLDER:
				setTagFolder(TAG_FOLDER_EDEFAULT);
				return;
			case UserconfigurationPackage.USER_CONFIGURATION__CORRELATION_MODE:
				setCorrelationMode(CORRELATION_MODE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UserconfigurationPackage.USER_CONFIGURATION__THEAD_NUMBER:
				return theadNumber != THEAD_NUMBER_EDEFAULT;
			case UserconfigurationPackage.USER_CONFIGURATION__OPTIMIZATION_GRAPH_DISPLAY:
				return optimizationGraphDisplay != OPTIMIZATION_GRAPH_DISPLAY_EDEFAULT;
			case UserconfigurationPackage.USER_CONFIGURATION__GENERATE_SUMMARY:
				return generateSummary != GENERATE_SUMMARY_EDEFAULT;
			case UserconfigurationPackage.USER_CONFIGURATION__ENGINE:
				return engine != ENGINE_EDEFAULT;
			case UserconfigurationPackage.USER_CONFIGURATION__NOISE_FUNCTION_LOADER:
				return noiseFunctionLoader != NOISE_FUNCTION_LOADER_EDEFAULT;
			case UserconfigurationPackage.USER_CONFIGURATION__RUNTIME_MODE:
				return runtimeMode != RUNTIME_MODE_EDEFAULT;
			case UserconfigurationPackage.USER_CONFIGURATION__MAXIMUM_FOLDER:
				return maximumFolder != MAXIMUM_FOLDER_EDEFAULT;
			case UserconfigurationPackage.USER_CONFIGURATION__BACKEND_SIMULATOR_SAMPLE_NUMBER:
				return backendSimulatorSampleNumber != BACKEND_SIMULATOR_SAMPLE_NUMBER_EDEFAULT;
			case UserconfigurationPackage.USER_CONFIGURATION__NOISE_FUNCTION_OUTPUT_MODE:
				return noiseFunctionOutputMode != NOISE_FUNCTION_OUTPUT_MODE_EDEFAULT;
			case UserconfigurationPackage.USER_CONFIGURATION__OPERATOR_QUANTIFICATION_TYPE:
				return operatorQuantificationType != OPERATOR_QUANTIFICATION_TYPE_EDEFAULT;
			case UserconfigurationPackage.USER_CONFIGURATION__OPERATOR_OVERFLOW_TYPE:
				return operatorOverflowType != OPERATOR_OVERFLOW_TYPE_EDEFAULT;
			case UserconfigurationPackage.USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_SAMPLE_NUMBER:
				return optimizationSimulatorSampleNumber != OPTIMIZATION_SIMULATOR_SAMPLE_NUMBER_EDEFAULT;
			case UserconfigurationPackage.USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_FREQUENCY:
				return optimizationSimulatorFrequency != OPTIMIZATION_SIMULATOR_FREQUENCY_EDEFAULT;
			case UserconfigurationPackage.USER_CONFIGURATION__OPTIMIZATION_SIMULATOR_SUPERIOR_LIMIT:
				return optimizationSimulatorSuperiorLimit != OPTIMIZATION_SIMULATOR_SUPERIOR_LIMIT_EDEFAULT;
			case UserconfigurationPackage.USER_CONFIGURATION__DATA_TYPE_REGNERATION:
				return dataTypeRegneration != DATA_TYPE_REGNERATION_EDEFAULT;
			case UserconfigurationPackage.USER_CONFIGURATION__OPERATOR_CHARACTERISTIC:
				return OPERATOR_CHARACTERISTIC_EDEFAULT == null ? operatorCharacteristic != null : !OPERATOR_CHARACTERISTIC_EDEFAULT.equals(operatorCharacteristic);
			case UserconfigurationPackage.USER_CONFIGURATION__TAG_FOLDER:
				return tagFolder != TAG_FOLDER_EDEFAULT;
			case UserconfigurationPackage.USER_CONFIGURATION__CORRELATION_MODE:
				return correlationMode != CORRELATION_MODE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case UserconfigurationPackage.USER_CONFIGURATION___IS_RELEASE_MODE:
				return isReleaseMode();
			case UserconfigurationPackage.USER_CONFIGURATION___IS_DEBUG_MODE:
				return isDebugMode();
			case UserconfigurationPackage.USER_CONFIGURATION___IS_TRACE_MODE:
				return isTraceMode();
			case UserconfigurationPackage.USER_CONFIGURATION___BACKEND_SIMULATION_IS_ACTIVATED:
				return backendSimulationIsActivated();
			case UserconfigurationPackage.USER_CONFIGURATION___OPTIMIZATION_SIMULATION_IS_ACTIVATED:
				return optimizationSimulationIsActivated();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (theadNumber: ");
		result.append(theadNumber);
		result.append(", optimizationGraphDisplay: ");
		result.append(optimizationGraphDisplay);
		result.append(", generateSummary: ");
		result.append(generateSummary);
		result.append(", engine: ");
		result.append(engine);
		result.append(", noiseFunctionLoader: ");
		result.append(noiseFunctionLoader);
		result.append(", runtimeMode: ");
		result.append(runtimeMode);
		result.append(", maximumFolder: ");
		result.append(maximumFolder);
		result.append(", backendSimulatorSampleNumber: ");
		result.append(backendSimulatorSampleNumber);
		result.append(", noiseFunctionOutputMode: ");
		result.append(noiseFunctionOutputMode);
		result.append(", operatorQuantificationType: ");
		result.append(operatorQuantificationType);
		result.append(", operatorOverflowType: ");
		result.append(operatorOverflowType);
		result.append(", optimizationSimulatorSampleNumber: ");
		result.append(optimizationSimulatorSampleNumber);
		result.append(", optimizationSimulatorFrequency: ");
		result.append(optimizationSimulatorFrequency);
		result.append(", optimizationSimulatorSuperiorLimit: ");
		result.append(optimizationSimulatorSuperiorLimit);
		result.append(", dataTypeRegneration: ");
		result.append(dataTypeRegneration);
		result.append(", operatorCharacteristic: ");
		result.append(operatorCharacteristic);
		result.append(", tagFolder: ");
		result.append(tagFolder);
		result.append(", correlationMode: ");
		result.append(correlationMode);
		result.append(')');
		return result.toString();
	}

} //UserConfigurationImpl
