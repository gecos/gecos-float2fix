/**
 */
package fr.irisa.cairn.idfix.model.fixedpointspecification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import float2fix.Node;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CDFG Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode#getSuccessors <em>Successors</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode#getPredecessors <em>Predecessors</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode#getSfgNodeInstances <em>Sfg Node Instances</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getCDFGNode()
 * @model abstract="true"
 * @generated
 */
public interface CDFGNode extends EObject {
	/**
	 * Returns the value of the '<em><b>Successors</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge}.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge#getPredecessor <em>Predecessor</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Successors</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Successors</em>' reference list.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getCDFGNode_Successors()
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge#getPredecessor
	 * @model opposite="predecessor"
	 * @generated
	 */
	EList<CDFGEdge> getSuccessors();

	/**
	 * Returns the value of the '<em><b>Predecessors</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge}.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge#getSuccessor <em>Successor</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Predecessors</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Predecessors</em>' reference list.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getCDFGNode_Predecessors()
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge#getSuccessor
	 * @model opposite="successor"
	 * @generated
	 */
	EList<CDFGEdge> getPredecessors();

	/**
	 * Returns the value of the '<em><b>Sfg Node Instances</b></em>' reference list.
	 * The list contents are of type {@link float2fix.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sfg Node Instances</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sfg Node Instances</em>' reference list.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getCDFGNode_SfgNodeInstances()
	 * @model
	 * @generated
	 */
	EList<Node> getSfgNodeInstances();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	FixedPointInformation getOutputInfo();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getName();

} // CDFGNode
