/**
 */
package fr.irisa.cairn.idfix.model.informationandtiming;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.idfix.model.informationandtiming.InformationandtimingPackage
 * @generated
 */
public interface InformationandtimingFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	InformationandtimingFactory eINSTANCE = fr.irisa.cairn.idfix.model.informationandtiming.impl.InformationandtimingFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Informations</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Informations</em>'.
	 * @generated
	 */
	Informations createInformations();

	/**
	 * Returns a new object of class '<em>Timing</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Timing</em>'.
	 * @generated
	 */
	Timing createTiming();

	/**
	 * Returns a new object of class '<em>Time Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Time Element</em>'.
	 * @generated
	 */
	TimeElement createTimeElement();

	/**
	 * Returns a new object of class '<em>Section</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Section</em>'.
	 * @generated
	 */
	Section createSection();

	/**
	 * Returns a new object of class '<em>Analytic Informations</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Analytic Informations</em>'.
	 * @generated
	 */
	AnalyticInformations createAnalyticInformations();

	/**
	 * Returns a new object of class '<em>Simulation Informations</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simulation Informations</em>'.
	 * @generated
	 */
	SimulationInformations createSimulationInformations();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	InformationandtimingPackage getInformationandtimingPackage();

} //InformationandtimingFactory
