/**
 */
package fr.irisa.cairn.idfix.model.fixedpointspecification.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import float2fix.Node;
import fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge;
import fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage;
import java.lang.reflect.InvocationTargetException;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CDFG Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.CDFGNodeImpl#getSuccessors <em>Successors</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.CDFGNodeImpl#getPredecessors <em>Predecessors</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.impl.CDFGNodeImpl#getSfgNodeInstances <em>Sfg Node Instances</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class CDFGNodeImpl extends MinimalEObjectImpl.Container implements CDFGNode {
	/**
	 * The cached value of the '{@link #getSuccessors() <em>Successors</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuccessors()
	 * @generated
	 * @ordered
	 */
	protected EList<CDFGEdge> successors;

	/**
	 * The cached value of the '{@link #getPredecessors() <em>Predecessors</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPredecessors()
	 * @generated
	 * @ordered
	 */
	protected EList<CDFGEdge> predecessors;

	/**
	 * The cached value of the '{@link #getSfgNodeInstances() <em>Sfg Node Instances</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSfgNodeInstances()
	 * @generated
	 * @ordered
	 */
	protected EList<Node> sfgNodeInstances;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CDFGNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FixedpointspecificationPackage.Literals.CDFG_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CDFGEdge> getSuccessors() {
		if (successors == null) {
			successors = new EObjectWithInverseResolvingEList<CDFGEdge>(CDFGEdge.class, this, FixedpointspecificationPackage.CDFG_NODE__SUCCESSORS, FixedpointspecificationPackage.CDFG_EDGE__PREDECESSOR);
		}
		return successors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CDFGEdge> getPredecessors() {
		if (predecessors == null) {
			predecessors = new EObjectWithInverseResolvingEList<CDFGEdge>(CDFGEdge.class, this, FixedpointspecificationPackage.CDFG_NODE__PREDECESSORS, FixedpointspecificationPackage.CDFG_EDGE__SUCCESSOR);
		}
		return predecessors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Node> getSfgNodeInstances() {
		if (sfgNodeInstances == null) {
			sfgNodeInstances = new EObjectResolvingEList<Node>(Node.class, this, FixedpointspecificationPackage.CDFG_NODE__SFG_NODE_INSTANCES);
		}
		return sfgNodeInstances;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixedPointInformation getOutputInfo() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FixedpointspecificationPackage.CDFG_NODE__SUCCESSORS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSuccessors()).basicAdd(otherEnd, msgs);
			case FixedpointspecificationPackage.CDFG_NODE__PREDECESSORS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPredecessors()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FixedpointspecificationPackage.CDFG_NODE__SUCCESSORS:
				return ((InternalEList<?>)getSuccessors()).basicRemove(otherEnd, msgs);
			case FixedpointspecificationPackage.CDFG_NODE__PREDECESSORS:
				return ((InternalEList<?>)getPredecessors()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FixedpointspecificationPackage.CDFG_NODE__SUCCESSORS:
				return getSuccessors();
			case FixedpointspecificationPackage.CDFG_NODE__PREDECESSORS:
				return getPredecessors();
			case FixedpointspecificationPackage.CDFG_NODE__SFG_NODE_INSTANCES:
				return getSfgNodeInstances();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FixedpointspecificationPackage.CDFG_NODE__SUCCESSORS:
				getSuccessors().clear();
				getSuccessors().addAll((Collection<? extends CDFGEdge>)newValue);
				return;
			case FixedpointspecificationPackage.CDFG_NODE__PREDECESSORS:
				getPredecessors().clear();
				getPredecessors().addAll((Collection<? extends CDFGEdge>)newValue);
				return;
			case FixedpointspecificationPackage.CDFG_NODE__SFG_NODE_INSTANCES:
				getSfgNodeInstances().clear();
				getSfgNodeInstances().addAll((Collection<? extends Node>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FixedpointspecificationPackage.CDFG_NODE__SUCCESSORS:
				getSuccessors().clear();
				return;
			case FixedpointspecificationPackage.CDFG_NODE__PREDECESSORS:
				getPredecessors().clear();
				return;
			case FixedpointspecificationPackage.CDFG_NODE__SFG_NODE_INSTANCES:
				getSfgNodeInstances().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FixedpointspecificationPackage.CDFG_NODE__SUCCESSORS:
				return successors != null && !successors.isEmpty();
			case FixedpointspecificationPackage.CDFG_NODE__PREDECESSORS:
				return predecessors != null && !predecessors.isEmpty();
			case FixedpointspecificationPackage.CDFG_NODE__SFG_NODE_INSTANCES:
				return sfgNodeInstances != null && !sfgNodeInstances.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case FixedpointspecificationPackage.CDFG_NODE___GET_OUTPUT_INFO:
				return getOutputInfo();
			case FixedpointspecificationPackage.CDFG_NODE___GET_NAME:
				return getName();
		}
		return super.eInvoke(operationID, arguments);
	}

} //CDFGNodeImpl
