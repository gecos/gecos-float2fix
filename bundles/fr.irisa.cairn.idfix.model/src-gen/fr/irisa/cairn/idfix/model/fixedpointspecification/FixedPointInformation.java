/**
 */
package fr.irisa.cairn.idfix.model.fixedpointspecification;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fixed Point Information</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#isFixedBitWidth <em>Fixed Bit Width</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#isSigned <em>Signed</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getBitWidth <em>Bit Width</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getIntegerWidth <em>Integer Width</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getOverflow <em>Overflow</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getQuantification <em>Quantification</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getDynamic <em>Dynamic</em>}</li>
 *   <li>{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getDynamicIntegerWidth <em>Dynamic Integer Width</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getFixedPointInformation()
 * @model
 * @generated
 */
public interface FixedPointInformation extends EObject {
	/**
	 * Returns the value of the '<em><b>Fixed Bit Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fixed Bit Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fixed Bit Width</em>' attribute.
	 * @see #setFixedBitWidth(boolean)
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getFixedPointInformation_FixedBitWidth()
	 * @model
	 * @generated
	 */
	boolean isFixedBitWidth();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#isFixedBitWidth <em>Fixed Bit Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fixed Bit Width</em>' attribute.
	 * @see #isFixedBitWidth()
	 * @generated
	 */
	void setFixedBitWidth(boolean value);

	/**
	 * Returns the value of the '<em><b>Bit Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bit Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bit Width</em>' attribute.
	 * @see #setBitWidth(int)
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getFixedPointInformation_BitWidth()
	 * @model
	 * @generated
	 */
	int getBitWidth();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getBitWidth <em>Bit Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bit Width</em>' attribute.
	 * @see #getBitWidth()
	 * @generated
	 */
	void setBitWidth(int value);

	/**
	 * Returns the value of the '<em><b>Integer Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Width</em>' attribute.
	 * @see #setIntegerWidth(int)
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getFixedPointInformation_IntegerWidth()
	 * @model
	 * @generated
	 */
	int getIntegerWidth();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getIntegerWidth <em>Integer Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integer Width</em>' attribute.
	 * @see #getIntegerWidth()
	 * @generated
	 */
	void setIntegerWidth(int value);

	/**
	 * Returns the value of the '<em><b>Signed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signed</em>' attribute.
	 * @see #setSigned(boolean)
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getFixedPointInformation_Signed()
	 * @model
	 * @generated
	 */
	boolean isSigned();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#isSigned <em>Signed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signed</em>' attribute.
	 * @see #isSigned()
	 * @generated
	 */
	void setSigned(boolean value);

	/**
	 * Returns the value of the '<em><b>Overflow</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overflow</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overflow</em>' attribute.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE
	 * @see #setOverflow(OVERFLOW_TYPE)
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getFixedPointInformation_Overflow()
	 * @model
	 * @generated
	 */
	OVERFLOW_TYPE getOverflow();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getOverflow <em>Overflow</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Overflow</em>' attribute.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE
	 * @see #getOverflow()
	 * @generated
	 */
	void setOverflow(OVERFLOW_TYPE value);

	/**
	 * Returns the value of the '<em><b>Quantification</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantification</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantification</em>' attribute.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE
	 * @see #setQuantification(QUANTIFICATION_TYPE)
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getFixedPointInformation_Quantification()
	 * @model
	 * @generated
	 */
	QUANTIFICATION_TYPE getQuantification();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getQuantification <em>Quantification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantification</em>' attribute.
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE
	 * @see #getQuantification()
	 * @generated
	 */
	void setQuantification(QUANTIFICATION_TYPE value);

	/**
	 * Returns the value of the '<em><b>Dynamic</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dynamic</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dynamic</em>' reference.
	 * @see #setDynamic(Dynamic)
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getFixedPointInformation_Dynamic()
	 * @model
	 * @generated
	 */
	Dynamic getDynamic();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getDynamic <em>Dynamic</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dynamic</em>' reference.
	 * @see #getDynamic()
	 * @generated
	 */
	void setDynamic(Dynamic value);

	/**
	 * Returns the value of the '<em><b>Dynamic Integer Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dynamic Integer Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dynamic Integer Width</em>' attribute.
	 * @see #setDynamicIntegerWidth(int)
	 * @see fr.irisa.cairn.idfix.model.fixedpointspecification.FixedpointspecificationPackage#getFixedPointInformation_DynamicIntegerWidth()
	 * @model
	 * @generated
	 */
	int getDynamicIntegerWidth();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation#getDynamicIntegerWidth <em>Dynamic Integer Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dynamic Integer Width</em>' attribute.
	 * @see #getDynamicIntegerWidth()
	 * @generated
	 */
	void setDynamicIntegerWidth(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	int getFractionalWidth();

} // FixedPointInformation
