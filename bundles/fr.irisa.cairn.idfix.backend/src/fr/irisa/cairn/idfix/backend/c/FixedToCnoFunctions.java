package fr.irisa.cairn.idfix.backend.c;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.idfix.backend.utils.BackEndUtils;
import gecos.core.ProcedureSet;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.CallInstruction;
import gecos.instrs.Instruction;
import gecos.types.ACFixedType;
import gecos.types.FloatType;
import gecos.types.IntegerType;
import gecos.types.Type;

/**
 * FIXME merge with {@link InlineCFixedPointBackend}
 * @author aelmouss
 */
public class FixedToCnoFunctions extends AbstractCFixedPointBackendModel {
	
	private static final boolean USE_STDINT_TYPES = true;
	private static final boolean PRAGMA_SYMBOL_DECLARTIONS = true;

	public FixedToCnoFunctions(GecosProject proj) {
		super(proj);
	}

	@Override
	protected Type createFixType(Symbol symbol, ACFixedType fixType) {
		Type type = BackEndUtils.createIntTypeFromACFixed(fixType, USE_STDINT_TYPES);
		if(PRAGMA_SYMBOL_DECLARTIONS && symbol != null) {
			GecosUserAnnotationFactory.pragma(symbol, fixType.toString());
		}
		return type;
		
//		long size = fixType.getBitwidth();
//		if(size < 0 || size%8 != 0 || size > 64) {
//			_logger.warn("[createFixType] size may not be supported! " + size);
//		}
//		
//		StringBuffer name = new StringBuffer();
//		name.append(fixType.isSigned()? "" : "u").append("fix").append(size).append("_t(").append(fixType.getFraction()).append(")");
//		BaseType aliased = GecosUserTypeFactory.INT();
//		aliased.setSize(size);
//		aliased.setSigned(fixType.isSigned());
//		AliasType type = GecosUserTypeFactory.ALIAS(aliased, name.toString());
////		GecosUserAnnotationFactory.pragma(type, PragmaAnnotation.CODEGEN_IGNORE_ANNOTATION);
//		
//		return type;
	}
	

	@Override
	protected Instruction createIntToFixInstruction(Instruction i, IntegerType fromIntType, ACFixedType toFixType) {
		long scaleAmount = -toFixType.getFraction();
		Instruction scaleInstruction = createScaleInstruction(i, toFixType, scaleAmount);
		return scaleInstruction;
		
//		if(INLINE_SCALAR_CONVERSIONS && (i instanceof IntInstruction)) {
//			Type type = BackEndUtils.createIntTypeFromACFixed(fixType, null, false); //XXX createFixType(null, fixType);
//			return BackEndUtils.convertInt2FixInst(type, fixType, ((IntInstruction)i).getValue());
//		}
//		else {
//			String name = PREFIX + "intToFix" + fixType.getBitwidth() + (fixType.isSigned()?"":"u");
//			Symbol address = GecosUserCoreFactory.procSymbol(name, fixType, new ArrayList<ParameterSymbol>()); //FIXME function parameter types
//			long format = USE_W_I_FORMAT ?  fixType.getInteger() : fixType.getFraction();
//			Instruction intToFix = GecosUserInstructionFactory.call(address, 
//					GecosUserInstructionFactory.Int(format), i);
//			return intToFix;
//		}
	}

	@Override
	protected Instruction createFloatToFixInstruction(Instruction f, FloatType fromFloatType, ACFixedType toFixType) {
		long scaleAmount = -toFixType.getFraction();
		Instruction scaleInstruction = createScaleInstruction(f, toFixType, scaleAmount);
		return scaleInstruction;
		
//		if(INLINE_SCALAR_CONVERSIONS && (i instanceof IntInstruction)) {
//			Type type = BackEndUtils.createIntTypeFromACFixed(fixType, null, false); //XXX createFixType(null, fixType);
//			return BackEndUtils.convertInt2FixInst(type, fixType, ((IntInstruction)i).getValue());
//		}
//		else {
//			String name = PREFIX + "intToFix" + fixType.getBitwidth() + (fixType.isSigned()?"":"u");
//			Symbol address = GecosUserCoreFactory.procSymbol(name, fixType, new ArrayList<ParameterSymbol>()); //FIXME function parameter types
//			long format = USE_W_I_FORMAT ?  fixType.getInteger() : fixType.getFraction();
//			Instruction intToFix = GecosUserInstructionFactory.call(address, 
//					GecosUserInstructionFactory.Int(format), i);
//			return intToFix;
//		}
	}
	
	/** 
	 * FIXME: use ACfixed quantization mode information and apply the 
	 * correspondent quantization (Truncation or Rounding ...) accordingly
	 * Currently we always apply truncation using cast operation.
	 */
	@Override
	protected Instruction createFixtToFixInstruction(Instruction child, ACFixedType fromFixType, ACFixedType toFixType) {
//		if(fromFixType.isSame(toFixType))
//			return i;
		
		return createInlineFixToFixInstruction(child, fromFixType, toFixType);
	}

	public static Instruction createInlineFixToFixInstruction(Instruction child,
			ACFixedType fromFixType, ACFixedType toFixType) {
		int toBW = (int)toFixType.getBitwidth();
//		int toIW = (int)toFixType.getInteger();
		int toFW = (int)toFixType.getFraction();
		
		int fromBW = (int)fromFixType.getBitwidth();
		int fromIW = (int)fromFixType.getInteger();
		int fromFW = (int)fromFixType.getFraction();
		boolean fromSigned = fromFixType.isSigned();
		
		System.out.println("scale: " + child + " from: " + fromFixType + " to: " + toFixType);
		
		int scaleAmount = fromFW - toFW;
		if(scaleAmount > 0) {
			/** >>(fromFW - toFW) first, then cast if toBW != fromBW */
			_logger.info("   shift");
			
			ACFixedType type = (ACFixedType) GecosUserTypeFactory.ACFIXED(fromBW, fromIW + scaleAmount, fromSigned);
			Instruction scaleInstruction = createScaleInstruction(child, type, scaleAmount);//XXX check type
			if(toBW != fromBW) {
				_logger.info("      cast");
//				type = (ACFixedType) GecosUserTypeFactory.ACFIXED(toBW, toIW, fromSigned);
				Instruction castInstruction = createCastInstruction(scaleInstruction, toFixType);//XXX check type: type must be equal to castType
				return castInstruction;
			}
			return scaleInstruction;
		}
		else if(scaleAmount < 0) {
			if(toBW > fromBW) {
				/** cast first, then <<(toFW - fromFW) */
				_logger.info("   cast(shift)");
				
				ACFixedType type = (ACFixedType) GecosUserTypeFactory.ACFIXED(toBW, fromIW - scaleAmount, fromSigned);
				Instruction castInstruction = createCastInstruction(child, type);//XXX check type
				
//				type = (ACFixedType) GecosUserTypeFactory.ACFIXED((int)type.getBitwidth(), (int)type.getInteger()+(fromFW-toFW), type.isSigned());
				Instruction scaleInstruction = createScaleInstruction(castInstruction, toFixType, scaleAmount);//XXX check type: type must be equal to castType
				return scaleInstruction;
			}
			else {
				/** shift and cast (order of << and cast is not important) */
				_logger.info("   shift");
				ACFixedType type = (ACFixedType) GecosUserTypeFactory.ACFIXED(fromBW, fromIW + scaleAmount, fromSigned);
				Instruction scaleInstruction = createScaleInstruction(child, type, scaleAmount);//XXX check type
				
				if(toBW != fromBW) {
					_logger.info("      cast");
					
//					type = (ACFixedType) GecosUserTypeFactory.ACFIXED(toBW, toIW, type.isSigned());
					Instruction castInstruction = createCastInstruction(scaleInstruction, toFixType);//XXX check type: type must be equal to castType
					return castInstruction;
				}
				return scaleInstruction;
			}
		}
		else {
			/** no shift is needed, cast if toBW != fromBW */
			if(toBW != fromBW) {
				_logger.info("   cast");
//				ACFixedType type = (ACFixedType) GecosUserTypeFactory.ACFIXED(toBW, toIW, fromSigned);//XXX check
				Instruction castInstruction = createCastInstruction(child, toFixType);//XXX check type: type must be equal to castType
				return castInstruction;
			}
			return child;
		}
	}
	
	@Override
	protected Instruction createFixedFunctionCall(CallInstruction c, ACFixedType fixType) {
//		throw new RuntimeException("not implemented yet! " + c);
		return scale(c, fixType);
	}
	
	/**
	 * @param child
	 * @param type
	 * @param scaleAmount: positive for >>, negative for <<
	 * @return (child >> scaleAmount) if scaleAmount > 0, (child << -scaleAmount) otherwise
	 */
	public static Instruction createScaleInstruction(Instruction child, Type type, long scaleAmount) {
		Instruction scaleInstruction;
		if(scaleAmount > 0)
			scaleInstruction = GecosUserInstructionFactory.shr(child, scaleAmount);
		else if (scaleAmount < 0)
			scaleInstruction = GecosUserInstructionFactory.shl(child, -scaleAmount);
		else 
			scaleInstruction = child;
		
		return scaleInstruction;
	}
	
	/**
	 * @param child
	 * @param castType
	 * @return
	 */
	public static Instruction createCastInstruction(Instruction child, Type castType) {
		return GecosUserInstructionFactory.cast(castType, child);
	}

	@Override
	protected void addHeaders(ProcedureSet ps) {
		if(USE_STDINT_TYPES)
			GecosUserAnnotationFactory.pragma(ps, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION + "#include <stdint.h>");
	}

}
