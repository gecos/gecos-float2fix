package fr.irisa.cairn.idfix.backend;

import org.apache.logging.log4j.Logger;

import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.logging.IDFixLogger;
import gecos.annotations.PragmaAnnotation;
import gecos.blocks.CompositeBlock;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.ScopeContainer;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;
import gecos.types.ACFixedType;
import gecos.types.FunctionType;
import gecos.types.Type;

/**
 * @author aelmouss
 */
public abstract class AbstractFixedPointBackendModel extends GecosBlocksInstructionsDefaultVisitor {

	protected static final Logger _logger = IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_BACKEND);
	protected GecosProject project;
	
	
	public AbstractFixedPointBackendModel(GecosProject proj) {
		this.project = proj;
	}
	
	/**
	 * @param symbol for which the fixed-point type is being created. Optional can be null. May be used to add annotations for example.
	 * @param fixType is the fixed-point type specification for which the backend fixed-point type is to be created
	 * @return the backend fixed-point type representing {@code fixType}
	 */
	protected abstract Type createFixType(Symbol symbol, ACFixedType fixType);
	
	/**
	 * Add necessary includes and header files location for code generation.
	 * Following pragma annotations can be used for this matter: <ul>
	 * <li>To add an include in the beginning of the ps:  {@link PragmaAnnotation#CODEGEN_PRINT_ANNOTATION} + "#include <necessary_header_file>" </li>
	 * <li>To add a filepath to be generated(copied) during code generation: {@link PragmaAnnotation#CODEGEN_COPYFILE_ANNOTATION} + <necessary_filepath>
	 * @param ps
	 */
	protected abstract void addHeaders(ProcedureSet ps);
	
	
	public void compute() {
		for(GecosSourceFile src : project.getSources())
			this.visit(src.getModel());
	}
	
	@Override
	public void visit(ProcedureSet ps) 
	{
		/* 1.Convert instructions */
		super.visit(ps);
		
		/* 2.Convert Symbols */
		visitScope(ps);
		
		/* 3. Generate/Add required header files */
		addHeaders(ps);
	}

	@Override
	public void visit(Procedure p) 
	{
		/* 1.Convert instructions */
		super.visit(p);
		
		/* 2.Convert Symbols */
		visitScope(p.getSymbol());
		
		/* 3.Update Procedure type */
		ProcedureSymbol psym = p.getSymbol();
		FunctionType ftype = (FunctionType) psym.getType();
		ftype.clearParameters();
		for(ParameterSymbol param : p.listParameters())
			ftype.addParameter(param.getType());	
		
		if(ftype.getReturnType() != null) {
			TypeAnalyzer retTypeAnalyzer = new TypeAnalyzer(ftype.getReturnType());
			
			if(retTypeAnalyzer.getBaseLevel().isFixedPoint()) {
				Type fixType = createFixType(null, (ACFixedType)retTypeAnalyzer.getBase());
				fixType = retTypeAnalyzer.revertAllLevelsOn(fixType);
				ftype.setReturnType(fixType);
			}
		}
	}

	@Override
	public void visitCompositeBlock(CompositeBlock c) 
	{
		/* 1.Convert instructions */
		super.visitCompositeBlock(c);
		
		/* 2.Convert Symbols */
		visitScope(c);
	}
	
	protected void visitScope(ScopeContainer sc) {
		if(sc.getScope() == null) 
			return;
		for(Symbol symbol : sc.getScope().getSymbols()) {
			visitSymbol(symbol);
		}
	}

	protected void visitSymbol(Symbol symbol) {
		TypeAnalyzer typeAnalyser = new TypeAnalyzer(symbol.getType());
		if(typeAnalyser.getBaseLevel().isFixedPoint()) {
			Type fixType = createFixType(symbol, (ACFixedType)typeAnalyser.getBase());
			
			/* 1- visit symbol value instructions */
			if(symbol != null && symbol.getValue() != null) {
				symbol.getValue().accept(this);
			}
			
			/* 2- Convert symbol's type from ACFixed to Integer */
			fixType = typeAnalyser.revertAllLevelsOn(fixType);
			symbol.setType(fixType);
		}
	}
	
}