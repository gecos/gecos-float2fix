//------------------------------------------------------------------------
//       ______     ______     _____
//      / ____/__  / ____/___ / ___/
//     / / __/ _ \/ /   / __ \\__ \
//    / /_/ /  __/ /___/ /_/ /__/ /
//    \____/\___/\____/\____/____/  --Generic Compiler Suite--
//                              	   (c) INRIA-IRISA 2011-2015
//------------------------------------------------------------------------
// @author: aelmouss
//------------------------------------------------------------------------
//NOTE: Rounding and Saturation are not supported yet!


#ifndef __IDFIX_H__
#define __IDFIX_H__

#include <stdint.h>
#include <math.h> 	//XXX

#define __INLINE_	inline


#ifndef __PREFIX_
	#define __PREFIX_(name)	alma_##name
#endif

#ifndef __FLOAT_TYPE_
	#define __FLOAT_TYPE_ 	float
#endif

#ifndef __MAX_INT_T
	#define __MAX_INT_BW	32
	#define __MAX_INT_T		int32_t
	#define __MAX_UINT_T	uint32_t
#endif

//TODO
//typedef enum {Q_TRUNC, Q_ROUND} quant_mode_t;
//typedef enum {S_WRAP, S_SAT} sat_mode_t;
//#define __QUANTIZATION_MODE = Q_TRUNC
//#define __QUANTIZATION_MODE = S_WRAP

/*==============================*/
/* Fixed-point data types 		*/
/*==============================*/
typedef int8_t 		fix8_t;
typedef uint8_t 	ufix8_t;

typedef int16_t 	fix16_t;
typedef uint16_t 	ufix16_t;

typedef int32_t 	fix32_t;
typedef uint32_t 	ufix32_t;

/* */
#define fix8_t(fw)		fix8_t
#define ufix8_t(fw)		ufix8_t
#define fix16_t(fw)		fix16_t
#define ufix16_t(fw)	ufix16_t
#define fix32_t(fw)		fix32_t
#define ufix32_t(fw)	ufix32_t

#define _fix_t(bw)		fix##bw##_t
#define _ufix_t(bw)		ufix##bw##_t

/*==============================*/
/* Macros				 		*/
/*==============================*/

/** fix to fix **/
#define _scale_(value, amount, type)                  \
	((amount) > 0 ? (type)((value) >> (amount))    :  \
	((amount) < 0 ? (((type)(value)) << -(amount)) :  (type)(value)))

#define _fixToFix_m(out_bw, out_fw, in_fw, value)	_scale_(value, (in_fw)-(out_fw), _fix_t(out_bw))
#define _UfixToFix_m(out_bw, out_fw, in_fw, value)	_scale_(value, (in_fw)-(out_fw), _ufix_t(out_bw))

/** fix_one **/
#define _fixOne_m(bw, fw)	((_fix_t(bw)) (1<<(fw)))
#define _UfixOne_m(bw, fw)	((_ufix_t(bw)) (1<<(fw)))

/** float to fix **/
#define _floatToFix_m(float_type, bw, fw, value)	((_fix_t(bw))((float_type)_fixOne_m(bw, fw) * (float_type)(value)))
#define _UfloatToFix_m(float_type, bw, fw, value)	((_ufix_t(bw))((float_type)_UfixOne_m(bw, fw) * (float_type)(value)))

/** fix to float **/
#define _fixToFloat_m(float_type, bw, fw, value)	((float_type)(value) / _fixOne_m(bw, fw))
#define _UfixToFloat_m(float_type, bw, fw, value)	((float_type)(value) / _UfixOne_m(bw, fw))

/** fix to int **/
#define _fixToInt_m(fw, value)			_fixToFix_m(__MAX_INT_BW, 0, fw, value)
#define _UfixToInt_m(fw, value)			_UfixToFix_m(__MAX_INT_BW, 0, fw, value)

/** int to fix **/
#define _intToFix_m(bw, fw, value)		_fixToFix_m(bw, fw, 0, value)
#define _UintToFix_m(bw, fw, value)		_UfixToFix_m(bw, fw, 0, value)

/** Arith operations **/
#define _fixMul_m(bw, fw, a, b)		_floatToFix_m(__FLOAT_TYPE_, bw, fw, _fixToFloat_m(__FLOAT_TYPE_, bw, fw, a) * _fixToFloat_m(__FLOAT_TYPE_, bw, fw, b))
#define _fixDiv_m(bw, fw, a, b)		_floatToFix_m(__FLOAT_TYPE_, bw, fw, _fixToFloat_m(__FLOAT_TYPE_, bw, fw, a) / _fixToFloat_m(__FLOAT_TYPE_, bw, fw, b))
#define _fixAdd_m(bw, fw, a, b)		_floatToFix_m(__FLOAT_TYPE_, bw, fw, _fixToFloat_m(__FLOAT_TYPE_, bw, fw, a) + _fixToFloat_m(__FLOAT_TYPE_, bw, fw, b))
#define _fixSub_m(bw, fw, a, b)		_floatToFix_m(__FLOAT_TYPE_, bw, fw, _fixToFloat_m(__FLOAT_TYPE_, bw, fw, a) - _fixToFloat_m(__FLOAT_TYPE_, bw, fw, b))

/** Math functions **/
//TODO
#define _fixCos_m(bw, fw, a)	    _floatToFix_m(__FLOAT_TYPE_, bw, fw, cos(_fixToFloat_m(__FLOAT_TYPE_, bw, fw, a)))
#define _fixSin_m(bw, fw, a)        _floatToFix_m(__FLOAT_TYPE_, bw, fw, sin(_fixToFloat_m(__FLOAT_TYPE_, bw, fw, a)))
#define _fixSqrt_m(bw, fw, a)       _floatToFix_m(__FLOAT_TYPE_, bw, fw, sqrt(_fixToFloat_m(__FLOAT_TYPE_, bw, fw, a)))
#define _fixfloor_m(bw, fw, a)      _floatToFix_m(__FLOAT_TYPE_, bw, fw, floor(_fixToFloat_m(__FLOAT_TYPE_, bw, fw, a)))
#define _fixRound_m(bw, fw, a)      _floatToFix_m(__FLOAT_TYPE_, bw, fw, round(_fixToFloat_m(__FLOAT_TYPE_, bw, fw, a)))
#define _fixCeil_m(bw, fw, a)       _floatToFix_m(__FLOAT_TYPE_, bw, fw, ceil(_fixToFloat_m(__FLOAT_TYPE_, bw, fw, a)))
#define _fixExp_m(bw, fw, a)        _floatToFix_m(__FLOAT_TYPE_, bw, fw, exp(_fixToFloat_m(__FLOAT_TYPE_, bw, fw, a)))
#define _fixLog_m(bw, fw, a)        _floatToFix_m(__FLOAT_TYPE_, bw, fw, log(_fixToFloat_m(__FLOAT_TYPE_, bw, fw, a)))
#define _fixLog2_m(bw, fw, a)       _floatToFix_m(__FLOAT_TYPE_, bw, fw, log2(_fixToFloat_m(__FLOAT_TYPE_, bw, fw, a)))
#define _fixAtan2_m(bw, fw, a, b)   _floatToFix_m(__FLOAT_TYPE_, bw, fw, atan2(_fixToFloat_m(__FLOAT_TYPE_, bw, fw, a), _fixToFloat_m(__FLOAT_TYPE_, bw, fw, b)))
#define _fixPow_m(bw, fw, a, b)     _floatToFix_m(__FLOAT_TYPE_, bw, fw, pow(_fixToFloat_m(__FLOAT_TYPE_, bw, fw, a), _fixToFloat_m(__FLOAT_TYPE_, bw, fw, b)))

//#define _fixMax_m(bw, fw, a, b)      //TODO
//#define _fixMin_m(bw, fw, a, b)      //TODO

/*==============================*/
/* Type Conversion Functions	*/
/*==============================*/
/* float to fixed Conversion */
__INLINE_ fix8_t   __PREFIX_(floatToFix8)  (int fw, __FLOAT_TYPE_ value) { return _floatToFix_m (__FLOAT_TYPE_, 8,  fw, value); }
__INLINE_ fix16_t  __PREFIX_(floatToFix16) (int fw, __FLOAT_TYPE_ value) { return _floatToFix_m (__FLOAT_TYPE_, 16, fw, value); }
__INLINE_ fix32_t  __PREFIX_(floatToFix32) (int fw, __FLOAT_TYPE_ value) { return _floatToFix_m (__FLOAT_TYPE_, 32, fw, value); }
__INLINE_ ufix8_t  __PREFIX_(ufloatToFix8) (int fw, __FLOAT_TYPE_ value) { return _UfloatToFix_m(__FLOAT_TYPE_, 8,  fw, value); }
__INLINE_ ufix16_t __PREFIX_(ufloatToFix16)(int fw, __FLOAT_TYPE_ value) { return _UfloatToFix_m(__FLOAT_TYPE_, 16, fw, value); }
__INLINE_ ufix32_t __PREFIX_(ufloatToFix32)(int fw, __FLOAT_TYPE_ value) { return _UfloatToFix_m(__FLOAT_TYPE_, 32, fw, value); }

/* fix to float */
__INLINE_ __FLOAT_TYPE_ __PREFIX_(fix8ToFloat)  (int fw, fix8_t   value) { return _fixToFloat_m (__FLOAT_TYPE_, 8,  fw, value); }
__INLINE_ __FLOAT_TYPE_ __PREFIX_(fix16ToFloat) (int fw, fix16_t  value) { return _fixToFloat_m (__FLOAT_TYPE_, 16, fw, value); }
__INLINE_ __FLOAT_TYPE_ __PREFIX_(fix32ToFloat) (int fw, fix32_t  value) { return _fixToFloat_m (__FLOAT_TYPE_, 32, fw, value); }
__INLINE_ __FLOAT_TYPE_ __PREFIX_(ufix8ToFloat) (int fw, ufix8_t  value) { return _UfixToFloat_m(__FLOAT_TYPE_, 8,  fw, value); }
__INLINE_ __FLOAT_TYPE_ __PREFIX_(ufix16ToFloat)(int fw, ufix16_t value) { return _UfixToFloat_m(__FLOAT_TYPE_, 16, fw, value); }
__INLINE_ __FLOAT_TYPE_ __PREFIX_(ufix32ToFloat)(int fw, ufix32_t value) { return _UfixToFloat_m(__FLOAT_TYPE_, 32, fw, value); }

/*
 * fixed to fixed Conversion:
 * ! sign indicator is the same for both input and output
 */
__INLINE_ fix8_t   __PREFIX_(fixToFix8)  (int out_fw, int in_fw, __MAX_INT_T  value) { return _fixToFix_m (8,  out_fw, in_fw, value); }
__INLINE_ fix16_t  __PREFIX_(fixToFix16) (int out_fw, int in_fw, __MAX_INT_T  value) { return _fixToFix_m (16, out_fw, in_fw, value); }
__INLINE_ fix32_t  __PREFIX_(fixToFix32) (int out_fw, int in_fw, __MAX_INT_T  value) { return _fixToFix_m (32, out_fw, in_fw, value); }
__INLINE_ ufix8_t  __PREFIX_(ufixToFix8) (int out_fw, int in_fw, __MAX_UINT_T value) { return _UfixToFix_m(8,  out_fw, in_fw, value); }
__INLINE_ ufix16_t __PREFIX_(ufixToFix16)(int out_fw, int in_fw, __MAX_UINT_T value) { return _UfixToFix_m(16, out_fw, in_fw, value); }
__INLINE_ ufix32_t __PREFIX_(ufixToFix32)(int out_fw, int in_fw, __MAX_UINT_T value) { return _UfixToFix_m(32, out_fw, in_fw, value); }

/*
 * int to fixed Conversion
 * ! sign indicator is the same for both input and output
 */
__INLINE_ fix8_t   __PREFIX_(intToFix8)  (int fw, __MAX_INT_T  value) { return _intToFix_m (8,  fw, value); }
__INLINE_ fix16_t  __PREFIX_(intToFix16) (int fw, __MAX_INT_T  value) { return _intToFix_m (16, fw, value); }
__INLINE_ fix32_t  __PREFIX_(intToFix32) (int fw, __MAX_INT_T  value) { return _intToFix_m (32, fw, value); }
__INLINE_ ufix8_t  __PREFIX_(uintToFix8) (int fw, __MAX_UINT_T value) { return _UintToFix_m(8,  fw, value); }
__INLINE_ ufix16_t __PREFIX_(uintToFix16)(int fw, __MAX_UINT_T value) { return _UintToFix_m(16, fw, value); }
__INLINE_ ufix32_t __PREFIX_(uintToFix32)(int fw, __MAX_UINT_T value) { return _UintToFix_m(32, fw, value); }

/*
 * fixed to int Conversion
 * ! sign indicator is the same for both input and output
 */
__INLINE_ __MAX_INT_T  __PREFIX_(fix8ToInt)  (int fw, fix8_t   value) { return _fixToInt_m (fw, value); }
__INLINE_ __MAX_INT_T  __PREFIX_(fix16ToInt) (int fw, fix16_t  value) { return _fixToInt_m (fw, value); }
__INLINE_ __MAX_INT_T  __PREFIX_(fix32ToInt) (int fw, fix32_t  value) { return _fixToInt_m (fw, value); }
__INLINE_ __MAX_UINT_T __PREFIX_(ufix8ToInt) (int fw, ufix8_t  value) { return _UfixToInt_m(fw, value); }
__INLINE_ __MAX_UINT_T __PREFIX_(ufix16ToInt)(int fw, ufix16_t value) { return _UfixToInt_m(fw, value); }
__INLINE_ __MAX_UINT_T __PREFIX_(ufix32ToInt)(int fw, ufix32_t value) { return _UfixToInt_m(fw, value); }

/*==============================*/
/* Arith operation Functions	*/
/*==============================*/
/*
 * Multiplication
 * ! fixed-point type is the same for inputs and output
 */
__INLINE_ fix8_t   __PREFIX_(fix_mul_8)  (int fw, fix8_t   a, fix8_t   b) { return _fixMul_m (8 , fw, a, b); }
__INLINE_ fix16_t  __PREFIX_(fix_mul_16) (int fw, fix16_t  a, fix16_t  b) { return _fixMul_m (16, fw, a, b); }
__INLINE_ fix32_t  __PREFIX_(fix_mul_32) (int fw, fix32_t  a, fix32_t  b) { return _fixMul_m (32, fw, a, b); }

/*
 * Division
 * ! fixed-point type is the same for inputs and output
 */
__INLINE_ fix8_t   __PREFIX_(fix_div_8)  (int fw, fix8_t   a, fix8_t   b) { return _fixDiv_m (8 , fw, a, b); }
__INLINE_ fix16_t  __PREFIX_(fix_div_16) (int fw, fix16_t  a, fix16_t  b) { return _fixDiv_m (16, fw, a, b); }
__INLINE_ fix32_t  __PREFIX_(fix_div_32) (int fw, fix32_t  a, fix32_t  b) { return _fixDiv_m (32, fw, a, b); }

/*
 * Addition
 * ! fixed-point type is the same for inputs and output
 */
__INLINE_ fix8_t   __PREFIX_(fix_add_8)  (int fw, fix8_t   a, fix8_t   b) { return _fixAdd_m (8 , fw, a, b); }
__INLINE_ fix16_t  __PREFIX_(fix_add_16) (int fw, fix16_t  a, fix16_t  b) { return _fixAdd_m (16, fw, a, b); }
__INLINE_ fix32_t  __PREFIX_(fix_add_32) (int fw, fix32_t  a, fix32_t  b) { return _fixAdd_m (32, fw, a, b); }

/*
 * Subtraction
 * ! fixed-point type is the same for inputs and output
 */
__INLINE_ fix8_t   __PREFIX_(fix_sub_8)  (int fw, fix8_t   a, fix8_t   b) { return _fixSub_m (8 , fw, a, b); }
__INLINE_ fix16_t  __PREFIX_(fix_sub_16) (int fw, fix16_t  a, fix16_t  b) { return _fixSub_m (16, fw, a, b); }
__INLINE_ fix32_t  __PREFIX_(fix_sub_32) (int fw, fix32_t  a, fix32_t  b) { return _fixSub_m (32, fw, a, b); }

/*
 * Reminder
 * ! fixed-point type is the same for inputs and output
 */
__INLINE_ fix32_t __PREFIX_(fix_rem_32)(int fw, fix32_t a) { return _fixCos_m(32, fw, a); }

/*==============================*/
/* Math Functions				*/
/*==============================*/
__INLINE_ fix8_t  __PREFIX_(fix_cos_8) (int fw, fix8_t  a) { return _fixCos_m(32, fw, a); }
__INLINE_ fix16_t __PREFIX_(fix_cos_16)(int fw, fix16_t a) { return _fixCos_m(32, fw, a); }
__INLINE_ fix32_t __PREFIX_(fix_cos_32)(int fw, fix32_t a) { return _fixCos_m(32, fw, a); }

__INLINE_ fix8_t  __PREFIX_(fix_sin_8) (int fw, fix8_t  a) { return _fixSin_m(32, fw, a); }
__INLINE_ fix16_t __PREFIX_(fix_sin_16)(int fw, fix16_t a) { return _fixSin_m(32, fw, a); }
__INLINE_ fix32_t __PREFIX_(fix_sin_32)(int fw, fix32_t a) { return _fixSin_m(32, fw, a); }

__INLINE_ fix32_t __PREFIX_(fix_sqrt_32)(int fw, fix32_t a)  { return _fixSqrt_m(32, fw, a); }
__INLINE_ fix32_t __PREFIX_(fix_floor_32)(int fw, fix32_t a) { return _fixfloor_m(32, fw, a); }
__INLINE_ fix32_t __PREFIX_(fix_round_32)(int fw, fix32_t a) { return _fixRound_m(32, fw, a); }
__INLINE_ fix32_t __PREFIX_(fix_ceil_32)(int fw, fix32_t a)  { return _fixCeil_m(32, fw, a); }
__INLINE_ fix32_t __PREFIX_(fix_exp_32)(int fw, fix32_t a)   { return _fixExp_m(32, fw, a); }
__INLINE_ fix32_t __PREFIX_(fix_log_32)(int fw, fix32_t a)   { return _fixLog_m(32, fw, a); }
__INLINE_ fix32_t __PREFIX_(fix_log2_32)(int fw, fix32_t a)  { return _fixLog2_m(32, fw, a); }
__INLINE_ fix32_t __PREFIX_(fix_atan2_32)(int fw, fix32_t a, fix32_t b) { return _fixAtan2_m(32, fw, a, b); }
__INLINE_ fix32_t __PREFIX_(fix_pow_32)(int fw, fix32_t a, fix32_t b)   { return _fixPow_m(32, fw, a, b); }

//__INLINE_ fix32_t __PREFIX_(fix_max_32)(int fw, fix32_t a, fix32_t b)   { return _fixMax_m(32, fw, a, b); }
//__INLINE_ fix32_t __PREFIX_(fix_min_32)(int fw, fix32_t a, fix32_t b)   { return _fixMin_m(32, fw, a, b); }


#endif /* !defined(__IDFIX_H__) */
