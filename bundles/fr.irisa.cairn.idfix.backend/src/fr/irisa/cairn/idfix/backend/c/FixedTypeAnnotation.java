package fr.irisa.cairn.idfix.backend.c;

import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation;
import gecos.annotations.impl.IAnnotationImpl;

import java.security.InvalidParameterException;

public class FixedTypeAnnotation extends IAnnotationImpl {
	private FixedPointInformation[] _operandsFixedPtInfo;
	
	
	public FixedTypeAnnotation(FixedPointInformation... operands) {
		this._operandsFixedPtInfo = operands;
	}

	public FixedPointInformation getInput(int index) {
		if(index < 0 || index > 1)
			throw new InvalidParameterException("can only have 2 inputs");
		return _operandsFixedPtInfo[index];
	}
	
	public FixedPointInformation getOutput() {
		return _operandsFixedPtInfo[2];
	}
	
	@Override
	public String toString() {
		return _operandsFixedPtInfo.toString();
	}
}