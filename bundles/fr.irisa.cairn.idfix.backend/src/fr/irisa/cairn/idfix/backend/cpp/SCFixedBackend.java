package fr.irisa.cairn.idfix.backend.cpp;

import fr.irisa.cairn.float2fix.model.extended.factory.ExtendedTypeFactory;
import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.idfix.backend.AbstractFixedPointBackendModel;
import gecos.core.ProcedureSet;
import gecos.core.Symbol;
import gecos.extended.types.OverflowMode;
import gecos.extended.types.QuantificationMode;
import gecos.gecosproject.GecosProject;
import gecos.instrs.ConvertInstruction;
import gecos.types.ACFixedType;
import gecos.types.Type;

public class SCFixedBackend extends AbstractFixedPointBackendModel {
	public SCFixedBackend(GecosProject proj) {
		super(proj);
	}
	
	@Override
	protected Type createFixType(Symbol symbol, ACFixedType fixType) {
		int bitwidth = (int) fixType.getBitwidth();
		int integerWidth = (int) fixType.getInteger();
		QuantificationMode quantification = getScQuant(fixType.getQuantificationMode());
		OverflowMode overflow = getScOver(fixType.getOverflowMode());
		ExtendedTypeFactory.setScope(fixType.getContainingScope());
		
		
		Type scType =  fixType.isSigned()? 
				ExtendedTypeFactory.SCFIXED(bitwidth, integerWidth, quantification, overflow) : 
				ExtendedTypeFactory.SCUFIXED(bitwidth, integerWidth, quantification, overflow);
				
		return scType;
	}

	 
	@Override
	public void visitConvertInstruction(ConvertInstruction c) {
		c.getExpr().accept(this);
		if(c.getType() instanceof ACFixedType){
			Type newType = processType(c.getType());
			c.setType(newType);
		}

//		ACFixedType castType = _currentCastType;
//		
//		if(c.getType() instanceof ACFixedType) {
//			_currentCastType = (ACFixedType) c.getType();
//		}
//		else {
//			_currentCastType = null; //FIXME support fix_to_int, fix_to_float ..
//		}
//		
//		/* visit innermost first */
//		Instruction child = c.getChild(0);
//		child.accept(this);
//		
//		_currentCastType = castType;
//		
//		/* in case child was substituted */
//		child = c.getChild(0);
//		
//		if(c.getType() instanceof ACFixedType)
//			c.substituteWith(child);
//		else {
//			Type newType = processType(c.getType());
//			if(newType != c.getType())
//				c.setType(newType);
//		}
	}

	@Override
	protected void addHeaders(ProcedureSet ps) {
		GecosUserAnnotationFactory.pragma(ps, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION+"#define SC_INCLUDE_FX");
		GecosUserAnnotationFactory.pragma(ps, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION+"#include \"systemc.h\"");
	}
	
	/**
	 * XXX use same QuantificationMode model for both ACFixed and SCFixed
	 */
	private OverflowMode getScOver(gecos.types.OverflowMode overflowMode) {
		switch(overflowMode.getValue()) {
		case gecos.types.OverflowMode.AC_SAT_VALUE:
			return OverflowMode.SC_SAT;
		case gecos.types.OverflowMode.AC_SAT_SYM_VALUE:
			return OverflowMode.SC_SAT_SYM;
		case gecos.types.OverflowMode.AC_SAT_ZERO_VALUE:
			return OverflowMode.SC_SAT_ZERO;
		case gecos.types.OverflowMode.AC_WRAP_VALUE:
			return OverflowMode.SC_WRAP;
		}
		return null;
	}

	/**
	 * XXX use same QuantificationMode model for both ACFixed and SCFixed
	 */
	private QuantificationMode getScQuant(gecos.types.QuantificationMode quantificationMode) {
		switch(quantificationMode.getValue()) {
		case gecos.types.QuantificationMode.AC_TRN_VALUE:
			return QuantificationMode.SC_TRN;
		case gecos.types.QuantificationMode.AC_RND_VALUE:
			return QuantificationMode.SC_RND;
		//TODO add other cases
		}
		return null;
	}

	private Type processType(Type type) {
		TypeAnalyzer typeAnalyser = new TypeAnalyzer(type);
		if(typeAnalyser.getBaseLevel().isFixedPoint()) {
			Type fixType = createFixType(null, (ACFixedType)typeAnalyser.getBase());
			
			return typeAnalyser.revertAllLevelsOn(fixType);
		}
		return type;
	}
	
}
