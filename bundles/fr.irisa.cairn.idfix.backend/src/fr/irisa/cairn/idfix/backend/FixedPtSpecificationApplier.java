package fr.irisa.cairn.idfix.backend;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.tools.switches.DAGDefaultInstructionSwitch;
import fr.irisa.cairn.gecos.model.tools.switches.SymbolSwitch;
import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor;
import fr.irisa.cairn.idfix.backend.c.FixedTypeAnnotation;
import fr.irisa.cairn.idfix.backend.utils.BackEndUtils;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Data;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Operation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixProject;
import fr.irisa.cairn.idfix.utils.IDFixUtils;
import gecos.blocks.BasicBlock;
import gecos.blocks.CompositeBlock;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.ScopeContainer;
import gecos.core.Symbol;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;
import gecos.dag.DAGOpNode;
import gecos.dag.DAGSimpleArrayNode;
import gecos.dag.DAGSymbolNode;
import gecos.gecosproject.GecosProject;
import gecos.instrs.ArrayValueInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.RetInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.ACFixedType;
import gecos.types.ArrayType;
import gecos.types.FunctionType;
import gecos.types.Type;

/**
 * Convert floating-point types CDFG to fixed-point with all necessary 
 * casts to reflect the semantic given by the fixed-point specification.
 * 
 * @author aelmouss
 */
public class FixedPtSpecificationApplier extends GecosBlocksInstructionsDefaultVisitor {
	
	public static boolean CHECK_SPEC_LSBMODE = false;
	
	private GecosProject gProj;
	private List<ProcedureSet> procSets;
	private FixedPointSpecification fixedPtSpecification;
	
	private ACFixedType _symbolACType;
	private ArrayValueInstruction _arrayValueInstruction;
	
	
	public FixedPtSpecificationApplier(IdfixProject idfixProj) {
		this(idfixProj.getGecosProjectAnnotedAndNotUnrolled(), idfixProj.getFixedPointSpecification());
	}
	
	public FixedPtSpecificationApplier(GecosProject gProj_original, FixedPointSpecification fixedPtSpecification) 
	{
		this.gProj = gProj_original;
		this.fixedPtSpecification = fixedPtSpecification;
		this.procSets = new ArrayList<ProcedureSet>();
		this.procSets.add(gProj.getSources().get(0).getModel()); //TODO add all procedure sets in gProj
	}
	
	public FixedPtSpecificationApplier(ProcedureSet ps, FixedPointSpecification fixedPtSpecification) 
	{
		this.procSets = new ArrayList<ProcedureSet>();
		this.procSets.add(ps);
		this.fixedPtSpecification = fixedPtSpecification;
		this.gProj = null;
	}
	
	/**
	 * Convert the original floating-point CDFG to AC_Fixed with all necessary 
	 * casts to reflect the semantic given by the fixed-point specification.
	 */
	public void compute() 
	{
		for(ProcedureSet ps : this.procSets) {
			/* 1. Convert symbols to ACFixed */
			new ConvertSymbolTypesToACFixed().doSwitch(ps);
			
			/* 2. Propagate */
			visit(ps);
		}
	}
	
	@Override
	public void visit(ProcedureSet ps) {
		if(ps.getScope() != null) {
			visitScope(ps.getScope());
		}
		
		super.visit(ps);
	}
	
	@Override
	public void visitCompositeBlock(CompositeBlock c) {
		if(c.getScope() != null) {
			visitScope(c.getScope());
		}
		
		super.visitCompositeBlock(c);
	}
	
	/**
	 * Convert {@link Symbol} values
	 * @param s
	 */
	public void visitScope(Scope s) {
		for(Symbol sym : s.getSymbols()) {
			Type symbolBaseType = new TypeAnalyzer(sym.getType()).getBase();
			if(sym.getValue() != null && symbolBaseType instanceof ACFixedType) {
				_symbolACType = (ACFixedType) symbolBaseType;
				sym.getValue().accept(this);
			}
		}
	}

	@Override
	public void visitBasicBlock(BasicBlock b) 
	{
		if(b.getInstructions().size() > 0) {
			for(Instruction inst : b.getInstructions()) {
				inst.accept(this);
			}
		}
	}
	
	@Override
	protected void visitComplexInstruction(ComplexInstruction c) {
		for(Instruction child : c.listChildren())
			child.accept(this);
	}
	
	@Override
	public void visitArrayValueInstruction(ArrayValueInstruction a) {
		ArrayValueInstruction parentArrayValInst = _arrayValueInstruction;
		_arrayValueInstruction = a;
		List<Instruction> children = new ArrayList<Instruction>(a.getChildren());
		for (Instruction i : children) {
			i.accept(this);
			if(!(i instanceof ArrayValueInstruction)) {
				ConvertInstruction cast = cast(i, _symbolACType);
				i.substituteWith(cast);
			}
		}
		_arrayValueInstruction = parentArrayValInst;
	}
	
	@Override
	public void visitSetInstruction(SetInstruction s) 
	{
		_assert(s.getChildrenCount() == 2, "SET Instruction must have 2 children!" + s);
		
		/* visit innermost first */
		visitComplexInstruction(s);
		
		Operation operator = findFixptSpec(s);
		Instruction dest = s.getDest();
		ACFixedType outType;
		if(operator != null) {
			EList<FixedPointInformation> fixPtInfo = operator.getOperands();
			_assert(fixPtInfo != null, "FIXME: fixPtSpec operator has no operands!" + operator);
			_assert(fixPtInfo.size() == 3, "XXX?: fixPtSpec operator is supposed to have 3 operands!" + operator);
			
			if(CHECK_SPEC_LSBMODE) checkFixedPtSpec_LSBMode(operator);
			
			outType = BackEndUtils.createACFixedFromFixedPtInfo(fixPtInfo.get(2));
			if(!dest.getType().isEqual(outType)) {
				System.out.println("XXX WARNING: SET destination's type is different from fixPtSpec");
			}
		}
		else if(dest.getType() instanceof ACFixedType) {
			outType = (ACFixedType) dest.getType().copy();
		}
		else {
			return;
		}

		ConvertInstruction cast = cast(s.getSource(), outType);
		s.getSource().substituteWith(cast);
		s.setType(outType);
	}
	
	@Override
	public void visitGenericInstruction(GenericInstruction g) 
	{
		//_assert(g.getChildrenCount() == 2, "Generic Instructions with more than 2 children are not supported yet!" + g);
		if(g.getChildrenCount() != 2)
			return;
		
		/* visit innermost first */
		visitComplexInstruction(g);
		
		Operation operator = findFixptSpec(g);
		if(operator != null) {
			EList<FixedPointInformation> fixPtInfo = operator.getOperands();
			_assert(fixPtInfo != null, "FIXME: fixPtSpec operator has no operands!" + operator);
			_assert(fixPtInfo.size() == 3, "FIXME: fixPtSpec operator is supposed to have 3 operands!" + operator);
			
			if(CHECK_SPEC_LSBMODE) checkFixedPtSpec_LSBMode(operator);
			
			ACFixedType op0Type = BackEndUtils.createACFixedFromFixedPtInfo(fixPtInfo.get(0));
			ACFixedType op1Type = BackEndUtils.createACFixedFromFixedPtInfo(fixPtInfo.get(1));
			ACFixedType outType = BackEndUtils.createACFixedFromFixedPtInfo(fixPtInfo.get(2));
			
			ConvertInstruction cast = cast(g.getChild(0), op0Type);
			g.replaceChild(g.getChild(0), cast);
			
			cast = cast(g.getChild(1), op1Type);
			g.replaceChild(g.getChild(1), cast);
			
			g.setType(outType);
			cast = cast(g, outType);
			g.substituteWith(cast);
		}
	}
	
	/**
	 * @param inst
	 * @param acFixedType
	 * @return a new {@link ConvertInstruction} to type 'acFixedType' containing
	 * a COPY of 'inst', if it is not a {@link ConvertInstruction}, or a COPY 
	 * of inst's child otherwise.  
	 */
	private ConvertInstruction cast(Instruction inst, ACFixedType acFixedType) 
	{
		if(inst instanceof ConvertInstruction) { // bypass casts
			inst = ((ConvertInstruction) inst).getExpr();
		}
		ConvertInstruction cast = GecosUserInstructionFactory.cast(acFixedType, inst.copy());
		return cast;
	}
	
	@Override
	public void visitCallInstruction(CallInstruction c) 
	{
		//FIXME: cast arguments. It won't be done since we replaced 
		//function calls by their values in the flattener => we don't get their fixed-point specification!
		
		/* visit innermost first */
		for(Instruction arg : c.getArgs()) {
			arg.accept(this);
		}
	}
	
	@Override
	public void visitSymbolInstruction(SymbolInstruction s) 
	{
		TypeAnalyzer typeAnalyzer = new TypeAnalyzer(s.getSymbol().getType());
		s.setType(typeAnalyzer.getBaseLevel().skipStaticConst()); //assuming symbol is not an array nor pointer type.
	}

	@Override
	public void visitSimpleArrayInstruction(SimpleArrayInstruction s) 
	{
		Type symType = s.getSymbol().getType();
		_assert(symType instanceof ArrayType, "type not supprted yet: " + symType);
		
		TypeAnalyzer typeAnalyzer = new TypeAnalyzer(symType);
		if(typeAnalyzer.getBaseLevel().isFixedPoint()) {
			s.setType(typeAnalyzer.getBase()); //XXX assumes that the nb index of s = nb dimensions of s.symbol array
		}
	}
	
	@Override
	public void visitDAGInstruction(DAGInstruction dag) 
	{
//		GecosUserTypeFactory.setScope(dag.getBasicBlock().getScope()); //XXX
		new DAGApplier().apply(dag);
	}
	
	
	@Override
	public void visitRetInstruction(RetInstruction r) {
		super.visitRetInstruction(r);
		r.setType(r.getExpr().getType().copy());
		
		// FIXME Need to detect if there are multiple return instruction in one procedure and check the type of each
		ProcedureSymbol psym = r.getContainingProcedure().getSymbol();
		FunctionType ftype = (FunctionType) psym.getType();
		ftype.setReturnType(r.getType());
	}

	private Operation findFixptSpec(Instruction i) {
		Integer numIDfix = IDFixUtils.getCDFGInstructionNumber(i);
		if(numIDfix == null)
			return null;
		return fixedPtSpecification.findOperatorFromConvCDFG(numIDfix);
	}
	
	private Operation findFixptSpec(DAGNode node) {
		Integer numIDfix = IDFixUtils.getCDFGInstructionNumber(node);
		if(numIDfix == null)
			return null;
		return fixedPtSpecification.findOperatorFromConvCDFG(numIDfix);
	}
	
	private Data findFixptSpec(Symbol symbol) {
		/* !! this is different from: fixedPtSpecification.findDataFromSymbol(symbol) !! */
		Integer numDataCDFG = IDFixUtils.getSymbolAnnotationNumber(symbol);
		if(numDataCDFG == null)
			return null;
		return fixedPtSpecification.findDataFromCDFG(numDataCDFG);
	}
	
	public static void checkFixedPtSpec_LSBMode(Operation operator) {
		int fw0 = operator.getOperands().get(0).getFractionalWidth();
		int fw1 = operator.getOperands().get(1).getFractionalWidth();
		int fwOut = operator.getOperands().get(2).getFractionalWidth();
		
		switch(operator.getKind().getValue()) {
		case OperatorKind.ADD_VALUE:
		case OperatorKind.SUB_VALUE:
			if(fw0 != fw1 || fw0 != fwOut)
				throw new RuntimeException("add operands not aligned! [" + fw0 + " " + fw1 + " " + fwOut + "]");
			break;
		case OperatorKind.MUL_VALUE:
			if(fwOut < (fw0 + fw1))
				throw new RuntimeException("mul output FP != sum(inputs FP)! [" + fw0 + " " + fw1 + " " + fwOut + "]");
			break;
		case OperatorKind.DIV_VALUE:
			throw new RuntimeException("TODO"); //TODO
		}
	}
	
	private static final void _assert(boolean b, String msg) {
		if(!b) 
			throw new RuntimeException(msg);
	}
	
	
	/**
	 * Convert type of all symbols to fixed-point types according to the fixed-point specification
	 * 
	 * @author aelmouss
	 */
	class ConvertSymbolTypesToACFixed extends SymbolSwitch<Boolean> {

		@Override
		public Boolean caseProcedureSet(ProcedureSet object) {
			//TODO avoid visiting procedures with no implementation available in the project
			return super.caseProcedureSet(object); 
			
//			for (Procedure proc: object.listProcedures()) {
//				doSwitch(proc);
//			}
//			return true;
		}

		@Override
		public Boolean caseProcedure(Procedure p) {	
			/* visit procedure symbol */
			doSwitch(p.getSymbol());
			
			/* update Function type */
			ProcedureSymbol psym = p.getSymbol();
			FunctionType ftype = (FunctionType) psym.getType();
			ftype.clearParameters();
			for(ParameterSymbol param : p.listParameters())
				ftype.addParameter(param.getType());	
			
			
			return super.caseProcedure(p);
		}

		@Override
		public Boolean caseProcedureSymbol(ProcedureSymbol object) {
			caseScopeContainer(object);
			return true;
		}
		
		@Override
		public Boolean caseScopeContainer(ScopeContainer scopeCont) {
 			Scope scope = scopeCont.getScope();
			if(scope!=null) {
				GecosUserTypeFactory.setScope(scope);
				for (Symbol sym : scope.getSymbols()) {
					handleSymbol(sym);
				}
			}
			return true;
		}

		private void handleSymbol(Symbol symbol) {
			Data data = findFixptSpec(symbol);
			if(data != null){
				FixedPointInformation fixedPointInfo = data.getFixedInformation();
				if(fixedPointInfo != null){
					Type acFixedType = BackEndUtils.createACFixedFromFixedPtInfo(fixedPointInfo);
					TypeAnalyzer typeAnalyser = new TypeAnalyzer(symbol.getType());
					symbol.setType(typeAnalyser.revertAllLevelsOn(acFixedType));
				}
			}
		}
	}
	
	/**
	 * Set the type of a DAGOpNode as described by the OUTPUT type of its fixSpec, if exists.
	 * Set the type of a DAGSimpleArrayAccess according to the array's type. 
	 * @author aelmouss
	 */
	public class DAGApplier extends DAGDefaultInstructionSwitch<Boolean> {
		
		private static final boolean VERBOSE_ANNOTATE = true;
		public static final String FIXTYPESPEC_ANNOTKEY = "_InFixedType"; //XXX
//		private DAGInstruction dag;
		
		public void apply(DAGInstruction dag) {
//			this.dag = dag;
			doSwitch(dag);
		}
		
		@Override
		public Boolean caseDAGInstruction(DAGInstruction dag) {
			for (DAGNode node : new ArrayList<>(dag.getNodes())) {
				doSwitch(node);
			}
			return true;
		}
		
		@Override
		public Boolean caseDAGOpNode(DAGOpNode node) {
			List<DAGNode> predecessors = node.getDataPredecessors();
			_assert(predecessors.size() == 2, "only OpNodes with 2 operands are supported for now! " + node);
			
			Operation operator = findFixptSpec(node);
			if(operator == null) { //this can be the case for SET nodes
				//System.err.println("operator not found: " + node);
				return false;
			}
			EList<FixedPointInformation> operands = operator.getOperands();
			if(operands == null) {
				//System.err.println(" *** Warning: ConvertDAGTypes: fixspec not found for op:" + node);
				return false;
			}
			
			if(CHECK_SPEC_LSBMODE) 
				checkFixedPtSpec_LSBMode(operator);
			if(VERBOSE_ANNOTATE) {// just for display purpose
				node.setAnnotation("in0<" + operands.get(0) + ">", null);
				node.setAnnotation("in1<" + operands.get(1) + ">", null);
			}
			
//			ACFixedType op0Type = BackEndUtils.createACFixedFromFixedPtInfo(operands.get(0));
//			ACFixedType op1Type = BackEndUtils.createACFixedFromFixedPtInfo(operands.get(1));
			ACFixedType outType = BackEndUtils.createACFixedFromFixedPtInfo(operands.get(2));
			
//			insertCastNode(predecessors.get(0), node, op0Type, 0);
//			insertCastNode(predecessors.get(1), node, op1Type, 1);
			
			/* set node (output) type */
			node.setType(outType);
			
			/* annotate with operands */
			FixedTypeAnnotation annotation = new FixedTypeAnnotation(operands.get(0), operands.get(1), operands.get(2));
			node.setAnnotation(FIXTYPESPEC_ANNOTKEY, annotation);
			
			return true;
		}
		
//		private DAGOpNode insertCastNode(DAGNode predNode, DAGNode succNode, Type castType, int predInputIndex) {
//			DAGOpNode castNode = GecosUserDagFactory.createDAGOPNode(dag, DAGOperator.CVT.getLiteral(), castType);
//			//connect predNode to castNode
//			castNode.getInputs().add(succNode.getInputs().get(predInputIndex));
//			//connect castNode to succNode
//			DAGOutPort srcPort = GecosUserDagFactory.createDAGOutputPort(castNode);
//			DAGInPort sinkPort = DagFactory.eINSTANCE.createDAGInPort();//GecosUserDagFactory.createDAGInputPort(succNode);
//			succNode.getInputs().add(predInputIndex, sinkPort);
//			dag.getEdges().add(GecosUserDagFactory.createDAGDataEdge(sinkPort, srcPort));
//			return castNode;
//		}

		@Override
		public Boolean caseDAGSimpleArrayNode(DAGSimpleArrayNode node) {
			Type symType = node.getSymbol().getType();
			_assert(symType instanceof ArrayType, "type not supprted yet: " + symType);
			
			TypeAnalyzer typeAnalyzer = new TypeAnalyzer(symType);
			if(typeAnalyzer.getBaseLevel().isFixedPoint()) {
				node.setType(typeAnalyzer.getBase()); //FIXME assuming nb index of node = nb dimensions of node.symbol array
			}
			return true;
		}

		@Override
		public Boolean caseDAGSymbolNode(DAGSymbolNode node) {
			TypeAnalyzer typeAnalyzer = new TypeAnalyzer(node.getSymbol().getType());
			node.setType(typeAnalyzer.getBaseLevel().skipStaticConst()); // assuming symbol is not an array nor pointer type.
			return true;
		}
	}
	
}
