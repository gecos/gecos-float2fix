package fr.irisa.cairn.idfix.backend.cpp;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.idfix.backend.AbstractFixedPointBackendModel;
import fr.irisa.cairn.idfix.backend.FixedPtSpecificationApplier;
import gecos.core.ProcedureSet;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.types.ACFixedType;
import gecos.types.Type;

public class ACFixedBackend extends AbstractFixedPointBackendModel {

	public ACFixedBackend(GecosProject proj) {
		super(proj);
	}
	
	/**
	 * NOTE: As long as {@link FixedPtSpecificationApplier} uses ACFixed data types to represent
	 * fixed-point types, this fixed-point backend does not need make any changes.
	 */
	@Override
	public void visit(ProcedureSet ps) {
		addHeaders(ps);
	}

	@Override
	protected Type createFixType(Symbol symbol, ACFixedType fixType) {
		return fixType;
	}

	@Override
	protected void addHeaders(ProcedureSet ps) {
		GecosUserAnnotationFactory.pragma(ps, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION + "#include <ac_fixed.h>");
	}

}
