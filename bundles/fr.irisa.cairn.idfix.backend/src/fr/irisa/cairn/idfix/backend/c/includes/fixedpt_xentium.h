#ifndef _ALMAAPI_FIXED_POINT_
#define _ALMAAPI_FIXED_POINT_

#include "../../../lib/iqmath-1.0.0/include/iqmathlib.h"

#define fix32_t						int32_t
#define alma_fix_pow_32(fw, a, x)	(fix_exp(alma_fix_mul_32(fw, x , fix_log(a, fw)),fw, fix_exp_coef))
#define alma_intToFix32(fw, x)		(x << fw)
#define alma_fix32ToInt(fw,x)		fix_to_int(x,fw)
#define alma_fix_sub_32(fw, a, b)	(a - b)
#define alma_fix_add_32(fw, a, b)	(a + b)
#define alma_fix_mul_32(fw, a, b)	((int32_t)(((int64_t) a * b) >> fw))
#define alma_fix_div_32(fw, a, b)	fix_div_32(a, b, fw, fix_div_lut)
#define alma_fix_cos_32(fw, x)		fix_cos(x, fw, fix_sincos_coeff)
#define alma_fix_sin_32(fw, x)		fix_sin(x, fw, fix_sincos_coeff)
#define alma_fix_floor_32(fw, x)	fix_floor(x, fw)
#define alma_fix_round_32(fw, x)	(alma_intToFix32(fw, fix_to_int(x, fw)))
#define alma_fix_ceil_32(fw, x)		fix_ceil(x, fw)
#define alma_fix_log_32(fw, x)		fix_log(x, fw)
#define alma_fix_log2_32(fw, x)		(fix_div_32(fix_log(x, fw),fix_log(alma_intToFix32(fw, 2), fw),fix_div_lut))
#define alma_fix_atan2_32(fw, y, x)	fix_atan2_32(y, x, fw, fix_div_lut, fix_atan_coeff)
#define alma_fix_exp_32(fw, x)		fix_exp(x, fw, fix_exp_coef)
#define alma_fix_sqrt_32(fw, x)		fix_sqrt(x, fw)
#define alma_fix_trunc_32(fw, x)	fix_int(x, fw)


const int32_t fix_exp_coef[] =
{
    0x00030115,
    0x001580B4,
    0x009DB44F,
    0x038D5B6D,
    0x0F5FDF83,
    0x2C5C85F9,
    0x40000000
};

const int32_t fix_div_lut[] =
{
    0x7D7E7F80,
    0x797A7B7C,
    0x75767778,
    0x72737475,
    0x6F707071,
    0x6C6D6D6E,
    0x696A6B6B,
    0x67676869,
    0x64656566,
    0x62626363,
    0x5F606061,
    0x5D5E5E5F,
    0x5B5C5C5D,
    0x595A5A5B,
    0x57585859,
    0x55565657,
    0x54545455,
    0x52525353,
    0x50515151,
    0x4F4F4F50,
    0x4D4E4E4E,
    0x4C4C4C4D,
    0x4A4B4B4B,
    0x49494A4A,
    0x48484849,
    0x46474747,
    0x45464646,
    0x44444545,
    0x43434344,
    0x42424243,
    0x41414142,
    0x40404041
};

const int32_t fix_sincos_coeff[] __attribute__ ((aligned(8))) =
{
    0xfffffffc,
    0xffffffcc,
    0x000002a1,
    0x00001e1e,
    0xfffecd2d,
    0xfff55162,
    0x00519af2,
    0x020783e1,
    0xf5aa218d,
    0xd885866d,
    0x6487ed51,
    0x7fffffff
};

const int32_t  fix_atan_coeff[] __attribute__ ((aligned(8))) =
{
    0x066f8183,
    0xf78b97ff,
    0x09d7d2ab,
    0xf45d1bf5,
    0x0e38e37e,
    0xedb6db6e,
    0x1999999a,
    0xd5555555,
    0x7fffffff
};

#endif //_ALMAAPI_FIXED_POINT_
