package fr.irisa.cairn.idfix.backend.c;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.idfix.backend.utils.BackEndUtils;
import gecos.core.ProcedureSet;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.CallInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.types.ACFixedType;
import gecos.types.FloatType;
import gecos.types.IntegerType;
import gecos.types.Type;

public class InlineCFixedPointBackend extends AbstractCFixedPointBackendModel {
	
	public static boolean ALWAYS_CAST = false;
	public static boolean USE_STDINT_TYPES = true;
	public static boolean COMMENT_SYMBOL_DECLARTIONS = true;

	
	public InlineCFixedPointBackend(GecosProject proj) {
		super(proj);
	}

	@Override
	protected Type createFixType(Symbol symbol, ACFixedType fixType) {
		Type type = BackEndUtils.createIntTypeFromACFixed(fixType, USE_STDINT_TYPES);
		if(COMMENT_SYMBOL_DECLARTIONS && symbol != null) {
			GecosUserAnnotationFactory.pragma(symbol, fixType.toString());
//			GecosUserAnnotationFactory.comment(symbol, fixType.toString());
		}
		return type;
	}
	
	@Override
	protected Instruction createIntToFixInstruction(Instruction i, IntegerType fromIntType, ACFixedType toFixType) {
		if(i instanceof IntInstruction) {
			Type type = createFixType(null, toFixType);
			return BackEndUtils.convertIntValueToFixedInst(type, toFixType, ((IntInstruction)i).getValue());
		}
		else {
			int bitwidth = (int)fromIntType.getSize();
			ACFixedType fromFixType = GecosUserTypeFactory.ACFIXED(bitwidth, bitwidth, fromIntType.getSigned(), toFixType.getQuantificationMode(), toFixType.getOverflowMode());
			return createFixtToFixInstruction(i, fromFixType , toFixType);
		}
	}
	
	@Override
	protected Instruction createFloatToFixInstruction(Instruction f, FloatType fromFloatType, ACFixedType toFixType) {
		if(f instanceof FloatInstruction) {
			Type type = createFixType(null, toFixType);
			return BackEndUtils.convertFloatValueToFixedInst(type, toFixType, ((FloatInstruction)f).getValue());
		}
		else{
			throw new RuntimeException("TODO: " + f);
//			((_fix_t(bw))((float_type)_fixOne_m(bw, fw) * (float_type)(value)))
		}
	}
	
	/** 
	 * FIXME: use ACfixed quantization mode information and apply the 
	 * correspondent quantization (Truncation or Rounding ...) accordingly
	 * Currently we always apply truncation using cast operation.
	 */
	@Override
	protected Instruction createFixtToFixInstruction(Instruction child, ACFixedType fromFixType, ACFixedType toFixType) {
		return createInlineFixToFixInstruction(child, fromFixType, toFixType);
	}

	public Instruction createInlineFixToFixInstruction(Instruction child, ACFixedType fromFixType, ACFixedType toFixType) {
		int toBW = (int)toFixType.getBitwidth();
//		int toIW = (int)toFixType.getInteger();
		int toFW = (int)toFixType.getFraction();
		
		int fromBW = (int)fromFixType.getBitwidth();
		int fromIW = (int)fromFixType.getInteger();
		int fromFW = (int)fromFixType.getFraction();
		boolean fromSigned = fromFixType.isSigned();
		
		_logger.debug("scaling " + child + " from: " + fromFixType + " to: " + toFixType);
		
		int scaleAmount = fromFW - toFW;
		if(scaleAmount > 0) {
			/** >>(fromFW - toFW) first, then cast if toBW != fromBW */
			_logger.debug("   shift");
			
			ACFixedType type = (ACFixedType) GecosUserTypeFactory.ACFIXED(fromBW, fromIW + scaleAmount, fromSigned);
			Instruction scaleInstruction = createScaleInstruction(child, createFixType(null, type), scaleAmount);//XXX check type
			if(ALWAYS_CAST || toBW != fromBW) {
				_logger.debug("      cast");
//				type = (ACFixedType) GecosUserTypeFactory.ACFIXED(toBW, toIW, fromSigned);
				Instruction castInstruction = createCastInstruction(scaleInstruction, createFixType(null, toFixType));
				return castInstruction;
			}
			return scaleInstruction;
		}
		else if(scaleAmount < 0) {
			if(toBW > fromBW) {
				/** cast first, then <<(toFW - fromFW) */
				_logger.debug("   cast(shift)");
				
				ACFixedType type = (ACFixedType) GecosUserTypeFactory.ACFIXED(toBW, fromIW + (toBW - fromBW), fromSigned);
				Instruction castInstruction = createCastInstruction(child, createFixType(null, type));
				
//				type = (ACFixedType) GecosUserTypeFactory.ACFIXED((int)type.getBitwidth(), (int)type.getInteger()+(fromFW-toFW), type.isSigned());
				Instruction scaleInstruction = createScaleInstruction(castInstruction, createFixType(null, toFixType), scaleAmount);//XXX check type: type must be equal to castType
				return scaleInstruction;
			}
			else {
				/** shift and cast (order of << and cast is not important) */
				_logger.debug("   shift");
				ACFixedType type = (ACFixedType) GecosUserTypeFactory.ACFIXED(fromBW, fromIW + scaleAmount, fromSigned);
				Instruction scaleInstruction = createScaleInstruction(child, createFixType(null, type), scaleAmount);//XXX check type
				
				if(ALWAYS_CAST || toBW != fromBW) {
					_logger.debug("      cast");
					
//					type = (ACFixedType) GecosUserTypeFactory.ACFIXED(toBW, toIW, type.isSigned());
					Instruction castInstruction = createCastInstruction(scaleInstruction, createFixType(null, toFixType));//XXX check type: type must be equal to castType
					return castInstruction;
				}
				return scaleInstruction;
			}
		}
		else {
			/** no shift is needed, cast if toBW != fromBW */
			if(ALWAYS_CAST || toBW != fromBW) {
				_logger.debug("   cast");
//				ACFixedType type = (ACFixedType) GecosUserTypeFactory.ACFIXED(toBW, toIW, fromSigned);//XXX check
				Instruction castInstruction = createCastInstruction(child, createFixType(null, toFixType));//XXX check type: type must be equal to castType
				return castInstruction;
			}
			return child;
		}
	}
	
	@Override
	protected Instruction createFixedFunctionCall(CallInstruction c, ACFixedType fixType) {
//		throw new RuntimeException("not implemented yet! " + c);
		return scale(c, fixType);
	}
	
	/**
	 * @param child
	 * @param type
	 * @param scaleAmount positive for >>, negative for <<
	 * @return (child >> scaleAmount) if scaleAmount > 0, (child << -scaleAmount) otherwise
	 */
	protected static Instruction createScaleInstruction(Instruction child, Type type, long scaleAmount) {
		Instruction scaleInstruction;
		if(scaleAmount > 0)
			scaleInstruction = GecosUserInstructionFactory.shr(child, scaleAmount);
		else if (scaleAmount < 0)
			scaleInstruction = GecosUserInstructionFactory.shl(child, -scaleAmount);
		else 
			scaleInstruction = child;
		
		return scaleInstruction;
	}
	
	/**
	 * @param child
	 * @param castType
	 * @return
	 */
	public static Instruction createCastInstruction(Instruction child, Type castType) {
		return GecosUserInstructionFactory.cast(castType, child);
	}

	@Override
	protected void addHeaders(ProcedureSet ps) {
		if(USE_STDINT_TYPES)
			GecosUserAnnotationFactory.pragma(ps, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION + "#include <stdint.h>");
	}

}
