package fr.irisa.cairn.idfix.backend.utils;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.apache.logging.log4j.Logger;

import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.idfix.model.factory.IDFixUserFixedPointSpecificationFactory;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.OVERFLOW_TYPE;
import fr.irisa.cairn.idfix.model.fixedpointspecification.QUANTIFICATION_TYPE;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.logging.IDFixLogger;
import gecos.core.ITypedElement;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.types.ACFixedType;
import gecos.types.ArrayType;
import gecos.types.BaseType;
import gecos.types.OverflowMode;
import gecos.types.PtrType;
import gecos.types.QuantificationMode;
import gecos.types.SignModifiers;
import gecos.types.SimdType;
import gecos.types.Type;

public class BackEndUtils {

	protected static final Logger _logger = IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_BACKEND);
	
	
	/**
	 * @param wf fractional bitwidth
	 * @param floatVal floating-point value
	 * @return fixed-point value with {@code wf} bits in fractional part
	 */
	static public long convertFloatValue2Fix(int wf, double floatVal, QuantificationMode qMode) {
		double one = Math.pow(2, wf);
		long fix;
		switch (qMode) {
		case AC_RND:
			fix = Math.round(floatVal * one); // untested
			break;
		case AC_TRN_ZERO:
			fix = (long)(floatVal * one); // untested
			break;
		case AC_TRN:
			fix = (long) Math.floor(floatVal * one); // untested
			break;
		default:
			fix = (long) Math.floor(floatVal * one); // untested
			break;
		}
		_logger.debug("**Convert float value: " + floatVal + " into fixed value (wf=" + wf + "): " + fix);
		return fix;
	}
	
	/**
	 * @param wf fractional bitwidth
	 * @param intVal integer value
	 * @return fixed-point value with {@code wf} bits in fractional part
	 */
	static public long convertIntValueToFixedValue(int wf, long intVal) {
		long fix = wf >= 0? (intVal << wf) : (intVal >> wf);
		_logger.debug("**Convert int value: " + intVal + " into fixed value (wf=" + wf + "): " + fix);
		return fix;
	}
	
	static public IntInstruction convertFloatValueToFixedInst(Type targetType, ACFixedType oldFixType, double floatVal) {
		long fixedValue = convertFloatValue2Fix((int)oldFixType.getFraction(), floatVal, oldFixType.getQuantificationMode());
		IntInstruction inst = GecosUserInstructionFactory.Int(fixedValue);
		inst.setType(targetType);
		return inst;
	}
	
	static public IntInstruction convertIntValueToFixedInst(Type targetType, ACFixedType oldType, long intVal) {
		long fixedValue = convertIntValueToFixedValue((int)oldType.getFraction(), intVal);
		IntInstruction inst = GecosUserInstructionFactory.Int(fixedValue);
		inst.setType(targetType);
		return inst;
	}
	
	static public Type createIntTypeFromACFixed(ACFixedType ac, boolean stdint) {
//		GecosUserTypeFactory.setScope(ac.getContainingScope());
		
		boolean saved = GecosUserTypeFactory.ALWAYS_RETURN_NEW_TYPE;
		GecosUserTypeFactory.ALWAYS_RETURN_NEW_TYPE = true;
		
		long size = ac.getBitwidth();
		if(size < 0 || size%8 != 0 || size > 64)
			System.err.println("[BackEndUtils]: *** WARNING: size may not be supported! " + size);
		
		Type type;
		if(stdint) {
			StringBuffer name = new StringBuffer();
			name.append(ac.isSigned()? "int" : "uint").append(size).append("_t");
			BaseType aliasBase = GecosUserTypeFactory.INT();
			aliasBase.setSize(size);
			type = GecosUserTypeFactory.ALIAS(aliasBase, name.toString());
			GecosUserAnnotationFactory.pragma(type, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		}
		else {
			if(ac.isSigned())
				type = GecosUserTypeFactory.INT(SignModifiers.SIGNED);
			else
				type = GecosUserTypeFactory.INT(SignModifiers.UNSIGNED);
			((BaseType)type).setSize(size);
		}
		
		GecosUserTypeFactory.ALWAYS_RETURN_NEW_TYPE = saved;
		return type;
	}
	
	static public ACFixedType createACFixedFromFixedPtInfo(FixedPointInformation info) {
		QuantificationMode qMode = resolveQuantification(info.getQuantification());
		OverflowMode oMode = resolveOverflow(info.getOverflow());
		boolean old = GecosUserTypeFactory.ALWAYS_RETURN_NEW_TYPE;
		GecosUserTypeFactory.ALWAYS_RETURN_NEW_TYPE = true;
		ACFixedType type = GecosUserTypeFactory.ACFIXED(info.getBitWidth(), info.getIntegerWidth(), info.isSigned(), qMode, oMode);
		GecosUserTypeFactory.ALWAYS_RETURN_NEW_TYPE = old;
		return type;
	}
	
	static public FixedPointInformation createFixedPtInfoFromACFixed(ACFixedType act) {
		FixedPointInformation fxinfo = IDFixUserFixedPointSpecificationFactory.FIXEDINFORMATION((int)act.getInteger(), (int)act.getBitwidth(), act.getSigned());
		//TODO quantization/overflow mode
		
		return fxinfo;
	}
	
	/**
	 * TODO parse optional trunc and sat mode
	 * 
	 * @param str ac_fixed< w, i, signed? [,trunc_mode] [,sat_mode] >
	 * @return
	 */
	static public ACFixedType createACFixedFromString(String str) {
		String[] splits = str.split("<");
		if(splits.length < 2)
			return null;
		
		splits = splits[1].split(",");
		if(splits.length < 3)
			return null;
		
		int width = Integer.parseInt(splits[0]);
		int integer = Integer.parseInt(splits[1]);
		boolean signed = Boolean.parseBoolean(splits[2]);
		
		QuantificationMode qMode = QuantificationMode.AC_TRN;
		OverflowMode oMode = OverflowMode.AC_SAT;
		
		if(splits.length > 3) {
			//TODO: parse optional trunc and sat mode ..
		}
		
		return GecosUserTypeFactory.ACFIXED(width, integer, signed, qMode, oMode);
	}
	
	static public QuantificationMode resolveQuantification(QUANTIFICATION_TYPE quantification){
		switch (quantification) {
		case TRUNCATION:
			return QuantificationMode.AC_TRN;
			
		case ROUNDING:
			return QuantificationMode.AC_RND;
		default:
			throw new RuntimeException("Quantification kind " + quantification + "not managed yet in the idfix backend");
		}
	}
	
	static public OverflowMode resolveOverflow(OVERFLOW_TYPE overflow){
		switch (overflow) {
		case SATURATION:
			return OverflowMode.AC_SAT;
		case WRAP:
			return OverflowMode.AC_WRAP;
			
		default:
			throw new RuntimeException("Overflow kind " + overflow + "not managed yet in the idfix backend");
		}
	}

	/**
	 * @deprecated it is not needed when using TypeAnalyzer.revertOn..
	 */
	static public Type copyType(Type type) {
		Type copy;
		if(((type instanceof ArrayType) && !(type instanceof BaseType))) {
			ArrayType arrayType = (ArrayType)type;
			List<Instruction> sizes = new ArrayList<Instruction>(); 
			for(Instruction i : arrayType.getSizes()) //copy size intructions
				sizes.add(i.copy());
			boolean old = GecosUserTypeFactory.ALWAYS_RETURN_NEW_TYPE;
			GecosUserTypeFactory.ALWAYS_RETURN_NEW_TYPE = true;
			copy = GecosUserTypeFactory.ARRAY(arrayType.getInnermostBase(), arrayType.getNbDims(), sizes);
			GecosUserTypeFactory.ALWAYS_RETURN_NEW_TYPE = old;
		}
		else if(type instanceof PtrType) {
			PtrType ptrType = (PtrType)type;
			boolean old = GecosUserTypeFactory.ALWAYS_RETURN_NEW_TYPE;
			GecosUserTypeFactory.ALWAYS_RETURN_NEW_TYPE = true;
			copy = GecosUserTypeFactory.PTR(ptrType.getInnermostBase(), ptrType.getNbDims());
			GecosUserTypeFactory.ALWAYS_RETURN_NEW_TYPE = old;
		}
		else {
			copy = type.copy();
		}
		return copy;
	}
	
	/**
	 * @deprecated Use {@link TypeAnalyzer} instead
	 */
	static public Entry<Type, Type> processType(Type oldType) {
		Type prev = null;
		Type oldBaseType = oldType;
		while (oldBaseType != null && (oldBaseType instanceof ArrayType) && !(oldBaseType instanceof BaseType)) {
			prev = oldBaseType;
			if(oldBaseType instanceof ArrayType)
				oldBaseType = ((ArrayType)oldBaseType).getBase();
			else if(oldBaseType instanceof PtrType) //XXX useless
				oldBaseType = ((PtrType)oldBaseType).getBase();
			else
				throw(new RuntimeException("not supported !"));
		}
		if(oldBaseType instanceof SimdType) {
			prev = oldBaseType;
			oldBaseType = ((SimdType) oldBaseType).getSubwordType();
		}
		
		return new SimpleEntry<Type,Type>(oldBaseType, prev);
	}
	
	/**
	 * @deprecated Use Type Analyzer instead
	 */
	static public void updateType(Type prev, Type newBaseType, ITypedElement typedElem) {
		if (prev == null) // sym is not an array
			typedElem.setType(newBaseType);
		else {
			if(prev instanceof ArrayType)
				((ArrayType)prev).setBase(newBaseType);
			else if(prev instanceof SimdType)
				((SimdType) prev).setSubwordType(newBaseType);
			else if(prev instanceof PtrType)
				((PtrType)prev).setBase(newBaseType);
			else 
				throw(new RuntimeException("not supported!"));
		}
	}
}
