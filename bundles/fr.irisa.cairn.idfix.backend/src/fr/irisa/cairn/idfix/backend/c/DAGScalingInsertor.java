package fr.irisa.cairn.idfix.backend.c;

import java.util.List;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.factory.GecosUserDagFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.tools.switches.DAGDefaultInstructionSwitch;
import fr.irisa.cairn.idfix.backend.FixedPtSpecificationApplier;
import fr.irisa.cairn.idfix.backend.utils.BackEndUtils;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointInformation;
import gecos.dag.DAGFloatImmNode;
import gecos.dag.DAGInPort;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGIntImmNode;
import gecos.dag.DAGNode;
import gecos.dag.DAGOpNode;
import gecos.dag.DAGOperator;
import gecos.dag.DAGOutPort;
import gecos.dag.DagFactory;
import gecos.instrs.ArithmeticOperator;
import gecos.types.ACFixedType;
import gecos.types.BaseType;
import gecos.types.FloatType;
import gecos.types.IntegerType;
import gecos.types.QuantificationMode;
import gecos.types.Type;


/**
 * @author aelmouss
 */
public class DAGScalingInsertor extends DAGDefaultInstructionSwitch<Boolean> {

	public static final String SCALING_ANNOTATION_KEY = "DAGScalingInsertor:SCALING_NODE";
	
	protected static final boolean EXPORT_DAG = false;
	protected boolean CONVERT_TO_INTEGER_TYPES = false;
	protected DAGInstruction dag;

	public DAGScalingInsertor(DAGInstruction DAG) {
		this.dag = DAG;
	}

	public void compute() {
		doSwitch(dag);
	}
	
	@Override
	public Boolean caseDAGInstruction(DAGInstruction dag) {
		/* visit nodes in topological order */
		List<DAGNode> topologicalOrderedNodes = fr.irisa.cairn.gecos.model.transforms.dag.DAGUtils.topologicalSort(dag);
		for (DAGNode node : topologicalOrderedNodes) {
			doSwitch(node);
		}
		if(EXPORT_DAG) 
			fr.irisa.cairn.gecos.model.transforms.dag.DAGUtils.exportDAG2Dot(dag, fr.irisa.cairn.gecos.model.transforms.dag.DAGUtils.nameDag(dag, "", "_with_Scaling_fix.dot"));
		return true;
	}

	@Override
	public Boolean caseDAGOpNode(DAGOpNode node) {
		if(node.getOpcode().equals(DAGOperator.SET.getLiteral())) {
			caseSetNode(node);
		}
		else if(node.getOpcode().equals(DAGOperator.CVT.getLiteral())) {
			//TODO
			//throw new RuntimeException("Not implemented yet." + node);
		}
		else if(node.getOpcode().equals(DAGOperator.RET.getLiteral())) {
			//TODO
			throw new RuntimeException(new Exception().getStackTrace()[0] + "Not implemented yet." + node);
		}
		else { // op nodes
			caseArtihNode(node);
		}
		return true;
	}
	
	protected void caseArtihNode(DAGOpNode node) {
		FixedTypeAnnotation fta = getFixedTypeAnnotation(node);
		if(fta == null) {
//			System.err.println(new Exception().getStackTrace()[0] + "** node was not annotated: " + node);
			return;
		}
		
		/* Handle input operands */
		EList<DAGNode> preds = node.getDataPredecessors();
		for(int i=0; i<2; i++) {
			DAGNode inNode = preds.get(i);
			scaleInput(node, inNode, i, fta.getInput(i));
		}
		
		castOutput(node, node.getType());
	}

	protected void caseSetNode(DAGOpNode node) {
		FixedPointInformation newType;
		FixedTypeAnnotation fta = getFixedTypeAnnotation(node);
		if(fta != null) {
			newType = fta.getOutput(); //check: this must be the type of dstNode
		} else {
//				System.out.println("** node was not annotated: " + node);
//				return;
			Type dstNodeType = node.getDataPredecessors().get(0).getType();
			if(dstNodeType instanceof ACFixedType) {
				newType = BackEndUtils.createFixedPtInfoFromACFixed((ACFixedType)dstNodeType);
			}
			else {
				System.out.println(new Exception().getStackTrace()[0] + "** node was not annotated: " + node);
				return;
			}
		}
		DAGNode srcNode = node.getDataPredecessors().get(1);
		
//			Type in_type = eliminate_static_const(inputNode.getType());
//			if(!(in_type instanceof ACFixedType))
//				throw new RuntimeException("not supported type: " + in_type + " of node" + node);
		
		scaleInput(node, srcNode, 1, newType/*, in_type*/);
	}
	
	private FixedTypeAnnotation getFixedTypeAnnotation(DAGNode node) {
		return (FixedTypeAnnotation) node.getAnnotation(FixedPtSpecificationApplier.DAGApplier.FIXTYPESPEC_ANNOTKEY);
	}
	
	/**
	 * Scale {@code inputOperanNode} from its type to the fixed-point type specified by {@code inputFixedPtInfo}.
	 * @param operationNode
	 * @param inputOperandNode
	 * @param inputIndex
	 * @param toFixedType
	 */
	private void scaleInput(DAGOpNode operationNode, DAGNode inputOperandNode, int inputIndex, FixedPointInformation toFixedType) {
		/* bypass convert nodes */ //FIXME
		if(fr.irisa.cairn.gecos.model.transforms.dag.DAGUtils.isCVT(inputOperandNode)) {
			//TODO remove cast node
			inputOperandNode = inputOperandNode.getDataPredecessors().get(0);
		}
		
		//!! no need to handle static/const types since they were skipped for DAGNodes (in FixedPtSpecificationApplier) !!
		TypeAnalyzer typeAnalyzer = new TypeAnalyzer(inputOperandNode.getType());
		Type inputNodeType = typeAnalyzer.getBase();
		
		/* case of scalar Nodes */
		if(inputOperandNode instanceof DAGFloatImmNode) {
			scaleFloatImmNode(operationNode, (DAGFloatImmNode)inputOperandNode, inputIndex, toFixedType);
			return;
		}
		else if(inputOperandNode instanceof DAGIntImmNode) {
			scaleIntImmNode(operationNode, (DAGIntImmNode)inputOperandNode, inputIndex, toFixedType);
			return;
		}
		
		/* case of non-scalar Nodes */
		if(inputNodeType instanceof ACFixedType) {
			scaleFixedNode(operationNode, inputOperandNode, inputIndex, (ACFixedType)inputNodeType, toFixedType);
			return;
		}
		else if(inputNodeType instanceof BaseType && ((BaseType) inputNodeType).asFloat() != null) {
			scaleFloatNode(operationNode, inputOperandNode, inputIndex, (FloatType)inputNodeType, toFixedType);
			return;
		}
		else if(inputNodeType instanceof BaseType && ((BaseType) inputNodeType).asInt() != null) {
			scaleIntNode(operationNode, inputOperandNode, inputIndex, (IntegerType)inputNodeType, toFixedType);
			return;
		}

//		throw new RuntimeException("Scaling not yet implemented for: " + inputNode + " -> " + node + "(" + inputNodeType + " -> " + inputFixedPtinfo + ")");
		System.err.println(new Exception().getStackTrace()[0] + "Scaling not yet implemented for: " + inputOperandNode + " -> " + operationNode + "(" + inputNodeType + " -> " + toFixedType + ")");
	}

	protected void scaleFloatImmNode(DAGOpNode operationNode, DAGFloatImmNode inputOperandNode, int inputIndex, FixedPointInformation toFixedType) {
		if(inputOperandNode.getDataOutputs().size() > 1)
			throw new RuntimeException("nodes with multiple successors are not handled yet");
		
		/* convert floating value to fixed value */
		int fw = toFixedType.getFractionalWidth();
		long fixVal = BackEndUtils.convertFloatValue2Fix(fw, inputOperandNode.getFloatValue(), QuantificationMode.AC_TRN);
		
		/* replace FloatNode with intNode */
		//this is correct if the node has only 1 data successor 
		DAGNode fixNode = GecosUserDagFactory.createDAGIntImmediateNode(dag, GecosUserInstructionFactory.Int(fixVal)); //XXX type should be acfixed not int
		fixNode.getOutputs().addAll(inputOperandNode.getOutputs());
		dag.getNodes().remove(inputOperandNode);
		
		/* insert explicit cast node */
		//TODO unchecked
		Type castType = BackEndUtils.createACFixedFromFixedPtInfo(toFixedType);
		insertCastBetween(fixNode, operationNode, castType, inputIndex);
	}
	
	protected void scaleIntImmNode(DAGOpNode operationNode, DAGIntImmNode inputOperandNode, int inputIndex, FixedPointInformation toFixedType) {
		if(inputOperandNode.getDataOutputs().size() > 1)
			throw new RuntimeException("nodes with multiple successors are not handled yet");
		
		/* convert value from <wd, 0> to fixspec as described by dstType */
		//XXX this is correct if the node has only 1 data successor
		int wf = toFixedType.getFractionalWidth();
		long fixVal = BackEndUtils.convertIntValueToFixedValue(wf, ((DAGIntImmNode) inputOperandNode).getIntValue()); //XXX check the word-length int or long, signed or not ...
		((DAGIntImmNode) inputOperandNode).getValue().setValue(fixVal);
		((DAGIntImmNode) inputOperandNode).setIntValue(fixVal); //XXX fix DAGIntImmNode: sync intValue and intInstruction.intValue
		
		/* insert explicit cast node */
		//TODO unchecked
		Type castType = BackEndUtils.createACFixedFromFixedPtInfo(toFixedType);
		insertCastBetween(inputOperandNode, operationNode, castType, inputIndex);
	}
	
	protected void scaleFloatNode(DAGNode operationNode, DAGNode inputOperandNode, int inputIndex, FloatType fromFloatType, FixedPointInformation toFixedType) {
		int fw = toFixedType.getFractionalWidth();
		if(fw < 0) 
			throw new RuntimeException("not yet implemented");
		double one = Math.pow(2, fw);
		DAGFloatImmNode oneNode = GecosUserDagFactory.createDAGFloatImmediateNode(dag, GecosUserInstructionFactory.Float(one));
		DAGOpNode mulNode = GecosUserDagFactory.createDAGOPNode(dag, ArithmeticOperator.MUL.getLiteral(), fromFloatType);
		ACFixedType castType = BackEndUtils.createACFixedFromFixedPtInfo(toFixedType);
		castOutput(mulNode, castType);
		dag.connectNodes(inputOperandNode, mulNode);
		dag.connectNodes(oneNode, mulNode);
	}
	
	protected void scaleIntNode(DAGNode operationNode, DAGNode inputOperandNode, int inputIndex, IntegerType fromIntType, FixedPointInformation toFixedType) {
		ACFixedType fromFixedType = GecosUserTypeFactory.ACFIXED((int)fromIntType.getSize(), (int)fromIntType.getSize(), fromIntType.getSigned(), null, null); //XXX
		scaleFixedNode(operationNode, inputOperandNode, inputIndex, fromFixedType , toFixedType);
	}

	protected void scaleFixedNode(DAGNode operationNode, DAGNode inputOperandNode, int inputIndex, ACFixedType fromFixedType, FixedPointInformation toFixedType) {
		/** 
		 * FIXME: use ACfixed quantization mode information and apply the 
		 * correspondent quantization (Truncation or Rounding ...) accordingly
		 * * Currently we always apply truncation using cast operation.
		 * TODO: modify insertCastNode() in order to handle rounding. change its name to insertQuantizationNode()
		 */
		int dstType_bw = toFixedType.getBitWidth();
		int dstType_iw = toFixedType.getIntegerWidth();
		int dstType_fw = toFixedType.getFractionalWidth();
		
		int srcType_bw = (int) fromFixedType.getBitwidth();
		int srcType_iw = (int) fromFixedType.getInteger();
		int srcType_fw = srcType_bw - srcType_iw;
		boolean srcType_signed = fromFixedType.isSigned();
		
//		debug("scale " + operationNode + " in" + inputOrder + " from " + srcType_fw + " to " + dstType_fw);
		
		if(dstType_fw < srcType_fw) {
			/* >>(srcFw - dstFw) first, then cast if needed */
			ACFixedType type = (ACFixedType) GecosUserTypeFactory.ACFIXED(srcType_bw, srcType_iw+(srcType_fw-dstType_fw), srcType_signed);//XXX check
			DAGOpNode shiftNode = insertShiftNode("shr", srcType_fw-dstType_fw, type, inputOperandNode, operationNode, inputIndex);
			if(dstType_bw != srcType_bw) {
				//cast shiftNode to dstType.width
				type = (ACFixedType) GecosUserTypeFactory.ACFIXED(dstType_bw, dstType_iw, srcType_signed);//XXX check
				insertCastBetween(shiftNode, operationNode, type, inputIndex);
			}
		}
		else if(dstType_fw > srcType_fw) {
			if(dstType_bw > srcType_bw) {
				/* cast first, then <<(dstFw - srcFw) */
				ACFixedType type = (ACFixedType) GecosUserTypeFactory.ACFIXED(dstType_bw, srcType_iw+(dstType_bw-srcType_bw), srcType_signed);//XXX check
				DAGOpNode castNode = insertCastBetween(inputOperandNode, operationNode, type, inputIndex);
				type = (ACFixedType) GecosUserTypeFactory.ACFIXED((int)type.getBitwidth(), (int)type.getInteger()+(srcType_fw-dstType_fw), type.isSigned());//XXX check
				insertShiftNode("shl", dstType_fw-srcType_fw, type, castNode, operationNode, inputIndex);
			}
			else {
				/* shift and cast (order of << and cast is not important) */
				ACFixedType type = (ACFixedType) GecosUserTypeFactory.ACFIXED(srcType_bw, srcType_iw+(srcType_fw-dstType_fw), srcType_signed);//XXX check
				DAGOpNode shiftNode = insertShiftNode("shl", dstType_fw-srcType_fw, type, inputOperandNode, operationNode, inputIndex);
				if(dstType_bw != srcType_bw) {
					type = (ACFixedType) GecosUserTypeFactory.ACFIXED(dstType_bw, dstType_iw, type.isSigned());//XXX check
					insertCastBetween(shiftNode, operationNode, type, inputIndex);
				}
			}
		}
		else {
			/* no shift is needed, cast if necessary */
			if(dstType_bw != srcType_bw) {
				ACFixedType type = (ACFixedType) GecosUserTypeFactory.ACFIXED(dstType_bw, dstType_iw, srcType_signed);//XXX check
				insertCastBetween(inputOperandNode, operationNode, type, inputIndex);
			}
		}
	}
	
	/**
	 * Add a Cast node after {@code node} and move all its outputs to the created cast node.
	 * @param node
	 * @param castType
	 */
	protected void castOutput(DAGOpNode node, Type castType) {
		/* insert explicit cast on output */
		DAGOpNode castNode = GecosUserDagFactory.createDAGOPNode(dag, DAGOperator.CVT.getLiteral(), castType);
		castNode.setAnnotation(SCALING_ANNOTATION_KEY, null);
		castNode.getOutputs().addAll(node.getOutputs());
		dag.connectNodes(node, castNode);
	}
	
	protected DAGOpNode insertCastBetween(DAGNode predNode, DAGNode succNode, Type castType, int predInputIndex) {
		DAGOpNode castNode = GecosUserDagFactory.createDAGOPNode(dag, DAGOperator.CVT.getLiteral(), castType);
		castNode.setAnnotation(SCALING_ANNOTATION_KEY, null);
		//connect predNode to castNode
		castNode.getInputs().add(succNode.getInputs().get(predInputIndex));
		
		//connect castNode to succNode
		DAGOutPort srcPort = GecosUserDagFactory.createDAGOutputPort(castNode);
		DAGInPort sinkPort = DagFactory.eINSTANCE.createDAGInPort();//GecosUserDagFactory.createDAGInputPort(succNode);
		succNode.getInputs().add(predInputIndex, sinkPort);
		dag.getEdges().add(GecosUserDagFactory.createDAGDataEdge(sinkPort, srcPort));
		
		return castNode;
	}

	protected DAGOpNode insertShiftNode(String direction, long shiftNBits, Type type, DAGNode predNode, DAGNode succNode, int order) {
//		System.out.println("insert shift between: " + predNode + " and " + succNode);
		DAGOpNode shiftNode = GecosUserDagFactory.createDAGOPNode(dag, direction, type);
		shiftNode.setAnnotation(SCALING_ANNOTATION_KEY, null);
		
		//connect predNode to shiftNode
		shiftNode.getInputs().add(succNode.getInputs().get(order));
		
		//connect shitNbits node to shiftNode
		DAGIntImmNode n = GecosUserDagFactory.createDAGIntImmediateNode(dag, GecosUserInstructionFactory.Int(shiftNBits));
		dag.connectNodes(n, shiftNode);
		
		//connect shiftNode to succNode
		DAGOutPort srcPort = GecosUserDagFactory.createDAGOutputPort(shiftNode);
		DAGInPort sinkPort = DagFactory.eINSTANCE.createDAGInPort();//createDAGInputPort(succNode);
		succNode.getInputs().add(order, sinkPort);
		dag.getEdges().add(GecosUserDagFactory.createDAGDataEdge(sinkPort, srcPort));
		
		return shiftNode;
	}

	public static boolean isScalingNode(DAGNode node) {
		return node.getAnnotations().containsKey(SCALING_ANNOTATION_KEY);
	}
	
}
