package fr.irisa.cairn.idfix.backend.c;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;

import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.idfix.backend.FixedPtSpecificationApplier;
import fr.irisa.cairn.idfix.backend.utils.BackEndUtils;
import gecos.core.ParameterSymbol;
import gecos.core.ProcedureSet;
import gecos.core.Symbol;
import gecos.gecosproject.GecosProject;
import gecos.instrs.CallInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.types.ACFixedType;
import gecos.types.AliasType;
import gecos.types.FloatType;
import gecos.types.IntegerType;
import gecos.types.Type;

public class CFunctionsFixedPointBackend extends AbstractCFixedPointBackendModel {
	
	/* 
	 * TODO: keep only mode W_F
	 * true: use width, integer part to represent fixed-point
	 * false: use width, fractional part
	 */
	protected static boolean USE_W_I_FORMAT = false;	
	protected static boolean USE_MACROS = false;
	protected static boolean INLINE_SCALAR_CONVERSIONS = true;
	protected static boolean INLINE_TYPE_CONVERSIONS = true;
	protected static boolean PRAGMA_SYMBOL_DECLARTIONS = true;
	protected static String PREFIX = "alma"; //XXX

	
	public CFunctionsFixedPointBackend(GecosProject proj) {
		super(proj);
	}

	@Override
	protected Type createFixType(Symbol symbol, ACFixedType fixType) {
		if(PRAGMA_SYMBOL_DECLARTIONS && symbol != null) {
			GecosUserAnnotationFactory.pragma(symbol, fixType.toString());
		}
		
		AliasType type = createFixType(fixType.getBitwidth(), fixType.getFraction(), fixType.isSigned());
		
		return type;
	}

	public static AliasType createFixType(long bw, long fw, boolean signed) {
		if(bw < 0 || bw%8 != 0 || bw > 64) {
			_logger.warn("[createFixType] size may not be supported! " + bw);
		}
		
		StringBuffer name = new StringBuffer();
		long format = USE_W_I_FORMAT ?  bw-fw : fw;
		name.append(signed? "" : "u").append("fix").append(bw).append("_t(").append(format).append(")");
		AliasType type = GecosUserTypeFactory.ALIAS(GecosUserTypeFactory.INT(), name.toString());
		GecosUserAnnotationFactory.pragma(type, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		return type;
	}
	
	@Override
	protected Instruction createIntToFixInstruction(Instruction i, IntegerType fromIntType, ACFixedType fixType) {
		if(INLINE_SCALAR_CONVERSIONS && (i instanceof IntInstruction)) {
			Type type = BackEndUtils.createIntTypeFromACFixed(fixType, false); //XXX createFixType(null, fixType);
			return BackEndUtils.convertIntValueToFixedInst(type, fixType, ((IntInstruction)i).getValue());
		}
		
		if(INLINE_TYPE_CONVERSIONS) {
			return createInlineFixToFix(fixType.getBitwidth(), fixType.getFraction(), fixType.isSigned(), 0, i);
		}
		
		String name = PREFIX + "intToFix" + fixType.getBitwidth() + (fixType.isSigned()?"":"u");
		Symbol address = GecosUserCoreFactory.procSymbol(name, fixType, new ArrayList<ParameterSymbol>()); //FIXME function parameter types
		long format = USE_W_I_FORMAT ?  fixType.getInteger() : fixType.getFraction();
		Instruction intToFix = GecosUserInstructionFactory.call(address, GecosUserInstructionFactory.Int(format), i);
		return intToFix;
	}
	
	//FIXME
	protected Instruction createFixToIntInstruction(Instruction i, ACFixedType fixType, IntegerType intType) {
//		if(INLINE_SCALAR_CONVERSIONS && (i instanceof IntInstruction)) {
//			Type type = BackEndUtils.createIntTypeFromACFixed(fixType, null, false); //XXX createFixType(null, fixType);
//			return BackEndUtils.convertInt2FixInst(type, fixType, ((IntInstruction)i).getValue());
//		}
		
		if(INLINE_TYPE_CONVERSIONS) {
			return createInlineFixToInt(intType, intType.getSize(), intType.getSigned(), fixType.getFraction(), i);
		}
		
		String name = PREFIX + (intType.getSigned()? "" : "u") + "fix" + fixType.getBitwidth() + "ToInt"; // + intType.isSigned() +"Int";
		Symbol address = GecosUserCoreFactory.procSymbol(name, intType, new ArrayList<ParameterSymbol>()); //FIXME function parameter types
		long format = USE_W_I_FORMAT ?  fixType.getInteger() : fixType.getFraction();
		CallInstruction fixFunc = GecosUserInstructionFactory.call(address, GecosUserInstructionFactory.Int(format), i);
		return fixFunc;
		
//		String name = PREFIX + "fixToInt" + fixType.getBitwidth() + (fixType.isSigned()?"":"u");
//		Symbol address = GecosUserCoreFactory.procSymbol(name, fixType, new ArrayList<ParameterSymbol>()); //FIXME function parameter types
//		long format = USE_W_I_FORMAT ?  fixType.getInteger() : fixType.getFraction();
//		Instruction intToFix = GecosUserInstructionFactory.call(address, GecosUserInstructionFactory.Int(format), i);
//		return intToFix;
	}
	
	
	@Override
	protected Instruction createFloatToFixInstruction(Instruction f, FloatType fromFloatType, ACFixedType fixType) {
		if(INLINE_SCALAR_CONVERSIONS && (f instanceof FloatInstruction)) {
			Type type = BackEndUtils.createIntTypeFromACFixed(fixType, false); //XXX createFixType(null, fixType);
			return BackEndUtils.convertFloatValueToFixedInst(type, fixType, ((FloatInstruction)f).getValue());
		}
		
		if(INLINE_TYPE_CONVERSIONS) {
			return createInlineFloatToFix(fixType.getBitwidth(), fixType.getFraction(), fixType.isSigned(), f);
		}
		
		String name = PREFIX + "floatToFix" + fixType.getBitwidth() + (fixType.isSigned()?"":"u");
		Symbol address = GecosUserCoreFactory.procSymbol(name, fixType, new ArrayList<ParameterSymbol>()); //FIXME function parameter types
		long format = USE_W_I_FORMAT ?  fixType.getInteger() : fixType.getFraction();
		Instruction floatToFix = GecosUserInstructionFactory.call(address, GecosUserInstructionFactory.Int(format), f);
		return floatToFix;
	}
	
	@Override
	protected Instruction createFixtToFixInstruction(Instruction i, ACFixedType fromFixType, ACFixedType toFixType) {
		if(fromFixType.isEqual(toFixType))
			return i;
		
		if(INLINE_SCALAR_CONVERSIONS && (i instanceof IntInstruction)) {
			//TODO
		}
		
		if(INLINE_TYPE_CONVERSIONS) {
			return createInlineFixToFix(toFixType.getBitwidth(), toFixType.getFraction(), toFixType.isSigned(), fromFixType.getFraction(), i);
		}
		
		String name = PREFIX + "fixToFix" + toFixType.getBitwidth() + (toFixType.isSigned()?"":"u");
		Symbol address = GecosUserCoreFactory.procSymbol(name, toFixType, new ArrayList<ParameterSymbol>()); //FIXME function parameter types
		long toFormat = USE_W_I_FORMAT ?  toFixType.getInteger() : toFixType.getFraction();
		long fromFormat = USE_W_I_FORMAT ?  fromFixType.getInteger() : fromFixType.getFraction();
		Instruction fixToFix = GecosUserInstructionFactory.call(address, 
				GecosUserInstructionFactory.Int(toFormat),
				GecosUserInstructionFactory.Int(fromFixType.getBitwidth()),
				GecosUserInstructionFactory.Int(fromFormat), i);
		return fixToFix;
	}
	
	@Override
	protected Instruction createFixedFunctionCall(CallInstruction c, ACFixedType fixType) {
		String name = c.getSymbolAddress();
		List<Instruction> params = new ArrayList<Instruction>();
		
		name = PREFIX + name + "_fix" + fixType.getBitwidth() + (fixType.isSigned()?"":"u");
		long format = USE_W_I_FORMAT ?  fixType.getInteger() : fixType.getFraction();
		params.add(GecosUserInstructionFactory.Int(format));
		
		params.addAll(c.getArgs());
		Symbol address = GecosUserCoreFactory.procSymbol(name, fixType, new ArrayList<ParameterSymbol>()); //FIXME function parameter types
		Instruction fixFunc = GecosUserInstructionFactory.call(address, params.toArray(new Instruction[params.size()]));
		
		return fixFunc;
	}
		
	@Override
	protected void addHeaders(ProcedureSet ps) {
		String headerName = USE_MACROS ? "IDFix_macros.h" : "IDFix.h";
		GecosUserAnnotationFactory.pragma(ps, GecosUserAnnotationFactory.CODEGEN_PRINT_ANNOTATION + "#include \"" + headerName + "\"");
		
		String headerPath;
		try {
			URL resource = FixedPtSpecificationApplier.class.getClassLoader().getResource("fr/irisa/cairn/idfix/backend/c/includes/" + headerName);
			headerPath = FileLocator.toFileURL(resource).getPath();
		} catch (Exception e) {
			throw new RuntimeException("couldn't locate header file: " + headerName);
		}
		GecosUserAnnotationFactory.pragma(ps, GecosUserAnnotationFactory.CODEGEN_COPYFILE_ANNOTATION + headerPath);
	}

	private static Instruction createInlineFixToFix(long out_bw, long out_fw, boolean out_signed, long in_fw, Instruction value) {
		Type type = createFixType(out_bw, out_fw, out_signed);
		long amount =  in_fw - out_fw;
		Instruction scaleInstruction = _createScaleInstruction(value, amount, type);
		return scaleInstruction;
	}
	
	private static Instruction createInlineFixToInt(Type intType, long out_bw, boolean out_signed, long in_fw, Instruction value) {
		Instruction inst = createInlineFixToFix(out_bw, 0, out_signed, in_fw, value);
		inst.setType(intType);
		return inst;
	}
	
	private static Instruction _createScaleInstruction(Instruction child, long scaleAmount, Type type) {
		Instruction scaleInstruction;
		if(scaleAmount > 0)
			scaleInstruction = GecosUserInstructionFactory.cast(type, GecosUserInstructionFactory.shr(child, scaleAmount));
		else if (scaleAmount < 0)
			scaleInstruction = GecosUserInstructionFactory.shl(GecosUserInstructionFactory.cast(type, child), -scaleAmount);
		else 
			scaleInstruction = GecosUserInstructionFactory.cast(type, child);
		
		return scaleInstruction;
	}
	
	private static Instruction createInlineFloatToFix(long bw, long fw, boolean signed, Instruction value) {
		double fixOne = Math.pow(2, fw);
		Type type = createFixType(bw, fw, signed);
		Instruction res = GecosUserInstructionFactory.cast(type, GecosUserInstructionFactory.mul(GecosUserInstructionFactory.Float(fixOne), value));
		return res;
	}
}
