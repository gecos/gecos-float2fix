//------------------------------------------------------------------------
//       ______     ______     _____
//      / ____/__  / ____/___ / ___/
//     / / __/ _ \/ /   / __ \\__ \
//    / /_/ /  __/ /___/ /_/ /__/ /
//    \____/\___/\____/\____/____/  --Generic Compiler Suite--
//                              	   (c) INRIA-IRISA 2011-2015
//------------------------------------------------------------------------
// @author: aelmouss
//------------------------------------------------------------------------
//NOTE: Rounding and Saturation are not supported yet!


#ifndef __IDFIX_H__
#define __IDFIX_H__

#include <stdint.h>
#include <math.h> 	//XXX

#define __INLINE_	inline


#ifndef __PREFIX_
	#define __PREFIX_(name)	alma_##name
#endif

#ifndef __FLOAT_TYPE_
	#define __FLOAT_TYPE_ 	float
#endif

#ifndef __MAX_INT_T
	#define __MAX_INT_BW	32
	#define __MAX_INT_T		int32_t
	#define __MAX_UINT_T	uint32_t
#endif

//TODO
//typedef enum {Q_TRUNC, Q_ROUND} quant_mode_t;
//typedef enum {S_WRAP, S_SAT} sat_mode_t;
//#define __QUANTIZATION_MODE = Q_TRUNC
//#define __QUANTIZATION_MODE = S_WRAP

/*==============================*/
/* Fixed-point data types 		*/
/*==============================*/
typedef int8_t 		fix8_t;
typedef uint8_t 	ufix8_t;

typedef int16_t 	fix16_t;
typedef uint16_t 	ufix16_t;

typedef int32_t 	fix32_t;
typedef uint32_t 	ufix32_t;

/* */
#define fix8_t(fw)		fix8_t
#define ufix8_t(fw)		ufix8_t
#define fix16_t(fw)		fix16_t
#define ufix16_t(fw)	ufix16_t
#define fix32_t(fw)		fix32_t
#define ufix32_t(fw)	ufix32_t

#define _fix_t(bw)		fix##bw##_t
#define _ufix_t(bw)		ufix##bw##_t

/*==============================*/
/* Macros				 		*/
/*==============================*/

/** fix to fix **/
#define _scale_(value, amount, type)                  \
	((amount) > 0 ? (type)((value) >> (amount))    :  \
	((amount) < 0 ? (((type)(value)) << -(amount)) :  (type)(value)))

#define _fixToFix_m(out_bw, out_fw, in_fw, value)	_scale_(value, (in_fw)-(out_fw), _fix_t(out_bw))
#define _UfixToFix_m(out_bw, out_fw, in_fw, value)	_scale_(value, (in_fw)-(out_fw), _ufix_t(out_bw))

/** fix_one **/
#define _fixOne_m(bw, fw)	((_fix_t(bw)) (1<<(fw)))
#define _UfixOne_m(bw, fw)	((_ufix_t(bw)) (1<<(fw)))

/** float to fix **/
#define _floatToFix_m(float_type, bw, fw, value)	((_fix_t(bw))((float_type)_fixOne_m(bw, fw) * (float_type)(value)))
#define _UfloatToFix_m(float_type, bw, fw, value)	((_ufix_t(bw))((float_type)_UfixOne_m(bw, fw) * (float_type)(value)))

/** fix to float **/
#define _fixToFloat_m(float_type, bw, fw, value)	((float_type)(value) / _fixOne_m(bw, fw))
#define _UfixToFloat_m(float_type, bw, fw, value)	((float_type)(value) / _UfixOne_m(bw, fw))

/** fix to int **/
#define _fixToInt_m(fw, value)			_fixToFix_m(__MAX_INT_BW, 0, fw, value)
#define _UfixToInt_m(fw, value)			_UfixToFix_m(__MAX_INT_BW, 0, fw, value)

/** int to fix **/
#define _intToFix_m(bw, fw, value)		_fixToFix_m(bw, fw, 0, value)
#define _UintToFix_m(bw, fw, value)		_UfixToFix_m(bw, fw, 0, value)


/**********************/
/** Arith operations **/
#define _fixMul_m(bw, fw, a, b)		((a) * (b))     // _fixToFix_m(bw, fw, 2*fw, (a) * (b))	//XXX make sure scaling is correct
#define _fixDiv_m(bw, fw, a, b)		((a) / (b))		//TODO scale
#define _fixAdd_m(bw, fw, a, b)		((a) + (b))		//TODO align...
#define _fixSub_m(bw, fw, a, b)		((a) - (b))		//TODO align...

/** Math functions **/
//TODO
#define _fixCos_m(bw, fw, a)	    _floatToFix_m(__FLOAT_TYPE_, bw, fw, cos(_fixToFloat_m(__FLOAT_TYPE_, bw, fw, a)))
#define _fixSin_m(bw, fw, a)        _floatToFix_m(__FLOAT_TYPE_, bw, fw, sin(_fixToFloat_m(__FLOAT_TYPE_, bw, fw, a)))
#define _fixSqrt_m(bw, fw, a)       _floatToFix_m(__FLOAT_TYPE_, bw, fw, sqrt(_fixToFloat_m(__FLOAT_TYPE_, bw, fw, a)))
#define _fixfloor_m(bw, fw, a)      _floatToFix_m(__FLOAT_TYPE_, bw, fw, floor(_fixToFloat_m(__FLOAT_TYPE_, bw, fw, a)))
#define _fixRound_m(bw, fw, a)      _floatToFix_m(__FLOAT_TYPE_, bw, fw, round(_fixToFloat_m(__FLOAT_TYPE_, bw, fw, a)))
#define _fixCeil_m(bw, fw, a)       _floatToFix_m(__FLOAT_TYPE_, bw, fw, ceil(_fixToFloat_m(__FLOAT_TYPE_, bw, fw, a)))
#define _fixExp_m(bw, fw, a)        _floatToFix_m(__FLOAT_TYPE_, bw, fw, exp(_fixToFloat_m(__FLOAT_TYPE_, bw, fw, a)))
#define _fixLog_m(bw, fw, a)        _floatToFix_m(__FLOAT_TYPE_, bw, fw, log(_fixToFloat_m(__FLOAT_TYPE_, bw, fw, a)))
#define _fixLog2_m(bw, fw, a)       _floatToFix_m(__FLOAT_TYPE_, bw, fw, log2(_fixToFloat_m(__FLOAT_TYPE_, bw, fw, a)))
#define _fixAtan2_m(bw, fw, a, b)   _floatToFix_m(__FLOAT_TYPE_, bw, fw, atan2(_fixToFloat_m(__FLOAT_TYPE_, bw, fw, a), _fixToFloat_m(__FLOAT_TYPE_, bw, fw, b)))
#define _fixPow_m(bw, fw, a, b)     _floatToFix_m(__FLOAT_TYPE_, bw, fw, pow(_fixToFloat_m(__FLOAT_TYPE_, bw, fw, a), _fixToFloat_m(__FLOAT_TYPE_, bw, fw, b)))


#if 0
//TODO
//#ifdef __XENTIUM__

	#include "../../../lib/iqmath-1.0.0/include/iqmathlib.h"

	#define fix32_t						int32_t

	#define alma_fix_pow_32(fw, a, x)	(fix_exp(alma_fix_mul_32(fw, x , fix_log(a, fw)),fw, fix_exp_coef))
	#define alma_intToFix32(fw, x)		(x << fw)
	#define alma_fix32ToInt(fw,x)		fix_to_int(x,fw)
	#define alma_fix_sub_32(fw, a, b)	(a - b)
	#define alma_fix_add_32(fw, a, b)	(a + b)
	#define alma_fix_mul_32(fw, a, b)	((int32_t)(((int64_t) a * b) >> fw))
	#define alma_fix_div_32(fw, a, b)	fix_div_32(a, b, fw, fix_div_lut)
	#define alma_fix_cos_32(fw, x)		fix_cos(x, fw, fix_sincos_coeff)
	#define alma_fix_sin_32(fw, x)		fix_sin(x, fw, fix_sincos_coeff)
	#define alma_fix_floor_32(fw, x)	fix_floor(x, fw)
	#define alma_fix_round_32(fw, x)	(alma_intToFix32(fw, fix_to_int(x, fw)))
	#define alma_fix_ceil_32(fw, x)		fix_ceil(x, fw)
	#define alma_fix_log_32(fw, x)		fix_log(x, fw)
	#define alma_fix_log2_32(fw, x)		(fix_div_32(fix_log(x, fw),fix_log(alma_intToFix32(fw, 2), fw),fix_div_lut))
	#define alma_fix_atan2_32(fw, y, x)	fix_atan2_32(y, x, fw, fix_div_lut, fix_atan_coeff)
	#define alma_fix_exp_32(fw, x)		fix_exp(x, fw, fix_exp_coef)
	#define alma_fix_sqrt_32(fw, x)		fix_sqrt(x, fw)
	#define alma_fix_trunc_32(fw, x)	fix_int(x, fw)


	const int32_t fix_exp_coef[] =
	{
		0x00030115,
		0x001580B4,
		0x009DB44F,
		0x038D5B6D,
		0x0F5FDF83,
		0x2C5C85F9,
		0x40000000
	};

	const int32_t fix_div_lut[] =
	{
		0x7D7E7F80,
		0x797A7B7C,
		0x75767778,
		0x72737475,
		0x6F707071,
		0x6C6D6D6E,
		0x696A6B6B,
		0x67676869,
		0x64656566,
		0x62626363,
		0x5F606061,
		0x5D5E5E5F,
		0x5B5C5C5D,
		0x595A5A5B,
		0x57585859,
		0x55565657,
		0x54545455,
		0x52525353,
		0x50515151,
		0x4F4F4F50,
		0x4D4E4E4E,
		0x4C4C4C4D,
		0x4A4B4B4B,
		0x49494A4A,
		0x48484849,
		0x46474747,
		0x45464646,
		0x44444545,
		0x43434344,
		0x42424243,
		0x41414142,
		0x40404041
	};

	const int32_t fix_sincos_coeff[] __attribute__ ((aligned(8))) =
	{
		0xfffffffc,
		0xffffffcc,
		0x000002a1,
		0x00001e1e,
		0xfffecd2d,
		0xfff55162,
		0x00519af2,
		0x020783e1,
		0xf5aa218d,
		0xd885866d,
		0x6487ed51,
		0x7fffffff
	};

	const int32_t  fix_atan_coeff[] __attribute__ ((aligned(8))) =
	{
		0x066f8183,
		0xf78b97ff,
		0x09d7d2ab,
		0xf45d1bf5,
		0x0e38e37e,
		0xedb6db6e,
		0x1999999a,
		0xd5555555,
		0x7fffffff
	};

#endif //define(__XENTIUM__)



/*==============================*/
/* Type Conversion Functions	*/
/*==============================*/
/* float to fixed Conversion */
#define alma_floatToFix8(fw,value)  _floatToFix_m (__FLOAT_TYPE_, 8,  fw, value)
#define alma_floatToFix16(fw,value)  _floatToFix_m (__FLOAT_TYPE_, 16, fw, value)
#define alma_floatToFix32(fw,value)  _floatToFix_m (__FLOAT_TYPE_, 32, fw, value)
#define alma_ufloatToFix8(fw,value)  _UfloatToFix_m(__FLOAT_TYPE_, 8,  fw, value)
#define alma_ufloatToFix16(fw,value)  _UfloatToFix_m(__FLOAT_TYPE_, 16, fw, value)
#define alma_ufloatToFix32(fw,value)  _UfloatToFix_m(__FLOAT_TYPE_, 32, fw, value)

/* fix to float */
#define alma_fix8ToFloat(fw,    value)  _fixToFloat_m (__FLOAT_TYPE_, 8,  fw, value)
#define alma_fix16ToFloat(fw,   value)  _fixToFloat_m (__FLOAT_TYPE_, 16, fw, value)
#define alma_fix32ToFloat(fw,   value)  _fixToFloat_m (__FLOAT_TYPE_, 32, fw, value)
#define alma_ufix8ToFloat(fw,   value)  _UfixToFloat_m(__FLOAT_TYPE_, 8,  fw, value)
#define alma_ufix16ToFloat(fw,  value)  _UfixToFloat_m(__FLOAT_TYPE_, 16, fw, value)
#define alma_ufix32ToFloat(fw,  value)  _UfixToFloat_m(__FLOAT_TYPE_, 32, fw, value)

/*
 * fixed to fixed Conversion:
 * ! sign indicator is the same for both input and output
 */
#define alma_fixToFix8(out_fw, in_fw,   value)  _fixToFix_m (8,  out_fw, in_fw, value)
#define alma_fixToFix16(out_fw, in_fw,  value)  _fixToFix_m (16, out_fw, in_fw, value)
#define alma_fixToFix32(out_fw, in_fw,  value)  _fixToFix_m (32, out_fw, in_fw, value)
#define alma_ufixToFix8(out_fw, in_fw,  value)  _UfixToFix_m(8,  out_fw, in_fw, value)
#define alma_ufixToFix16(out_fw, in_fw, value)  _UfixToFix_m(16, out_fw, in_fw, value)
#define alma_ufixToFix32(out_fw, in_fw, value)  _UfixToFix_m(32, out_fw, in_fw, value)

/*
 * int to fixed Conversion
 * ! sign indicator is the same for both input and output
 */
#define alma_intToFix8(fw,   value)  _intToFix_m (8,  fw, value)
#define alma_intToFix16(fw,  value)  _intToFix_m (16, fw, value)
#define alma_intToFix32(fw,  value)  _intToFix_m (32, fw, value)
#define alma_uintToFix8(fw,  value)  _UintToFix_m(8,  fw, value)
#define alma_uintToFix16(fw, value)  _UintToFix_m(16, fw, value)
#define alma_uintToFix32(fw, value)  _UintToFix_m(32, fw, value)

/*
 * fixed to int Conversion
 * ! sign indicator is the same for both input and output
 */
#define alma_fix8ToInt(fw,    value)  _fixToInt_m (fw, value)
#define alma_fix16ToInt(fw,   value)  _fixToInt_m (fw, value)
#define alma_fix32ToInt(fw,   value)  _fixToInt_m (fw, value)
#define alma_ufix8ToInt(fw,   value)  _UfixToInt_m(fw, value)
#define alma_ufix16ToInt(fw,  value)  _UfixToInt_m(fw, value)
#define alma_ufix32ToInt(fw,  value)  _UfixToInt_m(fw, value)

/*==============================*/
/* Arith operation Functions	*/
/*==============================*/
/*
 * Multiplication
 * ! fixed-point type is the same for inputs and output
 */
#define alma_fix_mul_8(fw,    a,   b)  _fixMul_m (8 , fw, a, b)
#define alma_fix_mul_16(fw,   a,   b)  _fixMul_m (16, fw, a, b)
#define alma_fix_mul_32(fw,   a,   b)  _fixMul_m (32, fw, a, b)

/*
 * Division
 * ! fixed-point type is the same for inputs and output
 */
#define alma_fix_div_8(fw,    a,   b)  _fixDiv_m (8 , fw, a, b)
#define alma_fix_div_16(fw,   a,   b)  _fixDiv_m (16, fw, a, b)
#define alma_fix_div_32(fw,   a,   b)  _fixDiv_m (32, fw, a, b)

/*
 * Addition
 * ! fixed-point type is the same for inputs and output
 */
#define alma_fix_add_8(fw,    a,   b)  _fixAdd_m (8 , fw, a, b)
#define alma_fix_add_16(fw,   a,   b)  _fixAdd_m (16, fw, a, b)
#define alma_fix_add_32(fw,   a,   b)  _fixAdd_m (32, fw, a, b)

/*
 * Subtraction
 * ! fixed-point type is the same for inputs and output
 */
#define alma_fix_sub_8(fw,    a,   b)  _fixSub_m (8 , fw, a, b)
#define alma_fix_sub_16(fw,   a,   b)  _fixSub_m (16, fw, a, b)
#define alma_fix_sub_32(fw,   a,   b)  _fixSub_m (32, fw, a, b)

/*
 * Reminder
 * ! fixed-point type is the same for inputs and output
 */
#define alma_fix_rem_32(fw,  a)  _fixCos_m(32, fw, a)

/*==============================*/
/* Math Functions				*/
/*==============================*/
#define alma_fix_cos_8(fw,   a)  _fixCos_m(32, fw, a)
#define alma_fix_cos_16(fw,  a)  _fixCos_m(32, fw, a)
#define alma_fix_cos_32(fw,  a)  _fixCos_m(32, fw, a)

#define alma_fix_sin_8(fw,   a)  _fixSin_m(32, fw, a)
#define alma_fix_sin_16(fw,  a)  _fixSin_m(32, fw, a)
#define alma_fix_sin_32(fw,  a)  _fixSin_m(32, fw, a)

#define alma_fix_sqrt_32(fw,  a)   _fixSqrt_m(32, fw, a)
#define alma_fix_floor_32(fw, a)   _fixfloor_m(32, fw, a)
#define alma_fix_round_32(fw, a)   _fixRound_m(32, fw, a)
#define alma_fix_ceil_32(fw,  a)   _fixCeil_m(32, fw, a)
#define alma_fix_exp_32(fw,   a)   _fixExp_m(32, fw, a)
#define alma_fix_log_32(fw,   a)   _fixLog_m(32, fw, a)
#define alma_fix_log2_32(fw,  a)   _fixLog2_m(32, fw, a)
#define alma_fix_atan2_32(fw, a,  b)  _fixAtan2_m(32, fw, a, b)
#define alma_fix_pow_32(fw,   a,  b)    _fixPow_m(32, fw, a, b)

#define alma_fix_sqrt_16(fw,  a)   _fixSqrt_m(16, fw, a)
#define alma_fix_floor_16(fw, a)   _fixfloor_m(16, fw, a)
#define alma_fix_round_16(fw, a)   _fixRound_m(16, fw, a)
#define alma_fix_ceil_16(fw,  a)   _fixCeil_m(16, fw, a)
#define alma_fix_exp_16(fw,   a)   _fixExp_m(16, fw, a)
#define alma_fix_log_16(fw,   a)   _fixLog_m(16, fw, a)
#define alma_fix_log2_16(fw,  a)   _fixLog2_m(16, fw, a)
#define alma_fix_atan2_16(fw, a,  b)  _fixAtan2_m(16, fw, a, b)
#define alma_fix_pow_16(fw,   a,  b)    _fixPow_m(16, fw, a, b)

#endif /* !defined(__IDFIX_H__) */
