package fr.irisa.cairn.idfix.backend.c;

import org.apache.logging.log4j.Logger;

import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.idfix.backend.AbstractFixedPointBackendModel;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.logging.IDFixLogger;
import gecos.blocks.BasicBlock;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;
import gecos.gecosproject.GecosProject;
import gecos.instrs.ArrayValueInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.RetInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SimpleArrayInstruction;
import gecos.instrs.SizeofTypeInstruction;
import gecos.instrs.SizeofValueInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.ACFixedType;
import gecos.types.FloatType;
import gecos.types.IntegerType;
import gecos.types.Type;

/**
 * Convert AC_Fixed to Integer types. Insert all necessary scalings. <br>
 * NOTE: ONLY support AC_Fixed types with bit-width in {8, 16, 32, (64)}
 * 
 * @author aelmouss
 */
public abstract class AbstractCFixedPointBackendModel extends AbstractFixedPointBackendModel {

	protected static final Logger _logger = IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_BACKEND);
	private ACFixedType _currentCastType;
	
	
	public AbstractCFixedPointBackendModel(GecosProject proj) {
		super(proj);
	}
	
	protected abstract Instruction createIntToFixInstruction(Instruction i, IntegerType fromIntType, ACFixedType toFixType);
	protected abstract Instruction createFloatToFixInstruction(Instruction f, FloatType fromFloatType, ACFixedType toFixType);
	protected abstract Instruction createFixtToFixInstruction(Instruction i, ACFixedType fromFixType, ACFixedType toFixType);
	protected abstract Instruction createFixedFunctionCall(CallInstruction c, ACFixedType fixType);
	protected Instruction createFixedGenericInstruction(GenericInstruction g, ACFixedType fixType) {
		Instruction scaled = scale(g, _currentCastType);
		return scaled;
	}
	
	//TODO
//	protected abstract Instruction createFixToIntInstruction(Instruction i, ACFixedType fixType);
//	protected abstract Instruction createFixToFloatInstruction(Instruction i, ACFixedType fixType);
	
	
	@Override
	public void visitBasicBlock(BasicBlock b) 
	{
		if(b.getInstructionCount() > 0) {
			for(Instruction inst : b.getInstructions()) {
				inst.accept(this);
			}
		}
	}
	
	@Override
	protected void visitComplexInstruction(ComplexInstruction c) {
		for(Instruction child : c.listChildren())
			child.accept(this);
	}

	@Override
	public void visitSetInstruction(SetInstruction s) {
		Type destType = s.getDest().getType();
		if(destType instanceof ACFixedType) {
			ACFixedType saved = _currentCastType;
			_currentCastType = (ACFixedType) destType;
			s.getSource().accept(this);
			_currentCastType = saved;
		}
		else
			s.getSource().accept(this);
		
		s.getDest().accept(this);
	}

	@Override
	public void visitConvertInstruction(ConvertInstruction c) {
		ACFixedType saved = _currentCastType;
		boolean substitute = false;
		
		if(c.getType() instanceof ACFixedType) {
			substitute = true;
			_currentCastType = (ACFixedType) c.getType();
		}
		else {
			/* case of cast(cast(..)): the outermost ACFixed cast type is used */ 
			if(c.getParent() instanceof ConvertInstruction && _currentCastType instanceof ACFixedType) {
				substitute = true;
				//_currentCastType remains the same as previous
			}
			else
				_currentCastType = null; //FIXME support fix_to_int, fix_to_float ..
		}
		
		/* visit innermost first */
		c.getExpr().accept(this);
		
		_currentCastType = saved;
		
		if(substitute)
			c.substituteWith(c.getExpr());
		else {
			Type newType = processType(c.getType());
			if(newType != c.getType())
				c.setType(newType);
		}

//		ACFixedType castType = _currentCastType;
//		
//		if(c.getType() instanceof ACFixedType) {
//			_currentCastType = (ACFixedType) c.getType();
//		}
//		else {
//			_currentCastType = null; //FIXME support fix_to_int, fix_to_float ..
//		}
//		
//		/* visit innermost first */
//		Instruction child = c.getChild(0);
//		child.accept(this);
//		
//		_currentCastType = castType;
//		
//		/* in case child was substituted */
//		child = c.getChild(0);
//		
//		if(c.getType() instanceof ACFixedType)
//			c.substituteWith(child);
//		else {
//			Type newType = processType(c.getType());
//			if(newType != c.getType())
//				c.setType(newType);
//		}
	}
	
	@Override
	public void visitGenericInstruction(GenericInstruction g) 
	{
		boolean isFixedOperation = false;
		for(Instruction child : g.getChildren()) {
			if(new TypeAnalyzer(child.getType()).getBaseLevel().isFixedPoint()) {
				isFixedOperation = true;
				break;
			}
		}
		
		/* visit innermost first */
		ACFixedType saved = _currentCastType;
		_currentCastType = null;
		visitComplexInstruction(g);
		_currentCastType = saved;
		
		Instruction scaled = null;
		if(isFixedOperation && (_currentCastType instanceof ACFixedType)) { //XXX
			scaled = createFixedGenericInstruction((GenericInstruction)g.copy(), _currentCastType);
		}
		else if(_currentCastType instanceof ACFixedType) {
			scaled = scale(g.copy(), _currentCastType);
		}
		
		if(scaled != null) {
			scaled = GecosUserInstructionFactory.cast(processType(_currentCastType), scaled);
			g.substituteWith(scaled);
		}
		//TODO? : always cast g to type ?
		
		
	}
	
	@Override
	public void visitCallInstruction(CallInstruction c) //FIXME
	{
		/* visit innermost first */
		ACFixedType saved = _currentCastType;
		_currentCastType = null;
		for(Instruction arg : c.getArgs())
			arg.accept(this);
		_currentCastType = saved;
		
		if(_currentCastType instanceof ACFixedType) {
			Instruction inst = createFixedFunctionCall((CallInstruction) c.copy(), _currentCastType);
			c.substituteWith(inst);	
		}
	}
	
	@Override
	public void visitArrayValueInstruction(ArrayValueInstruction a) {
		for (Instruction i : a.getChildren()) {
			i.accept(this);
		}
	}
	
	@Override
	public void visitIntInstruction(IntInstruction i) 
	{
		if(_currentCastType instanceof ACFixedType) {
			Instruction inst = createIntToFixInstruction((IntInstruction)i.copy(), (IntegerType)i.getType(), _currentCastType);
			i.substituteWith(inst);	
		}
	}

	@Override
	public void visitFloatInstruction(FloatInstruction f) 
	{
		if(_currentCastType instanceof ACFixedType) {
			Instruction inst = createFloatToFixInstruction((FloatInstruction)f.copy(), (FloatType)f.getType(), _currentCastType);
			f.substituteWith(inst);	
		}
	}
	
	@Override
	public void visitSymbolInstruction(SymbolInstruction s) 
	{
		if(_currentCastType instanceof ACFixedType) {
			Instruction scaled = scale(s.copy(), _currentCastType);
			s.substituteWith(scaled);
		}
	}

	@Override
	public void visitSimpleArrayInstruction(SimpleArrayInstruction s) 
	{
		/* visit innermost first */
		ACFixedType saved = _currentCastType;
		_currentCastType = null;
		for(Instruction arg : s.getIndex())
			arg.accept(this);
		_currentCastType = saved;
		
		if(_currentCastType instanceof ACFixedType) {
			Instruction scaled = scale(s.copy(), _currentCastType);
			s.substituteWith(scaled);
		}
	}
	
	@Override
	public void visitDAGInstruction(DAGInstruction dag) 
	{
		new DAGScalingInsertor(dag).compute();
		
		//XXX unchecked
		for(DAGNode node : dag.getNodes()) {
			TypeAnalyzer ta = new TypeAnalyzer(node.getType());
			if (ta.getBaseLevel().isFixedPoint()) {
				Type newBaseType = createFixType(null, (ACFixedType)ta.getBase()); //BackEndUtils.createIntTypeFromACFixed((ACFixedType)ta.getBase(), USE_STDINT_TYPES);
				node.setType(ta.revertAllLevelsOn(newBaseType));
			}
		}
	}
	
	@Override
	public void visitSizeofTypeInstruction(SizeofTypeInstruction s) {
		TypeAnalyzer typeAnalyzer = new TypeAnalyzer(s.getTargetType());
		if(typeAnalyzer.getBaseLevel().isFixedPoint()) {
			Type fixType = createFixType(null, (ACFixedType)typeAnalyzer.getBase());
			fixType = typeAnalyzer.revertAllLevelsOn(fixType);
			s.setTargetType(fixType);
		}
	}

	@Override
	public void visitSizeofValueInstruction(SizeofValueInstruction s) {
		// TODO Auto-generated method stub
		super.visitSizeofValueInstruction(s);
	}

	@Override
	public void visitRetInstruction(RetInstruction r) {
		super.visitRetInstruction(r);
	}
	
	protected Instruction scale(Instruction child, ACFixedType castType) {
		TypeAnalyzer typeAnalyzer = new TypeAnalyzer(child.getType());
		
		if(typeAnalyzer.getBaseLevel().isFixedPoint()) {
			return createFixtToFixInstruction(child, (ACFixedType) typeAnalyzer.getBase(), castType);
		}
		else if(typeAnalyzer.getBaseLevel().isInt()) {
			return createIntToFixInstruction(child, typeAnalyzer.getBaseLevel().getBaseAsInt(), castType);
		}
		else if(typeAnalyzer.getBaseLevel().isFloat()) {
			return createFloatToFixInstruction(child, typeAnalyzer.getBaseLevel().getBaseAsFloat(), castType);
		}
		
		throw new UnsupportedOperationException("not yet implemented for type: " + typeAnalyzer.getBase() + " in: " + child);
	}
	
	private Type processType(Type type) {
		TypeAnalyzer typeAnalyser = new TypeAnalyzer(type);
		if(typeAnalyser.getBaseLevel().isFixedPoint()) {
			Type fixType = createFixType(null, (ACFixedType)typeAnalyser.getBase());
			
			return typeAnalyser.revertAllLevelsOn(fixType);
		}
		return type;
	}
	
}