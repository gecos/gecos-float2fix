/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package FixPtSpecification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fix Pt Specif</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link FixPtSpecification.FixPtSpecif#getListFixPtOpsSpecif <em>List Fix Pt Ops Specif</em>}</li>
 *   <li>{@link FixPtSpecification.FixPtSpecif#getListFixPtType <em>List Fix Pt Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see FixPtSpecification.FixPtSpecificationPackage#getFixPtSpecif()
 * @model
 * @generated
 */
public interface FixPtSpecif extends EObject {
	/**
	 * Returns the value of the '<em><b>List Fix Pt Ops Specif</b></em>' containment reference list.
	 * The list contents are of type {@link FixPtSpecification.FixPtOpsSpecif}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Fix Pt Ops Specif</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Fix Pt Ops Specif</em>' containment reference list.
	 * @see FixPtSpecification.FixPtSpecificationPackage#getFixPtSpecif_ListFixPtOpsSpecif()
	 * @model containment="true"
	 * @generated
	 */
	EList<FixPtOpsSpecif> getListFixPtOpsSpecif();

	/**
	 * Returns the value of the '<em><b>List Fix Pt Type</b></em>' map.
	 * The key is of type {@link FixPtSpecification.DataId},
	 * and the value is of type {@link FixPtSpecification.FixPtType},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Fix Pt Type</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Fix Pt Type</em>' map.
	 * @see FixPtSpecification.FixPtSpecificationPackage#getFixPtSpecif_ListFixPtType()
	 * @model mapType="FixPtSpecification.StringToFixPtTypeMapEntry<FixPtSpecification.DataId, FixPtSpecification.FixPtType>"
	 * @generated
	 */
	EMap<DataId, FixPtType> getListFixPtType();

} // FixPtSpecif
