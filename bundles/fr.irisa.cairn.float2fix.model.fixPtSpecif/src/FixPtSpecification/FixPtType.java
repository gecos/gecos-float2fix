/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package FixPtSpecification;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fix Pt Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link FixPtSpecification.FixPtType#getTypeAcFixed <em>Type Ac Fixed</em>}</li>
 *   <li>{@link FixPtSpecification.FixPtType#getDyn <em>Dyn</em>}</li>
 * </ul>
 * </p>
 *
 * @see FixPtSpecification.FixPtSpecificationPackage#getFixPtType()
 * @model
 * @generated
 */
public interface FixPtType extends EObject {
	/**
	 * Returns the value of the '<em><b>Type Ac Fixed</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Ac Fixed</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Ac Fixed</em>' containment reference.
	 * @see #setTypeAcFixed(TypeAcFixed)
	 * @see FixPtSpecification.FixPtSpecificationPackage#getFixPtType_TypeAcFixed()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TypeAcFixed getTypeAcFixed();

	/**
	 * Sets the value of the '{@link FixPtSpecification.FixPtType#getTypeAcFixed <em>Type Ac Fixed</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Ac Fixed</em>' containment reference.
	 * @see #getTypeAcFixed()
	 * @generated
	 */
	void setTypeAcFixed(TypeAcFixed value);

	/**
	 * Returns the value of the '<em><b>Dyn</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dyn</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dyn</em>' containment reference.
	 * @see #setDyn(Dynamic)
	 * @see FixPtSpecification.FixPtSpecificationPackage#getFixPtType_Dyn()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Dynamic getDyn();

	/**
	 * Sets the value of the '{@link FixPtSpecification.FixPtType#getDyn <em>Dyn</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dyn</em>' containment reference.
	 * @see #getDyn()
	 * @generated
	 */
	void setDyn(Dynamic value);
	
	public FixPtType copy();

} // FixPtType
