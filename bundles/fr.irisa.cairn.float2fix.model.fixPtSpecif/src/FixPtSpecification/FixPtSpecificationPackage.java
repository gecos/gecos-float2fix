/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package FixPtSpecification;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see FixPtSpecification.FixPtSpecificationFactory
 * @model kind="package"
 * @generated
 */
public interface FixPtSpecificationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "FixPtSpecification";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "fr.irisa.cairn.float2fix.model.FixPtSpecification";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "fr.irisa.cairn.float2fix.model.FixPtSpecification";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FixPtSpecificationPackage eINSTANCE = FixPtSpecification.impl.FixPtSpecificationPackageImpl.init();

	/**
	 * The meta object id for the '{@link FixPtSpecification.impl.FixPtSpecifImpl <em>Fix Pt Specif</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FixPtSpecification.impl.FixPtSpecifImpl
	 * @see FixPtSpecification.impl.FixPtSpecificationPackageImpl#getFixPtSpecif()
	 * @generated
	 */
	int FIX_PT_SPECIF = 0;

	/**
	 * The feature id for the '<em><b>List Fix Pt Ops Specif</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIX_PT_SPECIF__LIST_FIX_PT_OPS_SPECIF = 0;

	/**
	 * The feature id for the '<em><b>List Fix Pt Type</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIX_PT_SPECIF__LIST_FIX_PT_TYPE = 1;

	/**
	 * The number of structural features of the '<em>Fix Pt Specif</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIX_PT_SPECIF_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link FixPtSpecification.impl.FixPtOpsSpecifImpl <em>Fix Pt Ops Specif</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FixPtSpecification.impl.FixPtOpsSpecifImpl
	 * @see FixPtSpecification.impl.FixPtSpecificationPackageImpl#getFixPtOpsSpecif()
	 * @generated
	 */
	int FIX_PT_OPS_SPECIF = 1;

	/**
	 * The feature id for the '<em><b>Input1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIX_PT_OPS_SPECIF__INPUT1 = 0;

	/**
	 * The feature id for the '<em><b>Input2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIX_PT_OPS_SPECIF__INPUT2 = 1;

	/**
	 * The feature id for the '<em><b>Output</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIX_PT_OPS_SPECIF__OUTPUT = 2;

	/**
	 * The feature id for the '<em><b>Num Op</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIX_PT_OPS_SPECIF__NUM_OP = 3;

	/**
	 * The feature id for the '<em><b>Name Op</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIX_PT_OPS_SPECIF__NAME_OP = 4;

	/**
	 * The number of structural features of the '<em>Fix Pt Ops Specif</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIX_PT_OPS_SPECIF_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link FixPtSpecification.impl.FixPtTypeImpl <em>Fix Pt Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FixPtSpecification.impl.FixPtTypeImpl
	 * @see FixPtSpecification.impl.FixPtSpecificationPackageImpl#getFixPtType()
	 * @generated
	 */
	int FIX_PT_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Type Ac Fixed</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIX_PT_TYPE__TYPE_AC_FIXED = 0;

	/**
	 * The feature id for the '<em><b>Dyn</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIX_PT_TYPE__DYN = 1;

	/**
	 * The number of structural features of the '<em>Fix Pt Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIX_PT_TYPE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link FixPtSpecification.impl.TypeAcFixedImpl <em>Type Ac Fixed</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FixPtSpecification.impl.TypeAcFixedImpl
	 * @see FixPtSpecification.impl.FixPtSpecificationPackageImpl#getTypeAcFixed()
	 * @generated
	 */
	int TYPE_AC_FIXED = 3;

	/**
	 * The feature id for the '<em><b>Wd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_AC_FIXED__WD = 0;

	/**
	 * The feature id for the '<em><b>Wip</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_AC_FIXED__WIP = 1;

	/**
	 * The feature id for the '<em><b>S</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_AC_FIXED__S = 2;

	/**
	 * The feature id for the '<em><b>Q</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_AC_FIXED__Q = 3;

	/**
	 * The feature id for the '<em><b>O</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_AC_FIXED__O = 4;

	/**
	 * The number of structural features of the '<em>Type Ac Fixed</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_AC_FIXED_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link FixPtSpecification.impl.DynamicImpl <em>Dynamic</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FixPtSpecification.impl.DynamicImpl
	 * @see FixPtSpecification.impl.FixPtSpecificationPackageImpl#getDynamic()
	 * @generated
	 */
	int DYNAMIC = 4;

	/**
	 * The feature id for the '<em><b>Lower Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DYNAMIC__LOWER_BOUND = 0;

	/**
	 * The feature id for the '<em><b>Upper Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DYNAMIC__UPPER_BOUND = 1;

	/**
	 * The number of structural features of the '<em>Dynamic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DYNAMIC_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link FixPtSpecification.impl.StringToFixPtTypeMapEntryImpl <em>String To Fix Pt Type Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FixPtSpecification.impl.StringToFixPtTypeMapEntryImpl
	 * @see FixPtSpecification.impl.FixPtSpecificationPackageImpl#getStringToFixPtTypeMapEntry()
	 * @generated
	 */
	int STRING_TO_FIX_PT_TYPE_MAP_ENTRY = 5;

	/**
	 * The feature id for the '<em><b>Key</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_FIX_PT_TYPE_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_FIX_PT_TYPE_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>String To Fix Pt Type Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_FIX_PT_TYPE_MAP_ENTRY_FEATURE_COUNT = 2;


	/**
	 * The meta object id for the '{@link FixPtSpecification.impl.DataIdImpl <em>Data Id</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FixPtSpecification.impl.DataIdImpl
	 * @see FixPtSpecification.impl.FixPtSpecificationPackageImpl#getDataId()
	 * @generated
	 */
	int DATA_ID = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ID__NAME = 0;

	/**
	 * The feature id for the '<em><b>Num</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ID__NUM = 1;

	/**
	 * The number of structural features of the '<em>Data Id</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ID_FEATURE_COUNT = 2;


	/**
	 * Returns the meta object for class '{@link FixPtSpecification.FixPtSpecif <em>Fix Pt Specif</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fix Pt Specif</em>'.
	 * @see FixPtSpecification.FixPtSpecif
	 * @generated
	 */
	EClass getFixPtSpecif();

	/**
	 * Returns the meta object for the containment reference list '{@link FixPtSpecification.FixPtSpecif#getListFixPtOpsSpecif <em>List Fix Pt Ops Specif</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>List Fix Pt Ops Specif</em>'.
	 * @see FixPtSpecification.FixPtSpecif#getListFixPtOpsSpecif()
	 * @see #getFixPtSpecif()
	 * @generated
	 */
	EReference getFixPtSpecif_ListFixPtOpsSpecif();

	/**
	 * Returns the meta object for the map '{@link FixPtSpecification.FixPtSpecif#getListFixPtType <em>List Fix Pt Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>List Fix Pt Type</em>'.
	 * @see FixPtSpecification.FixPtSpecif#getListFixPtType()
	 * @see #getFixPtSpecif()
	 * @generated
	 */
	EReference getFixPtSpecif_ListFixPtType();

	/**
	 * Returns the meta object for class '{@link FixPtSpecification.FixPtOpsSpecif <em>Fix Pt Ops Specif</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fix Pt Ops Specif</em>'.
	 * @see FixPtSpecification.FixPtOpsSpecif
	 * @generated
	 */
	EClass getFixPtOpsSpecif();

	/**
	 * Returns the meta object for the containment reference '{@link FixPtSpecification.FixPtOpsSpecif#getInput1 <em>Input1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Input1</em>'.
	 * @see FixPtSpecification.FixPtOpsSpecif#getInput1()
	 * @see #getFixPtOpsSpecif()
	 * @generated
	 */
	EReference getFixPtOpsSpecif_Input1();

	/**
	 * Returns the meta object for the containment reference '{@link FixPtSpecification.FixPtOpsSpecif#getInput2 <em>Input2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Input2</em>'.
	 * @see FixPtSpecification.FixPtOpsSpecif#getInput2()
	 * @see #getFixPtOpsSpecif()
	 * @generated
	 */
	EReference getFixPtOpsSpecif_Input2();

	/**
	 * Returns the meta object for the containment reference '{@link FixPtSpecification.FixPtOpsSpecif#getOutput <em>Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Output</em>'.
	 * @see FixPtSpecification.FixPtOpsSpecif#getOutput()
	 * @see #getFixPtOpsSpecif()
	 * @generated
	 */
	EReference getFixPtOpsSpecif_Output();

	/**
	 * Returns the meta object for the attribute '{@link FixPtSpecification.FixPtOpsSpecif#getNumOp <em>Num Op</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Op</em>'.
	 * @see FixPtSpecification.FixPtOpsSpecif#getNumOp()
	 * @see #getFixPtOpsSpecif()
	 * @generated
	 */
	EAttribute getFixPtOpsSpecif_NumOp();

	/**
	 * Returns the meta object for the attribute '{@link FixPtSpecification.FixPtOpsSpecif#getNameOp <em>Name Op</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name Op</em>'.
	 * @see FixPtSpecification.FixPtOpsSpecif#getNameOp()
	 * @see #getFixPtOpsSpecif()
	 * @generated
	 */
	EAttribute getFixPtOpsSpecif_NameOp();

	/**
	 * Returns the meta object for class '{@link FixPtSpecification.FixPtType <em>Fix Pt Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fix Pt Type</em>'.
	 * @see FixPtSpecification.FixPtType
	 * @generated
	 */
	EClass getFixPtType();

	/**
	 * Returns the meta object for the containment reference '{@link FixPtSpecification.FixPtType#getTypeAcFixed <em>Type Ac Fixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type Ac Fixed</em>'.
	 * @see FixPtSpecification.FixPtType#getTypeAcFixed()
	 * @see #getFixPtType()
	 * @generated
	 */
	EReference getFixPtType_TypeAcFixed();

	/**
	 * Returns the meta object for the containment reference '{@link FixPtSpecification.FixPtType#getDyn <em>Dyn</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Dyn</em>'.
	 * @see FixPtSpecification.FixPtType#getDyn()
	 * @see #getFixPtType()
	 * @generated
	 */
	EReference getFixPtType_Dyn();

	/**
	 * Returns the meta object for class '{@link FixPtSpecification.TypeAcFixed <em>Type Ac Fixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Ac Fixed</em>'.
	 * @see FixPtSpecification.TypeAcFixed
	 * @generated
	 */
	EClass getTypeAcFixed();

	/**
	 * Returns the meta object for the attribute '{@link FixPtSpecification.TypeAcFixed#getWd <em>Wd</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wd</em>'.
	 * @see FixPtSpecification.TypeAcFixed#getWd()
	 * @see #getTypeAcFixed()
	 * @generated
	 */
	EAttribute getTypeAcFixed_Wd();

	/**
	 * Returns the meta object for the attribute '{@link FixPtSpecification.TypeAcFixed#getWip <em>Wip</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wip</em>'.
	 * @see FixPtSpecification.TypeAcFixed#getWip()
	 * @see #getTypeAcFixed()
	 * @generated
	 */
	EAttribute getTypeAcFixed_Wip();

	/**
	 * Returns the meta object for the attribute '{@link FixPtSpecification.TypeAcFixed#isS <em>S</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>S</em>'.
	 * @see FixPtSpecification.TypeAcFixed#isS()
	 * @see #getTypeAcFixed()
	 * @generated
	 */
	EAttribute getTypeAcFixed_S();

	/**
	 * Returns the meta object for the attribute '{@link FixPtSpecification.TypeAcFixed#getQ <em>Q</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Q</em>'.
	 * @see FixPtSpecification.TypeAcFixed#getQ()
	 * @see #getTypeAcFixed()
	 * @generated
	 */
	EAttribute getTypeAcFixed_Q();

	/**
	 * Returns the meta object for the attribute '{@link FixPtSpecification.TypeAcFixed#getO <em>O</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>O</em>'.
	 * @see FixPtSpecification.TypeAcFixed#getO()
	 * @see #getTypeAcFixed()
	 * @generated
	 */
	EAttribute getTypeAcFixed_O();

	/**
	 * Returns the meta object for class '{@link FixPtSpecification.Dynamic <em>Dynamic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dynamic</em>'.
	 * @see FixPtSpecification.Dynamic
	 * @generated
	 */
	EClass getDynamic();

	/**
	 * Returns the meta object for the attribute '{@link FixPtSpecification.Dynamic#getLowerBound <em>Lower Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lower Bound</em>'.
	 * @see FixPtSpecification.Dynamic#getLowerBound()
	 * @see #getDynamic()
	 * @generated
	 */
	EAttribute getDynamic_LowerBound();

	/**
	 * Returns the meta object for the attribute '{@link FixPtSpecification.Dynamic#getUpperBound <em>Upper Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Upper Bound</em>'.
	 * @see FixPtSpecification.Dynamic#getUpperBound()
	 * @see #getDynamic()
	 * @generated
	 */
	EAttribute getDynamic_UpperBound();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>String To Fix Pt Type Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String To Fix Pt Type Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="FixPtSpecification.DataId" keyContainment="true"
	 *        valueType="FixPtSpecification.FixPtType" valueContainment="true"
	 * @generated
	 */
	EClass getStringToFixPtTypeMapEntry();

	/**
	 * Returns the meta object for the containment reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToFixPtTypeMapEntry()
	 * @generated
	 */
	EReference getStringToFixPtTypeMapEntry_Key();

	/**
	 * Returns the meta object for the containment reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToFixPtTypeMapEntry()
	 * @generated
	 */
	EReference getStringToFixPtTypeMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link FixPtSpecification.DataId <em>Data Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Id</em>'.
	 * @see FixPtSpecification.DataId
	 * @generated
	 */
	EClass getDataId();

	/**
	 * Returns the meta object for the attribute '{@link FixPtSpecification.DataId#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see FixPtSpecification.DataId#getName()
	 * @see #getDataId()
	 * @generated
	 */
	EAttribute getDataId_Name();

	/**
	 * Returns the meta object for the attribute '{@link FixPtSpecification.DataId#getNum <em>Num</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num</em>'.
	 * @see FixPtSpecification.DataId#getNum()
	 * @see #getDataId()
	 * @generated
	 */
	EAttribute getDataId_Num();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FixPtSpecificationFactory getFixPtSpecificationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link FixPtSpecification.impl.FixPtSpecifImpl <em>Fix Pt Specif</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FixPtSpecification.impl.FixPtSpecifImpl
		 * @see FixPtSpecification.impl.FixPtSpecificationPackageImpl#getFixPtSpecif()
		 * @generated
		 */
		EClass FIX_PT_SPECIF = eINSTANCE.getFixPtSpecif();

		/**
		 * The meta object literal for the '<em><b>List Fix Pt Ops Specif</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIX_PT_SPECIF__LIST_FIX_PT_OPS_SPECIF = eINSTANCE.getFixPtSpecif_ListFixPtOpsSpecif();

		/**
		 * The meta object literal for the '<em><b>List Fix Pt Type</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIX_PT_SPECIF__LIST_FIX_PT_TYPE = eINSTANCE.getFixPtSpecif_ListFixPtType();

		/**
		 * The meta object literal for the '{@link FixPtSpecification.impl.FixPtOpsSpecifImpl <em>Fix Pt Ops Specif</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FixPtSpecification.impl.FixPtOpsSpecifImpl
		 * @see FixPtSpecification.impl.FixPtSpecificationPackageImpl#getFixPtOpsSpecif()
		 * @generated
		 */
		EClass FIX_PT_OPS_SPECIF = eINSTANCE.getFixPtOpsSpecif();

		/**
		 * The meta object literal for the '<em><b>Input1</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIX_PT_OPS_SPECIF__INPUT1 = eINSTANCE.getFixPtOpsSpecif_Input1();

		/**
		 * The meta object literal for the '<em><b>Input2</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIX_PT_OPS_SPECIF__INPUT2 = eINSTANCE.getFixPtOpsSpecif_Input2();

		/**
		 * The meta object literal for the '<em><b>Output</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIX_PT_OPS_SPECIF__OUTPUT = eINSTANCE.getFixPtOpsSpecif_Output();

		/**
		 * The meta object literal for the '<em><b>Num Op</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIX_PT_OPS_SPECIF__NUM_OP = eINSTANCE.getFixPtOpsSpecif_NumOp();

		/**
		 * The meta object literal for the '<em><b>Name Op</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIX_PT_OPS_SPECIF__NAME_OP = eINSTANCE.getFixPtOpsSpecif_NameOp();

		/**
		 * The meta object literal for the '{@link FixPtSpecification.impl.FixPtTypeImpl <em>Fix Pt Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FixPtSpecification.impl.FixPtTypeImpl
		 * @see FixPtSpecification.impl.FixPtSpecificationPackageImpl#getFixPtType()
		 * @generated
		 */
		EClass FIX_PT_TYPE = eINSTANCE.getFixPtType();

		/**
		 * The meta object literal for the '<em><b>Type Ac Fixed</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIX_PT_TYPE__TYPE_AC_FIXED = eINSTANCE.getFixPtType_TypeAcFixed();

		/**
		 * The meta object literal for the '<em><b>Dyn</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FIX_PT_TYPE__DYN = eINSTANCE.getFixPtType_Dyn();

		/**
		 * The meta object literal for the '{@link FixPtSpecification.impl.TypeAcFixedImpl <em>Type Ac Fixed</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FixPtSpecification.impl.TypeAcFixedImpl
		 * @see FixPtSpecification.impl.FixPtSpecificationPackageImpl#getTypeAcFixed()
		 * @generated
		 */
		EClass TYPE_AC_FIXED = eINSTANCE.getTypeAcFixed();

		/**
		 * The meta object literal for the '<em><b>Wd</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TYPE_AC_FIXED__WD = eINSTANCE.getTypeAcFixed_Wd();

		/**
		 * The meta object literal for the '<em><b>Wip</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TYPE_AC_FIXED__WIP = eINSTANCE.getTypeAcFixed_Wip();

		/**
		 * The meta object literal for the '<em><b>S</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TYPE_AC_FIXED__S = eINSTANCE.getTypeAcFixed_S();

		/**
		 * The meta object literal for the '<em><b>Q</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TYPE_AC_FIXED__Q = eINSTANCE.getTypeAcFixed_Q();

		/**
		 * The meta object literal for the '<em><b>O</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TYPE_AC_FIXED__O = eINSTANCE.getTypeAcFixed_O();

		/**
		 * The meta object literal for the '{@link FixPtSpecification.impl.DynamicImpl <em>Dynamic</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FixPtSpecification.impl.DynamicImpl
		 * @see FixPtSpecification.impl.FixPtSpecificationPackageImpl#getDynamic()
		 * @generated
		 */
		EClass DYNAMIC = eINSTANCE.getDynamic();

		/**
		 * The meta object literal for the '<em><b>Lower Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DYNAMIC__LOWER_BOUND = eINSTANCE.getDynamic_LowerBound();

		/**
		 * The meta object literal for the '<em><b>Upper Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DYNAMIC__UPPER_BOUND = eINSTANCE.getDynamic_UpperBound();

		/**
		 * The meta object literal for the '{@link FixPtSpecification.impl.StringToFixPtTypeMapEntryImpl <em>String To Fix Pt Type Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FixPtSpecification.impl.StringToFixPtTypeMapEntryImpl
		 * @see FixPtSpecification.impl.FixPtSpecificationPackageImpl#getStringToFixPtTypeMapEntry()
		 * @generated
		 */
		EClass STRING_TO_FIX_PT_TYPE_MAP_ENTRY = eINSTANCE.getStringToFixPtTypeMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRING_TO_FIX_PT_TYPE_MAP_ENTRY__KEY = eINSTANCE.getStringToFixPtTypeMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRING_TO_FIX_PT_TYPE_MAP_ENTRY__VALUE = eINSTANCE.getStringToFixPtTypeMapEntry_Value();

		/**
		 * The meta object literal for the '{@link FixPtSpecification.impl.DataIdImpl <em>Data Id</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FixPtSpecification.impl.DataIdImpl
		 * @see FixPtSpecification.impl.FixPtSpecificationPackageImpl#getDataId()
		 * @generated
		 */
		EClass DATA_ID = eINSTANCE.getDataId();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_ID__NAME = eINSTANCE.getDataId_Name();

		/**
		 * The meta object literal for the '<em><b>Num</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_ID__NUM = eINSTANCE.getDataId_Num();

	}

} //FixPtSpecificationPackage
