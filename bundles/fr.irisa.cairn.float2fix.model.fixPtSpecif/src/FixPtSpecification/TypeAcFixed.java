/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package FixPtSpecification;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Ac Fixed</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link FixPtSpecification.TypeAcFixed#getWd <em>Wd</em>}</li>
 *   <li>{@link FixPtSpecification.TypeAcFixed#getWip <em>Wip</em>}</li>
 *   <li>{@link FixPtSpecification.TypeAcFixed#isS <em>S</em>}</li>
 *   <li>{@link FixPtSpecification.TypeAcFixed#getQ <em>Q</em>}</li>
 *   <li>{@link FixPtSpecification.TypeAcFixed#getO <em>O</em>}</li>
 * </ul>
 * </p>
 *
 * @see FixPtSpecification.FixPtSpecificationPackage#getTypeAcFixed()
 * @model
 * @generated
 */
public interface TypeAcFixed extends EObject {
	/**
	 * Returns the value of the '<em><b>Wd</b></em>' attribute.
	 * The default value is <code>"-9999"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wd</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wd</em>' attribute.
	 * @see #setWd(int)
	 * @see FixPtSpecification.FixPtSpecificationPackage#getTypeAcFixed_Wd()
	 * @model default="-9999" required="true"
	 * @generated
	 */
	int getWd();

	/**
	 * Sets the value of the '{@link FixPtSpecification.TypeAcFixed#getWd <em>Wd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wd</em>' attribute.
	 * @see #getWd()
	 * @generated
	 */
	void setWd(int value);

	/**
	 * Returns the value of the '<em><b>Wip</b></em>' attribute.
	 * The default value is <code>"-9999"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wip</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wip</em>' attribute.
	 * @see #setWip(int)
	 * @see FixPtSpecification.FixPtSpecificationPackage#getTypeAcFixed_Wip()
	 * @model default="-9999" required="true"
	 * @generated
	 */
	int getWip();

	/**
	 * Sets the value of the '{@link FixPtSpecification.TypeAcFixed#getWip <em>Wip</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wip</em>' attribute.
	 * @see #getWip()
	 * @generated
	 */
	void setWip(int value);

	/**
	 * Returns the value of the '<em><b>S</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>S</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>S</em>' attribute.
	 * @see #setS(boolean)
	 * @see FixPtSpecification.FixPtSpecificationPackage#getTypeAcFixed_S()
	 * @model default="true" required="true"
	 * @generated
	 */
	boolean isS();

	/**
	 * Sets the value of the '{@link FixPtSpecification.TypeAcFixed#isS <em>S</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>S</em>' attribute.
	 * @see #isS()
	 * @generated
	 */
	void setS(boolean value);

	/**
	 * Returns the value of the '<em><b>Q</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Q</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Q</em>' attribute.
	 * @see #setQ(String)
	 * @see FixPtSpecification.FixPtSpecificationPackage#getTypeAcFixed_Q()
	 * @model required="true"
	 * @generated
	 */
	String getQ();

	/**
	 * Sets the value of the '{@link FixPtSpecification.TypeAcFixed#getQ <em>Q</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Q</em>' attribute.
	 * @see #getQ()
	 * @generated
	 */
	void setQ(String value);

	/**
	 * Returns the value of the '<em><b>O</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>O</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>O</em>' attribute.
	 * @see #setO(String)
	 * @see FixPtSpecification.FixPtSpecificationPackage#getTypeAcFixed_O()
	 * @model required="true"
	 * @generated
	 */
	String getO();

	/**
	 * Sets the value of the '{@link FixPtSpecification.TypeAcFixed#getO <em>O</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>O</em>' attribute.
	 * @see #getO()
	 * @generated
	 */
	void setO(String value);
	
	public TypeAcFixed copy();

} // TypeAcFixed
