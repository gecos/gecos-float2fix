/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package FixPtSpecification;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dynamic</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link FixPtSpecification.Dynamic#getLowerBound <em>Lower Bound</em>}</li>
 *   <li>{@link FixPtSpecification.Dynamic#getUpperBound <em>Upper Bound</em>}</li>
 * </ul>
 * </p>
 *
 * @see FixPtSpecification.FixPtSpecificationPackage#getDynamic()
 * @model
 * @generated
 */
public interface Dynamic extends EObject {
	/**
	 * Returns the value of the '<em><b>Lower Bound</b></em>' attribute.
	 * The default value is <code>"9999"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lower Bound</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lower Bound</em>' attribute.
	 * @see #setLowerBound(double)
	 * @see FixPtSpecification.FixPtSpecificationPackage#getDynamic_LowerBound()
	 * @model default="9999" required="true"
	 * @generated
	 */
	double getLowerBound();

	/**
	 * Sets the value of the '{@link FixPtSpecification.Dynamic#getLowerBound <em>Lower Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lower Bound</em>' attribute.
	 * @see #getLowerBound()
	 * @generated
	 */
	void setLowerBound(double value);

	/**
	 * Returns the value of the '<em><b>Upper Bound</b></em>' attribute.
	 * The default value is <code>"-9999"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Upper Bound</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Upper Bound</em>' attribute.
	 * @see #setUpperBound(double)
	 * @see FixPtSpecification.FixPtSpecificationPackage#getDynamic_UpperBound()
	 * @model default="-9999" required="true"
	 * @generated
	 */
	double getUpperBound();

	/**
	 * Sets the value of the '{@link FixPtSpecification.Dynamic#getUpperBound <em>Upper Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Upper Bound</em>' attribute.
	 * @see #getUpperBound()
	 * @generated
	 */
	void setUpperBound(double value);

} // Dynamic
