/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package FixPtSpecification;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see FixPtSpecification.FixPtSpecificationPackage
 * @generated
 */
public interface FixPtSpecificationFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FixPtSpecificationFactory eINSTANCE = FixPtSpecification.impl.FixPtSpecificationFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Fix Pt Specif</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fix Pt Specif</em>'.
	 * @generated
	 */
	FixPtSpecif createFixPtSpecif();

	/**
	 * Returns a new object of class '<em>Fix Pt Ops Specif</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fix Pt Ops Specif</em>'.
	 * @generated
	 */
	FixPtOpsSpecif createFixPtOpsSpecif();

	/**
	 * Returns a new object of class '<em>Fix Pt Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fix Pt Type</em>'.
	 * @generated
	 */
	FixPtType createFixPtType();

	/**
	 * Returns a new object of class '<em>Type Ac Fixed</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Type Ac Fixed</em>'.
	 * @generated
	 */
	TypeAcFixed createTypeAcFixed();

	/**
	 * Returns a new object of class '<em>Dynamic</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dynamic</em>'.
	 * @generated
	 */
	Dynamic createDynamic();

	/**
	 * Returns a new object of class '<em>Data Id</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Id</em>'.
	 * @generated
	 */
	DataId createDataId();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	FixPtSpecificationPackage getFixPtSpecificationPackage();

} //FixPtSpecificationFactory
