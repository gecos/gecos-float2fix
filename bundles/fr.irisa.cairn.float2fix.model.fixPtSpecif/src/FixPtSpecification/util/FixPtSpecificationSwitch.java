/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package FixPtSpecification.util;

import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import FixPtSpecification.DataId;
import FixPtSpecification.Dynamic;
import FixPtSpecification.FixPtOpsSpecif;
import FixPtSpecification.FixPtSpecif;
import FixPtSpecification.FixPtSpecificationPackage;
import FixPtSpecification.FixPtType;
import FixPtSpecification.TypeAcFixed;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see FixPtSpecification.FixPtSpecificationPackage
 * @generated
 */
public class FixPtSpecificationSwitch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static FixPtSpecificationPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixPtSpecificationSwitch() {
		if (modelPackage == null) {
			modelPackage = FixPtSpecificationPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case FixPtSpecificationPackage.FIX_PT_SPECIF: {
				FixPtSpecif fixPtSpecif = (FixPtSpecif)theEObject;
				T result = caseFixPtSpecif(fixPtSpecif);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF: {
				FixPtOpsSpecif fixPtOpsSpecif = (FixPtOpsSpecif)theEObject;
				T result = caseFixPtOpsSpecif(fixPtOpsSpecif);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FixPtSpecificationPackage.FIX_PT_TYPE: {
				FixPtType fixPtType = (FixPtType)theEObject;
				T result = caseFixPtType(fixPtType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FixPtSpecificationPackage.TYPE_AC_FIXED: {
				TypeAcFixed typeAcFixed = (TypeAcFixed)theEObject;
				T result = caseTypeAcFixed(typeAcFixed);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FixPtSpecificationPackage.DYNAMIC: {
				Dynamic dynamic = (Dynamic)theEObject;
				T result = caseDynamic(dynamic);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FixPtSpecificationPackage.STRING_TO_FIX_PT_TYPE_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<DataId, FixPtType> stringToFixPtTypeMapEntry = (Map.Entry<DataId, FixPtType>)theEObject;
				T result = caseStringToFixPtTypeMapEntry(stringToFixPtTypeMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FixPtSpecificationPackage.DATA_ID: {
				DataId dataId = (DataId)theEObject;
				T result = caseDataId(dataId);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fix Pt Specif</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fix Pt Specif</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFixPtSpecif(FixPtSpecif object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fix Pt Ops Specif</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fix Pt Ops Specif</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFixPtOpsSpecif(FixPtOpsSpecif object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fix Pt Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fix Pt Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFixPtType(FixPtType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type Ac Fixed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Ac Fixed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypeAcFixed(TypeAcFixed object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dynamic</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dynamic</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDynamic(Dynamic object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String To Fix Pt Type Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String To Fix Pt Type Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStringToFixPtTypeMapEntry(Map.Entry<DataId, FixPtType> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Id</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Id</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataId(DataId object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object) {
		return null;
	}

} //FixPtSpecificationSwitch
