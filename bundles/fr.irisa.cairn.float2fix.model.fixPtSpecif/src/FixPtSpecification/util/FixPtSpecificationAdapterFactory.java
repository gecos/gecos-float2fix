/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package FixPtSpecification.util;

import java.util.Map;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

import FixPtSpecification.DataId;
import FixPtSpecification.Dynamic;
import FixPtSpecification.FixPtOpsSpecif;
import FixPtSpecification.FixPtSpecif;
import FixPtSpecification.FixPtSpecificationPackage;
import FixPtSpecification.FixPtType;
import FixPtSpecification.TypeAcFixed;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see FixPtSpecification.FixPtSpecificationPackage
 * @generated
 */
public class FixPtSpecificationAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static FixPtSpecificationPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixPtSpecificationAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = FixPtSpecificationPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FixPtSpecificationSwitch<Adapter> modelSwitch =
		new FixPtSpecificationSwitch<Adapter>() {
			@Override
			public Adapter caseFixPtSpecif(FixPtSpecif object) {
				return createFixPtSpecifAdapter();
			}
			@Override
			public Adapter caseFixPtOpsSpecif(FixPtOpsSpecif object) {
				return createFixPtOpsSpecifAdapter();
			}
			@Override
			public Adapter caseFixPtType(FixPtType object) {
				return createFixPtTypeAdapter();
			}
			@Override
			public Adapter caseTypeAcFixed(TypeAcFixed object) {
				return createTypeAcFixedAdapter();
			}
			@Override
			public Adapter caseDynamic(Dynamic object) {
				return createDynamicAdapter();
			}
			@Override
			public Adapter caseStringToFixPtTypeMapEntry(Map.Entry<DataId, FixPtType> object) {
				return createStringToFixPtTypeMapEntryAdapter();
			}
			@Override
			public Adapter caseDataId(DataId object) {
				return createDataIdAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link FixPtSpecification.FixPtSpecif <em>Fix Pt Specif</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FixPtSpecification.FixPtSpecif
	 * @generated
	 */
	public Adapter createFixPtSpecifAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link FixPtSpecification.FixPtOpsSpecif <em>Fix Pt Ops Specif</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FixPtSpecification.FixPtOpsSpecif
	 * @generated
	 */
	public Adapter createFixPtOpsSpecifAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link FixPtSpecification.FixPtType <em>Fix Pt Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FixPtSpecification.FixPtType
	 * @generated
	 */
	public Adapter createFixPtTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link FixPtSpecification.TypeAcFixed <em>Type Ac Fixed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FixPtSpecification.TypeAcFixed
	 * @generated
	 */
	public Adapter createTypeAcFixedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link FixPtSpecification.Dynamic <em>Dynamic</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FixPtSpecification.Dynamic
	 * @generated
	 */
	public Adapter createDynamicAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>String To Fix Pt Type Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createStringToFixPtTypeMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link FixPtSpecification.DataId <em>Data Id</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FixPtSpecification.DataId
	 * @generated
	 */
	public Adapter createDataIdAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //FixPtSpecificationAdapterFactory
