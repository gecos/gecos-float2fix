/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package FixPtSpecification;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Id</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link FixPtSpecification.DataId#getName <em>Name</em>}</li>
 *   <li>{@link FixPtSpecification.DataId#getNum <em>Num</em>}</li>
 * </ul>
 * </p>
 *
 * @see FixPtSpecification.FixPtSpecificationPackage#getDataId()
 * @model
 * @generated
 */
public interface DataId extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see FixPtSpecification.FixPtSpecificationPackage#getDataId_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link FixPtSpecification.DataId#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Num</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Num</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Num</em>' attribute.
	 * @see #setNum(String)
	 * @see FixPtSpecification.FixPtSpecificationPackage#getDataId_Num()
	 * @model required="true"
	 * @generated
	 */
	String getNum();

	/**
	 * Sets the value of the '{@link FixPtSpecification.DataId#getNum <em>Num</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Num</em>' attribute.
	 * @see #getNum()
	 * @generated
	 */
	void setNum(String value);

} // DataId
