/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package FixPtSpecification.impl;


import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;

import FixPtSpecification.Dynamic;
import FixPtSpecification.FixPtSpecificationFactory;
import FixPtSpecification.FixPtSpecificationPackage;
import FixPtSpecification.FixPtType;
import FixPtSpecification.TypeAcFixed;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fix Pt Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link FixPtSpecification.impl.FixPtTypeImpl#getTypeAcFixed <em>Type Ac Fixed</em>}</li>
 *   <li>{@link FixPtSpecification.impl.FixPtTypeImpl#getDyn <em>Dyn</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class FixPtTypeImpl extends EObjectImpl implements FixPtType {
	/**
	 * The cached value of the '{@link #getTypeAcFixed() <em>Type Ac Fixed</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeAcFixed()
	 * @generated
	 * @ordered
	 */
	protected TypeAcFixed typeAcFixed;

	/**
	 * The cached value of the '{@link #getDyn() <em>Dyn</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDyn()
	 * @generated
	 * @ordered
	 */
	protected Dynamic dyn;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	static final boolean EMFCopy=false;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FixPtTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FixPtSpecificationPackage.Literals.FIX_PT_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeAcFixed getTypeAcFixed() {
		return typeAcFixed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTypeAcFixed(TypeAcFixed newTypeAcFixed, NotificationChain msgs) {
		TypeAcFixed oldTypeAcFixed = typeAcFixed;
		typeAcFixed = newTypeAcFixed;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FixPtSpecificationPackage.FIX_PT_TYPE__TYPE_AC_FIXED, oldTypeAcFixed, newTypeAcFixed);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypeAcFixed(TypeAcFixed newTypeAcFixed) {
		if (newTypeAcFixed != typeAcFixed) {
			NotificationChain msgs = null;
			if (typeAcFixed != null)
				msgs = ((InternalEObject)typeAcFixed).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FixPtSpecificationPackage.FIX_PT_TYPE__TYPE_AC_FIXED, null, msgs);
			if (newTypeAcFixed != null)
				msgs = ((InternalEObject)newTypeAcFixed).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FixPtSpecificationPackage.FIX_PT_TYPE__TYPE_AC_FIXED, null, msgs);
			msgs = basicSetTypeAcFixed(newTypeAcFixed, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixPtSpecificationPackage.FIX_PT_TYPE__TYPE_AC_FIXED, newTypeAcFixed, newTypeAcFixed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dynamic getDyn() {
		return dyn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDyn(Dynamic newDyn, NotificationChain msgs) {
		Dynamic oldDyn = dyn;
		dyn = newDyn;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FixPtSpecificationPackage.FIX_PT_TYPE__DYN, oldDyn, newDyn);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDyn(Dynamic newDyn) {
		if (newDyn != dyn) {
			NotificationChain msgs = null;
			if (dyn != null)
				msgs = ((InternalEObject)dyn).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FixPtSpecificationPackage.FIX_PT_TYPE__DYN, null, msgs);
			if (newDyn != null)
				msgs = ((InternalEObject)newDyn).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FixPtSpecificationPackage.FIX_PT_TYPE__DYN, null, msgs);
			msgs = basicSetDyn(newDyn, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixPtSpecificationPackage.FIX_PT_TYPE__DYN, newDyn, newDyn));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FixPtSpecificationPackage.FIX_PT_TYPE__TYPE_AC_FIXED:
				return basicSetTypeAcFixed(null, msgs);
			case FixPtSpecificationPackage.FIX_PT_TYPE__DYN:
				return basicSetDyn(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FixPtSpecificationPackage.FIX_PT_TYPE__TYPE_AC_FIXED:
				return getTypeAcFixed();
			case FixPtSpecificationPackage.FIX_PT_TYPE__DYN:
				return getDyn();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FixPtSpecificationPackage.FIX_PT_TYPE__TYPE_AC_FIXED:
				setTypeAcFixed((TypeAcFixed)newValue);
				return;
			case FixPtSpecificationPackage.FIX_PT_TYPE__DYN:
				setDyn((Dynamic)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FixPtSpecificationPackage.FIX_PT_TYPE__TYPE_AC_FIXED:
				setTypeAcFixed((TypeAcFixed)null);
				return;
			case FixPtSpecificationPackage.FIX_PT_TYPE__DYN:
				setDyn((Dynamic)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FixPtSpecificationPackage.FIX_PT_TYPE__TYPE_AC_FIXED:
				return typeAcFixed != null;
			case FixPtSpecificationPackage.FIX_PT_TYPE__DYN:
				return dyn != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */

	public FixPtType copy()
	{
		if (EMFCopy) {
			Copier copier = new Copier();
			FixPtType fixType=(FixPtType)copier.copy(this);
			copier.copyReferences();
			return fixType;
		} else {
			FixPtType fixTypeCopy=FixPtSpecificationFactory.eINSTANCE.createFixPtType();
			TypeAcFixed acfixTypeCopy=this.getTypeAcFixed().copy();
			fixTypeCopy.setTypeAcFixed(acfixTypeCopy);
			Dynamic dyn=FixPtSpecificationFactory.eINSTANCE.createDynamic();
			if(this.getDyn()!=null)
			{
				dyn.setLowerBound(this.getDyn().getLowerBound());
				dyn.setUpperBound(this.getDyn().getUpperBound());
				fixTypeCopy.setDyn(dyn);
			}
			return fixTypeCopy;
		}
	}

	@Override
	public String toString() {
		return "FixPtTypeImpl [typeAcFixed=" + typeAcFixed + ", dyn=" + dyn
				+ "]";
	}


} //FixPtTypeImpl
