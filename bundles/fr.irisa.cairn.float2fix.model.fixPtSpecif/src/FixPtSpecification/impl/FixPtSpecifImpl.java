/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package FixPtSpecification.impl;

import java.util.Collection;
import java.util.Map.Entry;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

import FixPtSpecification.DataId;
import FixPtSpecification.FixPtOpsSpecif;
import FixPtSpecification.FixPtSpecif;
import FixPtSpecification.FixPtSpecificationPackage;
import FixPtSpecification.FixPtType;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Fix Pt Specif</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link FixPtSpecification.impl.FixPtSpecifImpl#getListFixPtOpsSpecif <em>
 * List Fix Pt Ops Specif</em>}</li>
 * <li>{@link FixPtSpecification.impl.FixPtSpecifImpl#getListFixPtType <em>List
 * Fix Pt Type</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class FixPtSpecifImpl extends EObjectImpl implements FixPtSpecif {
	/**
	 * The cached value of the '{@link #getListFixPtOpsSpecif()
	 * <em>List Fix Pt Ops Specif</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getListFixPtOpsSpecif()
	 * @generated
	 * @ordered
	 */
	protected EList<FixPtOpsSpecif> listFixPtOpsSpecif;

	/**
	 * The cached value of the '{@link #getListFixPtType()
	 * <em>List Fix Pt Type</em>}' map. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getListFixPtType()
	 * @generated
	 * @ordered
	 */
	protected EMap<DataId, FixPtType> listFixPtType;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected FixPtSpecifImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FixPtSpecificationPackage.Literals.FIX_PT_SPECIF;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<FixPtOpsSpecif> getListFixPtOpsSpecif() {
		if (listFixPtOpsSpecif == null) {
			listFixPtOpsSpecif = new EObjectContainmentEList<FixPtOpsSpecif>(
					FixPtOpsSpecif.class,
					this,
					FixPtSpecificationPackage.FIX_PT_SPECIF__LIST_FIX_PT_OPS_SPECIF);
		}
		return listFixPtOpsSpecif;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EMap<DataId, FixPtType> getListFixPtType() {
		if (listFixPtType == null) {
			listFixPtType = new EcoreEMap<DataId, FixPtType>(
					FixPtSpecificationPackage.Literals.STRING_TO_FIX_PT_TYPE_MAP_ENTRY,
					StringToFixPtTypeMapEntryImpl.class, this,
					FixPtSpecificationPackage.FIX_PT_SPECIF__LIST_FIX_PT_TYPE);
		}
		return listFixPtType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FixPtSpecificationPackage.FIX_PT_SPECIF__LIST_FIX_PT_OPS_SPECIF:
			return ((InternalEList<?>) getListFixPtOpsSpecif()).basicRemove(
					otherEnd, msgs);
		case FixPtSpecificationPackage.FIX_PT_SPECIF__LIST_FIX_PT_TYPE:
			return ((InternalEList<?>) getListFixPtType()).basicRemove(
					otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FixPtSpecificationPackage.FIX_PT_SPECIF__LIST_FIX_PT_OPS_SPECIF:
			return getListFixPtOpsSpecif();
		case FixPtSpecificationPackage.FIX_PT_SPECIF__LIST_FIX_PT_TYPE:
			if (coreType)
				return getListFixPtType();
			else
				return getListFixPtType().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FixPtSpecificationPackage.FIX_PT_SPECIF__LIST_FIX_PT_OPS_SPECIF:
			getListFixPtOpsSpecif().clear();
			getListFixPtOpsSpecif().addAll(
					(Collection<? extends FixPtOpsSpecif>) newValue);
			return;
		case FixPtSpecificationPackage.FIX_PT_SPECIF__LIST_FIX_PT_TYPE:
			((EStructuralFeature.Setting) getListFixPtType()).set(newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FixPtSpecificationPackage.FIX_PT_SPECIF__LIST_FIX_PT_OPS_SPECIF:
			getListFixPtOpsSpecif().clear();
			return;
		case FixPtSpecificationPackage.FIX_PT_SPECIF__LIST_FIX_PT_TYPE:
			getListFixPtType().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FixPtSpecificationPackage.FIX_PT_SPECIF__LIST_FIX_PT_OPS_SPECIF:
			return listFixPtOpsSpecif != null && !listFixPtOpsSpecif.isEmpty();
		case FixPtSpecificationPackage.FIX_PT_SPECIF__LIST_FIX_PT_TYPE:
			return listFixPtType != null && !listFixPtType.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	@Override
	public String toString() {
		String result = "FixPtSpecifImpl [listFixPtOpsSpecif=";
		for (FixPtOpsSpecif op : listFixPtOpsSpecif) {
			result += op +"\n";
		}
		result += ", listFixPtType=";
		for(Entry<DataId, FixPtType> type : listFixPtType){
			 result += type +"\n";
		}
		result += "]";
		return result;
	}
} // FixPtSpecifImpl
