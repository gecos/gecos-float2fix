/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package FixPtSpecification.impl;

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import FixPtSpecification.DataId;
import FixPtSpecification.FixPtOpsSpecif;
import FixPtSpecification.FixPtSpecif;
import FixPtSpecification.FixPtSpecificationFactory;
import FixPtSpecification.FixPtSpecificationPackage;
import FixPtSpecification.FixPtType;
import FixPtSpecification.TypeAcFixed;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FixPtSpecificationPackageImpl extends EPackageImpl implements FixPtSpecificationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fixPtSpecifEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fixPtOpsSpecifEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fixPtTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typeAcFixedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dynamicEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringToFixPtTypeMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataIdEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see FixPtSpecification.FixPtSpecificationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private FixPtSpecificationPackageImpl() {
		super(eNS_URI, FixPtSpecificationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link FixPtSpecificationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static FixPtSpecificationPackage init() {
		if (isInited) return (FixPtSpecificationPackage)EPackage.Registry.INSTANCE.getEPackage(FixPtSpecificationPackage.eNS_URI);

		// Obtain or create and register package
		FixPtSpecificationPackageImpl theFixPtSpecificationPackage = (FixPtSpecificationPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof FixPtSpecificationPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new FixPtSpecificationPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theFixPtSpecificationPackage.createPackageContents();

		// Initialize created meta-data
		theFixPtSpecificationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theFixPtSpecificationPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(FixPtSpecificationPackage.eNS_URI, theFixPtSpecificationPackage);
		return theFixPtSpecificationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFixPtSpecif() {
		return fixPtSpecifEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFixPtSpecif_ListFixPtOpsSpecif() {
		return (EReference)fixPtSpecifEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFixPtSpecif_ListFixPtType() {
		return (EReference)fixPtSpecifEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFixPtOpsSpecif() {
		return fixPtOpsSpecifEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFixPtOpsSpecif_Input1() {
		return (EReference)fixPtOpsSpecifEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFixPtOpsSpecif_Input2() {
		return (EReference)fixPtOpsSpecifEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFixPtOpsSpecif_Output() {
		return (EReference)fixPtOpsSpecifEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixPtOpsSpecif_NumOp() {
		return (EAttribute)fixPtOpsSpecifEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixPtOpsSpecif_NameOp() {
		return (EAttribute)fixPtOpsSpecifEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFixPtType() {
		return fixPtTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFixPtType_TypeAcFixed() {
		return (EReference)fixPtTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFixPtType_Dyn() {
		return (EReference)fixPtTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTypeAcFixed() {
		return typeAcFixedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTypeAcFixed_Wd() {
		return (EAttribute)typeAcFixedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTypeAcFixed_Wip() {
		return (EAttribute)typeAcFixedEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTypeAcFixed_S() {
		return (EAttribute)typeAcFixedEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTypeAcFixed_Q() {
		return (EAttribute)typeAcFixedEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTypeAcFixed_O() {
		return (EAttribute)typeAcFixedEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDynamic() {
		return dynamicEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDynamic_LowerBound() {
		return (EAttribute)dynamicEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDynamic_UpperBound() {
		return (EAttribute)dynamicEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringToFixPtTypeMapEntry() {
		return stringToFixPtTypeMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStringToFixPtTypeMapEntry_Key() {
		return (EReference)stringToFixPtTypeMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStringToFixPtTypeMapEntry_Value() {
		return (EReference)stringToFixPtTypeMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataId() {
		return dataIdEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataId_Name() {
		return (EAttribute)dataIdEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataId_Num() {
		return (EAttribute)dataIdEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixPtSpecificationFactory getFixPtSpecificationFactory() {
		return (FixPtSpecificationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		fixPtSpecifEClass = createEClass(FIX_PT_SPECIF);
		createEReference(fixPtSpecifEClass, FIX_PT_SPECIF__LIST_FIX_PT_OPS_SPECIF);
		createEReference(fixPtSpecifEClass, FIX_PT_SPECIF__LIST_FIX_PT_TYPE);

		fixPtOpsSpecifEClass = createEClass(FIX_PT_OPS_SPECIF);
		createEReference(fixPtOpsSpecifEClass, FIX_PT_OPS_SPECIF__INPUT1);
		createEReference(fixPtOpsSpecifEClass, FIX_PT_OPS_SPECIF__INPUT2);
		createEReference(fixPtOpsSpecifEClass, FIX_PT_OPS_SPECIF__OUTPUT);
		createEAttribute(fixPtOpsSpecifEClass, FIX_PT_OPS_SPECIF__NUM_OP);
		createEAttribute(fixPtOpsSpecifEClass, FIX_PT_OPS_SPECIF__NAME_OP);

		fixPtTypeEClass = createEClass(FIX_PT_TYPE);
		createEReference(fixPtTypeEClass, FIX_PT_TYPE__TYPE_AC_FIXED);
		createEReference(fixPtTypeEClass, FIX_PT_TYPE__DYN);

		typeAcFixedEClass = createEClass(TYPE_AC_FIXED);
		createEAttribute(typeAcFixedEClass, TYPE_AC_FIXED__WD);
		createEAttribute(typeAcFixedEClass, TYPE_AC_FIXED__WIP);
		createEAttribute(typeAcFixedEClass, TYPE_AC_FIXED__S);
		createEAttribute(typeAcFixedEClass, TYPE_AC_FIXED__Q);
		createEAttribute(typeAcFixedEClass, TYPE_AC_FIXED__O);

		dynamicEClass = createEClass(DYNAMIC);
		createEAttribute(dynamicEClass, DYNAMIC__LOWER_BOUND);
		createEAttribute(dynamicEClass, DYNAMIC__UPPER_BOUND);

		stringToFixPtTypeMapEntryEClass = createEClass(STRING_TO_FIX_PT_TYPE_MAP_ENTRY);
		createEReference(stringToFixPtTypeMapEntryEClass, STRING_TO_FIX_PT_TYPE_MAP_ENTRY__KEY);
		createEReference(stringToFixPtTypeMapEntryEClass, STRING_TO_FIX_PT_TYPE_MAP_ENTRY__VALUE);

		dataIdEClass = createEClass(DATA_ID);
		createEAttribute(dataIdEClass, DATA_ID__NAME);
		createEAttribute(dataIdEClass, DATA_ID__NUM);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(fixPtSpecifEClass, FixPtSpecif.class, "FixPtSpecif", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFixPtSpecif_ListFixPtOpsSpecif(), this.getFixPtOpsSpecif(), null, "listFixPtOpsSpecif", null, 0, -1, FixPtSpecif.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFixPtSpecif_ListFixPtType(), this.getStringToFixPtTypeMapEntry(), null, "listFixPtType", null, 0, -1, FixPtSpecif.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fixPtOpsSpecifEClass, FixPtOpsSpecif.class, "FixPtOpsSpecif", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFixPtOpsSpecif_Input1(), this.getFixPtType(), null, "input1", null, 1, 1, FixPtOpsSpecif.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFixPtOpsSpecif_Input2(), this.getFixPtType(), null, "input2", null, 1, 1, FixPtOpsSpecif.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFixPtOpsSpecif_Output(), this.getFixPtType(), null, "output", null, 1, 1, FixPtOpsSpecif.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFixPtOpsSpecif_NumOp(), ecorePackage.getEString(), "numOp", "-9999", 0, 1, FixPtOpsSpecif.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFixPtOpsSpecif_NameOp(), ecorePackage.getEString(), "nameOp", null, 0, 1, FixPtOpsSpecif.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fixPtTypeEClass, FixPtType.class, "FixPtType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFixPtType_TypeAcFixed(), this.getTypeAcFixed(), null, "typeAcFixed", null, 1, 1, FixPtType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFixPtType_Dyn(), this.getDynamic(), null, "dyn", null, 1, 1, FixPtType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(typeAcFixedEClass, TypeAcFixed.class, "TypeAcFixed", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTypeAcFixed_Wd(), ecorePackage.getEInt(), "wd", "-9999", 1, 1, TypeAcFixed.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTypeAcFixed_Wip(), ecorePackage.getEInt(), "wip", "-9999", 1, 1, TypeAcFixed.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTypeAcFixed_S(), ecorePackage.getEBoolean(), "s", "true", 1, 1, TypeAcFixed.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTypeAcFixed_Q(), ecorePackage.getEString(), "q", null, 1, 1, TypeAcFixed.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTypeAcFixed_O(), ecorePackage.getEString(), "o", null, 1, 1, TypeAcFixed.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dynamicEClass, FixPtSpecification.Dynamic.class, "Dynamic", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDynamic_LowerBound(), ecorePackage.getEDouble(), "lowerBound", "9999", 1, 1, FixPtSpecification.Dynamic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDynamic_UpperBound(), ecorePackage.getEDouble(), "upperBound", "-9999", 1, 1, FixPtSpecification.Dynamic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stringToFixPtTypeMapEntryEClass, Map.Entry.class, "StringToFixPtTypeMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStringToFixPtTypeMapEntry_Key(), this.getDataId(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStringToFixPtTypeMapEntry_Value(), this.getFixPtType(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataIdEClass, DataId.class, "DataId", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDataId_Name(), ecorePackage.getEString(), "Name", null, 1, 1, DataId.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDataId_Num(), ecorePackage.getEString(), "Num", null, 1, 1, DataId.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //FixPtSpecificationPackageImpl
