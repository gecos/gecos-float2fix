/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package FixPtSpecification.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;

import FixPtSpecification.FixPtSpecificationFactory;
import FixPtSpecification.FixPtSpecificationPackage;
import FixPtSpecification.TypeAcFixed;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type Ac Fixed</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link FixPtSpecification.impl.TypeAcFixedImpl#getWd <em>Wd</em>}</li>
 *   <li>{@link FixPtSpecification.impl.TypeAcFixedImpl#getWip <em>Wip</em>}</li>
 *   <li>{@link FixPtSpecification.impl.TypeAcFixedImpl#isS <em>S</em>}</li>
 *   <li>{@link FixPtSpecification.impl.TypeAcFixedImpl#getQ <em>Q</em>}</li>
 *   <li>{@link FixPtSpecification.impl.TypeAcFixedImpl#getO <em>O</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TypeAcFixedImpl extends EObjectImpl implements TypeAcFixed {
	/**
	 * The default value of the '{@link #getWd() <em>Wd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWd()
	 * @generated
	 * @ordered
	 */
	protected static final int WD_EDEFAULT = -9999;

	/**
	 * The cached value of the '{@link #getWd() <em>Wd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWd()
	 * @generated
	 * @ordered
	 */
	protected int wd = WD_EDEFAULT;

	/**
	 * The default value of the '{@link #getWip() <em>Wip</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWip()
	 * @generated
	 * @ordered
	 */
	protected static final int WIP_EDEFAULT = -9999;

	/**
	 * The cached value of the '{@link #getWip() <em>Wip</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWip()
	 * @generated
	 * @ordered
	 */
	protected int wip = WIP_EDEFAULT;

	/**
	 * The default value of the '{@link #isS() <em>S</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isS()
	 * @generated
	 * @ordered
	 */
	protected static final boolean S_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isS() <em>S</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isS()
	 * @generated
	 * @ordered
	 */
	protected boolean s = S_EDEFAULT;

	/**
	 * The default value of the '{@link #getQ() <em>Q</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQ()
	 * @generated
	 * @ordered
	 */
	protected static final String Q_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQ() <em>Q</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQ()
	 * @generated
	 * @ordered
	 */
	protected String q = Q_EDEFAULT;

	/**
	 * The default value of the '{@link #getO() <em>O</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getO()
	 * @generated
	 * @ordered
	 */
	protected static final String O_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getO() <em>O</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getO()
	 * @generated
	 * @ordered
	 */
	protected String o = O_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	static final boolean EMFCopy=false;
	
	protected TypeAcFixedImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FixPtSpecificationPackage.Literals.TYPE_AC_FIXED;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getWd() {
		return wd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWd(int newWd) {
		int oldWd = wd;
		wd = newWd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixPtSpecificationPackage.TYPE_AC_FIXED__WD, oldWd, wd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getWip() {
		return wip;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWip(int newWip) {
		int oldWip = wip;
		wip = newWip;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixPtSpecificationPackage.TYPE_AC_FIXED__WIP, oldWip, wip));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isS() {
		return s;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setS(boolean newS) {
		boolean oldS = s;
		s = newS;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixPtSpecificationPackage.TYPE_AC_FIXED__S, oldS, s));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getQ() {
		return q;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQ(String newQ) {
		String oldQ = q;
		q = newQ;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixPtSpecificationPackage.TYPE_AC_FIXED__Q, oldQ, q));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getO() {
		return o;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setO(String newO) {
		String oldO = o;
		o = newO;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixPtSpecificationPackage.TYPE_AC_FIXED__O, oldO, o));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FixPtSpecificationPackage.TYPE_AC_FIXED__WD:
				return getWd();
			case FixPtSpecificationPackage.TYPE_AC_FIXED__WIP:
				return getWip();
			case FixPtSpecificationPackage.TYPE_AC_FIXED__S:
				return isS();
			case FixPtSpecificationPackage.TYPE_AC_FIXED__Q:
				return getQ();
			case FixPtSpecificationPackage.TYPE_AC_FIXED__O:
				return getO();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FixPtSpecificationPackage.TYPE_AC_FIXED__WD:
				setWd((Integer)newValue);
				return;
			case FixPtSpecificationPackage.TYPE_AC_FIXED__WIP:
				setWip((Integer)newValue);
				return;
			case FixPtSpecificationPackage.TYPE_AC_FIXED__S:
				setS((Boolean)newValue);
				return;
			case FixPtSpecificationPackage.TYPE_AC_FIXED__Q:
				setQ((String)newValue);
				return;
			case FixPtSpecificationPackage.TYPE_AC_FIXED__O:
				setO((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FixPtSpecificationPackage.TYPE_AC_FIXED__WD:
				setWd(WD_EDEFAULT);
				return;
			case FixPtSpecificationPackage.TYPE_AC_FIXED__WIP:
				setWip(WIP_EDEFAULT);
				return;
			case FixPtSpecificationPackage.TYPE_AC_FIXED__S:
				setS(S_EDEFAULT);
				return;
			case FixPtSpecificationPackage.TYPE_AC_FIXED__Q:
				setQ(Q_EDEFAULT);
				return;
			case FixPtSpecificationPackage.TYPE_AC_FIXED__O:
				setO(O_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FixPtSpecificationPackage.TYPE_AC_FIXED__WD:
				return wd != WD_EDEFAULT;
			case FixPtSpecificationPackage.TYPE_AC_FIXED__WIP:
				return wip != WIP_EDEFAULT;
			case FixPtSpecificationPackage.TYPE_AC_FIXED__S:
				return s != S_EDEFAULT;
			case FixPtSpecificationPackage.TYPE_AC_FIXED__Q:
				return Q_EDEFAULT == null ? q != null : !Q_EDEFAULT.equals(q);
			case FixPtSpecificationPackage.TYPE_AC_FIXED__O:
				return O_EDEFAULT == null ? o != null : !O_EDEFAULT.equals(o);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		String result = " (wd: ";
		result += wd;
		result += ", wip: ";
		result += wip;
		result += ", s: ";
		result += s;
		result += ", q: ";
		result += q;
		result += ", o: ";
		result += o;
		result += ')';
		return result;
	}
	
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */

	public TypeAcFixed copy()
	{
		if (EMFCopy) {
			Copier copier = new Copier();
			TypeAcFixed acFix=(TypeAcFixed)copier.copy(this);
			
			copier.copyReferences();
			return acFix;
		} else {
			TypeAcFixed acFixCopy=FixPtSpecificationFactory.eINSTANCE.createTypeAcFixed();
			acFixCopy.setO(this.getO());
			acFixCopy.setQ(this.getQ());
			acFixCopy.setWd(this.getWd());
			acFixCopy.setWip(this.getWip());


			return acFixCopy;
		}
	}

} //TypeAcFixedImpl
