/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package FixPtSpecification.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import FixPtSpecification.FixPtOpsSpecif;
import FixPtSpecification.FixPtSpecificationPackage;
import FixPtSpecification.FixPtType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fix Pt Ops Specif</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link FixPtSpecification.impl.FixPtOpsSpecifImpl#getInput1 <em>Input1</em>}</li>
 *   <li>{@link FixPtSpecification.impl.FixPtOpsSpecifImpl#getInput2 <em>Input2</em>}</li>
 *   <li>{@link FixPtSpecification.impl.FixPtOpsSpecifImpl#getOutput <em>Output</em>}</li>
 *   <li>{@link FixPtSpecification.impl.FixPtOpsSpecifImpl#getNumOp <em>Num Op</em>}</li>
 *   <li>{@link FixPtSpecification.impl.FixPtOpsSpecifImpl#getNameOp <em>Name Op</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class FixPtOpsSpecifImpl extends EObjectImpl implements FixPtOpsSpecif {
	/**
	 * The cached value of the '{@link #getInput1() <em>Input1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInput1()
	 * @generated
	 * @ordered
	 */
	protected FixPtType input1;

	/**
	 * The cached value of the '{@link #getInput2() <em>Input2</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInput2()
	 * @generated
	 * @ordered
	 */
	protected FixPtType input2;

	/**
	 * The cached value of the '{@link #getOutput() <em>Output</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutput()
	 * @generated
	 * @ordered
	 */
	protected FixPtType output;

	/**
	 * The default value of the '{@link #getNumOp() <em>Num Op</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumOp()
	 * @generated
	 * @ordered
	 */
	protected static final String NUM_OP_EDEFAULT = "-9999";

	/**
	 * The cached value of the '{@link #getNumOp() <em>Num Op</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumOp()
	 * @generated
	 * @ordered
	 */
	protected String numOp = NUM_OP_EDEFAULT;

	/**
	 * The default value of the '{@link #getNameOp() <em>Name Op</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNameOp()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_OP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNameOp() <em>Name Op</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNameOp()
	 * @generated
	 * @ordered
	 */
	protected String nameOp = NAME_OP_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FixPtOpsSpecifImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FixPtSpecificationPackage.Literals.FIX_PT_OPS_SPECIF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixPtType getInput1() {
		return input1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInput1(FixPtType newInput1, NotificationChain msgs) {
		FixPtType oldInput1 = input1;
		input1 = newInput1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__INPUT1, oldInput1, newInput1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInput1(FixPtType newInput1) {
		if (newInput1 != input1) {
			NotificationChain msgs = null;
			if (input1 != null)
				msgs = ((InternalEObject)input1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__INPUT1, null, msgs);
			if (newInput1 != null)
				msgs = ((InternalEObject)newInput1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__INPUT1, null, msgs);
			msgs = basicSetInput1(newInput1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__INPUT1, newInput1, newInput1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixPtType getInput2() {
		return input2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInput2(FixPtType newInput2, NotificationChain msgs) {
		FixPtType oldInput2 = input2;
		input2 = newInput2;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__INPUT2, oldInput2, newInput2);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInput2(FixPtType newInput2) {
		if (newInput2 != input2) {
			NotificationChain msgs = null;
			if (input2 != null)
				msgs = ((InternalEObject)input2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__INPUT2, null, msgs);
			if (newInput2 != null)
				msgs = ((InternalEObject)newInput2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__INPUT2, null, msgs);
			msgs = basicSetInput2(newInput2, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__INPUT2, newInput2, newInput2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixPtType getOutput() {
		return output;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOutput(FixPtType newOutput, NotificationChain msgs) {
		FixPtType oldOutput = output;
		output = newOutput;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__OUTPUT, oldOutput, newOutput);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutput(FixPtType newOutput) {
		if (newOutput != output) {
			NotificationChain msgs = null;
			if (output != null)
				msgs = ((InternalEObject)output).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__OUTPUT, null, msgs);
			if (newOutput != null)
				msgs = ((InternalEObject)newOutput).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__OUTPUT, null, msgs);
			msgs = basicSetOutput(newOutput, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__OUTPUT, newOutput, newOutput));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNumOp() {
		return numOp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumOp(String newNumOp) {
		String oldNumOp = numOp;
		numOp = newNumOp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__NUM_OP, oldNumOp, numOp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNameOp() {
		return nameOp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNameOp(String newNameOp) {
		String oldNameOp = nameOp;
		nameOp = newNameOp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__NAME_OP, oldNameOp, nameOp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__INPUT1:
				return basicSetInput1(null, msgs);
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__INPUT2:
				return basicSetInput2(null, msgs);
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__OUTPUT:
				return basicSetOutput(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__INPUT1:
				return getInput1();
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__INPUT2:
				return getInput2();
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__OUTPUT:
				return getOutput();
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__NUM_OP:
				return getNumOp();
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__NAME_OP:
				return getNameOp();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__INPUT1:
				setInput1((FixPtType)newValue);
				return;
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__INPUT2:
				setInput2((FixPtType)newValue);
				return;
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__OUTPUT:
				setOutput((FixPtType)newValue);
				return;
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__NUM_OP:
				setNumOp((String)newValue);
				return;
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__NAME_OP:
				setNameOp((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__INPUT1:
				setInput1((FixPtType)null);
				return;
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__INPUT2:
				setInput2((FixPtType)null);
				return;
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__OUTPUT:
				setOutput((FixPtType)null);
				return;
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__NUM_OP:
				setNumOp(NUM_OP_EDEFAULT);
				return;
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__NAME_OP:
				setNameOp(NAME_OP_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__INPUT1:
				return input1 != null;
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__INPUT2:
				return input2 != null;
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__OUTPUT:
				return output != null;
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__NUM_OP:
				return NUM_OP_EDEFAULT == null ? numOp != null : !NUM_OP_EDEFAULT.equals(numOp);
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF__NAME_OP:
				return NAME_OP_EDEFAULT == null ? nameOp != null : !NAME_OP_EDEFAULT.equals(nameOp);
		}
		return super.eIsSet(featureID);
	}

	@Override
	public String toString() {
		return "FixPtOpsSpecifImpl [input1=" + input1 + ", input2=" + input2
				+ ", output=" + output + ", numOp=" + numOp + ", nameOp="
				+ nameOp + "]";
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */


} //FixPtOpsSpecifImpl
