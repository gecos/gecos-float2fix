/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package FixPtSpecification.impl;

import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import FixPtSpecification.DataId;
import FixPtSpecification.FixPtOpsSpecif;
import FixPtSpecification.FixPtSpecif;
import FixPtSpecification.FixPtSpecificationFactory;
import FixPtSpecification.FixPtSpecificationPackage;
import FixPtSpecification.FixPtType;
import FixPtSpecification.TypeAcFixed;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FixPtSpecificationFactoryImpl extends EFactoryImpl implements FixPtSpecificationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FixPtSpecificationFactory init() {
		try {
			FixPtSpecificationFactory theFixPtSpecificationFactory = (FixPtSpecificationFactory)EPackage.Registry.INSTANCE.getEFactory("fr.irisa.cairn.float2fix.model.FixPtSpecification"); 
			if (theFixPtSpecificationFactory != null) {
				return theFixPtSpecificationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new FixPtSpecificationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixPtSpecificationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case FixPtSpecificationPackage.FIX_PT_SPECIF: return createFixPtSpecif();
			case FixPtSpecificationPackage.FIX_PT_OPS_SPECIF: return createFixPtOpsSpecif();
			case FixPtSpecificationPackage.FIX_PT_TYPE: return createFixPtType();
			case FixPtSpecificationPackage.TYPE_AC_FIXED: return createTypeAcFixed();
			case FixPtSpecificationPackage.DYNAMIC: return createDynamic();
			case FixPtSpecificationPackage.STRING_TO_FIX_PT_TYPE_MAP_ENTRY: return (EObject)createStringToFixPtTypeMapEntry();
			case FixPtSpecificationPackage.DATA_ID: return createDataId();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixPtSpecif createFixPtSpecif() {
		FixPtSpecifImpl fixPtSpecif = new FixPtSpecifImpl();
		return fixPtSpecif;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixPtOpsSpecif createFixPtOpsSpecif() {
		FixPtOpsSpecifImpl fixPtOpsSpecif = new FixPtOpsSpecifImpl();
		return fixPtOpsSpecif;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixPtType createFixPtType() {
		FixPtTypeImpl fixPtType = new FixPtTypeImpl();
		return fixPtType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeAcFixed createTypeAcFixed() {
		TypeAcFixedImpl typeAcFixed = new TypeAcFixedImpl();
		return typeAcFixed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixPtSpecification.Dynamic createDynamic() {
		DynamicImpl dynamic = new DynamicImpl();
		return dynamic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<DataId, FixPtType> createStringToFixPtTypeMapEntry() {
		StringToFixPtTypeMapEntryImpl stringToFixPtTypeMapEntry = new StringToFixPtTypeMapEntryImpl();
		return stringToFixPtTypeMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataId createDataId() {
		DataIdImpl dataId = new DataIdImpl();
		return dataId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixPtSpecificationPackage getFixPtSpecificationPackage() {
		return (FixPtSpecificationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static FixPtSpecificationPackage getPackage() {
		return FixPtSpecificationPackage.eINSTANCE;
	}

} //FixPtSpecificationFactoryImpl
