/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package FixPtSpecification;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fix Pt Ops Specif</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link FixPtSpecification.FixPtOpsSpecif#getInput1 <em>Input1</em>}</li>
 *   <li>{@link FixPtSpecification.FixPtOpsSpecif#getInput2 <em>Input2</em>}</li>
 *   <li>{@link FixPtSpecification.FixPtOpsSpecif#getOutput <em>Output</em>}</li>
 *   <li>{@link FixPtSpecification.FixPtOpsSpecif#getNumOp <em>Num Op</em>}</li>
 *   <li>{@link FixPtSpecification.FixPtOpsSpecif#getNameOp <em>Name Op</em>}</li>
 * </ul>
 * </p>
 *
 * @see FixPtSpecification.FixPtSpecificationPackage#getFixPtOpsSpecif()
 * @model
 * @generated
 */
public interface FixPtOpsSpecif extends EObject {
	/**
	 * Returns the value of the '<em><b>Input1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input1</em>' containment reference.
	 * @see #setInput1(FixPtType)
	 * @see FixPtSpecification.FixPtSpecificationPackage#getFixPtOpsSpecif_Input1()
	 * @model containment="true" required="true"
	 * @generated
	 */
	FixPtType getInput1();

	/**
	 * Sets the value of the '{@link FixPtSpecification.FixPtOpsSpecif#getInput1 <em>Input1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input1</em>' containment reference.
	 * @see #getInput1()
	 * @generated
	 */
	void setInput1(FixPtType value);

	/**
	 * Returns the value of the '<em><b>Input2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input2</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input2</em>' containment reference.
	 * @see #setInput2(FixPtType)
	 * @see FixPtSpecification.FixPtSpecificationPackage#getFixPtOpsSpecif_Input2()
	 * @model containment="true" required="true"
	 * @generated
	 */
	FixPtType getInput2();

	/**
	 * Sets the value of the '{@link FixPtSpecification.FixPtOpsSpecif#getInput2 <em>Input2</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input2</em>' containment reference.
	 * @see #getInput2()
	 * @generated
	 */
	void setInput2(FixPtType value);

	/**
	 * Returns the value of the '<em><b>Output</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output</em>' containment reference.
	 * @see #setOutput(FixPtType)
	 * @see FixPtSpecification.FixPtSpecificationPackage#getFixPtOpsSpecif_Output()
	 * @model containment="true" required="true"
	 * @generated
	 */
	FixPtType getOutput();

	/**
	 * Sets the value of the '{@link FixPtSpecification.FixPtOpsSpecif#getOutput <em>Output</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output</em>' containment reference.
	 * @see #getOutput()
	 * @generated
	 */
	void setOutput(FixPtType value);

	/**
	 * Returns the value of the '<em><b>Num Op</b></em>' attribute.
	 * The default value is <code>"-9999"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Num Op</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Num Op</em>' attribute.
	 * @see #setNumOp(String)
	 * @see FixPtSpecification.FixPtSpecificationPackage#getFixPtOpsSpecif_NumOp()
	 * @model default="-9999"
	 * @generated
	 */
	String getNumOp();

	/**
	 * Sets the value of the '{@link FixPtSpecification.FixPtOpsSpecif#getNumOp <em>Num Op</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Num Op</em>' attribute.
	 * @see #getNumOp()
	 * @generated
	 */
	void setNumOp(String value);

	/**
	 * Returns the value of the '<em><b>Name Op</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name Op</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name Op</em>' attribute.
	 * @see #setNameOp(String)
	 * @see FixPtSpecification.FixPtSpecificationPackage#getFixPtOpsSpecif_NameOp()
	 * @model
	 * @generated
	 */
	String getNameOp();

	/**
	 * Sets the value of the '{@link FixPtSpecification.FixPtOpsSpecif#getNameOp <em>Name Op</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name Op</em>' attribute.
	 * @see #getNameOp()
	 * @generated
	 */
	void setNameOp(String value);

} // FixPtOpsSpecif
