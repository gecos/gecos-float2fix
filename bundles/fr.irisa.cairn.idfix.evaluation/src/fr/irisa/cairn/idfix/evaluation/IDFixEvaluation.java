package fr.irisa.cairn.idfix.evaluation;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;

import fr.irisa.cairn.idfix.model.factory.IDFixUserInformationAndTimingFactory;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixProject;
import fr.irisa.cairn.idfix.model.informationandtiming.Section;
import fr.irisa.cairn.idfix.model.userconfiguration.NOISE_FUNCTION_LOADER;
import fr.irisa.cairn.idfix.model.userconfiguration.RUNTIME_MODE;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.EnvironmentInformations;
import fr.irisa.cairn.idfix.utils.EnvironmentInformations.ENGINE;
import fr.irisa.cairn.idfix.utils.ListenProcessInputStandardStream;
import fr.irisa.cairn.idfix.utils.exceptions.EnvironmentSetupException;
import fr.irisa.cairn.idfix.utils.exceptions.IDFixEvalException;
import fr.irisa.cairn.idfix.utils.logging.IDFixLogger;

public class IDFixEvaluation {
	public static enum EvaluationType {DYNAMIC, NOISE, ALL}
	
	private IdfixProject _idfixProject;
	private EvaluationType _evaluationType;
	
	private Section _sectionTimeLog;	
	private static final Logger _logger = IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_EVALUATION);
	
	public IDFixEvaluation(IdfixProject project){
		this(project, EvaluationType.ALL);
	}
	
	public IDFixEvaluation(IdfixProject project, EvaluationType evalType){
		_idfixProject = project;
		_evaluationType = evalType;
		_sectionTimeLog = IDFixUserInformationAndTimingFactory.SECTION("Evaluation");
		_idfixProject.getProcessInformations().getTiming().getSections().add(_sectionTimeLog);
	}
	
	public void compute(){
		EnvironmentInformations.setup();
		
		long totaltimingStart = System.currentTimeMillis();
		long timingStart = System.currentTimeMillis();
		try{
			
			// If the "use last result mode" is enable, the system have been not changed. We don't need to run IDFixEval
			// because the result will be the same.
			if(!_idfixProject.getProcessInformations().isUseLastResultModeEnable()){			
				_logger.info("    *** Noise behavior computation ***");
				runIDFixEval(_idfixProject.getSFGFilePath(), _idfixProject.getSimulatedValuesFilePath(),
						_idfixProject.getIDFixEvalOutputFolderPath(), _evaluationType, _idfixProject.getUserConfiguration().getTheadNumber(),
						_idfixProject.getUserConfiguration().getRuntimeMode());
			}
			else{
				_logger.info("    *** No need to run IDFixEval process. Reuse last run process result activated ***");
				_logger.debug("Dynamic file path : " + _idfixProject.getDynamicFilePath());
				_logger.debug("Noise library file path : " + _idfixProject.getNoiseLibraryFilePath());
			}
		}catch (IDFixEvalException e){
			_logger.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
		long timingStop = System.currentTimeMillis();
		_sectionTimeLog.addTimeLog("IDFixEval (dynamic + accuracy)", timingStart, timingStop);
		
		// Update dynamic information of operators and datas in the fixed point specification.
		// Dynamics have obtained from the IDFixEval dynamic result file.
		if(_evaluationType != EvaluationType.NOISE){
			long timingStart2 = System.currentTimeMillis();
			_idfixProject.getFixedPointSpecification().updateDynamicInformation(_idfixProject.getDynamicFilePath());
			long timingStop2 = System.currentTimeMillis();
			_sectionTimeLog.addTimeLog("Update dynamic information", timingStart2, timingStop2);
		}
		
		long totaltimingStop = System.currentTimeMillis();
		_sectionTimeLog.setTime((totaltimingStop - totaltimingStart) / 1000f);
	}
	
	private void runIDFixEval(String sfgPath, String simulatedValueFilePath, String outputDirPath, EvaluationType evalType, int nbThread, RUNTIME_MODE mode) throws IDFixEvalException{
		List<String> command = new LinkedList<String>();
		String binIDFixPath = ConstantPathAndName.IDFIX_EVAL_FOLDER_PATH + ConstantPathAndName.IDFIX_BIN_FOLDER_NAME;
		binIDFixPath += resolveRuntimeMode(mode);
		File binIDFix = new File(binIDFixPath);
		if(!binIDFix.isFile())
			throw new IDFixEvalException("Error : IDFixEval binary not found after installation procedure (try to reset installation)");
		else if(!binIDFix.canExecute())
			binIDFix.setExecutable(true);
		
		command.add(binIDFixPath);
		
		command.add("--xml-file");
		command.add(sfgPath);
		
		command.add("--output-dir");
		command.add(outputDirPath);
		
		command.add("--eval-type");
		command.add(resovleEvalutationType(evalType));
		
		command.add("--output-format");
		if(_idfixProject.getUserConfiguration().getNoiseFunctionLoader() == NOISE_FUNCTION_LOADER.JNI)
			command.add("CJni");
		else if(_idfixProject.getUserConfiguration().getNoiseFunctionLoader() == NOISE_FUNCTION_LOADER.JNA)
			command.add("C");
		else
			throw new IDFixEvalException("Tools to run native code is not correct. JNI or JNA can be used");

		if (mode == RUNTIME_MODE.TRACE || mode == RUNTIME_MODE.DEBUG){
			command.add("--debug");
		}
		
		if(_idfixProject.getUserConfiguration().isCorrelationMode())
			command.add("--correlation");
		
		command.add("--timing");
			
		command.add("--simulated-values-xml");
		command.add(simulatedValueFilePath);
		
		command.add("--resources-dir");
		command.add(ConstantPathAndName.IDFIX_EVAL_FOLDER_PATH + ConstantPathAndName.IDFIX_EVAL_RESOURCES_FOLDER_NAME);
		
		command.add("--thread-numbers");
		command.add(Integer.toString(nbThread));
		
		_logger.info("Command = " + command);
		ProcessBuilder pb = new ProcessBuilder(command);
		try{	
			String value;
			if((value = pb.environment().get("PATH")) != null){
				if(EnvironmentInformations.getEngineUsed() == ENGINE.OCTAVE){
					String newPath = EnvironmentInformations.getEnv(EnvironmentInformations.KEY_OCTAVE) + ":" + value; 
					pb.environment().put("PATH", newPath);
				}
			}
			else{
				if(EnvironmentInformations.getEngineUsed() == ENGINE.OCTAVE){
					value = EnvironmentInformations.getEnv(EnvironmentInformations.KEY_OCTAVE);
					pb.environment().put("PATH", value);
				}
			}
			
			if((value = pb.environment().get("LD_LIBRARY_PATH")) != null){
				value += ":" + EnvironmentInformations.getEnv(EnvironmentInformations.KEY_XML2);	
				if(EnvironmentInformations.getEngineUsed() == ENGINE.MATLAB){
					value += ":" + EnvironmentInformations.getEnv(EnvironmentInformations.KEY_MATLAB);
					value += ":" + EnvironmentInformations.getEnv(EnvironmentInformations.KEY_MATIO);
				}
			}
			else{
				value = EnvironmentInformations.getEnv(EnvironmentInformations.KEY_XML2);
				if(EnvironmentInformations.getEngineUsed() == ENGINE.MATLAB){
					value += ":" + EnvironmentInformations.getEnv(EnvironmentInformations.KEY_MATLAB);
					value += ":" + EnvironmentInformations.getEnv(EnvironmentInformations.KEY_MATIO);
				}
			}
			pb.environment().put("LD_LIBRARY_PATH", value);
			Process process = pb.start();
			
			BufferedReader errReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
			ListenProcessInputStandardStream outReader = new ListenProcessInputStandardStream(process.getInputStream(), _logger, Level.DEBUG);
			outReader.start();
			
			String line;
			StringBuffer lineBuffer = new StringBuffer();
			while((line = errReader.readLine()) != null){
				lineBuffer.append(line).append("\n");
			}
			process.waitFor();
			outReader.join();
			
			if(lineBuffer.length() > 0){
				_logger.debug(lineBuffer.toString());
//				throw new IDFixEvalException("IDFixEval computation error");
			}
		}catch(IOException | InterruptedException | EnvironmentSetupException e){
			throw new IDFixEvalException(e);
		}
	}

	private String resovleEvalutationType(EvaluationType evalType) throws IDFixEvalException {
		switch (evalType) {
		case ALL:
			return "all";

		case DYNAMIC:
			return "dynamic";
			
		case NOISE:
			return "noise";
			
		default:
			throw new IDFixEvalException("Unkown evaluation type chosen. Only choice is all, dynamic or noise");
		}
	}

	private String resolveRuntimeMode(RUNTIME_MODE mode)
			throws IDFixEvalException {
		switch (mode) {
		case RELEASE:
			return ConstantPathAndName.IDFIX_RELEASE_BIN_NAME;
			
		case DEBUG:
			return ConstantPathAndName.IDFIX_DEBUG_BIN_NAME;
			
		case TRACE:
			return ConstantPathAndName.IDFIX_TRACE_BIN_NAME;

		default:
			throw new IDFixEvalException("Runtime mode of the user configuration incompatible with the available option of IDFix Eval"); 
		}
	}
}
