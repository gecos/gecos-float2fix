package fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils;

/**
 * Object representing an uninitialized value in the interpretation (we cannot use null with switches)
 * @author qmeunier
 */
public class UninitializedValue {
	
	public UninitializedValue(){}
	
}
