//package fr.irisa.cairn.idfix.frontend.sfg.generator;
//
//import float2fix.Node;
//import fr.irisa.cairn.idfix.frontend.sfg.generator.utils.INodeType;
//import gecos.core.Procedure;
//import gecos.instrs.RetInstruction;
//
///**
// * Procedure switch
// * The return value is the {@link Node} corresponding of the {@link Node} of the {@link RetInstruction}
// * @author Nicolas Simon
// * @date Jul 19, 2013
// */
//public class ProcedureGenerator {
//	private SFGFactory _factory;
//	private final boolean _interpretSetSourceKnwonValue;
//	
//	public ProcedureGenerator(SFGFactory factory, boolean interpretSetSourceKnownValue){
//		_factory = factory;
//		_interpretSetSourceKnwonValue = interpretSetSourceKnownValue;
//	}
//	
//	public INodeType doSwitch(Procedure p){
//		BlockGenerator blockGenerator = new BlockGenerator(_factory, _interpretSetSourceKnwonValue);
//		return blockGenerator.doSwitch(p.getBody());
//		
//	}
//}
