package fr.irisa.cairn.idfix.frontend.flattener.inconstruction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.tools.switches.DefaultInstructionSwitch;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils.ArrayIndexReplacer;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils.Context;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils.FieldAddressSymbol;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils.FieldSymbol;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils.ISymbolType;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils.IndexedAddressSymbol;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils.IndexedFieldSymbol;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils.IndexedSymbol;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils.SymbolArray;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils.SymbolStruct;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils.SymbolValue;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils.ValueReplacer;
import fr.irisa.cairn.idfix.utils.exceptions.FlattenerException;
import gecos.core.Procedure;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.CondInstruction;
import gecos.instrs.FieldInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.RetInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;

/**
 * InstructionFlattener is an instruction visitor, which interprets
 * instructions when possible, or copies them to the flatten program when not interpretable.
 * In the latter case, the known variables have to be replaced in the expression,
 * what is done thanks to the ValueReplacer
 * 
 * When we flatten instruction, we copy the instruction from the original program.
 * The instruction symbol are the original program variable symbol and not the new one copied from the block switch.
 * An update are been applied when an instruction is added to the block manager.
 * @author Nicolas Simon
 * @date Jul 3, 2013
 */
public class InstructionFlattener extends DefaultInstructionSwitch<Object> {
	/*
	 * The mode variable allows to select the operation done by the method
	 * caseSymbolInstruction or caseArrayInstruction
	 * The mode is RHS (Right-Hand Side) when we evaluate the right part of an affectation,
	 * or an array indexation (eg : h[a] = h[a] + 1);
	 */
	private enum KIND {LHS, RHS};
	private KIND _kind;
	
	private Context _context;
	private GenericInstInterpreter _ginstFlattener;
	@SuppressWarnings("unused")
	private ValueReplacer _valueReplacer;
	private ArrayIndexReplacer _arrayIndexReplacer;
	private BlockManager _manager;
	
	public InstructionFlattener(Context context, BlockManager manager){
		_context = context;
		_ginstFlattener = new GenericInstInterpreter(this);
		_valueReplacer = new ValueReplacer(context);
		_arrayIndexReplacer = new ArrayIndexReplacer(context);
		_manager = manager;
		_kind = KIND.RHS;
	}
	
	private void myassert(boolean b, String message) throws FlattenerException{
		if(!b)
			throw new FlattenerException(message);
	}
	
	private String generateFunctionName(CallInstruction i){
		// Création du nom temporaire de la variable
		// Une vérification est faites pour choisir un nom de symbol non utilisé dans le scope courant
		
		Scope scopeCurrent = _manager.getCurrentCompositeBlock().getScope();
		StringBuilder sBuilder = new StringBuilder();
		for(Symbol symb : scopeCurrent.lookupAllSymbols()){
			sBuilder.append(symb.getName());
		}
		Pattern pattern;
		Matcher matcher;
		String nameSymbol = i.getProcedureSymbol().getName();
		do{
			nameSymbol += "_call_";
			pattern = Pattern.compile(nameSymbol+"\\d+");
			matcher = pattern.matcher(sBuilder);
		}while(matcher.find());
		int temp = _manager.getProcedureFlattener().useIndexFunction();
		nameSymbol += temp;
		
		return nameSymbol;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Object caseSetInstruction(SetInstruction set){
		_kind = KIND.LHS;
		Object dest = doSwitch(set.getDest());
		
		_kind = KIND.RHS;
		SetInstruction inst = (SetInstruction) set.copy();
		Object ret = doSwitch(inst.getSource());
		
		try{
			myassert(ret instanceof ISymbolType, "No SymbolType used for a result of a set instruction");			
			ISymbolType value = (ISymbolType) ret;
			
			//TODO mettre une option
			//if(value instanceof UninitializedValue){
			//	_valueReplacer.doSwitch(inst);
				_arrayIndexReplacer.doSwitch(inst);
			//	}
			
		
			// Adding of the new instruction "flatten" into the flatten program
			_manager.addInstruction(inst);
			
			//Updating the context of the original program
			if(dest instanceof Symbol) { // a = b (need copy)
				myassert(value instanceof SymbolValue, "Error: Scalar variable type not used an SymbolValue");
				_context.setValue((Symbol) dest, ((SymbolValue) value).copy());
			}
			else if(dest instanceof FieldAddressSymbol){ // a = b (a and b are structure) (need copy)
				myassert(value instanceof SymbolStruct<?>, "Error: Structure variable type not used an SymbolStruct");
				_context.setValue(((FieldAddressSymbol) dest).getSymbol(),((SymbolStruct<ISymbolType>) value).copy());
			}
			else if(dest instanceof FieldSymbol){ // a.re = b.re (need copy)
				FieldSymbol fieldSymbol = (FieldSymbol) dest;
				_context.setValue(fieldSymbol.getSymbol(), fieldSymbol.getField(), value.copy());
			}
			else if(dest instanceof IndexedFieldSymbol){ // a[i].re = b.re (need copy)
				IndexedFieldSymbol is = (IndexedFieldSymbol) dest;
				_context.setValue(is.getSymbol(), is.getIndexes(), is.getField(), value.copy());
			}
			else if(dest instanceof IndexedSymbol){ // a[i] = b (need copy)
				IndexedSymbol s = (IndexedSymbol) dest;
				_context.setValue(s.getSymbol(), s.getIndexes(), value.copy());
			}
			else if(dest instanceof IndexedAddressSymbol){ // t = r (t and r are array)
				myassert(value instanceof SymbolArray<?>, "Error: need a IndexedAddressSymbol as source of set instruction IndexedAddressSymbol destination");
				_context.setValue(((IndexedAddressSymbol) dest).getSymbol() , (SymbolArray<ISymbolType>) value);
			}
			else{
				throw new FlattenerException("Kind of variable not managed yet");
			}
			
		} catch (FlattenerException e){
			throw new RuntimeException(e);
		}
		return null;
	}
	
	@Override
	public Object caseSymbolInstruction(SymbolInstruction s){
		try{
			if(_kind == KIND.LHS){
				if(s.getSymbol().getType().asBase() != null){
					return s.getSymbol();
				}
				else if(s.getSymbol().getType().asRecord() != null){
					return new FieldAddressSymbol(s.getSymbol());
				}
				else if(s.getSymbol().getType().asArray() != null){
					return new IndexedAddressSymbol(s.getSymbol());
				}
				else{
					throw new FlattenerException("Error: Type of SymbolInstruction symbol not correct (Symbol: " + s.getSymbol() + ", Type:" + s.getSymbol().getType() + ")");
				}
			}
			else{
				return _context.getValue(s.getSymbol());
			}
		} catch (FlattenerException e){
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Object caseFieldInstruction(FieldInstruction f){
		try{
			if(_kind == KIND.LHS){
				if(f.getExpr() instanceof SymbolInstruction){
					return new FieldSymbol(((SymbolInstruction)f.getExpr()).getSymbol(), f.getField());
				}
				else if(f.getExpr() instanceof ArrayInstruction){
					ArrayInstruction symInst = (ArrayInstruction) f.getExpr();
					Object arr = doSwitch(symInst);
					myassert(arr instanceof IndexedSymbol, "Error: a ArrayInstruction is not converted into IndexedSymbol");
					IndexedSymbol s = (IndexedSymbol) arr;
					return new IndexedFieldSymbol(s, f.getField());
				}
				else{
					throw new FlattenerException("Kind of structure variable not managed yet");
				}
			} else {
				if(f.getExpr() instanceof SymbolInstruction){
					SymbolInstruction symInst = (SymbolInstruction) f.getExpr();
					return _context.getValue(symInst.getSymbol(), f.getField());
				}
				else if(f.getExpr() instanceof ArrayInstruction){
					ArrayInstruction arrInst = (ArrayInstruction) f.getExpr();
					List<Long> indices = new ArrayList<>();
					for(Instruction inst : arrInst.getIndex()){
						Object o = doSwitch(inst);
						myassert(o instanceof SymbolValue && ((SymbolValue)o).getValue() instanceof Long, "Array index is not a integer (" + o + " : " + o.getClass() + ")");
						indices.add( (Long) (((SymbolValue)o).getValue()));
					}
					return _context.getValue(((SymbolInstruction)arrInst.getDest()).getSymbol(), indices, f.getField());
				}
				else {
					throw new FlattenerException("Kind of structure variable not managed yet");
				}
				
			}
		} catch (FlattenerException e){
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Object caseArrayInstruction(ArrayInstruction i){
		try{
			if(_kind == KIND.LHS){
				_kind = KIND.RHS;
				List<Long> indices = new ArrayList<>();
				for(Instruction inst : i.getIndex()){
					Object o = doSwitch(inst);
					myassert(o instanceof SymbolValue && ((SymbolValue)o).getValue() instanceof Long, "Array index is not a integer (" + o + " : " + o.getClass() + ")");
					indices.add( (Long) (((SymbolValue)o).getValue()));
				}
				_kind = KIND.LHS;
				return new IndexedSymbol(((SymbolInstruction) i.getDest()).getSymbol(), indices);
			}
			else{
				List<Long> indices = new ArrayList<>();
				for(Instruction inst : i.getIndex()){
					Object o = doSwitch(inst);
					myassert(o instanceof SymbolValue && ((SymbolValue)o).getValue() instanceof Long, "Array index is not a integer (" + o + " : " + o.getClass() + ")");					
					indices.add( (Long) (((SymbolValue)o).getValue()));
				}
				return _context.getValue(((SymbolInstruction) i.getDest()).getSymbol(), indices);
			}
		} catch (FlattenerException e){
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Object caseCondInstruction(CondInstruction c){
		Object res = doSwitch(c.getCond());
		return res;
	}
	
	@Override
	public Object caseGenericInstruction(GenericInstruction g){
		return new SymbolValue(_ginstFlattener.doSwitch(g));
	}
	
	@Override
	public Object caseIntInstruction(IntInstruction f){
		return new SymbolValue(new Long(f.getValue()));
	}
	
	@Override
	public Object caseFloatInstruction(FloatInstruction f){
		return new SymbolValue(new Double(f.getValue()));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Object caseCallInstruction(CallInstruction g){
		try{
			ProcedureSymbol procsym = (ProcedureSymbol) g.getProcedureSymbol();
			Procedure proc = procsym.getProcedure();
			
			//TODO A refaire avec les factory de gecos et non des simples copies
			// Copy of the orginal procedure called. Generate a new procedure symbol and a new name for the flatten version
			Procedure newProc = proc.copy();
			String name = generateFunctionName(g);
			ProcedureSymbol newProcSym = procsym.copy();
			newProcSym.setName(name);
			newProcSym.setProcedure(newProc);
			

			// Creation of the param scope of the called procedure
			// We need to work with the symbol name of the procedure parameters but know if their values are known or not (via the call instruction)
			Map<Symbol, ISymbolType> params = new HashMap<>();
			int i = 1;
			for(Symbol sym : newProc.listParameters()){
				if(sym.getType().asBase() != null){
					Instruction inst = g.getChildAt(i);
					Object result = doSwitch(inst);
					myassert(result instanceof SymbolValue, "Error: parameters of function doesn't return a SymbolValue");
					params.put(sym, new SymbolValue((SymbolValue) result)); // Need to create a copy
				} else if(sym.getType().asArray() != null){
					Symbol arg = ((SymbolInstruction) g.getChildAt(i)).getSymbol();
					params.put(sym, _context.getAddress(arg)); // No need to create a copy (reference)
				} else if(sym.getType().asRecord() != null){
					Symbol arg = ((SymbolInstruction) g.getChildAt(i)).getSymbol();
					myassert(_context.getAddress(arg) instanceof SymbolStruct, "A structure variable doesn't used context symbol structure");					
					params.put(sym, new SymbolStruct<ISymbolType>((SymbolStruct<ISymbolType>) _context.getAddress(arg)));
				} else if(sym.getType().asPointer() != null){
					throw new FlattenerException("Pointer type not managed yet");
				} else if(sym.getType().asAlias() != null){
					throw new FlattenerException("Alias type not managed yet");
				}
				i++;
			}
			
			// Create a frame on the context with the new params scope of the procedure 
			_context.pushFrame(params, name);
			_manager.getProcedureFlattener().doSwitch(newProc);
			
			// Creation of a new call instruction with the new name of the called procedure
			// This call instruction is used to replace the call instruction of the calling procedure
			// FIXME besoin de copier cette instruction? vu que normalement le body block de la main procedure est copié
			// 		  et que les procedures appelées sont copiées.
			CallInstruction call = GecosUserInstructionFactory.call(newProcSym);
//			for(int j = 1 ; j < g.getChildrenCount() ; j++){
//				call.getChildren().add(j, g.getChildAt(j).copy());
//			}
			for(Instruction arg : g.getArgs()){
				call.addArg(arg.copy());
			}
			
			// If the call instruction have a parent, the parent have already been copy and add to the block manager and
			// We just need to modify the old call instruction by the new one
			if(g.getParent() != null){ 
				ComplexInstruction parent = g.getParent();
//				int index = parent.getChildren().indexOf(g);
//				parent.getChildren().set(index, call);
				parent.replaceChild(g, call);
				_context.popFrame();
				return _context.getLastFunctionReturnValue();
			} else{
				_manager.addInstruction(call);
				_context.popFrame();
				return null;
			}
		} catch (FlattenerException e){
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Object caseRetInstruction(RetInstruction g){
		try{
			Instruction ret = g.getExpr();
			if(ret != null){ // Set into the context the last function return value. Needed to update context if this value is used
				Object res = doSwitch(ret);
				myassert(res instanceof ISymbolType, "Return value of a function is not a ISymbolType");
				_context.setLastFunctionReturnValue((ISymbolType) res);
			}
			_manager.addInstruction(g.copy());
		} catch (FlattenerException e){
			throw new RuntimeException(e);
		}
		return null;
	}
	
	
	
}
