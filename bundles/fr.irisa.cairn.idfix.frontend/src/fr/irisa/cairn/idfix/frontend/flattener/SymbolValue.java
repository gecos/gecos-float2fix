package fr.irisa.cairn.idfix.frontend.flattener;

/**
 * This class is used to store the current value of a symbol
 * For array compatibility, vectors are used, but it is supposed that arrays are linearized before using this class
 * @author qmeunier
 */
import java.util.ArrayList;


public class SymbolValue {
	
	private ArrayList<Object> values;
	
	private void myassert(boolean b, String s){
		if (!b){
			throw new RuntimeException(s);
		}
	}
	
	public SymbolValue(ArrayList<Object> v){
		values = new ArrayList<Object>();
		if (v != null){
			for (int i = 0; i < v.size(); i++){
				if (v.get(i) == null){
					values.add(new UninitializedValue());
				}
				else {
					values.add(v.get(i));
				}
			}
		}
		else {
			values.add(new UninitializedValue());
		}
	}
	
	public SymbolValue copy(){
		ArrayList<Object> init = new ArrayList<Object>();
		for (int i = 0; i < this.getNbValues(); i++){
			init.add(null);
		}
		SymbolValue res = new SymbolValue(init);
		for (int i = 0; i < this.getNbValues(); i++){
			res.setValueAt(this.getValueAt(i), i);
		}
		return res;
	}
	
	public void setValueAt(Object o, int n){
		myassert(o != null, "");
		values.set(n, o);
	}
	
	public Object getValueAt(long n){
		myassert(values.get((int) n) != null, "");
		return values.get((int) n);
	}
	
	public int getNbValues(){
		return values.size();
	}
	
}