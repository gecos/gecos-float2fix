
package fr.irisa.cairn.idfix.frontend.simulation;

import java.io.PrintStream;
import java.util.HashMap;

import affectationValueBySimulation.ValuesList;
import fr.irisa.cairn.idfix.utils.TraceManager;


class TraceManagerProbaBlock implements TraceManager {

	private int nbCalls;
	private int maxBlockNum;
	
	private HashMap<Integer, Integer> countNbTimesInBlock;
	
	public TraceManagerProbaBlock(){
		nbCalls = 0;
		countNbTimesInBlock = new HashMap<Integer, Integer>();
	}
	
	public void initiateExecution(){
		nbCalls++;
	}
	
	public void terminateExecution(){
		
	}
	
	public void traceBlock(int numBlock){
		if (countNbTimesInBlock.get(numBlock) == null){
			countNbTimesInBlock.put(numBlock, 1);
		}
		else {
			countNbTimesInBlock.put(numBlock, countNbTimesInBlock.get(numBlock) + 1);
		}
		if (numBlock > maxBlockNum){
			maxBlockNum = numBlock;
		}
	}
	
	public void showProbabilities(){
		for (int i = 0; i < getNbBlocks(); i++){
			Double proba;
			if (countNbTimesInBlock.get(i) == null){
				proba = 0.0;
			}
			else {
				proba = (double) countNbTimesInBlock.get(i) / nbCalls;
			}
			System.out.println("proba pour le block " + i + " : " + proba);
		}
	}
	
	public int getNbBlocks(){
		return maxBlockNum + 1;
	}
	
	
	public void printSequences(PrintStream fichierSortie){
		
	}
	
	public void traceAffectationValue(int numAffect, double value){
		
	}
	
	public ValuesList getValuesList(){
		return null;
	}
	
}


