package fr.irisa.cairn.idfix.frontend.flattener;

import java.util.ArrayList;

import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import gecos.core.Procedure;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.CondInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.RetInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.AliasType;
import gecos.types.ArrayType;
import gecos.types.BaseType;
import gecos.types.Type;

/**
 * InstructionFlattener is an instruction visitor, which interprets
 * instructions when possible, or copies them to the flatten program when not interpretable.
 * In the latter case, the known variables have to be replaced in the expression,
 * what is done thanks to the ValueReplacer
 * @author qmeunier
 */
public class InstFlattener extends BlockInstructionSwitch<Object> {

	/*
	 * The mode variable allows to select the operation done by the method
	 * caseSymbolInstruction or caseArrayInstruction
	 * The mode is RHS (Right-Hand Side) when we evaluate the right part of an affectation,
	 * or an array indexation (eg : h[a] = h[a] + 1);
	 */
	public final static int LHS = 1;
	public final static int RHS = 2;
	private int mode;

	public void setMode(int mode) {
		this.mode = mode;
	}

	public int getMode() {
		return mode;
	}

	private ExecutionContext context;
	private ExecutionContext newContext;
	private ExpFlattener expFlattener;
	private ProcFlattener procFlattener;
	private BlockFlattener blockFlattener;
	private ValueReplacer valueReplacer;
	private SymbolReplacer symbolReplacer;
	
	private boolean partialFlattening;
	
	private int nbCalls = 0;
	
	private Object retValue;
	
	
	private boolean createNewNodeOnUpdate = false;
	
	private void myassert(boolean b, String s) throws FlattenerException{
		if (!b){
			throw new FlattenerException(s);
		}
	}

	public InstFlattener(ExecutionContext context, ExecutionContext newContext, ProcFlattener Flattener, BlockFlattener blockFlattener, SymbolReplacer symbolReplacer) {
		this.context = context;
		this.newContext = newContext;
		this.procFlattener = Flattener;
		this.blockFlattener = blockFlattener;
		this.symbolReplacer = symbolReplacer;
		this.partialFlattening = false;
		expFlattener = new ExpFlattener(this);
		valueReplacer = new ValueReplacer(context, expFlattener, procFlattener.isForceInterpreting());
		retValue = null;
	}
	
	
	public void setPartialFlattening(boolean b){
		partialFlattening = b;
	}
	
	public boolean isInPartialFlattening(){
		return partialFlattening;
	}
	
	
	public boolean getCreateNewNodeOnUpdate(){
		return createNewNodeOnUpdate;
	}
	
	public void setCreateNewNodeOnUpdate(boolean b){
		createNewNodeOnUpdate = b;
	}
	
	private String generateNewFunctionName(CallInstruction inst){
		nbCalls++;
		String newName = inst.getProcedureSymbol().getName() + "_call_" + nbCalls;
		return newName;
	}
	

	@Override
	public Object caseIntInstruction(IntInstruction g) {
		// Return the actual Int value
		return new Long(g.getValue());
	}
	
	@Override
	public Object caseFloatInstruction(FloatInstruction g) {
		// Return the actual Float value
		return new Float(g.getValue());
	}

	@Override
	public Object caseSetInstruction(SetInstruction s) {
		// Visit Left-Hand Side of statement to find out 
		// the variable (symbol) that has to be modified.
		setMode(LHS);
		Object target = doSwitch(s.getDest());
		// Visit Right-Hand Side of statement to find out 
		// what is the new value of the variable
		setMode(RHS);
		/* On recopie l'instruction et on évalue la copie pour faire les modifications en place (noms des fonctions appelées) */
		SetInstruction inst = (SetInstruction) s.copy();
		Object value = doSwitch(inst.getSource());
		if (!partialFlattening && (value instanceof UninitializedValue || blockFlattener.forceInstructionCopy() || !procFlattener.isForceInterpreting())){
			/* On utilise une valeur non initialisée dans l'expression, cette dernière n'est pas évaluable */
			/* Ou alors on est dans le block then/else d'un if non évaluable, donc on recopie dans tous les cas l'instruction */
			// On ajoute l'instruction et on remplace dans inst les paramètres connus par leur valeur
			valueReplacer.doSwitch(inst);
			symbolReplacer.doSwitch(inst);
			procFlattener.getCurrentBlockManager().addInstruction(inst);
		}
	
		if (target instanceof Symbol){
			// Update symbol value
			value = castValueToType(value, ((Symbol) target).getType());
			this.context.setValue((Symbol) target, value);
		}
		else if (target instanceof IndexedSymbol){
			// Update symbol value
			try {
				value = castValueToType(value, ((IndexedSymbol) target).getSymbol().getType());
				this.context.setValue(((IndexedSymbol) target).getSymbol(), value, ((IndexedSymbol) target).getIndexes());
			} catch (FlattenerException e) {
				throw new RuntimeException(e);
			}
		}
		else {
			throw new RuntimeException("Type incorrect pour la destination de l'affectation");
		}
		return null;
	}

	private Object castValueToType(Object value, Type type) {
		if (value instanceof Long) {
			TypeAnalyzer typeAnalyzer = new TypeAnalyzer(type);
			if(typeAnalyzer.getBaseLevel().isFloat()) {
				/* convert value to Float type */
				value = Float.valueOf(((Long)value).floatValue());
			}
		}
		else if (value instanceof Float) {
			TypeAnalyzer typeAnalyzer = new TypeAnalyzer(type);
			if(typeAnalyzer.getBaseLevel().isInt()) {
				/* convert value to Long type */
				value = Long.valueOf(((Float)value).longValue());
			}
		}
		
		return value;		
	}

	@Override
	public Object caseCallInstruction(CallInstruction g) {
		try{
			SymbolInstruction symInst = (SymbolInstruction) g.getAddress();
			ProcedureSymbol sym = (ProcedureSymbol) symInst.getSymbol();
			Procedure procedure = sym.getProcedure();
			if (procedure == null){
				// exemple : fonction définie dans un .h

				Object value = null;
				switch(symInst.getSymbol().getName()) {
				case "cos" :
					myassert(g.getArgs().size() == 1, "Invalid call to cos! It should have only one argument.");
					Object arg1 = doSwitch(g.getArgs().get(0));
					if(arg1 instanceof Float) 
						value = Float.valueOf((float) Math.cos((Float)arg1));
					else if(arg1 instanceof Long)
						value = Float.valueOf((float) Math.cos((Long)arg1));
					
					//System.out.println("[Gecos instFlattener] cos(" + arg1 + ") = " + value);
					break;
				case "sin" :
					myassert(g.getArgs().size() == 1, "Invalid call to sin! It should have only one argument.");
					arg1 = doSwitch(g.getArgs().get(0));
					if(arg1 instanceof Float)
						value = Float.valueOf((float) Math.sin((Float)arg1));
					else if(arg1 instanceof Long)
						value = Float.valueOf((float) Math.sin((Long)arg1));
					
					//System.out.println("[Gecos instFlattener] sin(" + arg1 + ") = " + value);
					break;
				case "log" :
					myassert(g.getArgs().size() == 1, "Invalid call to log! It should have only one argument.");
					arg1 = doSwitch(g.getArgs().get(0));
					if(arg1 instanceof Long)
						value = Float.valueOf((float) (Math.log((Long)arg1))); //FIXME the result might be incorrect !
					else if(arg1 instanceof Float)
						value = Float.valueOf((float) (Math.log((Float)arg1))); //FIXME the result might be incorrect !
					break;
				case "log2" :
					myassert(g.getArgs().size() == 1, "Invalid call to log2! It should have only one argument.");
					arg1 = doSwitch(g.getArgs().get(0));
					if(arg1 instanceof Long)
						value = Float.valueOf((float) (Math.log((Long)arg1) / Math.log(2))); //FIXME the result might be incorrect !
					else if(arg1 instanceof Float)
						value = Float.valueOf((float) (Math.log((Float)arg1) / Math.log(2))); //FIXME the result might be incorrect !
					break;
				case "pow" :
					myassert(g.getArgs().size() == 2, "Invalid call to pow! It should have 2 arguments.");
					arg1 = doSwitch(g.getArgs().get(0));
					Object arg2 = doSwitch(g.getArgs().get(1));
					if(arg1 instanceof Float) {
						if(arg2 instanceof Float)
							value = Float.valueOf((float) Math.pow((Float)arg1, (Float)arg2));
						else if(arg2 instanceof Long)
							value = Float.valueOf((float) Math.pow((Float)arg1, (Long)arg2));
					}
					else if(arg1 instanceof Long) {
						if(arg2 instanceof Float)
							value = Float.valueOf((float) Math.pow((Long)arg1, (Float)arg2));
						else if(arg2 instanceof Long)
							value = Float.valueOf((float) Math.pow((Long)arg1, (Long)arg2));
					}
					break;
				case "floor" :
					myassert(g.getArgs().size() == 1, "Invalid call to floor! It should have only one argument.");
					arg1 = doSwitch(g.getArgs().get(0));
					if(arg1 instanceof Float)
						value = Float.valueOf((float) Math.floor((Float)arg1));
					else if(arg1 instanceof Long) //XXX
						value = arg1;
					break;
				}
				
				if(value != null) {
					/* replace function call with its evaluation */
					//TODO: move it to ValueReplacer
					if(g.getParent() != null) {
						Instruction valueInst = null;
						if(value instanceof Long)
							valueInst = GecosUserInstructionFactory.Int((Long) value);
						else if(value instanceof Float)
							valueInst = GecosUserInstructionFactory.Float((Float) value);
						
						if(valueInst != null ) 
							g.getParent().replaceChild(g, valueInst);
					}
					
					return value;
				}
				
				return new UninitializedValue();
			}
			Scope params = procedure.getScope();
	
			myassert((params.getSymbols().size() == (g.getArgs().size())), "Wrong Number of argument for call to " + sym.getName());
			
			// On n'interprète plus les appels de fonction
			
			ProcedureSymbol procedureSymbol = (ProcedureSymbol) g.getProcedureSymbol();
			if (procedureSymbol != sym) throw new RuntimeException();
			ProcedureSymbol procSymbolCopy = procedureSymbol.copy();
			Procedure newProcedure = procSymbolCopy.getProcedure();
			myassert(newProcedure != null, "Copy of the procedure give null pointer");
			Scope newParams = newProcedure.getScope();
			
			CallInstruction newInst = (CallInstruction) g.copy();
			
			valueReplacer.doSwitch(newInst);
			
			String newName = generateNewFunctionName(newInst);
			procSymbolCopy.setName(newName);
			((SymbolInstruction)newInst.getAddress()).setSymbol(procSymbolCopy);
			
			context.initParamsTheo();
			newContext.initParamsTheo();
			int i = 1;
			for (Symbol param : params.getSymbols()) {
				Symbol newParam = newParams.getSymbols().get(i - 1);
				myassert(param.getName().equals(newParam.getName()), "");
				if (param.getType() instanceof ArrayType){
					Symbol arg;
					// Note : apparemment Gecos rajoute des cast en double pour les paramètres tableau de type double...
					if (g.getChildAt(i) instanceof ConvertInstruction){
						arg = ((SymbolInstruction) ((ConvertInstruction) g.getChildAt(i)).getExpr()).getSymbol();
					}
					else {
						arg = ((SymbolInstruction) g.getChildAt(i)).getSymbol();
					}
					context.getParamsTheo().put(newParam, context.getSymbolValue(arg));
					newContext.getParamsTheo().put(newParam, null);
				}
				else {
					Instruction child = g.getChildAt(i);
					Object result = doSwitch(child);
					ArrayList<Object> resVector = new ArrayList<Object>();
					resVector.add(result);
					context.getParamsTheo().put(newParam, new SymbolValue(resVector));
					newContext.getParamsTheo().put(newParam, null);
				}
				i++;
			}
			
			context.pushFrame();
			newContext.pushFrame();
			procFlattener.doSwitch(newProcedure);
			context.popFrame();
			newContext.popFrame();
			
			// Attention : le test dessous porte bien sur g et non sur newInst
			if (g.getParent() != null){
				// Appel de fonction dans une expression :
				// On remplace dans l'instruction parente le lien vers l'ancienne instruction de call vers la nouvelle instruction de call
				ComplexInstruction parent = g.getParent();
//				int instIndex = 0;
//				for (Instruction child : parent.getChildren()){
//					if (child == g){
//						break;
//					}
//					instIndex++;
//				}
//				parent.getChildren().set(instIndex, newInst);
				parent.replaceChild(g, newInst);
				Object res = retValue;
				retValue = null;
				return res;
			}
			else {
				myassert(!partialFlattening, ""); // Un call doit toujours être dans un block qui est annoté FORCE_FLATTENING
				symbolReplacer.doSwitch(newInst);
				procFlattener.getCurrentBlockManager().addInstruction(newInst);
				return null;
			}
		}catch (FlattenerException e){
			throw new RuntimeException(e);
		}
	}
	

	@Override
	public Object caseSymbolInstruction(SymbolInstruction object) {
		if (getMode() == LHS) {
			// we want to know which symbol to write, we therefore 
			// return the Symbol referenced by the instruction 
			return object.getSymbol();
		}
		else {
			// we want to know what to write, we use the value of 
			// the symbol referenced by the instruction 
			Object res;
			try {
				res = this.context.getValue(object.getSymbol());
			} catch (FlattenerException e) {
				throw new RuntimeException(e);
			}
			//System.out.println("CaseSymbol: On retourne " + res + " (classe " + res.getClass() + ")");
			return res;
		}
	}

	@Override
	public Object caseGenericInstruction(GenericInstruction g) {
		return expFlattener.doSwitch(g);
	}

	@Override
	public Object caseCondInstruction(CondInstruction g) {
		Object res = doSwitch(g.getCond());
		return res;
	}
	
	@Override
	public Object caseArrayInstruction(ArrayInstruction g){
		try{
			if (getMode() == LHS){
				/* On repasse en RHS pour l'évaluation des indices du tableau */
				setMode(RHS);
				ArrayList<Long> indices = _constructArrayIndices(g);
				setMode(LHS);
				IndexedSymbol indexedSymbol = new IndexedSymbol(((SymbolInstruction) g.getDest()).getSymbol(), indices);
				return indexedSymbol;
			}
			else {
				ArrayList<Long> indices = _constructArrayIndices(g);
				return context.getValue(((SymbolInstruction) g.getDest()).getSymbol(), indices);
			}
		}catch (FlattenerException e){
			throw new RuntimeException(e);
		}
	}

	private ArrayList<Long> _constructArrayIndices(ArrayInstruction g) throws FlattenerException {
		ArrayList<Long> indices = new ArrayList<Long>();
		for (Instruction index : g.getIndex()){
			Object o = doSwitch(index);
			myassert((o instanceof Long), "Indice de tableau non entier (" + o + " : " + o.getClass() + ") in: " + g);
			indices.add((Long) o);
			
//			/* Replace array subscript with their values */
//			g.replaceChild(index, GecosUserInstructionFactory.Int((Long)o));
		}
		return indices;
	}

	
	@Override
	public Object caseRetInstruction(RetInstruction g) {
		Instruction expr = g.getExpr();
		if (expr != null){
			retValue = doSwitch(expr);
			/* On recopie toujours l'instruction, sauf dans le cas du partial Flattening */
			if (!partialFlattening){
				Instruction inst = g.copy();
				// On remplace dans inst les paramètres connus par leur valeur
				valueReplacer.doSwitch(inst);
				symbolReplacer.doSwitch(inst);
				procFlattener.getCurrentBlockManager().addInstruction(inst);
			}
			return null;
		}
		else {
			//myassert(false, "");
			return null;
		}
	}
	
	
	@Override
	public Object caseConvertInstruction(ConvertInstruction g){
		Object res = doSwitch(g.getExpr());
		Type castType = g.getType();
		
		if(castType instanceof AliasType) {
			castType = ((AliasType) castType).getAlias();
		}
		
		if (res instanceof Long){
			if(castType instanceof BaseType && ((BaseType)castType).asFloat() != null) {
				res = Float.valueOf(((Long)res).floatValue());
			}
		}
		else if (res instanceof Float){
			if(castType instanceof BaseType && ((BaseType)castType).asInt() != null) {
				res = Long.valueOf(((Float)res).longValue());
			}
		}
		
		return res;
	}

}
