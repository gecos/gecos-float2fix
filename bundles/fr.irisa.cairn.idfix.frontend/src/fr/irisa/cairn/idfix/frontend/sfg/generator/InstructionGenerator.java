//package fr.irisa.cairn.idfix.frontend.sfg.generator;
//
//import float2fix.Node;
//import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
//import fr.irisa.cairn.gecos.model.tools.switches.DefaultInstructionSwitch;
//import fr.irisa.cairn.idfix.frontend.sfg.generator.utils.FieldNode;
//import fr.irisa.cairn.idfix.frontend.sfg.generator.utils.INodeType;
//import fr.irisa.cairn.idfix.frontend.sfg.generator.utils.ScalarNode;
//import fr.irisa.cairn.idfix.frontend.utils.AnnotateOperators;
//import fr.irisa.cairn.idfix.utils.IDFixUtils;
//import fr.irisa.cairn.idfix.utils.exceptions.SFGGeneratorException;
//import fr.irisa.cairn.idfix.utils.exceptions.SFGModelCreationException;
//import gecos.core.ParameterSymbol;
//import gecos.core.ProcedureSymbol;
//import gecos.core.Symbol;
//import gecos.instrs.ArrayInstruction;
//import gecos.instrs.CallInstruction;
//import gecos.instrs.FieldInstruction;
//import gecos.instrs.FloatInstruction;
//import gecos.instrs.GenericInstruction;
//import gecos.instrs.Instruction;
//import gecos.instrs.IntInstruction;
//import gecos.instrs.RetInstruction;
//import gecos.instrs.SetInstruction;
//import gecos.instrs.SymbolInstruction;
//import gecos.types.ArrayType;
//import gecos.types.Field;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * InstructionGenerator is an {@link Instruction} visitor, which generate the subgraph associate to the instruction
// * @author Nicolas Simon
// * @date Jul 19, 2013
// */
//public class InstructionGenerator extends DefaultInstructionSwitch<INodeType> {
//	private enum KIND {LHS, RHS};
//	private KIND _kind;
//	private SFGFactory _factory;
//	private ArrayIndexInterpreter _arrayIndexInterpreter;
//	private boolean _isDestDelayNode, _isSrcDelayNode;
//	
//	private final boolean _interpretSetSourceKnownValue;
//		
//	public InstructionGenerator(SFGFactory factory, boolean interpretSetSourceKnownValue){
//		_factory = factory;
//		_arrayIndexInterpreter = new ArrayIndexInterpreter();
//		_kind = KIND.RHS;
//		_isDestDelayNode = false;
//		_isSrcDelayNode = false;
//		_interpretSetSourceKnownValue = interpretSetSourceKnownValue;
//	}
//
//	private void myassert(boolean b, String s) throws SFGGeneratorException{
//		if (!b){
//			throw new SFGGeneratorException(s);
//		}
//	}
//	
//	@Override
//	public INodeType caseSetInstruction(SetInstruction s) {	
//		_isSrcDelayNode = false;
//		_isDestDelayNode = false;
//		// Creation of the source sub-graph first because we will create a new node for the destination		
//		_kind = KIND.RHS;
//		INodeType source = doSwitch(s.getSource());
//		
//		_kind = KIND.LHS;
//		INodeType dest = doSwitch(s.getDest());
//		
//		try {
//			if(!(s.getSource() instanceof GenericInstruction)){
//				if(source instanceof ScalarNode && dest instanceof ScalarNode){
//					ScalarNode src = (ScalarNode) source;
//					ScalarNode dst = (ScalarNode) dest;
//					if(_isSrcDelayNode && _isDestDelayNode){
//						Node delay = _factory.generateDelayNode(s);
//						_factory.createEdge(src.getNode(), delay, 0);
//						_factory.createEdge(delay, dst.getNode());
//					}
//					else{
//						/* FIXME due to the no interpretation of all subgraph interpretable,, the problem can be appeared
//						 * IDFix-Eval need to have in LTI system directly as mult/div predecessor a variable with known value
//						 * and not the subgraph associated.
//						 * When the options is activated, the subgraph is interpreted.
//						 */
//						String annotValue;
//						if( _interpretSetSourceKnownValue && (annotValue = _factory.getValueAnnotation(src.getNode(), "VALUE")) != null){
//							@SuppressWarnings("unused")
//							double value = Double.parseDouble(annotValue);
//							_factory.updateNodeAnnotation(dst.getNode(), "VALUE", annotValue);
//							_factory.updateNodeAnnotation(dst.getNode(), "NATURE", "VARIABLE_VAL_KNOWN");
//							
//						}
//						else{
//							Node set = _factory.generateSetNode(s);
//							_factory.createEdge(src.getNode(), set, 0);
//							_factory.createEdge(set, dst.getNode());
//						}
//					}
//				}
//				else if(source instanceof FieldNode && dest instanceof FieldNode){
//					FieldNode src = (FieldNode) source;
//					FieldNode dst = (FieldNode) dest;
//					Node set;
//					for(Field sf : src.getFields()){
//						for(Field df : dst.getFields()){
//							if(df.equals(sf)){
//								set = _factory.generateSetNode(s);
//								_factory.createEdge(src.getNode(sf), set, 0);
//								_factory.createEdge(set, dst.getNode(df));
//							}
//						}
//					}
//				}
//				else{
//					throw new RuntimeException("Not managed yet");
//				}
//			}
//			else{				
//				if(source instanceof ScalarNode && dest instanceof ScalarNode){
//					_factory.createEdge(((ScalarNode)source).getNode(), ((ScalarNode)dest).getNode(), 0);
//				}
//				else{
//					throw new RuntimeException("Not managed yet");
//				}
//			}
//				
//		} catch (SFGModelCreationException | SFGGeneratorException e) {
//			throw new RuntimeException(e);
//		}
//		
//		return null;
//	}
//
//	@Override
//	public INodeType caseGenericInstruction(GenericInstruction g) {
//		Node op;
//		try {
//			op = _factory.generateGenericOperationNode(g);
//			
//			int numInput = 0;
//			for(Instruction i : g.getChildren()){
//				INodeType node = doSwitch(i);
//				myassert(node instanceof ScalarNode, "A generic instruction have not ScalarNode as children");
//				_factory.createEdge(((ScalarNode)node).getNode(), op, numInput);
//				numInput++;
//			}
//		} catch (SFGGeneratorException | SFGModelCreationException e) {
//			throw new RuntimeException(e);
//		}
//		return new ScalarNode(op);
//	}
//	
//	@Override
//	public INodeType caseSymbolInstruction(SymbolInstruction object) {
//		try {
//			if(_kind == KIND.RHS){
//				_isSrcDelayNode = IDFixUtils.isDelayedVar(object.getSymbol());
//				return _factory.getNode(object.getSymbol());
//			}
//			else{
//				_isDestDelayNode = IDFixUtils.isDelayedVar(object.getSymbol());
//				if(_isDestDelayNode)
//					return _factory.getNode(object.getSymbol());
//				else if(object.getSymbol().getType().isScalarType())
//					return _factory.createScalarNode(object.getSymbol());
//				else if(object.getSymbol().getType().isStruct())
//					return _factory.createFieldNode(object.getSymbol());
//				else
//					throw new SFGGeneratorException("Kind of SymbolInstruction symbol type not managed (" + object.getSymbol() + ")");
//			}
//		} catch (SFGGeneratorException e) {
//			throw new RuntimeException(e);
//		}
//	}
//	
//	@Override
//	public INodeType caseFieldInstruction(FieldInstruction f){
//		try {
//			if(_kind == KIND.RHS){
//				if(f.getChild(0) instanceof SymbolInstruction){
//					return _factory.getNode(((SymbolInstruction) f.getChild(0)).getSymbol(), f.getField());
//				}
//				else if(f.getChild(0) instanceof ArrayInstruction){
//					ArrayInstruction arrInst = (ArrayInstruction) f.getChild(0);
//					List<Long> indices = new ArrayList<>();
//					for(Instruction inst : arrInst.getIndex()){
//						Object o = _arrayIndexInterpreter.doSwitch(inst);
//						myassert(o instanceof Long, "Array index is not a integer (" + o + " : " + o.getClass() + ")");
//						indices.add( (Long) o);
//					}
//					return _factory.getNode(((SymbolInstruction)arrInst.getDest()).getSymbol(), indices, f.getField());
//				}
//				else {
//					throw new SFGGeneratorException("Kind of field variable not managed yet");
//				}
//			}
//			else{
//				if(f.getChild(0) instanceof SymbolInstruction){
//					return _factory.createFieldNode(((SymbolInstruction) f.getChild(0)).getSymbol(), f.getField());
//				}
//				else if(f.getChild(0) instanceof ArrayInstruction){
//					ArrayInstruction arrInst = (ArrayInstruction) f.getChild(0);
//					List<Long> indices = new ArrayList<>();
//					for(Instruction inst : arrInst.getIndex()){
//						Object o = _arrayIndexInterpreter.doSwitch(inst);
//						myassert(o instanceof Long, "Array index is not a integer (" + o + " : " + o.getClass() + ")");
//						indices.add( (Long) o);
//					}
//					return _factory.createIndexedNode(((SymbolInstruction)arrInst.getDest()).getSymbol(), indices, f.getField());
//				}
//				else {
//					throw new SFGGeneratorException("Kind of field variable not managed yet");
//				}
//			}
//		} catch (SFGGeneratorException e) {
//			throw new RuntimeException(e);
//		}
//	}
//	
//	@Override
//	public INodeType caseIntInstruction(IntInstruction g) {
//		return _factory.createConstantNode(g.getValue());
//	}
//
//	@Override
//	public INodeType caseFloatInstruction(FloatInstruction g) {
//		return _factory.createConstantNode(g.getValue());
//	}
//
//	@Override
//	public INodeType caseArrayInstruction(ArrayInstruction i){
//		try{
//			List<Long> indices = new ArrayList<>();
//			for(Instruction inst : i.getIndex()){
//				Object o = _arrayIndexInterpreter.doSwitch(inst);
//				myassert(o instanceof Long, "Array index is not a integer (" + o + " : " + o.getClass() + ")");
//				indices.add( (Long) o);
//			}
//			
//			if(_kind == KIND.RHS){
//				_isSrcDelayNode = IDFixUtils.isDelayedVar(((SymbolInstruction) i.getDest()).getSymbol());
//				return _factory.getNode(((SymbolInstruction) i.getDest()).getSymbol(), indices);
//			}
//			else{
//				ArrayType arrType = (ArrayType)((SymbolInstruction) i.getDest()).getSymbol().getType();
//				_isDestDelayNode = IDFixUtils.isDelayedVar(((SymbolInstruction) i.getDest()).getSymbol());
//				if(_isDestDelayNode)
//					return _factory.getNode(((SymbolInstruction) i.getDest()).getSymbol(), indices);
//				else if(arrType.getBase().isScalarType())
//					return _factory.createIndexedNode(((SymbolInstruction) i.getDest()).getSymbol(), indices);
//				else if(arrType.getBase().isStruct())
//					return _factory.createIndexedFieldNode(((SymbolInstruction) i.getDest()).getSymbol(), indices);
//				else
//					throw new SFGGeneratorException("Kind of SymbolInstruction symbol type not managed");
//			}
//		} catch (SFGGeneratorException e){
//			throw new RuntimeException(e);
//		}
//	}
//
//	@Override
//	public INodeType caseCallInstruction(CallInstruction g) {
//		SymbolInstruction symInst = (SymbolInstruction) g.getAddress();
//		ProcedureSymbol procSym = (ProcedureSymbol) symInst.getSymbol();
//		List<ParameterSymbol> params = procSym.getProcedure().listParameters();
//		
//		try {
//			_factory.generateVariableNode(procSym.getProcedure().listParameters());
//		} catch (SFGGeneratorException | SFGModelCreationException e) {
//			throw new RuntimeException(e);
//		}
//		
//		int index = 1;
//		for(ParameterSymbol paramThe : params){
//			if(paramThe.getType().isScalarType()){
//				Instruction paramEff = g.getChild(index);
//				SetInstruction set = GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(paramThe), paramEff.copy());
//				AnnotateOperators.createAnnotation(set);
//				doSwitch(set);
//			}
//			else if (paramThe.getType().isArray()){
//				Symbol paramEff = ((SymbolInstruction)g.getChild(index)).getSymbol();
//				_factory.updateCallArrayParameters(paramEff, paramThe);
//			}
//			else if (paramThe.getType().isStruct()){
//				Instruction paramEff = g.getChild(index);
//				SetInstruction set = GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(paramThe), paramEff.copy());
//				AnnotateOperators.createAnnotation(set);
//				doSwitch(set);
//				//throw new RuntimeException("Error: Structure type variable not managed yet to call function parameters");
//			}
//			else if (paramThe.getType().isPointer()){
//				throw new RuntimeException("Error: Pointer type variable not managed yet to call function parameters");
//			}
//			else if (paramThe.getType().isAlias()){
//				throw new RuntimeException("Error: Alias type variable not managed yet to call function parameters");
//			}
//			else{
//				throw new RuntimeException("Error: Kind of variable type unknown to call function paramaters");
//			}
//			
//			index++;
//		}
//		ProcedureGenerator procGenerator = new ProcedureGenerator(_factory, _interpretSetSourceKnownValue);
//		INodeType ret = procGenerator.doSwitch(procSym.getProcedure());
//		return ret;
//	}
//
//	@Override
//	public INodeType caseRetInstruction(RetInstruction g) {
//		_kind = KIND.RHS;
//		Instruction ret = g.getExpr();
//		if(ret != null){
//			return doSwitch(ret);
//		}
//		else{
//			return null;
//		}
//	}
//	
//	
//}
