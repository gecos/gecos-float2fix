package fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils;

import java.util.List;

/**
 * Represent the data structure of an array.
 * Mutli-dimensional array are represent under flat form. 
 * @author Nicolas Simon
 * @date Jun 28, 2013
 */
public class SymbolArray<T extends ISymbolType> implements ISymbolType {
	private ISymbolType[] _values;
	
	
	public SymbolArray(int size){
		_values = new ISymbolType[size];
	}
	
	public SymbolArray(List<T> values){				
		_values = new ISymbolType[values.size()];
		for(int i = 0 ; i < values.size() ; i++){
			_values[i] = values.get(i);
		}		
	}
	
	public SymbolArray(SymbolArray<T> other){
		for(int i = 0 ; i < other._values.length ; i++){
			_values[i] = other._values[i];
		}
	}
	
	
	public void setValue(int indice, T value){
		_values[indice] = value;
	}
	
	public ISymbolType getValue(int indice){
		return _values[indice];
	}
	
	public SymbolArray<T> copy(){
		return new SymbolArray<>(this);
	}
	
	public String toString(){
		String ret = "SymbolArray@(";
		for(int i = 0 ; i < _values.length ; i++)
			ret += i + "=" + _values[i] +" ";
		return ret + ")";
	}
}
