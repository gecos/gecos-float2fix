//package fr.irisa.cairn.idfix.frontend.sfg.generator;
//
//import fr.irisa.cairn.gecos.model.tools.switches.DefaultInstructionSwitch;
//import fr.irisa.cairn.idfix.utils.exceptions.SFGGeneratorException;
//import gecos.instrs.CallInstruction;
//import gecos.instrs.FloatInstruction;
//import gecos.instrs.GenericInstruction;
//import gecos.instrs.Instruction;
//import gecos.instrs.IntInstruction;
//import gecos.instrs.SymbolInstruction;
//
///**
// * Interpret {@link Instruction} use as value at the declaration of variable. It can be return a long/double or create a subgraph and return the root.
// * @author Nicolas Simon
// * @date Jul 19, 2013
// */
//public class DeclarationInterpreter extends  DefaultInstructionSwitch<Object>{
//	private SFGFactory _factory;
//	private GenericInstInterpreter _ginstInterpreter;
//	
//	
//	public DeclarationInterpreter(SFGFactory factory){
//		_factory = factory;
//		_ginstInterpreter = new GenericInstInterpreter(this, _factory);
//	}
//	
//	@Override
//	public Object caseIntInstruction(IntInstruction g){
//		return new Long(g.getValue());
//	}
//	
//	@Override
//	public Object caseFloatInstruction(FloatInstruction g){
//		return new Double(g.getValue());
//	}
//	
//	@Override
//	public Object caseGenericInstruction(GenericInstruction g){
//		return _ginstInterpreter.doSwitch(g);
//	}
//	
//	@Override
//	public Object caseSymbolInstruction(SymbolInstruction g){
//		try {
//			return _factory.getNode(g.getSymbol());
//		} catch (SFGGeneratorException e) {
//			throw new RuntimeException(e);
//		}
//	}
//	
//	@Override
//	public Object caseCallInstruction(CallInstruction g){
//		throw new RuntimeException("Error: Declaration initialization via function is not implemented yet");
//	}
//}
