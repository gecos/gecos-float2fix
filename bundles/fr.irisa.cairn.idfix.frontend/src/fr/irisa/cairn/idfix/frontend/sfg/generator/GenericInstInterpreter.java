//package fr.irisa.cairn.idfix.frontend.sfg.generator;
//
//import float2fix.Node;
//import fr.irisa.cairn.gecos.model.tools.switches.DefaultInstructionSwitch;
//import fr.irisa.cairn.gecos.model.tools.switches.GenericInstructionSwitch;
//import fr.irisa.cairn.idfix.utils.exceptions.SFGGeneratorException;
//import fr.irisa.cairn.idfix.utils.exceptions.SFGModelCreationException;
//import gecos.instrs.GenericInstruction;
//import gecos.instrs.Instruction;
//
///**
// * Interpret generic instruction and return when evaluable the value of the instruction or create the subgraph otherwise.
// * @author Nicolas Simon
// * @date Jul 3, 2013
// */
//public class GenericInstInterpreter extends GenericInstructionSwitch<Object> {
//	private DefaultInstructionSwitch<Object> _switcher = null;
//	private SFGFactory _factory;
//	
//	
//	public GenericInstInterpreter(DefaultInstructionSwitch<Object> switcher, SFGFactory factory){
//		_switcher = switcher;
//		_factory = factory;
//	}
//
//	/**
//	 * Evaluates an instruction <i>g</i> and returns its value.
//	 * @param g
//	 * @return the evaluated value
//	 */
//	private Object evaluate(Instruction g) {
//		Object res = _switcher.doSwitch(g);
//		if (res == null){
//			throw new RuntimeException("res est null dans evaluate");
//		}
//		
//		
//		return res;
//	}
//	
//	@Override
//	public Object caseUnknownGeneric(GenericInstruction g) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public Object caseMUL(GenericInstruction g) {
//		Object o1 = evaluate(g.getOperand(0));
//		Object o2 = evaluate(g.getOperand(1));
//		if (o1 instanceof Node && o2 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				_factory.createEdge((Node) o1, op, 0);
//				_factory.createEdge((Node) o2, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if(o1 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				Node c = null;
//				if(o2 instanceof Long){
//					c = _factory.createConstantNode((Long) o2).getNode();
//				}
//				else if (o2 instanceof Double){
//					c = _factory.createConstantNode((Double) o2).getNode();
//				}
//				else{
//					throw new RuntimeException("Type mismatch for MUL operands");
//				}
//		
//				_factory.createEdge((Node) o1, op, 0);
//				_factory.createEdge(c, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if(o2 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				Node c = null;
//				if(o1 instanceof Long){
//					c = _factory.createConstantNode((Long) o1).getNode();
//				}
//				else if (o1 instanceof Double){
//					c = _factory.createConstantNode((Double) o1).getNode();
//				}
//				else{
//					throw new RuntimeException("Type mismatch for MUL operands");
//				}
//		
//				_factory.createEdge(c, op, 0);
//				_factory.createEdge((Node) o2, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if (o1 instanceof Long && o2 instanceof Long){
//			return new Long((Long) o1 * (Long) o2);
//		}
//		else if (o1 instanceof Long && o2 instanceof Double){
//			return new Double((Long) o1 * (Double) o2);
//		}
//		else if (o1 instanceof Double && o2 instanceof Long){
//			return new Double((Double) o1 * (Long) o2);
//		}
//		else if  (o1 instanceof Double && o2 instanceof Double){
//			return new Double((Double) o1 * (Double) o2);
//		}
//		else {
//			throw new RuntimeException("Type mismatch for MUL operands");
//		}
//	}
//	
//	@Override
//	public Object caseADD(GenericInstruction g) {
//		Object o1 = evaluate(g.getOperand(0));
//		Object o2 = evaluate(g.getOperand(1));
//		if (o1 instanceof Node && o2 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				_factory.createEdge((Node) o1, op, 0);
//				_factory.createEdge((Node) o2, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if(o1 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				Node c = null;
//				if(o2 instanceof Long){
//					c = _factory.createConstantNode((Long) o2).getNode();
//				}
//				else if (o2 instanceof Double){
//					c = _factory.createConstantNode((Double) o2).getNode();
//				}
//				else{
//					throw new RuntimeException("Type mismatch for ADD operands");
//				}
//		
//				_factory.createEdge((Node) o1, op, 0);
//				_factory.createEdge(c, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if(o2 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				Node c = null;
//				if(o1 instanceof Long){
//					c = _factory.createConstantNode((Long) o1).getNode();
//				}
//				else if (o1 instanceof Double){
//					c = _factory.createConstantNode((Double) o1).getNode();
//				}
//				else{
//					throw new RuntimeException("Type mismatch for ADD operands");
//				}
//		
//				_factory.createEdge(c, op, 0);
//				_factory.createEdge((Node) o2, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if (o1 instanceof Long && o2 instanceof Long){
//			return new Long((Long) o1 + (Long) o2);
//		}
//		else if (o1 instanceof Long && o2 instanceof Double){
//			return new Double((Long) o1 + (Double) o2);
//		}
//		else if (o1 instanceof Double && o2 instanceof Long){
//			return new Double((Double) o1 + (Long) o2);
//		}
//		else if  (o1 instanceof Double && o2 instanceof Double){
//			return new Double((Double) o1 + (Double) o2);
//		}
//		else {
//			throw new RuntimeException("Type mismatch for ADD operands");
//		}
//	}
//	
//	@Override
//	public Object caseSUB(GenericInstruction g) {
//		Object o1 = evaluate(g.getOperand(0));
//		Object o2 = evaluate(g.getOperand(1));
//		if (o1 instanceof Node && o2 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				_factory.createEdge((Node) o1, op, 0);
//				_factory.createEdge((Node) o2, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if(o1 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				Node c = null;
//				if(o2 instanceof Long){
//					c = _factory.createConstantNode((Long) o2).getNode();
//				}
//				else if (o2 instanceof Double){
//					c = _factory.createConstantNode((Double) o2).getNode();
//				}
//				else{
//					throw new RuntimeException("Type mismatch for SUB operands");
//				}
//		
//				_factory.createEdge((Node) o1, op, 0);
//				_factory.createEdge(c, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if(o2 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				Node c = null;
//				if(o1 instanceof Long){
//					c = _factory.createConstantNode((Long) o1).getNode();
//				}
//				else if (o1 instanceof Double){
//					c = _factory.createConstantNode((Double) o1).getNode();
//				}
//				else{
//					throw new RuntimeException("Type mismatch for SUB operands");
//				}
//		
//				_factory.createEdge(c, op, 0);
//				_factory.createEdge((Node) o2, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if (o1 instanceof Long && o2 instanceof Long){
//			return new Long((Long) o1 - (Long) o2);
//		}
//		else if (o1 instanceof Long && o2 instanceof Float){
//			return new Float((Long) o1 - (Float) o2);
//		}
//		else if (o1 instanceof Float && o2 instanceof Long){
//			return new Float((Float) o1 - (Long) o2);
//		}
//		else if  (o1 instanceof Float && o2 instanceof Float){
//			return new Float((Float) o1 - (Float) o2);
//		}
//		else {
//			throw new RuntimeException("Type mismatch for SUB operands");
//		}
//	}
//	
//	@Override
//	public Object caseEQ(GenericInstruction g) {
//		throw new RuntimeException("Error: EQ operator don't managa at the variable initialization");
//	}
//	
//	
//	@Override
//	public Object caseNE(GenericInstruction g) {
//		throw new RuntimeException("Error: NE operator don't managa at the variable initialization");
//	}
//
//	@Override
//	public Object caseGT(GenericInstruction g) {
//		throw new RuntimeException("Error: GT operator don't managa at the variable initialization");
//	}
//
//	@Override
//	public Object caseGE(GenericInstruction g) {
//		throw new RuntimeException("Error: GE operator don't managa at the variable initialization");
//	}
//
//	@Override
//	public Object caseLT(GenericInstruction g) {
//		throw new RuntimeException("Error: LT operator don't managa at the variable initialization");
//	}
//	
//	@Override
//	public Object caseLE(GenericInstruction g) {
//		throw new RuntimeException("Error: LE operator don't managa at the variable initialization");
//	}
//	
//	@Override
//	public Object caseLNOT(GenericInstruction g) {
//		throw new RuntimeException("Error: LNOT operator don't managa at the variable initialization");
//	}
//	
//	@Override
//	public Object caseNEG(GenericInstruction g) {
//		Object o = evaluate(g.getOperand(0));
//		
//		if (o instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				_factory.createEdge((Node) o, op, 0);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if (o instanceof Long){
//			return new Long(-(Long) o);
//		}
//		else if (o instanceof Double){
//			return new Double(-(Double) o);
//		}
//		else {
//			throw new RuntimeException("Type mismatch for NEG operand");
//		}
//	}
//	
//	@Override
//	public Object caseXOR(GenericInstruction g) {
//		Object o1 = evaluate(g.getOperand(0));
//		Object o2 = evaluate(g.getOperand(1));
//		if (o1 instanceof Node && o2 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				_factory.createEdge((Node) o1, op, 0);
//				_factory.createEdge((Node) o2, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if(o1 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				Node c = null;
//				if(o2 instanceof Long){
//					c = _factory.createConstantNode((Long) o2).getNode();
//				}
//				else{
//					throw new RuntimeException("Type mismatch for MUL operands");
//				}
//		
//				_factory.createEdge((Node) o1, op, 0);
//				_factory.createEdge(c, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if(o2 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				Node c = null;
//				if(o1 instanceof Long){
//					c = _factory.createConstantNode((Long) o1).getNode();
//				}
//				else{
//					throw new RuntimeException("Type mismatch for MUL operands");
//				}
//		
//				_factory.createEdge(c, op, 0);
//				_factory.createEdge((Node) o2, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if (o1 instanceof Long && o2 instanceof Long){
//			return new Long((Long) o1 ^ (Long) o2);
//		}
//		else {
//			throw new RuntimeException("Type mismatch for XOR operands");
//		
//		}
//	}
//	
//	@Override
//	public Object caseSHR(GenericInstruction g) {
//		Object o1 = evaluate(g.getOperand(0));
//		Object o2 = evaluate(g.getOperand(1));
//		if (o1 instanceof Node && o2 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				_factory.createEdge((Node) o1, op, 0);
//				_factory.createEdge((Node) o2, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if(o1 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				Node c = null;
//				if(o2 instanceof Long){
//					c = _factory.createConstantNode((Long) o2).getNode();
//				}
//				else{
//					throw new RuntimeException("Type mismatch for MUL operands");
//				}
//		
//				_factory.createEdge((Node) o1, op, 0);
//				_factory.createEdge(c, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if(o2 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				Node c = null;
//				if(o1 instanceof Long){
//					c = _factory.createConstantNode((Long) o1).getNode();
//				}
//				else{
//					throw new RuntimeException("Type mismatch for MUL operands");
//				}
//		
//				_factory.createEdge(c, op, 0);
//				_factory.createEdge((Node) o2, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if (o1 instanceof Long && o2 instanceof Long){
//			return new Long((Long) o1 >> (Long) o2);
//		}
//		else {
//			throw new RuntimeException("Type mismatch for SHR operands");
//		}
//	}
//	
//	@Override
//	public Object caseSHL(GenericInstruction g) {
//		Object o1 = evaluate(g.getOperand(0));
//		Object o2 = evaluate(g.getOperand(1));
//		if (o1 instanceof Node && o2 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				_factory.createEdge((Node) o1, op, 0);
//				_factory.createEdge((Node) o2, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if(o1 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				Node c = null;
//				if(o2 instanceof Long){
//					c = _factory.createConstantNode((Long) o2).getNode();
//				}
//				else{
//					throw new RuntimeException("Type mismatch for MUL operands");
//				}
//		
//				_factory.createEdge((Node) o1, op, 0);
//				_factory.createEdge(c, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if(o2 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				Node c = null;
//				if(o1 instanceof Long){
//					c = _factory.createConstantNode((Long) o1).getNode();
//				}
//				else{
//					throw new RuntimeException("Type mismatch for MUL operands");
//				}
//		
//				_factory.createEdge(c, op, 0);
//				_factory.createEdge((Node) o2, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if (o1 instanceof Long && o2 instanceof Long){
//			return new Long((Long) o1 << (Long) o2);
//		}
//		else {
//			throw new RuntimeException("Type mismatch for SHL operands");
//		}
//	}
//	
//	@Override
//	public Object caseOR(GenericInstruction g) {
//		Object o1 = evaluate(g.getOperand(0));
//		Object o2 = evaluate(g.getOperand(1));
//		if (o1 instanceof Node && o2 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				_factory.createEdge((Node) o1, op, 0);
//				_factory.createEdge((Node) o2, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if(o1 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				Node c = null;
//				if(o2 instanceof Long){
//					c = _factory.createConstantNode((Long) o2).getNode();
//				}
//				else{
//					throw new RuntimeException("Type mismatch for MUL operands");
//				}
//		
//				_factory.createEdge((Node) o1, op, 0);
//				_factory.createEdge(c, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if(o2 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				Node c = null;
//				if(o1 instanceof Long){
//					c = _factory.createConstantNode((Long) o1).getNode();
//				}
//				else{
//					throw new RuntimeException("Type mismatch for MUL operands");
//				}
//		
//				_factory.createEdge(c, op, 0);
//				_factory.createEdge((Node) o2, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if (o1 instanceof Long && o2 instanceof Long){
//			return new Long((Long) o1 | (Long) o2);
//		}
//		else {
//			throw new RuntimeException("Type mismatch for Bitwise OR operands");
//		}
//	}
//	
//	@Override
//	public Object caseMOD(GenericInstruction g) {
//		Object o1 = evaluate(g.getOperand(0));
//		Object o2 = evaluate(g.getOperand(1));
//		if (o1 instanceof Node && o2 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				_factory.createEdge((Node) o1, op, 0);
//				_factory.createEdge((Node) o2, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if(o1 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				Node c = null;
//				if(o2 instanceof Long){
//					c = _factory.createConstantNode((Long) o2).getNode();
//				}
//				else{
//					throw new RuntimeException("Type mismatch for MUL operands");
//				}
//		
//				_factory.createEdge((Node) o1, op, 0);
//				_factory.createEdge(c, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if(o2 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				Node c = null;
//				if(o1 instanceof Long){
//					c = _factory.createConstantNode((Long) o1).getNode();
//				}
//				else{
//					throw new RuntimeException("Type mismatch for MUL operands");
//				}
//		
//				_factory.createEdge(c, op, 0);
//				_factory.createEdge((Node) o2, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if (o1 instanceof Long && o2 instanceof Long){
//			return new Long((Long) o1 % (Long) o2);
//		}
//		else {
//			throw new RuntimeException("Type mismatch for MOD operands");
//		}
//	}
//	
//	@Override
//	public Object caseDIV(GenericInstruction g) {
//		Object o1 = evaluate(g.getOperand(0));
//		Object o2 = evaluate(g.getOperand(1));
//		if (o1 instanceof Node && o2 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				_factory.createEdge((Node) o1, op, 0);
//				_factory.createEdge((Node) o2, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if(o1 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				Node c = null;
//				if(o2 instanceof Long){
//					c = _factory.createConstantNode((Long) o2).getNode();
//				}
//				else if (o2 instanceof Double){
//					c = _factory.createConstantNode((Double) o2).getNode();
//				}
//				else{
//					throw new RuntimeException("Type mismatch for MUL operands");
//				}
//		
//				_factory.createEdge((Node) o1, op, 0);
//				_factory.createEdge(c, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if(o2 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				Node c = null;
//				if(o1 instanceof Long){
//					c = _factory.createConstantNode((Long) o1).getNode();
//				}
//				else if (o1 instanceof Double){
//					c = _factory.createConstantNode((Double) o1).getNode();
//				}
//				else{
//					throw new RuntimeException("Type mismatch for MUL operands");
//				}
//		
//				_factory.createEdge(c, op, 0);
//				_factory.createEdge((Node) o2, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if (o1 instanceof Long && o2 instanceof Long){
//			return new Long((Long) o1 / (Long) o2);
//		}
//		else if (o1 instanceof Long && o2 instanceof Double){
//			return new Double((Long) o1 / (Double) o2);
//		}
//		else if (o1 instanceof Double && o2 instanceof Long){
//			return new Double((Double) o1 / (Long) o2);
//		}
//		else if  (o1 instanceof Double && o2 instanceof Double){
//			return new Double((Double) o1 / (Double) o2);
//		}
//		else {
//			throw new RuntimeException("Type mismatch for DIV operands");
//		}
//	}
//	
//	@Override
//	public Object caseAND(GenericInstruction g) {
//		Object o1 = evaluate(g.getOperand(0));
//		Object o2 = evaluate(g.getOperand(1));
//		if (o1 instanceof Node && o2 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				_factory.createEdge((Node) o1, op, 0);
//				_factory.createEdge((Node) o2, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if(o1 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				Node c = null;
//				if(o2 instanceof Long){
//					c = _factory.createConstantNode((Long) o2).getNode();
//				}
//				else{
//					throw new RuntimeException("Type mismatch for MUL operands");
//				}
//		
//				_factory.createEdge((Node) o1, op, 0);
//				_factory.createEdge(c, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if(o2 instanceof Node){
//			try {
//				Node op = _factory.generateGenericOperationNode(g);
//				Node c = null;
//				if(o1 instanceof Long){
//					c = _factory.createConstantNode((Long) o1).getNode();
//				}
//				else{
//					throw new RuntimeException("Type mismatch for MUL operands");
//				}
//		
//				_factory.createEdge(c, op, 0);
//				_factory.createEdge((Node) o2, op, 1);
//				return op;
//			} catch (SFGGeneratorException | SFGModelCreationException e) {
//				throw new RuntimeException(e);
//			}
//		}
//		else if (o1 instanceof Long && o2 instanceof Long){
//			return new Long((Long) o1 & (Long) o2);
//		}
//		else {
//			throw new RuntimeException("Type mismatch for Bitwise AND operands");
//		}
//	}
//
//	@Override
//	public Object caseARRAY(GenericInstruction g) {
//		throw new UnsupportedOperationException("Not yet implemented");
//	}
//
//}
