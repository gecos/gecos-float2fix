package fr.irisa.cairn.idfix.frontend.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.tools.switches.BasicBlockSwitch;
import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixProject;
import gecos.annotations.PragmaAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.ForBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.Symbol;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.SymbolInstruction;

/**
 * Detect the pragma "CELL name number" apply to a simple for block (for(a = 0 ; a < n ; a++)).
 * This pragma  replace the "name" which represent the index of the loop by the "number" given.
 * This number must be positive
 * @author Nicolas Simon
 *
 */
public class CellManagement {
	private IdfixProject _idfixProject;
	
	public CellManagement(IdfixProject idfixProject){
		_idfixProject = idfixProject;
	}
	
	public void compute(){
		for(ProcedureSet ps : _idfixProject.getGecosProject().listProcedureSets()){
			for(Procedure p : ps.listProcedures()){
				new CellDetection().doSwitch(p.getBody());	
			}
		}
	}
	
	private class CellDetection extends BasicBlockSwitch<Object> {
		@Override
		public Object caseBasicBlock(BasicBlock b) {
			return null;
		}

		@Override
		public Object caseForBlock(ForBlock b) {
			super.caseForBlock(b);
			
			// Pragma detection
			String variableName = "";
			PragmaAnnotation pragma = b.getPragma();
			if(pragma != null){
				for(String content : pragma.getContent()){
					Pattern pattern = Pattern.compile("CELL\\s([a-zA-Z]\\w*)\\s([0-9]+)");
					Matcher matcher = pattern.matcher(content);
					if(matcher.find()){
						variableName = matcher.group(1);
						int cellNumber = Integer.parseInt(matcher.group(2));
						if(cellNumber < 0)
							throw new RuntimeException("Value of the pragma CELL must be positive because it can represent an array index");
						new CheckIfVariableExistInForBlock(variableName).check(b.getTestBlock());
						new ReplaceSymbolInstructionToIntInstruction(variableName, cellNumber).doSwitch(b.getBodyBlock());
						b.getParent().replace(b, b.getBodyBlock());
					}
				}				
			}
			
			return null;
		}
	}
	
	/**
	 * Check if the "variable" given by the pragma CELL exist inside the block given as parameter.
	 * @author Nicolas Simon
	 */
	private class CheckIfVariableExistInForBlock extends BlockInstructionSwitch<Object> {
		private String _variableName;
		private boolean _exist = false;

		public CheckIfVariableExistInForBlock(String variableName) {
			_variableName = variableName;
		}
		
		public void check(Block b){
			doSwitch(b);
			if(!_exist)
				throw new RuntimeException("Pragma CELL use a variable name not found in the for block process");
		}
		
		@Override
		public Object caseSymbolInstruction(SymbolInstruction object) {
			if(object.getSymbolName().equals(_variableName)){
				_exist = true;
			}
			return super.caseSymbolInstruction(object);
		}
	}
	
	/**
	 * Replace all {@link SymbolInstruction} which is pointed to the {@link Symbol} with the same variable name
	 * given by the pragma CELL. The new instruction is a {@link IntInstruction}. 
	 * @author Nicolas Simon
	 *
	 */
	private class ReplaceSymbolInstructionToIntInstruction extends BlockInstructionSwitch<Object> {
		private String _variableName;
		private int _cellNumber;
		
		public ReplaceSymbolInstructionToIntInstruction(String variableName, int cellNumber){
			_variableName = variableName;
			_cellNumber = cellNumber;
		}

		@Override
		public Object caseSymbolInstruction(SymbolInstruction object) {
			if(_variableName.equals(object.getSymbolName())){
				ComplexInstruction parent;
				if((parent = object.getParent()) != null){
					parent.replaceChild(object, GecosUserInstructionFactory.Int(_cellNumber));
				}
			}
			return null;
		}		
	}
}
