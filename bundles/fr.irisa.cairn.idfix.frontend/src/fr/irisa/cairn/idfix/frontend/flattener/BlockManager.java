
package fr.irisa.cairn.idfix.frontend.flattener;

import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.tools.switches.BasicBlockSwitch;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksFactory;
import gecos.blocks.CompositeBlock;
import gecos.instrs.Instruction;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This class gathers operations dealing with the management of blocks in the flatten final code.
 * Since we copy non evaluable condition in "if", the final code may not be completely "flat", and
 * can contain several nested composite blocks.
 * @author qmeunier
 */
class BlockManager {
	
	public BlockManager(){}
	
	private BasicBlock currBasicBlock = null;
	private CompositeBlock currCompositeBlock = null;
	private CompositeBlock mainBlock;
	
	
	public void setCurrCompositeBlock(CompositeBlock compositeBlock){
		this.currCompositeBlock = compositeBlock;
		currBasicBlock = null;
	}
	
	public CompositeBlock getCurrCompositeBlock(){
		return currCompositeBlock;
	}
	
	
	public void addInstruction(Instruction inst){
		if (currCompositeBlock == null){
			currCompositeBlock = GecosUserBlockFactory.CompositeBlock();
		}
		if (currBasicBlock == null){
			currBasicBlock = BlocksFactory.eINSTANCE.createBasicBlock();
			currCompositeBlock.addChildren(currBasicBlock);
		}
		//System.out.println("Ajout de l'instruction " + inst);
		currBasicBlock.addInstruction(inst);
	}
	
	public void addToCurrCompositeBlock(Block b){
		if (currCompositeBlock == null){
			currCompositeBlock = GecosUserBlockFactory.CompositeBlock();
		}
		currCompositeBlock.addChildren(b);
	}
	
	public void setMainBlock(CompositeBlock b){
		mainBlock = b;
	}
	
	public CompositeBlock getMainBlock(){
		return mainBlock;
	}
	
	
	public void mergeBasicBlocksWithoutScope(){
		BlockMergerVisitor merger = new BlockMergerVisitor();
		merger.doSwitch(mainBlock);
	}
	
	
	private class BlockMergerVisitor extends BasicBlockSwitch<Object> {
		
		private boolean containsOnlySingleBB(CompositeBlock b){
			if (b.getScope().getSymbols().size() == 0 &&
			((CompositeBlock) b).getChildren().size() == 1 &&
			((CompositeBlock) b).getChildren().get(0) instanceof BasicBlock){
				return true;
			}
			else {
				return false;
			}
		}
		
		private void addToRemove(Map<CompositeBlock, List<Block>> toRemove, CompositeBlock b, Block elem){
			if (toRemove.get(b) == null){
				List<Block> list = new LinkedList<Block>();
				list.add(elem);
				toRemove.put(b, list);
			}
			else {
				toRemove.get(b).add(elem);
			}
		}
		
		/**
		 * Algo non trivial. On effectue la fusion récursive des CompositeBlocks et des BasicBlocks.
		 */
		@Override
		public Object caseCompositeBlock(CompositeBlock b){
			boolean firstPass = true;
			Map<CompositeBlock, List<Block>> toRemove = new LinkedHashMap<CompositeBlock, List<Block>>();
			Block child1 = null;
			for (Block child2 : b.getChildren()){
				if (child2 instanceof CompositeBlock){
					doSwitch(child2);
				}
				if (!firstPass){
					// Condition de fusion : les deux blocks sont soit des CompositeBlocks avec scope vide et dont tous les fils sont des BasicB, soit des BasicB.
					// Dans le cas d'une fusion, on ne change pas child1
					if (child1 instanceof CompositeBlock && child2 instanceof CompositeBlock &&
							containsOnlySingleBB((CompositeBlock) child1) &&
							containsOnlySingleBB((CompositeBlock) child2)){
						BasicBlock bb1 = (BasicBlock) ((CompositeBlock) child1).getChildren().get(0);
						BasicBlock bb2 = (BasicBlock) ((CompositeBlock) child2).getChildren().get(0);
						for (Instruction inst : bb2.getInstructions()){
							bb1.addInstruction(inst.copy());
						}
						addToRemove(toRemove, b, child2);
					}
					else if (child1 instanceof BasicBlock && child2 instanceof CompositeBlock &&
							containsOnlySingleBB((CompositeBlock) child2)){
						BasicBlock bb2 = (BasicBlock) ((CompositeBlock) child2).getChildren().get(0);
						for (Instruction inst : bb2.getInstructions()){
							((BasicBlock) child1).addInstruction(inst.copy());
						}
						addToRemove(toRemove, b, child2);
					}
					else if (child2 instanceof BasicBlock && child1 instanceof CompositeBlock &&
							containsOnlySingleBB((CompositeBlock) child1)){
						BasicBlock bb1 = (BasicBlock) ((CompositeBlock) child1).getChildren().get(0);
						for (Instruction inst : ((BasicBlock) child2).getInstructions()){
							bb1.addInstruction(inst.copy());
						}
						addToRemove(toRemove, b, child2);
					}
					else if (child1 instanceof BasicBlock && child2 instanceof BasicBlock){
						for (Instruction inst : ((BasicBlock) child2).getInstructions()){
							((BasicBlock) child1).addInstruction(inst.copy());
						}
						addToRemove(toRemove, b, child2);
					}
					else {
						child1 = child2;
					}
				}
				else {
					child1 = child2;
					firstPass = false;
				}
			}
			// On supprime les blocks recopiés
			for (CompositeBlock parent : toRemove.keySet()){
				for (Block child : toRemove.get(parent)){
					parent.removeBlock(child);
				}
			}
			// Enfin, si le CompositeBlock ne contient plus qu'un CompositeBlock sans Scope qui ne contient qu'un BasicBlock,
			// on remplace le fils unique par ce BasicBlock
			if (b.getChildren().size() == 1 && b.getChildren().get(0) instanceof CompositeBlock &&
					containsOnlySingleBB((CompositeBlock) b.getChildren().get(0))){
				b.getChildren().set(0, ((CompositeBlock) b.getChildren().get(0)).getChildren().get(0));
			}
			return null;
		}

		@Override
		public Object caseBasicBlock(BasicBlock b) {
			return null;
		}
	
	}
	
}