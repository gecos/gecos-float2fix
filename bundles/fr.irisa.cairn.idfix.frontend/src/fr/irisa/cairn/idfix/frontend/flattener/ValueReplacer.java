package fr.irisa.cairn.idfix.frontend.flattener;


import java.util.ArrayList;

import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import gecos.core.ProcedureSymbol;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.InstrsFactory;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.AliasType;
import gecos.types.ArrayType;
import gecos.types.BaseType;
import gecos.types.Type;

/**
 * Replaces known variables by their values in an instruction copied to the flattened source.
 * @author qmeunier
 *
 */
public class ValueReplacer extends BlockInstructionSwitch<Object> {

	private ExecutionContext context;
	private ExpFlattener expFlattener;
	private boolean forceInterpreting;
	
	public ValueReplacer(ExecutionContext context, ExpFlattener expFlattener){
		this.context = context;
		this.expFlattener = new ExpFlattener(this);
		this.forceInterpreting = true;
		setMode(RHS);
	}
	
	public ValueReplacer(ExecutionContext context, ExpFlattener expFlattener, boolean forceInterpreting){
		this.context = context;
		this.expFlattener = new ExpFlattener(this);
		this.forceInterpreting = forceInterpreting;
		setMode(RHS);
	}
	

	private void myassert(boolean b, String s) throws FlattenerException{
		if (!b){
			throw new FlattenerException(s);
		}
	}


	public final static int LHS = 1;
	public final static int RHS = 2;
	private int mode;

	private void setMode(int mode) {
		this.mode = mode;
	}

	private int getMode() {
		return mode;
	}
	

	@Override
	public Object caseSetInstruction(SetInstruction s) {
		setMode(LHS);
		doSwitch(s.getDest());
		setMode(RHS);
		doSwitch(s.getSource()); 
		return null;
	}
	
	@Override
	public Object caseCallInstruction(CallInstruction g) {
		for (Instruction inst : g.getArgs()){
			doSwitch(inst);
		}
		return new UninitializedValue();
	}

	/**
	 * Si le symbole est connu, on le remplace par une instruction int ou float de la valeur
	 */
	@Override
	public Object caseSymbolInstruction(SymbolInstruction s){
		try{
			myassert(!(s.getSymbol() instanceof ProcedureSymbol), "");
			
			Object res = null;
			if (!(s.getSymbol().getType() instanceof ArrayType) && getMode() == RHS){
				// symboles de tableaux : paramètres d'appels de fonction (sinon caseArray)
				Instruction inst = null;
				res = this.context.getValue(s.getSymbol());
				if (res instanceof Long){
					inst = InstrsFactory.eINSTANCE.createIntInstruction();
					inst.setType(s.getType());
					((IntInstruction) inst).setValue((Long) res);
				}
				else if (res instanceof Float){
					inst = InstrsFactory.eINSTANCE.createFloatInstruction();
					inst.setType(s.getType());
					((FloatInstruction) inst).setValue((Float) res);
				}
				else if (!(res instanceof UninitializedValue)){
					throw new RuntimeException("Classe de la valeur : " + res.getClass());
				}
				if (inst != null && this.forceInterpreting){
					ComplexInstruction parent = s.getParent();
//					int instIndex = 0;
//					for (Instruction child : parent.getChildren()){
//						if (child == s){
//							break;
//						}
//						instIndex++;
//					}
//					parent.getChildren().set(instIndex, inst);
					parent.replaceChild(s, inst);
				}
			}
			
			return res;
		}catch (FlattenerException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	
	@Override
	public Object caseArrayInstruction(ArrayInstruction g){
		try{
			boolean replaceByLitteral = false;
			Object res = null;
			Instruction newInst = null;
			if (getMode() == LHS){
				/* On repasse en RHS pour l'évaluation des indices du tableau */
				setMode(RHS);
				for (Instruction inst : g.getIndex()){
					Object o = doSwitch(inst);
					myassert(o instanceof Long, "Indice de tableau non entier");
				}
				setMode(LHS);
			}
			else {
				ArrayList<Long> indices = new ArrayList<Long>();
				for (Instruction inst : g.getIndex()){
					Object o = doSwitch(inst);
					myassert(o instanceof Long, "Indice de tableau non entier dans l'instruction " + g);
					indices.add((Long) o);
				}
				
				res = context.getValue(((SymbolInstruction) g.getDest()).getSymbol(), indices);
				if (res instanceof Long){
					newInst = InstrsFactory.eINSTANCE.createIntInstruction();
					((IntInstruction) newInst).setValue((Long) res);
					newInst.setType(g.getType());
					replaceByLitteral = true;
				}
				else if (res instanceof Float){
					newInst = InstrsFactory.eINSTANCE.createFloatInstruction();
					((FloatInstruction) newInst).setValue((Float) res);
					newInst.setType(g.getType());
					replaceByLitteral = true;
				}
				else if (!(res instanceof UninitializedValue)){
					throw new RuntimeException("Classe de la valeur : " + res.getClass());
				}
			}
	
			if (replaceByLitteral && this.forceInterpreting){
				ComplexInstruction parent = g.getParent();
//				int i = 0;
//				for (Instruction child : parent.getChildren()){
//					if (child == g){
//						break;
//					}
//					i++;
//				}
//				parent.getChildren().set(i, newInst);
				parent.replaceChild(g, newInst);
			}
			return res;
		}catch (FlattenerException e){
			throw new RuntimeException(e);
		}
	}


	@Override
	public Object caseIntInstruction(IntInstruction g) {
		return new Long(g.getValue());
	}
	
	@Override
	public Object caseFloatInstruction(FloatInstruction g) {
		return new Float(g.getValue());
	}

	@Override
	public Object caseGenericInstruction(GenericInstruction g) {
		Object res = expFlattener.doSwitch(g);
		Instruction inst = null;
		boolean update = false;
		if (res instanceof Long){
			inst = InstrsFactory.eINSTANCE.createIntInstruction();
			((IntInstruction) inst).setValue((Long) res);
			inst.setType(g.getType());
			update = true;
		}
		else if (res instanceof Float){
			inst = InstrsFactory.eINSTANCE.createFloatInstruction();
			inst.setType(g.getType());
			((FloatInstruction) inst).setValue((Float) res);
			update = true;
		}
		else if (res instanceof Boolean){
			inst = InstrsFactory.eINSTANCE.createIntInstruction();
			inst.setType(g.getType());
			if ((Boolean) res){
				((IntInstruction) inst).setValue(1);
			}
			else {
				((IntInstruction) inst).setValue(0);
			}
			update = true;
		}
		else if (!(res instanceof UninitializedValue)){
			throw new RuntimeException("Classe de la valeur : " + res.getClass());
		}
		if (update){
			ComplexInstruction parent = g.getParent();
//			int i = 0;
//			for (Instruction child : parent.getChildren()){
//				if (child == g){
//					break;
//				}
//				i++;
//			}
//			parent.getChildren().set(i, inst);
			parent.replaceChild(g, inst);
		}
		return res;
	}
	
//	@Override
//	public Object caseConvertInstruction(ConvertInstruction g){
//		Object o = doSwitch(g.getExpr());
//		return o;
//	}
	@Override
	public Object caseConvertInstruction(ConvertInstruction g){
		Object res = doSwitch(g.getExpr());
		Type castType = g.getType();
		
		if(castType instanceof AliasType) {
			castType = ((AliasType) castType).getAlias();
		}
		
		if (res instanceof Long){
			if(castType instanceof BaseType && ((BaseType)castType).asFloat() != null) {
				res = Float.valueOf(((Long)res).floatValue());
			}
		}
		else if (res instanceof Float){
			if(castType instanceof BaseType && ((BaseType)castType).asInt() != null) {
				res = Long.valueOf(((Float)res).longValue());
			}
		}
		
		return res;
	}
	
}
