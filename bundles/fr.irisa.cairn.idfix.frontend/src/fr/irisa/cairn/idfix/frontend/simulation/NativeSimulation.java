package fr.irisa.cairn.idfix.frontend.simulation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import fr.irisa.cairn.idfix.utils.TraceManager;

class NativeSimulation {	
	private TraceManager traceManager;
	
	public NativeSimulation(TraceManager traceManager, String libPath){
		this.traceManager = traceManager;
		loadSimulationLibrary(libPath);
	}
	
	private native void runSimulation();
	
	public void simulate(){
		runSimulation();
	}
	
	
	private static boolean loadSimulationLibrary(String libName){
		try {
			File f = new File(libName);
			InputStream inputStream = f.toURI().toURL().openStream();
			
			// Copy resource to file system in a temp folder with a unique name
			File temporaryDll = File.createTempFile((new File(libName)).getName(),".tmp");
			FileOutputStream outputStream = new FileOutputStream(temporaryDll);
			byte [] array = new byte[(int) f.length() + 1];
			int read = 0;
			while ((read = inputStream.read(array)) > 0){
				outputStream.write(array, 0, read);
			}
			outputStream.close();
			temporaryDll.deleteOnExit();

			// Finally, load the dll
			System.load(temporaryDll.getAbsolutePath());
		} catch(Throwable e) {
			e.printStackTrace();
			System.err.println("*** Error: while loading library " + libName);
			System.exit(1);
			return false;
		}
		//System.out.println("Library " + libName + " loaded with success.");
		return true;
	}
	
	public void initiateExecution(){
		traceManager.initiateExecution();
	}

	public void traceBlock(int a){
		traceManager.traceBlock(a);
	}

	public void terminateExecution(){
		traceManager.terminateExecution();
	}
	
	public void traceAffectationValue(int numAffect, double value){
		traceManager.traceAffectationValue(numAffect, value);
	}
	
}
