package fr.irisa.cairn.idfix.frontend.interpreter;

/**
 * This class is used to store the current value and node (in SFG) of a symbol
 * For array compatibility, vectors are used, but it is supposed that arrays are linearized before using this class
 * @author qmeunier
 */
import java.util.Vector;


public class SymbolValue {
	
	private Vector<Object> values;
	private Vector<Object> nodes;
	
	private void myassert(boolean b, String s){
		if (!b){
			throw new RuntimeException(s);
		}
	}
	
	public SymbolValue(Vector<Object> v){
		nodes = new Vector<Object>();
		values = new Vector<Object>();
		if (v != null){
			for (int i = 0; i < v.size(); i++){
				if (v.get(i) == null){
					values.add(new UninitializedValue());
				}
				else {
					values.add(v.get(i));
				}
				nodes.add(new UninitializedNode());
			}
		}
		else {
			values.add(new UninitializedValue());
			nodes.add(new UninitializedNode());
		}
	}
	
	public SymbolValue copy(){
		Vector<Object> init = new Vector<Object>();
		for (int i = 0; i < this.getNbNodes(); i++){
			init.add(null);
		}
		SymbolValue res = new SymbolValue(init);
		for (int i = 0; i < this.getNbNodes(); i++){
			res.setNodeAt(this.getNodeAt(i), i);
		}
		for (int i = 0; i < this.getNbValues(); i++){
			res.setValueAt(this.getValueAt(i), i);
		}
		return res;
	}
	
	public void setValueAt(Object o, long n){
		values.set((int) n, o);
	}
	
	public Object getValueAt(long n){
		myassert(values.get((int) n) != null, "Error 91 : The value of a SymbolValue is null");
		return values.get((int) n);
	}
	
	public int getNbValues(){
		return values.size();
	}
	
	public void setNodeAt(Object node, long n){
		nodes.set((int) n, node);
	}
	
	public Object getNodeAt(long n){
		myassert(nodes.get((int) n) != null, "Error 92 : The node of a SymbolValue is null");
		return nodes.get((int) n);
	}
	
	public int getNbNodes(){
		return nodes.size();
	}
	
}