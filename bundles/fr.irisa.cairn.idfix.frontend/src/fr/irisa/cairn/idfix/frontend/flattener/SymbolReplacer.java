package fr.irisa.cairn.idfix.frontend.flattener;

import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import gecos.core.ProcedureSymbol;
import gecos.core.Symbol;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;

public class SymbolReplacer extends BlockInstructionSwitch<Object> {
	private ExecutionContext newContext;
	
	public SymbolReplacer(ExecutionContext newContext){
		this.newContext = newContext;
		setMode(RHS);
	}
	

	private void myassert(boolean b, String s) throws FlattenerException{
		if (!b){
			throw new FlattenerException(s);
		}
	}


	public final static int LHS = 1;
	public final static int RHS = 2;
	private int mode;

	private void setMode(int mode) {
		this.mode = mode;
	}

	private int getMode() {
		return mode;
	}
	

	@Override
	public Object caseSetInstruction(SetInstruction s) {
		setMode(LHS);
		doSwitch(s.getDest());
		setMode(RHS);
		doSwitch(s.getSource()); 
		return null;
	}
	
	@Override
	public Object caseCallInstruction(CallInstruction g) {
		for (Instruction inst : g.getArgs()){
			doSwitch(inst);
		}
		return null;
	}

	/**
	 * Le symbole ne devrait pas être connu : on a dû appeler avant ValueReplacer 
	 * on crée une nouvelle SymbolInstruction
	 * qui pointe vers le nouveau symbol, i.e. celui du context copié
	 */
	@Override
	public Object caseSymbolInstruction(SymbolInstruction s){
		try {
			myassert(!(s.getSymbol() instanceof ProcedureSymbol), "");
					
			Object res = null;
			ComplexInstruction parent = s.getParent();
			// Parent peut être null si la symbole instruction est une initialisation... ex : int a = sample;)
			// Dans ce cas, on remplace la "value" du symbole
//			int instIndex = 0;
//			if (parent != null){
//				for (Instruction child : parent.getChildren()){
//					if (child == s){
//						break;
//					}
//					instIndex++;
//				}
//			}
			
			// On remplace donc par le nouveau symbol
			//System.out.println("On cherche le symbol correspondant au symbol " + s.getSymbol().getName() + " (" + Integer.toHexString(s.getSymbol().hashCode()) + ")");
			Symbol newSymb = newContext.getCorrespondingSymbol(s.getSymbol());
			//System.out.println("Résultat : symbol " + newSymb.getName() + " (" + Integer.toHexString(newSymb.hashCode()) + ")");
			Instruction inst = s.copy();
			((SymbolInstruction) inst).setSymbol(newSymb);
			if (parent != null){
//				parent.getChildren().set(instIndex, inst);
				parent.replaceChild(s, inst);
			}
			else {
				// On fait la transformation en place sans utiliser la nouvelle instruction
				// TODO: pourquoi ne fait-on pas ça dans les autres cas ?
				s.setSymbol(newSymb);
			}
			return res;
		} catch (FlattenerException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	
	@Override
	public Object caseArrayInstruction(ArrayInstruction g){
		Object res = null;
		
		int savedMode = getMode();
		/* On repasse en RHS pour l'évaluation des indices du tableau */
		setMode(RHS);
		for (Instruction inst : g.getIndex()){
			doSwitch(inst);
		}
		setMode(savedMode);
		
		// On remplace l'ancien symbol par le nouveau symbol
		Symbol newSymb;
		try {
			newSymb = newContext.getCorrespondingSymbol(((SymbolInstruction) g.getDest()).getSymbol());
		} catch (FlattenerException e) {
			throw new RuntimeException(e);
		}
		SymbolInstruction symbInst = (SymbolInstruction) g.getDest().copy();
		symbInst.setSymbol(newSymb);
		g.setDest(symbInst);
		return res;
	}

	
	@Override
	public Object caseConvertInstruction(ConvertInstruction g){
		Object o = doSwitch(g.getExpr());
		return o;
	}
	
}
