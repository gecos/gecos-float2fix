package fr.irisa.cairn.idfix.frontend.interpreter;

import float2fix.Graph;
import fr.irisa.cairn.idfix.utils.exceptions.InterpreterException;
import gecos.blocks.Block;
import gecos.core.Procedure;
import gecos.instrs.Instruction;

/**
 * Interprets a procedure
 * @author Administrateur/QM
 */
public class ProcInterpreter {

	private BlockInterpreter blockInterpreter;
	private InstInterpreter instInterpreter;
	private ExecutionContext context;
	

	public ProcInterpreter(ExecutionContext context, Graph graphF2F) {
		this.context = context;
		this.blockInterpreter = new BlockInterpreter(this.context, this);
		this.instInterpreter = new InstInterpreter(this.context, this, graphF2F);
		this.blockInterpreter.setInstInterpreter(instInterpreter);
	}
	
	public InstInterpreter getInstInterpreter(){
		return instInterpreter;
	}

	/**
	 * Visit the Procedure <i>proc</i>. The result of the interpretation of a
	 * procedure is the result of the body block (which normally ends with the
	 * interpretation of a return instruction, otherwise the result doesn't
	 * matter).
	 * @throws InterpreterException 
	 */
	public Object doSwitch(Procedure proc) throws InterpreterException {
		Object res;
		if (proc == null){
			throw new InterpreterException("Procedure null during the interpretation");
		}
		doSwitch(proc.getStart());
		res = doSwitch(proc.getBody());
		doSwitch(proc.getEnd());
		
		return res;
	}


	/**
	 * Call the BlockInterpreter to visit blocks
	 */
	public Object doSwitch(Block b) {
		return this.blockInterpreter.doSwitch(b);
	}

	/**
	 * Call the InstructionInterpreter to visit instructions
	 */
	public Object doSwitch(Instruction i) {
		return this.instInterpreter.doSwitch(i);
	}
	
	

}
