//package fr.irisa.cairn.idfix.frontend.sfg.generator.utils;
//
//import float2fix.Node;
//import fr.irisa.cairn.idfix.utils.exceptions.SFGGeneratorException;
//import gecos.types.Field;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * Represent data structure for a structure type.
// * @author Nicolas Simon
// * @date Jul 19, 2013
// */
//public class FieldNode implements INodeType {
//	private Map<Field, Node> _values;
//		
//	public FieldNode(Map<Field, Node> map){
//		_values = new HashMap<Field, Node>(map);
//	}
//	
//	public void setValue(Field field, Node value) throws SFGGeneratorException{
//		if(!_values.containsKey(field))
//			throw new SFGGeneratorException("Error: The structure field (" + field +") have been not found");
//		_values.put(field, value);
//	}
//	
//	public Node getNode(Field field) throws SFGGeneratorException{
//		if(!_values.containsKey(field))
//			throw new SFGGeneratorException("Error: The structure field (" + field +") have been not found");
//		Node ret = _values.get(field);
//		return ret;
//	}
//	
//	public List<Field> getFields(){
//		List<Field> ret = new ArrayList<Field>();
//		for(Field field : _values.keySet()){
//			ret.add(field);
//		}
//		return ret;
//	}
//	
//	@Override
//	public String toString(){
//		String res = "FieldNode@ (";
//		int i = 0;
//		for(Field f : _values.keySet()){
//			if(i < _values.size() - 1)
//				res += f.getName() + "= " + _values.get(f) +",";
//			else
//				res += f.getName() + " = " + _values.get(f);
//			i++;
//		}
//		res += ")";
//		return res;
//	}
//
//}
