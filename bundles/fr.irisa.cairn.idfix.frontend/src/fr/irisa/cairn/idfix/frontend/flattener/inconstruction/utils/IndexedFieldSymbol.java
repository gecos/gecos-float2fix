package fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils;

import gecos.core.Symbol;
import gecos.types.Field;

import java.util.List;

public class IndexedFieldSymbol extends IndexedSymbol {
	private Field _field;

	public IndexedFieldSymbol(IndexedSymbol is, Field field){
		super(is.getSymbol(), is.getIndexes());
		_field = field;
	}
	
	public IndexedFieldSymbol(Symbol s, List<Long> v, Field field) {
		super(s, v);
		_field = field;
	}
	
	public Field getField(){
		return _field;
	}

}
