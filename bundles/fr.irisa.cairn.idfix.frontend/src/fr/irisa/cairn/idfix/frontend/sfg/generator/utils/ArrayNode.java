//package fr.irisa.cairn.idfix.frontend.sfg.generator.utils;
//
//import java.util.List;
//
//import float2fix.Node;
//import fr.irisa.cairn.idfix.utils.exceptions.SFGGeneratorException;
//
///**
// * Represent the data structure for an array.
// * Mutli-dimensional array are represented under flat form
// * @author Nicolas Simon
// * @date Jul 19, 2013
// */
//public class ArrayNode implements INodeType {
//	private Node[] _nodes;
//	
//	public ArrayNode(){
//		_nodes = null;
//	}
//	
//	public ArrayNode(int size){
//		_nodes = new Node[size];
//	}
//	
//	public ArrayNode(List<Node> l){
//		_nodes = new Node[l.size()];
//		for(int i = 0 ; i < l.size() ; i++){
//			_nodes[i] = l.get(i);
//		}
//	}
//	
//	public Node getNode(int index) throws SFGGeneratorException {
//		if(index >= _nodes.length)
//			throw new SFGGeneratorException("Index more higher than ArrayNode size");
//		return _nodes[index];
//	}
//	
//	public void setNode(int index, Node n) throws SFGGeneratorException{
//		if(index >= _nodes.length)
//			throw new SFGGeneratorException("Index more higher than ArrayNode size");
//		
//		_nodes[index] = n;
//	}
//	
//	@Override
//	public String toString(){
//		String ret ="ArrayNode@(";
//		for(int i = 0 ; i < _nodes.length ; i++)
//			ret += i + "=" + _nodes[i] +" ";
//		return ret + ")";
//	}
//
//}
