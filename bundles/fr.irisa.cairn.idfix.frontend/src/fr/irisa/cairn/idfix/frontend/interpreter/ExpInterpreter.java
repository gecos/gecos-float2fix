package fr.irisa.cairn.idfix.frontend.interpreter;

import fr.irisa.cairn.gecos.model.tools.switches.GenericInstructionSwitch;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;

/**
 * This class is the "generic instruction" interpreter and evaluates the expressions.
 * @author qmeunier
 */
public class ExpInterpreter extends GenericInstructionSwitch<Object> {

	/*
	 * We keep a link to parent GecosInstructionInterpreter in order to have
	 * access to the interpreter context and to evaluate
	 */
	private InstInterpreter parent; 

	public ExpInterpreter(InstInterpreter parent) {
		this.parent = parent;
	}

	/**
	 * Evaluates an instruction <i>g</i> and returns its value.
	 * @param g
	 * @return the evaluated value
	 */
	private Object evaluate(Instruction g) {
		Object res = parent.doSwitch(g);
		if (res == null){
			throw new RuntimeException("The result of the evaluation of a instruction is null");
		}
		return res;
	}
	
	@Override
	public Object caseNEG(GenericInstruction g) {
		Object o = evaluate(g.getOperand(0));
		if (o instanceof UninitializedValue){
			return new UninitializedValue();
		}
		else if (o instanceof Long){
			return new Long(-(Long) o);
		}
		else if (o instanceof Float){
			return new Float(-(Float) o);
		}
		else {
			throw new RuntimeException("Type mismatch for NEG operand");
		}
		
	}

	@Override
	public Object caseXOR(GenericInstruction g) {
		Object o1 = evaluate(g.getOperand(0));
		Object o2 = evaluate(g.getOperand(1));
		if (o1 instanceof UninitializedValue || o2 instanceof UninitializedValue){
			return new UninitializedValue();
		}
		else if (o1 instanceof Long && o2 instanceof Long){
			return new Long((Long) o1 ^ (Long) o2);
		}
		else {
			throw new RuntimeException("Type mismatch for XOR operands");
		}
		
	}

	@Override
	public Object caseSUB(GenericInstruction g) {
		Object o1 = evaluate(g.getOperand(0));
		Object o2 = evaluate(g.getOperand(1));
		if (o1 instanceof UninitializedValue || o2 instanceof UninitializedValue){
			return new UninitializedValue();
		}
		else if (o1 instanceof Long && o2 instanceof Long){
			return new Long((Long) o1 - (Long) o2);
		}
		else if (o1 instanceof Long && o2 instanceof Float){
			return new Float((Long) o1 - (Float) o2);
		}
		else if (o1 instanceof Float && o2 instanceof Long){
			return new Float((Float) o1 - (Long) o2);
		}
		else if  (o1 instanceof Float && o2 instanceof Float){
			return new Float((Float) o1 - (Float) o2);
		}
		else {
			throw new RuntimeException("Type mismatch for SUB operands");
		}
	}

	@Override
	public Object caseSHR(GenericInstruction g) {
		Object o1 = evaluate(g.getOperand(0));
		Object o2 = evaluate(g.getOperand(1));
		if (o1 instanceof UninitializedValue || o2 instanceof UninitializedValue){
			return new UninitializedValue();
		}
		else if (o1 instanceof Long && o2 instanceof Long){
			return new Long((Long) o1 >> (Long) o2);
		}
		else {
			throw new RuntimeException("Type mismatch for SHR operands");
		}
	}

	@Override
	public Object caseSHL(GenericInstruction g) {
		Object o1 = evaluate(g.getOperand(0));
		Object o2 = evaluate(g.getOperand(1));
		if (o1 instanceof UninitializedValue || o2 instanceof UninitializedValue){
			return new UninitializedValue();
		}
		else if (o1 instanceof Long && o2 instanceof Long){
			return new Long((Long) o1 << (Long) o2);
		}
		else {
			throw new RuntimeException("Type mismatch for SHL operands");
		}
	}

	@Override
	public Object caseOR(GenericInstruction g) {
		Object o1 = evaluate(g.getOperand(0));
		Object o2 = evaluate(g.getOperand(1));
		if (o1 instanceof UninitializedValue || o2 instanceof UninitializedValue){
			return new UninitializedValue();
		}
		else if (o1 instanceof Long && o2 instanceof Long){
			return new Long((Long) o1 | (Long) o2);
		}
		else {
			throw new RuntimeException("Type mismatch for Bitwise OR operands");
		}
	}

	@Override
	public Object caseMUL(GenericInstruction g) {
		Object o1 = evaluate(g.getOperand(0));
		Object o2 = evaluate(g.getOperand(1));
		if (o1 instanceof UninitializedValue || o2 instanceof UninitializedValue){
			return new UninitializedValue();
		}
		else if (o1 instanceof Long && o2 instanceof Long){
			return new Long((Long) o1 * (Long) o2);
		}
		else if (o1 instanceof Long && o2 instanceof Float){
			return new Float((Long) o1 * (Float) o2);
		}
		else if (o1 instanceof Float && o2 instanceof Long){
			return new Float((Float) o1 * (Long) o2);
		}
		else if  (o1 instanceof Float && o2 instanceof Float){
			return new Float((Float) o1 * (Float) o2);
		}
		else {
			throw new RuntimeException("Type mismatch for MUL operands");
		}
	}

	@Override
	public Object caseMOD(GenericInstruction g) {
		Object o1 = evaluate(g.getOperand(0));
		Object o2 = evaluate(g.getOperand(1));
		if (o1 instanceof UninitializedValue || o2 instanceof UninitializedValue){
			return new UninitializedValue();
		}
		else if (o1 instanceof Long && o2 instanceof Long){
			return new Long((Long) o1 % (Long) o2);
		}
		else if (o1 instanceof Long && o2 instanceof Float){
			return new Float((Long) o1 % (Float) o2);
		}
		else if (o1 instanceof Float && o2 instanceof Long){
			return new Float((Float) o1 % (Long) o2);
		}
		else if  (o1 instanceof Float && o2 instanceof Float){
			return new Float((Float) o1 % (Float) o2);
		}
		else {
			throw new RuntimeException("Type mismatch for MOD operands");
		}
	}

	@Override
	public Object caseDIV(GenericInstruction g) {
		Object o1 = evaluate(g.getOperand(0));
		assert(o1 != null);
		Object o2 = evaluate(g.getOperand(1));
		if (o1 instanceof UninitializedValue || o2 instanceof UninitializedValue){
			return new UninitializedValue();
		}
		else if (o1 instanceof Long && o2 instanceof Long){
			return new Long((Long) o1 / (Long) o2);
		}
		else if (o1 instanceof Long && o2 instanceof Float){
			return new Float((Long) o1 / (Float) o2);
		}
		else if (o1 instanceof Float && o2 instanceof Long){
			return new Float((Float) o1 / (Long) o2);
		}
		else if  (o1 instanceof Float && o2 instanceof Float){
			return new Float((Float) o1 / (Float) o2);
		}
		else {
			throw new RuntimeException("Type mismatch for DIV operands");
		}
	}

	@Override
	public Object caseAND(GenericInstruction g) {
		Object o1 = evaluate(g.getOperand(0));
		Object o2 = evaluate(g.getOperand(1));
		if (o1 instanceof UninitializedValue || o2 instanceof UninitializedValue){
			return new UninitializedValue();
		}
		else if (o1 instanceof Long && o2 instanceof Long){
			return new Long((Long) o1 & (Long) o2);
		}
		else {
			throw new RuntimeException("Type mismatch for Bitwise AND operands");
		}
	}

	@Override
	public Object caseADD(GenericInstruction g) {
		Object o1 = evaluate(g.getOperand(0));
		Object o2 = evaluate(g.getOperand(1));
		if (o1 instanceof UninitializedValue || o2 instanceof UninitializedValue){
			return new UninitializedValue();
		}
		else if (o1 instanceof Long && o2 instanceof Long){
			return new Long((Long) o1 + (Long) o2);
		}
		else if (o1 instanceof Long && o2 instanceof Float){
			return new Float((Long) o1 + (Float) o2);
		}
		else if (o1 instanceof Float && o2 instanceof Long){
			return new Float((Float) o1 + (Long) o2);
		}
		else if  (o1 instanceof Float && o2 instanceof Float){
			return new Float((Float) o1 + (Float) o2);
		}
		else {
			throw new RuntimeException("Type mismatch for ADD operands");
		}
	}

	@Override
	public Object caseARRAY(GenericInstruction g) {
		throw new UnsupportedOperationException("caseARRAY not yet implemented");
	}

	@Override
	public Object caseEQ(GenericInstruction g) {
		Object o1 = evaluate(g.getOperand(0));
		Object o2 = evaluate(g.getOperand(1));
		if (o1 instanceof UninitializedValue || o2 instanceof UninitializedValue){
			return new UninitializedValue();
		}
		else if (o1 instanceof Long && o2 instanceof Long){
			return new Boolean(((Long) o1).equals((Long) o2));
		}
		else if (o1 instanceof Long && o2 instanceof Float){
			return new Boolean(new Float((Long) o1).equals((Float) o2));
		}
		else if (o1 instanceof Float && o2 instanceof Long){
			return new Boolean(((Float) o1).equals(new Float((Long) o2)));
		}
		else if (o1 instanceof Float && o2 instanceof Float){
			return new Boolean(((Float) o1).equals((Float) o2));
		}
		else if (o1 instanceof Boolean && o2 instanceof Boolean){
			return new Boolean(((Boolean) o1).equals((Boolean) o2));
		}
		else {
			throw new RuntimeException("Type mismatch for EQ operands");
		}
	}
	
	
	@Override
	public Object caseNE(GenericInstruction g) {
		Object o1 = evaluate(g.getOperand(0));
		Object o2 = evaluate(g.getOperand(1));
		if (o1 instanceof UninitializedValue || o2 instanceof UninitializedValue){
			return new UninitializedValue();
		}
		else if (o1 instanceof Long && o2 instanceof Long){
			return new Boolean(!((Long) o1).equals((Long) o2));
		}
		else if (o1 instanceof Long && o2 instanceof Float){
			return new Boolean(!(new Float((Long) o1).equals((Float) o2)));
		}
		else if (o1 instanceof Float && o2 instanceof Long){
			return new Boolean(!((Float) o1).equals(new Float((Long) o2)));
		}
		else if (o1 instanceof Float && o2 instanceof Float){
			return new Boolean(!((Float) o1).equals((Float) o2));
		}
		else if (o1 instanceof Boolean && o2 instanceof Boolean){
			return new Boolean(!((Boolean) o1).equals((Boolean) o2));
		}
		else {
			throw new RuntimeException("Type mismatch for NE operands");
		}
	}

	@Override
	public Object caseGT(GenericInstruction g) {
		Object o1 = evaluate(g.getOperand(0));
		Object o2 = evaluate(g.getOperand(1));
		if (o1 instanceof UninitializedValue || o2 instanceof UninitializedValue){
			return new UninitializedValue();
		}
		else if (o1 instanceof Long && o2 instanceof Long){
			return new Boolean((Long) o1 > (Long) o2);
		}
		else if (o1 instanceof Long && o2 instanceof Float){
			return new Boolean(new Float((Long) o1) > (Float) o2);
		}
		else if (o1 instanceof Float && o2 instanceof Long){
			return new Boolean((Float) o1 > new Float((Long) o2));
		}
		else if (o1 instanceof Float && o2 instanceof Float){
			return new Boolean((Float) o1 > (Float) o2);
		}
		else {
			throw new RuntimeException("Type mismatch for GT operands");
		}
	}

	@Override
	public Object caseGE(GenericInstruction g) {
		Object o1 = evaluate(g.getOperand(0));
		Object o2 = evaluate(g.getOperand(1));
		if (o1 instanceof UninitializedValue || o2 instanceof UninitializedValue){
			return new UninitializedValue();
		}
		else if (o1 instanceof Long && o2 instanceof Long){
			return new Boolean((Long) o1 >= (Long) o2);
		}
		else if (o1 instanceof Long && o2 instanceof Float){
			return new Boolean(new Float((Long) o1) >= (Float) o2);
		}
		else if (o1 instanceof Float && o2 instanceof Long){
			return new Boolean((Float) o1 >= new Float((Long) o2));
		}
		else if (o1 instanceof Float && o2 instanceof Float){
			return new Boolean((Float) o1 >= (Float) o2);
		}
		else {
			throw new RuntimeException("Type mismatch for GE operands");
		}
	}

	@Override
	public Object caseLT(GenericInstruction g) {
		Object o1 = evaluate(g.getOperand(0));
		Object o2 = evaluate(g.getOperand(1));
		if (o1 instanceof UninitializedValue || o2 instanceof UninitializedValue){
			return new UninitializedValue();
		}
		else if (o1 instanceof Long && o2 instanceof Long){
			return new Boolean((Long) o1 < (Long) o2);
		}
		else if (o1 instanceof Long && o2 instanceof Float){
			return new Boolean(new Float((Long) o1) < (Float) o2);
		}
		else if (o1 instanceof Float && o2 instanceof Long){
			return new Boolean((Float) o1 < new Float((Long) o2));
		}
		else if (o1 instanceof Float && o2 instanceof Float){
			return new Boolean((Float) o1 < (Float) o2);
		}
		else {
			throw new RuntimeException("Type mismatch for LT operands");
		}
	}
	

	@Override
	public Object caseLE(GenericInstruction g) {
		Object o1 = evaluate(g.getOperand(0));
		Object o2 = evaluate(g.getOperand(1));
		if (o1 instanceof UninitializedValue || o2 instanceof UninitializedValue){
			return new UninitializedValue();
		}
		else if (o1 instanceof Long && o2 instanceof Long){
			return new Boolean((Long) o1 <= (Long) o2);
		}
		else if (o1 instanceof Long && o2 instanceof Float){
			return new Boolean(new Float((Long) o1) <= (Float) o2);
		}
		else if (o1 instanceof Float && o2 instanceof Long){
			return new Boolean((Float) o1 <= new Float((Long) o2));
		}
		else if (o1 instanceof Float && o2 instanceof Float){
			return new Boolean((Float) o1 <= (Float) o2);
		}
		else {
			throw new RuntimeException("Type mismatch for LE operands");
		}
	}
	
	
	@Override
	public Object caseLNOT(GenericInstruction g) {
		Object o1 = evaluate(g.getOperand(0));
		if (o1 instanceof UninitializedValue){
			return new UninitializedValue();
		}
		else if (o1 instanceof Boolean){
			return new Boolean(!((Boolean) o1));
		}
		else {
			throw new RuntimeException("Type mismatch for LNOT operands");
		}
	}

	@Override
	public Object caseUnknownGeneric(GenericInstruction g) {
		// TODO Auto-generated method stub
		return null;
	}
}