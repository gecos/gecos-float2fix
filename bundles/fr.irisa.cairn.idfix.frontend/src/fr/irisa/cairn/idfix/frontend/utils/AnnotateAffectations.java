
package fr.irisa.cairn.idfix.frontend.utils;

/**
 * @author qmeunier
 * Annotates all affectations in the CDFG with a unique number
 * This number is used for tracing the value of each affectation during the simulation
 * and make the correspondance with nodes in the SFG
 */

import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import gecos.annotations.AnnotationsFactory;
import gecos.annotations.StringAnnotation;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.instrs.SetInstruction;


public class AnnotateAffectations {
	private ProcedureSet modelps;
	private int currNumAffect = 0;

	public AnnotateAffectations(ProcedureSet ps) {
		this.modelps = ps;
	}
	
	public void compute() {
		VisitorF2FAddAnnotationToAffectation monVisitor = new VisitorF2FAddAnnotationToAffectation(modelps);
		monVisitor.compute();
	}
	
	private void createAnnotation(SetInstruction i){
		StringAnnotation annotation = AnnotationsFactory.eINSTANCE.createStringAnnotation();
		String nombre = Integer.toString(currNumAffect);
		annotation.setContent(nombre);
		i.getDest().getAnnotations().put(ConstantPathAndName.ANNOTATION_VALUE_AFFECTATION, annotation);
		currNumAffect++;
	}
	
	private class VisitorF2FAddAnnotationToAffectation extends BlockInstructionSwitch<Object> {

		private ProcedureSet modelps;

		public VisitorF2FAddAnnotationToAffectation(ProcedureSet modelps) {
			this.modelps = modelps;
		}

		public void compute() {
			for (Procedure proc : modelps.listProcedures()) {
				this.doSwitch(proc);
			}
		}
		
		@Override
		public Object caseSetInstruction(SetInstruction s) {
			createAnnotation(s);
			return super.caseSetInstruction(s);
		}
	}
}
