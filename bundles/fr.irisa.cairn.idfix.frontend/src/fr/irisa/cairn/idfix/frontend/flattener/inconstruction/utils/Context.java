package fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import fr.irisa.cairn.idfix.utils.exceptions.FlattenerException;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.ArrayValueInstruction;
import gecos.instrs.Instruction;
import gecos.types.ArrayType;
import gecos.types.BaseType;
import gecos.types.Field;
import gecos.types.RecordType;
import gecos.types.Type;

/**
 * Represent a program execution context. Each level represent composite block scope.
 * Use {@link ISymbolType} interface to value associate to the symbol.
 * This {@link ISymbolType} allow to create custom data structure to manage scalar, array, structure type...
 * @author Nicolas Simon
 * @date Jun 28, 2013
 */
public class Context {
	/*
	 * One level of the stack represent one level of nested block. Each time the
	 * Flattener visits a block, a new Map is pushed on the top of the stack.
	 * When a function is called, a new stack is created, with the first level
	 * set with the globals variables of the ProcedureSet. The old stack is
	 * stored in a stack of stack : framestack.
	 */
	private Stack<Map<Symbol, ISymbolType>> _stack;
	private Stack<Stack<Map<Symbol, ISymbolType>>> _framestack;
	private DeclarationInterpreter _declInterpreter;
	private String _functionName;
	private ISymbolType _returnValue;
	
	
	public Context(){
		_stack = new Stack<>();
		_framestack = new Stack<>();
		_declInterpreter = new DeclarationInterpreter(this);
	}
	
	/**
	 * Push a scope into the context execution.
	 * If a value is initialized during the declaration, the value is updated into the context.
	 * @param scope
	 * @throws FlattenerException
	 */
	public void push(Scope scope) throws FlattenerException{
		Map<Symbol, ISymbolType> map = new HashMap<Symbol, ISymbolType>();
		_stack.push(map);
		for(Symbol s : scope.getSymbols()){
			if(s.getType().asBase() != null){
				pushScalarSymbol(s, map);
			} else if(s.getType().isArray()){
				pushArraySymbol(s, map);
			} else if(s.getType().asRecord() != null){
				pushStructSymbol(s, map);
			} else if(s.getType().isPointer()){
				throw new FlattenerException("Pointer type not managed yet");
			} else if(s.getType().asAlias() != null){
				throw new FlattenerException("Alias type not managed yet");
			} else{
				map.put(s, null);
			}			
		}
	}
	
	/**
	 * Pop a level of the stack
	 */
	public void pop(){
		_stack.pop();
	}
	
	/**+
	 * Get the global scope of the program
	 * Corresponding to the first level of the context
	 * @return
	 */
	public Scope getGlobalScope(){
		for(Symbol s : _stack.get(0).keySet())
			return s.getContainingScope();
		
		return null;
	}
	
	/**
	 * Create a new context for the new frame (function). This new context is contains automatically the global scope.
	 * Save the old context.
	 * @param params
	 * @param functionName
	 */
	public void pushFrame(Map<Symbol,ISymbolType> params, String functionName){
		_framestack.push((Stack<Map<Symbol,ISymbolType>>) _stack);
		_stack = new Stack<>();
		_stack.push(_framestack.peek().get(0));
		_stack.push(params);
		_functionName = functionName;
		_returnValue = null;
	}
	
	/**
	 * Restore the previous context frame
	 */
	public void popFrame(){
		_stack = _framestack.pop();
	}
	
	public void setLastFunctionReturnValue(ISymbolType value){
		_returnValue = value;
	}
	
	public ISymbolType getLastFunctionReturnValue(){
		return _returnValue;
	}
	
	/**
	 * Add a scalar element into the level of the execution context.
	 * Initialize the element (Value provide at the declaration or {@link UninitializedValue})
	 * @param s
	 * @param map
	 * @throws FlattenerException
	 */
	private void pushScalarSymbol(Symbol s, Map<Symbol, ISymbolType> map) throws FlattenerException {
		BaseType baseType = (BaseType) s.getType();
		if(s.getValue() != null){ // int a = 5;
			Object res = _declInterpreter.doSwitch(s.getValue());
			if(!(res instanceof SymbolValue))
				throw new FlattenerException("Error: SymbolValue is not used for a scalar variable or constant");
			
			SymbolValue value = (SymbolValue) res; 
			if(baseType.asFloat() != null || baseType.asInt() != null){
				if(!(value.getValue() instanceof Long || value.getValue() instanceof Double || value.getValue() instanceof UninitializedValue))
					throw new FlattenerException("Error: Value of a scalar/constant is not a int/float value");
				map.put(s, value);
			}
			else{
				throw new FlattenerException("Error: Kind of scalar variable not managed (" + s.getName() + ":" +baseType + ")");
			}
		}
		else {
				map.put(s, new SymbolValue(new UninitializedValue()));
		}
	}
	
	/**
	 * Add an array element into the level of the execution context.
	 * Initialize the element (Value provide at the declaration or {@link UninitializedValue})
	 * @param s
	 * @param map
	 * @throws FlattenerException
	 */
	private void pushArraySymbol(Symbol s, Map<Symbol, ISymbolType> map) throws FlattenerException{
		Instruction sizeInst;
		Object res;
		SymbolValue index;
		Instruction instValue = s.getValue();
		ArrayType arrType;
		
		int size = 1;
		Type type;
		for(type = s.getType() ; type instanceof ArrayType ; type = ((ArrayType) type).getBase()){
			arrType = (ArrayType) type;
			sizeInst = arrType.getSizeExpr();
			res = _declInterpreter.doSwitch(sizeInst);
			if(!(res instanceof SymbolValue))
				throw new FlattenerException("Error: SymbolValue is not used for a scalar variable or constant");
			
			index = (SymbolValue) res;
			if(!(index.getValue() instanceof Long))
				throw new FlattenerException("Error: Index of array is not a integer or a long (initialization part)");
			
			size *= (Long) index.getValue();			
		}
				
		
		if(type.asBase() != null){
			List<SymbolValue> values = new ArrayList<SymbolValue>(); 
			arrayInitialization(instValue, values, size);
			map.put(s,  new SymbolArray<SymbolValue>(values));
		}
		else if(type.asRecord() != null){
			SymbolArray<SymbolStruct<ISymbolType>> symAR = new SymbolArray<SymbolStruct<ISymbolType>>(size);
			for(int i = 0 ; i < size ; i++){
				symAR.setValue(i, createStructure((RecordType) type));
			}
			map.put(s, symAR);
		}
		else if(type.isPointer()){
			throw new FlattenerException("Error: Array of pointer type not managed yet");
		}
		else if(type.asAlias() != null){
			throw new FlattenerException("Error: Array of alias type not managed yet");
		}
		else{
			throw new FlattenerException("Error: Type of array unknown (Symbol:" + s + ", Type:" + type + ")");
		}
	}
	
	/**
	 * Return the elements list contains into the Gecos IR for an array (mutli-dimensional or not)
	 * @param inst
	 * @param v
	 * @throws FlattenerException 
	 */
	private void arrayInitialization(Instruction inst, List<SymbolValue> v, int size) throws FlattenerException{
		if(inst != null){
			recArrayInitalization(inst, v);
		}
		else{
			for(int i = 0 ; i < size ; i++){
				v.add(new SymbolValue(new UninitializedValue()));
			}
		}
			
	}
	
	private void recArrayInitalization(Instruction inst, List<SymbolValue> v) throws FlattenerException{
		if(inst != null){
			if (inst instanceof ArrayValueInstruction){
				ArrayValueInstruction insts = (ArrayValueInstruction) inst;
				for (Instruction i : insts.getChildren()){
					recArrayInitalization(i,v);
				}
			}
			else {
				Object res = _declInterpreter.doSwitch(inst);
				if(!(res instanceof SymbolValue))
					throw new FlattenerException("Error: SymbolValue is not used for a scalar variable or constant");
				
				SymbolValue value = (SymbolValue) res;
				if(!(value.getValue() instanceof Long || value.getValue() instanceof Double || value.getValue() instanceof UninitializedValue))
					throw new FlattenerException("Error: Value of a scalar/constant is not a int/float value");
				
				v.add(value);
			}
		}
	}
	
	/**
	 * Add a structure element into the level of the execution context.
	 * Initialize the element to {@link UninitializedValue}
	 * @param s
	 * @param map
	 * @throws FlattenerException
	 */
	private void pushStructSymbol(Symbol s, Map<Symbol, ISymbolType> map) throws FlattenerException{
		RecordType rType = (RecordType) s.getType();
		map.put(s, createStructure(rType));
	}
	
	private SymbolStruct<ISymbolType> createStructure(RecordType rType) throws FlattenerException{
		Map<Field, ISymbolType> values = new HashMap<Field, ISymbolType>();
		for(Field field : rType.getFields()){
			if(field.getType().asBase() != null){
				values.put(field, new SymbolValue(new UninitializedValue()));
			}
			else if(field.getType().isArray()){
				int size = 1;
				for(Type type = field.getType() ; type instanceof ArrayType ; type = ((ArrayType) type).getBase()){
					ArrayType arrType = (ArrayType) type;
					Instruction sizeInst = arrType.getSizeExpr();
					Object res = _declInterpreter.doSwitch(sizeInst);
					if(!(res instanceof SymbolValue))
						throw new FlattenerException("Error: SymbolValue is not used for a scalar variable or constant");
					
					SymbolValue index = (SymbolValue) res;
					if(!(index.getValue() instanceof Long))
						throw new FlattenerException("Error: Index of array is not a integer or a long (initialization part)");
					
					size *= (Long) index.getValue();			
				}
				
				values.put(field, new SymbolArray<ISymbolType>(size));
			}
			else if(field.getType().asRecord() != null){
				SymbolStruct<ISymbolType> ret = createStructure((RecordType) field.getType());
				values.put(field, ret);
			}
		}
		
		return new SymbolStruct<ISymbolType>(values);
	}
	
	
	/******************* GLOBAL GETTERS/SETTERS *********************/
	
	/**
	 * Return the {@link ISymbolType} associate to the symbol from the higher level in the context
	 * @param s
	 * @return
	 * @throws FlattenerException
	 */
	private ISymbolType getSymbolType(Symbol s) throws FlattenerException{
		int top = _stack.size() - 1;
		for(int i = top ; i >= 0 ; i--){
			Map<Symbol, ISymbolType> map = _stack.get(i);
			if(map.containsKey(s)){
				return map.get(s);
			}
		}
		throw new FlattenerException("Error : Value of symbol " + s.getName() + " not found in the context.");
	}
	
	/**
	 * Set the {@link ISymbolType} associate to the symbol to the higher level in the context
	 * @param s
	 * @return
	 * @throws FlattenerException
	 */
	private void setSymbolType(Symbol s, ISymbolType value) throws FlattenerException{
		int top = _stack.size() - 1;
		for(int i = top ; i >= 0 ; i--){
			Map<Symbol, ISymbolType> map = _stack.get(i);
			if(map.containsKey(s)){
				map.put(s, value);
				return;
			}
		}
		throw new FlattenerException("Error : Value of symbol " + s.getName() + " not found in the context.");
	}
	
	public ISymbolType getAddress(Symbol s) throws FlattenerException{
		return getSymbolType(s);
	}
	
	
	/******************* SCALAR GETTERS *********************/
	
	/**
	 * Return the {@link SymbolValue}e of the symbol. The symbol must be a scalar.
	 * @param s
	 * @return
	 * @throws FlattenerException
	 */
	public ISymbolType getValue(Symbol s) throws FlattenerException{
		ISymbolType symT = getSymbolType(s);
//		if(!(symT instanceof SymbolValue))
//			throw new FlattenerException("Error: SymbolValue is not used for a scalar variable (" + s.getName() +")");
		return symT;
	}
	
	/******************* STRUCT GETTERS *********************/
	
	/**
	 * Return the {@link ISymbolType} of the field of the symbol structure. The symbol must be a structure type.
	 * @param s
	 * @param field
	 * @return
	 * @throws FlattenerException
	 */
	public ISymbolType getValue(Symbol s, Field field) throws FlattenerException{
		ISymbolType symV = getSymbolType(s);
		if(!(symV instanceof SymbolStruct))
			throw new FlattenerException("Error : Not SymbolStruct used for structure variable (symbol: " + s.getName() +", field: " + field.getName() +")");
		
		SymbolStruct<?> symST = (SymbolStruct<?>) symV;
		return symST.getValues(field);
	}
	
	/**
	 * Return the {@link ISymbolType} of the array symbol at the index provide. The symbol must be an array type which contains structure variable.
	 * @param s
	 * @param indices
	 * @return
	 * @throws FlattenerException
	 */
	public ISymbolType getValue(Symbol s, List<Long> indices, Field field) throws FlattenerException{
		Object ret = getValue(s, indices);
		if(!(ret instanceof SymbolStruct))
			throw new FlattenerException("Error : Not SymbolStruct used for structure variable");
		
		SymbolStruct<?> symST = (SymbolStruct<?>) ret;
		return symST.getValues(field);
	}
	
	/******************* ARRAY GETTERS *********************/
	
	/**
	 * Return the {@link ISymbolType} of the array symbol at the index provide. The symbol must be an array type.
	 * @param s
	 * @param indices
	 * @return
	 * @throws FlattenerException
	 */
	public ISymbolType getValue(Symbol s, List<Long> indices) throws FlattenerException{
		//FIXME Work for struct complex t[i] => a = t[i].re
		//		Doesn't work for struct complex a ; a = t[i]
		ISymbolType symV = getSymbolType(s);
		if(!(symV instanceof SymbolArray))
			throw new FlattenerException("Error : Not SymbolArray used for array variable (symbol: " + s.getName() +")");
		
		SymbolArray<?> symSA = (SymbolArray<?>) symV;
		ISymbolType ret = symSA.getValue(getArrayIndex(s.getType(), indices));
		return ret;
	}
		
	/******************* SCALAR SETTERS *********************/
	
	/**
	 * Set the {@link SymbolValue} of the symbol. s must b an scalar variable.
	 * @param s
	 * @param value
	 * @throws FlattenerException
	 */
	public void setValue(Symbol s, SymbolValue value) throws FlattenerException{
		if(s.getType().asBase() == null)
			throw new FlattenerException("Error: Impossible to set a SymbolValue to a no scalar variable type (Symbol:" + s.getName() + ", Type: " + s.getType() + ")");
		
		setSymbolType(s, value);
	}
	
	
	/******************* STRUCT SETTERS *********************/
	
	/**
	 * Set the {@link SymbolStruct} of the symbol. s must be an structure variable.
	 * @param s
	 * @param value
	 * @throws FlattenerException
	 */
	public void setValue(Symbol s, SymbolStruct<ISymbolType> value) throws FlattenerException{
		if(s.getType().asRecord() == null)
			throw new FlattenerException("Error: Impossible to set a SymbolStruct to a no structure variable type (Symbol:" + s.getName() + ", Type: " + s.getType() + ")");
		
		setSymbolType(s, value);
	}
	
	/**
	 * Set the {@link ISymbolType} of the symbol. The symbol must be a structure type.
	 * @param <T>
	 * @param s
	 * @param field
	 * @param value
	 * @throws FlattenerException
	 */
	public <T extends ISymbolType> void setValue(Symbol s, Field field, T value) throws FlattenerException{
		ISymbolType symV = getSymbolType(s);
		if(!(symV instanceof SymbolStruct<?>))
			throw new FlattenerException("Error : Not SymbolStruct used for structure variable (" + s.getName() + ")");
		
		@SuppressWarnings("unchecked")
		SymbolStruct<T> symST = (SymbolStruct<T>) symV;
		symST.setValue(field, value);
	}
	
	/**
	 * Set the {@link ISymbolType} of the symbol. The symbol must be an array type which contains structure variable.
	 * @param <T>
	 * @param s
	 * @param indices
	 * @param value
	 * @throws FlattenerException
	 */
	public <T extends ISymbolType> void setValue(Symbol s, List<Long> indices, Field field, T value) throws FlattenerException{
		ISymbolType symV = getSymbolType(s);
		if(!(symV instanceof SymbolArray))
			throw new FlattenerException("Error : Not SymbolArray used for array variable (" + s.getName() +")");
		
		SymbolArray<?> symSA = (SymbolArray<?>) symV;
		Object ret = symSA.getValue(getArrayIndex(s.getType(), indices));
		if(!(ret instanceof SymbolStruct))
			throw new FlattenerException("Error : Not SymbolStruct used for struct variable (Symbol:" + s.getName() + ", Field:" + field.getName() +")");
		
		@SuppressWarnings("unchecked")
		SymbolStruct<T> struct = (SymbolStruct<T>) ret;
		struct.setValue(field, value);
	}
		
	/******************* ARRAY SETTERS *********************/
	
	/**
	 * Set the {@link SymbolStruct} of the symbol. s must be an structure variable.
	 * @param s
	 * @param value
	 * @throws FlattenerException
	 */
	public void setValue(Symbol s, SymbolArray<ISymbolType> value) throws FlattenerException{
		if(!s.getType().isArray())
			throw new FlattenerException("Error: Impossible to set a SymbolArray to a no array variable type (Symbol:" + s.getName() + ", Type: " + s.getType() + ")");
		
		setSymbolType(s, value);
	}
	
	/**
	 * Set the {@link ISymbolType} of the symbol. The symbol must be an array type.
	 * @param <T>
	 * @param s
	 * @param indices
	 * @param value
	 * @throws FlattenerException
	 */
	public <T extends ISymbolType> void setValue(Symbol s, List<Long> indices, T value) throws FlattenerException{
		//FIXME Doesn't work for struct complex t[i] : struct complex a ; t[i] = a;
		ISymbolType symV = getSymbolType(s);
		if(!(symV instanceof SymbolArray))
			throw new FlattenerException("Error : Not SymbolArray used for array variable (" + s.getName() +")");
		
		@SuppressWarnings("unchecked")
		SymbolArray<T> symSA = (SymbolArray<T>) symV;
		symSA.setValue(getArrayIndex(s.getType(), indices), value);
	}

	
	/**
	 * Display method
	 */
	public String toString() {
		StringBuffer res = new StringBuffer();
		res.append("ret function (" + _functionName + ") = " + _returnValue).append("\n");
		
		int top = _stack.size() - 1;
		for (int i = top; i >= 0; i--) {
			res.append("level " + i + "\n");
			Map<Symbol, ISymbolType> current = _stack.get(i);
			for (Symbol s : current.keySet()) {
				String str = s.getName() + " (" + Integer.toHexString(s.hashCode()) + ") = " + current.get(s) + " : " + s.getType();
				res.append("\t" + str + "\n");
			}
		}
		return res.toString();
	}
	
	/**
	 * Return a copy of object
	 * @return context object copy
	 */
	public Context copy(){
		Context copy = new Context();
		Map<Symbol, ISymbolType> map;
		for(int i = 0 ; i < this._stack.size() ; i++){
			map = new HashMap<>();
			map.putAll(this._stack.get(i));
			copy._stack.push(map);
		}
		return copy;
	}
	
	/**
	 * Compute and return the flat index of a array (multi-dimensional or not)
	 * @param type
	 * @param indices
	 * @return
	 * @throws FlattenerException
	 */
	private int getArrayIndex(Type type, List<Long> indices) throws FlattenerException{ 
		Type localType = type;
		ArrayType arrayType = (ArrayType) localType;
		if(arrayType.getNbDims() != indices.size())
			throw new FlattenerException("Dimensions number problem");
		
		int ind = 0;
		int nbElemParDim = 1;
		for (int j = indices.size() - 1; j >= 0; j--){
			arrayType = (ArrayType) localType;
			ind += indices.get(j)*nbElemParDim;
			nbElemParDim *= arrayType.getUpper() - arrayType.getLower() + 1;
			localType = arrayType.getBase();
		}
		return ind;
	}
}
