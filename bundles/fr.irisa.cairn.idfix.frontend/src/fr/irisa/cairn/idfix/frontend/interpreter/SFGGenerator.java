
package fr.irisa.cairn.idfix.frontend.interpreter;


import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;

import float2fix.Annotations;
import float2fix.Edge;
import float2fix.Float2fixFactory;
import float2fix.Graph;
import float2fix.Node;
import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import fr.irisa.cairn.gecos.model.tools.switches.DefaultInstructionSwitch;
import fr.irisa.cairn.idfix.frontend.utils.AnnotateOperators;
import fr.irisa.cairn.idfix.frontend.utils.AnnotateVariables;
import fr.irisa.cairn.idfix.model.factory.IDFixUserFixedPointSpecificationFactory;
import fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGEdge;
import fr.irisa.cairn.idfix.model.fixedpointspecification.CDFGNode;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Data;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification;
import fr.irisa.cairn.idfix.model.fixedpointspecification.Operation;
import fr.irisa.cairn.idfix.model.fixedpointspecification.OperatorKind;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.IDFixUtils;
import fr.irisa.cairn.idfix.utils.exceptions.InterpreterException;
import fr.irisa.cairn.idfix.utils.exceptions.SFGGeneratorException;
import fr.irisa.cairn.idfix.utils.exceptions.SFGModelCreationException;
import gecos.annotations.StringAnnotation;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.ArithmeticOperator;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.BitwiseOperator;
import gecos.instrs.CallInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.InstrsFactory;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.RetInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.ArrayType;
import gecos.types.BaseType;
import gecos.types.FunctionType;

public class SFGGenerator extends BlockInstructionSwitch<Object> {
	
	private int currId;
	private int currNumOpSFG;
	private Graph modelGraphF2F;
	private ExecutionContext context;
	private InstInterpreter instInterpreter;
	private ProcInterpreter procInterpreter;
	
	private HashMap<IndexedSymbol, Node> firstDelayedVarNode;
	private HashMap<IndexedSymbol, Node> lastDelayedVarNode;
	
	private HashMap<IndexedSymbol, Node> lastOutputVarNode;
	
//	private static HashMap<Integer, List<Integer>> opsOutputAffectingVar; // Associates a list of opsIDFix to a Variable, the list corresponding to the Ops preceeding the variable (for bitwidth propagation)
//	private static HashMap<Integer, List<Integer>> opsInputAffectingVar;
	
	private static HashMap<Integer, Integer> paramProtoToParamCall;
	private static HashMap<Integer, Integer> paramCallToParamProto;
	
	private Node retNode = null;
	private boolean forceReturnNodeCreation = false;
	
	/* On commence la numérotaion des id de noeuds à 2 car dans IDFix,
	 * un noeud init et un noeud fin sont automatiquement générés avec les id 0 et 1 */
	private final int INIT_NODE_ID = 2;
	
	private FixedPointSpecification _fixedSpecif;
	private int _eval_cdfg_number;
	private List<Integer> _operatorAdded;
	private List<Integer> _dataAdded;
	private _ResolveOperatorKind _resolveOperatorKind = new _ResolveOperatorKind();
	
	private Map<Node, Symbol> _nodeToSymbol;
	
	private void myassert(boolean b, String s) throws SFGGeneratorException{
		if (!b){
			throw new SFGGeneratorException(s);
		}
	}
	
	public SFGGenerator(ExecutionContext context, Graph modelGraph, InstInterpreter instInterpreter, ProcInterpreter procInterpreter){
		currId = INIT_NODE_ID;
		currNumOpSFG = 0;
		this.context = context;
		this.modelGraphF2F = modelGraph;
		this.instInterpreter = instInterpreter;
		this.procInterpreter = procInterpreter;
		firstDelayedVarNode = new HashMap<IndexedSymbol, Node>();
		lastDelayedVarNode = new HashMap<IndexedSymbol, Node>();
		lastOutputVarNode = new HashMap<IndexedSymbol, Node>();
//		opsOutputAffectingVar = new HashMap<Integer,List<Integer>>();
//		opsInputAffectingVar = new HashMap<Integer, List<Integer>>();
		paramProtoToParamCall = new HashMap<Integer, Integer>();
		paramCallToParamProto = new HashMap<Integer, Integer>();
		
		_eval_cdfg_number = 0;
		_operatorAdded = new LinkedList<Integer>();
		_dataAdded = new LinkedList<Integer>();
		_nodeToSymbol = new HashMap<Node, Symbol>();
	}
	
	public void setFixedPointSpecification(FixedPointSpecification fixedPointSpecif){
		_fixedSpecif = fixedPointSpecif;
	}
	
	public boolean returnNodeForced(){
		return forceReturnNodeCreation;
	}
	
	private int addOperatorToFixedSpecification(Instruction inst) throws SFGGeneratorException{	
		Operation operator = null;
		Integer conv_cdfg = IDFixUtils.getCDFGInstructionNumber(inst);
		myassert((conv_cdfg != null), "An instruction which represent an operator have not CDFG number annotation");
		int ret = -1;
		OperatorKind kind = _resolveOperatorKind.doSwitch(inst);
		if(kind == null){
			throw new RuntimeException("Kind of operator instruction not managed yet :" + inst);
		}
		else{
			if(!_operatorAdded.contains(conv_cdfg)){
				_operatorAdded.add(conv_cdfg);
				operator = IDFixUserFixedPointSpecificationFactory.OPERATOR(conv_cdfg, _eval_cdfg_number, kind, inst);
				ret = _eval_cdfg_number;
				_fixedSpecif.getOperations().add(operator);
				_eval_cdfg_number++;
			}
			else{
				for(Operation op : _fixedSpecif.getOperations()){
					if(op.getConv_cdfg_number() == conv_cdfg){
						if(!op.getCorrespondingInstructions().contains(inst))
							op.getCorrespondingInstructions().add(inst);
						ret = op.getEval_cdfg_number();
						operator = op;
						break;
					}
				}
			}
			
			if(ret == -1)
				throw new RuntimeException("An operator created during the SFG creation is missing in the fixed point specification");
			
			operator.setUsageNumber(operator.getUsageNumber() + 1);
			
			return ret;
		}
	}
	
	private void addDataToFixedSpecification(Symbol s){
		int cdfg_number = IDFixUtils.getSymbolAnnotationNumber(s);
		if(!_dataAdded.contains(cdfg_number)){
			_dataAdded.add(cdfg_number);
			Integer width = IDFixUtils.getVarWidth(s);
			Data data;
			if(width != null){
				data = IDFixUserFixedPointSpecificationFactory.DATA(s, cdfg_number,
						IDFixUserFixedPointSpecificationFactory.FIXEDINFORMATION(width));
			}
			else{
				data = IDFixUserFixedPointSpecificationFactory.DATA(s, cdfg_number);	
			}
			
			_fixedSpecif.getDatas().add(data);
		}
	}
	
	private void localCreateAnnotation(Object n, String key, String value) throws SFGGeneratorException{
		Annotations annotation = Float2fixFactory.eINSTANCE.createAnnotations();
		annotation.setKey(key);
		annotation.setValue(value);
		if (n instanceof Node){
			((Node) n).getAttr().add(annotation);
		}
		else if (n instanceof Edge){
			((Edge) n).getAttr().add(annotation);
		}
		else {
			myassert(false,"The creation of a graph annotation called on bad object");
		}
	}
	
	
	private Node localCreateNode(String name){
		Node newNode = Float2fixFactory.eINSTANCE.createNode();
		newNode.setName(name);
		newNode.setId(currId);
		currId++;
		this.modelGraphF2F.getNode().add(newNode);
		return newNode;
	}
	
	/**
	 * Vérifie si un donné est la source d'un edge, i.e. que ce noeud est utilisé dans un calcul
	 * @param node : Le noeud à tester
	 */
	public boolean isSourceNode(Node node){
		for (Edge edge : modelGraphF2F.getEdge()){
			if (edge.getId_pred() == node){
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * Vérifie si un donné est la destination d'un edge, i.e. que ce noeud est déjà affecté (utile lorsque l'on rajoute les noeuds z-1)
	 * @param node : Le noeud à tester
	 */
	public boolean isDestNode(Node node){
		for (Edge edge : modelGraphF2F.getEdge()){
			if (edge.getId_succ() == node){
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * @param nodeId : l'Id du noeud vers lequel pointe l'arc à supprimer 
	 */
	public void removeEdge(int nodeId){
		Edge toDelete = null;
		for (Edge edge : modelGraphF2F.getEdge()){
			if (edge.getId_succ().getId() == nodeId){
				toDelete = edge;
				break;
			}
		}
		modelGraphF2F.getEdge().remove(toDelete);
	}
	
	
	private Edge getFirstPrevEdge(Node node){
		Edge res = null;
		for (Edge e : modelGraphF2F.getEdge()){
			if (e.getId_succ() == node){
				res = e;
				break;
			}
		}
		return res;
	}
	

	private Edge getFirstSuccEdge(Node node){
		Edge res = null;
		for (Edge e : modelGraphF2F.getEdge()){
			if (e.getId_pred() == node){
				res = e;
				break;
			}
		}
		return res;
	}
	

	private void localCreateDataEdge(Node from, Node to, Integer numInput) throws SFGGeneratorException {
		Edge newEdge = Float2fixFactory.eINSTANCE.createEdge();
		newEdge.setId_pred(from);
		newEdge.setId_succ(to);
		if (numInput != null){
			localCreateAnnotation(newEdge, "NUM_INPUT", Integer.toString(numInput));
		}
		this.modelGraphF2F.getEdge().add(newEdge);
	}
	

	private String convertInstName(String inst) throws SFGGeneratorException{
		if (inst.equals("add")){
			return "+";
		}
		else if (inst.equals("sub") || inst.equals("neg")){
			return "-";
		}
		else if (inst.equals("div")){
			return "/";
		}
		else if (inst.equals("mul")){
			return "*";
		}
		else if (inst.equals("shl")){
			return "<<";
		}
		else if (inst.equals("shr")){
			return ">>";
		}
		else if(inst.equals("sqrt")){
			return "sqrt";
		}
		else {
			throw new SFGGeneratorException("Instruction " + inst + " not supported yet");
		}
	}
	
	private String convertInstName2(String inst) throws SFGGeneratorException{
		if (inst.equals(ArithmeticOperator.ADD.getLiteral())){
			return "ADD";
		}
		else if (inst.equals(ArithmeticOperator.SUB.getLiteral())){
			return "SUB";
		}
		else if (inst.equals(ArithmeticOperator.NEG.getLiteral())){
			return "NEG";
		}
		else if (inst.equals(ArithmeticOperator.DIV.getLiteral())){
			return "DIV";
		}
		else if (inst.equals(ArithmeticOperator.MUL.getLiteral())){
			return "MUL";
		}
		else if (inst.equals(BitwiseOperator.SHL.getLiteral())){
			return "SHL";
		}
		else if (inst.equals(BitwiseOperator.SHR.getLiteral())){
			return "SHR";
		}
		else if(inst.equals("sqrt")){
			return "SQRT";
		}
		else {
			throw new SFGGeneratorException("Instruction " + inst + " not supported yet");
		}
	}
	/**
	 * Rempli les maps permettant de trouver rapidement le successeur et le predecesseur de chaque noeud.
	 * Rempli la liste comprenant tout les noeuds output du graph.
	 * 
	 * @author nicolas simon
	 * @param mapNodePred
	 * @param mapNodeSucc
	 * @param listNodeOutput
	 */
	private void createMapInterdependanceNodeEdge(Map<Node, List<Edge>> mapNodePred, Map<Node, List<Edge>> mapNodeSucc, List<Node> listNodeOutput){
		List<Edge> listEdge;
		Node node;

		for(Edge edge : modelGraphF2F.getEdge()){
			// Rempli la map correspondant à chaque edge pred d'un noeud
			node = edge.getId_succ();
			if(node != null){
				if(mapNodePred.containsKey(node)){
					listEdge = mapNodePred.get(node);
				}
				else{
					listEdge = new ArrayList<Edge>();
				}
				listEdge.add(edge);
				mapNodePred.put(node, listEdge);
			
				// On rempli la liste contenant uniquement les noeuds output du graph
				// Les noeuds output n'ont pas de successeur. C'est pour cela que cette liste soit remplie à cet endroit.
				for(Annotations annot : node.getAttr()){
					if(annot.getValue().equals("OUTPUT")){
						listNodeOutput.add(node);
					}
				}
			}
			
			// Rempli la map correspondant à chaque edge succ d'un noeud
			node = edge.getId_pred();
			if(node != null){
				if(mapNodeSucc.containsKey(node)){
					listEdge = mapNodeSucc.get(node);
				}
				else{
					listEdge = new ArrayList<Edge>();
				}
				listEdge.add(edge);
				mapNodeSucc.put(node, listEdge);
			}
		}
	}
	
	/**
	 * Parcours le graphe et ajoute les noeuds connecté directement ou indirectement à une sortie à une liste
	 * 
	 * @author nicolas simon
	 * @date 18/07/12
	 * 
	 * @param node
	 * @param taggedNodeList
	 * @param taggedEdgeList
	 * @param mapNodePred
	 * @param mapNodeSucc
	 * @throws SFGGeneratorException 
	 */
	private void addTaggedList(Node node, List<Node> taggedNodeList, List<Edge> taggedEdgeList, Map<Node, List<Edge>> mapNodePred,  Map<Node, List<Edge>> mapNodeSucc) throws SFGGeneratorException{
		Node nodePred;
		
		taggedNodeList.add(node);
		if(mapNodePred.containsKey(node)){
			myassert(mapNodePred.get(node) != null, "mapNodePred contains a key which have no predecessor");
			for(Edge edgePred : mapNodePred.get(node)){
				taggedEdgeList.add(edgePred);
				nodePred = edgePred.getId_pred();
				if(taggedNodeList.contains(nodePred)){
					continue;
				}
				else{
					this.addTaggedList(nodePred, taggedNodeList, taggedEdgeList, mapNodePred, mapNodeSucc);
				}
			}
		}
	}
	
//	/**
//	 * Met à jour à partir du graph épuré les maps représentant les connections entre les data et les opérateurs utilisées lors de l'optimisation.
//	 * 
//	 * @author nicolas simon
//	 * @date 18/07/2012
//	 * 
//	 * @param mapNodePred
//	 * @param mapNodeSucc
//	 * @throws SFGGeneratorException 
//	 * 
//	 * TODO remove it
//	 */
//	private void updateInputOutputAffectationVar(Map<Node, List<Edge>> mapNodePred,  Map<Node, List<Edge>> mapNodeSucc) throws SFGGeneratorException{
//		Node nodePred, nodeSucc;
//		Integer numOpCDFG, numDataCDFG;
//		Data data;
//		Operator operator;
//		
//		for(Node node : modelGraphF2F.getNode()){
//			numDataCDFG = isDataNode(node);
//			if(numDataCDFG != null){
//				data = _fixedSpecif.findDataFormCDFG(numDataCDFG);
//				myassert(data != null, "No data with CDFG number " + numDataCDFG + " find in the fixed point specification");
//				
//				if(mapNodePred.containsKey(node)){ // Ajout du pred
//					myassert(mapNodePred.get(node).size() == 1, "Data node with more one predecessor");
//					nodePred = mapNodePred.get(node).get(0).getId_pred();
//					numOpCDFG = isOperatorNode(nodePred);
//					myassert(numOpCDFG != null, "Data node predecessor is not a operator node or this operator node have not a NUM_OP_CDFG");
//					
//					operator = null;
//					for(Operator op : _fixedSpecif.getOperators()){
//						if(op.getEval_cdfg_number() == numOpCDFG){
//							operator = op;
//							break;
//						}
//					}
//					myassert(operator != null, "No operator with CDFG number " + numOpCDFG + " find in the fixed point specification");
//					
//					if(opsOutputAffectingVar.containsKey(numDataCDFG)){
//						if(!opsOutputAffectingVar.get(numDataCDFG).contains(numOpCDFG))
//							opsOutputAffectingVar.get(numDataCDFG).add(numOpCDFG);
//					}
//					else{
//						List<Integer> listInputAffectingVar = new ArrayList<Integer>();
//						listInputAffectingVar.add(numOpCDFG);
//						opsOutputAffectingVar.put(numDataCDFG, listInputAffectingVar);
//					}
//				}
//					
//				if(mapNodeSucc.containsKey(node)){// Ajout des Succ
//					for(Edge edge : mapNodeSucc.get(node)){
//						nodeSucc = edge.getId_succ();
//						numOpCDFG = isOperatorNode(nodeSucc);
//						myassert(numOpCDFG != null, "Data node successor is not a operator node or this operator node have not a NUM_OP_CDFG");
//						
//						operator = null;
//						for(Operator op : _fixedSpecif.getOperators()){
//							if(op.getEval_cdfg_number() == numOpCDFG){
//								operator = op;
//								break;
//							}
//						}
//						myassert(operator != null, "No operator with CDFG number " + numOpCDFG + " find in the fixed point specification");
//						
//						if(opsInputAffectingVar.containsKey(numDataCDFG)){
//							if(!opsInputAffectingVar.get(numDataCDFG).contains(numOpCDFG))
//								opsInputAffectingVar.get(numDataCDFG).add(numOpCDFG);
//						}
//						else{
//							List<Integer> listInputAffectationVar = new ArrayList<Integer>();
//							listInputAffectationVar.add(numOpCDFG);
//							opsInputAffectingVar.put(numDataCDFG, listInputAffectationVar);
//						}
//					}
//				}
//			}
//		}
//	}
	
	/**
	 * Supprime tout les noeuds et les arcs n'étant pas connecté directement ou indirectement aux sorties.
	 * 
	 * @author nicolas simon
	 * @throws SFGGeneratorException 
	 * @date 18/07/2012
	 */
	public void deleteSecondaryGraph() throws SFGGeneratorException{
		Map<Node, List<Edge>> mapNodePred = new HashMap<Node, List<Edge>>();
		Map<Node, List<Edge>> mapNodeSucc = new HashMap<Node, List<Edge>>();
		List<Node> listNodeOutput = new LinkedList<Node>();
		List<Node> taggedNodeList = new ArrayList<Node>();
		List<Edge> taggedEdgeList = new ArrayList<Edge>();
		
		this.createMapInterdependanceNodeEdge(mapNodePred, mapNodeSucc, listNodeOutput);
		
		for(Node nodeOutput : listNodeOutput){
			taggedNodeList.add(nodeOutput);
			for(Edge edgePred : mapNodePred.get(nodeOutput)){
				taggedEdgeList.add(edgePred);
				this.addTaggedList(edgePred.getId_pred(), taggedNodeList, taggedEdgeList, mapNodePred, mapNodeSucc);
			}
		}
				
		modelGraphF2F.getEdge().clear();
		modelGraphF2F.getEdge().addAll(taggedEdgeList);
		modelGraphF2F.getNode().clear();
		modelGraphF2F.getNode().addAll(taggedNodeList);
		
		for(Node node : modelGraphF2F.getNode()){
			if(_nodeToSymbol.containsKey(node))
				addDataToFixedSpecification(_nodeToSymbol.get(node));
		}
		
		mapNodePred.clear();
		mapNodeSucc.clear();
		listNodeOutput.clear();
//		this.createMapInterdependanceNodeEdge(mapNodePred, mapNodeSucc, listNodeOutput);
//		updateInputOutputAffectationVar(mapNodePred, mapNodeSucc);
		
//		System.out.println("Input");
//		for(Integer key : SFGGenerator.opsInputAffectingVar.keySet()){
//			System.out.println("Key : " + key);
//			System.out.println("Value : ");
//			for(Integer value : SFGGenerator.opsInputAffectingVar.get(key)){
//				System.out.println(value);
//			}
//		}
//		System.out.println();
//		System.out.println("Output");
//		for(Integer key : SFGGenerator.opsOutputAffectingVar.keySet()){
//			System.out.println("Key : " + key);
//			System.out.println("Value : ");
//			for(Integer value : SFGGenerator.opsOutputAffectingVar.get(key)){
//				System.out.println(value);
//			}
//		}	
	}

	public void addDelayEdges() throws SFGGeneratorException{
		List<IndexedSymbol> keys = new ArrayList<IndexedSymbol>(lastDelayedVarNode.keySet());
		IndexedSymbolComparator comparator = new IndexedSymbolComparator();
		Collections.sort(keys, comparator);
		for (IndexedSymbol indexedSymbol : keys){
			Node endNode = lastDelayedVarNode.get(indexedSymbol);
			Node startNode = firstDelayedVarNode.get(indexedSymbol);
			if (startNode != endNode && !isDestNode(startNode)){
				// On vérifie si startNode a un edge incident, auquel cas on ne crée pas de noeud z-1
				// Car cela signifie que la variable est affectée sans être lue (et erreur sémantique dans le SFG : le noeud endNode possède deux edge incidents)
				// Il faudrait aussi vérifier le cas (équivalent au cas de l'arc incident) où la variable est initialisée avec une constante
				Node newNode = localCreateNode("z-1");
				localCreateAnnotation(newNode, "CLASS", "OP");
				localCreateAnnotation(newNode, "NATURE", "DELAY");
				localCreateAnnotation(newNode, "NUM_OP_SFG", Integer.toString(currNumOpSFG));
				currNumOpSFG++;
				
				/* Attribut numOpCDFG : ne correspond pas à une opération */
				localCreateAnnotation(newNode, "NUM_OP_CDFG", Integer.toString(-1));
				
				localCreateDataEdge(endNode, newNode, 0);
				localCreateDataEdge(newNode, startNode, null);
			}
		}
	}
	
	/**
	 * On transforme les séquences du type "X[3]" ->  "="  -> "X[4]" -> "z-1" -> "X[4]" en
	 *                                     "X[3]" -> "z-1" -> "X[4]"
	 * Ceci pour des raisons de compatibilité avec IDFix
	 * (mais j'espère qu'à terme ce ne sera plus nécessaire)
	 * De plus, on récupère le numOpCDFG du "=" pour le mettre sur le "z-1" car IDFix suppose que c'est un "="...
	 * @throws SFGGeneratorException 
	 */
	public void suppressAffectBeforeDelay() throws SFGGeneratorException{
		List<Integer> nodesToDelete = new LinkedList<Integer>();
		for (Node node : modelGraphF2F.getNode()){
			if (node.getName().equals("z-1")){
				Edge prevZ1 = getFirstPrevEdge(node);
				Node uselessVar = prevZ1.getId_pred();
				Integer numAffect = null;
				for (Annotations annot : uselessVar.getAttr()){
					if (annot.getKey().equals("NUM_AFFECT")){
						numAffect = Integer.parseInt(annot.getValue());
						break;
					}
				}
				myassert(numAffect != null, "NUM_AFFECT is null");
				
				Edge prevUselessVar = getFirstPrevEdge(uselessVar);
				Node uselessAffect = prevUselessVar.getId_pred();
				myassert(uselessAffect.getName().equals("="), "The node before z-1 is node a \"=\" node");
				Integer numOpCDFG = isOperatorNode(uselessAffect);
				myassert(numOpCDFG != null, "NUM_OP_CDFG is null");
				updateNodeAnnotation(node, "NUM_OP_CDFG", Integer.toString(numOpCDFG));
				
				Integer numBasicBlock = null;
				for (Annotations annot : uselessAffect.getAttr()){
					if (annot.getKey().equals("NUM_BASIC_BLOCK")){
						numBasicBlock = Integer.parseInt(annot.getValue());
						break;
					}
				}
				myassert(numBasicBlock != null, "NUM_BASIC_BLOCK is null");
				updateNodeAnnotation(node, "NUM_BASIC_BLOCK", Integer.toString(numBasicBlock));
				
				Edge prevUselessAffect = getFirstPrevEdge(uselessAffect);
				Node init = prevUselessAffect.getId_pred();
				//localCreateDataEdge(init, node, 0);
				
				// On déplace les arcs ayant pour source le noeud supprimé vers le noeud init
				List<Edge> edgesToDelete = new LinkedList<Edge>();
				List<Edge> edgeToCreate = new LinkedList<Edge>();
				for (Edge edge : modelGraphF2F.getEdge()){
					if (edge.getId_pred() == uselessVar){
						// On crée le nouvel arc
						Integer numInput = null;
						for (Annotations annot : edge.getAttr()){
							if (annot.getKey().equals("NUM_INPUT")){
								numInput = Integer.parseInt(annot.getValue());
								break;
							}
						}
						myassert(numInput != null, "The numInput of the older edge is null");
						Edge newEdge = Float2fixFactory.eINSTANCE.createEdge();
						newEdge.setId_pred(init);
						newEdge.setId_succ(edge.getId_succ());
						localCreateAnnotation(newEdge, "NUM_INPUT", Integer.toString(numInput));
						edgeToCreate.add(newEdge);
						// On supprime l'arc
						edgesToDelete.add(edge);
					}
				}
				for (Edge e : edgesToDelete){
					modelGraphF2F.getEdge().remove(e);
				}
				for (Edge e : edgeToCreate){
					modelGraphF2F.getEdge().add(e);
				}
				
				
				// On crée un attribut NUM_AFFECT sur la variable après le z-1 s'il n'y en a pas
				Edge nextZ1 = getFirstSuccEdge(node);
				Node finalVar = nextZ1.getId_succ();
				
				boolean found = false;
				for (Annotations annotation : finalVar.getAttr()){
					if (annotation.getKey().equals("NUM_AFFECT")){
						found = true;
						break;
					}
				}
				if (!found){
					localCreateAnnotation(finalVar, "NUM_AFFECT", Integer.toString(numAffect));
				}
				
				
				nodesToDelete.add(uselessVar.getId());
				nodesToDelete.add(uselessAffect.getId());
				modelGraphF2F.getEdge().remove(prevZ1);
				modelGraphF2F.getEdge().remove(prevUselessVar);
				modelGraphF2F.getEdge().remove(prevUselessAffect);
			}
		}
		for (Integer i : nodesToDelete){
			Iterator<Node> nodeIter = modelGraphF2F.getNode().iterator();
			while (nodeIter.hasNext()){
				Node node = nodeIter.next();
				if (node.getId() == i.intValue()){
					nodeIter.remove();
					break;
				}
			}
		}
	}
	
	
//	private void addOperationInputAffectingVar(Node operand, Integer numOpIDFix){
//		for (Annotations annotation : operand.getAttr()){
//			if (annotation.getKey().equals(ConstantPathAndName.ANNOTATION_CDFG_DATA)){
//				/* L'opérande est une variable du programme */
//				Integer varNum = Integer.parseInt(annotation.getValue());
//				if (opsInputAffectingVar.containsKey(varNum)){
//					if (!opsInputAffectingVar.get(varNum).contains(numOpIDFix)){
//						opsInputAffectingVar.get(varNum).add(numOpIDFix);
//					}
//				}
//				else {
//					List<Integer> newList = new LinkedList<Integer>();
//					newList.add(numOpIDFix);
//					opsInputAffectingVar.put(varNum,newList);
//				}
//				break;
//			}
//		}
//	}
//	
//	
//	private void addOperationOutputAffectingVar(Node node, Object destObject){
//		Integer varNum = null;
//		if (destObject instanceof Symbol){
//			varNum = AnnotateVariables.getSymbolAnnotationNumber((Symbol) destObject);
//		}
//		else if (destObject instanceof IndexedSymbol){
//			varNum = AnnotateVariables.getSymbolAnnotationNumber(((IndexedSymbol) destObject).getSymbol());
//		}
//		else {
//			throw new SFGGeneratorException("destObject de mauvais type");
//		}
//		Integer opNumInteger = null;
//		for (Annotations annotation : node.getAttr()){
//			if (annotation.getKey().equals("NUM_OP_CDFG")){
//				opNumInteger = Integer.parseInt(annotation.getValue());
//				break;
//			}
//		}
//		myassert(opNumInteger != null,"");
//		if (opsOutputAffectingVar.containsKey(varNum)){
//			if (!opsOutputAffectingVar.get(varNum).contains(opNumInteger)){
//				opsOutputAffectingVar.get(varNum).add(opNumInteger);
//			}
//		}
//		else {
//			List<Integer> newList = new LinkedList<Integer>();
//			newList.add(opNumInteger);
//			opsOutputAffectingVar.put(varNum,newList);
//		}
//	}
	
//	public static List<Integer> getOpsOutputAffectingVar(Integer varNum){
//		return opsOutputAffectingVar.get(varNum);
//	}
//	
//	public static List<Integer> getOpsInputAffectingVar(Integer varNum){
//		return opsInputAffectingVar.get(varNum);
//	}
	
	public static Integer getParamProtoToParamCall(Integer numDataCDFG){
		return paramProtoToParamCall.get(numDataCDFG);
	}
	
	public static Integer getParamCallToParamProto(Integer numDataCDFG){
		return paramCallToParamProto.get(numDataCDFG);
	}
	
	/**
	 * Rajoute une annotation PROPERTY : OUTPUT pour chaque dernier noeud correspondant à une variable OUTPUT
	 * @throws SFGGeneratorException 
	 */
	public void setOutputNodes() throws SFGGeneratorException{
		for (Node node : lastOutputVarNode.values()){
			localCreateAnnotation(node, "PROPERTY", "OUTPUT");
		}
	}
	
	
	public void renumberNodes(){
		int num = INIT_NODE_ID;
		for (Node node : modelGraphF2F.getNode()){
			node.setId(num);
			num++;
		}
	}
	
	
	
	public Node createPhiNode(Node node1, Node node2) throws SFGGeneratorException{
		Node phiNode = localCreateNode("Phi");
		localCreateAnnotation(phiNode, "CLASS","OP");
		localCreateAnnotation(phiNode, "NATURE","PHI");
		localCreateAnnotation(phiNode, "NUM_OP_SFG", Integer.toString(currNumOpSFG));
		currNumOpSFG++;
		
		/* Attribut numOpCDFG */
		Integer numOpIDFix = -1;
		localCreateAnnotation(phiNode, "NUM_OP_CDFG", Integer.toString(numOpIDFix));
		localCreateDataEdge(node1, phiNode, 0);
		localCreateDataEdge(node2, phiNode, 1);
		
		String varNum = getNodeAnnotation(node1, ConstantPathAndName.ANNOTATION_CDFG_DATA);
		String varName = getNodeAnnotation(node1, "NAME");
		String wd = getNodeAnnotation(node1, "WIDTH");
		String indexes = getNodeAnnotation(node1, "INDEXES");
		Node newNode = localCreateNode(node1.getName());
		localCreateAnnotation(newNode, "NAME", varName);
		localCreateAnnotation(newNode, "CLASS", "DATA");
		localCreateAnnotation(newNode, "NATURE", "VARIABLE_VAL_UNKNOWN");
		localCreateAnnotation(newNode, ConstantPathAndName.ANNOTATION_CDFG_DATA, varNum);
		if (wd != null){
			localCreateAnnotation(newNode, "VARIABLE_WIDTH", "TRUE");
		}
		if (indexes != null){
			localCreateAnnotation(newNode, "INDEXES", indexes);
		}
		
		/*
		 * Il faudrait passer en paramètre le symbol fusionné afin de mettre à jour la map des "Last Delayed Node" si le symbole est une var de delay
		 * Néanmoins, cela doit se faire après avoir résolu le problème de la fusion de 2 variables différentes
		 * (cf. iir4_andrei.c) qui se produit à cause de la suppression des noeuds "=" avant les z-1 quand on a un if non évaluable sur l'affection...
		if (AnnotateVariables.isDelayedVar(indexedSymbol.getSymbol())){
			for (IndexedSymbol key : firstDelayedVarNode.keySet()){
				if (key.isEqual(indexedSymbol)){
					lastDelayedVarNode.put(key, newNode);
					break;
				}
			}
		}
		*/
		
		
		localCreateDataEdge(phiNode, newNode, 1);
		return newNode;
	}
	
	
	/**
	 * Crée le nom d'un noeud dans le graphe SFG à partir du nom de la variable
	 * Dans le cas d'un tableau, on différencie le nom du noeud -- pour lequel on 
	 * ajoute les indices après le nom du tableau --
	 * et le nom de la "clé" "Name" des attributs du noeud --auquel cas on renvoie juste le nom du symbole
	 */
	public String createNodeName(Symbol s, Vector<Long> indexes, boolean key){
		if (indexes == null){
			return s.getName();
		}
		else {
			String res = s.getName();
			if (!key){
				res = res.concat(formatIndexes(indexes));
			}
			return res;
		}
	}
	
	private String formatIndexes(Vector<Long> indexes){
		String res = "";
		for (int i = 0; i < indexes.size(); i++){
			res = res.concat("[" + indexes.get(i) + "]");
		}
		return res;
	}
	
	
	
	
	/**
	 * Met à jour la valeur de l'annotation "VALUE" d'un noeud
	 * Si l'annotation existe, on la met à jour, sinon on la crée
	 * @throws SFGGeneratorException 
	 */
	public void updateNodeValueAnnotation(Node node, Object newValue) throws SFGGeneratorException{
//		System.out.println("Maj du noeud " + node + " - valeur : " + newValue);
		boolean found = false;
		String strVal;
		if (newValue instanceof Long){
			strVal = Long.toString((Long) newValue);
		}
		else if (newValue instanceof Float){
			strVal = Float.toString((Float) newValue);
		}
		else {
			throw new SFGGeneratorException("The variable value  " + node.getName() + " is not a integer or flaot valueValeur de la variable");
		}
		for (Annotations annotation : node.getAttr()){
			if (annotation.getKey().equals("VALUE")){
				annotation.setValue(strVal);
				found = true;
			}
		}
		if (!found){
			localCreateAnnotation(node, "VALUE", strVal);
		}
		updateNodeAnnotation(node, "NATURE", "VARIABLE_VAL_KNOWN");
	}
	
	
	public void updateNodeAnnotation(Node node, String key, String value) throws SFGGeneratorException{
		boolean found = false;
		for (Annotations annotation : node.getAttr()){
			if (annotation.getKey().equals(key)){
				annotation.setValue(value);
				found = true;
			}
		}
		if (!found){
			localCreateAnnotation(node, key, value);
		}
	}
	
	public String getNodeAnnotation(Node node, String key){
		for (Annotations annotation : node.getAttr()){
			if (annotation.getKey().equals(key)){
				return annotation.getValue();
			}
		}
		return null;
	}
	
	
	private void createNodeStandardAnnotations(Node node, Symbol symbol, Vector<Long> indexes, boolean possibleInput) throws SFGGeneratorException{
		if(_nodeToSymbol.containsKey(node))
			throw new SFGGeneratorException("Node already link with a symbol during the node annotation");
		_nodeToSymbol.put(node, symbol);
		localCreateAnnotation(node, "NAME", createNodeName(symbol,indexes,true));
		localCreateAnnotation(node, "CLASS", "DATA");
		localCreateAnnotation(node, "NATURE", "VARIABLE_VAL_UNKNOWN");
		localCreateAnnotation(node, ConstantPathAndName.ANNOTATION_CDFG_DATA, Integer.toString(IDFixUtils.getSymbolAnnotationNumber(symbol)));
		Integer width = IDFixUtils.getVarWidth(symbol);
		if (width != null) {
			localCreateAnnotation(node, "VARIABLE_WIDTH", "TRUE");
		}
		if (indexes != null){
			localCreateAnnotation(node, "INDEXES", formatIndexes(indexes));
		}
		if (IDFixUtils.getDynamicProcessing(symbol).equals("OVERFLOW")){
			localCreateAnnotation(node, "DYNAMIC_PROCESSING", "OVERFLOW");
			localCreateAnnotation(node, "OVERFLOW_PROBABILITY", Double.toString(IDFixUtils.getOverflowProbability(symbol)));
		}
		if (possibleInput){
			Double lowerBound = IDFixUtils.getVarMin(symbol);
			Double upperBound = IDFixUtils.getVarMax(symbol);
			if (lowerBound != null && upperBound != null){
				localCreateAnnotation(node, "VAL_MIN", Double.toString(lowerBound));
				localCreateAnnotation(node, "VAL_MAX", Double.toString(upperBound));
				localCreateAnnotation(node, "PROPERTY", "INPUT");
			}
		}
	}
	
	
	
	/**
	 * Crée les annotations d'un noeud (correspondant à une variable) que l'on vient de créer
	 * ainsi que son initialisation
	 * @throws SFGModelCreationException 
	 * @throws InterpreterException 
	 * @throws SFGGeneratorException 
	 */
	public void createNodeAnnotations(Node node, Symbol symbol, Vector<Long> indexes) throws SFGGeneratorException, InterpreterException, SFGModelCreationException{
		createNodeAnnotations(node, symbol, indexes, null);
	}
	
	public void createNodeAnnotations(Node node, Symbol symbol, Vector<Long> indexes, Object value) throws SFGGeneratorException, InterpreterException, SFGModelCreationException{
		/* On doit avoir l'équivalence (indexes != null) <=> (symbol.getTypes() instanceof ArrayType) */ 
		createNodeStandardAnnotations(node, symbol, indexes, true);
		
		/*** Initialisation de la variable ***/
		if (value == null){
			if (indexes != null){
				value = context.getValue(symbol, indexes);
			}
			else {
				value = context.getValue(symbol);
			}
		}
		
		if (value instanceof UninitializedValue){
			/* Variable qui n'a pas de valeur connue dans l'interprétation */
			/* N'est pas possible quand cette fonction est appelée directement depuis InstInterpreter */
			if (symbol.getValue() != null){
				/* Son initialisation existe mais n'est pas connue à la compilation */
				/* Exemple : int n = sample + 1; (!createNodeEqual) */
				/*      ou : int n = sample; (createNodeEqual) */
				/* On arrive ici à la première lecture de cette variable */
				/* Il faut alors créer la partie du SFG correspondant à l'initialisation */
				Node init = (Node) doSwitch(symbol.getValue());
				boolean createNodeEqual = true;
				for (Annotations a : init.getAttr()){
					if (a.getKey().equals("CLASS")){
						if (a.getValue().equals("OP")){
							createNodeEqual = false;
						}
						break;
					}
				}
				if (createNodeEqual){
					// On fait comme si l'initialisation était une affectation
					Node newNode = localCreateNode("=");
					
					localCreateAnnotation(newNode, "NUM_OP_SFG", Integer.toString(currNumOpSFG));
					currNumOpSFG++;
					
					Integer numOpCDFG = IDFixUtils.getCDFGInstructionNumber(symbol.getValue());
					myassert(numOpCDFG == null, "The annotation NUMOPCDFG of the symbol is not null");
					AnnotateOperators.createAnnotation(symbol.getValue());
					numOpCDFG = IDFixUtils.getCDFGInstructionNumber(symbol.getValue());
					myassert(numOpCDFG != null, "The annotation NUMOPCDFG of the symbol is null");
					int eval_cdfg = addOperatorToFixedSpecification(symbol.getValue());

					localCreateAnnotation(newNode, "NUM_OP_CDFG", Integer.toString(eval_cdfg));
					localCreateAnnotation(newNode, "CLASS", "OP");
					localCreateAnnotation(newNode, "NATURE", "EQ");
					
//					addOperationInputAffectingVar(init, AnnotateOperators.getNumOpIDFix(numOpCDFG));
//					addOperationOutputAffectingVar(newNode, symbol);
					
					localCreateDataEdge(init, newNode, 0);
					localCreateDataEdge(newNode, node, null);
				}
				else {
					localCreateDataEdge(init, node, 0);
				}
			}
			else {
				/* Variable non initialisée, on ne fait rien */
			}
		}
		else {
			/* On met à jour la valeur du noeud */
			updateNodeValueAnnotation(node, value);
		}
	}
	

	public void doSwitch(SetInstruction s) throws SFGModelCreationException, SFGGeneratorException, InterpreterException{
		doSwitch(s, false);
	}
	
	public void doSwitch(SetInstruction s, boolean destIsParamTheo) throws SFGModelCreationException, SFGGeneratorException, InterpreterException{
		Integer numOpCDFG = IDFixUtils.getCDFGInstructionNumber(s);
		processSetInstruction(s, s.getDest(), s.getSource(), numOpCDFG, destIsParamTheo);
	}
	
	public void processSetInstruction(Instruction set, Instruction dest, Instruction source, Integer numOpCDFG, boolean destIsParamTheo) throws SFGGeneratorException, InterpreterException, SFGModelCreationException{	
		boolean evaluable = false;
		boolean createNodeEqual = true;
		Node sourceNode = null;
		Object value = instInterpreter.doSwitch(source);
		if (value instanceof UninitializedValue){
			Object o = doSwitch(source);
			myassert((o instanceof Node), "The right node of the affectation is not a Node object");
			sourceNode = (Node) o;
			
			for (Annotations annotation : sourceNode.getAttr()){
				if (annotation.getKey().equals("CLASS")){
					if (annotation.getValue().equals("OP")){
						createNodeEqual = false;
					}
					break;
				}
			}
		}
		else {
			evaluable = true;
			createNodeEqual = false;
		}
		
		instInterpreter.setMode(InstInterpreter.LHS);
		Object destObject = instInterpreter.doSwitch(dest);
		myassert(destObject != null, "The destination of the instruction have returned null");
		
		Symbol symbol;
		Vector<Long> indexes = null;
		if (destObject instanceof Symbol){
			symbol = (Symbol) destObject;
		}
		else if (destObject instanceof IndexedSymbol){
			symbol = ((IndexedSymbol) destObject).getSymbol();
			indexes = ((IndexedSymbol) destObject).getIndexes();
		}
		else {
			throw new SFGGeneratorException("Left operand of instruction " + dest + " = " + source + " must be a Symbol or an Indexed Symbol (here : " + destObject.getClass() + ")");
		}
		
		Node destNode = localCreateNode(createNodeName(symbol, indexes, false));
		createNodeStandardAnnotations(destNode, symbol, indexes, false);
		// Les affectations peuvent ne pas être annotées dans le cas des appels de fonctions (param -> param théo)
		Integer numAffect = IDFixUtils.getNumAffectation(dest);
		if (numAffect != null){
			localCreateAnnotation(destNode, "NUM_AFFECT", Integer.toString(numAffect));
		}
		
		if (evaluable){
			updateNodeValueAnnotation(destNode, value);
		}
		else {
			if (createNodeEqual){
				/* Exemple : a = sample; */
				Node newNode = localCreateNode("=");
				localCreateAnnotation(newNode, "CLASS", "OP");
				localCreateAnnotation(newNode, "NATURE", "EQ");
				localCreateAnnotation(newNode, "NUM_OP_SFG", Integer.toString(currNumOpSFG));
				currNumOpSFG++;
				
				int eval_cdfg = addOperatorToFixedSpecification(set);
				localCreateAnnotation(newNode, "NUM_OP_CDFG", Integer.toString(eval_cdfg));
				localCreateAnnotation(newNode, "NUM_BASIC_BLOCK", IDFixUtils.getBasicBlockNumber(dest.getBasicBlock()));
				localCreateDataEdge(sourceNode, newNode, 0);
				localCreateDataEdge(newNode, destNode, null);
				
				/* On met à jour le lien entre les variables en entrée de cet opérateur et cet opérateur */
				/* Dans le cas où ce n'est pas un "=", les "liens" ont été faits dans caseGenericInstruction */
//				addOperationInputAffectingVar(sourceNode, AnnotateOperators.getNumOpIDFix(numOpCDFG));
				/* Add the operation corresponding to the node as an operation affecting the variable node */
//				addOperationOutputAffectingVar(newNode,destObject);
			}
			else {
				localCreateDataEdge(sourceNode, destNode, 0);
//				addOperationOutputAffectingVar(sourceNode,destObject);
			}
		}
		
		instInterpreter.setMode(InstInterpreter.RHS);
		/* Mise à jour du noeud correspondant à la variable ainsi que de sa valeur */
		IndexedSymbol indexedSymbol;
		if (destObject instanceof Symbol){
			indexedSymbol = new IndexedSymbol((Symbol) destObject);
			this.context.setNode(indexedSymbol.getSymbol(), destNode, destIsParamTheo);
			this.context.setValue(indexedSymbol.getSymbol(), value, destIsParamTheo);
			
			if (IDFixUtils.isDelayedVar(indexedSymbol.getSymbol())){
				StringAnnotation strAnnot = (StringAnnotation) indexedSymbol.getSymbol().getAnnotation("DELAY");
				String nodeDelay = strAnnot.getContent();
				sourceNode.getName();
				if(nodeDelay.equals(sourceNode.getName())){
					System.out.println("icicic");
					boolean delayedVarNodeExists = false;
					for (IndexedSymbol key : firstDelayedVarNode.keySet()){
						if (key.isEqual(indexedSymbol)){
							lastDelayedVarNode.put(key,destNode);
							delayedVarNodeExists = true;
							break;
						}
					}
					if (!delayedVarNodeExists){
						firstDelayedVarNode.put(indexedSymbol,sourceNode);
						lastDelayedVarNode.put(indexedSymbol,destNode);
					}
				}
			}
			
		}
		else if (destObject instanceof IndexedSymbol){
			indexedSymbol = (IndexedSymbol) destObject;
			this.context.setNode(indexedSymbol.getSymbol(), destNode, indexedSymbol.getIndexes(), destIsParamTheo);
			this.context.setValue(indexedSymbol.getSymbol(), value, indexedSymbol.getIndexes(), destIsParamTheo);
			/* S'il s'agit d'une variable de Delay, on ajoute ce noeud (le premier) dans la Map correspondante afin de pouvoir reboucler dessus à la fin */
			if (IDFixUtils.isDelayedVar(indexedSymbol.getSymbol())){
				boolean delayedVarNodeExists = false;
				for (IndexedSymbol key : firstDelayedVarNode.keySet()){
					if (key.isEqual(indexedSymbol)){
						lastDelayedVarNode.put(key,destNode);
						delayedVarNodeExists = true;
						break;
					}
				}
				if (!delayedVarNodeExists){
					firstDelayedVarNode.put(indexedSymbol,destNode);
					lastDelayedVarNode.put(indexedSymbol,destNode);
				}
			}
		}
		else {
			throw new SFGGeneratorException("Incorrect type to the affectation destination");
		}
		
		/* S'il s'agit d'une variable OUTPUT, on met à jour la map correspondante pour avoir le dernier noeud à la fin */
		/* On ne fait pas cette mise à jour dans les fonctions caseSymbolInstruction et caseArrayInstruction car contrairement
		 * au cas des "delayed nodes" ici seuls comptent les noeuds résultant d'une affectation */
		if (IDFixUtils.isOutputVar(indexedSymbol.getSymbol())){
			boolean outputVarNodeExists = false;
			for (IndexedSymbol key : lastOutputVarNode.keySet()){
				if (key.isEqual(indexedSymbol)){
					lastOutputVarNode.put(key, destNode);
					outputVarNodeExists = true;
					break;
				}
			}
			if (!outputVarNodeExists){
				lastOutputVarNode.put(indexedSymbol, destNode);
			}
		}
	}
	
	
	@Override
	public Object caseSymbolInstruction(SymbolInstruction s) {
		try{
			Symbol symb = s.getSymbol();
			Object symbolNode = context.getNode(symb);
			if (symbolNode instanceof UninitializedNode){
				Node localNode = localCreateNode(symb.getName());
				createNodeAnnotations(localNode, symb, null, null);
				/* Mise à jour du noeud correspondant à la variable */
				context.setNode(symb, localNode);
				
				if (IDFixUtils.isDelayedVar(symb)){
					IndexedSymbol indexedSymbol = new IndexedSymbol((Symbol) symb);
					firstDelayedVarNode.put(indexedSymbol,localNode);
					lastDelayedVarNode.put(indexedSymbol,localNode);
				}
				
				symbolNode = localNode;
			}
			myassert(symbolNode instanceof Node, "The object is not a Node object");
			return symbolNode;
		}catch (SFGGeneratorException e){
			throw new RuntimeException(e);
		} catch (InterpreterException e) {
			throw new RuntimeException(e);
		} catch (SFGModelCreationException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	public Object createSymbolNodeWithValue(Map<Symbol,SymbolValue> map, Object symb) throws InterpreterException, SFGGeneratorException, SFGModelCreationException{
		int index = -1;
		Symbol symbol = null;
		Vector<Long> indices = null;
		if (symb instanceof Symbol){
			index = 0;
			symbol = (Symbol) symb;
		}
		else if (symb instanceof IndexedSymbol){
			symbol = ((IndexedSymbol) symb).getSymbol();
			indices = ((IndexedSymbol) symb).getIndexes();
			index = context.getArrayIndex(symbol.getType(), indices);
		}
		else {
			myassert(false, "The symbol is a not a Symbol or IndexedSymbol");
		}
		Object symbolNode = map.get(symbol).getNodeAt(index);
		if (symbolNode instanceof UninitializedNode){
			Node localNode = localCreateNode(createNodeName(symbol, indices, false));
			createNodeAnnotations(localNode, symbol, indices, map.get(symbol).getValueAt(index));
			/* Mise à jour du noeud correspondant à la variable */
			if (symb instanceof Symbol){
				context.setNode(symbol, localNode);
			}
			else {
				context.setNode(symbol, localNode, indices);
			}
			symbolNode = localNode;
		}
		myassert(symbolNode instanceof Node, "The object is not a Node object");
		return symbolNode;
	}
	
	
	@Override
	public Object caseArrayInstruction(ArrayInstruction g){
		try{
			int savedMode = instInterpreter.getMode();
			instInterpreter.setMode(InstInterpreter.RHS);
			Vector<Long> indices = new Vector<Long>();
			for (Instruction inst : g.getIndex()){
				Object o = instInterpreter.doSwitch(inst);
				myassert((o instanceof Long), "The table index is not a long/integer (" + o + " : " + o.getClass() + ")");
				indices.add((Long) o);
			}
			instInterpreter.setMode(savedMode);
			IndexedSymbol indexedSymbol = new IndexedSymbol(((SymbolInstruction) g.getDest()).getSymbol(), indices);
			Object node = context.getNode(indexedSymbol.getSymbol(), indexedSymbol.getIndexes());
			if (node instanceof UninitializedNode){
				Node localNode = localCreateNode(createNodeName(indexedSymbol.getSymbol(), indexedSymbol.getIndexes(), false));
				createNodeAnnotations(localNode,indexedSymbol.getSymbol(),indices);
				/* Mise à jour du noeud correspondant à la variable */
				context.setNode(indexedSymbol.getSymbol(), localNode, indexedSymbol.getIndexes());
				/* S'il s'agit d'une variable de Delay, on ajoute ce noeud (le premier) dans la Map correspondante afin de pouvoir reboucler dessus à la fin */
				if (IDFixUtils.isDelayedVar(indexedSymbol.getSymbol())){
					firstDelayedVarNode.put(indexedSymbol,localNode);
					lastDelayedVarNode.put(indexedSymbol,localNode);
				}
				node = localNode;
			}
			myassert(node instanceof Node, "The object is not a Node object");
			return node;
		}catch(SFGGeneratorException e){
			throw new RuntimeException(e);
		} catch (InterpreterException e) {
			throw new RuntimeException(e);
		} catch (SFGModelCreationException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	@Override
	public Object caseGenericInstruction(GenericInstruction inst){
		try{
			Node newNode = localCreateNode(convertInstName(inst.getName()));
			localCreateAnnotation(newNode, "CLASS", "OP");
			localCreateAnnotation(newNode, "NATURE", convertInstName2(inst.getName()));
			localCreateAnnotation(newNode, "NUM_OP_SFG", Integer.toString(currNumOpSFG));
			currNumOpSFG++;
			
			/* Attribut numOpCDFG */
			int eval_cdfg = addOperatorToFixedSpecification(inst);
			localCreateAnnotation(newNode, "NUM_OP_CDFG", Integer.toString(eval_cdfg));
			localCreateAnnotation(newNode, "NUM_BASIC_BLOCK", IDFixUtils.getBasicBlockNumber(inst.getBasicBlock()));
	
			int numInput = 0;
			for (Instruction operand : inst.getOperands()) {
				Node op = (Node) doSwitch(operand);
				localCreateDataEdge(op, newNode, numInput);
				/* On met à jour le lien entre les variables en entrée de cet opérateur et cet opérateur */
				/* Ceci dans le but de pouvoir répercuter les largeurs aux variables */
	//			addOperationInputAffectingVar(op, numOpIDFix);
				numInput++;
			}
			
			return newNode;
		} catch(SFGGeneratorException e){
			throw new RuntimeException(e);
		} catch (SFGModelCreationException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	
	@Override
	public Object caseIntInstruction(IntInstruction inst){
		try{
			Long val = inst.getValue();
			String strVal = Long.toString(val);
			Node newNode = localCreateNode(strVal);
			localCreateAnnotation(newNode,"CLASS","DATA");
			localCreateAnnotation(newNode,"NATURE","CONSTANT");
			localCreateAnnotation(newNode,"NAME",strVal);
			localCreateAnnotation(newNode,"VALUE",strVal);
			return newNode;
		}catch (SFGGeneratorException e){
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Object caseFloatInstruction(FloatInstruction inst){
		try{
			Float val = new Float(inst.getValue());
			String strVal = Float.toString(val);
			Node newNode = localCreateNode(strVal);
			localCreateAnnotation(newNode,"CLASS","DATA");
			localCreateAnnotation(newNode,"NATURE","CONSTANT");
			localCreateAnnotation(newNode,"NAME",strVal);
			localCreateAnnotation(newNode,"VALUE",strVal);
		return newNode;
		}catch (SFGGeneratorException e){
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Object caseConvertInstruction(ConvertInstruction g){
		Object o = doSwitch(g.getExpr());
		return o;
	}
	
	
	@Override
	public Object caseCallInstruction(CallInstruction inst){
		try{
			SymbolInstruction symInst = (SymbolInstruction) inst.getAddress();
			ProcedureSymbol sym = (ProcedureSymbol) symInst.getSymbol();
			
			/* Getting parameters */
			Scope params = sym.getProcedure().getScope();
			
			context.initParamsTheo();
			int i = 0;
			for (Symbol param : params.getSymbols()) {
				// On crée une affectation entre l'expression passée en paramètre et le paramètre effectif
				// On peut ainsi appeler le doSwitch de SFGGenerator de manière transparente (il fera les mises à jour du contexte, création des noeuds, etc.)
				if (param.getType() instanceof ArrayType){
					Symbol arg;
					// Note : apparemment Gecos rajoute des cast en double pour les paramètres tableau de type double...
					if (inst.getArgs().get(i) instanceof ConvertInstruction){
						arg = ((SymbolInstruction) ((ConvertInstruction) inst.getArgs().get(i)).getExpr()).getSymbol();
					}
					else {
						arg = ((SymbolInstruction) inst.getArgs().get(i)).getSymbol();
					}
					AnnotateVariables.updateParamTableauAnnotations(param, arg);
					context.getParamsTheo().put(param, context.getSymbolValue(arg));
					paramProtoToParamCall.put(IDFixUtils.getSymbolAnnotationNumber(param), IDFixUtils.getSymbolAnnotationNumber(arg));
					paramCallToParamProto.put(IDFixUtils.getSymbolAnnotationNumber(arg), IDFixUtils.getSymbolAnnotationNumber(param));
				}
				else {
					Instruction child = inst.getArgs().get(i);
					SetInstruction setInstruction = InstrsFactory.eINSTANCE.createSetInstruction();
					AnnotateOperators.createAnnotation(setInstruction);
					//addOperatorToFixedSpecification(setInstruction);
//						AnnotateOperators.addNumOpIDFix(AnnotateOperators.getNumOpCDFG(setInstruction));
					SymbolInstruction symbolInstruction = InstrsFactory.eINSTANCE.createSymbolInstruction();
					symbolInstruction.setSymbol(param);
					Instruction clonedChild = child.copy();
					setInstruction.setSource(clonedChild);
					setInstruction.setDest(symbolInstruction);
					//System.out.println("Génération du DAG (3) pour l'instruction " + symbolInstruction + " = " + child);
					Vector<Object> resVector = new Vector<Object>();
					resVector.add(new UninitializedValue());
					context.getParamsTheo().put(param, new SymbolValue(resVector));
					doSwitch(setInstruction, true);
				}
				i++;
			}
			boolean savedForceReturnNodeCreation = forceReturnNodeCreation;
			forceReturnNodeCreation = true;
			context.pushFrame();
			procInterpreter.doSwitch(sym.getProcedure());
			forceReturnNodeCreation = savedForceReturnNodeCreation;
			
			context.popFrame();
			
			Node n = retNode;
			myassert((n != null) || (((BaseType) ((FunctionType) sym.getType()).getReturnType())).asVoid() != null, "The return node of a call instruction with no void return is null");
			retNode = null;
			return n;
		}catch(SFGGeneratorException e){
			throw new RuntimeException(e);
		} catch (InterpreterException e) {
			throw new RuntimeException(e);
		} catch (SFGModelCreationException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Object caseRetInstruction(RetInstruction inst){
		try{
			Node n = (Node) doSwitch(inst.getExpr());
			myassert(n != null, "The Node object is null");
			retNode = n;
			return null;
		}catch(SFGGeneratorException e){
			throw new RuntimeException(e);
		}
	}
	
	
	private class IndexedSymbolComparator implements Comparator<IndexedSymbol>{
		public int compare(IndexedSymbol is1, IndexedSymbol is2) {
			int symbValue = is1.getSymbol().getName().compareTo(is2.getSymbol().getName());
			if (symbValue != 0){
				return symbValue;
			}
			else {
				try {
					int ret = context.getArrayIndex(is1.getSymbol().getType(), is1.getIndexes())
							- context.getArrayIndex(is2.getSymbol().getType(), is2.getIndexes());
					return ret;
				} catch (InterpreterException e) {
					throw new RuntimeException(e);
				}
			}
		}
	}
	
	/**
	 * Permet de merge deux noeuds étant identiques (Représentant la même donnée dans le context actuel) 
	 * Cas dans if ou une donnée est lu dans chaque branche mais son noeud n'a pas encore était créé dans le contexte supérieur. Ceci créé deux noeuds à la place d'un.
	 * Cette fonction permet de les merger
	 * 
	 * @author nicolas simon
	 * @param node1
	 * @param node2
	 * @throws SFGGeneratorException 
	 */
	public void mergeNode(Node node1, Node node2) throws SFGGeneratorException{
		
		Integer numDataCDFGNode1 = isDataNode(node1);
		myassert(numDataCDFGNode1 != null, "The node to merge is not a data or the data have a wrong NUM_DATA_CDFG");
		
		Integer numDataCDFGNode2 = isDataNode(node2);
		myassert(numDataCDFGNode2 != null, "The node to merge is not a data or the data have a wrong NUM_DATA_CDFG");

		myassert(numDataCDFGNode1 == numDataCDFGNode2, "The two nodes to merge don't represent the same data");
		
		Edge pred = getFirstPrevEdge(node2);
		Node nodePred;
		while(pred != null){
			nodePred = pred.getId_pred();
			this.localCreateDataEdge(nodePred, node1, null);
			this.removeEdge(node2.getId());
			pred = getFirstPrevEdge(node2);
		}
		
		Edge succ = getFirstSuccEdge(node2);
		Node nodeSucc;
		int numInput = -1;
		while(succ != null){
			nodeSucc = succ.getId_succ();
			for(Annotations annot : succ.getAttr()){
				if(annot.getKey().equals("NUM_INPUT")){
					numInput = Integer.parseInt(annot.getValue());
				}
			}
			if(numInput == -1){
			this.localCreateDataEdge(node1, nodeSucc, null);
			}
			else{
				this.localCreateDataEdge(node1, nodeSucc, numInput);
			}
			this.removeEdge(nodeSucc.getId());
			succ = getFirstSuccEdge(node2);
		}
		
		modelGraphF2F.getNode().remove(node2);
	}
		
	public void initCDFGNodeInstances() {
		/* Update sfgInstances */
		for (Node sfgNode : modelGraphF2F.getNode()) {
			Integer numCDFGData = isDataNode(sfgNode);
			Integer numCDFGOp = isOperatorNode(sfgNode);
			
			if(numCDFGData != null) { //sfgNode is a Data
				Data data = _fixedSpecif.findDataFromCDFG(numCDFGData);
				data.getSfgNodeInstances().add(sfgNode);
			} else if(numCDFGOp != null) {
				Operation operator = _fixedSpecif.findOperatorFromEvalCDFG(numCDFGOp);
				operator.getSfgNodeInstances().add(sfgNode);
			}
		}
		
//		/* Update Data Pred/Successors */
//		for(Data cdfgData: _fixedSpecif.getDatas()) {
//			Set<Entry<Operator, Integer>> predOpsSet = new LinkedHashSet<Entry<Operator, Integer>>();
//			Set<Entry<Operator, Integer>> succOpsSet = new LinkedHashSet<Entry<Operator, Integer>>();
//			
//			for(Node sfgInstance : cdfgData.getSfgNodeInstances()) {
//				/* get predecessors */
//				for(Edge sfgEdge : sfgInstance.getPredecessors()) {
//					Node sfgPredNode = sfgEdge.getId_pred();
//					Integer numCDFG = isOperatorNode(sfgPredNode); //XXX assuming Data node predecessors can only be Operators
//					if(numCDFG != null) {
//						Operator predOperator = _fixedSpecif.findOperatorFromEvalCDFG(numCDFG);
//						//assert (predOperator != null, "");
//						Integer index = getSfgEdgeIndex(sfgEdge);
//						
//						predOpsSet.add(new SimpleEntry<Operator, Integer>(predOperator, index));
//					}
//				}
//				
//				/* get successors */
//				for(Edge sfgEdge : sfgInstance.getSuccessors()) {
//					Node sfgSuccNode = sfgEdge.getId_succ();
//					Integer numCDFG = isOperatorNode(sfgSuccNode); //XXX assuming Data node successors can only be Operators
//					if(numCDFG != null) {
//						Operator succOperator = _fixedSpecif.findOperatorFromEvalCDFG(numCDFG);
//						//assert (succOperator != null, "");
//						Integer index = getSfgEdgeIndex(sfgEdge);
//						succOpsSet.add(new SimpleEntry<Operator, Integer>(succOperator, index));
//					}
//				}
//			}
//			
//			for(Entry<Operator, Integer> predOperator : predOpsSet)
//				_connectCDFGNodes(predOperator.getKey(), cdfgData, predOperator.getValue());
//			for(Entry<Operator, Integer> succOperator : succOpsSet)
//				_connectCDFGNodes(cdfgData, succOperator.getKey(), succOperator.getValue());
//		}
		
		
		/* Update Data/Operator Pred/Successors */
		//XXX assuming Data pred/succ can only be Operators
		for(Operation cdfgOperator : _fixedSpecif.getOperations()) {
			Set<Entry<CDFGNode, Integer>> predCDFGNodesSet = new LinkedHashSet<Entry<CDFGNode, Integer>>();
			Set<CDFGNode> succCDFGNodesSet = new LinkedHashSet<CDFGNode>();
			
			for(Node sfgInstance : cdfgOperator.getSfgNodeInstances()) {
				for(Edge sfgEdge : sfgInstance.getPredecessors()) {
					Node sfgPredNode = sfgEdge.getId_pred();
					Integer numCDFG = isOperatorNode(sfgPredNode);
					Integer index = getSfgEdgeIndex(sfgEdge); //XXX null?
					if(numCDFG != null) {
						Operation predOperator = _fixedSpecif.findOperatorFromEvalCDFG(numCDFG);
						//assert (predOperator != null, "");
						predCDFGNodesSet.add(new SimpleEntry<CDFGNode, Integer>(predOperator, index));
					}
					else if( (numCDFG = isDataNode(sfgPredNode)) != null ) {
						Data predData = _fixedSpecif.findDataFromCDFG(numCDFG);
						predCDFGNodesSet.add(new SimpleEntry<CDFGNode, Integer>(predData, index));
					}
				}
				for(Edge sfgEdge : sfgInstance.getSuccessors()) {
					Node sfgSucNode = sfgEdge.getId_succ();
					Integer numCDFG = isOperatorNode(sfgSucNode);
					if(numCDFG != null) {
						Operation predOperator = _fixedSpecif.findOperatorFromEvalCDFG(numCDFG);
						//assert (predOperator != null, "");
						succCDFGNodesSet.add(predOperator);
					}
					else if( (numCDFG = isDataNode(sfgSucNode)) != null ) {
						Data predData = _fixedSpecif.findDataFromCDFG(numCDFG);
						succCDFGNodesSet.add(predData);
					}
				}
			}
			
			for(Entry<CDFGNode, Integer> predCDFGNode : predCDFGNodesSet) {
				CDFGEdge cdfgEdge = _connectCDFGNodes(predCDFGNode.getKey(), cdfgOperator);
				cdfgEdge.setPredIndex(predCDFGNode.getValue());
			}
			for(CDFGNode succCDFGNode : succCDFGNodesSet)
				_connectCDFGNodes(cdfgOperator, succCDFGNode);
		}
	}

	private Integer getSfgEdgeIndex(Edge sfgEdge) {
		for (Annotations annot : sfgEdge.getAttr()) {
			if(annot.getKey().equals("NUM_INPUT")) //XXX
				return Integer.valueOf(annot.getValue());
		}
		return null;
	}

	private CDFGEdge _connectCDFGNodes(CDFGNode src, CDFGNode dst) {
		for(CDFGEdge edge : _fixedSpecif.getEdges()) {
			if(edge.getPredecessor().equals(src) && edge.getSuccessor().equals(dst)) {
				return edge;
			}
		}
		CDFGEdge cdfgEdge = IDFixUserFixedPointSpecificationFactory.EDGE(src, dst);
		_fixedSpecif.getEdges().add(cdfgEdge);
		return cdfgEdge;
	}

	private Integer isOperatorNode(Node node){
		for (Annotations annot : node.getAttr()) {
			if(annot.getKey().equals("NUM_OP_CDFG")) //XXX
				return Integer.valueOf(annot.getValue());
		}
		return null;
	}

	private Integer isDataNode(Node node) {
		for (Annotations annot : node.getAttr()) {
			if(annot.getKey().equals(ConstantPathAndName.ANNOTATION_CDFG_DATA))
				return Integer.valueOf(annot.getValue());
		}
		return null;
	}
	
	
	
	private class _ResolveOperatorKind extends DefaultInstructionSwitch<OperatorKind> {

		@Override
		public OperatorKind caseGenericInstruction(GenericInstruction inst) {
			if (inst.getName().equals(ArithmeticOperator.ADD.getLiteral())){
				return OperatorKind.ADD;
			}
			else if (inst.getName().equals(ArithmeticOperator.SUB.getLiteral())){
				return OperatorKind.SUB;
			}
			else if (inst.getName().equals(ArithmeticOperator.NEG.getLiteral())){
				return OperatorKind.NEG;
			}
			else if (inst.getName().equals(ArithmeticOperator.DIV.getLiteral())){
				return OperatorKind.DIV;
			}
			else if (inst.getName().equals(ArithmeticOperator.MUL.getLiteral())){
				return OperatorKind.MUL;
			}
			else if (inst.getName().equals(BitwiseOperator.SHL.getLiteral())){
				return OperatorKind.SHL;
			}
			else if (inst.getName().equals(BitwiseOperator.SHR.getLiteral())){
				return OperatorKind.SHR;
			}
			
			return null;
		}

		@Override
		public OperatorKind caseSetInstruction(SetInstruction s) {
			return OperatorKind.SET;
		}
	}
}
