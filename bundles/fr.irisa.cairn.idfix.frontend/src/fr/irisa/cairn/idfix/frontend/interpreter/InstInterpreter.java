package fr.irisa.cairn.idfix.frontend.interpreter;

import java.util.Vector;

import float2fix.Graph;
import float2fix.Node;
import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import fr.irisa.cairn.idfix.utils.exceptions.InterpreterException;
import fr.irisa.cairn.idfix.utils.exceptions.SFGGeneratorException;
import fr.irisa.cairn.idfix.utils.exceptions.SFGModelCreationException;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.CondInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.RetInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.AliasType;
import gecos.types.ArrayType;
import gecos.types.BaseType;
import gecos.types.Type;

/**
 * InstructionInterpreter is an instruction visitor, which interprets
 * instructions. It use an ExecutionContext to evaluate the symbol instruction
 * and a ProcInterpreter to evaluate the procedures in case of call
 * instruction. This file was originally taken from the interpreter tutorial
 * @author QM
 */
public class InstInterpreter extends BlockInstructionSwitch<Object> {

	/*
	 * The mode variable allows to select the operation done by the method
	 * caseSymbolInstruction. When set to LHS (Left Hand Side), we expect the
	 * method to return the Symbol in which we put the result of an expression.
	 * When set to RHS (Right Hand Side), the result has to be the value of the
	 * variable in the execution context.
	 */
	public final static int LHS = 1;
	public final static int RHS = 2;
	private int mode;

	public void setMode(int mode) {
		this.mode = mode;
	}

	public int getMode() {
		return mode;
	}

	private ExecutionContext context;
	private ExpInterpreter expInterpreter;
	private ProcInterpreter procInterpreter;
	private SFGGenerator sfgGenerator;
	
	private boolean createNewNodeOnUpdate = false;
	
	private void myassert(boolean b, String s) throws InterpreterException{
		if (!b){
			throw new InterpreterException(s);
		}
	}

	public InstInterpreter(ExecutionContext context, ProcInterpreter interpreter, Graph graphF2F) {
		this.context = context;
		this.procInterpreter = interpreter;
		expInterpreter = new ExpInterpreter(this);
		sfgGenerator = new SFGGenerator(context,graphF2F,this,interpreter);
	}
	
	
	public boolean getCreateNewNodeOnUpdate(){
		return createNewNodeOnUpdate;
	}
	
	public void setCreateNewNodeOnUpdate(boolean b){
		createNewNodeOnUpdate = b;
	}
	

	
	public SFGGenerator getSfgGenerator(){
		return sfgGenerator;
	}

	@Override
	public Object caseIntInstruction(IntInstruction g) {
		// Return the actual Int value
		return new Long(g.getValue());
	}
	
	@Override
	public Object caseFloatInstruction(FloatInstruction g) {
		// Return the actual Float value
		return new Float(g.getValue());
	}

	@Override
	public Object caseSetInstruction(SetInstruction s) {
		try{
			// Visit Left-Hand Side of statement to find out 
			// the variable(symbol) that has to be modified.
			setMode(LHS);
			Object target = doSwitch(s.getDest());
			// Visit Right-Hand Side of statement to find out 
			// what is the new value of the variable
			setMode(RHS);
			Object value = doSwitch(s.getSource());
			if (value instanceof UninitializedValue){
				/* On utilise une valeur non initialisée dans l'expression */
				if (s.getParent() == null){
					/* On génère la partie du DAG de l'instruction */
					//System.out.println("Génération du DAG pour l'instruction " + s);
					sfgGenerator.doSwitch(s);
				}
			}
			else {
				/* L'affectation est évaluable */
				/* S'il existe déjà un noeud correspondant à la variable, il y a deux possibilités :
				 * - soit le noeud est inutilisé, dans ce cas :
				 *     -> si le noeud était le résultat d'une opération, on supprime l'arc menant au noeud
				 *     -> Dans tous les cas (noeud résultat d'une opération ou noeud contenait une valeur constante), on met à jour la constante
				 * - soit le noeud a été utilisé, dans ce cas on crée un nouveau noeud.
				 */
					boolean updateNodeValue = createNewNodeOnUpdate;
					Node nodeToInit = null;
					if (target instanceof Symbol){
						// Update symbol value
						this.context.setValue((Symbol) target, value);
						if (!(context.getNode((Symbol) target) instanceof UninitializedNode)){
							updateNodeValue = true;
							nodeToInit = (Node) context.getNode((Symbol) target);
						}
					}
					else if (target instanceof IndexedSymbol){
						// Update symbol value
						this.context.setValue(((IndexedSymbol) target).getSymbol(), value, ((IndexedSymbol) target).getIndexes());
						if (!(context.getNode(((IndexedSymbol) target).getSymbol(), ((IndexedSymbol) target).getIndexes()) instanceof UninitializedNode)){
							updateNodeValue = true;
							nodeToInit = (Node) context.getNode(((IndexedSymbol) target).getSymbol(), ((IndexedSymbol) target).getIndexes());
						}
					}
					else {
						throw new InterpreterException("Incorrect Type to the affectation destination");
					}
					if (updateNodeValue){
						myassert(nodeToInit != null || createNewNodeOnUpdate, "No nodeToInit or createNewNodeOnUpdate false");
						/* On regarde si le noeud a déjà été utilisé : pour cela, on vérifie s'il existe un arc sortant du noeud (le noeud est la source d'un arc) */
						if (sfgGenerator.isSourceNode(nodeToInit) || createNewNodeOnUpdate){
							// On crée un nouveau noeud
							//System.out.println("Génération du DAG (2) pour l'instruction " + s);
							sfgGenerator.doSwitch(s);
						}
						else {
							/* On commence par supprimer l'arc qui affecte la valeur actuelle de la variable */
							sfgGenerator.removeEdge(nodeToInit.getId());
							/* Puis on met à jour la valeur du noeud correspondant (attribut "VALUE") */
							sfgGenerator.updateNodeValueAnnotation(nodeToInit, value);
						}
					}
				
			}
			return null;
		}catch (InterpreterException e){
			throw new RuntimeException(e);
		} catch (SFGGeneratorException e) {
			throw new RuntimeException(e);
		} catch (SFGModelCreationException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Object caseCallInstruction(CallInstruction g) {
		/* Traitement vis-à-vis de la génération du SFG :
		 * - Si tous les paramètres sont évaluables, alors on interprète le résultat de la fonction
		 * - Sinon, on évalue les expressions des paramètres avec des noeuds (si les valeurs sont connues, noeud constants)
		 */
		SymbolInstruction symInst = (SymbolInstruction) g.getAddress();
		
		try{
			//TODO: no need to re-evaluate math functions since they were replaced by the flattener 
			switch(symInst.getSymbol().getName()) {
			case "cos" :
				myassert(g.getArgs().size() == 1, "Invalid call to cos! It should have only one argument.");
				Object arg1 = doSwitch(g.getArgs().get(0));
				if(arg1 instanceof Float) {
					Float valueOf = Float.valueOf((float) Math.cos((Float)arg1));
//					System.out.println("[idfix instInterpreter] cos(" + arg1 + ") = " + valueOf);
					return valueOf;
				}
				if(arg1 instanceof Long) {
					Float valueOf = Float.valueOf((float) Math.cos((Long)arg1));
//					System.out.println("[idfix instInterpreter] cos(" + arg1 + ") = " + valueOf);
					return valueOf;
				}
				throw new InterpreterException("not supported");
			case "sin" :
				myassert(g.getArgs().size() == 1, "Invalid call to cos! It should have only one argument.");
				arg1 = doSwitch(g.getArgs().get(0));
				if(arg1 instanceof Float) {
					Float valueOf = Float.valueOf((float) Math.sin((Float)arg1));
//					System.out.println("[idfix instInterpreter] sin(" + arg1 + ") = " + valueOf);
					return valueOf;
				}
				if(arg1 instanceof Long) {
					Float valueOf = Float.valueOf((float) Math.sin((Long)arg1));
//					System.out.println("[idfix instInterpreter] sin(" + arg1 + ") = " + valueOf);
					return valueOf;
				}
				throw new InterpreterException("not supported");
			case "log" :
				myassert(g.getArgs().size() == 1, "Invalid call to cos! It should have only one argument.");
				arg1 = doSwitch(g.getArgs().get(0));
				if(arg1 instanceof Long)
					return Float.valueOf((float) (Math.log((Long)arg1))); //FIXME the result might be incorrect !
				if(arg1 instanceof Float)
					return Float.valueOf((float) (Math.log((Float)arg1))); //FIXME the result might be incorrect !
				throw new InterpreterException("not supported");
			case "log2" :
				myassert(g.getArgs().size() == 1, "Invalid call to cos! It should have only one argument.");
				arg1 = doSwitch(g.getArgs().get(0));
				if(arg1 instanceof Long)
					return Float.valueOf((float) (Math.log((Long)arg1) / Math.log(2))); //FIXME the result might be incorrect !
				if(arg1 instanceof Float)
					return Float.valueOf((float) (Math.log((Float)arg1) / Math.log(2))); //FIXME the result might be incorrect !
				throw new InterpreterException("not supported");
			case "pow" :
				myassert(g.getArgs().size() == 2, "Invalid call to cos! It should have 2 arguments.");
				arg1 = doSwitch(g.getArgs().get(0));
				Object arg2 = doSwitch(g.getArgs().get(1));
				if(arg1 instanceof Float) {
					if(arg2 instanceof Float)
						return Float.valueOf((float) Math.pow((Float)arg1, (Float)arg2));
					if(arg2 instanceof Long)
						return Float.valueOf((float) Math.pow((Float)arg1, (Long)arg2));
				}
				else if(arg1 instanceof Long) {
					if(arg2 instanceof Float)
						return Float.valueOf((float) Math.pow((Long)arg1, (Float)arg2));
					if(arg2 instanceof Long)
						return Float.valueOf((float) Math.pow((Long)arg1, (Long)arg2));
				}
				throw new InterpreterException("not supported");
			case "floor" :
				myassert(g.getArgs().size() == 1, "Invalid call to cos! It should have only one argument.");
				arg1 = doSwitch(g.getArgs().get(0));
				if(arg1 instanceof Float)
					return Float.valueOf((float) Math.floor((Float)arg1));
				if(arg1 instanceof Long) //XXX
					return arg1;
				throw new InterpreterException("not supported");
			case "sqrt" :
				myassert(g.getArgs().size() == 1, "Invalid call to sqrt! It should have only one argument.");
				arg1 = doSwitch(g.getArgs().get(0));
				if(arg1 instanceof Float){
					if((Float)arg1  >= 0){
						return Float.valueOf((float) Math.sqrt(((Float)arg1)));
					}
					else{
						throw new InterpreterException("Try to interpret sqrt with parameter < 0");
					}
				}
				if(arg1 instanceof Long){ 
					if((Long)arg1  >= 0){
						return Float.valueOf((float) Math.sqrt(((Long)arg1)));
					}
					else{
						throw new InterpreterException("Try to interpret sqrt with parameter < 0");
					}
				}
				throw new InterpreterException("not supported");
			default:
				ProcedureSymbol sym = (ProcedureSymbol) symInst.getSymbol();
				Scope params = sym.getProcedure().getScope();
		
				myassert((params.getSymbols().size() == (g.getArgs().size())), "Wrong Number of argument in call to " + sym.getName());
				
				boolean allParamsEvaluable = true;
				for (Instruction child : g.getArgs()) {
					if (child instanceof SymbolInstruction && ((SymbolInstruction) child).getType().isArray()){
						SymbolInstruction symbolInstruction = (SymbolInstruction) child;
						if (!context.arrayIsFullyInitialized(symbolInstruction.getSymbol())){
							allParamsEvaluable = false;
							break;
						}
					}
					else {
						Object result = doSwitch(child);
						if (result instanceof UninitializedValue){
							allParamsEvaluable = false;
							break;
						}
					}
				}
				
				if (allParamsEvaluable){
					context.initParamsTheo();
					int i = 1;
					for (Symbol param : params.getSymbols()) {
						// Update new Symbol's value with the value passed in parameter
						if (param.getType() instanceof ArrayType){
							Symbol arg = ((SymbolInstruction) g.getChildAt(i)).getSymbol();
							context.getParamsTheo().put(param, context.getSymbolValue(arg));
						}
						else {
							Instruction child = g.getChildAt(i);
							Object result = doSwitch(child);
							Vector<Object> resVector = new Vector<Object>();
							resVector.add(result);
							context.getParamsTheo().put(param, new SymbolValue(resVector));
						}
						i++;
					}
					context.pushFrame();
					Object result = procInterpreter.doSwitch(sym.getProcedure());
					
					myassert(g.getParent() == null || result != null, "result of the interpretation of the function is null");
					// Soit le résultat est connu (interprété), soit on ne l'utilise pas
					context.popFrame();
					return result;
				}
				else {
					if (g.getParent() == null){
						// Cas de l'appel d'une fonction comme seule instruction (retour de la fonction de type void)
						sfgGenerator.doSwitch(g);
						return null;
					}
					else {
						return new UninitializedValue();
					}
				}
			}
		} catch(InterpreterException e){
			throw new RuntimeException(e);
		}
		
//		if(symInst.toString().equals("sqrt")){
//			if (g.getParent() == null){
//				// Cas de l'appel d'une fonction comme seule instruction (retour de la fonction de type void)
//				sfgGenerator.doSwitch(g);
//				return null;
//			}
//			else {
//				return new UninitializedValue();
//			}
//		}
//		else{
//			try{
//				ProcedureSymbol sym = (ProcedureSymbol) symInst.getSymbol();
//				Scope params = sym.getProcedure().getScope();
//		
//				myassert((params.getSymbols().size() == (g.getChildren().size() - 1)), "Wrong Number of argument in call to " + sym.getName());
//				
//				boolean allParamsEvaluable = true;
//				for (int i = 1; i < g.getChildren().size(); i++) {
//					Instruction child = g.getComponentAt(i);
//					if (child instanceof SymbolInstruction && ((SymbolInstruction) child).getType().isArray()){
//						SymbolInstruction symbolInstruction = (SymbolInstruction) child;
//						if (!context.arrayIsFullyInitialized(symbolInstruction.getSymbol())){
//							allParamsEvaluable = false;
//							break;
//						}
//					}
//					else {
//						Object result = doSwitch(child);
//						if (result instanceof UninitializedValue){
//							allParamsEvaluable = false;
//							break;
//						}
//					}
//				}
//				
//				if (allParamsEvaluable){
//					context.initParamsTheo();
//					int i = 1;
//					for (Symbol param : params.getSymbols()) {
//						// Update new Symbol's value with the value passed in parameter
//						if (param.getType() instanceof ArrayType){
//							Symbol arg = ((SymbolInstruction) g.getComponentAt(i)).getSymbol();
//							context.getParamsTheo().put(param, context.getSymbolValue(arg));
//						}
//						else {
//							Instruction child = g.getComponentAt(i);
//							Object result = doSwitch(child);
//							Vector<Object> resVector = new Vector<Object>();
//							resVector.add(result);
//							context.getParamsTheo().put(param, new SymbolValue(resVector));
//						}
//						i++;
//					}
//					context.pushFrame();
//					Object result = procInterpreter.doSwitch(sym.getProcedure());
//					
//					myassert(g.getParent() == null || result != null, "result of the interpretation of the function is null");
//					// Soit le résultat est connu (interprété), soit on ne l'utilise pas
//					context.popFrame();
//					return result;
//				}
//				else {
//					if (g.getParent() == null){
//						// Cas de l'appel d'une fonction comme seule instruction (retour de la fonction de type void)
//						sfgGenerator.doSwitch(g);
//						return null;
//					}
//					else {
//						return new UninitializedValue();
//					}
//				}
//			}catch(InterpreterException e){
//				throw new RuntimeException(e);
//			}
//		}
	}

	
	@Override
	public Object caseSymbolInstruction(SymbolInstruction object) {
		if (getMode() == LHS) {
			// we want to know which symbol to write, we therefore 
			// return the Symbol referenced by the instruction 
			return object.getSymbol();
		}
		else {
			// we want to know what to write, we use the value of 
			// the symbol referenced by the instruction 
			Object res;
			try {
				res = this.context.getValue(object.getSymbol());
			} catch (InterpreterException e) {
				throw new RuntimeException(e);
			}
			//System.out.println("CaseSymbol: On retourne " + res + " (classe " + res.getClass() + ")");
			return res;
		}
	}

	@Override
	public Object caseGenericInstruction(GenericInstruction g) {
		return expInterpreter.doSwitch(g);
	}

	@Override
	public Object caseCondInstruction(CondInstruction g) {
		Object res = doSwitch(g.getCond());
		return res;
	}
	
	@Override
	public Object caseArrayInstruction(ArrayInstruction g){
		try{
			if (getMode() == LHS){
				/* On repasse en RHS pour l'évaluation des indices du tableau */
				setMode(RHS);
				Vector<Long> indices = _constructIndices(g);
				setMode(LHS);
				IndexedSymbol indexedSymbol = new IndexedSymbol(((SymbolInstruction) g.getDest()).getSymbol(), indices);
				return indexedSymbol;
			}
			else {
				Vector<Long> indices = _constructIndices(g);
				return context.getValue(((SymbolInstruction) g.getDest()).getSymbol(), indices);
			}
		}catch (InterpreterException e) {
			throw new RuntimeException(e);
		}
	}

	private Vector<Long> _constructIndices(ArrayInstruction g) throws InterpreterException {
		Vector<Long> indices = new Vector<Long>();
		for (Instruction index : g.getIndex()){
			// Mettre un truc du style "evalRequired = true"
			Object o = doSwitch(index);
			if (!(o instanceof Long)){
				throw new InterpreterException("Couldn't evaluate array subscript! must be an integer. (" + g + ")");
			}
			indices.add((Long) o);
		}
		return indices;
	}

	
	@Override
	public Object caseRetInstruction(RetInstruction g) {
		Instruction expr = g.getExpr();
		if (expr != null){
			Object o = doSwitch(expr);
			if (o instanceof Long){
				if (sfgGenerator.returnNodeForced()){
					sfgGenerator.doSwitch(g);
				}
				return o;
			}
			else if (o instanceof Float){
				if (sfgGenerator.returnNodeForced()){
					sfgGenerator.doSwitch(g);
				}
				return o;
			}
			else if (o instanceof UninitializedValue){
				sfgGenerator.doSwitch(g);
				return o;
			}
			else {
				throw new RuntimeException(new InterpreterException("Unsupported return type : " + o.getClass()));
			}
		}
		else {
			return null; // cas d'une fonction void
		}
	}
	
	
	@Override
	public Object caseConvertInstruction(ConvertInstruction g){
		Object o = doSwitch(g.getExpr());
		
		if(!(o instanceof UninitializedValue)){
			Type castType = g.getType();
			
			if(castType instanceof AliasType) {
				castType = ((AliasType) castType).getAlias();
			}
			
			if(castType instanceof BaseType) {
//			if(g.getType().isScalarType()){
				BaseType bType = (BaseType) castType;
				if(bType.asInt() != null){
					if(o instanceof Float){
						o = (long) ((float)o);
					}
					else if(!(o instanceof Long)){
						throw new RuntimeException("Scalar type unknown");
					}
				}
				else if(bType.asFloat() != null){
					if(o instanceof Long){
						o = Float.valueOf(((Long) o).floatValue()); //(float) o;
					}
					else if(!(o instanceof Float)){
						throw new RuntimeException("Scalar type unknown");
					}	
				}
				else{
					throw new RuntimeException("Cast not managed yet");
				}
			}
		}
		
		return o;
	}
	
	
	public void specialCaseCallInstruction(CallInstruction g){
		
	}

}
