package fr.irisa.cairn.idfix.frontend.interpreter;

/**
 * Object representing an uninitialized value in the interpretation (we cannot use null with switches)
 * @author qmeunier
 */
class UninitializedValue {
	
	public UninitializedValue(){
		
	}
	
}
