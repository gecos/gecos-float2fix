package fr.irisa.cairn.idfix.frontend.interpreter;

/**
 * The ReturnFoundMessage exception is used to handle return instructions
 * (which breaks the execution flow). 
 * @author QM
 */
public class ReturnFloat extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private Float result;
	
	ReturnFloat(Float f) {
		this.result = f;
	}
	
	public Float getResult() {
		return result;
	}
}
