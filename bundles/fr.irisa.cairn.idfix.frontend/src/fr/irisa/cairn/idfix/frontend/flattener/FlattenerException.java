package fr.irisa.cairn.idfix.frontend.flattener;

@SuppressWarnings("serial")
public class FlattenerException extends Exception {
	public FlattenerException(String msg){
		super(msg);
	}
}
