package fr.irisa.cairn.idfix.frontend.interpreter;

/**
 * The ReturnFoundMessage exception is used to handle return instructions
 * (which break the execution flow).
 * @author QM
 */
public class ReturnInt extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private Long result;
	
	ReturnInt(Long i) {
		this.result = i;
	}
	
	public Long getResult() {
		return result;
	}
}
