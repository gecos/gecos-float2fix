//package fr.irisa.cairn.idfix.frontend.sfg.generator;
//
//import float2fix.Graph;
//import float2fix.util.Float2FixIO;
//import fr.irisa.cairn.idfix.utils.IDFixUtils;
//import fr.irisa.cairn.idfix.utils.exceptions.SFGGeneratorException;
//import fr.irisa.cairn.idfix.utils.exceptions.SFGModelCreationException;
//import gecos.core.Procedure;
//import gecos.core.ProcedureSet;
//
///**
// * Create a Signal Flow Graph from a procedure set. The procedure set must be represented a DFG.
// * @author Nicolas Simon
// * @date Jul 19, 2013
// */
//public class SFGGenerator {
//	private ProcedureSet _ps;
//	private String _outputFolder;
//	private String _outputFileName;
//	
//	private final boolean _interpretSetSourceKnwonValue;
//	
//	public SFGGenerator(ProcedureSet ps, String outputFolder, String outputFileName){
//		_ps = ps;
//		_outputFolder = outputFolder;
//		_outputFileName = outputFileName;
//		_interpretSetSourceKnwonValue = false;
//	}
//	
//	public SFGGenerator(ProcedureSet ps, String outputFolder, String outputFileName, boolean interpretSetSourceKnown){
//		_ps = ps;
//		_outputFolder = outputFolder;
//		_outputFileName = outputFileName;
//		_interpretSetSourceKnwonValue = interpretSetSourceKnown;
//	}
//	
//	/**
//	 * Launch the generation of the Signal Flow Graph
//	 * @throws SFGGeneratorException
//	 * @throws SFGModelCreationException
//	 */
//	public void generate() throws SFGGeneratorException, SFGModelCreationException{
//		Procedure mainProcedure = null;
//		for(Procedure p : _ps.listProcedures()){
//			if(IDFixUtils.isMainFunc(p.getSymbol())){
//				mainProcedure = p;
//				break;
//			}
//		}
//		
//		if(mainProcedure == null)
//			throw new RuntimeException("Error : no main function defined (use the pragma MAIN_FUNC to define the entry function point)");
//		
//		SFGFactory factory = new SFGFactory();
//		ProcedureGenerator procedureGenerator = new ProcedureGenerator(factory, _interpretSetSourceKnwonValue);
//		factory.generateVariableNode(mainProcedure.listParameters());
//		procedureGenerator.doSwitch(mainProcedure);
//		
//		Graph graph = factory.finishhim();
//		
//		Float2FixIO.save(_outputFolder + _outputFileName , graph);
//		ConvertSFGToDot.generate(graph, _outputFolder, _outputFileName +".dot", true);
//	}
//}
