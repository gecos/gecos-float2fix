package fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils;

import fr.irisa.cairn.idfix.utils.exceptions.FlattenerException;
import gecos.types.Field;

import java.util.HashMap;
import java.util.Map;

/**
 * Represent data structure for a structure type.
 * @author Nicolas Simon
 * @date Jun 28, 2013
 */
public class SymbolStruct<T extends ISymbolType> implements ISymbolType {
	private Map<Field, ISymbolType> _values;
	
//	public SymbolStruct(){
//		_values = new HashMap<>();
//	}
	
	public SymbolStruct(Map<Field, T> values){
		_values = new HashMap<Field, ISymbolType>(values);
	}
	
	public SymbolStruct(SymbolStruct<T> other){
		_values = new HashMap<>(other._values);
	}
	
	public void setValue(Field field, T value) throws FlattenerException{
		if(!_values.containsKey(field))
			throw new FlattenerException("Error: The structure field (" + field +") have been not found");
		_values.put(field, value);
	}
	
	public ISymbolType getValues(Field field) throws FlattenerException{
		if(!_values.containsKey(field))
			throw new FlattenerException("Error: The structure field (" + field +") have been not found");
		ISymbolType ret = _values.get(field);
		return ret;
	}
	
	public SymbolStruct<T> copy(){
		return new SymbolStruct<>(this);
	}
	
	public String toString(){
		String res = "SymbolStruct@ (";
		int i = 0;
		for(Field f : _values.keySet()){
			if(i < _values.size() - 1)
				res += f.getName() + "= " + _values.get(f) +",";
			else
				res += f.getName() + " = " + _values.get(f);
			i++;
		}
		res += ")";
		return res;
	}
}
