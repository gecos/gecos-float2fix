package fr.irisa.cairn.idfix.frontend.flattener;

import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import gecos.annotations.PragmaAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.CompositeBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.Symbol;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Entry point for the Flattener. It processes a Procedure, and interprets it
 * when possible. When an interpretation is not possible, the instruction is
 * added to the flatten output.
 * 
 * @author qmeunier
 */
public class SourceFlattener {

	private ProcedureSet ps;
	private ProcFlattener procFlattener;
	private ExecutionContext context;
	private ExecutionContext newContext;	
	
	public SourceFlattener(ProcedureSet ps) {
//		this.ps = ps;
//		initiateAttributes(false, false);
		this(ps, false, false);
	}
	
	public SourceFlattener(ProcedureSet ps, boolean interpret) {
//		this.ps = ps;
//		initiateAttributes(false, interpret);
		this(ps, interpret, false);
	}
	
	public SourceFlattener(ProcedureSet ps, boolean interpret, boolean partial) {
		this.ps = ps;
		if (partial){
			PartialFlatteningBlockAnnotation flatteningAnnotationsVisitor = new PartialFlatteningBlockAnnotation(ps);
			flatteningAnnotationsVisitor.compute();
		}
		initiateAttributes(partial, interpret);
	}
		
	private void initiateAttributes(boolean partial, boolean interpret){
		this.context = new ExecutionContext();
		this.newContext = new ExecutionContext();
		this.procFlattener = new ProcFlattener(context, newContext, partial, interpret);
		this.context.setProcFlattener(this.procFlattener);
		this.newContext.setProcFlattener(this.procFlattener);
	}
	
	private void myassert(boolean b, String s) throws FlattenerException{
		if (!b){
			throw new FlattenerException(s);
		}
	}
	
	private boolean isMainFunc(Symbol s){
		PragmaAnnotation pragmaAnnotation = s.getPragma();
		if (pragmaAnnotation != null){
			StringBuilder sBuilder = new StringBuilder();
			for (String str : ((PragmaAnnotation) pragmaAnnotation).getContent()) {
				sBuilder.append(str);
			}
			Pattern pattern = Pattern.compile("\\.*MAIN_FUNC\\.*");
			Matcher matcher = pattern.matcher(sBuilder);
			if (matcher.find()) {
				return true;
			}
		}
		return false;
	}
	
	
	public void compute() throws FlattenerException {
		
		// We look for the main procedure: it must have the pragma "MAIN_FUNC"
		Procedure p = null;
		Iterator<Procedure> it = ps.listProcedures().iterator();
		while (p == null && it.hasNext()) {
			Procedure proc = it.next();
			if (isMainFunc(proc.getSymbol())){
				p = proc;
			}
		}
		myassert(p != null, "*** Error: no main function defined (please annotate system's main procedure with #pragma MAIN_FUNC)");
		
		// Pushing the global scope
		context.push(ps.getScope());
		newContext.push(ps.getScope());
		// Visiting the main procedure
		// On push le scope du main "à la main" et c'est la seule fonction, car pour les autres on passe par les param Théo du caseCallInstruction
		context.push(p.getScope());
		newContext.push(p.getScope());
		procFlattener.doSwitch(p);
		// On ajoute les procédures créées (suite aux appels de fonction qui dupliquent les fonctions)
		// Il faut parcourir les procédures toujours dans le même ordre...
		List<Procedure> keys = new ArrayList<Procedure>(procFlattener.getManagerOfProc().keySet());
		ProcedureComparator comparator = new ProcedureComparator();
		Collections.sort(keys, comparator);
		for (Procedure proc : keys){
			if (ps.findProcedure(proc.getSymbol().getName()) == null){
				ps.addProcedure(proc);
			}
		}
		Vector<Procedure> procToRemove = new Vector<Procedure>();
		for (Procedure proc : ps.listProcedures()){
			BlockManager manager = procFlattener.getManagerOfProc().get(proc);
			if (manager == null){
				procToRemove.add(proc);
			}
			else {
				CompositeBlock mainBlock = manager.getMainBlock();
				if(mainBlock.getChildren().size() != 1 && !(mainBlock.getChildren().get(0) instanceof CompositeBlock))
					throw new FlattenerException("The temporary block created to store the body block");
				
				mainBlock = (CompositeBlock) mainBlock.getChildren().get(0);
				BasicBlock bb1 = GecosUserBlockFactory.BBlock();
				BasicBlock bb2 = GecosUserBlockFactory.BBlock();
				mainBlock.getChildren().add(0, bb1);
				mainBlock.getChildren().add(bb2);
				proc.setBody(mainBlock);
				proc.setStart(bb1);
				proc.setEnd(bb2);
			}
		}
		
		for (Procedure proc : procToRemove){
			ps.removeProcedure(proc);
		}
		
		for (Procedure proc : procFlattener.getManagerOfProc().keySet()){
			procFlattener.getManagerOfProc().get(proc).mergeBasicBlocksWithoutScope();
		}
	}
	
	
	private class ProcedureComparator implements Comparator<Procedure>{
		public int compare(Procedure p1, Procedure p2) {
			return p1.getSymbol().getName().compareTo(p2.getSymbol().getName());
		}
	}
	

}
