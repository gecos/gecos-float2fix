
package fr.irisa.cairn.idfix.frontend.utils;

/**
 * @author qmeunier
 * Annotates all operators in the CDFG with a unique number
 * This class also provides some tools relative to operator treatment:
 * - it makes the conversion between an CDFG operator number and the corresponding IDFix number
 * - it keeps the nature (eg : add) of each operator in the CDFG (useful for cost evaluation)
 * - it counts how many times is used each operator in the SFG 
 */
import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import gecos.annotations.AnnotationsFactory;
import gecos.annotations.StringAnnotation;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.instrs.CallInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;

public class AnnotateOperators {
	private ProcedureSet ps;
	
	// Ces attributs ne font pas partie de la classe Visiteur car on peut avoir à annoter des affectations qui ne font pas partie
	// du programme initial (en particulier lors de l'appel de fonctions), c'est pourquoi la fonction createAnnotation est publique
	private static Integer count; /* Num courant pour les opérations CDFG */

	public AnnotateOperators(ProcedureSet ps) {
		this.ps = ps;
		count = 0;
	}
	
	public void compute() {
		VisitorF2FAddAnnotationToOperator monVisitor = new VisitorF2FAddAnnotationToOperator(ps);
		monVisitor.compute();
	}
	
	public static void createAnnotation(Instruction i){ 
		StringAnnotation annotation = AnnotationsFactory.eINSTANCE.createStringAnnotation();
		String nombre = Integer.toString(count);
		annotation.setContent(nombre);
		i.getAnnotations().put(ConstantPathAndName.ANNOTATION_CDFG_OPERATOR, annotation);
		count++;
	}

	private class VisitorF2FAddAnnotationToOperator extends BlockInstructionSwitch<Object> {
		private ProcedureSet modelps;

		public VisitorF2FAddAnnotationToOperator(ProcedureSet modelps) {
			this.modelps = modelps;
		}

		public void compute() {
			for (Procedure proc : modelps.listProcedures()) {
				this.doSwitch(proc);
			}
		}

		@Override
		public Object caseGenericInstruction(GenericInstruction g) {
			createAnnotation(g);
			return super.caseGenericInstruction(g);
		}

		@Override
		public Object caseSetInstruction(SetInstruction s) {
			createAnnotation(s);
			return super.caseSetInstruction(s);
		}
		
		@Override
		public Object caseCallInstruction(CallInstruction c){
			SymbolInstruction symInst = (SymbolInstruction) c.getAddress();
			if(symInst.toString().equals("sqrt")){
				createAnnotation(c);
			}
			return super.caseCallInstruction(c);
		}
	}
}
