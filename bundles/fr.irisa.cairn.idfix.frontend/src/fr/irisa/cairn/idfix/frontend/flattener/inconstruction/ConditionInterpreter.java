package fr.irisa.cairn.idfix.frontend.flattener.inconstruction;

import fr.irisa.cairn.gecos.model.tools.switches.BasicBlockSwitch;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils.Context;
import gecos.blocks.BasicBlock;
import gecos.instrs.Instruction;

//FIXME ne plus utiliser InstructionFlattener
public class ConditionInterpreter extends BasicBlockSwitch<Object> {
	private InstructionFlattener _intFlattener;
	
	public ConditionInterpreter(Context context, BlockManager manager){
		_intFlattener = new InstructionFlattener(context, manager);
	}
	
	@Override
	public Object caseBasicBlock(BasicBlock b) {
		Object res;
		for(Instruction i : b.getInstructions()){
			res = _intFlattener.doSwitch(i);
			if(res != null)
				return res;
		}
		return null;
	}
}
