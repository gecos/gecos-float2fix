package fr.irisa.cairn.idfix.frontend;

import java.io.PrintStream;

import org.apache.logging.log4j.Logger;

import fr.irisa.cairn.gecos.model.c.generator.XtendCGenerator;
import fr.irisa.cairn.idfix.frontend.flattener.FlattenerException;
import fr.irisa.cairn.idfix.frontend.flattener.SourceFlattener;
import fr.irisa.cairn.idfix.frontend.interpreter.Interpreter;
import fr.irisa.cairn.idfix.frontend.simulation.ValueAffectationSimulation;
import fr.irisa.cairn.idfix.frontend.utils.AnnotateAffectations;
import fr.irisa.cairn.idfix.frontend.utils.AnnotateBasicBlocks;
import fr.irisa.cairn.idfix.frontend.utils.AnnotateOperators;
import fr.irisa.cairn.idfix.frontend.utils.AnnotateVariables;
import fr.irisa.cairn.idfix.frontend.utils.CellManagement;
import fr.irisa.cairn.idfix.model.factory.IDFixUserInformationAndTimingFactory;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixProject;
import fr.irisa.cairn.idfix.model.informationandtiming.Section;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.exceptions.InterpreterException;
import fr.irisa.cairn.idfix.utils.exceptions.SFGGeneratorException;
import fr.irisa.cairn.idfix.utils.exceptions.SFGModelCreationException;
import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;
import fr.irisa.cairn.idfix.utils.logging.IDFixLogger;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;
import gecos.gecosproject.GecosprojectFactory;

/**
 * IDFIX frontend.
 * Contain all the need to create the signal flow graph (SFG) requested by IDFIX-eval part. 
 * Add informations of this SFG in the fixed point specification.
 * 	- Annotate operators/variables and add to the fixed point specification operator/variables information 
 * 	- Flatten the IR
 *  - Annotate affectation operator to prepare simulation
 *  - Annotate basic block to manage conditional structure
 *  - Simulate if necessary
 *  - Creation of the signal flow graph with interpretation
 * @author Nicolas Simon
 * @date Apr 12, 2013
 */
public class IDFixConvFrontEnd {
	private IdfixProject _idfixProject;
	private int _nbSimulation;
	
	private Section _sectionTimeLog;
	
	@SuppressWarnings("unused")
	private PrintStream _out;
	@SuppressWarnings("unused")
	private PrintStream _err;
	
	private static final Logger _logger = IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_FRONTEND);
	
	/**
	 * Block which create the Gecos project from the input c source code and call the transformation needed to generate the signal flow graph
	 * @param cFilePath
	 * @param outputDirPath
	 * @param nbSimulation
	 */
	public IDFixConvFrontEnd(IdfixProject idfixProject, int nbSimulation){
		_idfixProject = idfixProject;
		_nbSimulation = nbSimulation;
		_sectionTimeLog = IDFixUserInformationAndTimingFactory.SECTION("FrontEnd");
		_idfixProject.getProcessInformations().getTiming().getSections().add(_sectionTimeLog);
		
		_out = System.out;
		_err = System.err;
	}
		
	/**
	 * Set a custom {@link PrintStream} for all out messages. It is System.out by default
	 * @param out
	 */
	public void setPrintStreamOut(PrintStream out){
		_out = out;
	}
	
	/**
	 * Set a custom {@link PrintStream} for all err messages. It is System.err by default
	 * @param err
	 */
	public void setPrintStreamErr(PrintStream err){
		_err = err;
	}
	
	/**
	 * Launch the generation of the signal flow graph
	 * @return path to the signal flow graph file generated
	 */
	public void compute(){
		long timingStart, timingStop;
		timingStart = System.currentTimeMillis();
		
		ProcedureSet ps;
		if(_idfixProject.getGecosProject().getSources().size() != 1)
			throw new RuntimeException("IDFix FrontEnd is only compatible to work with just one procedure set");
	
		try{
			_logger.info("    *** SFG model creation ***");
			ps = _idfixProject.getGecosProject().getSources().get(0).getModel();
			this.SFGModelCreation(ps);
		} catch (SFGModelCreationException | SimulationException | InterpreterException | SFGGeneratorException | FlattenerException e) {
			_logger.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
		
		timingStop = System.currentTimeMillis();
		_sectionTimeLog.setTime((timingStop - timingStart) / 1000f);
	}
	
	private void SFGModelCreation(ProcedureSet ps) throws InterpreterException, SFGGeneratorException, SFGModelCreationException, FlattenerException, SimulationException{
		Long timingStart;
		Long timingStop;
		ProcedureSet psAnnotedAndNotUnrolled;
		
		/*
		 * Operations annotation 
		 */
		timingStart = System.currentTimeMillis();
		_logger.info("               - Operators annotation");
		AnnotateOperators annotateOp = new AnnotateOperators(ps);
		annotateOp.compute();
		timingStop = System.currentTimeMillis();
		_sectionTimeLog.addTimeLog("Operators annotation", timingStart, timingStop);
		
		/*
		 * Variables annotation
		 */
		timingStart = System.currentTimeMillis();
		_logger.info("               - Variables annotation");
		AnnotateVariables annotateVar = new AnnotateVariables(ps);
		annotateVar.compute();
		psAnnotedAndNotUnrolled = ps.copy();
		GecosProject projectAnnoted = GecosprojectFactory.eINSTANCE.createGecosProject();
		projectAnnoted.setName("IDFixFixProjectAnnotedAndNotUnrolled");
		GecosSourceFile sourceFile = GecosprojectFactory.eINSTANCE.createGecosSourceFile();
		sourceFile.setName(((GecosSourceFile) ps.eContainer()).getName());
		sourceFile.setModel(psAnnotedAndNotUnrolled);
		projectAnnoted.getSources().add(sourceFile);
		
		timingStop = System.currentTimeMillis();
		_idfixProject.setGecosProjectAnnotedAndNotUnrolled(projectAnnoted);
		_sectionTimeLog.addTimeLog("Variables annotation", timingStart, timingStop);
		
		new CellManagement(_idfixProject).compute();
		new XtendCGenerator(_idfixProject.getGecosProject(), _idfixProject.getIDFixConvOutputFolderPath()).compute();
	
		/*
		 * Loop unrolling ( need to delete all control structure inside the CDFG)
		 */
		timingStart = System.currentTimeMillis();
		_logger.info("               - First interpretation and flattening");
		SourceFlattener flatten = new SourceFlattener(ps);//, false, Float2fixParams.getOutputGenDirectory() +"flatten.c");
		flatten.compute();
//		Flattener newflatten = new Flattener(_ps);
//		try {
//			newflatten.compute();
//		} catch (fr.irisa.cairn.idfix.utils.exceptions.FlattenerException e) {
//			throw new RuntimeException(e);
//		}
		timingStop = System.currentTimeMillis();
		_sectionTimeLog.addTimeLog("Flattening process", timingStart, timingStop);

		/*
		 * Set operation annotation. These annotations are used by the simulation part
		 */
		timingStart = System.currentTimeMillis();
		_logger.info("               - Affectations annotation");
		AnnotateAffectations annotateAff = new AnnotateAffectations(ps);
		annotateAff.compute();
		_sectionTimeLog.addTimeLog("Affectations annotation", timingStart, timingStop);
		
		/*
		 * Basic blocks annotation
		 */
		timingStart = System.currentTimeMillis();
		_logger.info("               - Basic blocks annotation");
		AnnotateBasicBlocks annotateBB = new AnnotateBasicBlocks(ps);
		annotateBB.compute();
		timingStop = System.currentTimeMillis();
		_sectionTimeLog.addTimeLog("Basic blocks annotation", timingStart, timingStop);
		
		/*
		 * Simulation
		 */
		if(_nbSimulation > 0){		
			timingStart = System.currentTimeMillis();
			_logger.info("               - Simulation (x" + _nbSimulation + ")");
			ValueAffectationSimulation simulation = new ValueAffectationSimulation(ps, _nbSimulation, _idfixProject.getOutputFolderPath());
			simulation.compute();
			timingStop = System.currentTimeMillis();
			_sectionTimeLog.addTimeLog("Simulation", timingStart, timingStop);
		}
		
		/*
		 * Interpretation and SFG generation
		 */
		timingStart = System.currentTimeMillis();
		_logger.info("               - Final interpretation and SFG generation");
//		SFGGenerator generator = new SFGGenerator(_ps, _outputDirPath + ConstantPathAndName.IDFIX_CONV_OUTPUT_FOLDER_NAME, ConstantPathAndName.GEN_SFG_FILE_NAME, true);
//		generator.generate();
		Interpreter interpret = new Interpreter(ps, _idfixProject.getFixedPointSpecification(), _idfixProject.getSFGFilePath());
		interpret.compute();
		timingStop = System.currentTimeMillis();
		_sectionTimeLog.addTimeLog("SFG generation", timingStart, timingStop);
	}	
}
