
package fr.irisa.cairn.idfix.frontend.simulation;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import org.apache.logging.log4j.Level;

import affectationValueBySimulation.AffectationValueBySimulationFactory;
import affectationValueBySimulation.VariableValues;
import fr.irisa.cairn.gecos.model.analysis.types.ArrayLevel;
import fr.irisa.cairn.gecos.model.analysis.types.BaseLevel;
import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableCGenerator;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableTypeCGenerator;
import fr.irisa.cairn.gecos.model.extensions.generators.PerfectMatchDispatchStrategy;
import fr.irisa.cairn.gecos.model.factory.GecosUserAnnotationFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.idfix.frontend.utils.Instrument;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.IDFixUtils;
import fr.irisa.cairn.idfix.utils.ListenProcessInputStandardStream;
import fr.irisa.cairn.idfix.utils.TraceManager;
import fr.irisa.cairn.idfix.utils.exceptions.CompilationException;
import fr.irisa.cairn.idfix.utils.exceptions.InterpreterException;
import fr.irisa.cairn.idfix.utils.exceptions.SFGModelCreationException;
import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;
import fr.irisa.cairn.idfix.utils.io.AffectationValueSimulationIO;
import fr.irisa.cairn.idfix.utils.logging.IDFixLogger;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Symbol;
import gecos.instrs.Instruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SizeofTypeInstruction;
import gecos.types.AliasType;
import gecos.types.ArrayType;
import gecos.types.BaseType;
import gecos.types.PtrType;
import gecos.types.Type;

/**
 * Create the file to compile needed to simulate. Compile it, and start the simulation.
 * Step :
 * 	- Instrument the code to get back the information after each affectation and extract delayed variable
 *  - Build file to compile
 *  - Compile
 *  - Simulate
 *  - Save informations
 * @author Nicolas Simon
 * @date Jun 19, 2013
 */
public class ValueAffectationSimulation {
	private PrintStream _out = System.out;
	@SuppressWarnings("unused")
	private PrintStream _err = System.err;
	boolean debug = false;

	private ProcedureSet _ps;
	private ProcedureSet _psInstrumented;

	private String _outputSimuFolder;
	private final int _nbSimus;
	private String _simuValuesFileToCompile;
	private String _probaFileToCompile;
	private String _simulatedValuesFile;
	
	private TraceManager _traceManager;
	private NativeSimulation _nativeSimulation;
	
	// Dummy types and symbols
	private ProcedureSymbol _fopen, _fseek, _rewind, _malloc, _free, _fread, _printf, _ftell, _exit;
	private Symbol _snull, _sseekend;
	private Type _ptrcfile;
	
	private String _lineseparator = System.getProperty("line.separator");
	
	public ValueAffectationSimulation(ProcedureSet procedureSet, int nbSimulationNeeded, String outputDirPath){
		_ps = procedureSet;
		_nbSimus = nbSimulationNeeded;
		_simulatedValuesFile = outputDirPath + ConstantPathAndName.IDFIX_CONV_OUTPUT_FOLDER_NAME + ConstantPathAndName.SIMU_VALUES_FILE_NAME;
		
		
		_outputSimuFolder = outputDirPath + ConstantPathAndName.IDFIX_CONV_OUTPUT_FOLDER_NAME + ConstantPathAndName.SIMU_FOLDER;
		_simuValuesFileToCompile = _outputSimuFolder + ConstantPathAndName.SIMU_FILE_TO_COMPILE_FILE_NAME;
		_probaFileToCompile = _outputSimuFolder + ConstantPathAndName.SIMU_PROBABILITIES_FILE_NAME;
		
		_traceManager = new TraceManagerProbaSequence();
		
		File simulationFolder = new File(_outputSimuFolder);
		if(!simulationFolder.exists() && !simulationFolder.isDirectory()){
			if(!simulationFolder.mkdir()){
				throw new RuntimeException("The creation of the folder " + _outputSimuFolder + "have fail");
			}
		}
	}
		
	private static final void _assert(boolean b, String msg) throws SimulationException 
	{
		if(!b) throw new SimulationException(msg);
	}
	
	public void compute() throws SimulationException, SFGModelCreationException, InterpreterException{
		Instrument instrument = new Instrument(_ps, _outputSimuFolder);
		_psInstrumented = instrument.compute();
		Procedure p = IDFixUtils.getMainProcedure(_psInstrumented);
		
		try{
			buildFileToCompile(p);
			try{
			compileFile(_simuValuesFileToCompile, ConstantPathAndName.SIMU_RUN_LIBRARY_FILE_NAME);
			} catch (CompilationException e){
				if(!(new File(_outputSimuFolder + ConstantPathAndName.SIMU_RUN_LIBRARY_FILE_NAME).isFile()))
					throw new SimulationException(e);
				_out.println(e.getMessage());
			}
			
			_nativeSimulation = new NativeSimulation(_traceManager, _outputSimuFolder + ConstantPathAndName.SIMU_RUN_LIBRARY_FILE_NAME);
			_nativeSimulation.simulate();
			
			if(debug)
				_traceManager.showProbabilities();
			
			buildSimulationValuesFile();
			buildProbabilitiesFile();
			
			try{
				compileFile(_probaFileToCompile, ConstantPathAndName.SIMU_PROBA_LIBRARY_FILE_NAME);
			} catch (CompilationException e){
				if(!(new File(_outputSimuFolder + ConstantPathAndName.SIMU_PROBA_LIBRARY_FILE_NAME).isFile()))
					throw new SimulationException(e);
				_out.println(e.getMessage());
			}
		}catch(IOException e){
			throw new SimulationException(e);
		}catch(InterruptedException e){
			throw new SimulationException(e);
		}
		
	}
	
	/**
	 * Build the file to compile. Copy the callback function, generate the instrumented procedure set and write the main function.
	 * @param p
	 * @throws IOException
	 * @throws SFGModelCreationException
	 * @throws InterpreterException
	 * @throws SimulationException 
	 */
	private void buildFileToCompile(Procedure p) throws IOException, SFGModelCreationException, InterpreterException, SimulationException{
		PrintStream fileToCompile = new PrintStream(new BufferedOutputStream(new FileOutputStream(_simuValuesFileToCompile)));
		
		fileToCompile.println();
		fileToCompile.println("#include <jni.h>");
		fileToCompile.println("#include <math.h>");
		fileToCompile.println("#include <stdlib.h>");
		fileToCompile.println("#include \"runsimulation.h\"");
		fileToCompile.println();
		fileToCompile.println("static JNIEnv * globalEnv;");
		fileToCompile.println("static jobject globalJobj;");
		fileToCompile.println();
		
		File fileTemp = IDFixUtils.copyFileFromResourcesFolderToTemporaryFolder(ConstantPathAndName.SIMU_CALL_BACK_FUNCTIONS_FILE_NAME);
		IDFixUtils.copyFile(new FileInputStream(fileTemp), fileToCompile);
		
		StringBuilder builder = new StringBuilder();
		String lineseparator = System.getProperty("line.separator");
		
		for(Symbol s : p.getScope().getSymbols()){
			if(IDFixUtils.isOutputVar(s))
				writeInitialization(s, builder, false);
			if(IDFixUtils.isInputVar(s))
				writeInitialization(s, builder, true);
		}
	
		builder.append(System.getProperty("line.separator"));
		builder.append("JNIEXPORT void JNICALL Java_fr_irisa_cairn_idfix_frontend_simulation_NativeSimulation_runSimulation(JNIEnv * env, jobject jobj){");
		builder.append(lineseparator).append(lineseparator);
		builder.append("\tglobalEnv = env;");
		builder.append(lineseparator);
		builder.append("\tglobalJobj = jobj;");
		builder.append(lineseparator);
		
		RandomValuesBuilder random = new RandomValuesBuilder(_traceManager);
		for(Symbol s : p.getScope().getSymbols()){
			if(IDFixUtils.isOutputVar(s)){
				writeDeclaration(s, builder);
				builder.append("init_" + s.getName() + "(&" + s.getName() + ");");
				builder.append(lineseparator);
			}
			else if(IDFixUtils.isInputVar(s)){
				writeDeclaration(s, builder);
				String path = random.generate(s, _nbSimus, _outputSimuFolder);
				builder.append("init_" + s.getName() + "(&" + s.getName() + ", \"" + path+ "\");");
				builder.append(lineseparator);
			}
			else
				throw new SFGModelCreationException("");
		}
		writeCallInformation(p, builder);
		builder.append("}");
		builder.append(lineseparator);
		
		for(Symbol s : p.getScope().getSymbols()){
			if(IDFixUtils.isOutputVar(s) || IDFixUtils.isInputVar(s))
				changeArrayTypeToPtrType(s);
		}
		ExtendableCGenerator gecoscoreTemplate = new ExtendableCGenerator(new PerfectMatchDispatchStrategy());
		CharSequence charSequence = gecoscoreTemplate.generate(_psInstrumented);
		fileToCompile.print(charSequence.toString());
		
		
		fileToCompile.print(builder.toString());
		fileToCompile.close();
}
	
	private void changeArrayTypeToPtrType(Symbol s) throws SimulationException{
		TypeAnalyzer analyzer = new TypeAnalyzer(s.getType());
		Type ptrType;
		if(s.getType() instanceof ArrayType){
			ArrayLevel arrLevel = analyzer.tryGetArrayLevel(0);
			_assert(arrLevel != null, "The type of the symbol " + s.getName() + " not correspond to a array type");
			GecosUserTypeFactory.setScope(s.getContainingScope());
			ptrType = GecosUserTypeFactory.PTR(analyzer.getBase(), arrLevel.getNDims());
		}
		else if (analyzer.isBase()){
			ptrType = s.getType();
		}
		else{
			throw new SimulationException("Kind of paramater type not managed yet in the back end simulator : " + analyzer);
		}
		s.setType(ptrType);
//		analyzer.revertAllLevelsOn(ptrType);
	}

	/**
	 * Write into the PrintStream the delay declaration. the variable represent by s must be a delay
	 * @param s
	 * @param fileToCompile
	 * @param regenerator
	 */
	@Deprecated
	@SuppressWarnings("unused")
	private void writeDelayDeclaration(Symbol s, PrintStream fileToCompile){
		if(!(s.getType() instanceof ArrayType))
			throw new RuntimeException("Scalar type for delay variable not manage into the simulation process");
		
		
		fileToCompile.print("\t" + ExtendableTypeCGenerator.eInstance.generate(s.getType()) + " " + s.getName());
		ArrayType tabType = (ArrayType) s.getType();
		long nbElems = tabType.getUpper() - tabType.getLower() + 1;
		String dims = "[" + (tabType.getUpper() - tabType.getLower() + 1) + "]";
		while (tabType.getBase() instanceof ArrayType) {
			tabType = (ArrayType) tabType.getBase();
			nbElems = nbElems * (tabType.getUpper() - tabType.getLower() + 1);
			dims = dims + "[" + (tabType.getUpper() - tabType.getLower() + 1) + "]";
		}
		fileToCompile.print(dims);
		fileToCompile.print(" = { ");
		long elem = 0;
		boolean firstElem = true;
		while (elem < nbElems){
			if (!firstElem){
				fileToCompile.print(", ");
			}
			fileToCompile.print(0);
			firstElem = false;
			elem++;
		}
		fileToCompile.print(" };");
	}	
	
	/**
	 * Write into the PrintStream the output declaration. the variable represent by s must be a delay
	 * @param s
	 * @param fileToCompile
	 * @param regenerator
	 * @throws SimulationException
	 *  
	 */
	@SuppressWarnings("unused")
	@Deprecated
	private void writeOutputDeclaration(Symbol s, PrintStream fileToCompile) throws SimulationException{
		if(s.getType() instanceof ArrayType){
			String type = ExtendableTypeCGenerator.eInstance.generate(s.getType());
			fileToCompile.print("\t" + type + " " + s.getName() + "[" + _nbSimus + "]");
			
			ArrayType arrType = (ArrayType) s.getType();
			String dims = "[" + (arrType.getUpper() - arrType.getLower() + 1) + "]";
			while(arrType.getBase() instanceof ArrayType){
				arrType = (ArrayType) arrType.getBase();
				dims = "[" + (arrType.getUpper() - arrType.getLower() + 1) + "]" + dims;
			}
			fileToCompile.println(dims + ";");		
		}
		else if(s.getType() instanceof BaseType){
			String type = ExtendableTypeCGenerator.eInstance.generate(s.getType());
			fileToCompile.print("\t" + type + " " + s.getName() + "[" + _nbSimus + "];");
		}
		else{
			throw new SimulationException("Gecos type unknown: " + s.getType());
		}
	}
	
	private void writeDeclaration(Symbol s, StringBuilder builder) throws SimulationException{
		TypeAnalyzer typeAnalyzer = new TypeAnalyzer(s.getType());
		String type = ExtendableTypeCGenerator.eInstance.generate(typeAnalyzer.getBase());
		if(typeAnalyzer.isArray()){
			ArrayLevel arrLevel = typeAnalyzer.tryGetArrayLevel(0);
			_assert(arrLevel != null, "The type of the symbol " + s.getName() + " not correspond to a array type");
			builder.append("\t" + type + " *");
			for(int i = 0 ; i < arrLevel.getNDims() ; i++){
				builder.append('*');
			}
			builder.append(s.getName() + ';');		
			builder.append(_lineseparator);
		}
		else if (typeAnalyzer.isBase()){
			builder.append("\t" + type + " *" + s.getName() + ';');
		}
		else{
			throw new SimulationException("Type not managed as input of the simulation sample");
		}
	}
	
	/**
	 * Create a array which contained dimension instructions of the symbol. Dimension instructions represent the array index of the array created in the main function.
	 * So, the number of simulation is the first instruction.
	 * @param s
	 * @return array which contained the dimension instruction of the symbol s
	 * @throws SimulationException 
	 */
	private Instruction[] getDimensions(Symbol s) throws SimulationException{
		TypeAnalyzer analyzer = new TypeAnalyzer(s.getType());
		Instruction[] dimensions;
		
		if(analyzer.isBase() && analyzer.getBaseLevel().isBaseType()){
			dimensions = new Instruction[1];
			dimensions[0] = GecosUserInstructionFactory.Int(_nbSimus);
		}
		else if(analyzer.isArray()){
			ArrayLevel arrLevel = analyzer.tryGetArrayLevel(0);
			_assert(analyzer.isArray() && arrLevel != null, "The type of the symbol " + s.getName() + " not correspond to a array type");
			dimensions = new Instruction[arrLevel.getNDims() + 1];
			dimensions[0] = GecosUserInstructionFactory.Int(_nbSimus);
			for(int i = 0 ; i < arrLevel.getNDims() ; i++){
				long dim = arrLevel.tryGetSize(i);
				_assert(dim != -1, "An array dimension size of a system input/output is not a integer : " + arrLevel.getSize(i));
				dimensions[arrLevel.getNDims() - i] = GecosUserInstructionFactory.Int(dim);
			}
		}
		else{
			throw new SimulationException("Kind of paramater type not managed yet in the back end simulator : " + analyzer);
		}
		
		return dimensions;
	}
	
	private void writeInitialization(Symbol s, StringBuilder builder, boolean isInput) throws SimulationException{
		TypeAnalyzer analyzer = new TypeAnalyzer(s.getType());
		CompositeBlock bodyproc = GecosUserBlockFactory.CompositeBlock();
		GecosUserTypeFactory.setScope(bodyproc.getScope());
		
		BaseLevel baseLevel = analyzer.getBaseLevel();
		_assert(baseLevel.isBaseType(), "Base type of the pointer is not a scalar base type. Impossible to create the input/output initialization procedure");
		BaseType baseType = (BaseType) baseLevel.getBase();
		Type ptrType;
		int ptrSize;
		if(analyzer.isArray()){
			ArrayLevel arrLevel = analyzer.tryGetArrayLevel(0);
			ptrSize = arrLevel.getNDims()+1;
			_assert(arrLevel != null, "The type of the symbol " + s.getName() + " not correspond to a array type");
			ptrType = GecosUserTypeFactory.PTR(baseType, arrLevel.getNDims()+1);
		}
		else if (analyzer.isBase()){
			ptrSize = 1;
			ptrType = GecosUserTypeFactory.PTR(baseType);
		}
		else{
			throw new SimulationException("Kind of paramater type not managed yet in the back end simulator : " + analyzer);
		}
		
		List<ParameterSymbol> params = new ArrayList<>();
		ParameterSymbol paramSym = GecosUserCoreFactory.paramSymbol(s.getName(), GecosUserTypeFactory.PTR(baseType, ptrSize + 1));
		params.add(paramSym);
		ParameterSymbol pathFile = null;
		if(isInput){
			pathFile = GecosUserCoreFactory.paramSymbol("pathFile", GecosUserTypeFactory.PTR(GecosUserTypeFactory.CHAR()));	
			params.add(pathFile);
		}
		Procedure p = GecosUserCoreFactory.proc(null, GecosUserCoreFactory.procSymbol("init_" + s.getName(), GecosUserTypeFactory.VOID(), params), bodyproc);
		addDummyTypeAndSymbol(bodyproc);
		
		// Creation of the loop which allocate memory of the input and output
		Instruction[] dimensions = getDimensions(s);
		CompositeBlock currentCB;
		ForBlock forBlock;
		CompositeBlock cbToAdd = bodyproc;
		LinkedList<Symbol> indexes = new LinkedList<>();
		Symbol index;
		
		Instruction sizeof;
		if(ptrSize == 1){
			sizeof = GecosUserInstructionFactory.sizeOf(baseType);
		}
		else{
			sizeof = GecosUserInstructionFactory.sizeOf(GecosUserTypeFactory.PTR(baseType, ptrSize-1));
		}
		Instruction castmalloc = GecosUserInstructionFactory.cast(ptrType, GecosUserInstructionFactory.call(_malloc,
				GecosUserInstructionFactory.mul(sizeof,dimensions[0].copy())));
		SetInstruction setInst = GecosUserInstructionFactory.set(GecosUserInstructionFactory.indir(GecosUserInstructionFactory.symbref(s)), castmalloc);
		cbToAdd.addChildren(GecosUserBlockFactory.BBlock(setInst));
		for(int i = 0 ; i < dimensions.length - 1 ; i++){
			index = GecosUserCoreFactory.symbol("i", GecosUserTypeFactory.INT(), bodyproc.getScope());
			indexes.add(index);
			bodyproc.getScope().makeUnique(index);
			BasicBlock init = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(index), GecosUserInstructionFactory.Int(0)));
			BasicBlock test = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.lt(GecosUserInstructionFactory.symbref(index), dimensions[i].copy()));
			BasicBlock step = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(index), GecosUserInstructionFactory.add(
								GecosUserInstructionFactory.symbref(index), GecosUserInstructionFactory.Int(1))));
			
			currentCB = GecosUserBlockFactory.CompositeBlock();
			forBlock = GecosUserBlockFactory.For(init, test, step, currentCB);
			
			Type malloctypecast = GecosUserTypeFactory.PTR(baseType, dimensions.length-1-i);
			Type sizeoftypecast;
			if(dimensions.length-1-i > 1)
				 sizeoftypecast = GecosUserTypeFactory.PTR(baseType, dimensions.length-1-i-1);
			else
				sizeoftypecast = GecosUserTypeFactory.PTR(baseType);
			
			castmalloc = GecosUserInstructionFactory.cast(malloctypecast, GecosUserInstructionFactory.call(_malloc,
					GecosUserInstructionFactory.mul(GecosUserInstructionFactory.sizeOf(sizeoftypecast),dimensions[i+1].copy())));
		
			List<Instruction> indexInst = new ArrayList<>(); 
			for(Symbol sym : indexes){
				indexInst.add(GecosUserInstructionFactory.symbref(sym));
			}
			Instruction arrInst = GecosUserInstructionFactory.array(GecosUserInstructionFactory.indir(GecosUserInstructionFactory.symbref(s)), indexInst);
			setInst = GecosUserInstructionFactory.set(arrInst, castmalloc);
			
			currentCB.addChildren(GecosUserBlockFactory.BBlock(setInst));
			cbToAdd.addChildren(forBlock);
			cbToAdd = currentCB;
		}
		
		if(isInput){
			Symbol file = GecosUserCoreFactory.symbol("pfile", _ptrcfile);		
			Symbol fsize = GecosUserCoreFactory.symbol("fsize", GecosUserTypeFactory.LONG());
			Symbol nbElems = GecosUserCoreFactory.symbol("nbElems", GecosUserTypeFactory.LONG());
			Symbol buffer;
			BaseType bufferType;
			
			bufferType = baseType;
			buffer = GecosUserCoreFactory.symbol("buffer", GecosUserTypeFactory.PTR(bufferType));
			
			bodyproc.getScope().getSymbols().add(file);
			bodyproc.getScope().getSymbols().add(fsize);
			bodyproc.getScope().getSymbols().add(nbElems);
			bodyproc.getScope().getSymbols().add(buffer);
			
			// Creation of the instruction needed to init the input array from the binary file
			Instruction callfopen = GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(file),
					GecosUserInstructionFactory.call(_fopen, GecosUserInstructionFactory.symbref(pathFile), GecosUserInstructionFactory.string("rb")));
			bodyproc.addChildren(GecosUserBlockFactory.BBlock(callfopen));
			
			BasicBlock then = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.call(_printf, GecosUserInstructionFactory.string("Impossible to open binary random values file")), GecosUserInstructionFactory.call(_exit ,GecosUserInstructionFactory.Int(1)));
			IfBlock iffopen = GecosUserBlockFactory.IfThen(GecosUserInstructionFactory.eq(GecosUserInstructionFactory.symbref(file), GecosUserInstructionFactory.symbref(_snull)), then);
			bodyproc.addChildren(iffopen);
			
			Instruction callfseek = GecosUserInstructionFactory.call(_fseek, GecosUserInstructionFactory.symbref(file), GecosUserInstructionFactory.Int(0), GecosUserInstructionFactory.symbref(_sseekend));
			Instruction callftell = GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(fsize), GecosUserInstructionFactory.call(_ftell, GecosUserInstructionFactory.symbref(file)));
			Instruction callrewind = GecosUserInstructionFactory.call(_rewind, GecosUserInstructionFactory.symbref(file));
			castmalloc = GecosUserInstructionFactory.cast(buffer.getType(), GecosUserInstructionFactory.call(_malloc, GecosUserInstructionFactory.symbref(fsize)));
			Instruction callmalloc = GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(buffer), castmalloc);
			bodyproc.addChildren(GecosUserBlockFactory.BBlock(callfseek, callftell, callrewind, callmalloc));
			
			then = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.call(_printf, GecosUserInstructionFactory.string("Impossible to allocate memory to store random values")), GecosUserInstructionFactory.call(_exit, GecosUserInstructionFactory.Int(1)));
			IfBlock ifmalloc = GecosUserBlockFactory.IfThen(GecosUserInstructionFactory.eq(GecosUserInstructionFactory.symbref(buffer), GecosUserInstructionFactory.symbref(_snull)), then);
			bodyproc.addChildren(ifmalloc);
			
			Instruction callfread = GecosUserInstructionFactory.call(_fread, GecosUserInstructionFactory.symbref(buffer), GecosUserInstructionFactory.Int(1), GecosUserInstructionFactory.symbref(fsize), GecosUserInstructionFactory.symbref(file));
			SizeofTypeInstruction sizeoftype = GecosUserInstructionFactory.sizeOf(bufferType);
			Instruction setNbElems = GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(nbElems), GecosUserInstructionFactory.div(GecosUserInstructionFactory.symbref(fsize), sizeoftype));
			bodyproc.addChildren(GecosUserBlockFactory.BBlock(callfread, setNbElems));
			
			// Add the test to check if the element contained in the binary file is equal of the number of element contained to the array allocation
			_assert(dimensions.length >= 1, "An input have not index size during the reading of random value. Need to check if the value is a pointer of a base type.");
			Instruction insttemp = dimensions[0].copy();
			for(int i = 1 ; i < dimensions.length ; i++){
				insttemp = GecosUserInstructionFactory.mul(insttemp, dimensions[i].copy());				
			}
			
			Instruction cond = GecosUserInstructionFactory.ne(insttemp, GecosUserInstructionFactory.symbref(nbElems));
			then = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.call(_printf, GecosUserInstructionFactory.string("Number of random values contains in the binary files is different from the input array size")), GecosUserInstructionFactory.call(_exit, GecosUserInstructionFactory.Int(1)));
			IfBlock ifCheckSize = GecosUserBlockFactory.IfThen(cond, then);
			bodyproc.addChildren(ifCheckSize);
		
		
			// Creation of the loop allow to do the initialization of the corresponding array symbol
			BasicBlock blockInit = GecosUserBlockFactory.BBlock();  
			Block currentBlock = blockInit;
			indexes = new LinkedList<>();
			for(int i = dimensions.length - 1 ; i >= 0 ; i--){
				index = GecosUserCoreFactory.symbol("i", GecosUserTypeFactory.INT(), bodyproc.getScope());
				bodyproc.getScope().makeUnique(index);
				indexes.addFirst(index);
				BasicBlock init = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(index), GecosUserInstructionFactory.Int(0)));
				BasicBlock test = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.lt(GecosUserInstructionFactory.symbref(index), dimensions[i].copy()));
				BasicBlock step = GecosUserBlockFactory.BBlock(GecosUserInstructionFactory.set(GecosUserInstructionFactory.symbref(index), GecosUserInstructionFactory.add(
									GecosUserInstructionFactory.symbref(index), GecosUserInstructionFactory.Int(1))));
				
				currentBlock = GecosUserBlockFactory.For(init, test, step, currentBlock);
			}
			bodyproc.addChildren(currentBlock);
	
			// Create the set instruction to add in the body of the last for loop
			List<Instruction> indexInst = new ArrayList<>(); 
			for(Symbol sym : indexes){
				indexInst.add(GecosUserInstructionFactory.symbref(sym));
			}
			Instruction current = GecosUserInstructionFactory.symbref(indexes.get(indexes.size()-1));
			Instruction temp;
			for(int i = 0 ; i < indexes.size() - 1 ; i++){
				temp = GecosUserInstructionFactory.symbref(indexes.get(i));
				for(int j = dimensions.length - 1  ; j >= (i+1) ; j--){
	
					temp = GecosUserInstructionFactory.mul(temp, dimensions[j].copy());
				}
				current = GecosUserInstructionFactory.add(current, temp);
			}
			Instruction arrInstDest = GecosUserInstructionFactory.array(GecosUserInstructionFactory.indir(
					GecosUserInstructionFactory.symbref(s)), indexInst);
			Instruction arrInstSource = GecosUserInstructionFactory.array(buffer, current);
			Instruction init = GecosUserInstructionFactory.set(arrInstDest, arrInstSource);
			blockInit.addInstruction(init);
			
			// need to free the buffer memory allocation
			Instruction callFree = GecosUserInstructionFactory.call(_free, GecosUserInstructionFactory.symbref(buffer));
			BasicBlock freeBlock = GecosUserBlockFactory.BBlock(callFree);
			bodyproc.addChildren(freeBlock);
		}
		
		builder.append(ExtendableCGenerator.eInstance.generate(p));
		builder.append(_lineseparator);
	}
	
	/**
	 * Add all dummy type and symbol in the IR needed to read a file
	 * @param ps
	 */
	private void addDummyTypeAndSymbol(CompositeBlock p){	
		// Type creation
//		Type voidType = GecosUserTypeFactory.VOID(); due to gecos auto instruction type deduction...
		Type voidType = GecosUserTypeFactory.INT();
		Type charType = GecosUserTypeFactory.CHAR();
		Type intType = GecosUserTypeFactory.INT();
		Type longType = GecosUserTypeFactory.LONG();
		Type uintType = GecosUserTypeFactory.UINT();
		AliasType cfile = GecosUserTypeFactory.ALIAS(intType, "FILE");		
		
		// Pointer creation
		PtrType ptrchar = GecosUserTypeFactory.PTR(charType);
		_ptrcfile = GecosUserTypeFactory.PTR(cfile);
		PtrType ptrvoid = GecosUserTypeFactory.PTR(voidType);
		
		// Const creation
		Type constptrtype = ptrchar.copy();
		constptrtype.setConstant(true);
		
		
		// Parameter symbol creation
		ParameterSymbol filename = GecosUserCoreFactory.paramSymbol("filename", constptrtype);
		ParameterSymbol mode = GecosUserCoreFactory.paramSymbol("mode", constptrtype);
		ParameterSymbol stream = GecosUserCoreFactory.paramSymbol("stream", _ptrcfile);
		ParameterSymbol offset = GecosUserCoreFactory.paramSymbol("offset", intType);
		ParameterSymbol origin = GecosUserCoreFactory.paramSymbol("origin", intType);
		ParameterSymbol size = GecosUserCoreFactory.paramSymbol("size", uintType);
		ParameterSymbol count = GecosUserCoreFactory.paramSymbol("count", uintType);
		ParameterSymbol ptr = GecosUserCoreFactory.paramSymbol("ptr", ptrvoid);
		ParameterSymbol msg = GecosUserCoreFactory.paramSymbol("msg", ptrchar);
		ParameterSymbol status = GecosUserCoreFactory.paramSymbol("status", intType);
		
		// Symbol creation
		_snull = GecosUserCoreFactory.symbol("NULL", voidType);
		_sseekend = GecosUserCoreFactory.symbol("SEEK_END", intType);
		p.getScope().getSymbols().add(_snull);
		p.getScope().getSymbols().add(_sseekend);
		
		// Dummy procedure creation
		List<ParameterSymbol> params = new ArrayList<>();
		params.add(filename);params.add(mode);
		_fopen = GecosUserCoreFactory.procSymbol("fopen", _ptrcfile, params);
		p.getScope().getSymbols().add(_fopen);
		
		params.clear();params.add(stream);params.add(offset);params.add(origin);
		_fseek = GecosUserCoreFactory.procSymbol("fseek", intType, params);
		p.getScope().getSymbols().add(_fseek);
		
		params.clear();params.add(stream);
		_ftell = GecosUserCoreFactory.procSymbol("ftell", longType, params);
		p.getScope().getSymbols().add(_ftell);
		
		params.clear(); params.add(stream);
		_rewind = GecosUserCoreFactory.procSymbol("rewind", voidType, params);
		p.getScope().getSymbols().add(_rewind);
		
		params.clear();params.add(size);
		_malloc = GecosUserCoreFactory.procSymbol("malloc", ptrvoid, params);
		p.getScope().getSymbols().add(_malloc);
		
		params.clear();params.add(ptr);
		_free = GecosUserCoreFactory.procSymbol("free", voidType, params);
		p.getScope().getSymbols().add(_free);
		
		params.clear();params.add(ptr);params.add(size);params.add(count);params.add(stream);
		_fread = GecosUserCoreFactory.procSymbol("fread", uintType, params);
		p.getScope().getSymbols().add(_fread);
		
		params.clear();params.add(msg);
		_printf = GecosUserCoreFactory.procSymbol("printf", voidType, params);
		p.getScope().getSymbols().add(_printf);
		
		params.clear();params.add(status);
		_exit = GecosUserCoreFactory.procSymbol("exit", voidType, params);
		p.getScope().getSymbols().add(_exit);
			
		GecosUserAnnotationFactory.pragma(cfile, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(_snull, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(_sseekend, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(_fopen, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(_fseek, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(_ftell, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(_rewind, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(_malloc, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(_free, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(_fread, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(_printf, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
		GecosUserAnnotationFactory.pragma(_exit, GecosUserAnnotationFactory.CODEGEN_IGNORE_ANNOTATION);
	}
	
	/**
	 * Write into the PrintStream the output declaration. the variable represent by s must be a delay
	 * @param s
	 * @param fileToCompile
	 * @param regenerator
	 * @throws SimulationException 
	 */
	@SuppressWarnings("unused")
	@Deprecated
	private void writeInputDeclaration(Symbol s, PrintStream fileToCompile) throws SimulationException{
		if(s.getType() instanceof ArrayType){
			String type = ExtendableTypeCGenerator.eInstance.generate(s.getType());
			fileToCompile.print("\t" + type + " " + s.getName() + "[" + _nbSimus + "]");
			
			ArrayType arrType = (ArrayType) s.getType();
			long nbElems = _nbSimus;
			nbElems *=  arrType.getUpper() - arrType.getLower() + 1;
			String dims = "[" + (arrType.getUpper() - arrType.getLower() + 1) + "]";
			while(arrType.getBase() instanceof ArrayType){
				arrType = (ArrayType) arrType.getBase();
				nbElems *= (arrType.getUpper() - arrType.getLower() + 1);
				
				dims = "[" + (arrType.getUpper() - arrType.getLower() + 1) + "]" + dims;
			}
			BaseType baseType = (BaseType) arrType.getBase();
			fileToCompile.print(dims + " = {");
			
			if(baseType.asFloat() != null){
				float rand;
				float valMin = IDFixUtils.getVarMin(s).floatValue();
				float valMax = IDFixUtils.getVarMax(s).floatValue();
				rand = IDFixUtils.generateRandomFloatValue(valMin, valMax);
				saveTraceInputVariable(s, 0l, (double)rand);
				fileToCompile.print(rand);
				for(long k = 1 ; k < nbElems ; k++){
					rand = IDFixUtils.generateRandomFloatValue(valMin, valMax);
					fileToCompile.print(", " + rand);	
					saveTraceInputVariable(s, k, (double)rand);
				}
			}
			else if(baseType.asInt() != null){
				int rand;
				int valMin = IDFixUtils.getVarMin(s).intValue();
				int valMax = IDFixUtils.getVarMax(s).intValue();
				rand = IDFixUtils.generateRandomIntValue(valMin, valMax);
				saveTraceInputVariable(s, 0l, (double)rand);
				fileToCompile.print(rand);
				for(int k = 1 ; k < nbElems ; k++){
					rand = IDFixUtils.generateRandomIntValue(valMin, valMax);
					fileToCompile.print(", " + rand);
					saveTraceInputVariable(s, k, (double)rand);
				}
			}
			else{
				throw new SimulationException("Gecos base type unknown: " + baseType);
			}
			fileToCompile.println("};");
		}
		else if(s.getType() instanceof BaseType){
			String type = ExtendableTypeCGenerator.eInstance.generate(s.getType());
			fileToCompile.print("\t" + type + " " + s.getName() + "[" + _nbSimus + "] =  {");
			BaseType baseType = (BaseType) s.getType();
			if(baseType.asFloat() != null){
				float rand;
				float valMin = IDFixUtils.getVarMin(s).floatValue();
				float valMax = IDFixUtils.getVarMax(s).floatValue();
				rand = IDFixUtils.generateRandomFloatValue(valMin, valMax);
				traceVariableValue(s, (double) rand, "");
				fileToCompile.print(rand);
				for(int i = 1 ; i < _nbSimus ; i++){ 
					rand = IDFixUtils.generateRandomFloatValue(valMin, valMax);
					fileToCompile.print(", " + rand);
					traceVariableValue(s, (double) rand, "");
				}
			}
			else if(baseType.asInt() != null){
				int rand;
				int valMin = IDFixUtils.getVarMin(s).intValue();
				int valMax = IDFixUtils.getVarMax(s).intValue();
				rand = IDFixUtils.generateRandomIntValue(valMin, valMax);
				traceVariableValue(s, (double) rand, "");
				fileToCompile.print(rand);
				for(int i = 1 ; i < _nbSimus ; i++){
					rand = IDFixUtils.generateRandomIntValue(valMin, valMax);
					fileToCompile.print(", " + rand);
					traceVariableValue(s, (double) rand, "");
				}
			}
			else{
				throw new SimulationException("Gecos base type unknown: " + baseType);
			}
			
			fileToCompile.println("};");
		}
		else{
			throw new SimulationException("Gecos type unknown: " + s.getType());
		}
	}
	
	private void saveTraceInputVariable(Symbol s, long index, double value){
		Vector<Long> indices = IDFixUtils.getArrayIndexes(s.getType(), index);
		String indexes = "";
		for(Long ind : indices){ 
			 indexes = indexes + "[" + ind +"]";
		}
		traceVariableValue(s, value, indexes);
	}
		
	/**
	 * Write into the PrintStream the call loop
	 * @param p
	 * @param builder
	 * @param regenerator
	 */
	private void writeCallInformation(Procedure p, StringBuilder builder){
		builder.append(_lineseparator);
		builder.append("\tint i;");
		builder.append(_lineseparator);
		builder.append("\tfor(i = 0 ; i < " + _nbSimus + " ; i++){");
		builder.append(_lineseparator);
		builder.append("\t\tinitiate_execution();");
		builder.append(_lineseparator);
		builder.append("\t\t" + p.getSymbol().getName() + "(");
		if(p.getScope().getSymbols().size() > 0){
			Symbol s = p.getScope().getSymbols().get(0);
			if(IDFixUtils.isDelayedVar(s))
				builder.append(s.getName());
			else
				builder.append(s.getName() + "[i]");
			
			for(int i = 1 ; i < p.getScope().getSymbols().size() ; i++){
				s = p.getScope().getSymbols().get(i);
				if(IDFixUtils.isDelayedVar(s))
					builder.append(", " + s.getName());
				else
					builder.append(", " + s.getName()+ "[i]");
			}
		}
		builder.append(");");
		builder.append(_lineseparator);
		builder.append("\t\tterminate_execution();");
		builder.append(_lineseparator);
		builder.append("\t}");
		builder.append(_lineseparator);
	}
	
	private void compileFile(String pathFile, String nameLib) throws IOException, InterruptedException, CompilationException{
		IDFixUtils.copyFileFromResourcesFolderToFolder(ConstantPathAndName.SIMU_JNI_HEADER, _outputSimuFolder);
		IDFixUtils.copyFileFromResourcesFolderToFolder(ConstantPathAndName.SIMU_JNI_MD_HEADER, _outputSimuFolder);
		IDFixUtils.copyFileFromResourcesFolderToFolder(ConstantPathAndName.SIMU_SIMULATION_LIBRARY_HEADER, _outputSimuFolder);
		IDFixUtils.copyFileFromResourcesFolderToFolder(ConstantPathAndName.SIMU_PROBA_LIBRARY_HEADER, _outputSimuFolder);
		
		List<String> cmd = new LinkedList<String>();
		cmd.add("gcc");
		cmd.add("-std=c99");
		cmd.add("-g");
		cmd.add("-fPIC");
		cmd.add("-I" + _outputSimuFolder);
		cmd.add("-o");
		cmd.add(_outputSimuFolder + nameLib);
		cmd.add("-shared");
		cmd.add(pathFile);

		ProcessBuilder pb = new ProcessBuilder(cmd);
		Process process = pb.start();

		BufferedReader err = new BufferedReader(new InputStreamReader(process.getErrorStream()));
		ListenProcessInputStandardStream out = new ListenProcessInputStandardStream(process.getInputStream(), IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_FRONTEND) , Level.INFO);
		out.start();
		
		String line;
		StringBuffer lineBuffer = new StringBuffer();
		while((line = err.readLine()) != null)
			lineBuffer.append(line).append("\n");
		
		process.waitFor();
		out.join();
				
		if (lineBuffer.length() > 0) 
			throw new CompilationException(lineBuffer.toString());
	}
	
	private void traceVariableValue(Symbol s, Double value, String indexes){
		Integer numDataCDFG = IDFixUtils.getSymbolAnnotationNumber(s);
		boolean found = false;
		for (VariableValues affect : _traceManager.getValuesList().getVariableList()){
			if (affect.getNumDataCDFG() == numDataCDFG.intValue() && affect.getIndex().equals(indexes)){
				affect.getValues().add(value);
				found = true;
				break;
			}
		}
		if (!found){
			VariableValues varValue = AffectationValueBySimulationFactory.eINSTANCE.createVariableValues();
			varValue.setNumDataCDFG(numDataCDFG);
			varValue.getValues().add(value);
			if (indexes != null){
				varValue.setIndex(indexes);
			}
			else {
				varValue.setIndex("");
			}
			_traceManager.getValuesList().getVariableList().add(varValue);
		}
	}

	private void buildSimulationValuesFile(){
		AffectationValueSimulationIO.saveAffectationList(_simulatedValuesFile, _traceManager.getValuesList());
	}
	
	private void buildProbabilitiesFile() throws IOException{
			PrintStream outFile = new PrintStream(new BufferedOutputStream(new FileOutputStream(_probaFileToCompile))); 
			
			File fileTemp = IDFixUtils.copyFileFromResourcesFolderToTemporaryFolder(ConstantPathAndName.SIMU_PROBA_FUNCTIONS_FILE_NAME);
			IDFixUtils.copyFile(new FileInputStream(fileTemp), outFile);
			outFile.println("int init_automated_generate_sequence(block * block_parcours_1, block * block_parcours_2, int * id){");
			
			_traceManager.printSequences(outFile);
			outFile.println("   return nb_total_times;");
			outFile.println("}");
			outFile.println();
			outFile.close();
	}
}