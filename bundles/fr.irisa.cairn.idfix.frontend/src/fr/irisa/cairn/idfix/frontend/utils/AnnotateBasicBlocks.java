package fr.irisa.cairn.idfix.frontend.utils;

import fr.irisa.cairn.gecos.model.tools.switches.BasicBlockSwitch;
import gecos.annotations.AnnotationsFactory;
import gecos.annotations.StringAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.IfBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;

/**
 * Effectue l'annotation des BasicBlocks.
 * @author qmeunier
 *
 */
public class AnnotateBasicBlocks extends BasicBlockSwitch<Object> {
	private ProcedureSet procedureSet;
	private int currNumBasicBlock;
	
	public AnnotateBasicBlocks(ProcedureSet procedureSet) {
		this.procedureSet = procedureSet;

		currNumBasicBlock = 0;
	}
	

	public void compute() {
		if (procedureSet != null) {
			for (Procedure proc : procedureSet.listProcedures()) {
				doSwitch(proc.getBody());
			}
		}
	}
	
	
	@Override
	public Object caseBasicBlock(BasicBlock b) {
		StringAnnotation numAnnotation = AnnotationsFactory.eINSTANCE.createStringAnnotation();
		numAnnotation.setContent(Integer.toString(currNumBasicBlock));
		b.getAnnotations().put("NUM_BASIC_BLOCK", numAnnotation);
		
		currNumBasicBlock++;
		return null;
	}
	
	@Override
	public Object caseIfBlock (IfBlock b){
		// On ne veut pas visiter le basic block de la condition
		doSwitch(b.getThenBlock());
		if (b.getElseBlock() != null){
			doSwitch(b.getElseBlock());
		}
		return null;
	}
}

