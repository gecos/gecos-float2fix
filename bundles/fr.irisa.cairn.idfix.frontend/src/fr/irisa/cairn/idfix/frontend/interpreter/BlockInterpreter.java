package fr.irisa.cairn.idfix.frontend.interpreter;

import java.util.Map;
import java.util.Stack;

import fr.irisa.cairn.gecos.model.tools.switches.BasicBlockSwitch;
import fr.irisa.cairn.idfix.utils.exceptions.InterpreterException;
import fr.irisa.cairn.idfix.utils.exceptions.SFGGeneratorException;
import fr.irisa.cairn.idfix.utils.exceptions.SFGModelCreationException;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Symbol;
import gecos.instrs.Instruction;

/**
 * Interprets blocks. It keeps a link to an
 * ExecutionContext to be able to push or pop scopes, and a ProcInterpreter to
 * interpret the instructions inside basic blocks.
 * @author Administrateur (interpreter tutorial)
 * Modified by qmeunier
 */
public class BlockInterpreter extends BasicBlockSwitch<Object> {

	private ExecutionContext context;
	private ProcInterpreter modelInterpreter;
	private InstInterpreter instInterpreter;

	/**
	 * Keep a reference to the ProcInterpreter to be able to evaluate
	 * Instructions.
	 */
	public BlockInterpreter(ExecutionContext context, ProcInterpreter modelInterpreter) {
		this.context = context;
		this.modelInterpreter = modelInterpreter;
	}

	public void setInstInterpreter(InstInterpreter instInterpreter){
		this.instInterpreter = instInterpreter;
	}
	
	/**
	 * In order to correctly interpret a basic block, all the instructions have
	 * to be interpreted, and the result of a basic block is the result of the
	 * last interpreted instruction (Conditions block are basic blocks, and have
	 * to return a value).
	 * 
	 * @throws ReturnFoundMessage
	 */
	@Override
	public Object caseBasicBlock(BasicBlock b) {
		Object res = null;
		for (Instruction i : b.getInstructions()) {
			//System.out.println("Processing inst " + i);
			res = modelInterpreter.doSwitch(i);
			if (res != null){
				return res;
			}
		}
		return null;
	}

	/**
	 * When visiting a composite block, there is a new scope to manage. After
	 * adding declarations in the context, the interpretation consists in
	 * visiting each block of the composite block.
	 */
	@Override
	public Object caseCompositeBlock(CompositeBlock b) {
		Object res;
		try {
			context.push(b.getScope());
		} catch (InterpreterException e) {
			throw new RuntimeException(e);
		}
		for (Block block : b.getChildren()) {
			res = doSwitch(block);
			if (res != null){
				return res;
			}
		}
		context.pop();
		return null;
	}

	/**
	 * For
	 */
	@Override
	public Object caseForBlock(ForBlock b) {
		Object res;
		doSwitch(b.getInitBlock());
		boolean taken = (Boolean) doSwitch(b.getTestBlock());
		while (taken) {
			res = doSwitch(b.getBodyBlock());
			if (res != null){
				return res;
			}
			doSwitch(b.getStepBlock());
			taken = (Boolean) doSwitch(b.getTestBlock());
		}
		return null;
	}

	/**
	 * If
	 */
	@Override
	public Object caseIfBlock(IfBlock b) {
		Object res;
		Object condRes = doSwitch(b.getTestBlock());
		if (condRes instanceof UninitializedValue){
			boolean saveCreateNodeOnUpdate = instInterpreter.getCreateNewNodeOnUpdate();
			instInterpreter.setCreateNewNodeOnUpdate(true);
			try{
				if (b.getElseBlock() != null){
					Stack<Map<Symbol, SymbolValue>> oldNodesAndValues = context.cloneCurrNodesAndValues();
					res = doSwitch(b.getThenBlock());
					Stack<Map<Symbol, SymbolValue>> thenNodesAndValues = context.cloneCurrNodesAndValues();
					Stack<Map<Symbol, SymbolValue>> elseNodesAndValues = oldNodesAndValues;
					context.setCurrNodesAndValues(elseNodesAndValues);
					oldNodesAndValues = context.cloneCurrNodesAndValues();
					res = doSwitch(b.getElseBlock());
					elseNodesAndValues = context.cloneCurrNodesAndValues();
					context.setCurrNodesAndValues(oldNodesAndValues);
					context.mergeNodesAndValues(oldNodesAndValues, thenNodesAndValues, elseNodesAndValues);
				}
				else {
					Stack<Map<Symbol, SymbolValue>> oldNodesAndValues =  context.cloneCurrNodesAndValues();
					res = doSwitch(b.getThenBlock());
					Stack<Map<Symbol, SymbolValue>> thenNodesAndValues = context.cloneCurrNodesAndValues();
					context.setCurrNodesAndValues(oldNodesAndValues);
					oldNodesAndValues = context.cloneCurrNodesAndValues();
					context.mergeNodesAndValues(oldNodesAndValues, thenNodesAndValues, null);
				}
			}catch (InterpreterException e){
				throw new RuntimeException(e);
			} catch (SFGGeneratorException e) {
				throw new RuntimeException(e);
			} catch (SFGModelCreationException e) {
				throw new RuntimeException(e);
			}
			instInterpreter.setCreateNewNodeOnUpdate(saveCreateNodeOnUpdate);
		}
		else {
			boolean taken = (Boolean) condRes;
			if (taken) {
				res = doSwitch(b.getThenBlock());
			}
			else {
				res = doSwitch(b.getElseBlock());
			}
		}
		return res;
	}

	/**
	 * Loop
	 */
	@Override
	public Object caseDoWhileBlock(DoWhileBlock b) {
		Object res;
		boolean taken;
		do {
			res = doSwitch(b.getBodyBlock());
			if (res != null){
				return res;
			}
			taken = (Boolean) doSwitch(b.getTestBlock());
		} while (taken);
		return null;
	}

	/**
	 * While
	 */
	@Override
	public Object caseWhileBlock(WhileBlock b) {
		Object res;
		boolean taken = (Boolean) doSwitch(b.getTestBlock());
		while (taken) {
			res = doSwitch(b.getBodyBlock());
			if (res != null){
				return res;
			}
			taken = (Boolean) doSwitch(b.getTestBlock());
		}
		return null;
	}
}
