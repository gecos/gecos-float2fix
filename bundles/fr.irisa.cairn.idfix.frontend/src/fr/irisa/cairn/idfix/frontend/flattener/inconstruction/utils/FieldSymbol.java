package fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils;

import gecos.core.Symbol;
import gecos.types.Field;

public class FieldSymbol {
	private Symbol _symbol;
	private Field _field;

	public FieldSymbol(Symbol symbol, Field field){
		_symbol = symbol;
		_field = field;
	}
	
	public Symbol getSymbol(){
		return _symbol;
	}

	public Field getField(){
		return _field;
	}
	
	public String toString(){
		return _symbol.getName() + ":" + _symbol.getType();
	}
}
