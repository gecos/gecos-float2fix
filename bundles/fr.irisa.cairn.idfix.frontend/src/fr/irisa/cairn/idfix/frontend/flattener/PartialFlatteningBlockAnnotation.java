package fr.irisa.cairn.idfix.frontend.flattener;


import fr.irisa.cairn.gecos.model.tools.switches.BasicBlockSwitch;
import fr.irisa.cairn.gecos.model.tools.switches.BlockInstructionSwitch;
import gecos.annotations.AnnotationsFactory;
import gecos.annotations.StringAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.ConvertInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.RetInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;


public class PartialFlatteningBlockAnnotation extends BasicBlockSwitch<Object> {

	private ProcedureSet ps;
	
	public PartialFlatteningBlockAnnotation(ProcedureSet ps){
		this.ps = ps;
	}
	
	public void compute(){
		for (Procedure proc : ps.listProcedures()){
			doSwitch(proc.getBody());
		}
	}
	
	
	private void createForceFlatteningAnnotation(Block b){
		StringAnnotation flattenAnnotation = AnnotationsFactory.eINSTANCE.createStringAnnotation();
		flattenAnnotation.setContent("YES");
		b.getAnnotations().put("FORCE_FLATTEN", flattenAnnotation);
	}
	
	public static boolean forceFlattening(Block b){
		StringAnnotation flattenAnnotation = (StringAnnotation) b.getAnnotation("FORCE_FLATTEN");
		if (flattenAnnotation == null){
			return false;
		}
		else {
			return (flattenAnnotation.getContent().equals("YES"));
		}
	}
	

	@Override
	public Object caseCompositeBlock(CompositeBlock b) {
		Boolean flatten = false;
		for (Block block  : b.getChildren()) {
			flatten = flatten || (Boolean) doSwitch(block);
		}
		return flatten;
	}

	@Override
	public Object caseIfBlock(IfBlock b) {
		Boolean flatten = false;
		if (b.getThenBlock() != null){
			flatten = flatten || (Boolean) doSwitch(b.getThenBlock());
		}
		if (b.getElseBlock() != null){
			flatten = flatten || (Boolean) doSwitch(b.getElseBlock());
		}
		
		if (flatten){
			createForceFlatteningAnnotation(b);
		}
		return flatten;
	}

	@Override
	public Object caseDoWhileBlock(DoWhileBlock b) {
		doSwitch(b.getTestBlock());
		if (b.getBodyBlock() != null){
			if ((Boolean) doSwitch(b.getBodyBlock())){
				createForceFlatteningAnnotation(b);
			}
		}
		return false;
	}
	
	@Override
	public Object caseWhileBlock(WhileBlock b) {
		doSwitch(b.getTestBlock());
		if ((Boolean) doSwitch(b.getBodyBlock())){
			createForceFlatteningAnnotation(b);
		}
		return false;
	}
	
	@Override
	public Object caseForBlock(ForBlock b) {
		doSwitch(b.getTestBlock());
		Boolean flatten = (Boolean) doSwitch(b.getBodyBlock());
		doSwitch(b.getStepBlock());
		doSwitch(b.getInitBlock());
		if (flatten){
			createForceFlatteningAnnotation(b);
		}
		return false;
	}
	
	@Override
	public Object caseBasicBlock(BasicBlock b) {
		Boolean flatten = false;
		for (Instruction inst : b.getInstructions()){
			PartialFlatteningInstructionVisitor visitor = new PartialFlatteningInstructionVisitor();
			Boolean isCall = (Boolean) visitor.doSwitch(inst);
			if (isCall){
				flatten = true;
				break;
			}
		}
		if (flatten){
			createForceFlatteningAnnotation(b);
		}
		return flatten;
	}
	
	
	
	
	
	private class PartialFlatteningInstructionVisitor extends BlockInstructionSwitch<Object> {
		
		@Override
		public Object caseIntInstruction(IntInstruction inst){
			return false;
		}
		
		@Override
		public Object caseFloatInstruction(FloatInstruction inst){
			return false;
		}
		
		@Override
		public Object caseSetInstruction(SetInstruction inst){
			return (Boolean) doSwitch(inst.getSource());
		}
		
		@Override
		public Object caseCallInstruction(CallInstruction inst){
			return true;
		}
		
		@Override
		public Object caseSymbolInstruction(SymbolInstruction inst){
			return false;
		}
	
		@Override
		public Object caseGenericInstruction(GenericInstruction inst){
			Boolean isCall = false;
			for (Instruction i : inst.getChildren()){
				isCall = isCall || (Boolean) doSwitch(i);
			}
			return isCall;
		}
		
		@Override
		public Object caseArrayInstruction(ArrayInstruction inst){
			return false;
		}
		
		@Override
		public Object caseRetInstruction(RetInstruction inst){
			if (inst.getExpr() != null){
				return doSwitch(inst.getExpr());
			}
			else {
				return false;
			}
		}
		
		@Override
		public Object caseConvertInstruction(ConvertInstruction inst){
			return doSwitch(inst.getExpr());
		}	
		
	}
	
}
