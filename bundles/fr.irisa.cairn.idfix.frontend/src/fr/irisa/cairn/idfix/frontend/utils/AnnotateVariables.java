package fr.irisa.cairn.idfix.frontend.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.tools.switches.BasicBlockSwitch;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.exceptions.SFGModelCreationException;
import gecos.annotations.AnnotationsFactory;
import gecos.annotations.PragmaAnnotation;
import gecos.annotations.StringAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.CompositeBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.types.ArrayType;
import gecos.types.Field;
import gecos.types.RecordType;
import gecos.types.Type;


/**
 * @author qmeunier
 * Annotates all variables in the CDFG with a unique number
 * This class also provides some tools relative to variables treatment:
 * - it makes the conversion between a variable number and its symbol
 * - it provides functions to know the properties of a variable (pragma annotations)
 */
public class AnnotateVariables extends BasicBlockSwitch<Object> {
	private static final boolean SET_ALL_DATA_TO_MAX_WIDTH = false;
	private ProcedureSet procedureSet;
	private Integer currNumData;

	public AnnotateVariables(ProcedureSet procedureSet) {
		this.procedureSet = procedureSet;
		currNumData = 0;
	}
	
	private static void myassert(boolean b, String s) throws SFGModelCreationException{
		if (!b){
			throw new SFGModelCreationException(s);
		}
	}

	public void compute() throws SFGModelCreationException {		
		if (procedureSet != null) {
			Scope scope = procedureSet.getScope();
			if(scope != null)
				for (Symbol symb : scope.getSymbols()){
					if(symb instanceof ProcedureSymbol) continue;
					try {
						updateVariableAnnotation(symb);
					} catch (SFGModelCreationException e) {
						throw new RuntimeException(e);
					}
				}
			
			for (Procedure proc : procedureSet.listProcedures())
				visitProc(proc);
		}
	}
	
	public Object visitProc(Procedure p) throws SFGModelCreationException {
		updateVariableAnnotation(p.getSymbol());
		
		Scope scope = p.getScope();
		if(scope != null)
			processScope(scope);
		
		return doSwitch(p.getBody());
	}
	
	
	@Override
	public Object caseCompositeBlock(CompositeBlock b){
		Scope scope = b.getScope();
		if(scope != null)
			processScope(scope);
		return super.caseCompositeBlock(b);
	}
	
	public void processScope(Scope scope) {
		for (Symbol symb : scope.getSymbols()){
			try {
				updateVariableAnnotation(symb);
			} catch (SFGModelCreationException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	
	@Override
	public Object caseBasicBlock (BasicBlock b){
		return null;
	}
	
	private static void copyStringAnnotation(Symbol paramDest, Symbol paramSource, String annotation){
		StringAnnotation strAnnotSource = (StringAnnotation) paramSource.getAnnotation(annotation);
		StringAnnotation strAnnotDest = (StringAnnotation) paramDest.getAnnotation(annotation);
		if (strAnnotSource == null){
			if (strAnnotDest != null){
				paramDest.getAnnotations().removeKey(annotation);
			}
		}
		else {
			if (strAnnotDest == null){
				// On crée l'annotation
				strAnnotDest = AnnotationsFactory.eINSTANCE.createStringAnnotation();
				strAnnotDest.setContent(strAnnotSource.getContent());
				paramDest.setAnnotation(annotation, strAnnotDest);
			}
			else {
				strAnnotDest.setContent(strAnnotSource.getContent());
			}
		}
	}
	

	/**
	 * Copie les annotations d'un paramètre de type tableau (effectif) vers le paramètre théorique.
	 * Supprime l'annotation correspondante si cette dernière n'existe pas
	 * @param paramTheo
	 * @param paramEffectif
	 */
	public static void updateParamTableauAnnotations (Symbol paramTheo, Symbol paramEffectif){
		copyStringAnnotation(paramTheo, paramEffectif, ConstantPathAndName.ANNOTATION_MIN_RANGE_VARIABLE);
		copyStringAnnotation(paramTheo, paramEffectif, ConstantPathAndName.ANNOTATION_MAX_RANGE_VARIABLE);
		copyStringAnnotation(paramTheo, paramEffectif, ConstantPathAndName.ANNOTATION_OUTPUT_VARIABLE);
		copyStringAnnotation(paramTheo, paramEffectif, ConstantPathAndName.ANNOTATION_WIDTH_VARIABLE);
		copyStringAnnotation(paramTheo, paramEffectif, ConstantPathAndName.ANNOTATION_DELAY_VARIABLE);
		copyStringAnnotation(paramTheo, paramEffectif, ConstantPathAndName.ANNOTATION_DYNAMIC_PROCESSING);
		copyStringAnnotation(paramTheo, paramEffectif, ConstantPathAndName.ANNOTATION_OVERFLOW_PROBABILITY);
	}
	
	private void updateVariableAnnotation(Symbol symb) throws SFGModelCreationException {
		if (symb.getAnnotation(ConstantPathAndName.ANNOTATION_CDFG_DATA) != null)
			myassert(false, "variable " + symb.getName() + " déjà annotée");

		Type type;
		if(symb.getType().isArray()){
			ArrayType arrType = (ArrayType) symb.getType();
			type = arrType.getInnermostBase();
		}
		else{
			type = symb.getType();
		}
		
		if(type.asRecord() != null) {
			StringAnnotation stringAnnot = AnnotationsFactory.eINSTANCE.createStringAnnotation();
			RecordType recType = (RecordType) type;
			String content = "";
			EList<Field> fields = recType.getFields();
			int i;
			for(i = 0 ; i < fields.size() - 1 ; i++){
				content += fields.get(i).getName() + ":" + currNumData + "/";
				currNumData++;
			}
			content += fields.get(i).getName() + ":" + currNumData;
			currNumData++;
			stringAnnot.setContent(content);
			symb.setAnnotation(ConstantPathAndName.ANNOTATION_CDFG_DATA, stringAnnot);
			
			updatePragmaAnnotation(symb);
		}
		else {
			StringAnnotation stringAnnot = AnnotationsFactory.eINSTANCE.createStringAnnotation();
			String content = Integer.toString(currNumData);
			stringAnnot.setContent(content);
			symb.setAnnotation(ConstantPathAndName.ANNOTATION_CDFG_DATA, stringAnnot);
			currNumData++;
			
			updatePragmaAnnotation(symb);
		}
	}
			
			
	public static void updatePragmaAnnotation(Symbol symb){
		/* On remplace les annotations de pragma par des annotations plus simples :
		 * pragma OUTPUT -> setAnnotation("OUTPUT","yes")
		 * pragma WIDTH [20] -> setAnnotation("WIDTH","20")
		 * pragma DYNAMIC [-1;1] -> setAnnotation("VAL_MIN","-1") et setAnnotation("VAL_MAX","1")
		 * pragma DELAY -> setAnnotation("DELAY","yes")
		 * Cela facilite grandement les accès à ces attributs et accélère le temps d'exécution
		 */
		PragmaAnnotation pragmaAnnotation = symb.getPragma();
		if (pragmaAnnotation != null) {
			StringBuilder sBuilder = new StringBuilder();
			for (String str : ((PragmaAnnotation) pragmaAnnotation).getContent()) {
				sBuilder.append(str);
			}
			
			/* MAIN_FUNC (pour les fonctions) */
			Pattern pattern = Pattern.compile("\\.*MAIN_FUNC\\.*");
			Matcher matcher = pattern.matcher(sBuilder);
			if (matcher.find()) {
				StringAnnotation mainFuncAnnotation = AnnotationsFactory.eINSTANCE.createStringAnnotation();
				mainFuncAnnotation.setContent("yes");
				symb.setAnnotation(ConstantPathAndName.ANNOTATION_MAIN_FUNC, mainFuncAnnotation);
			}
			
			/* DYNAMIC */
			Pattern pattern1 = Pattern.compile("\\.*DYNAMIC\\s*\\[\\s*(-?\\d+\\.?\\d*)\\s*[,;]\\s*(-?\\d+\\.?\\d*)\\s*\\]\\.*");
			Matcher matcher1 = pattern1.matcher(sBuilder);
			if (matcher1.find()) {
				Double lowerBound = Double.valueOf(matcher1.group(1));
				Double upperBound = Double.valueOf(matcher1.group(2));

				StringAnnotation valMinAnnotation = AnnotationsFactory.eINSTANCE.createStringAnnotation();
				valMinAnnotation.setContent(Double.toString(lowerBound));
				symb.setAnnotation(ConstantPathAndName.ANNOTATION_MIN_RANGE_VARIABLE, valMinAnnotation);
				
				StringAnnotation valMaxAnnotation = AnnotationsFactory.eINSTANCE.createStringAnnotation();
				valMaxAnnotation.setContent(Double.toString(upperBound));
				symb.setAnnotation(ConstantPathAndName.ANNOTATION_MAX_RANGE_VARIABLE, valMaxAnnotation);
			}
			
			/* OUTPUT */
			Pattern pattern2 = Pattern.compile("\\.*OUTPUT\\.*");
			Matcher matcher2 = pattern2.matcher(sBuilder);
			if (matcher2.find()) {
				StringAnnotation outputAnnotation = AnnotationsFactory.eINSTANCE.createStringAnnotation();
				outputAnnotation.setContent("yes");
				symb.setAnnotation(ConstantPathAndName.ANNOTATION_OUTPUT_VARIABLE, outputAnnotation);
			}
			
			/* WIDTH */
			Pattern pattern3 = Pattern.compile("\\.*WIDTH\\s*\\[\\s*(-?\\d+\\.?\\d*)\\s*\\]\\.*");
			Matcher matcher3 = pattern3.matcher(sBuilder);
			if (matcher3.find()) {
				StringAnnotation widthAnnotation = AnnotationsFactory.eINSTANCE.createStringAnnotation();
				widthAnnotation.setContent(Integer.toString(Integer.valueOf(matcher3.group(1))));
				symb.setAnnotation(ConstantPathAndName.ANNOTATION_WIDTH_VARIABLE, widthAnnotation);
			}
			else if(SET_ALL_DATA_TO_MAX_WIDTH){
				// force WIDTH annotation to max width if pragma WIDTH was not specified.
				// This forces noise power function generation to dynamically consider Data width.
				StringAnnotation widthAnnotation = AnnotationsFactory.eINSTANCE.createStringAnnotation();
				widthAnnotation.setContent(Integer.toString(ConstantPathAndName.MAX_DATA_BITWIDTH));
				symb.setAnnotation(ConstantPathAndName.ANNOTATION_WIDTH_VARIABLE, widthAnnotation);
			}
			
			/* DELAY */
			Pattern pattern4 = Pattern.compile("\\.*DELAY\\.*");
			Matcher matcher4 = pattern4.matcher(sBuilder);
			if (matcher4.find()) {
				StringAnnotation delayAnnotation = AnnotationsFactory.eINSTANCE.createStringAnnotation();
				delayAnnotation.setContent("yes");
				symb.setAnnotation(ConstantPathAndName.ANNOTATION_DELAY_VARIABLE, delayAnnotation);
			}
			
			/* OVERFLOW_DYNAMIC_PROCESSING */
			Pattern pattern5 = Pattern.compile("\\.*OVERFLOW_DYNAMIC_PROCESSING\\s*\\[\\s*([-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?)\\s*\\]\\.*");
			Matcher matcher5 = pattern5.matcher(sBuilder);
			if (matcher5.find()) {
				StringAnnotation dynProcessAnnotation = AnnotationsFactory.eINSTANCE.createStringAnnotation();
				dynProcessAnnotation.setContent("OVERFLOW");
				symb.setAnnotation(ConstantPathAndName.ANNOTATION_DYNAMIC_PROCESSING, dynProcessAnnotation);
				
				StringAnnotation probaAnnotation = AnnotationsFactory.eINSTANCE.createStringAnnotation();
				probaAnnotation.setContent(Double.toString(Double.valueOf(matcher5.group(1))));
				symb.setAnnotation(ConstantPathAndName.ANNOTATION_OVERFLOW_PROBABILITY, probaAnnotation);
			}
			
			/*  */
			Pattern pattern6 = Pattern.compile("DELAY\\s([a-zA-Z]\\w*)\\-\\>([a-zA-Z]\\w*)$");
//				Pattern pattern6 = Pattern.compile("DELAY [a-zA-Z]\\w*\\.*");
			Matcher matcher6 = pattern6.matcher(sBuilder);
			if (matcher6.find()) {
				StringAnnotation delayAnnotation = AnnotationsFactory.eINSTANCE.createStringAnnotation();
				delayAnnotation.setContent(matcher6.group(1));
				symb.setAnnotation(ConstantPathAndName.ANNOTATION_DELAY_VARIABLE, delayAnnotation);
			}
			
			/*  Pragma CONST to detect which variable is a const (needed to make easier the simulation to verify the result of analytic noise function */
			Pattern pattern7 = Pattern.compile("CONST");
			Matcher matcher7 = pattern7.matcher(sBuilder);
			if (matcher7.find()) {
				StringAnnotation constAnnotation = AnnotationsFactory.eINSTANCE.createStringAnnotation();
				constAnnotation.setContent("yes");
				symb.setAnnotation(ConstantPathAndName.ANNOTATION_CONST_VARIABLE, constAnnotation);
			}
		}
		else if(SET_ALL_DATA_TO_MAX_WIDTH){
			// force WIDTH annotation to max width if pragma WIDTH was not specified.
			// This forces noise power function generation to dynamically consider Data width.
			StringAnnotation widthAnnotation = AnnotationsFactory.eINSTANCE.createStringAnnotation();
			widthAnnotation.setContent(Integer.toString(ConstantPathAndName.MAX_DATA_BITWIDTH));
			symb.setAnnotation(ConstantPathAndName.ANNOTATION_WIDTH_VARIABLE, widthAnnotation);
		}
	}
}
