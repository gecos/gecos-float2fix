package fr.irisa.cairn.idfix.frontend.simulation;

import java.util.List;

class Sequence {

	List<Integer> blockSequence;
	int nbCalls; // Nombre de fois où cette séquence à été obtenue après simulation
	Double proba;
	
	public Sequence(List<Integer> s){
		blockSequence = s;
	}
	
	public void incrementUse(){
		nbCalls++;
	}
	
	public Integer getNbCalls(){
		return nbCalls;
	}
	
	public List<Integer> getBlockSequence(){
		return blockSequence;
	}
}
