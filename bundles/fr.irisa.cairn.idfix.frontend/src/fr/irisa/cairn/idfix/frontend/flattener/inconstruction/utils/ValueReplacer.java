package fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.tools.switches.DefaultInstructionSwitch;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.GenericInstInterpreter;
import fr.irisa.cairn.idfix.utils.exceptions.FlattenerException;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.CallInstruction;
import gecos.instrs.ComplexInstruction;
import gecos.instrs.FieldInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;


/**
 * Replaces known variables by their values in an instruction copied to the flattened source.
 * @author Nicolas Simon
 * @date Jul 3, 2013
 */
public class ValueReplacer extends DefaultInstructionSwitch<Object> {
	private enum KIND {RHS, LHS};
	private KIND _kind;
	
	private Context _context;
	private GenericInstInterpreter _ginst;
	
	public ValueReplacer(Context context){
		_context = context;
		_ginst = new GenericInstInterpreter(this);
		_kind = KIND.RHS;
	}
	
	private void myassert(boolean b, String message) throws FlattenerException{
		if(!b)
			throw new FlattenerException(message);
	}
	
	@Override
	public Object caseSetInstruction(SetInstruction set){
		_kind = KIND.LHS;
		doSwitch(set.getDest());
		_kind = KIND.RHS;
		doSwitch(set.getSource());
		return null;
	}
	
	@Override
	public Object caseSymbolInstruction(SymbolInstruction s){
		try{
			SymbolValue res = null;
			Instruction inst = null;
			if(s.getType().asBase() != null &&_kind == KIND.RHS){
				myassert(_context.getValue(s.getSymbol()) instanceof SymbolValue, "Error: A scalar variable must be used SymbolValue type");
				res = (SymbolValue) _context.getValue(s.getSymbol());
				if(res.getValue() instanceof Long){
					inst = GecosUserInstructionFactory.Int((Long)res.getValue());
				} else if(res.getValue() instanceof Double){
					inst = GecosUserInstructionFactory.Float((Double) res.getValue());
				} else if(!(res.getValue() instanceof UninitializedValue)) {
					throw new FlattenerException("An value have a invalid value: " + res);
				}
				
				// FIXME Bug si activé tout seul (sans la partie genericInstruction)
				if(inst != null){
					ComplexInstruction parent = s.getParent();
//					int i;
//					for(i = 0 ; i < parent.getChildrenCount() ; i++){
//						if(parent.getChildAt(i).equals(s))
//							break;
//					}
//					parent.getChildren().set(i, inst);
					parent.replaceChild(s, inst);
				}
			}
			
			return res;
		} catch (FlattenerException e){
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Object caseFieldInstruction(FieldInstruction f){
		try{
			ISymbolType res = null;
			if(_kind == KIND.RHS){
				if(f.getExpr() instanceof SymbolInstruction){
					SymbolInstruction symInst = (SymbolInstruction) f.getExpr();
					res = _context.getValue(symInst.getSymbol(), f.getField());
				} else if(f.getExpr() instanceof ArrayInstruction){
					ArrayInstruction arrInst = (ArrayInstruction) f.getExpr();
					List<Long> indices = new ArrayList<>();
					Object o;
					for(Instruction index : arrInst.getIndex()){
						o = doSwitch(index);
						myassert(o instanceof SymbolValue && ((SymbolValue) o).getValue() instanceof Long, "Array index is not a integer (" + o + " : " + o.getClass() + ")");						
						indices.add((Long) ((SymbolValue) o).getValue());
					}
					res = _context.getValue(((SymbolInstruction)arrInst.getDest()).getSymbol(), indices, f.getField());
				} else {
					throw new FlattenerException("Kind of struture not managed yet");
				}
				
				//myassert(res instanceof SymbolValue, "Management of complex structure are not implemented yet");
				
				if(res instanceof SymbolValue){
					SymbolValue symV = (SymbolValue) res;
					Instruction inst = null;
					if(symV.getValue() instanceof Long){
						inst = GecosUserInstructionFactory.Int((Long)symV.getValue());
					} else if(symV.getValue() instanceof Double){
						inst = GecosUserInstructionFactory.Float((Double) symV.getValue());
					} else if(!(symV.getValue() instanceof UninitializedValue)) {
						throw new FlattenerException("An value have a invalid value: " + symV);
					}
					
					if(inst != null){
						ComplexInstruction parent = f.getParent();
//						int i;
//						for(i = 0 ; i < parent.getChildrenCount() ; i++){
//							if(parent.getChildAt(i).equals(f))
//								break;
//						}
//						parent.getChildren().set(i, inst);
						parent.replaceChild(f, inst);
					}
				}
			}
			
			return res;
		} catch(FlattenerException e){
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Object caseArrayInstruction(ArrayInstruction i){
		try{
			ISymbolType res = null;
			if(_kind == KIND.LHS){
				_kind = KIND.RHS;
				for(Instruction inst : i.getIndex()){
					doSwitch(inst);
				}
				_kind = KIND.LHS;
			} else {
				List<Long> indices = new ArrayList<>();
				Object o;
				for(Instruction index : i.getIndex()){
					o = doSwitch(index);
					myassert(o instanceof SymbolValue && ((SymbolValue) o).getValue() instanceof Long, "Array index is not a integer (" + o + " : " + o.getClass() + ")");
					indices.add((Long) ((SymbolValue) o).getValue());
				}
				
				 res = _context.getValue(((SymbolInstruction) i.getDest()).getSymbol(), indices);
				
				 if(res instanceof SymbolValue){
					SymbolValue symV = (SymbolValue) res;
					Instruction inst = null;
					if(symV.getValue() instanceof Long){
						inst = GecosUserInstructionFactory.Int((Long)symV.getValue());
					} else if(symV.getValue() instanceof Double){
						inst = GecosUserInstructionFactory.Float((Double) symV.getValue());
					} else if(!(symV.getValue() instanceof UninitializedValue)) {
						throw new FlattenerException("An value have a invalid value: " + res);
					}
					
					if(inst != null){
						ComplexInstruction parent = i.getParent();
//						int j;
//						for(j = 0 ; j < parent.getChildrenCount() ; j++){
//							if(parent.getChildAt(j).equals(i))
//								break;
//						}
//						parent.getChildren().set(j, inst);
						parent.replaceChild(i, inst);
					}
				}
			}
			
			return res;
		} catch(FlattenerException e){
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Object caseGenericInstruction(GenericInstruction g){
		Object res = _ginst.doSwitch(g);
		Instruction inst = null;
		if(res instanceof Long){
			inst = GecosUserInstructionFactory.Int((Long) res);
		} else if(res instanceof Double){
			inst = GecosUserInstructionFactory.Float((Double) res);
		} else if(res instanceof Boolean){
			if((Boolean) res)
				inst = GecosUserInstructionFactory.Int(1);
			else
				inst = GecosUserInstructionFactory.Int(0);
		} else if(!(res instanceof UninitializedValue)){
			throw new RuntimeException("An value have a invalid value: " + res);
		}
		
		if (inst != null){
			ComplexInstruction parent = g.getParent();
//			int i = 0;
//			for (Instruction child : parent.getChildren()){
//				if (child == g){
//					break;
//				}
//				i++;
//			}
//			parent.getChildren().set(i, inst);
			parent.replaceChild(g, inst);
		}
		
		return new SymbolValue(res);
	}
	
	@Override
	public Object caseIntInstruction(IntInstruction g) {
		return new SymbolValue( new Long(g.getValue()));
	}
	
	@Override
	public Object caseFloatInstruction(FloatInstruction g) {
		return  new SymbolValue( new Double(g.getValue()));
	}
	
	@Override
	public Object caseCallInstruction(CallInstruction g){
		if(_context.getLastFunctionReturnValue() != null){
			Instruction inst = null;
			ISymbolType res = _context.getLastFunctionReturnValue();
			if(res instanceof SymbolValue){
				SymbolValue symV = (SymbolValue) res;
				if(symV.getValue() instanceof Long){
					inst = GecosUserInstructionFactory.Int((Long)symV.getValue());
				} else if(symV.getValue() instanceof Double){
					inst = GecosUserInstructionFactory.Float((Double) symV.getValue());
				} else if(!(symV.getValue() instanceof UninitializedValue)) {
					throw new RuntimeException("An value have a invalid value: " + res);
				}
				if(inst != null){
					ComplexInstruction parent = g.getParent();
//					int index = parent.getChildren().indexOf(g);
//					parent.getChildren().set(index, inst);
					parent.replaceChild(g, inst);
				}
			}
			return res;
		}
		else{
			for(Instruction inst : g.getArgs()){
				doSwitch(inst);
			}
			return new SymbolValue(new UninitializedValue());
		}
	}
	
	
}
