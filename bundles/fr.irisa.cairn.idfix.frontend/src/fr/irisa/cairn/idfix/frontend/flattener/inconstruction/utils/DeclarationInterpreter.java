package fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils;

import fr.irisa.cairn.gecos.model.tools.switches.DefaultInstructionSwitch;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.GenericInstInterpreter;
import fr.irisa.cairn.idfix.utils.exceptions.FlattenerException;
import gecos.instrs.CallInstruction;
import gecos.instrs.FloatInstruction;
import gecos.instrs.GenericInstruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.SymbolInstruction;

/**
 * Interpret the instruction used for initialize a value during declaration step
 * @author Nicolas Simon
 * @date Jul 3, 2013
 */
public class DeclarationInterpreter extends DefaultInstructionSwitch<Object> {
	private Context _context;
	private GenericInstInterpreter _ginstInterpreter;
	
	public DeclarationInterpreter(Context context){
		_context = context;
		_ginstInterpreter = new GenericInstInterpreter(this);
	}
	
	@Override
	public Object caseIntInstruction(IntInstruction g){
		return new SymbolValue(new Long(g.getValue()));
	}
	
	@Override
	public Object caseFloatInstruction(FloatInstruction g){
		return new SymbolValue(new Double(g.getValue()));
	}
	
	@Override
	public Object caseGenericInstruction(GenericInstruction g){
		return new SymbolValue(_ginstInterpreter.doSwitch(g));
	}
	
	@Override
	public Object caseSymbolInstruction(SymbolInstruction g){
		try{
			return _context.getValue(g.getSymbol());
		} catch(FlattenerException e){
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Object caseCallInstruction(CallInstruction g){
		throw new RuntimeException("Error: Declaration initialization via function not implemented yet");
	}
}
