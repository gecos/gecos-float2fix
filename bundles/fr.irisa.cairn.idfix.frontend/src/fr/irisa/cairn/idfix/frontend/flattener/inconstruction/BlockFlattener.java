package fr.irisa.cairn.idfix.frontend.flattener.inconstruction;

import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.tools.switches.BasicBlockSwitch;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils.Context;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils.SymbolValue;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils.UninitializedValue;
import fr.irisa.cairn.idfix.utils.exceptions.FlattenerException;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Scope;
import gecos.instrs.Instruction;

/**
 * Block switch
 * @author Nicolas Simon
 * @date Jul 3, 2013
 */
public class BlockFlattener extends BasicBlockSwitch<Object>{
	private Context _context;
	private InstructionFlattener _instFlattener;
	private ConditionInterpreter _condInterpreter;
	private BlockManager _manager;
	
	public BlockFlattener(Context context, BlockManager manager){
		_context = context;
		_instFlattener = new InstructionFlattener(context, manager);
		_condInterpreter = new ConditionInterpreter(context, manager);
		_manager = manager;
	}
	
	/**
	 * When visiting a composite block, there is a new scope to manage. After
	 * adding declarations to the context, the interpretation consists in
	 * visiting each block of the composite block.
	 */
	@Override
	public Object caseCompositeBlock(CompositeBlock cb){
		try {			
			Scope newScope = cb.getScope().copy();
			CompositeBlock newCB = GecosUserBlockFactory.CompositeBlock(newScope);
			_manager.addToCurrentBlock(newCB);
			CompositeBlock saveCurrentCB = _manager.getCurrentCompositeBlock();
			_manager.setCurrentCompositeBlock(newCB);
			_context.push(cb.getScope());
			
			for(Block b : cb.getChildren()){
				doSwitch(b);
			}
			
			_context.pop();
			
			_manager.setCurrentCompositeBlock(saveCurrentCB);
			return null;
		} catch (FlattenerException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Object caseBasicBlock(BasicBlock b) {
		for(Instruction i : b.getInstructions()){
			_instFlattener.doSwitch(i);
		}
		return null;
	}
	
	/**
	 * FOR with no static resolvable conditions are not managed
	 */
	@Override
	public Object caseForBlock(ForBlock b){
		doSwitch(b.getInitBlock());
		Object cond;
		SymbolValue symSA;
		cond =_condInterpreter.doSwitch(b.getTestBlock());
		if(!(cond instanceof SymbolValue))
			throw new RuntimeException("A condition is not a symbol scalar");
		 symSA = (SymbolValue) cond;
		
		if(!(symSA.getValue() instanceof Boolean)) // FIXME Modifier le condinterpreter pour faire en sorte qu'il ne rend que des boolean ou null
			throw new RuntimeException("Error: A for loop used no static evaluable guard (" + b.getTestBlock().getInstructions() + ")" );
		boolean k = (Boolean) symSA.getValue();

		while (k){
			doSwitch(b.getBodyBlock());
			doSwitch(b.getStepBlock());
			cond =_condInterpreter.doSwitch(b.getTestBlock());
			if(!(cond instanceof SymbolValue))
				throw new RuntimeException("A condition is not a symbol scalar");
			 symSA = (SymbolValue) cond;
			
			if(symSA.getValue() instanceof UninitializedValue) // FIXME Modifier le condinterpreter pour faire en sorte qu'il ne rend que des boolean ou null
				throw new RuntimeException("Error: A for loop used no static evaluable guard (" + b.getTestBlock().getInstructions() + ")" );
			k = (Boolean) symSA.getValue();
		}

		return null;
	}
	
	
	
	@Override
	public Object caseDoWhileBlock(DoWhileBlock b) {
		throw new RuntimeException("Error: Loop management not implemented yet");
	}

	/**
	 * WHILE with no static resolvable conditions are not managed
	 */
	@Override
	public Object caseWhileBlock(WhileBlock b) {
		SymbolValue symSA;
		Object cond = _condInterpreter.doSwitch(b.getTestBlock());
		if(!(cond instanceof SymbolValue))
			throw new RuntimeException("Error: A condition is not a symbol scalar");
		 symSA = (SymbolValue) cond;
		 
		 if(!(symSA.getValue() instanceof Boolean)) // FIXME Modifier le condinterpreter pour faire en sorte qu'il ne rend que des boolean ou null
			throw new RuntimeException("Error: A for loop used no static evaluable guard (" + b.getTestBlock() + ")" );
		boolean k = (Boolean) symSA.getValue();
		
		while(k){
			doSwitch(b.getBodyBlock());
			cond = _condInterpreter.doSwitch(b.getTestBlock());
			if(!(cond instanceof SymbolValue))
				throw new RuntimeException("Error: A condition is not a symbol scalar");
			 symSA = (SymbolValue) cond;
			 
			 if(!(symSA.getValue() instanceof Boolean)) // FIXME Modifier le condinterpreter pour faire en sorte qu'il ne rend que des boolean ou null
				throw new RuntimeException("Error: A for loop used no static evaluable guard (" + b.getTestBlock() + ")" );
			k = (Boolean) symSA.getValue();
		}
		
		return null;
	}

	/**
	 * IF with no static resolvable conditions are not managed
	 */
	@Override
	public Object caseIfBlock(IfBlock b){
		SymbolValue symSA;
		
		Object cond = _condInterpreter.doSwitch(b.getTestBlock());
		if(!(cond instanceof SymbolValue))
			throw new RuntimeException("A condition is not a symbol scalar");
		 symSA = (SymbolValue) cond;
		 
		if(symSA.getValue() instanceof UninitializedValue)  // FIXME Modifier le condinterpreter pour faire en sorte qu'il ne rend que des boolean ou null
			throw new RuntimeException("Error: A conditional structure IF used no static evaluable guard (" + b.getTestBlock() + ")" );
		boolean k = (Boolean) symSA.getValue();
		
		if(k){
			doSwitch(b.getThenBlock());
		}
		else{
			if(b.getElseBlock() != null)
				doSwitch(b.getElseBlock());
		}
		
		return null;
	}
}
