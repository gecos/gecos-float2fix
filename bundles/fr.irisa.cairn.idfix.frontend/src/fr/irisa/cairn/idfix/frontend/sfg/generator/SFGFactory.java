//package fr.irisa.cairn.idfix.frontend.sfg.generator;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Map;
//
//import float2fix.Annotations;
//import float2fix.Edge;
//import float2fix.Float2fixFactory;
//import float2fix.Graph;
//import float2fix.Node;
//import fr.irisa.cairn.idfix.frontend.sfg.generator.utils.FieldNode;
//import fr.irisa.cairn.idfix.frontend.sfg.generator.utils.INodeType;
//import fr.irisa.cairn.idfix.frontend.sfg.generator.utils.IndexedNode;
//import fr.irisa.cairn.idfix.frontend.sfg.generator.utils.ScalarNode;
//import fr.irisa.cairn.idfix.frontend.utils.AnnotateBasicBlocks;
//import fr.irisa.cairn.idfix.frontend.utils.AnnotateOperators;
//import fr.irisa.cairn.idfix.frontend.utils.AnnotateVariables;
//import fr.irisa.cairn.idfix.utils.IDFixUtils;
//import fr.irisa.cairn.idfix.utils.exceptions.FlattenerException;
//import fr.irisa.cairn.idfix.utils.exceptions.SFGGeneratorException;
//import fr.irisa.cairn.idfix.utils.exceptions.SFGModelCreationException;
//import gecos.core.ParameterSymbol;
//import gecos.core.Scope;
//import gecos.core.Symbol;
//import gecos.instrs.ArrayValueInstruction;
//import gecos.instrs.GenericInstruction;
//import gecos.instrs.Instruction;
//import gecos.instrs.SetInstruction;
//import gecos.types.ArrayType;
//import gecos.types.Field;
//import gecos.types.RecordType;
//import gecos.types.Type;
//
///**
// * Factory use to create a Signal Flow Graph for IDFixEval. It provide function to get node and create new different kind of node
// * @author Nicolas Simon
// * @date Jul 12, 2013
// */
//public class SFGFactory {
//	private int _idNode; /**< current id use to specify the unique id of the next node created */
//	private int _currNumOpSFG;
//	
//	private Float2fixFactory _factory; /**< Factory use to create node and edge */
//	private Graph _graph; /**< Graph build */
//	
//	private ArrayIndexInterpreter _arrayIndexInterpreter; /**<  Interpreter use to obtain the index for array */
//	private DeclarationInterpreter _declarationInterpreter; /**< Interpreter use to initialization at the variable declaration */
//
//	private Map<Symbol, INodeType> _binding; /**< Map which specify which last {@link INodeType} have been created for a specific {@link Symbol} */
//	
//	private static Map<Integer, List<Integer>> _opsOutputAffectingVar;
//	private static Map<Integer, List<Integer>> _opsInputAffectingVar;
//	
//	public SFGFactory(){
//		_idNode = 2;
//		_currNumOpSFG = 0;
//		_factory = Float2fixFactory.eINSTANCE;
//		_graph = _factory.createGraph();
//		_arrayIndexInterpreter = new ArrayIndexInterpreter();
//		_declarationInterpreter = new DeclarationInterpreter(this);
//		_binding = new HashMap<>();
//		
//		_opsOutputAffectingVar = new HashMap<Integer, List<Integer>>();
//		_opsInputAffectingVar = new HashMap<Integer, List<Integer>>();
//	}
//	
//	
//	private void myassert(boolean b, String s) throws SFGGeneratorException{
//		if (!b){
//			throw new SFGGeneratorException(s);
//		}
//	}
//	
//	/**
//	 * Generate {@link Node} corresponding of the {@link Symbol} contains into the {@link Scope}
//	 * @param scope
//	 * @throws SFGGeneratorException
//	 * @throws SFGModelCreationException
//	 */
//	public void generateVariableNode(Scope scope) throws SFGGeneratorException, SFGModelCreationException{
//		for(Symbol s : scope.getSymbols()){
//			if(s.getType().isScalarType()){
//				initScalarMap(s);
//			}
//			else if(s.getType().isStruct()){
//				initFieldMap(s);
//			}
//			else if(s.getType().isArray()){
//				initIndexedNode(s);
//			}
//			else{
//				throw new RuntimeException("Kind of SymbolInstruction not managed");
//			}
//		}
//	}
//	
//	/**
//	 * Generate {@link Node} corresponding of the {@link Symbol} contains into the list
//	 * @param list
//	 * @throws SFGGeneratorException
//	 * @throws SFGModelCreationException
//	 */
//	public void generateVariableNode(List<ParameterSymbol> list) throws SFGGeneratorException, SFGModelCreationException{
//		for(Symbol s : list){
//			if(s.getType().isScalarType()){
//				initScalarMap(s);
//			}
//			else if(s.getType().isStruct()){
//				initFieldMap(s);
//			}
//			else if(s.getType().isArray()){
//				initIndexedNode(s);
//			}
//			else{
//				throw new RuntimeException("Kind of SymbolInstruction not managed");
//			}
//		}
//	}
//	
//	/**
//	 * Finalize the generation of the SFG and return it
//	 * @return The SFG generated
//	 * @throws SFGGeneratorException
//	 */
//	public Graph finishhim() throws SFGGeneratorException{
//		deleteSecondaryGraph();
//		
//		Map<Node, List<Edge>> mapEdgePred = new HashMap<Node, List<Edge>>();
//		Map<Node, List<Edge>> mapEdgeSucc = new HashMap<Node, List<Edge>>();
//		List<Node> listNodeOutput = new LinkedList<Node>();
//		
//		createMapInterdependanceNodeEdge(mapEdgePred, mapEdgeSucc, listNodeOutput);
//		updateInputOutputAffectationVar(mapEdgePred, mapEdgeSucc);
//		renumberNodes();
//		
//		return _graph;
//	}
//
//	/**
//	 * Use for create and initialize {@link Node} from a {@link Symbol} corresponding to a scalar variable
//	 * @param s
//	 * @throws SFGGeneratorException
//	 * @throws SFGModelCreationException
//	 */
//	private void initScalarMap(Symbol s) throws SFGGeneratorException, SFGModelCreationException{
//		_binding.put(s, new ScalarNode(null));
//		
//		ScalarNode scalarNode = createScalarNode(s);
//		Node snode = scalarNode.getNode();
////		Node snode = getNode(s);
//		if(s.getValue() != null){
//			
//			Object o = _declarationInterpreter.doSwitch(s.getValue());
//			if(o instanceof Node){
//				Node decl = (Node) o;
//				boolean createSetNode = false;
//				for(Annotations annot : decl.getAttr()){
//					if(annot.getKey().equals("CLASS") && annot.getValue().equals("DATA")){
//						createSetNode = true;
//						break;
//					}
//				}
//				
//				if(createSetNode){
//					Node op = createNode("=");
//					op.getAttr().add(createAnnotation("CLASS", "OP"));
//					op.getAttr().add(createAnnotation("NATURE", "EQ"));	
//					op.getAttr().add(createAnnotation("NUM_OP_SFG", Integer.toString(_currNumOpSFG)));
//					_currNumOpSFG++;
//				
//					Integer numOpCDFG = AnnotateOperators.getNumOpCDFG(s.getValue());
//					myassert((numOpCDFG != null), "NUM_OP_CDFG is null");
//					// FIXME Lors de la création des numéros CDFG-IDFixEval, il y a des trous qui apparaissent dû a des opérations de contrôle toujours présent dans le code
//					// Ces opérations de contrôle sont correct car elle représente le step block du For par exemple
//					AnnotateOperators.addNumOpIDFix(numOpCDFG);
//					AnnotateOperators.incNumOpCDFGUsed(s.getValue());
//					
//					op.getAttr().add(createAnnotation("NUM_OP_CDFG", Integer.toString(AnnotateOperators.getNumOpIDFix(numOpCDFG))));
//					_graph.getNode().add(op);
//					
//					createEdge((Node) o, op, 0);
//					createEdge(op, snode);
//				}
//				else{
//					createEdge((Node) o, snode);
//				}
//			}
//			else if(o instanceof Long || o instanceof Double){
//				updateNodeValue(snode, o);
//			}
//			else{
//				throw new SFGGeneratorException("Error: Unknown kind of value for scalar initialization (" + o + ")");
//			}
//		}
//	}
//	
//	/**
//	 * Use for create and initialize {@link Node} from a {@link Symbol} corresponding to a field variable
//	 * @param s
//	 * @throws SFGGeneratorException
//	 */
//	private void initFieldMap(Symbol s) throws SFGGeneratorException{
//		RecordType recordType = (RecordType) s.getType();
//		Map<Field, Node> map = new HashMap<>();
//		for(Field f : recordType.getFields()){
//			map.put(f, null);
//		}
//		
//		INodeType ret = new FieldNode(map);
//		_binding.put(s, ret);	
//		createFieldNode(s);
//		
//	}
//	
//	/**
//	 * Use for create and initialize {@link Node} from a {@link Symbol} corresponding to a array variable
//	 * @param s
//	 * @throws SFGGeneratorException
//	 * @throws SFGModelCreationException
//	 */	
//	private void initIndexedNode(Symbol s) throws SFGGeneratorException, SFGModelCreationException {
//		ArrayType arrType;
//		Instruction sizeInst;
//		Object res;
//		Type type;
//		int size = 1;
//		for(type = s.getType() ; type instanceof ArrayType ; type = ((ArrayType) type).getBase()){
//			arrType = (ArrayType) type;
//			sizeInst = arrType.getSizeExpr();
//			res = _arrayIndexInterpreter.doSwitch(sizeInst);
//			if(!(res instanceof Long))
//				throw new SFGGeneratorException("Error: SymbolValue is not used for a scalar variable or constant");
//			
//			size *= (Long) res;			
//		}
//		
//		if(type.isScalarType()){
//			IndexedNode<ScalarNode> indexedNode = new IndexedNode<ScalarNode>(size);
//			_binding.put(s, indexedNode);
//			for(int i = 0 ; i < size ; i++){
//				indexedNode.setNodeType(i, new ScalarNode(null));
//				createIndexedNode(s, getArrayIndex(s.getType(), i));
//			}
//			if(s.getValue() != null){
//				List<Object> values = new ArrayList<>();
//				recArrayInitalization(s.getValue(), values);
//				myassert(size == values.size(), "Error: Size of array is different of the number of value into the declaration field");
//				initValuesArrayNode(s, values);
//			}
//		}
//		else if(type.isStruct()){
//			IndexedNode<FieldNode> indexedNode = new IndexedNode<FieldNode>(size);
//			_binding.put(s, indexedNode);
//			RecordType recType = (RecordType) type;
//			Map<Field, Node> map = new HashMap<>();
//			for(Field f : recType.getFields()){
//				map.put(f, null);
//			}
//			for(int i = 0 ; i < size ; i++){
//				indexedNode.setNodeType(i, new FieldNode(map));
//				createIndexedFieldNode(s, getArrayIndex(s.getType(), i));
//			}
//		}
//		else if(type.isPointer()){
//			throw new SFGGeneratorException("Error: Array of pointer type not managed yet");
//		}
//		else if(type.isAlias()){
//			throw new SFGGeneratorException("Error: Array of alias type not managed yet");
//		}
//		else{
//			throw new SFGGeneratorException("Error: Type of array unknown (Symbol:" + s + ", Type:" + type + ")");
//		}
//		
//	}
//	
//	/**
//	 * Return the elements list contains into the Gecos IR for an array (mutli-dimensional or not)
//	 * @param inst
//	 * @param v
//	 * @throws FlattenerException 
//	 * @throws SFGGeneratorException 
//	 */	
//	private void recArrayInitalization(Instruction inst, List<Object> v) throws SFGGeneratorException{
//		if (inst instanceof ArrayValueInstruction){
//			ArrayValueInstruction insts = (ArrayValueInstruction) inst;
//			for (Instruction i : insts.getChildren()){
//				recArrayInitalization(i,v);
//			}
//		}
//		else {
//			Object res = _declarationInterpreter.doSwitch(inst);
//			if(!(res instanceof Node || res instanceof Long || res instanceof Double))
//				throw new SFGGeneratorException("Error: Unknown kind of value for scalar initialization (" + res + ")");
//			
//			v.add(res);
//		}
//	}
//	
//	private void initValuesArrayNode(Symbol s, List<Object> values) throws SFGGeneratorException, SFGModelCreationException{
//		Object o;
//		Node node;
//		for(int i = 0 ; i < values.size() ; i++){
//			o = values.get(i);
//			ScalarNode scalarNode = createIndexedNode(s, getArrayIndex(s.getType(), i));
//			node = scalarNode.getNode();
//			if(o instanceof Node){
//				Node decl = (Node) o;
//				boolean createSetNode = false;
//				for(Annotations annot : decl.getAttr()){
//					if(annot.getKey().equals("CLASS") && annot.getValue().equals("DATA")){
//						createSetNode = true;
//						break;
//					}
//				}
//				
//				if(createSetNode){
//					Node op = createNode("=");
//					op.getAttr().add(createAnnotation("CLASS", "OP"));
//					op.getAttr().add(createAnnotation("NATURE", "EQ"));	
//					op.getAttr().add(createAnnotation("NUM_OP_SFG", Integer.toString(_currNumOpSFG)));
//					_currNumOpSFG++;
//				
//					Integer numOpCDFG = AnnotateOperators.getNumOpCDFG(s.getValue());
//					myassert((numOpCDFG != null), "NUM_OP_CDFG is null");
//					// FIXME Lors de la création des numéros CDFG-IDFixEval, il y a des trous qui apparaissent dû a des opérations de contrôle toujours présent dans le code
//					// Ces opérations de contrôle sont correct car elle représente le step block du For par exemple
//					AnnotateOperators.addNumOpIDFix(numOpCDFG);
//					AnnotateOperators.incNumOpCDFGUsed(s.getValue());
//					
//					op.getAttr().add(createAnnotation("NUM_OP_CDFG", Integer.toString(AnnotateOperators.getNumOpIDFix(numOpCDFG))));
//					
//					_graph.getNode().add(op);
//					
//					createEdge((Node) o, op, 0);
//					createEdge(op, node);
//				}
//				else{
//					createEdge((Node) o, node);
//				}
//			}
//			else if(o instanceof Long || o instanceof Double){
//				updateNodeValue(node, o);
//			}
//		}
//	}
//	
//	/**
//	 * Associate parameters of the procedure to the data given as parameters
//	 * @param paramEff
//	 * @param paramThe
//	 */
//	public void updateCallArrayParameters(Symbol paramEff, Symbol paramThe){
//		INodeType nodeEff = getNodeType(paramEff);
//		_binding.put(paramThe, nodeEff);
//	}
//		
//	/********************* NODE GETTERS *********************/
//	
//	private INodeType getNodeType(Symbol s){
//		return _binding.get(s);
//	}
//	
//	/**
//	 * Get the {@link Node} associate to the {@link Symbol} s. The {@link Symbol} s must be represented a scalar variable 
//	 * @param s
//	 * @return
//	 * @throws SFGGeneratorException 
//	 */
//	public INodeType getNode(Symbol s) throws SFGGeneratorException{
//		INodeType ret = getNodeType(s);
//		if(ret instanceof ScalarNode){
//			if(((ScalarNode)ret).getNode() == null){
//				return createScalarNode(s);
//			}
//		}
//		return ret;
//	}
//	
//	/**
//	 * Get the {@link Node} associate to the {@link Symbol} s. The {@link Symbol} s must be represented a field variable
//	 * @param s
//	 * @param f
//	 * @return
//	 * @throws SFGGeneratorException 
//	 */
//	public ScalarNode getNode(Symbol s, Field f) throws SFGGeneratorException{
//		INodeType ret = getNodeType(s);
//		myassert(ret instanceof FieldNode, "A field variable not used a FieldNode");
//		FieldNode fnode = (FieldNode) ret;
//		if(fnode.getNode(f) == null){
//			throw new SFGGeneratorException("Error: Node doesn't exist");
//		}
//		else{
//			return new ScalarNode(fnode.getNode(f));
//		}
//		
//	}
//	
//	/**
//	 * Get the {@link Node} associate to the {@link Symbol} s. The {@link Symbol} s must be represented a array variable
//	 * @param s
//	 * @return
//	 * @throws SFGGeneratorException
//	 */	
//	public INodeType getNode(Symbol s, List<Long> indexes) throws SFGGeneratorException{
//		INodeType ret = getNodeType(s);
//		myassert(ret instanceof IndexedNode<?>, "A array variable not used a IndexedNode");
//		IndexedNode<?> anode = (IndexedNode<?>) ret;
//		int index = getArrayIndex(s.getType(), indexes);
//		
//		if(anode.getNodeType(index) == null)
//			throw new SFGGeneratorException("Error: NodeType at index " + index + " of IndexedNode doesn't exist");
//		
//		if(!(anode.getNodeType(index) instanceof ScalarNode || anode.getNodeType(index) instanceof FieldNode))
//			throw new SFGGeneratorException("Kind unknown use into IndexedNode type");		
//		else{
//			INodeType value = anode.getNodeType(index);
//			return value;
//		}
//	}
//	
//	public INodeType getNode(Symbol s, List<Long> indexes, Field f) throws SFGGeneratorException{
//		INodeType ret = getNodeType(s);
//		myassert(ret instanceof IndexedNode<?>, "A array variable not used a IndexedNode");
//		IndexedNode<?> anode = (IndexedNode<?>) ret;
//		int index = getArrayIndex(s.getType(), indexes);
//		
//		if(anode.getNodeType(index) == null)
//			throw new SFGGeneratorException("Error: NodeType at index " + index + " of IndexedNode doesn't exist");
//		
//		if(!(anode.getNodeType(index) instanceof FieldNode))
//			throw new SFGGeneratorException("Structure variable not used FieldNode type");		
//		else{
//			FieldNode value = (FieldNode) anode.getNodeType(index);
//			return new ScalarNode(value.getNode(f));
//		}
//	}
//	
//	/********************* OPERATOR NODE CONSTUCTOR *********************/
//	
//	/**
//	 * Create a Set node from a {@link SetInstruction}
//	 * @param s
//	 * @return
//	 * @throws SFGGeneratorException
//	 * @throws SFGModelCreationException
//	 */
//	public Node generateSetNode(SetInstruction s) throws SFGGeneratorException, SFGModelCreationException{
//		Node node = createNode("=");
//		
//		node.getAttr().add(createAnnotation("CLASS", "OP"));
//		node.getAttr().add(createAnnotation("NATURE", "EQ"));
//		node.getAttr().add(createAnnotation("NUM_OP_SFG", Integer.toString(_currNumOpSFG)));
//		_currNumOpSFG++;
//	
//		Integer numOpCDFG = AnnotateOperators.getNumOpCDFG(s);
//		myassert((numOpCDFG != null), "NUM_OP_CDFG is null");
//		// FIXME Lors de la création des numéros CDFG-IDFixEval, il y a des trous qui apparaissent dû a des opérations de contrôle toujours présent dans le code
//		// Ces opérations de contrôle sont correct car elle représente le step block du For par exemple
//		AnnotateOperators.addNumOpIDFix(numOpCDFG);
//		AnnotateOperators.incNumOpCDFGUsed(s);
//		
//		node.getAttr().add(createAnnotation("NUM_OP_CDFG", Integer.toString(AnnotateOperators.getNumOpIDFix(numOpCDFG))));
//		node.getAttr().add(createAnnotation("NUM_BASIC_BLOCK", AnnotateBasicBlocks.getBasicBlockNumber(s.getDest().getBasicBlock())));
//		_graph.getNode().add(node);
//		return node;
//	}
//	
//	/**
//	 * Create a delay node from a {@link SetInstruction}
//	 * @param s
//	 * @return
//	 * @throws SFGGeneratorException
//	 * @throws SFGModelCreationException 
//	 */
//	public Node generateDelayNode(SetInstruction s) throws SFGGeneratorException, SFGModelCreationException{
//		Node node = createNode("z-1");
//		
//		node.getAttr().add(createAnnotation("CLASS", "OP"));
//		node.getAttr().add(createAnnotation("NATURE", "DELAY"));
//		node.getAttr().add(createAnnotation("NUM_OP_SFG", Integer.toString(_currNumOpSFG)));
//		_currNumOpSFG++;
//		
//		Integer numOpCDFG = AnnotateOperators.getNumOpCDFG(s);
//		myassert((numOpCDFG != null), "Error: Num Ops CDFG (IDFixConv) is null");
//		// FIXME Lors de la création des numéros CDFG-IDFixEval, il y a des trous qui apparaissent dû a des opérations de contrôle toujours présent dans le code
//		// Ces opérations de contrôle sont correct car elle représente le step block du For par exemple
//		AnnotateOperators.addNumOpIDFix(numOpCDFG);
//		AnnotateOperators.incNumOpCDFGUsed(s);
//		
//		node.getAttr().add(createAnnotation("NUM_OP_CDFG", Integer.toString(AnnotateOperators.getNumOpIDFix(numOpCDFG))));
//		
//		_graph.getNode().add(node);
//		return node;
//	}
//	
//	/**
//	 * Create a operation node from a {@link GenericInstruction}
//	 * @param g
//	 * @return
//	 * @throws SFGGeneratorException
//	 * @throws SFGModelCreationException
//	 */
//	public Node generateGenericOperationNode(GenericInstruction g) throws SFGGeneratorException, SFGModelCreationException{
//		Node node = createNode(convertInstName(g.getName()));
//		
//		node.getAttr().add(createAnnotation("CLASS", "OP"));
//		node.getAttr().add(createAnnotation("NATURE", convertInstName2(g.getName())));
//		node.getAttr().add(createAnnotation("NUM_OP_SFG", Integer.toString(_currNumOpSFG)));
//		_currNumOpSFG++;
//		
//		Integer numOpCDFG = AnnotateOperators.getNumOpCDFG(g);
//		myassert((numOpCDFG != null), "NUM_OP_CDFG is null");
//		// FIXME Lors de la création des numéros CDFG-IDFixEval, il y a des trous qui apparaissent dû a des opérations de contrôle toujours présent dans le code
//		// Ces opérations de contrôle sont correct car elle représente le step block du For par exemple
//		AnnotateOperators.addNumOpIDFix(numOpCDFG);
//		AnnotateOperators.incNumOpCDFGUsed(g);
//		
//		node.getAttr().add(createAnnotation("NUM_OP_CDFG", Integer.toString(AnnotateOperators.getNumOpIDFix(numOpCDFG))));
//		node.getAttr().add(createAnnotation("NUM_BASIC_BLOCK", AnnotateBasicBlocks.getBasicBlockNumber(g.getBasicBlock())));
//		
//		_graph.getNode().add(node);
//		return node;
//	}
//	
//	
//	/********************* VARIABLE NODE CONSTUCTOR *********************/
//	
//	/**
//	 * Create a {@link Node} with an unique id number
//	 * @param name
//	 * @return
//	 */
//	private Node createNode(String name){
//		Node node = _factory.createNode();
//		node.setName(name);
//		node.setId(_idNode);
//		_idNode++;
//		return node;
//	}
//	
//	/**
//	 * Create a {@link Node} {@link Annotations}
//	 * @param key
//	 * @param value
//	 * @return
//	 */
//	private Annotations createAnnotation(String key, String value){
//		Annotations annot = _factory.createAnnotations();
//		annot.setKey(key);
//		annot.setValue(value);
//		return annot;
//	}
//	
//	/**
//	 * Create a {@link Node} for scalar variable from its {@link Symbol}
//	 * @param s
//	 * @return
//	 * @throws SFGGeneratorException
//	 */
//	public ScalarNode createScalarNode(Symbol s) throws SFGGeneratorException{
//		Node node = createNode(s.getName());
//		
//		node.getAttr().add(createAnnotation("NAME", s.getName()));
//		node.getAttr().add(createAnnotation("CLASS", "DATA"));
//		node.getAttr().add(createAnnotation("NATURE", "VARIABLE_VAL_UNKNOWN"));
//		node.getAttr().add(createAnnotation("NUM_DATA_CDFG", Integer.toString(IDFixUtils.getSymbolAnnotationNumber(s))));
//		
//		if(AnnotateVariables.getVarWidth(s) != null)
//			node.getAttr().add(createAnnotation("VARIABLE_WIDTH", "TRUE"));
//		
//		if(IDFixUtils.getDynamicProcessing(s).equals("OVERFLOW")){
//			node.getAttr().add(createAnnotation("DYNAMIC_PROCESSING", "OVERFLOW"));
//			node.getAttr().add(createAnnotation("OVERFLOW_PROBABILITY", Double.toString(IDFixUtils.getOverflowProbability(s))));
//		}
//		
//		Double upper, lower;
//		if((lower = IDFixUtils.getVarMin(s)) != null && (upper = IDFixUtils.getVarMax(s)) != null){
//			node.getAttr().add(createAnnotation("VAL_MIN", Double.toString(lower)));
//			node.getAttr().add(createAnnotation("VAL_MAX", Double.toString(upper)));
//			node.getAttr().add(createAnnotation("PROPERTY", "INPUT"));
//		}
//		
//		if(IDFixUtils.isOutputVar(s)){
//			node.getAttr().add(createAnnotation("PROPERTY", "OUTPUT"));
//		}
//		
//		_graph.getNode().add(node);
//		
//		INodeType nodeType = getNodeType(s);
//		if(nodeType == null){
//			throw new RuntimeException("A scalar variable have not a ScalarNode already created during the initialization");
////			nodeType = new ScalarNode(null);
////			_binding.put(s, nodeType);
//		}
//		myassert(nodeType instanceof ScalarNode, "A scalar variable not used a ScalarNode (" + s + ")");
//		ScalarNode scalarNode = (ScalarNode) nodeType;
//		scalarNode.setNode(node);
//		return scalarNode;
//	}
//	
//	// FIXME Changer l'annotation des symbol pour que les stucture soit annoté aux niveau des champs
//	// 		  Regarder pour les annotations (DYNAMIC, WIDTH et autre)
//	/**
//	 * Create a {@link Node} for field variable from its {@link Symbol}
//	 * @param s
//	 * @param f
//	 * @return
//	 * @throws SFGGeneratorException
//	 */
//	public INodeType createFieldNode(Symbol s, Field f) throws SFGGeneratorException{
//		Node node = createNode(s.getName() + "." + f.getName());
//		
//		node.getAttr().add(createAnnotation("NAME", s.getName() + "." + f.getName()));
//		node.getAttr().add(createAnnotation("CLASS", "DATA"));
//		node.getAttr().add(createAnnotation("NATURE", "VARIABLE_VAL_UNKNOWN"));
//		node.getAttr().add(createAnnotation("NUM_DATA_CDFG", Integer.toString(IDFixUtils.getSymbolAnnotationNumber(s, f))));
//		
//		if(AnnotateVariables.getVarWidth(s) != null)
//			node.getAttr().add(createAnnotation("VARIABLE_WIDTH", "TRUE"));
//		
//		if(IDFixUtils.getDynamicProcessing(s).equals("OVERFLOW")){
//			node.getAttr().add(createAnnotation("DYNAMIC_PROCESSING", "OVERFLOW"));
//			node.getAttr().add(createAnnotation("OVERFLOW_PROBABILITY", Double.toString(IDFixUtils.getOverflowProbability(s))));
//		}
//		
//		Double upper, lower;
//		if((lower = IDFixUtils.getVarMin(s)) != null && (upper = IDFixUtils.getVarMax(s)) != null){
//			node.getAttr().add(createAnnotation("VAL_MIN", Double.toString(lower)));
//			node.getAttr().add(createAnnotation("VAL_MAX", Double.toString(upper)));
//			node.getAttr().add(createAnnotation("PROPERTY", "INPUT"));
//		}
//		
//		if(IDFixUtils.isOutputVar(s)){
//			node.getAttr().add(createAnnotation("PROPERTY", "OUTPUT"));
//		}
//		
//		_graph.getNode().add(node);
//		INodeType nodeType = getNodeType(s);
//		myassert(nodeType instanceof FieldNode, "A field variable not used a FieldNode");
//		FieldNode fieldNode = (FieldNode) nodeType;
//		fieldNode.setValue(f, node);
//		
//		return new ScalarNode(node);
//	}
//	
//	public FieldNode createFieldNode(Symbol s) throws SFGGeneratorException{
//		RecordType recordType = (RecordType) s.getType();
//		
//		INodeType nodeType = getNodeType(s);
//		myassert(nodeType instanceof FieldNode, "A field variable not used a FieldNode");
//		FieldNode fieldNode = (FieldNode) nodeType;
//		
//		for(Field f : recordType.getFields()){
//		
//			Node node = createNode(s.getName() + "." + f.getName());
//			
//			node.getAttr().add(createAnnotation("NAME", s.getName() + "." + f.getName()));
//			node.getAttr().add(createAnnotation("CLASS", "DATA"));
//			node.getAttr().add(createAnnotation("NATURE", "VARIABLE_VAL_UNKNOWN"));
//			node.getAttr().add(createAnnotation("NUM_DATA_CDFG", Integer.toString(IDFixUtils.getSymbolAnnotationNumber(s, f))));
//			
//			if(AnnotateVariables.getVarWidth(s) != null)
//				node.getAttr().add(createAnnotation("VARIABLE_WIDTH", "TRUE"));
//			
//			if(IDFixUtils.getDynamicProcessing(s).equals("OVERFLOW")){
//				node.getAttr().add(createAnnotation("DYNAMIC_PROCESSING", "OVERFLOW"));
//				node.getAttr().add(createAnnotation("OVERFLOW_PROBABILITY", Double.toString(IDFixUtils.getOverflowProbability(s))));
//			}
//			
//			Double upper, lower;
//			if((lower = IDFixUtils.getVarMin(s)) != null && (upper = IDFixUtils.getVarMax(s)) != null){
//				node.getAttr().add(createAnnotation("VAL_MIN", Double.toString(lower)));
//				node.getAttr().add(createAnnotation("VAL_MAX", Double.toString(upper)));
//				node.getAttr().add(createAnnotation("PROPERTY", "INPUT"));
//			}
//			
//			if(IDFixUtils.isOutputVar(s)){
//				node.getAttr().add(createAnnotation("PROPERTY", "OUTPUT"));
//			}
//			
//			_graph.getNode().add(node);
//			fieldNode.setValue(f, node);
//		}
//		return fieldNode;
//	}		
//	
//	/**
//	 * Create a {@link Node} for array variable from its {@link Symbol}
//	 * @param s
//	 * @param indexes
//	 * @return
//	 * @throws SFGGeneratorException
//	 */
//	public ScalarNode createIndexedNode(Symbol s, List<Long> indexes) throws SFGGeneratorException{
//		String res = "";
//		for (int i = 0; i < indexes.size(); i++){
//			res = res +"[" + indexes.get(i) + "]";
//		}
//		
//		Node node = createNode(s.getName() + res);
//		
//		node.getAttr().add(createAnnotation("NAME", s.getName()));
//		node.getAttr().add(createAnnotation("CLASS", "DATA"));
//		node.getAttr().add(createAnnotation("NATURE", "VARIABLE_VAL_UNKNOWN"));
//		node.getAttr().add(createAnnotation("NUM_DATA_CDFG", Integer.toString(IDFixUtils.getSymbolAnnotationNumber(s))));
//		node.getAttr().add(createAnnotation("INDEXES", res));
//		
//		if(AnnotateVariables.getVarWidth(s) != null)
//			node.getAttr().add(createAnnotation("VARIABLE_WIDTH", "TRUE"));
//		
//		if(IDFixUtils.getDynamicProcessing(s).equals("OVERFLOW")){
//			node.getAttr().add(createAnnotation("DYNAMIC_PROCESSING", "OVERFLOW"));
//			node.getAttr().add(createAnnotation("OVERFLOW_PROBABILITY", Double.toString(IDFixUtils.getOverflowProbability(s))));
//		}
//		
//		Double upper, lower;
//		if((lower = IDFixUtils.getVarMin(s)) != null && (upper = IDFixUtils.getVarMax(s)) != null){
//			node.getAttr().add(createAnnotation("VAL_MIN", Double.toString(lower)));
//			node.getAttr().add(createAnnotation("VAL_MAX", Double.toString(upper)));
//			node.getAttr().add(createAnnotation("PROPERTY", "INPUT"));
//		}
//		
//		if(IDFixUtils.isOutputVar(s)){
//			node.getAttr().add(createAnnotation("PROPERTY", "OUTPUT"));
//		}
//			
//		_graph.getNode().add(node);
//		INodeType nodeType = getNodeType(s);
//		myassert(nodeType instanceof IndexedNode<?>, "A array variable not used a IndexedNode");
//		IndexedNode<INodeType> indexedNode = (IndexedNode<INodeType>) nodeType;
//		INodeType nodeTypeIndexed = indexedNode.getNodeType(getArrayIndex(s.getType(), indexes));
//		myassert(nodeTypeIndexed instanceof ScalarNode, "A scalar variable not used a ScalarNode");
//		((ScalarNode) nodeTypeIndexed).setNode(node);
//		
//		return ((ScalarNode) nodeTypeIndexed);
//	}
//	
//	public FieldNode createIndexedFieldNode(Symbol s, List<Long> indexes) throws SFGGeneratorException{
//		String res = "";
//		for (int i = 0; i < indexes.size(); i++){
//			res = res +"[" + indexes.get(i) + "]";
//		}
//		
//		ArrayType arrType = (ArrayType) s.getType();
//		RecordType recordType = (RecordType) arrType.getBase();
//		
//		INodeType nodeType = getNodeType(s);
//		myassert(nodeType instanceof IndexedNode<?>, "A array variable not used a IndexedNode");
//		IndexedNode<INodeType> indexedNode = (IndexedNode<INodeType>) nodeType;
//		INodeType nodeTypeIndexed = indexedNode.getNodeType(getArrayIndex(s.getType(), indexes));
//		myassert(nodeTypeIndexed instanceof FieldNode, "A field variable not used a FieldNode");
//		FieldNode fieldNode = (FieldNode) nodeTypeIndexed;
//		
//		for(Field f : recordType.getFields()){
//		
//			Node node = createNode(s.getName() + "." + f.getName());
//			
//			node.getAttr().add(createAnnotation("NAME", s.getName() + "." + f.getName()));
//			node.getAttr().add(createAnnotation("CLASS", "DATA"));
//			node.getAttr().add(createAnnotation("NATURE", "VARIABLE_VAL_UNKNOWN"));
//			node.getAttr().add(createAnnotation("NUM_DATA_CDFG", Integer.toString(IDFixUtils.getSymbolAnnotationNumber(s, f))));
//			node.getAttr().add(createAnnotation("INDEXES", res));
//			
//			if(AnnotateVariables.getVarWidth(s) != null)
//				node.getAttr().add(createAnnotation("VARIABLE_WIDTH", "TRUE"));
//			
//			if(IDFixUtils.getDynamicProcessing(s).equals("OVERFLOW")){
//				node.getAttr().add(createAnnotation("DYNAMIC_PROCESSING", "OVERFLOW"));
//				node.getAttr().add(createAnnotation("OVERFLOW_PROBABILITY", Double.toString(IDFixUtils.getOverflowProbability(s))));
//			}
//			
//			Double upper, lower;
//			if((lower = IDFixUtils.getVarMin(s)) != null && (upper = IDFixUtils.getVarMax(s)) != null){
//				node.getAttr().add(createAnnotation("VAL_MIN", Double.toString(lower)));
//				node.getAttr().add(createAnnotation("VAL_MAX", Double.toString(upper)));
//				node.getAttr().add(createAnnotation("PROPERTY", "INPUT"));
//			}
//			
//			if(IDFixUtils.isOutputVar(s)){
//				node.getAttr().add(createAnnotation("PROPERTY", "OUTPUT"));
//			}
//				
//			_graph.getNode().add(node);
//			fieldNode.setValue(f, node);
//		
//		}
//		return fieldNode;
//	}
//	
//	/**
//	 * Create a {@link Node} for array variable from its {@link Symbol}
//	 * @param s
//	 * @param indexes
//	 * @return
//	 * @throws SFGGeneratorException
//	 */
//	public INodeType createIndexedNode(Symbol s, List<Long> indexes, Field f) throws SFGGeneratorException{
//		String res = "";
//		for (int i = 0; i < indexes.size(); i++){
//			res = res +"[" + indexes.get(i) + "]";
//		}
//		
//		Node node = createNode(s.getName() + "." + f.getName());
//		
//		node.getAttr().add(createAnnotation("NAME", s.getName() + "." + f.getName()));
//		node.getAttr().add(createAnnotation("CLASS", "DATA"));
//		node.getAttr().add(createAnnotation("NATURE", "VARIABLE_VAL_UNKNOWN"));
//		node.getAttr().add(createAnnotation("NUM_DATA_CDFG", Integer.toString(IDFixUtils.getSymbolAnnotationNumber(s, f))));
//		node.getAttr().add(createAnnotation("INDEXES", res));
//		
//		if(AnnotateVariables.getVarWidth(s) != null)
//			node.getAttr().add(createAnnotation("VARIABLE_WIDTH", "TRUE"));
//		
//		if(IDFixUtils.getDynamicProcessing(s).equals("OVERFLOW")){
//			node.getAttr().add(createAnnotation("DYNAMIC_PROCESSING", "OVERFLOW"));
//			node.getAttr().add(createAnnotation("OVERFLOW_PROBABILITY", Double.toString(IDFixUtils.getOverflowProbability(s))));
//		}
//		
//		Double upper, lower;
//		if((lower = IDFixUtils.getVarMin(s)) != null && (upper = IDFixUtils.getVarMax(s)) != null){
//			node.getAttr().add(createAnnotation("VAL_MIN", Double.toString(lower)));
//			node.getAttr().add(createAnnotation("VAL_MAX", Double.toString(upper)));
//			node.getAttr().add(createAnnotation("PROPERTY", "INPUT"));
//		}
//		
//		if(IDFixUtils.isOutputVar(s)){
//			node.getAttr().add(createAnnotation("PROPERTY", "OUTPUT"));
//		}
//			
//		_graph.getNode().add(node);
//		INodeType nodeType = getNodeType(s);
//		myassert(nodeType instanceof IndexedNode<?>, "A array variable not used a IndexedNode");
//		IndexedNode<INodeType> indexedNode = (IndexedNode<INodeType>) nodeType;
//		INodeType nodeTypeIndexed = indexedNode.getNodeType(getArrayIndex(s.getType(), indexes));
//		myassert(nodeTypeIndexed instanceof FieldNode, "A field variable not used a FieldNode");
//		((FieldNode) nodeTypeIndexed).setValue(f, node);
//		
//		return new ScalarNode(((FieldNode) nodeTypeIndexed).getNode(f));
//	}
//	
//	/**
//	 * Create a {@link Node} for int/long constant from its value
//	 * @param value
//	 * @return
//	 */
//	public ScalarNode createConstantNode(long value){
//		String name = Long.toString(value);
//		Node node = createNode(name);
//		
//		node.getAttr().add(createAnnotation("CLASS", "DATA"));
//		node.getAttr().add(createAnnotation("NATURE", "CONSTANT"));
//		node.getAttr().add(createAnnotation("NAME", name));
//		node.getAttr().add(createAnnotation("VALUE", name));
//		_graph.getNode().add(node);
//		return new ScalarNode(node);
//	}
//	
//	/**
//	 * Create a {@link Node} for float/double constant from its value
//	 * @param value
//	 * @return
//	 */
//	public ScalarNode createConstantNode(double value){
//		String name = Double.toString(value);
//		Node node = createNode(name);
//		
//		node.getAttr().add(createAnnotation("CLASS", "DATA"));
//		node.getAttr().add(createAnnotation("NATURE", "CONSTANT"));
//		node.getAttr().add(createAnnotation("NAME", name));
//		node.getAttr().add(createAnnotation("VALUE", name));
//		_graph.getNode().add(node);
//		return new ScalarNode(node);
//	}
//	
//	
//	/********************* EDGE CONSTUCTOR *********************/
//	
//	/**
//	 * Create an {@link Edge} from a predecessor {@link Node} to a successor {@link Node}.
//	 * @param pred
//	 * @param succ
//	 * @return
//	 * @throws SFGGeneratorException
//	 */
//	public Edge createEdge(Node pred, Node succ) throws SFGGeneratorException{
//		myassert(pred != null, "Predecessor of edge is null");
//		myassert(succ != null, succ + "Successor of edge is null");
//		Edge edge = _factory.createEdge();
//		edge.setId_pred(pred);
//		edge.setId_succ(succ);
//		_graph.getEdge().add(edge);
//		return edge;
//		
//	}
//	
//	/**
//	 * Create an {@link Edge} from a predecessor {@link Node} to a successor {@link Node} with an input operand number
//	 * @param pred
//	 * @param succ
//	 * @param numInput
//	 * @return
//	 * @throws SFGGeneratorException
//	 */
//	public Edge createEdge(Node pred, Node succ, int numInput) throws SFGGeneratorException{
//		myassert(numInput >= 0, "");
//		Edge edge = createEdge(pred, succ);
//		edge.getAttr().add(createAnnotation("NUM_INPUT", Integer.toString(numInput)));
//		return edge;
//	}
//	
//	
//	/********************* STATIC METHODS (USE DURING OPTIMIZATION) *********************/
//	
//	public static List<Integer> getOpsOutputAffectingVar(Integer varNum){
//		return _opsOutputAffectingVar.get(varNum);
//	}
//	
//	public static List<Integer> getOpsInputAffectingVar(Integer varNum){
//		return _opsInputAffectingVar.get(varNum);
//	}
//	
//	
//	/********************* PRIVATE METHODS *********************/
//	
//	private String convertInstName(String inst) throws SFGGeneratorException{
//		if (inst.equals("add")){
//			return "+";
//		}
//		else if (inst.equals("sub") || inst.equals("neg")){
//			return "-";
//		}
//		else if (inst.equals("div")){
//			return "/";
//		}
//		else if (inst.equals("mul")){
//			return "*";
//		}
//		else if (inst.equals("shl")){
//			return "<<";
//		}
//		else if (inst.equals("shr")){
//			return ">>";
//		}
//		else if(inst.equals("sqrt")){
//			return "sqrt";
//		}
//		else {
//			throw new SFGGeneratorException("Instruction " + inst + " not supported yet");
//		}
//	}
//	
//	private String convertInstName2(String inst) throws SFGGeneratorException{
//		if (inst.equals("add")){
//			return "ADD";
//		}
//		else if (inst.equals("sub")){
//			return "SUB";
//		}
//		else if (inst.equals("neg")){
//			return "NEG";
//		}
//		else if (inst.equals("div")){
//			return "DIV";
//		}
//		else if (inst.equals("mul")){
//			return "MUL";
//		}
//		else if (inst.equals("shl")){
//			return "SHL";
//		}
//		else if (inst.equals("shr")){
//			return "SHR";
//		}
//		else if(inst.equals("sqrt")){
//			return "SQRT";
//		}
//		else {
//			throw new SFGGeneratorException("Instruction " + inst + " not supported yet");
//		}
//	}
//	
//	
//	/**
//	 * Compute and return the flat index of a array (multi-dimensional or not)
//	 * @param type
//	 * @param indices
//	 * @return
//	 * @throws SFGGeneratorException
//	 */
//	private int getArrayIndex(Type type, List<Long> indices) throws SFGGeneratorException{ 
//		Type localType = type;
//		ArrayType arrayType = (ArrayType) localType;
//		if(arrayType.getNumberOfDimensions() != indices.size())
//			throw new SFGGeneratorException("Dimensions number problem");
//		
//		int ind = 0;
//		int nbElemParDim = 1;
//		for (int j = indices.size() - 1; j >= 0; j--){
//			arrayType = (ArrayType) localType;
//			ind += indices.get(j)*nbElemParDim;
//			nbElemParDim *= arrayType.getUpper() - arrayType.getLower() + 1;
//			localType = arrayType.getBase();
//		}
//		return ind;
//	}
//	
//	private List<Long> getArrayIndex(Type type, int index) throws SFGGeneratorException{		
//		ArrayType arrType;
//		Long dims[] = new Long[((ArrayType) type).getNumberOfDimensions()];
//		Long sizeDims[] = new Long[((ArrayType) type).getNumberOfDimensions()];
//		int i = ((ArrayType) type).getNumberOfDimensions() - 1;
//		for(Type itype = type ; itype instanceof ArrayType ; itype = ((ArrayType) itype).getBase()){
//			arrType = (ArrayType) itype;
//			sizeDims[i] = arrType.getUpper() - arrType.getLower() + 1;
//			dims[i] = 0l;
//		}
//		
//		for(int j = 0 ; j< index ; j++)
//			incrementDims(dims, sizeDims);
//		
//		List<Long> ret = new ArrayList<>();
//		for(Long o : dims){
//			ret.add(o);
//		}
//		return ret;
//	}
//	
//	private void incrementDims(Long dims[], Long sizeDims[]) throws SFGGeneratorException{
//		if(dims[dims.length - 1] + 1 < sizeDims[sizeDims.length - 1]){
//			dims[dims.length - 1]++;
//		}
//		else{
//			Long newListDims[] = new Long[dims.length - 1];
//			Long newListSizeDims[] = new Long[dims.length - 1];
//			for(int i = 0 ; i < dims.length - 2 ; i++){
//				newListDims[i] = dims[i];
//				newListSizeDims[i]  = sizeDims[i];
//			}
//			myassert(newListDims.length != 0, "Dimensions number problem");
//			incrementDims(newListDims, newListSizeDims);
//			dims[dims.length - 1] = 0l;
//		}
//	}
//	
//	/**
//	 * Delete all node and edge which are not directly or indirectly connected to output nodes  
//	 * @throws SFGGeneratorException
//	 */
//	private void deleteSecondaryGraph() throws SFGGeneratorException{
//		Map<Node, List<Edge>> mapEdgePred = new HashMap<Node, List<Edge>>();
//		Map<Node, List<Edge>> mapEdgeSucc = new HashMap<Node, List<Edge>>();
//		List<Node> listNodeOutput = new LinkedList<Node>();
//		List<Node> taggedNodeList = new ArrayList<Node>();
//		List<Edge> taggedEdgeList = new ArrayList<Edge>();
//		
//		this.createMapInterdependanceNodeEdge(mapEdgePred, mapEdgeSucc, listNodeOutput);
//		
//		for(Node nodeOutput : listNodeOutput){
//			taggedNodeList.add(nodeOutput);
//			for(Edge edgePred : mapEdgePred.get(nodeOutput)){
//				taggedEdgeList.add(edgePred);
//				this.addTaggedList(edgePred.getId_pred(), taggedNodeList, taggedEdgeList, mapEdgePred, mapEdgeSucc);
//			}
//		}
//				
//		_graph.getEdge().clear();
//		_graph.getEdge().addAll(taggedEdgeList);
//		_graph.getNode().clear();
//		_graph.getNode().addAll(taggedNodeList);
//	}
//	
//	/**
//	 * Add into the corresponding map the informations of successor/predecessor edge/node and which nodes are output of the system.
//	 * @param mapNodePred 
//	 * @param mapNodeSucc
//	 * @param listNodeOutput
//	 * @throws SFGGeneratorException
//	 */
//	private void createMapInterdependanceNodeEdge(Map<Node, List<Edge>> mapEdgePred, Map<Node, List<Edge>> mapEdgeSucc, List<Node> listNodeOutput) throws SFGGeneratorException{
//		List<Edge> listEdge;
//		Node node;
//
//		for(Edge edge : _graph.getEdge()){
//			// Successor management + output node test
//			node = edge.getId_succ();
//			myassert(node != null, "Error: An edge from the SFG generated have no successor node");
//			if(mapEdgePred.containsKey(node)){
//				listEdge = mapEdgePred.get(node);
//				listEdge.add(edge);
//			}
//			else{
//				listEdge = new ArrayList<Edge>();
//				listEdge.add(edge);
//				mapEdgePred.put(node, listEdge);
//			}
//			for(Annotations annot : node.getAttr()){
//				if(annot.getValue().equals("OUTPUT")){
//					listNodeOutput.add(node);
//				}
//			}
//			
//			/// Predecessor management 
//			node = edge.getId_pred();
//			myassert(node != null, "Error: An edge from the SFG generated have no predecessor node");
//			if(mapEdgeSucc.containsKey(node)){
//				listEdge = mapEdgeSucc.get(node);
//				listEdge.add(edge);
//			}
//			else{
//				listEdge = new ArrayList<Edge>();
//				listEdge.add(edge);
//				mapEdgeSucc.put(node, listEdge);
//			}
//		}
//	}
//	
//	/**
//	 * Cover the graph and add into node and edge list edges and nodes directly and indirectly connected to output nodes
//	 * @param node
//	 * @param taggedNodeList
//	 * @param taggedEdgeList
//	 * @param mapEdgePred
//	 * @param mapEdgeSucc
//	 * @throws SFGGeneratorException
//	 */
//	private void addTaggedList(Node node, List<Node> taggedNodeList, List<Edge> taggedEdgeList, Map<Node, List<Edge>> mapEdgePred,  Map<Node, List<Edge>> mapEdgeSucc) throws SFGGeneratorException{
//		Node nodePred;
//		
//		taggedNodeList.add(node);
//		if(mapEdgePred.containsKey(node)){
//			myassert(mapEdgePred.get(node) != null, "mapNodePred contains a key which have no predecessor");
//			for(Edge edgePred : mapEdgePred.get(node)){
//				taggedEdgeList.add(edgePred);
//				nodePred = edgePred.getId_pred();
//				if(taggedNodeList.contains(nodePred)){
//					continue;
//				}
//				else{
//					this.addTaggedList(nodePred, taggedNodeList, taggedEdgeList, mapEdgePred, mapEdgeSucc);
//				}
//			}
//		}
//	}
//	
//	private void updateInputOutputAffectationVar(Map<Node, List<Edge>> mapEdgePred,  Map<Node, List<Edge>> mapEdgeSucc) throws SFGGeneratorException{
//		Node nodePred, nodeSucc;
//		int numOpCDFG, numDataCDFG;
//		
//		for(Node node : _graph.getNode()){
//			for(Annotations annot : node.getAttr()){
//				if(annot.getKey().equals("NUM_DATA_CDFG")){
//					numDataCDFG = Integer.parseInt(annot.getValue());
//					if(mapEdgePred.containsKey(node)){
//						myassert(mapEdgePred.get(node).size() == 1, "Error: Data node have more one predecessor");
//						nodePred = mapEdgePred.get(node).get(0).getId_pred();
//						numOpCDFG = -2;
//						for(Annotations annotPred : nodePred.getAttr()){
//							if(annotPred.getKey().equals("NUM_OP_CDFG")){
//								numOpCDFG = Integer.parseInt(annotPred.getValue());
//								break;
//							}
//						}
//						myassert(numOpCDFG != -2, "Error: Data node predecessor is not a operator node or this operator node have not a NUM_OP_CDFG");
//						if(_opsOutputAffectingVar.containsKey(numDataCDFG)){
//							if(!_opsOutputAffectingVar.get(numDataCDFG).contains(numOpCDFG))
//								_opsOutputAffectingVar.get(numDataCDFG).add(numOpCDFG);
//						}
//						else{
//							List<Integer> listInputAffectingVar = new ArrayList<Integer>();
//							listInputAffectingVar.add(numOpCDFG);
//							_opsOutputAffectingVar.put(numDataCDFG, listInputAffectingVar);
//						}
//					}
//					
//					if(mapEdgeSucc.containsKey(node)){
//						for(Edge edge : mapEdgeSucc.get(node)){
//							nodeSucc = edge.getId_succ();
//							numOpCDFG = -2;
//							for(Annotations annotSucc : nodeSucc.getAttr()){
//								if(annotSucc.getKey().equals("NUM_OP_CDFG")){
//									numOpCDFG = Integer.parseInt(annotSucc.getValue());
//									break;
//								}
//							}
//							myassert(numOpCDFG != -2, "Error: Data node successor is not a operator node or this operator node have not a NUM_OP_CDFG");
//							if(_opsInputAffectingVar.containsKey(numDataCDFG)){
//								if(!_opsInputAffectingVar.get(numDataCDFG).contains(numOpCDFG))
//									_opsInputAffectingVar.get(numDataCDFG).add(numOpCDFG);
//							}
//							else{
//								List<Integer> listInputAffectationVar = new ArrayList<Integer>();
//								listInputAffectationVar.add(numOpCDFG);
//								_opsInputAffectingVar.put(numDataCDFG, listInputAffectationVar);
//							}
//						}
//					}
//				}
//			}
//		}
//	}
//	
//	private void renumberNodes(){
//		int num = 2;
//		for (Node node : _graph.getNode()){
//			node.setId(num);
//			num++;
//		}
//	}
//	
//	/**
//	 * Update the value annotation of the node and change node nature to VARIABLE_VAL_KNOWN
//	 * @param node
//	 * @param o
//	 * @throws SFGGeneratorException
//	 */
//	private void updateNodeValue(Node node, Object o) throws SFGGeneratorException{
//		String strValue;
//		if(o instanceof Long)
//			strValue = Long.toString((Long) o);
//		else if(o instanceof Double)
//			strValue = Double.toString((Double) o);
//		else
//			throw new SFGGeneratorException("The variable value " + node.getName() + " is not an integer/float value");
//		
//		updateNodeAnnotation(node, "VALUE", strValue);
//		updateNodeAnnotation(node, "NATURE", "VARIABLE_VAL_KNOWN");
//		
//	}
//	
//	/**
//	 * Change the {@link Annotations} key of the {@link Node} node to the new value provided
//	 * @param node
//	 * @param key
//	 * @param value
//	 * @throws SFGGeneratorException
//	 */
//	public void updateNodeAnnotation(Node node, String key, String value) throws SFGGeneratorException{
//		for (Annotations annotation : node.getAttr()){
//			if (annotation.getKey().equals(key)){
//				annotation.setValue(value);
//				return;
//			}
//		}
//		
//		node.getAttr().add(createAnnotation(key, value));
//	}
//	
//	public String getValueAnnotation(Node node, String key){
//		for(Annotations annotation : node.getAttr()){
//			if(annotation.getKey().equals(key)){
//				return annotation.getValue();
//			}
//		}
//		
//		return null;
//	}
//}
