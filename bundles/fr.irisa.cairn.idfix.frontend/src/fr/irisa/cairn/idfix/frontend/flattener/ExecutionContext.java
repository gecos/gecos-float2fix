package fr.irisa.cairn.idfix.frontend.flattener;

import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.ArrayValueInstruction;
import gecos.instrs.Instruction;
import gecos.types.ArrayType;
import gecos.types.FunctionType;
import gecos.types.Type;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Represents a program execution context
 * This code was started from the Interpreter tutorial
 * @author qmeunier
 */
public class ExecutionContext {

	/**
	 * One level of the stack represent one level of nested block. Each time the
	 * Flattener visits a block, a new Map is pushed on the top of the stack.
	 * When a function is called, a new stack is created, with the first level
	 * set with the globals variables of the ProcedureSet. The old stack is
	 * stored in a stack of stack : framestack.
	 */
	private Stack<Map<Symbol, SymbolValue>> stack;
	private Stack<Stack<Map<Symbol, SymbolValue>>> framestack;

	// ParamsTheo représente les paramètres théoriques lors d'un appel de fonction
	// On a besoin d'y accéder de manière indépendante de la pile car dans le cas d'un appel récursif,
	// on veut pouvoir effectuer une affectation d'une valeur (SymbolValue) à une autre pour deux symboles
	// identiques (si on rappelle la même fonction avec le même paramètre)
	// Du coup, on ne peut plus différencier dans la pile la valeur du symbole passé en paramètre
	// et celle du paramètre théorique
	// Par ailleurs, on est obligé d'avoir une Stack à cause des appels de fonctions imbriqués :
	// ex : f(1, f(a, 2, b), b); sinon à la sortie de l'appel interne on aura perdu les paramètres théoriques
	// correspondants à l'appel le plus externe
	private Stack<Map<Symbol, SymbolValue>> paramsTheo;
	
	/* The Flattener allows to evaluate the values of initialized variables */
	private ProcFlattener Flattener;
	
	private void myassert(boolean b, String s) throws FlattenerException{
		if (!b){
			throw new FlattenerException(s);
		}
	}
	
	
	public ExecutionContext() {
		stack = new Stack<Map<Symbol, SymbolValue>>();
		framestack = new Stack<Stack<Map<Symbol, SymbolValue>>>();
		paramsTheo = new Stack<Map<Symbol, SymbolValue>>();
	}

	public void setProcFlattener(ProcFlattener Flattener) {
		this.Flattener = Flattener;
	}
	
	
	
	/**
	 * Pour un niveau donné, recherche le symbole dans ce niveau correspondant à un symbole passé en paramètre (basé sur les noms : sémantique du langage)
	 * @param level le niveau donné du context
	 * @param var le symbol dont on cherche la correspondance
	 * @return le symbole correspondant, null s'il ne se trouve pas à ce niveau
	 */
	private Symbol getSymbolFromVarName(Map<Symbol, SymbolValue> level, Symbol var){
		for (Symbol sym : level.keySet()){
			if (sym.getName().equals(var.getName())){
				return sym;
			}
		}
		return null;
	}
	
	/**
	 * Permet de récupérer le symbole du context à partir d'un symbole passé en paramètre en effectuant la comparaison sur le nom
	 * @throws FlattenerException 
	 */
	public Symbol getCorrespondingSymbol(Symbol sym) throws FlattenerException{
		Symbol res = null;
		int top = stack.size() - 1;
		/* Browse the stack downwards (select the Symbol in the highest scope) */
		for (int i = top; i >= 0; i--) {
			Map<Symbol, SymbolValue> current = stack.get(i);
			res = getSymbolFromVarName(current, sym);
			if (res != null){
				return res;
			}
		}
		myassert(false, "");
		return null;
	}
	
	
	
	/**
	 * Calcule l'indice "à plat" d'un élément d'un tableau à une ou plusieurs dimensions
	 * L'indice de l'élément du tableau est calculé à partir de :
	 *     - sa définition pour connaitre la taille des autres dimensions
	 *     - la liste des indices
	 * exemple : int t[5][3];
	 * pour accéder t[3][2], on calcule un indice de 3*3 + 2*1 = 11 si le tableau est représenté à plat
	 * @param type : le type du tableau
	 * @param indices : les indices pour chaque dimension
	 * @return l'indice en considérant le tableau en entrée à plat
	 * @throws FlattenerException 
	 */
	public int getArrayIndex(Type type, ArrayList<Long> indices) throws FlattenerException{ 
		Type localType = type;
		ArrayType arrayType = (ArrayType) localType;
		myassert(arrayType.getNbDims() == indices.size(), "Le nombre d'indices passés dans getNbTotalElement " + indices.size() + " ne correspond pas à la dimension du tableau (" + arrayType.getNbDims() + ")");
		int ind = 0;
		int nbElemParDim = 1;
		for (int j = indices.size() - 1; j >= 0; j--){
			arrayType = (ArrayType) localType;
			ind += indices.get(j)*nbElemParDim;
//			nbElemParDim *= arrayType.getUpper() - arrayType.getLower() + 1;
			Instruction inst = arrayType.getSizeExpr();
			Object index = Flattener.doSwitch(inst);
			myassert((index instanceof Long), "L'index du tableau lors de la mise à plat des dimensions n'est pas un long");
			nbElemParDim *= (Long) index;
			localType = arrayType.getBase();
		}
		return ind;
	}
	
	/**
	 * Calcule le vecteur d'indices d'un élément d'un tableau à une ou plusieurs dimensions
	 * à partir de son indice à plat et du type du tableau
	 * @param type : le type du tableau
	 * @param indice : l'indice à plat que l'on veut factoriser
	 * @return un vecteur d'indice correspondant à l'indice à plat pour le type du tableau
	 * @throws FlattenerException 
	 */
	public ArrayList<Long> getArrayIndexes(Type type, Long indice) throws FlattenerException{
		Type localType = type;
		ArrayType arrayType;
		ArrayList<Long> indices = new ArrayList<Long>();
		Long currIndice = indice;
		Long indiceDim;
		Long nbElemDim;
		while (localType instanceof ArrayType){
			arrayType = (ArrayType) localType;
			nbElemDim = arrayType.getUpper() - arrayType.getLower() + 1;
			indiceDim = currIndice % nbElemDim;
			currIndice -= indiceDim;
			currIndice = currIndice / nbElemDim;
			localType = arrayType.getBase();
			indices.add(indiceDim);
		}
		myassert(((ArrayType) type).getNbDims() == indices.size(), "Number of Dimensions: " + ((ArrayType) type).getNbDims() + " - indices.size(): " + indices.size());
		ArrayList<Long> res = new ArrayList<Long>();
		for (int i = indices.size() - 1; i >= 0; i--){
			res.add(indices.get(i));
		}
		return res;
	}
	
	
	
	/**
	 * Gecos ne traite pas les initialisations de tableaux à plusieurs dimensions qui ne suivent pas exactement sa définition :
	 * int t[2][2] = { 0, 1, 2, 3 } n'est pas valide pour Gecos...
	 * De plus, les valeurs d'initialisation à plusieurs dimensions sont laissées sous forme de "Valeurs de valeurs"
	 * On a donc besoin de les applatir pour correspondre à notre représentation de tableau à une dimension
	 * @param inst : l'instruction d'initialisation
	 * @param v : le vecteur que l'en enrichit avec les valeurs qu'on lit
	 */
	private void flattenArrayInitValues(Instruction inst, ArrayList<Object> v){
		if (inst instanceof ArrayValueInstruction){
			ArrayValueInstruction insts = (ArrayValueInstruction) inst;
			for (Instruction i : insts.getChildren()){
				flattenArrayInitValues(i,v);
			}
		}
		else {
			Object value = this.Flattener.doSwitch(inst);
			v.add(value);
		}
	}


	/**
	 * Test si toutes les valeurs d'un tableau sont initialisées
	 * @param array : le tableau à tester
	 * @return vrai ssi toutes les valeurs du tableaux sont initialisées
	 * @throws FlattenerException 
	 */
	public boolean arrayIsFullyInitialized(Symbol array) throws FlattenerException{
		myassert(array.getType() instanceof ArrayType, "");
		for (int i = stack.size() - 1; i >= 0; i--) {
			Map<Symbol, SymbolValue> current = stack.get(i);
			if (current.containsKey(array)) {
				for (Integer idx = 0; idx < current.get(array).getNbValues(); idx++){
					if (current.get(array).getValueAt(idx) instanceof UninitializedValue){
						return false;
					}
				}
				return true;
			}
		}
		myassert(false, "");
		return false;
	}
	
	
	/**
	 * Met à jour la valeur d'un symbole dans la représentation intermédiaire
	 * @param sym : le symbole à mettre à jour
	 * @param newValue : la nouvelle valeur
	 */
	public void setValue(Symbol sym, Object newValue) {
		setValue(sym, newValue, false);
	}
	
	public void setValue(Symbol sym, Object newValue, boolean destIsParamTheo) {
		if (destIsParamTheo){
			if (paramsTheo.peek().containsKey(sym)) {
				paramsTheo.peek().get(sym).setValueAt(newValue, 0);
				return;
			}
		}
		else {
			int top = stack.size() - 1;
			/* We browse the stack downwards (select the Symbol in the highest scope) */
			for (int i = top; i >= 0; i--) {
				Map<Symbol, SymbolValue> current = stack.get(i);
				if (current.containsKey(sym)) {
					//System.out.println("Maj du symbole " + sym + " (valeur " + newValue + ")");
					current.get(sym).setValueAt(newValue, 0);
					return;
				}
			}
		}
		throw new RuntimeException("*** Error : Symbol " + sym.getName() + " not found.");
	}
	
	/**
	 * Met à jour la valeur d'un symbole dans la représentation intermédiaire, quand il s'agit d'un symbole de tableau
	 * @param sym : le symbole à mettre à jour
	 * @param newValue : la nouvelle valeur
	 * @param indices : la liste des indices de l'élément du tableau à mettre à jour
	 * @throws FlattenerException 
	 */
	public void setValue(Symbol sym, Object newValue, ArrayList<Long> indices) throws FlattenerException {
		setValue(sym, newValue, indices, false);
	}
	
	public void setValue(Symbol sym, Object newValue, ArrayList<Long> indices, boolean destIsParamTheo) throws FlattenerException {
		if (destIsParamTheo){
			if (paramsTheo.peek().containsKey(sym)) {
				int ind = getArrayIndex(sym.getType(), indices);
				paramsTheo.peek().get(sym).setValueAt(newValue, ind);
				return;
			}
		}
		else {
			int top = stack.size() - 1;
			/* We browse the stack downwards (select the Symbol in the highest scope) */
			for (int i = top; i >= 0; i--) {
				Map<Symbol, SymbolValue> current = stack.get(i);
				if (current.containsKey(sym)) {
					int ind = getArrayIndex(sym.getType(), indices);
					current.get(sym).setValueAt(newValue, ind);
					return;
				}
			}
		}
		throw new RuntimeException("*** Error : Symbol " + sym.getName() + " not found.");
	}

	
	/**
	 * Retourne la valeur d'un symbole dans la représentation intermédiaire 
	 * @param sym : le symbole
	 * @return sa valeur
	 * @throws FlattenerException 
	 */
	public Object getValue(Symbol sym) throws FlattenerException {
		return getValue(stack, sym);
	}
	
	public Object getValue(Stack<Map<Symbol, SymbolValue>> theStack, Symbol sym) throws FlattenerException {
		Object value = null;
		int top = theStack.size() - 1;
		/* Browse the stack downwards (select the Symbol in the highest scope) */
		for (int i = top; i >= 0; i--) {
			Map<Symbol, SymbolValue> current = theStack.get(i);
			if (current.containsKey(sym)) {
				value = theStack.get(i).get(sym).getValueAt(0);
				myassert(value != null, "");
				return value;
			}
		}
		throw new RuntimeException("*** Error : Symbol " + sym.getName() + " not found.");
	}

	
	/**
	 * Retourne la valeur d'un symbole de type tableau dans la représentation intermédiaire 
	 * @param sym : le symbole
	 * @param indices : la liste des indices de l'élément du tableau
	 * @return la valeur du symbole
	 * @throws FlattenerException 
	 */
	public Object getValue(Symbol sym, ArrayList<Long> indices) throws FlattenerException {
		return getValue(stack, sym, indices);
	}
	
	public Object getValue(Stack<Map<Symbol, SymbolValue>> theStack, Symbol sym, ArrayList<Long> indices) throws FlattenerException {
		Object value = null;
		int top = theStack.size() - 1;
		/* Browse the stack downwards (selects the Symbol in the highest scope) */
		for (int i = top; i >= 0; i--) {
			Map<Symbol, SymbolValue> current = theStack.get(i);
			if (current.containsKey(sym)) {
				int ind = getArrayIndex(sym.getType(), indices);
				value = current.get(sym).getValueAt(ind);
				myassert(value != null, "");
				return value;
			}
		}
		throw new RuntimeException("*** Error : Symbol " + sym.getName() + " not found (indices : " + indices + ")");
	}
	
	/**
	 * Retourne la symbolValue associée à une variable
	 * @param symbol
	 * @return The SymbolValue associated to a symbol
	 * @throws FlattenerException 
	 */
	public SymbolValue getSymbolValue(Symbol symbol) throws FlattenerException{
		SymbolValue res = null;
		int top = stack.size() - 1;
		for (int i = top; i >= 0; i--) {
			Map<Symbol, SymbolValue> current = stack.get(i);
			if (current.containsKey(symbol)) {
				res = stack.get(i).get(symbol);
				if (res.getValueAt(0) != null){
					return res;
				}
			}
		}
		myassert(false, "*** Error : Symbol " + symbol.getName() + " not found.");
		return null;
	}
	
	
	/**
	 * Sets a symbolValue to a given symbol
	 * @param symbol
	 * @throws FlattenerException 
	 */
	public void setSymbolValue(Symbol symbol, SymbolValue newValue) throws FlattenerException{
		int top = stack.size() - 1;
		boolean found = false;
		/* Browse the stack downwards (select the Symbol in the highest scope) */
		for (int i = top; i >= 0; i--) {
			Map<Symbol, SymbolValue> current = stack.get(i);
			if (current.containsKey(symbol)) {
				found = true;
				stack.get(i).put(symbol, newValue);
			}
		}
		myassert(found, "*** Error : Symbol " + symbol.getName() + " not found.");
	}
	
	
	/**
	 * Pops the current scope
	 */
	public void pop() {
		this.stack.pop();
	}

	
	/**
	 * Pushes a new scope (C block)
	 * If the variables are initialized, their values are stored
	 * @param scope : the scope to push
	 * @throws FlattenerException 
	 */
	public void push(Scope scope) throws FlattenerException {
		Map<Symbol, SymbolValue> newLevel = new LinkedHashMap<Symbol, SymbolValue>();
		stack.push(newLevel);
		for(Symbol s : scope.getSymbols()){
			if(s.getType().asBase() != null){
				pushScalarSymbol(s, newLevel);
			}
			else if(s.getType().asArray() != null){
				pushArraySymbol(s, newLevel);
			}
			else if(!(s.getType() instanceof FunctionType)){
//				System.out.println(s);
				throw new FlattenerException("Kind of symbol type not managed by the flattener: " + s);
			}
		}
		
//		HashMap<Symbol, SymbolValue> newLevel =  initMapSymbols(scope);
//		this.stack.push(newLevel);
	}
	
	private void pushScalarSymbol(Symbol s, Map<Symbol, SymbolValue> newLevel){
		ArrayList<Object> initList = new ArrayList<Object>();
		if(s.getValue() != null){
			Object value = this.Flattener.doSwitch(s.getValue());
			initList.add(value);
		}
		else{
			initList.add(null);
		}
		newLevel.put(s, new SymbolValue(initList));
	}
	
	private void pushArraySymbol(Symbol s, Map<Symbol, SymbolValue> newLevel) throws FlattenerException{
		Instruction sizeInst;
		Object index;
		ArrayType arrType;
		
		ArrayList<Object> initList = new ArrayList<Object>();
		int size = 1;
		Type type;
		for(type = s.getType() ; type instanceof ArrayType ; type = ((ArrayType) type).getBase()){
			arrType = (ArrayType) type;
			sizeInst = arrType.getSizeExpr();
			index = Flattener.doSwitch(sizeInst);
			myassert((index instanceof Long), "Index du tableau lors de sa création dans le contexte d'éxécution n'est pas un long");
					
			size *= (Long) index;			
		}
		if (s.getValue() == null){
			for (int i = 0; i < size; i++){
				initList.add(new UninitializedValue());
			}
		}
		else {
			flattenArrayInitValues(s.getValue(),initList);
			for(int i=initList.size(); i<size; i++)   //in case not all values are specified
				initList.add(new Long(0));	//XXX type long? shall it be UninitializedValue?
		}
		newLevel.put(s, new SymbolValue(initList));
	}
	
	Map<Symbol, SymbolValue> initMapSymbols(Scope scope) throws FlattenerException{
		Map<Symbol, SymbolValue> res = new LinkedHashMap<Symbol, SymbolValue>(); 
		for (Symbol sym : scope.getSymbols()) {
			Instruction initValue = sym.getValue();
			Type t = sym.getType();
			ArrayList<Object> initVector = new ArrayList<Object>();
			if (t instanceof ArrayType){
				/* On calcule le nombre total d'éléments dans le tableau */
				ArrayType arrayType = (ArrayType) t;
				int nbElemTotal = 1;
				for (int j = 0; j < arrayType.getNbDims(); j++){
					arrayType = (ArrayType) t;
					//nbElemTotal *= arrayType.getUpper() - arrayType.getLower() + 1;
					Instruction inst = arrayType.getSizeExpr();
					Object index = Flattener.doSwitch(inst);
					myassert((index instanceof Long), "Index du tableau lors de sa création dans le contexte d'éxécution n'est pas un long");
					nbElemTotal *= (Long)index;
					t = arrayType.getBase();
				}
				//System.out.println("Nombre total d'éléments trouvés pour le tableau " + sym.getName() + " : " + nbElemTotal);
				if (initValue == null){
					for (int i = 0; i < nbElemTotal; i++){
						initVector.add(new UninitializedValue());
					}
				}
				else {
					flattenArrayInitValues(initValue,initVector);
				}
				res.put(sym, new SymbolValue(initVector));
			}
			else {
				if (initValue != null) {
					// If the Symbol is initialized, its value has to be evaluated. 
					Object value = this.Flattener.doSwitch(initValue);
					initVector.add(value);
				}
				else {
					initVector.add(null);
				}
				res.put(sym, new SymbolValue(initVector));
			}
		}
		return res;
	}
	
	

	public void initParamsTheo(){
		Map<Symbol, SymbolValue> newParams = new LinkedHashMap<Symbol, SymbolValue>();
		paramsTheo.push(newParams);
	}
	
	public Map<Symbol, SymbolValue> getParamsTheo(){
		return paramsTheo.peek();
	}
	

	/**
	 * Assumes that current().peek() contains the declaration and values of
	 * function's parameters.
	 */
	@SuppressWarnings("unchecked")
	public void pushFrame() {
		/* The first step consists in storing the current context in the
		 * framestack. Then a new context has to be created for the procedure
		 * call. Finally, the new context is initialized with the global
		 * variables and the procedure's parameters */
		framestack.push((Stack<Map<Symbol, SymbolValue>>) stack.clone());
		stack = new Stack<Map<Symbol, SymbolValue>>();
		stack.push(framestack.peek().get(0));
		stack.push(paramsTheo.peek());
	}

	
	public void popFrame() {
		/* When poping a frame, the operation consists in restoring the last
		 * context, and in updating the global variables with the values from the
		 * procedure's context */
		Stack<Map<Symbol, SymbolValue>> oldstack = (Stack<Map<Symbol, SymbolValue>>) framestack.pop();
		oldstack.set(0, stack.get(0));
		stack = oldstack;
		paramsTheo.pop();
	}
	
	
	@SuppressWarnings("unchecked")
	public Stack<Map<Symbol, SymbolValue>> cloneCurrValues(){
		Stack<Map<Symbol, SymbolValue>> res = new Stack<Map<Symbol, SymbolValue>>();
		for (int i = 0; i < stack.size(); i++){
			LinkedHashMap<Symbol, SymbolValue> current = (LinkedHashMap<Symbol, SymbolValue>) stack.get(i);
			Map<Symbol, SymbolValue> currMap = (Map<Symbol, SymbolValue>) current.clone();
			for (Symbol s : currMap.keySet()){
				currMap.put(s, currMap.get(s).copy());
			}
			res.add(currMap);
		
		}
		return res;
	}
	
	public void setCurrValues(Stack<Map<Symbol, SymbolValue>> newStack){
		this.stack = newStack;
	}
	
	
	/**
	 * Merges the current context values with the nodes present in the savedValues parameter, setting
	 * the value of merged variables to Uninitialized
	 * @param savedValues the values to merge with the current context
	 * @throws FlattenerException 
	 */
	public void mergeValues(Stack<Map<Symbol, SymbolValue>> origValues, Stack<Map<Symbol, SymbolValue>> thenValues, Stack<Map<Symbol, SymbolValue>> elseValues) throws FlattenerException{
		myassert(origValues.size() == thenValues.size() && (elseValues == null || thenValues.size() == elseValues.size()), "orig : " + origValues.size() + " - then : " + thenValues.size());
		for (int i = origValues.size() - 1; i >= 0; i--) {
			Map<Symbol, SymbolValue> oldMap = origValues.get(i);
			Map<Symbol, SymbolValue> thenMap = thenValues.get(i);
			Map<Symbol, SymbolValue> elseMap = null;
			if (elseValues != null){
				elseMap = elseValues.get(i);
			}
			for (Symbol s : oldMap.keySet()) {
				myassert(thenMap.containsKey(s), "");
				myassert(elseMap == null || elseMap.containsKey(s), "");
				if (s.getType() instanceof ArrayType){
					// On regarde tous les éléments du tableau
					for (long index = 0; index < oldMap.get(s).getNbValues(); index++){
						ArrayList<Long> indices = getArrayIndexes(s.getType(),index);
						//System.out.println("Symbol " + s.getName() + " - old val : " + oldMap.get(s).getValueAt(0) + " - then val : " + thenMap.get(s).getValueAt(0) + " - else val : " + elseMap.get(s).getValueAt(0));
						if ((thenMap.get(s).getValueAt(index) != oldMap.get(s).getValueAt(index)) ||
							(elseMap != null && elseMap.get(s).getValueAt(index) != oldMap.get(s).getValueAt(index))){
							setValue(s, new UninitializedValue(), indices);
						}
					}
				}
				else {
					if ((thenMap.get(s).getValueAt(0) != oldMap.get(s).getValueAt(0)) ||
					    (elseMap != null && elseMap.get(s).getValueAt(0) != oldMap.get(s).getValueAt(0))){
						setValue(s, new UninitializedValue());
					}
				}
			}
		}
	}
	
	
	
	public String toString() {
		StringBuffer res = new StringBuffer();
		int top = stack.size() - 1;
		for (int i = top; i >= 0; i--) {
			res.append("level " + i + "\n");
			Map<Symbol, SymbolValue> current = stack.get(i);
			for (Symbol s : current.keySet()) {
				String str = s.getName() + " (" + Integer.toHexString(s.hashCode()) + ") = " + current.get(s) + " : " + s.getType();
				res.append("\t" + str + "\n");
			}
		}
		return res.toString();
	}

}
