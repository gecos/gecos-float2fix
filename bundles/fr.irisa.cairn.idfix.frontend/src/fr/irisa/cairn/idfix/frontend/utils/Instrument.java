
package fr.irisa.cairn.idfix.frontend.utils;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory;
import fr.irisa.cairn.gecos.model.tools.switches.BasicBlockSwitch;
import fr.irisa.cairn.idfix.utils.IDFixUtils;
import fr.irisa.cairn.idfix.utils.exceptions.SFGModelCreationException;
import gecos.annotations.StringAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.BlocksFactory;
import gecos.blocks.CompositeBlock;
import gecos.blocks.IfBlock;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.CallInstruction;
import gecos.instrs.InstrsFactory;
import gecos.instrs.Instruction;
import gecos.instrs.IntInstruction;
import gecos.instrs.SetInstruction;
import gecos.instrs.SymbolInstruction;
import gecos.types.FunctionType;

/**
 * Effetue l'ajout des instructions d'instrumentations : appel à une fonction spécifique au début de chaque basic_block,
 * et avant chaque affectation => Cela est fait sur une copie de la RI qui est regénérée dans un fichier.
 * @author qmeunier
 */

@SuppressWarnings("rawtypes")
public class Instrument extends BasicBlockSwitch {
	boolean _debug = false;
	private final ProcedureSet _ps;
	private ProcedureSet _psCopy; // On rajoute des instructions pour regénérer le code en vue de la simulation
	private ProcedureSymbol _blockTraceProcedureSymbol;
	private ProcedureSymbol _affectValuesprocedureSymbol;
		
	public Instrument(ProcedureSet procedureSet, String outputDirPath){
		_ps = procedureSet;
		_blockTraceProcedureSymbol = null;
		_affectValuesprocedureSymbol = null;
	}
	
	public ProcedureSet compute() throws SFGModelCreationException{
		if (_ps != null) {
			_psCopy = _ps.copy();
			
			GecosUserTypeFactory.setScope(_psCopy.getScope());
			List<ParameterSymbol> params = new ArrayList<ParameterSymbol>();
			params.add(GecosUserCoreFactory.paramSymbol("a", GecosUserTypeFactory.INT()));
			_blockTraceProcedureSymbol = GecosUserCoreFactory.procSymbol("passInBlock", GecosUserTypeFactory.VOID(), params);
			
			params = new ArrayList<ParameterSymbol>();
			params.add(GecosUserCoreFactory.paramSymbol("affect", GecosUserTypeFactory.INT()));
			params.add(GecosUserCoreFactory.paramSymbol("value", GecosUserTypeFactory.DOUBLE()));
			_affectValuesprocedureSymbol = GecosUserCoreFactory.procSymbol("traceAffectationValue", GecosUserTypeFactory.VOID(), params);
			
			// On effectue le switch sur la copie
			for (Procedure proc : _psCopy.listProcedures()) {
				doSwitch(proc.getBody());
			}
			
			return _psCopy;
		}
		else{
			throw new SFGModelCreationException("ProcedureSet is null");
		}
	}
	
	@Override
	public Object caseCompositeBlock(CompositeBlock cb){
		Symbol symbolCopy;
		FunctionType functionType;
		Scope scope = GecosUserCoreFactory.scope(); // Nouveau scope où seront contenu les symbols n'ayant pas une annotation de delay
		
		Procedure proc = cb.getContainingProcedure();
		
		for(Symbol sym : cb.getScope().getSymbols()){
			symbolCopy = EcoreUtil.copy(sym);
			if((StringAnnotation) sym.getAnnotation("VAL_MAX") != null/* || (StringAnnotation) sym.getAnnotation("DELAY") != null */){
				proc.getScope().getSymbols().add(symbolCopy);
				functionType = (FunctionType) proc.getSymbol().getType();
				functionType.addParameter(symbolCopy.getType());
			}
			else{
				scope.getSymbols().add(symbolCopy);
			}
		}
		
		// Remplacement de l'ancien scope par la nouvelle dans le compositeBlock courant
		cb.getScope().getSymbols().clear();
		for(Symbol sym : scope.getSymbols()){
			cb.getScope().getSymbols().add(sym.copy());
		}
	
		// Récursivité sur les blocks fils
		for (Block block : cb.getChildren()) {
			doSwitch(block);
		}

		return null;
	}
	
	
	@Override
	public Object caseBasicBlock(BasicBlock b) {
		CallInstruction inst = InstrsFactory.eINSTANCE.createCallInstruction();
		SymbolInstruction symbolInstruction = InstrsFactory.eINSTANCE.createSymbolInstruction();
		symbolInstruction.setSymbol(_blockTraceProcedureSymbol);
		inst.setAddress(symbolInstruction);
		IntInstruction param = InstrsFactory.eINSTANCE.createIntInstruction();
		try {
			param.setValue(Integer.parseInt(IDFixUtils.getBasicBlockNumber(b)));
		} catch (NumberFormatException e) {
			throw new RuntimeException(e);
		} catch (SFGModelCreationException e) {
			throw new RuntimeException(e);
		}
		inst.addArg(param);
		b.getInstructions().add(0, inst);
		
		BasicBlock instrumBlock = BlocksFactory.eINSTANCE.createBasicBlock(); // Block temporaire où l'on met toutes les instructions originales et créées avant de le recopier
		for (Instruction i : b.getInstructions()){
			instrumBlock.addInstruction(i.copy());
			if (i instanceof SetInstruction){
				CallInstruction affectInst = InstrsFactory.eINSTANCE.createCallInstruction();
				SymbolInstruction affectSymbolInstruction = InstrsFactory.eINSTANCE.createSymbolInstruction();
				affectSymbolInstruction.setSymbol(_affectValuesprocedureSymbol);
				affectInst.setAddress(affectSymbolInstruction);
				
				IntInstruction affectParam1 = InstrsFactory.eINSTANCE.createIntInstruction();
				Integer numAffect = IDFixUtils.getNumAffectation(((SetInstruction) i).getDest());
				affectParam1.setValue(numAffect);
				affectInst.addArg(affectParam1);
				
				Instruction affectParam2 = ((SetInstruction) i).getDest().copy();
				affectInst.addArg(affectParam2);

				instrumBlock.addInstruction(affectInst);
			}
		}
		b.getInstructions().clear();
		for (Instruction i : instrumBlock.getInstructions()){
			b.addInstruction(i.copy());
		}
		
		return null;
	}
	
	@Override
	public Object caseIfBlock (IfBlock b){
		// On ne veut pas visiter le basic block de la condition (non annoté)
		doSwitch(b.getThenBlock());
		if (b.getElseBlock() != null){
			doSwitch(b.getElseBlock());
		}
		return null;
	}
	
	
}