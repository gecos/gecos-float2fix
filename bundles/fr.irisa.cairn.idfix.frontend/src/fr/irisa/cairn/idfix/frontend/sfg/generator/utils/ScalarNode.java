//package fr.irisa.cairn.idfix.frontend.sfg.generator.utils;
//
//import float2fix.Node;
//
///**
// * Represent data structure for a scalar variable.
// * @author Nicolas Simon
// * @date Jul 19, 2013
// */
//public class ScalarNode implements INodeType {
//	private Node _node;
//	
//	
//	public ScalarNode(Node s){
//		_node = s;
//	}
//
//	public Node getNode() {
//		return _node;
//	}
//	
//	public void setNode(Node n){
//		_node = n;
//	}
//
//	@Override
//	public String toString(){
//		return "ScalarNode@(" + _node + ")";
//	}
//}
