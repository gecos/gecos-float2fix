package fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils;

import fr.irisa.cairn.gecos.model.modules.GetProcedureSet;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.Flattener;
import fr.irisa.cairn.idfix.utils.exceptions.FlattenerException;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;
import gecos.gecosproject.GecosSourceFile;

public class TestFlatten {
	private GecosProject _p;
	
	public TestFlatten(GecosProject p){
		_p = p;
	}
	
	public void compute() throws FlattenerException{
		for(GecosSourceFile system : _p.getSources()){	
			
			GetProcedureSet getProcedureSet = new GetProcedureSet(system);
			ProcedureSet ps = getProcedureSet.compute();
			Flattener fl = new Flattener(ps);
			fl.compute();
			System.out.println("");
		}
	}
}
