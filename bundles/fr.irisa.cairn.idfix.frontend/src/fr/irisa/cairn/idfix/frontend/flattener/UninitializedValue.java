package fr.irisa.cairn.idfix.frontend.flattener;

/**
 * Object representing an uninitialized value in the interpretation (we cannot use null with switches)
 * @author qmeunier
 */
class UninitializedValue {
	
	public UninitializedValue(){}
	
}
