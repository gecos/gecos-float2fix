//package fr.irisa.cairn.idfix.frontend.sfg.generator.utils;
//
//import java.util.Vector;
//
//public class IndexedNode <T extends INodeType> implements INodeType {
//	private Vector<T> _nodes;
//	private int _size;
//	
//	public IndexedNode(int size){
//		_nodes = new Vector<T>(size);
//		for(int i = 0 ; i < size ; i++){
//			_nodes.add(null);
//		}
//		_size = size;
//	}
//	
//	public T getNodeType(int index){
//		if(index >= _size)
//			throw new RuntimeException("Error: index " + index + " is greater than array size");
//
//		return _nodes.get(index);
//	}
//	
//	public void setNodeType(int index, T node){
//		if(index >= _size)
//			throw new RuntimeException("Error: index " + index + " is greater than array size");
//		
//		_nodes.set(index, node);
//	}
//	
//	public String toString(){
//		String ret = "IndexedNode@(";
//		for(int i = 1 ; i < _size ; i++){
//			ret += i + ":" + _nodes.get(i).toString() + ", ";
//		}
//		ret += _size - 1 + ":" + _nodes.get(_size - 1).toString() + ")";
//		return ret;
//	}
//}
