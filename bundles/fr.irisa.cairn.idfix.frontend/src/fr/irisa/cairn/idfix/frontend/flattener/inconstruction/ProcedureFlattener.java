package fr.irisa.cairn.idfix.frontend.flattener.inconstruction;

import java.util.Map;

import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils.Context;
import fr.irisa.cairn.idfix.utils.exceptions.FlattenerException;
import gecos.blocks.CompositeBlock;
import gecos.core.Procedure;

/**
 * Procedure switch
 * @author Nicolas Simon
 * @date Jul 3, 2013
 */
public class ProcedureFlattener {
	private Context _context;
	private Map<Procedure, BlockManager> _manager;
	private int _nbFunctionCall;
	
	public ProcedureFlattener(Context context, Map<Procedure, BlockManager> manager){
		_context = context;
		_manager = manager;
		_nbFunctionCall = 0;
	}
	
	public int useIndexFunction(){
		return _nbFunctionCall++;
	}
	
	public Object doSwitch(Procedure p){
		try{
			// Create a specific block manager for the procedure p
			// We can reached only once time a procedure because each time we reach the original procedure, we create a new one
			BlockManager blockManager = _manager.get(p);
			if(blockManager != null)
				throw new FlattenerException("Error: Block manager for procedure " + p.getSymbol().getName() + " already created " );
			
			blockManager = new BlockManager(this, _context.getGlobalScope(), p.getScope());
			_manager.put(p, blockManager);
			
			//FIXME Vraiment utile? (Si on teste dans le block manager si un main block est deja present
			// lorsque nous attaquons le body de la fonction, nous pouvons le créer à ce moment là)
			CompositeBlock mainBlock = GecosUserBlockFactory.CompositeBlock(GecosUserCoreFactory.scope());
			try {
				blockManager.setMainBlock(mainBlock);
				blockManager.setCurrentCompositeBlock(mainBlock);
			} catch (FlattenerException e) {
				throw new RuntimeException(e);
			}
			BlockFlattener blockFlattener = new BlockFlattener(_context, blockManager);
			blockFlattener.doSwitch(p.getBody());
			
			
			//FIXME A améliorer
			blockManager.mergeBasicBlocksWithoutScope();
//			for(Block b :blockManager.getMainBlock().getChildren() )
//				blockManager.getMainBlock().mergeChildComposite((CompositeBlock) b);
			
			//FIXME retourner le résutlat de la fonction (retInstruction)?
			return null;
		} catch (FlattenerException e){
			throw new RuntimeException(e);
		}
	}
}
