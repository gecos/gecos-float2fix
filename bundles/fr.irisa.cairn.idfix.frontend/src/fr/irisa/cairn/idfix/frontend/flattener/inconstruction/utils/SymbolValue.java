package fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils;

/**
 * Represent data structure for a scalar value.
 * @author Nicolas Simon
 * @date Jun 28, 2013
 */
public class SymbolValue implements ISymbolType {
	private Object _value;
	
	public SymbolValue(Object value){
		_value = value;
	}
	
	public SymbolValue(SymbolValue other){
		_value = other._value;
	}
	
	public void setValue(Object value){
		_value = value;
	}
	
	public Object getValue(){
		return _value;
	}
	
	public SymbolValue copy(){
		return new SymbolValue(this);
	}
	
	public String toString(){
		return "SymbolScalar@(" + _value + ")";
	}
}
