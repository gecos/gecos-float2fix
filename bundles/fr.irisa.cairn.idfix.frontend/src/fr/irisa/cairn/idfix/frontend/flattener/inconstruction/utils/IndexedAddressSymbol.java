package fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils;

import gecos.core.Symbol;

public class IndexedAddressSymbol {
private Symbol _symbol;
	
	public IndexedAddressSymbol(Symbol symbol){
		_symbol = symbol;
	}
	
	public Symbol getSymbol(){
		return _symbol;
	}
	
	public String toString(){
		return _symbol.getName() + ":" + _symbol.getType();
	}
}
