package fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils;

import fr.irisa.cairn.gecos.model.tools.switches.DefaultInstructionSwitch;
import fr.irisa.cairn.idfix.utils.exceptions.FlattenerException;
import gecos.core.Symbol;
import gecos.instrs.CallInstruction;
import gecos.instrs.Instruction;
import gecos.instrs.SymbolInstruction;

/**
 * Replace instruction SymbolInstruction Symbol by the equivalent symbol contained into the ScopeContext 
 * @author Nicolas Simon
 * @date Jul 3, 2013
 */
public class SymbolReplacer extends DefaultInstructionSwitch<Object> {
	private ScopeContext _context;
	
	public SymbolReplacer(ScopeContext context){
		_context = context;
	}
	
	@Override
	public Object caseCallInstruction(CallInstruction g) {
		for (Instruction inst : g.getArgs()){
			doSwitch(inst);
		}
		return null;
	}
	
	@Override
	public Object caseSymbolInstruction(SymbolInstruction g){
		try{
			Symbol corrSym = _context.getCorrespondingSymbol(g.getSymbolName());
			g.setSymbol(corrSym);
			return null;
		} catch(FlattenerException e){
			throw new RuntimeException(e);
		}
	}
	
	
}
