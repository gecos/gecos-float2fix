//package fr.irisa.cairn.idfix.frontend.sfg.generator;
//
//import float2fix.Node;
//import fr.irisa.cairn.gecos.model.tools.switches.BasicBlockSwitch;
//import fr.irisa.cairn.idfix.frontend.sfg.generator.utils.INodeType;
//import fr.irisa.cairn.idfix.utils.exceptions.SFGGeneratorException;
//import fr.irisa.cairn.idfix.utils.exceptions.SFGModelCreationException;
//import gecos.blocks.BasicBlock;
//import gecos.blocks.Block;
//import gecos.blocks.CompositeBlock;
//import gecos.blocks.ForBlock;
//import gecos.blocks.IfBlock;
//import gecos.blocks.LoopBlock;
//import gecos.blocks.WhileBlock;
//import gecos.core.Scope;
//import gecos.instrs.Instruction;
//import gecos.instrs.RetInstruction;
//
///**
// * Block switch
// * @author Nicolas Simon
// * @date Jul 19, 2013
// */
//public class BlockGenerator extends BasicBlockSwitch<INodeType> {
//	private SFGFactory _factory;
//	private InstructionGenerator _instGenerator;
//	
//	public BlockGenerator(SFGFactory factory, boolean interpretSetSourceKnownValue){
//		_factory = factory;
//		_instGenerator = new InstructionGenerator(_factory, interpretSetSourceKnownValue);
//	}
//	
//	/**
//	 * The return value is the {@link Node} return by a {@link RetInstruction}
//	 */
//	@Override
//	public INodeType caseBasicBlock(BasicBlock b) {
//		INodeType ret = null;
//		for(Instruction inst : b.getInstructions()){
//			ret = _instGenerator.doSwitch(inst);
//		}
//		return ret;
//	}
//
//	/**
//	 * When visiting a {@link CompositeBlock}, you need generate new {@link Node} due to the new {@link Scope} of the {@link CompositeBlock}
//	 * The return value is the {@link Node} return by a {@link RetInstruction}
//	 */
//	@Override
//	public INodeType caseCompositeBlock(CompositeBlock b) {
//		try {
//			_factory.generateVariableNode(b.getScope());
//		} catch (SFGGeneratorException | SFGModelCreationException e) {
//			throw new RuntimeException(e);
//		}
//		INodeType ret = null;
//		for(Block block : b.getChildren()){
//			ret = doSwitch(block);
//			if(ret != null){
//				return ret;
//			}
//		}
//		return null;
//	}
//
//	@Override
//	public INodeType caseIfBlock(IfBlock b) {
//		throw new RuntimeException("Error: The SFG generation work from a flatten source code, but here a If block have been found, doesn't it?");
//	}
//
//	@Override
//	public INodeType caseLoopBlock(LoopBlock b) {
//		throw new RuntimeException("Error: The SFG generation work from a flatten source code, but here a Loop block have been found, doesn't it?");
//	}
//
//	@Override
//	public INodeType caseWhileBlock(WhileBlock b) {
//		throw new RuntimeException("Error: The SFG generation work from a flatten source code, but here a While block have been found, doesn't it?");
//	}
//
//	@Override
//	public INodeType caseForBlock(ForBlock b) {
//		throw new RuntimeException("Error: The SFG generation work from a flatten source code, but here a For block have been found, doesn't it?");
//	}
//
//}
