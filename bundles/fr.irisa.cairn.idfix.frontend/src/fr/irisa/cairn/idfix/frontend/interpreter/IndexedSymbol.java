package fr.irisa.cairn.idfix.frontend.interpreter;

import gecos.core.Symbol;

import java.util.Vector;

/**
 * @author qmeunier
 * Represents a variable of type "array" fully indexed.
 * Ex : "int t[10][15] ; t[5]" is not fully indexed while "int t[10][15]; t[5][4]" is fully indexed
 * Contains the symbol of the variable and the list of indexes to index the array
 */

public class IndexedSymbol {
	private Symbol symbol;
	private Vector<Long> indices;
	
	@SuppressWarnings("unchecked")
	public IndexedSymbol(Symbol s, Vector<Long> v){
		symbol = s;
		indices = (Vector<Long>) v.clone();
	}
	
	public IndexedSymbol(Symbol s){
		symbol = s;
		indices = null;
	}
	
	public Symbol getSymbol(){
		return symbol;
	}
	
	public Vector<Long> getIndexes(){
		return indices;
	}
	
	/**
	 * On n'override pas la méthode equals, sinon il faut redéfinir la fonction de hashCode
	 * @param i
	 * @return
	 */
	public boolean isEqual(IndexedSymbol i){
		if (i == null){
			return false;
		}
		
		if (i.getIndexes() == null && indices == null){
			return i.getSymbol() == symbol;
		}
		else if (i.getIndexes() == null || indices == null){
			return false;
		}
		
		if (i.getIndexes().size() != indices.size()){
			return false;
		}
		if (i.getSymbol() != symbol){
			return false;
		}
		
		for (int j = 0; j < indices.size(); j++){
			if (indices.get(j).intValue() != i.getIndexes().get(j).intValue()){
				return false;
			}
		}
		return true;
	}
	
	public String toString(){
		String res;
		res = symbol.getName();
		for (int j = 0; j < indices.size(); j++){
			res += ("[" + indices.get(j)+ "]");
		}
		return res;
	}
	
}
