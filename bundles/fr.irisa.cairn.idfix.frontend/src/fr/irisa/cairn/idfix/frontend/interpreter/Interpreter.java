package fr.irisa.cairn.idfix.frontend.interpreter;

import float2fix.Float2fixFactory;
import float2fix.Graph;
import float2fix.util.Float2FixIO;
import fr.irisa.cairn.idfix.frontend.utils.ConvertSFGToDot;
import fr.irisa.cairn.idfix.model.fixedpointspecification.FixedPointSpecification;
import fr.irisa.cairn.idfix.utils.ConstantPathAndName;
import fr.irisa.cairn.idfix.utils.IDFixUtils;
import fr.irisa.cairn.idfix.utils.exceptions.InterpreterException;
import fr.irisa.cairn.idfix.utils.exceptions.SFGGeneratorException;
import fr.irisa.cairn.idfix.utils.logging.IDFixLogger;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;

import java.util.Iterator;

import org.apache.logging.log4j.Logger;


/**
 * Entry point for the Interpreter. It processes a
 * Procedure (EMF representation), and interprets it when possible.
 * When an interpretation is not possible, the instruction is added to
 * the SFG 
 * @author qmeunier
 */
public class Interpreter {

	private ProcedureSet ps;
	private ProcInterpreter procInterpreter;
	private ExecutionContext context;
	private String sfgFilePath;
	private Graph graphF2F;
	private SFGGenerator sfgGenerator;
	
	private Logger _logger = IDFixLogger.getLogger(ConstantPathAndName.LOG_NAME_FRONTEND); 

	public Interpreter(ProcedureSet ps, FixedPointSpecification fixedPointSpecif, String sfgFilePath) {
		this.ps = ps;
		this.graphF2F = Float2fixFactory.eINSTANCE.createGraph();
		this.context = new ExecutionContext();
		this.procInterpreter = new ProcInterpreter(context, graphF2F);
		this.context.setInterpreter(this.procInterpreter);
		this.sfgGenerator = procInterpreter.getInstInterpreter().getSfgGenerator();
		this.sfgGenerator.setFixedPointSpecification(fixedPointSpecif);
		this.context.setSFGGenerator(sfgGenerator);
		this.sfgFilePath = sfgFilePath;
	}


	private void myassert(boolean b, String s) throws InterpreterException {
		if (!b) {
			throw new InterpreterException(s);
		}
	}
	
	public void compute() throws InterpreterException, SFGGeneratorException {
		// We look for the main procedure
		Procedure p = null;
		Iterator<Procedure> it = ps.listProcedures().iterator();
		while (p == null && it.hasNext()) {
			Procedure proc = it.next();
			if (IDFixUtils.isMainFunc(proc.getSymbol())){
				p = proc;
			}
		}
		myassert(p != null, "No main function defined (done with the pragma MAIN_FUNC)");

		// Pushing the global scope
		context.push(ps.getScope());
		context.push(p.getScope());
		// Visiting the main procedure
		procInterpreter.doSwitch(p);
		/* To do: grouper tous ces appels dans une fonction du type "finalize" */
		sfgGenerator.addDelayEdges();
		sfgGenerator.suppressAffectBeforeDelay();
		sfgGenerator.setOutputNodes();
		sfgGenerator.deleteSecondaryGraph();
		sfgGenerator.renumberNodes();
		sfgGenerator.initCDFGNodeInstances();
		
		ConvertSFGToDot.generate(graphF2F, sfgFilePath, ".dot", true);
		
		Float2FixIO.save(sfgFilePath, graphF2F);
		
		// Printing the final context (after interpretation)
		_logger.debug("----------------------\n" + "final context : \n" + "----------------------" + context + "----------------------");
	}

}
