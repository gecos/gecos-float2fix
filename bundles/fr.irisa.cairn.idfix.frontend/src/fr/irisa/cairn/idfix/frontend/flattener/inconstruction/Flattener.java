package fr.irisa.cairn.idfix.frontend.flattener.inconstruction;

import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils.Context;
import fr.irisa.cairn.idfix.utils.IDFixUtils;
import fr.irisa.cairn.idfix.utils.exceptions.FlattenerException;
import gecos.blocks.BasicBlock;
import gecos.blocks.CompositeBlock;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Flatten a procedure set via an entry point. (Here #pragma MAIN_FUNC)
 * Create an execution context which allow to flatten loop/conditional structure.
 * 
 * During all the interpretation , we don't modify the orginal procedure set.
 * Creation a manager which associate procedure with new body block.
 * Scope, block and instruction are duplicated and write into this new body block. 
 * All the transformation are apply to copies. 
 * We update the original procedure set at the end of the transformation
 * 
 * @author Nicolas Simon
 * @date Jul 3, 2013
 */
public class Flattener {
	private ProcedureSet _ps;
	
	public Flattener(ProcedureSet ps){
		_ps = ps;
	}
	
	public void compute() throws FlattenerException{
		// Create a manager which associate for all procedure reached a BlockManager
		// Create the execution context
		Map<Procedure, BlockManager> manager =  new HashMap<Procedure, BlockManager>();
		Context context = new Context();
		ProcedureFlattener procFlattener = new ProcedureFlattener(context, manager);
		
		Procedure mainProcedure = null;
		for(Procedure p : _ps.listProcedures()){
			if(IDFixUtils.isMainFunc(p.getSymbol())){
				mainProcedure = p;
				break;
			}
		}
		
		if(mainProcedure == null)
			throw new FlattenerException("Error : no main function defined (use the pragma MAIN_FUNC to define the entry function point)");
		
		// Push into the context the global scope and the scope of the entry procedure
		context.push(_ps.getScope());
		context.push(mainProcedure.getScope());
		
		procFlattener.doSwitch(mainProcedure);
		
		
		// Add the new procedure created due to the flatten process
		for(Procedure p : manager.keySet()){
			if(_ps.findProcedure(p.getSymbol().getName()) == null){
				_ps.addProcedure(p);
			}
		}
		
		// Update main block of procedure and delete useless procedure
		List<Procedure> procToRemove = new ArrayList<>();
		for(Procedure p : _ps.listProcedures()){
			BlockManager blockManager = manager.get(p);
			if(blockManager == null){
				procToRemove.add(p);
			}
			else{
				CompositeBlock mainBlock =  blockManager.getMainBlock();
				BasicBlock bb1 = GecosUserBlockFactory.BBlock();
				BasicBlock bb2 = GecosUserBlockFactory.BBlock();
				mainBlock.getChildren().add(0, bb1);
				mainBlock.getChildren().add(bb2);
				p.setBody(mainBlock);
				p.setStart(bb1);
				p.setEnd(bb2);
			}
		}
		
		for(Procedure p : procToRemove){
			if(!(p.getSymbol().getName().equals("main"))){
				_ps.removeProcedure(p);
			}
		}		
	}	
}
