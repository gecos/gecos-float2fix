package fr.irisa.cairn.idfix.frontend.simulation;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Vector;

import affectationValueBySimulation.AffectationValueBySimulationFactory;
import affectationValueBySimulation.VariableValues;
import fr.irisa.cairn.gecos.model.analysis.types.ArrayLevel;
import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.idfix.utils.IDFixUtils;
import fr.irisa.cairn.idfix.utils.TraceManager;
import fr.irisa.cairn.idfix.utils.exceptions.SimulationException;
import gecos.core.Symbol;

/**
 * Check if the parameter between the fixed and float part have the same name and the same type.
 * Generate random values used for the simulation from the dynamic pragma of each input of the system.
 * 
 * For a scalar variable, we generate N sample in a binary file.
 * For a array variable, we generate N*array_size sample in a binary file.
 * The name of the binary file is the name of the input variable
 * @author nsimon
 * @date 24/02/14
 *
 */
public class RandomValuesBuilder {
	public TraceManager _tracemanager;
	
	private static final void _assert(boolean b, String msg) throws SimulationException 
	{
		if(!b) throw new SimulationException(msg);
	}
	
	public RandomValuesBuilder(TraceManager manager){
		_tracemanager = manager;
	}
	
	/**
	 * Generate random values for each input (with DYNAMIC pragma). This values are save in a binary file for each system input. 
	 * - For an scalar variable, we generate N sample in a binary file.
	 * - For an array variable, we generate N*array_size sample in a binary file.
	 * @throws SimulationException
	 */
	
	/**
	 * Return the number element to generate to a given type
	 * In this number of elements, we add the number of sample.
	 * Example (10 sample) : 
	 * 	- Scalar x -> return 10
	 *  - Array x[5] -> return 50
	 *  - Array x[5][2] -> return 100
	 * @param analyzer
	 * @return number sample value needed to generate
	 * @throws SimulationException
	 */
	private int getNumberElementToGenerate(TypeAnalyzer analyzer, int numberOfSimulation) throws SimulationException{
		int numberOfElement;
		if(analyzer.isBase() && analyzer.getBaseLevel().isBaseType()){
			numberOfElement = numberOfSimulation;
		}
		else if(analyzer.isArray()){
			ArrayLevel arrLevel = analyzer.tryGetArrayLevel(0);
			_assert(arrLevel != null, "Error in the analyze of a type by the TypeAnalyser");
			
			numberOfElement = numberOfSimulation;
			for(int i = 0 ; i < arrLevel.getNDims() ; i++){
				Long value = arrLevel.tryGetSize(i);
				_assert(value != -1, "Simulation backend only manage integer dimension array size (not generic instruction for example)");
				numberOfElement *= value.intValue();
			}
		}
		else{
			throw new SimulationException("Pointer as system input are not managed in the IDFix backend simulator");
		}
		
		return numberOfElement;
	}
	
	/**
	 * Create a binary file where we save generated sample value for the symbol given as parameter 
	 * @param s
	 * @throws SimulationException
	 */
	public String generate(Symbol s, int numberOfSimulation, String outputFolderPath) throws SimulationException{
		TypeAnalyzer typeAnalyzer = new TypeAnalyzer(s.getType());
		int numberOfElementToGenerate = getNumberElementToGenerate(typeAnalyzer, numberOfSimulation);
		File file = new File(outputFolderPath + s.getName() + "_values.bin");
		try {
			DataOutputStream outstream = new DataOutputStream(new FileOutputStream(file));
			ByteBuffer buffer;
			if(typeAnalyzer.getBaseLevel().isFloatSingle()) {
				buffer = ByteBuffer.allocate(numberOfElementToGenerate * Float.SIZE/Byte.SIZE).order(ByteOrder.nativeOrder());
				float valmin = IDFixUtils.getVarMin(s).floatValue();
				float valmax = IDFixUtils.getVarMax(s).floatValue();
				for(int i = 0 ; i < numberOfElementToGenerate ; i++){
					float temp = IDFixUtils.generateRandomFloatValue(valmin, valmax);
					saveTraceInputVariable(s, i, (double)temp);
					buffer.putFloat(temp);
				}
			}
			else if(typeAnalyzer.getBaseLevel().isFloatDouble()){
				buffer = ByteBuffer.allocate(numberOfElementToGenerate * Double.SIZE/Byte.SIZE).order(ByteOrder.nativeOrder());
				double valmin = IDFixUtils.getVarMin(s);
				double valmax = IDFixUtils.getVarMax(s);
				for(int i = 0 ; i < numberOfElementToGenerate ; i++){
					double temp = IDFixUtils.generateRandomDoubleValue(valmin, valmax);
					saveTraceInputVariable(s, i, (double)temp);
					buffer.putDouble(temp);
				}
			}
			else if(typeAnalyzer.getBaseLevel().isInt32()){
				buffer = ByteBuffer.allocate(numberOfElementToGenerate * Integer.SIZE/Byte.SIZE).order(ByteOrder.nativeOrder());
				int valmin = IDFixUtils.getVarMin(s).intValue();
				int valmax = IDFixUtils.getVarMax(s).intValue();
				for(int i = 0 ; i < numberOfElementToGenerate ; i++){
					int temp = IDFixUtils.generateRandomIntValue(valmin, valmax);
					saveTraceInputVariable(s, i, (double)temp);
					buffer.putInt(temp);
				}
			}
			else if(typeAnalyzer.getBaseLevel().isInt64()){
				buffer = ByteBuffer.allocate(numberOfElementToGenerate * Long.SIZE/Byte.SIZE).order(ByteOrder.nativeOrder());
				long valmin = IDFixUtils.getVarMin(s).longValue();
				long valmax = IDFixUtils.getVarMax(s).longValue();
				for(int i = 0 ; i < numberOfElementToGenerate ; i++){
					long temp = IDFixUtils.generateRandomLongValue(valmin, valmax);
					saveTraceInputVariable(s, i, (double)temp);
					buffer.putLong(temp);
				}
			}
			else{
				outstream.close();
				throw new SimulationException("Type not managed in the input sample generation in the backend simulator : " + typeAnalyzer);
			}
			outstream.write(buffer.array());
			outstream.close();
		} catch (IOException e) {
			throw new SimulationException(e);
		}
		
		return file.getAbsolutePath();
	}
	
	private void saveTraceInputVariable(Symbol s, long index, double value) throws SimulationException{
		TypeAnalyzer analyzer = new TypeAnalyzer(s.getType());
		if(analyzer.isBase()){
			traceVariableValue(s, value, "");
		}
		else if(analyzer.isArray()){
			Vector<Long> indices = IDFixUtils.getArrayIndexes(s.getType(), index);
			String indexes = "";
			for(Long ind : indices){ 
				 indexes = indexes + "[" + ind +"]";
			}
			traceVariableValue(s, value, indexes);
		}
		else{
			throw new SimulationException("Type not managed as input in the non lti simulation : " + s.getType());
		}
	}
	
	private void traceVariableValue(Symbol s, Double value, String indexes){
		Integer numDataCDFG = IDFixUtils.getSymbolAnnotationNumber(s);
		boolean found = false;
		for (VariableValues affect : _tracemanager.getValuesList().getVariableList()){
			if (affect.getNumDataCDFG() == numDataCDFG.intValue() && affect.getIndex().equals(indexes)){
				affect.getValues().add(value);
				found = true;
				break;
			}
		}
		if (!found){
			VariableValues varValue = AffectationValueBySimulationFactory.eINSTANCE.createVariableValues();
			varValue.setNumDataCDFG(numDataCDFG);
			varValue.getValues().add(value);
			if (indexes != null){
				varValue.setIndex(indexes);
			}
			else {
				varValue.setIndex("");
			}
			_tracemanager.getValuesList().getVariableList().add(varValue);
		}
	}
	
}