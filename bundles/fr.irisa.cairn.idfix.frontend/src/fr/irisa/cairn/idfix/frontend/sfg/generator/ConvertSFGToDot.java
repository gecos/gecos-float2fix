//package fr.irisa.cairn.idfix.frontend.sfg.generator;
//
//import java.io.FileWriter;
//import java.io.IOException;
//import java.io.PrintWriter;
//
//import float2fix.Annotations;
//import float2fix.Graph;
//
///**
// * Convert the {@link Graph} generate by the {@link SFGGenerator} to a dot file.
// * @author Nicolas Simon
// * @date Jul 19, 2013
// */
//public class ConvertSFGToDot {
//	public static void generate(Graph graph,  String outputDirPath, String fileName, boolean printAnnotations) {
//		FileWriter f;
//		try {
//			f = new FileWriter(outputDirPath + fileName);
//		} catch (IOException e) {
//			throw new RuntimeException(e);
//		}
//		PrintWriter pw = new PrintWriter(f);
//		pw.println("digraph G {");
//		pw.println("node [shape=oval];");
//		for (int i = 0; i < graph.getNode().size(); i++){
//			pw.print("n" + graph.getNode().get(i).getId() + " [label=\"" + graph.getNode().get(i).getName() + "\\n");
//			if (printAnnotations){
//				 pw.print("(" + graph.getNode().get(i).getId() + ")\\n");
//				for (Annotations a : graph.getNode().get(i).getAttr()){
//					pw.print(a.getKey() + " : " + a.getValue() + "\\n");
//				}
//			}
//			pw.println("\"];");
//		}
//		for (int i = 0; i < graph.getEdge().size(); i++){
//			pw.println("n" + graph.getEdge().get(i).getId_pred().getId() + " -> n" + graph.getEdge().get(i).getId_succ().getId() + ";");
//		}
//		pw.println("}\n");
//		pw.close();		
//	}
//}
//
