package fr.irisa.cairn.idfix.frontend.flattener.inconstruction;

import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.tools.switches.BasicBlockSwitch;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils.ScopeContext;
import fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils.SymbolReplacer;
import fr.irisa.cairn.idfix.utils.exceptions.FlattenerException;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.core.Scope;
import gecos.instrs.Instruction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * The BlockManager allow to create a body of procedure during the flattening process.
 * When instruction is added into the block manager. An updating of symbols contained into the instruction are done.
 * The ScopeContext regroup the symbol of the flatten procedure and are used to update the instruction symbols added.
 *  
 * @author Nicolas Simon
 * @date Jun 28, 2013
 */
public class BlockManager {
	private CompositeBlock _mainBlock;
	private CompositeBlock _currentCB;
	private BasicBlock _currentBB;
	private List<Block> _blockContained;
	private ProcedureFlattener _procFlattener;
	private ScopeContext _scopeContext;
	
	private SymbolReplacer _symbolReplacer;
	
	public BlockManager(ProcedureFlattener procInterpreter, Scope global, Scope params){
		_blockContained = new ArrayList<>();
		_procFlattener = procInterpreter;
		_scopeContext = new ScopeContext();
		if(global != null)
			_scopeContext.push(global);
		_scopeContext.push(params);
		_symbolReplacer = new SymbolReplacer(_scopeContext);
	}
	
	/**
	 * Return the procedure interpreter
	 * @return
	 */
	public ProcedureFlattener getProcedureFlattener(){
		return _procFlattener;
	}
	
	/**
	 * Return the main block
	 * @return
	 */
	public CompositeBlock getMainBlock(){
		return _mainBlock;
	}
	
	/**
	 * Set the main block
	 * @param main
	 * @throws FlattenerException 
	 */
	public void setMainBlock(CompositeBlock main) throws FlattenerException{
		if(_mainBlock == null){
			_mainBlock = main;
			_scopeContext.push(main.getScope());
			_blockContained.add(main);
		}
		else
			throw new FlattenerException("Error: Main block already set");
	
	}
	
	/**
	 * Return the current composite block
	 * @return
	 */
	public CompositeBlock getCurrentCompositeBlock(){
		return _currentCB;
	}
	
	/**
	 * Set a new current block. Need to be added before into the block manager.
	 * @param cb
	 * @throws FlattenerException 
	 */
	public void setCurrentCompositeBlock(CompositeBlock cb) throws FlattenerException{
		if(!_blockContained.contains(cb))
			throw new FlattenerException("The new current block " + cb + " is not contained into the block manager");
		
		_currentCB = cb;
		_currentBB = null;
	}
	
	/**
	 * Add to current block a new block.
	 * @param b
	 * @throws FlattenerException 
	 */
	public void addToCurrentBlock(Block b) throws FlattenerException{
		if(_currentCB == null)
			throw new FlattenerException("No current block specified");
		
		if(b instanceof CompositeBlock){
			_blockContained.add(b);
			_scopeContext.push(b.getScope());
		}
		_currentCB.addChildren(b);
	}
	
	/**
	 * Add into the current composite block a new instruction.
	 * @param inst
	 * @throws FlattenerException
	 */
	public void addInstruction(Instruction inst) throws FlattenerException{
		if(_currentCB == null){
			throw new FlattenerException("No current block specified");
		}
		if(_currentBB == null){
			_currentBB = GecosUserBlockFactory.BBlock(inst);
			_currentCB.addChildren(_currentBB);
		}
		else{
			_currentBB.addInstruction(inst);
		}
		
		_symbolReplacer.doSwitch(inst);
	}	
	
	/**
	 * Merge all the possible block.
	 */
	public void mergeBasicBlocksWithoutScope(){
		BlockMergerVisitor merger = new BlockMergerVisitor();
		merger.doSwitch(_mainBlock);
	}
	
	private class BlockMergerVisitor extends BasicBlockSwitch<Object> {
		
		private boolean containsOnlySingleBB(CompositeBlock b){
			if (b.getScope().getSymbols().size() == 0 &&
			((CompositeBlock) b).getChildren().size() == 1 &&
			((CompositeBlock) b).getChildren().get(0) instanceof BasicBlock){
				return true;
			}
			else {
				return false;
			}
		}
		
		private void addToRemove(HashMap<CompositeBlock, List<Block>> toRemove, CompositeBlock b, Block elem){
			if (toRemove.get(b) == null){
				List<Block> list = new LinkedList<Block>();
				list.add(elem);
				toRemove.put(b, list);
			}
			else {
				toRemove.get(b).add(elem);
			}
		}
		
		/**
		 * Algo non trivial. On effectue la fusion récursive des CompositeBlocks et des BasicBlocks.
		 */
		@Override
		public Object caseCompositeBlock(CompositeBlock b){
			boolean firstPass = true;
			HashMap<CompositeBlock, List<Block>> toRemove = new HashMap<CompositeBlock, List<Block>>();
			Block child1 = null;
			for (Block child2 : b.getChildren()){
				if (child2 instanceof CompositeBlock){
					doSwitch(child2);
				}
				if (!firstPass){
					// Condition de fusion : les deux blocks sont soit des CompositeBlocks avec scope vide et dont tous les fils sont des BasicB, soit des BasicB.
					// Dans le cas d'une fusion, on ne change pas child1
					if (child1 instanceof CompositeBlock && child2 instanceof CompositeBlock &&
							containsOnlySingleBB((CompositeBlock) child1) &&
							containsOnlySingleBB((CompositeBlock) child2)){
						BasicBlock bb1 = (BasicBlock) ((CompositeBlock) child1).getChildren().get(0);
						BasicBlock bb2 = (BasicBlock) ((CompositeBlock) child2).getChildren().get(0);
						for (Instruction inst : bb2.getInstructions()){
							bb1.addInstruction(inst.copy());
						}
						addToRemove(toRemove, b, child2);
					}
					else if (child1 instanceof BasicBlock && child2 instanceof CompositeBlock &&
							containsOnlySingleBB((CompositeBlock) child2)){
						BasicBlock bb2 = (BasicBlock) ((CompositeBlock) child2).getChildren().get(0);
						for (Instruction inst : bb2.getInstructions()){
							((BasicBlock) child1).addInstruction(inst.copy());
						}
						addToRemove(toRemove, b, child2);
					}
					else if (child2 instanceof BasicBlock && child1 instanceof CompositeBlock &&
							containsOnlySingleBB((CompositeBlock) child1)){
						BasicBlock bb1 = (BasicBlock) ((CompositeBlock) child1).getChildren().get(0);
						for (Instruction inst : ((BasicBlock) child2).getInstructions()){
							bb1.addInstruction(inst.copy());
						}
						addToRemove(toRemove, b, child2);
					}
					else if (child1 instanceof BasicBlock && child2 instanceof BasicBlock){
						for (Instruction inst : ((BasicBlock) child2).getInstructions()){
							((BasicBlock) child1).addInstruction(inst.copy());
						}
						addToRemove(toRemove, b, child2);
					}
					else {
						child1 = child2;
					}
				}
				else {
					child1 = child2;
					firstPass = false;
				}
			}
			// On supprime les blocks recopiés
			for (CompositeBlock parent : toRemove.keySet()){
				for (Block child : toRemove.get(parent)){
					parent.removeBlock(child);
				}
			}
			// Enfin, si le CompositeBlock ne contient plus qu'un CompositeBlock sans Scope qui ne contient qu'un BasicBlock,
			// on remplace le fils unique par ce BasicBlock
			if (b.getChildren().size() == 1 && b.getChildren().get(0) instanceof CompositeBlock &&
					containsOnlySingleBB((CompositeBlock) b.getChildren().get(0))){
				b.getChildren().set(0, ((CompositeBlock) b.getChildren().get(0)).getChildren().get(0));
			}
			return null;
		}

		@Override
		public Object caseBasicBlock(BasicBlock b) {
			return null;
		}
	
	}
}
