package fr.irisa.cairn.idfix.frontend.flattener;

import java.util.Map;
import java.util.Stack;

import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import fr.irisa.cairn.gecos.model.tools.switches.BasicBlockSwitch;
import fr.irisa.cairn.gecos.model.tools.utils.BlockCopyManager;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.DoWhileBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.blocks.WhileBlock;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.Instruction;

/**
 * Interprets blocks. If a "if" is not evaluable, we copy the instruction and flatten the "then"
 * and "else" parts.
 * @author qmeunier
 */
public class BlockFlattener extends BasicBlockSwitch<Object> {

	private ExecutionContext context;
	private ExecutionContext newContext;
	private ProcFlattener procFlattener;
	private SymbolReplacer symbolReplacer;
	private BlockSymbolReplacer blockSymbolReplacer;
	
	private boolean forceInstructionCopy;
	private boolean partialFlattening;
	
	public BlockFlattener(ExecutionContext context, ExecutionContext newContext, ProcFlattener procFlattener, boolean partial, SymbolReplacer symbolReplacer) {
		this.context = context;
		this.newContext = newContext;
		this.procFlattener = procFlattener;
		this.partialFlattening = partial;
		this.symbolReplacer = symbolReplacer;
		this.blockSymbolReplacer = new BlockSymbolReplacer(symbolReplacer);
		this.forceInstructionCopy = false;
	}

	public boolean forceInstructionCopy(){
		return forceInstructionCopy;
	}
	
	/**
	 * In order to correctly interpret a basic block, all the instructions have
	 * to be interpreted, and the result of a basic block is the result of the
	 * last interpreted instruction (Conditions block are basic blocks, and have
	 * to return a value).
	 */
	@Override
	public Object caseBasicBlock(BasicBlock b) {
		Object res = null;
		for (Instruction i : b.getInstructions()) {
			//System.out.println("Processing inst " + i);
			res = procFlattener.doSwitch(i);
			
			//FIXME: this will stop the flattener at this point resulting in an incorrect IR
			//Quick fix: remove the test below ?
			if (res != null){
				return res;
			}
		}
		return null;
	}

	/**
	 * When visiting a composite block, there is a new scope to manage. After
	 * adding declarations to the context, the interpretation consists in
	 * visiting each block of the composite block.
	 */
	@Override
	public Object caseCompositeBlock(CompositeBlock b) {
		Object res = null;
		
		Scope newScope = b.getScope().managedCopy(new BlockCopyManager());
		CompositeBlock newBlock = GecosUserBlockFactory.CompositeBlock(newScope);
		procFlattener.getCurrentBlockManager().addToCurrCompositeBlock(newBlock);
		CompositeBlock currentCompositeBlock = procFlattener.getCurrentBlockManager().getCurrCompositeBlock();
		procFlattener.getCurrentBlockManager().setCurrCompositeBlock(newBlock);
		
		try {
			context.push(b.getScope());
			newContext.push(newScope);
		} catch (FlattenerException e) {
			throw new RuntimeException(e);
		}
		
		for (Symbol s : newScope.getSymbols()){
			if (s.getValue() != null){
				symbolReplacer.doSwitch(s.getValue());
			}
		}
		
		for (Block block : b.getChildren()) {
			res = doSwitch(block);
		}
		context.pop();
		newContext.pop();
		
		procFlattener.getCurrentBlockManager().setCurrCompositeBlock(currentCompositeBlock);
		
		return res;
	}

	/**
	 * For. Non-evaluable conditions are not managed
	 */
	@Override
	public Object caseForBlock(ForBlock b) {
		Object res;
		Boolean interpreteInstsWithoutCopy = partialFlattening && !PartialFlatteningBlockAnnotation.forceFlattening(b); 
		Boolean saveInstInterpreterPartialMode = null;
		if (interpreteInstsWithoutCopy){
			saveInstInterpreterPartialMode = procFlattener.getInstFlattener().isInPartialFlattening();
			procFlattener.getInstFlattener().setPartialFlattening(true);
		}
		doSwitch(b.getInitBlock());
		boolean taken = (Boolean) doSwitch(b.getTestBlock());
		while (taken) {
			res = doSwitch(b.getBodyBlock());
			if (res != null){
				return res;
			}
			doSwitch(b.getStepBlock());
			taken = (Boolean) doSwitch(b.getTestBlock());
		}
		if (interpreteInstsWithoutCopy){
			Block copiedBlock = b.copy();
			blockSymbolReplacer.doSwitch(copiedBlock);
			procFlattener.getCurrentBlockManager().addToCurrCompositeBlock(copiedBlock);
			procFlattener.getInstFlattener().setPartialFlattening(saveInstInterpreterPartialMode);
		}
		return null;
	}
	

	/**
	 * If. Non-evaluable "ifs" require a special treatment:
	 * - We copy all instructions inside the then and else parts because they cannot be properly interpreted
	 * - We need to reset the context before evaluating the else part, and to merge the two contexts at the
	 *   end (i.e., setting to Uninitialized the value of modified variables)
	 */
	@Override
	public Object caseIfBlock(IfBlock b) {
		Object res = null;
		Object condRes = doSwitch(b.getTestBlock());
		if (condRes instanceof UninitializedValue){
			// On recopie le if (pour la condition et le else)
			
			// On force la recopie d'instructions à l'intérieur des conditions non évaluables
			boolean savedForceInstructionCopy = forceInstructionCopy;
			forceInstructionCopy = true;
			// Sauvegarde du CompositeBlock courant
			CompositeBlock currentCompositeBlock = procFlattener.getCurrentBlockManager().getCurrCompositeBlock();
			
			IfBlock ifBlock = (IfBlock) b.copy();
			symbolReplacer.doSwitch(ifBlock.getTestBlock());
			procFlattener.getCurrentBlockManager().addToCurrCompositeBlock(ifBlock);
			
			CompositeBlock thenBlock = GecosUserBlockFactory.CompositeBlock();
			ifBlock.setThenBlock(thenBlock);
			procFlattener.getCurrentBlockManager().setCurrCompositeBlock(thenBlock);
			Stack<Map<Symbol, SymbolValue>> oldNodesAndValues = context.cloneCurrValues();
			res = doSwitch(b.getThenBlock());
			// Pour n'avoir qu'une paire d'accolades
			ifBlock.setThenBlock(thenBlock.getChildren().get(0));
			Stack<Map<Symbol, SymbolValue>> thenNodesAndValues = context.cloneCurrValues();
			
			if (b.getElseBlock() != null){
				CompositeBlock elseBlock = GecosUserBlockFactory.CompositeBlock();
				ifBlock.setElseBlock(elseBlock);
				procFlattener.getCurrentBlockManager().setCurrCompositeBlock(elseBlock);
				
				Stack<Map<Symbol, SymbolValue>> elseNodesAndValues = oldNodesAndValues;
				context.setCurrValues(elseNodesAndValues);
				oldNodesAndValues = context.cloneCurrValues();
				
				res = doSwitch(b.getElseBlock());
				// Pour n'avoir qu'une paire d'accolades
				ifBlock.setElseBlock(elseBlock.getChildren().get(0));
				
				elseNodesAndValues = context.cloneCurrValues();
				context.setCurrValues(oldNodesAndValues);
				try {
					context.mergeValues(oldNodesAndValues, thenNodesAndValues, elseNodesAndValues);
				} catch (FlattenerException e) {
					throw new RuntimeException(e);
				}
			}
			else {
				context.setCurrValues(oldNodesAndValues);
				oldNodesAndValues = context.cloneCurrValues();
				try {
					context.mergeValues(oldNodesAndValues, thenNodesAndValues, null);
				} catch (FlattenerException e) {
					throw new RuntimeException(e);
				}
			}
			
			procFlattener.getCurrentBlockManager().setCurrCompositeBlock(currentCompositeBlock);
			
			forceInstructionCopy = savedForceInstructionCopy;
		}
		else {
			Boolean interpreteInstsWithoutCopy = partialFlattening && !PartialFlatteningBlockAnnotation.forceFlattening(b); 
			Boolean saveInstInterpreterPartialMode = null;
			if (interpreteInstsWithoutCopy){
				saveInstInterpreterPartialMode = procFlattener.getInstFlattener().isInPartialFlattening();
				procFlattener.getInstFlattener().setPartialFlattening(true);
			}
			boolean taken = (Boolean) condRes;
			if (taken) {
				res = doSwitch(b.getThenBlock());
			}
			else if (b.getElseBlock() != null){
				res = doSwitch(b.getElseBlock());
			}
			if (interpreteInstsWithoutCopy){
				Block copiedBlock = b.copy();
				blockSymbolReplacer.doSwitch(copiedBlock);
				procFlattener.getCurrentBlockManager().addToCurrCompositeBlock(copiedBlock);
				procFlattener.getInstFlattener().setPartialFlattening(saveInstInterpreterPartialMode);
			}
		}
		return res;
	}

	/**
	 * Loop. Non-evaluable conditions are not managed
	 */
	@Override
	public Object caseDoWhileBlock(DoWhileBlock b) {
		Object res;
		Boolean interpreteInstsWithoutCopy = partialFlattening && !PartialFlatteningBlockAnnotation.forceFlattening(b); 
		Boolean saveInstInterpreterPartialMode = null;
		if (interpreteInstsWithoutCopy){
			saveInstInterpreterPartialMode = procFlattener.getInstFlattener().isInPartialFlattening();
			procFlattener.getInstFlattener().setPartialFlattening(true);
		}
		
		boolean taken;
		do {
			res = doSwitch(b.getBodyBlock());
			if (res != null){
				return res;
			}
			taken = (Boolean) doSwitch(b.getTestBlock());
		} while (taken);
		
		if (interpreteInstsWithoutCopy){
			Block copiedBlock = b.copy();
			blockSymbolReplacer.doSwitch(copiedBlock);
			procFlattener.getCurrentBlockManager().addToCurrCompositeBlock(copiedBlock);
			procFlattener.getInstFlattener().setPartialFlattening(saveInstInterpreterPartialMode);
		}
		return null;
	}

	/**
	 * While. Non-evaluable conditions are not managed
	 */
	@Override
	public Object caseWhileBlock(WhileBlock b) {
		Object res;
		Boolean interpreteInstsWithoutCopy = partialFlattening && !PartialFlatteningBlockAnnotation.forceFlattening(b); 
		Boolean saveInstInterpreterPartialMode = null;
		if (interpreteInstsWithoutCopy){
			saveInstInterpreterPartialMode = procFlattener.getInstFlattener().isInPartialFlattening();
			procFlattener.getInstFlattener().setPartialFlattening(true);
		}
		
		boolean taken = (Boolean) doSwitch(b.getTestBlock());
		while (taken) {
			res = doSwitch(b.getBodyBlock());
			if (res != null){
				return res;
			}
			taken = (Boolean) doSwitch(b.getTestBlock());
		}
	
		if (interpreteInstsWithoutCopy){
			Block copiedBlock = b.copy();
			blockSymbolReplacer.doSwitch(copiedBlock);
			procFlattener.getCurrentBlockManager().addToCurrCompositeBlock(copiedBlock);
			procFlattener.getInstFlattener().setPartialFlattening(saveInstInterpreterPartialMode);
		}
		return null;
	}
	
	
	
	private class BlockSymbolReplacer extends BasicBlockSwitch<Object> {

		private SymbolReplacer symbolReplacer;
		
		public BlockSymbolReplacer(SymbolReplacer symbolReplacer){
			this.symbolReplacer = symbolReplacer;
		}
		
		@Override
		public Object caseBasicBlock(BasicBlock b) {
			for (Instruction inst : b.getInstructions()){
				symbolReplacer.doSwitch(inst);
			}
			return null;
		}	
	}
	
	
}
