//package fr.irisa.cairn.idfix.frontend.sfg.generator;
//
//import fr.irisa.cairn.gecos.model.tools.switches.DefaultInstructionSwitch;
//import gecos.instrs.CallInstruction;
//import gecos.instrs.ConvertInstruction;
//import gecos.instrs.FieldInstruction;
//import gecos.instrs.FloatInstruction;
//import gecos.instrs.GenericInstruction;
//import gecos.instrs.IntInstruction;
//import gecos.instrs.SymbolInstruction;
//
///**
// * Return the value of the index instruction of an array. The index must be resolved before.
// * It need to be a int/long value
// * @author Nicolas Simon
// * @date Jul 19, 2013
// */
//public class ArrayIndexInterpreter extends DefaultInstructionSwitch<Object> {
//	
//	@Override
//	public Object caseIntInstruction(IntInstruction g){
//		return new Long(g.getValue());
//	}
//	
//	@Override
//	public Object caseFloatInstruction(FloatInstruction g){
//		return new Double(g.getValue());
//	}
//	
//	@Override
//	public Object caseGenericInstruction(GenericInstruction g){
//		throw new RuntimeException("Error: SFG generator must be take a source code with only array index resolved");
//	}
//	
//	@Override
//	public Object caseSymbolInstruction(SymbolInstruction g){
//		throw new RuntimeException("Error: SFG generator must be take a source code with only array index resolved");
//	}
//	
//	@Override
//	public Object caseCallInstruction(CallInstruction g){
//		throw new RuntimeException("Error: SFG generator must be take a source code with only array index resolved");
//	}
//
//	@Override
//	public Object caseConvertInstruction(ConvertInstruction g) {
//		throw new RuntimeException("Error: SFG generator must be take a source code with only array index resolved");
//	}
//
//	@Override
//	public Object caseFieldInstruction(FieldInstruction g) {
//		throw new RuntimeException("Error: SFG generator must be take a source code with only array index resolved");
//	}
//}
