package fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils;

public interface ISymbolType {
	public ISymbolType copy();
}
