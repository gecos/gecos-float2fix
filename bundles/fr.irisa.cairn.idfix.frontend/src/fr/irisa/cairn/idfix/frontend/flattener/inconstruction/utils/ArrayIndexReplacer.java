package fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils;

import fr.irisa.cairn.gecos.model.tools.switches.DefaultInstructionSwitch;
import gecos.instrs.ArrayInstruction;
import gecos.instrs.Instruction;

public class ArrayIndexReplacer extends DefaultInstructionSwitch<Object>{
	private Context _context;
	private ValueReplacer _valueReplacer;
	
	public ArrayIndexReplacer(Context context){
		_context = context;
		_valueReplacer = new ValueReplacer(_context);
	}
	
	@Override
	public Object caseArrayInstruction(ArrayInstruction object) {
		for(Instruction index : object.getIndex())
			_valueReplacer.doSwitch(index);
		return null;
	}	
}
