package fr.irisa.cairn.idfix.frontend.interpreter;

import float2fix.Node;
import fr.irisa.cairn.idfix.utils.exceptions.InterpreterException;
import fr.irisa.cairn.idfix.utils.exceptions.SFGGeneratorException;
import fr.irisa.cairn.idfix.utils.exceptions.SFGModelCreationException;
import gecos.core.Scope;
import gecos.core.Symbol;
import gecos.instrs.ArrayValueInstruction;
import gecos.instrs.Instruction;
import gecos.types.ArrayType;
import gecos.types.FunctionType;
import gecos.types.Type;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.Vector;

/**
 * Represents a program execution context
 * This code was started from the interpreter tutorial
 * @author qmeunier
 */
public class ExecutionContext {

	/**
	 * One level of the stack represent one level of nested block. Each time the
	 * interpreter visits a block, a new Map is pushed on the top of the stack.
	 * When a function is called, a new stack is created, with the first level
	 * set with the globals variables of the ProcedureSet. The old stack is
	 * stored in a stack of stack : framestack.
	 */
	private Stack<Map<Symbol, SymbolValue>> stack;
	private Stack<Stack<Map<Symbol, SymbolValue>>> framestack;

	// ParamsTheo représente les paramètres théoriques lors d'un appel de fonction
	// On a besoin d'y accéder de manière indépendante de la pile car dans le cas d'un appel récursif,
	// on veut pouvoir effectuer une affectation d'une valeur (SymbolValue) à une autre pour deux symboles
	// identiques (si on rappelle la même fonction avec le même paramètre)
	// Du coup, on ne peut plus différencier dans la pile la valeur du symbole passé en paramètre
	// et celle du paramètre théorique
	// Par ailleurs, on est obligé d'avoir une Stack à cause des appels de fonctions imbriqués :
	// ex : f(1, f(a, 2, b), b); sinon à la sortie de l'appel interne on aura perdu les paramètres théoriques
	// correspondants à l'appel le plus externe
	private Stack<Map<Symbol, SymbolValue>> paramsTheo;
	
	/* The interpreter allows to evaluate the values of initialized variables */
	private ProcInterpreter interpreter;
	
	private SFGGenerator sfgGenerator;

	private static void myassert(boolean b, String s) throws InterpreterException{
		if (!b){
			throw new InterpreterException(s);
		}
	}
	
	
	public ExecutionContext() {
		stack = new Stack<Map<Symbol, SymbolValue>>();
		framestack = new Stack<Stack<Map<Symbol, SymbolValue>>>();
		paramsTheo = new Stack<Map<Symbol, SymbolValue>>();
	}

	public void setInterpreter(ProcInterpreter interpreter) {
		this.interpreter = interpreter;
	}
	
	public void setSFGGenerator(SFGGenerator sfgGenerator) {
		this.sfgGenerator = sfgGenerator;
	}
		
	
	/**
	 * Calcule l'indice "à plat" d'un élément d'un tableau à une ou plusieurs dimensions
	 * L'indice de l'élément du tableau est calculé à partir de :
	 *     - sa définition pour connaitre la taille des autres dimensions
	 *     - la liste des indices
	 * exemple : int t[5][3];
	 * pour accéder t[3][2], on calcule un indice de 3*3 + 2*1 = 11 si le tableau est représenté à plat
	 * @param type : le type du tableau
	 * @param indices : les indices pour chaque dimension
	 * @return l'indice en considérant le tableau en entrée à plat
	 * @throws InterpreterException 
	 */
	public int getArrayIndex(Type type, Vector<Long> indices) throws InterpreterException{ 
		Type localType = type;
		ArrayType arrayType = (ArrayType) localType;
		myassert(arrayType.getNbDims() == indices.size(), "The number of index give (" + indices.size() + ") doesn't correpond to the number of the table dimension (" + arrayType.getNbDims() + ")");
		int ind = 0;
		int nbElemParDim = 1;
		for (int j = indices.size() - 1; j >= 0; j--){
			arrayType = (ArrayType) localType;
			ind += indices.get(j)*nbElemParDim;
//TODO A remettre comme avant quand le problème de la non initialisation de lower et upper dans arrayType sera résolu
//			nbElemParDim *= arrayType.getUpper() - array<Type.getLower() + 1;
			Instruction inst = arrayType.getSizeExpr();
			Object index = interpreter.doSwitch(inst);
			myassert((index instanceof Long), "The flatten table index is not a long/integer");
			nbElemParDim *= (Long) index;
			localType = arrayType.getBase();
		}
		return ind;
	}
	
	/**
	 * Calcule le vecteur d'indices d'un élément d'un tableau à une ou plusieurs dimensions
	 * à partir de son indice à plat et du type du tableau
	 * @param type : le type du tableau
	 * @param indice : l'indice à plat que l'on veut factoriser
	 * @return un vecteur d'indice correspondant à l'indice à plat pour le type du tableau
	 * @throws InterpreterException 
	 */
	public static Vector<Long> getArrayIndexes(Type type, Long indice) throws InterpreterException{
		Type localType = type;
		ArrayType arrayType;
		Vector<Long> indices = new Vector<Long>();
		Long currIndice = indice;
		Long indiceDim;
		Long nbElemDim;
		while (localType instanceof ArrayType){
			arrayType = (ArrayType) localType;
			nbElemDim = arrayType.getUpper() - arrayType.getLower() + 1;
			indiceDim = currIndice % nbElemDim;
			currIndice -= indiceDim;
			currIndice = currIndice / nbElemDim;
			localType = arrayType.getBase();
			indices.add(indiceDim);
		}
		myassert(((ArrayType) type).getNbDims() == indices.size(), "The number of dimensions (" + ((ArrayType) type).getNbDims() + ") is not equal to the number of table index generated (" + indices.size()+")");
		Vector<Long> res = new Vector<Long>();
		for (int i = indices.size() - 1; i >= 0; i--){
			res.add(indices.get(i));
		}
		//System.out.println("indice : " + indice + " - res : " + res);
		return res;
	}
	
	
	
	/**
	 * Gecos ne traite pas les initialisations de tableaux à plusieurs dimensions qui ne suivent pas exactement sa définition :
	 * int t[2][2] = { 0, 1, 2, 3 } n'est pas valide pour Gecos...
	 * De plus, les valeurs d'initialisation à plusieurs dimensions sont laissées sous forme de "Valeurs de valeurs"
	 * On a donc besoin de les applatir pour correspondre à notre représentation de tableau à une dimension
	 * @param inst : l'instruction d'initialisation
	 * @param v : le vecteur que l'en enrichit avec les valeurs qu'on lit
	 */
	private void flattenArrayInitValues(Instruction inst, Vector<Object> v){
		if (inst instanceof ArrayValueInstruction){
			ArrayValueInstruction insts = (ArrayValueInstruction) inst;
			for (Instruction i : insts.getChildren()){
				flattenArrayInitValues(i,v);
			}
		}
		else {
			Object value = this.interpreter.doSwitch(inst);
			v.add(value);
			//System.out.println("flattenArrayInitValues : on ajoute " + value + " à la liste d'initialisation");
		}
	}


	/**
	 * Test si toutes les valeurs d'un tableau sont initialisées
	 * @param array : le tableau à tester
	 * @return vrai ssi toutes les valeurs du tableaux sont initialisées
	 * @throws InterpreterException 
	 */
	public boolean arrayIsFullyInitialized(Symbol array) throws InterpreterException{
		myassert(array.getType() instanceof ArrayType, "The symbol type is not a ArrayType");
		for (int i = stack.size() - 1; i >= 0; i--) {
			Map<Symbol, SymbolValue> current = stack.get(i);
			if (current.containsKey(array)) {
			//if (levelContainsVarName(current,array)) {
				for (Integer idx = 0; idx < current.get(array).getNbValues(); idx++){
				//for (Integer idx = 0; idx < getSymbolValueFromVarName(current, array).getNbValues(); idx++){
					if (current.get(array).getValueAt(idx) instanceof UninitializedValue){
					//if (getSymbolValueFromVarName(current, array).getValueAt(idx) instanceof UninitializedValue){
						return false;
					}
				}
				return true;
			}
		}
		myassert(false, "Table symbol " + array + " (" + Integer.toHexString(array.hashCode()) + ") not found");
		return false;
	}
	
	
	/**
	 * Met à jour la valeur d'un symbole dans la représentation intermédiaire
	 * @param sym : le symbole à mettre à jour
	 * @param newValue : la nouvelle valeur
	 * @throws InterpreterException 
	 */
	public void setValue(Symbol sym, Object newValue) throws InterpreterException {
		setValue(sym, newValue, false);
	}
	
	public void setValue(Symbol sym, Object newValue, boolean destIsParamTheo) throws InterpreterException {
		if (destIsParamTheo){
			if (paramsTheo.peek().containsKey(sym)) {
				//System.out.println("Maj du symbole " + sym + " (valeur " + newValue + ")");
				paramsTheo.peek().get(sym).setValueAt(newValue, 0);
				return;
			}
		}
		else {
			int top = stack.size() - 1;
			/* browse the stack downwards (select the Symbol in the highest scope) */
			for (int i = top; i >= 0; i--) {
				Map<Symbol, SymbolValue> current = stack.get(i);
				if (current.containsKey(sym)) {
					//System.out.println("Maj du symbole " + sym + " (valeur " + newValue + ")");
					current.get(sym).setValueAt(newValue, 0);
					return;
				}
			}
		}
		/* If we get out of the loop, then no scope contains the symbol */
		throw new InterpreterException("Symbol " + sym.getName() + " not found.");
	}
	
	/**
	 * Met à jour la valeur d'un symbole dans la représentation intermédiaire, quand il s'agit d'un symbole de tableau
	 * @param sym : le symbole à mettre à jour
	 * @param newValue : la nouvelle valeur
	 * @param indices : la liste des indices de l'élément du tableau à mettre à jour
	 * @throws InterpreterException 
	 */
	public void setValue(Symbol sym, Object newValue, Vector<Long> indices) throws InterpreterException {
		setValue(sym, newValue, indices, false);
	}
	
	public void setValue(Symbol sym, Object newValue, Vector<Long> indices, boolean destIsParamTheo) throws InterpreterException {
		if (destIsParamTheo){
			if (paramsTheo.peek().containsKey(sym)) {
				int ind = getArrayIndex(sym.getType(), indices);
				paramsTheo.peek().get(sym).setValueAt(newValue, ind);
				return;
			}
		}
		else {
			int top = stack.size() - 1;
			/* browse the stack downwards (select the Symbol in the highest scope) */
			for (int i = top; i >= 0; i--) {
				Map<Symbol, SymbolValue> current = stack.get(i);
				if (current.containsKey(sym)) {
					int ind = getArrayIndex(sym.getType(), indices);
					/*System.out.print("Maj du symbole " + sym + " (valeur " + newValue + ") - indices : ");
					for (int j = 0; j < indices.size(); j++){
						System.out.print("[" + indices.get(j)+ "]");
					}
					System.out.println(" (total : " + ind + ")");
					*/
					current.get(sym).setValueAt(newValue, ind);
					return;
				}
			}
		}
		/* If we get out of the loop, then no scope contains the symbol */
		throw new InterpreterException("Symbol " + sym.getName() + " not found.");
	}

	
	/**
	 * Retourne la valeur d'un symbole dans la représentation intermédiaire 
	 * @param sym : le symbole
	 * @return sa valeur
	 * @throws InterpreterException 
	 */
	public Object getValue(Symbol sym) throws InterpreterException {
		return getValue(stack, sym);
	}
	
	public Object getValue(Stack<Map<Symbol, SymbolValue>> theStack, Symbol sym) throws InterpreterException {
		Object value = null;
		int top = theStack.size() - 1;
		/* Browse the stack downwards (select the Symbol in the highest scope) */
		for (int i = top; i >= 0; i--) {
			Map<Symbol, SymbolValue> current = theStack.get(i);
			if (current.containsKey(sym)) {
				value = current.get(sym).getValueAt(0);
				//System.out.println("Context : getValue retourne le symbol " + sym.getName() + " - valeur : " + value);
				myassert(value != null, "The value of the symbol " +sym.getName() + " is null");
				return value;
			}
		}
		throw new InterpreterException("Symbol " + sym.getName() + " not found");
	}

	
	/**
	 * Retourne la valeur d'un symbole de type tableau dans la représentation intermédiaire 
	 * @param sym : le symbole
	 * @param indices : la liste des indices de l'élément du tableau
	 * @return la valeur du symbole
	 * @throws InterpreterException 
	 */
	public Object getValue(Symbol sym, Vector<Long> indices) throws InterpreterException {
		return getValue(stack, sym, indices);
	}
	
	public Object getValue(Stack<Map<Symbol, SymbolValue>> theStack, Symbol sym, Vector<Long> indices) throws InterpreterException {
		Object value = null;
		int top = theStack.size() - 1;
		/* Browse the stack downwards (select the Symbol in the highest scope) */
		for (int i = top; i >= 0; i--) {
			Map<Symbol, SymbolValue> current = theStack.get(i);
			if (current.containsKey(sym)) {
				long ind = getArrayIndex(sym.getType(), indices);
				/*System.out.print("Context : getValue retourne le symbol " + sym.getName() + " - valeur : " + value + " - indices : ");
				for (int j = 0; j < indices.size(); j++){
					System.out.print("[" + indices.get(j)+ "]");
				}
				System.out.println(" (total : " + ind + ")");*/
				value = current.get(sym).getValueAt(ind);
				myassert(value != null, "The ArraySymbol " + sym.getName() + " have not value to the index "+ indices);
				return value;
			}
		}
		throw new InterpreterException("Symbol " + sym.getName() + " not found (indices : " + indices + ")");
	}
	
	/**
	 * Retourne la symbolValue associée à une variable
	 * @param symbol
	 * @return The SymbolValue associated to a symbol
	 * @throws InterpreterException 
	 */
	public SymbolValue getSymbolValue(Symbol symbol) throws InterpreterException{
		SymbolValue res = null;
		int top = stack.size() - 1;
		for (int i = top; i >= 0; i--) {
			Map<Symbol, SymbolValue> current = stack.get(i);
			if (current.containsKey(symbol)) {
				res = current.get(symbol);
				//System.out.println("Context : getValue retourne le symbol " + sym.getName() + " - valeur : " + value);
				myassert(res.getValueAt(0) != null, "The value of the symbol is null into the current context");
				return res;
			}
		}
		/* if we go out of the loop, it means that the symbols does not exist in the current context */
		myassert(false, "Symbol " + symbol.getName() + " not found.");
		return null;
	}
	
	
	/**
	 * Sets a symbolValue to a given symbol
	 * @param symbol
	 * @throws InterpreterException 
	 */
	public void setSymbolValue(Symbol symbol, SymbolValue newValue) throws InterpreterException{
		int top = stack.size() - 1;
		boolean found = false;
		/* Browse the stack downwards (select the Symbol in the highest scope) */
		for (int i = top; i >= 0; i--) {
			Map<Symbol, SymbolValue> current = stack.get(i);
			if (current.containsKey(symbol)) {
				found = true;
				current.put(symbol, newValue);
			}
		}
		myassert(found, "Symbol " + symbol.getName() + " not found.");
	}
	
	
	
	
	/* Désormais on fait la même chose pour les noeuds du SFG associés aux variables */
	/**
	 * Met à jour le noeud d'un symbole dans la représentation intermédiaire
	 * @param sym : le symbole à mettre à jour
	 * @param newNode : le nouveau noeud
	 * @throws InterpreterException 
	 */
	public void setNode(Symbol sym, Node newNode) throws InterpreterException {
		setNode(sym, newNode, false);
	}
	
	public void setNode(Symbol sym, Node newNode, boolean destIsParamTheo) throws InterpreterException {
		if (destIsParamTheo){
			if (paramsTheo.peek().containsKey(sym)) {
				//System.out.println("Maj du noeud pour pour le symbole " + sym + " (node " + newNode + ")");
				paramsTheo.peek().get(sym).setNodeAt(newNode, 0);
				return;
			}
		}
		else {
			int top = stack.size() - 1;
			/* browse the stack downwards (select the Symbol in the highest scope) */
			for (int i = top; i >= 0; i--) {
				Map<Symbol, SymbolValue> current = stack.get(i);
				if (current.containsKey(sym)) {
					//System.out.println("Maj du noeud pour pour le symbole " + sym + " (node " + newNode + ")");
					current.get(sym).setNodeAt(newNode, 0);
					return;
				}
			}
		}
		/* If we get out of the loop, then no scope contains the symbol */
		throw new InterpreterException("Symbol " + sym.getName() + " not found.");
	}
	
	
	/**
	 * Met à jour le noeud d'un symbole dans la représentation intermédiaire, quand il s'agit d'un symbole de tableau
	 * @param sym : le symbole à mettre à jour
	 * @param newNode : le nouveau noeud
	 * @param indices : la liste des indices de l'élément du tableau à mettre à jour
	 * @throws InterpreterException 
	 */
	public void setNode(Symbol sym, Node newNode, Vector<Long> indices) throws InterpreterException {
		setNode(sym, newNode, indices, false);
	}
	
	public void setNode(Symbol sym, Node newNode, Vector<Long> indices, boolean destIsParamTheo) throws InterpreterException {
		if (destIsParamTheo){
			if (paramsTheo.peek().containsKey(sym)) {
				long ind = getArrayIndex(sym.getType(), indices);
				paramsTheo.peek().get(sym).setNodeAt(newNode, ind);
				return;
			}
		}
		else {
			int top = stack.size() - 1;
			/* browse the stack downwards (select the Symbol in the highest scope) */
			for (int i = top; i >= 0; i--) {
				Map<Symbol, SymbolValue> current = stack.get(i);
				if (current.containsKey(sym)) {
					int ind = getArrayIndex(sym.getType(), indices);
					/*System.out.print("Maj du noeud pour le symbole " + sym + " (node " + newNode + ") - indices : ");
					for (int j = 0; j < indices.size(); j++){
						System.out.print("[" + indices.get(j)+ "]");
					}
					System.out.println(" (total : " + ind + ")");*/
					current.get(sym).setNodeAt(newNode, ind);
					return;
				}
			}
		}
		throw new InterpreterException("Symbol " + sym.hashCode() + " not found (indices : " + indices + ")");
	}

	
	/**
	 * Retourne le noeud d'un symbole dans la représentation intermédiaire 
	 * @param sym : le symbole
	 * @return son noeud associé
	 * @throws InterpreterException 
	 */
	public Object getNode(Symbol sym) throws InterpreterException {
		return getNode(stack, sym);
	}
	
	public Object getNode(Stack<Map<Symbol, SymbolValue>> theStack, Symbol sym) throws InterpreterException{
		Object node = null;
		int top = theStack.size() - 1;
		/* browse the stack downwards (select the Symbol in the highest scope) */
		for (int i = top; i >= 0; i--) {
			Map<Symbol, SymbolValue> current = theStack.get(i);
			if (current.containsKey(sym)) {
				node = current.get(sym).getNodeAt(0);
				//System.out.println("Context : getNode sur le symbole " + sym.getName() + " retourne le node : " + node);
				myassert(node != null, "Node wanted associed to the symbol "+sym.getName()+" doesn't exist");
				return node;
			}
		}
		throw new InterpreterException("Symbol " + sym.getName() + " not found");
	}

	
	/**
	 * Retourne le noeud d'un symbole de type tableau dans la représentation intermédiaire 
	 * @param sym : le symbole
	 * @param indices : la liste des indices de l'élément du tableau
	 * @return le noeud du symbole
	 * @throws InterpreterException 
	 */
	public Object getNode(Symbol sym, Vector<Long> indices) throws InterpreterException {
		return getNode(stack, sym, indices);
	}
	
	public Object getNode(Stack<Map<Symbol, SymbolValue>> theStack, Symbol sym, Vector<Long> indices) throws InterpreterException {
		Object node = null;
		int top = theStack.size() - 1;
		/* browse the stack downwards (select the Symbol in the highest scope) */
		for (int i = top; i >= 0; i--) {
			Map<Symbol, SymbolValue> current = theStack.get(i);
			if (current.containsKey(sym)) {
				int ind = getArrayIndex(sym.getType(), indices);
				node = current.get(sym).getNodeAt(ind);
				myassert(node != null, "Node wanted associed to the ArraySymbol "+sym.getName()+" with the index " + ind + " doesn't exist");
				/*System.out.print("Context : getNode sur le symbole " + sym.getName() + " retourne le node : " + node + " - indices : ");
				for (int j = 0; j < indices.size(); j++){
					System.out.print("[" + indices.get(j)+ "]");
				}
				System.out.println(" (total : " + ind + ")");*/
				return node;
			}
		}
		throw new InterpreterException("Symbol " + sym.getName() + " not found");
	}
	
	
	/**
	 * Pops the current scope
	 */
	public void pop() {
		this.stack.pop();
	}

	
	/**
	 * Pushes a new scope (C block)
	 * If the variables are initialized, their values are stored
	 * @param scope : the scope to push
	 * @throws InterpreterException 
	 */
	public void push(Scope scope) throws InterpreterException {
		Map<Symbol, SymbolValue> newLevel = new HashMap<Symbol, SymbolValue>();
		stack.push(newLevel);
		for(Symbol s : scope.getSymbols()){
			if(s.getType().asBase() != null){
				pushScalarSymbol(s, newLevel);
			}
			else if(s.getType().isArray()){
				pushArraySymbol(s, newLevel);
			}
			else if(!(s.getType() instanceof FunctionType)){
				System.out.println(s);
				throw new InterpreterException("Kind of symbol type not managed by the i");
			}
		}
	}
	
	private void pushScalarSymbol(Symbol s, Map<Symbol, SymbolValue> newLevel){
		Vector<Object> initList = new Vector<Object>();
		if(s.getValue() != null){
			Object value = this.interpreter.doSwitch(s.getValue());
			initList.add(value);
		}
		else{
			initList.add(null);
		}
		newLevel.put(s, new SymbolValue(initList));
	}
	
	private void pushArraySymbol(Symbol s, Map<Symbol, SymbolValue> newLevel) throws InterpreterException{
		Instruction sizeInst;
		Object index;
		ArrayType arrType;
		
		Vector<Object> initList = new Vector<Object>();
		int size = 1;
		Type type;
		for(type = s.getType() ; type instanceof ArrayType ; type = ((ArrayType) type).getBase()){
			arrType = (ArrayType) type;
			sizeInst = arrType.getSizeExpr();
			index = interpreter.doSwitch(sizeInst);
			myassert((index instanceof Long), "Index du tableau " + s.getName() + " lors de sa création dans le contexte d'éxécution n'est pas un long");
					
			size *= (Long) index;			
		}
		if (s.getValue() == null){
			for (int i = 0; i < size; i++){
				initList.add(new UninitializedValue());
			}
		}
		else {
			flattenArrayInitValues(s.getValue(),initList);
			for(int i=initList.size(); i<size; i++)   //in case not all values are specified
				initList.add(new Long(0));	//XXX type long? shall it be UninitializedValue?
		}
		newLevel.put(s, new SymbolValue(initList));
	}	

	public void initParamsTheo(){
		HashMap<Symbol, SymbolValue> newParams = new HashMap<Symbol, SymbolValue>();
		paramsTheo.push(newParams);
	}
	
	public Map<Symbol, SymbolValue> getParamsTheo(){
		return paramsTheo.peek();
	}
	

	/**
	 * Assumes that current().peek() contains the declaration and values of
	 * function's parameters.
	 */
	@SuppressWarnings("unchecked")
	public void pushFrame() {
		/* The first step consists in storing the current context in the
		 * framestack. Then a new context has to be created for the procedure
		 * call. Finally, the new context is initialized with the global
		 * variables and the procedure's parameters */
		framestack.push((Stack<Map<Symbol, SymbolValue>>) stack.clone());
		stack = new Stack<Map<Symbol, SymbolValue>>();
		stack.push(framestack.peek().get(0));
		stack.push(paramsTheo.peek());
	}

	
	public void popFrame() {
		/* When poping a frame, the operation consists in restoring the last
		 * context, and in updating the global variables with the values from the
		 * procedure's context */
		Stack<Map<Symbol, SymbolValue>> oldstack = (Stack<Map<Symbol, SymbolValue>>) framestack.pop();
		oldstack.set(0, stack.get(0));
		stack = oldstack;
		paramsTheo.pop();
	}
	
	
	@SuppressWarnings("unchecked")
	public Stack<Map<Symbol, SymbolValue>> cloneCurrNodesAndValues(){
		Stack<Map<Symbol, SymbolValue>> res = new Stack<Map<Symbol, SymbolValue>>();
		for (int i = 0; i < stack.size(); i++){
			HashMap<Symbol, SymbolValue> current = (HashMap<Symbol, SymbolValue>) stack.get(i);
			Map<Symbol, SymbolValue> currMap = (Map<Symbol, SymbolValue>) current.clone();
			for (Symbol s : currMap.keySet()){
				currMap.put(s, currMap.get(s).copy());
			}
			res.add(currMap);
		
		}
		return res;
	}
	
	public void setCurrNodesAndValues(Stack<Map<Symbol, SymbolValue>> newStack){
		this.stack = newStack;
	}
	
	/**
	 * Merges the current context nodes with the nodes present in the savedValues parameter, using phi nodes
	 * @param savedValues the values to merge with the current context
	 * @throws InterpreterException 
	 * @throws SFGModelCreationException 
	 * @throws SFGGeneratorException 
	 */
	public void mergeNodesAndValues(Stack<Map<Symbol, SymbolValue>> origValues, Stack<Map<Symbol, SymbolValue>> thenValues, Stack<Map<Symbol, SymbolValue>> elseValues) throws InterpreterException, SFGGeneratorException, SFGModelCreationException{
		myassert(origValues.size() == thenValues.size() && (elseValues == null || thenValues.size() == elseValues.size()), "The size of the different context of phi interpretation is incorrect.  orig : " + origValues.size() + " - then : " + thenValues.size());
		for (int i = origValues.size() - 1; i >= 0; i--) {
			Map<Symbol, SymbolValue> oldMap = origValues.get(i);
			Map<Symbol, SymbolValue> thenMap = thenValues.get(i);
			Map<Symbol, SymbolValue> elseMap = null;
			if (elseValues != null){
				elseMap = elseValues.get(i);
			}
			// On parcourt les symboles dans un ordre fonction de leur nom, ceci pour qu'il soit toujours parcouru dans le même ordre,
			// afin que plusieurs exécutions de suite donnent toujours le même sfg (sinon, des inversions dans l'ordre des noeuds sont
			// possible, ce qui fait échouer les tests de non régression)
			List<Symbol> keys = new ArrayList<Symbol>(oldMap.keySet());
			SymbolComparator comparator = new SymbolComparator();
			Collections.sort(keys, comparator);
			for (Symbol s : keys) {
				myassert(thenMap.containsKey(s), "The then context doesn't contain a symbol of the original context");
				myassert(elseMap == null || elseMap.containsKey(s), "The else context doesn't contain a symbol of the original context");
				if (s.getType() instanceof ArrayType){
					// On regarde tous les éléments du tableau
					for (long index = 0; index < oldMap.get(s).getNbNodes(); index++){
						boolean merge = true;
						Node origNode1 = null;
						Node origNode2 = null;
						Vector<Long> indices = getArrayIndexes(s.getType(),index);
						IndexedSymbol indexedSymbol = new IndexedSymbol(s, indices);
						//System.out.println("Symbol " + s.getName() + " - old val : " + oldMap.get(s).getValueAt(0) + " - then val : " + thenMap.get(s).getValueAt(0) + " - else val : " + elseMap.get(s).getValueAt(0));
						if (elseMap != null && thenMap.get(s).getValueAt(index) != oldMap.get(s).getValueAt(index) && elseMap.get(s).getValueAt(index) != oldMap.get(s).getValueAt(index)){
							// la variable est affectée des deux côtés
							origNode1 = (Node) sfgGenerator.createSymbolNodeWithValue(thenMap,indexedSymbol);
							origNode2 = (Node) sfgGenerator.createSymbolNodeWithValue(elseMap,indexedSymbol);
						}
						else if (thenMap.get(s).getValueAt(index) != oldMap.get(s).getValueAt(index)){
							origNode1 = (Node) sfgGenerator.createSymbolNodeWithValue(oldMap,indexedSymbol);
							origNode2 = (Node) sfgGenerator.createSymbolNodeWithValue(thenMap,indexedSymbol);
						}
						else if (elseMap != null && elseMap.get(s).getValueAt(index) != oldMap.get(s).getValueAt(index)){
							origNode1 = (Node) sfgGenerator.createSymbolNodeWithValue(oldMap,indexedSymbol);
							origNode2 = (Node) sfgGenerator.createSymbolNodeWithValue(elseMap,indexedSymbol);
						}
						else {
							merge = false;
						}
						if (merge){
							//System.out.println("Merging Nodes " + origNode1 + " and " + origNode2);
							Node mergedNode = sfgGenerator.createPhiNode(origNode1, origNode2);
							setNode(s, mergedNode, indices);
							setValue(s, new UninitializedValue(), indices);
						}
						else if(s.getAnnotation("NUM_DATA_CDFG") != null && this.getNode(s) instanceof UninitializedNode && elseMap != null && !(thenMap.get(s).getNodeAt(index) instanceof UninitializedNode) && !(elseMap.get(s).getNodeAt(index) instanceof UninitializedNode)){
							Node nodeThen = (Node)thenMap.get(s).getNodeAt(index);
							Node nodeElse = (Node)elseMap.get(s).getNodeAt(index);
							sfgGenerator.mergeNode(nodeThen, nodeElse);
						}
					}
				}
				else {
					boolean merge = true;
					Node origNode1 = null;
					Node origNode2 = null;

//System.out.println("Symbol " + s.getName() + " - old val : " + oldMap.get(s).getValueAt(0) + " - then val : " + thenMap.get(s).getValueAt(0) + " - else val : " + elseMap.get(s).getValueAt(0));
					if (elseMap != null && thenMap.get(s).getValueAt(0) != oldMap.get(s).getValueAt(0) && elseMap.get(s).getValueAt(0) != oldMap.get(s).getValueAt(0)){
						// la variable est affectée des deux côtés
						origNode1 = (Node) sfgGenerator.createSymbolNodeWithValue(thenMap,s);
						origNode2 = (Node) sfgGenerator.createSymbolNodeWithValue(elseMap,s);
					}
					else if (thenMap.get(s).getValueAt(0) != oldMap.get(s).getValueAt(0)){
						origNode1 = (Node) sfgGenerator.createSymbolNodeWithValue(oldMap,s);
						origNode2 = (Node) sfgGenerator.createSymbolNodeWithValue(thenMap,s);
					}
					else if (elseMap != null && elseMap.get(s).getValueAt(0) != oldMap.get(s).getValueAt(0)){
						origNode1 = (Node) sfgGenerator.createSymbolNodeWithValue(oldMap,s);
						origNode2 = (Node) sfgGenerator.createSymbolNodeWithValue(elseMap,s);
					}
					else {
						merge = false;
					}
					if (merge){
						//System.out.println("Merging Nodes " + origNode1 + " and " + origNode2);
						Node mergedNode = sfgGenerator.createPhiNode(origNode1, origNode2);
						setNode(s, mergedNode);
						setValue(s, new UninitializedValue());
					}
					else if(s.getAnnotation("NUM_DATA_CDFG") != null && this.getNode(s) instanceof UninitializedNode && elseMap != null && !(thenMap.get(s).getNodeAt(0) instanceof UninitializedNode) && !(elseMap.get(s).getNodeAt(0) instanceof UninitializedNode)){
						Node nodeThen = (Node)thenMap.get(s).getNodeAt(0);
						Node nodeElse = (Node)elseMap.get(s).getNodeAt(0);
						sfgGenerator.mergeNode(nodeThen, nodeElse);
					}
				}
			}
		}
	}
		
	
	
	
	public String toString() {
		StringBuffer res = new StringBuffer();
		String str;
		int top = stack.size() - 1;
		for (int i = top; i >= 0; i--) {
			res.append("level " + i + "\n");
			Map<Symbol, SymbolValue> current = stack.get(i);
			for (Symbol s : current.keySet()) {
				if(s.getType().isArray()){
					//System.out.println(s.getType().getSize());
					for(int k = 0 ; k < s.getType().getSize() ; k++){
						str = s.getName()+"["+k+"]" + " = " + current.get(s).getValueAt(k) + " (" + s.hashCode() + ") : " + s.getType() + " : NbNode = " + current.get(s).getNbNodes();
						res.append("\t" + str + "\n");
					}
				}
				else{
					str = s.getName() + " = " + current.get(s).getValueAt(0) + " (" + s.hashCode() + ") : " + s.getType() + " : NbNode = " + current.get(s).getNbNodes();
					res.append("\t" + str + "\n");
				}
				
			}
		}
		return res.toString();
	}
	
	private class SymbolComparator implements Comparator<Symbol>{
		public int compare(Symbol s1, Symbol s2) {
			return s1.getName().compareTo(s2.getName());
		}
	}

}
