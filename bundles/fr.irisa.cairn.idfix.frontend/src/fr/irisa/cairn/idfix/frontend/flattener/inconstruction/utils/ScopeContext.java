package fr.irisa.cairn.idfix.frontend.flattener.inconstruction.utils;

import fr.irisa.cairn.idfix.utils.exceptions.FlattenerException;
import gecos.core.Scope;
import gecos.core.Symbol;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * It is based of the same system as Context but more simple.
 * Each level represent composite block scope without their value associate.
 * @author Nicolas Simon
 * @date Jul 3, 2013
 */
public class ScopeContext {
	private Stack<List<Symbol>> _stack = new Stack<>();
	
	public void push(Scope scope){
		List<Symbol> list = new ArrayList<>();
		for(Symbol s : scope.getSymbols()){
			list.add(s);
		}
		_stack.push(list);
	}
	
	public void pop(){
		_stack.pop();
	}
	

	/**
	 * Permet de récupérer le symbole du context à partir d'un symbole passé en paramètre en effectuant la comparaison sur le nom
	 * @throws FlattenerException 
	 */
	public Symbol getCorrespondingSymbol(String name) throws FlattenerException{
		Symbol res = null;
		int top = _stack.size() - 1;
		
		for (int i = top; i >= 0; i--) {
			List<Symbol> current = _stack.get(i);
			res = getSymbolFromVarName(current, name);
			if (res != null){
				return res;
			}
		}
		
		throw new FlattenerException("Symbol corresponding to a name symbol " + name + " not found in the context");
	}
	
	/**
	 * Pour un niveau donné, recherche le symbole dans ce niveau correspondant à un symbole passé en paramètre (basé sur les noms : sémantique du langage)
	 * @param level le niveau donné du context
	 * @param var le symbol dont on cherche la correspondance
	 * @return le symbole correspondant, null s'il ne se trouve pas à ce niveau
	 */
	private Symbol getSymbolFromVarName(List<Symbol> level, String name){
		for (Symbol sym : level){
			if (sym.getName().equals(name)){
				return sym;
			}
		}
		return null;
	}
}
