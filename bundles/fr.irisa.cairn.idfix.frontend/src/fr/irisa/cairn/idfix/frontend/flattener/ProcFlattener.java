package fr.irisa.cairn.idfix.frontend.flattener;

import fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.core.Procedure;
import gecos.instrs.Instruction;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Interprets a procedure
 * @author Administrateur/qmeunier
 */
public class ProcFlattener {

	private BlockFlattener blockFlattener;
	private InstFlattener instFlattener;
	private ExecutionContext context;
	private ExecutionContext newContext;
	private BlockManager currentBlockManager;
	private SymbolReplacer symbolReplacer;
	private Map<Procedure, BlockManager> managerOfProc;
	
	// Permet de savoir si nous forcons l'interpretation lors de la mise a plat. Si oui, il y aura de possible perte d'information dû à une interpretation trop poussé.
	private boolean forceInterpreting;
	

	public ProcFlattener(ExecutionContext context, ExecutionContext newContext, boolean partial, boolean interpret) {
		this.forceInterpreting = interpret;
		managerOfProc = new LinkedHashMap<Procedure, BlockManager>();
		this.context = context;
		this.newContext = newContext;
		this.symbolReplacer = new SymbolReplacer(newContext);
		this.blockFlattener = new BlockFlattener(this.context, this.newContext, this, partial, symbolReplacer);
		this.instFlattener = new InstFlattener(this.context, this.newContext, this, blockFlattener, symbolReplacer);
	}
	
	private void myassert(boolean b, String s) throws FlattenerException{
		if (!b){
			throw new FlattenerException(s);
		}
	}
	
	public boolean isForceInterpreting(){
		return this.forceInterpreting;
	}
	
	public BlockManager getCurrentBlockManager(){
		return currentBlockManager;
	}
	
	public Map<Procedure, BlockManager> getManagerOfProc(){
		return managerOfProc;
	}
	
	public InstFlattener getInstFlattener(){
		return instFlattener;
	}

	/**
	 * Visit the Procedure <i>proc</i>. The result of the interpretation of a
	 * procedure is the result of the body block (which normally ends with the
	 * interpretation of a return instruction, otherwise the result doesn't
	 * matter).
	 * @throws FlattenerException 
	 */
	public Object doSwitch(Procedure proc) throws FlattenerException {
		Object res;
		myassert((proc != null), "proc is null");
		BlockManager savedManager = currentBlockManager;
		
		BlockManager manager = managerOfProc.get(proc);
		if (manager == null){
			manager = new BlockManager();
			managerOfProc.put(proc, manager);
		}
		
		currentBlockManager = manager;
		
		CompositeBlock funcMainBlock = GecosUserBlockFactory.CompositeBlock();
		manager.setMainBlock(funcMainBlock);
		manager.setCurrCompositeBlock(funcMainBlock);
		//System.out.println("Visiting procedure " + proc.getSymbol().getName());
		res = doSwitch(proc.getBody());
		//System.out.println("End of Visiting procedure " + proc.getSymbol().getName());
		
		currentBlockManager = savedManager;
		return res;
	}


	/**
	 * Call the BlockFlattener to visit blocks
	 */
	public Object doSwitch(Block b) {
		return this.blockFlattener.doSwitch(b);
	}

	/**
	 * Call the InstructionFlattener to visit instructions
	 */
	public Object doSwitch(Instruction i) {
		return this.instFlattener.doSwitch(i);
	}
	
	

}
