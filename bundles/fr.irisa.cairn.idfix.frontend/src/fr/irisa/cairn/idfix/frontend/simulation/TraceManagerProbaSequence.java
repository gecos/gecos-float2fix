
package fr.irisa.cairn.idfix.frontend.simulation;

import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;

import affectationValueBySimulation.AffectationValueBySimulationFactory;
import affectationValueBySimulation.AffectationValues;
import affectationValueBySimulation.ValuesList;
import fr.irisa.cairn.idfix.utils.TraceManager;


class TraceManagerProbaSequence implements TraceManager {

	private int nbCalls;
	private int maxBlockNum;
	
	private List<Sequence> sequences;
	private List<Integer> currSequence;
	
	private ValuesList valuesList;
	
	public TraceManagerProbaSequence(){
		nbCalls = 0;
		sequences = new LinkedList<Sequence>();
		valuesList = AffectationValueBySimulationFactory.eINSTANCE.createValuesList();
	}
	
	
	public ValuesList getValuesList(){
		return valuesList;
	}
	
	
	public void initiateExecution(){
		currSequence = new LinkedList<Integer>();
		nbCalls++;
	}
	
	
	public void terminateExecution(){
		boolean exists = false;
		// On compare la séquence obtenue avec les séquences existantes
		for (Sequence sequence : sequences){
			if (sequence.getBlockSequence().equals(currSequence)){
				exists = true;
				sequence.incrementUse();
				break;
			}
		}
		if (!exists){
			Sequence newSequence = new Sequence(currSequence);
			newSequence.incrementUse();
			sequences.add(newSequence);
		}
	}
	
	
	public void traceBlock(int numBlock){
		currSequence.add(numBlock);
		if (numBlock > maxBlockNum){
			maxBlockNum = numBlock;
		}
	}
	
	
	public void showProbabilities(){
		for (Sequence sequence : sequences){
			System.out.print((double) sequence.getNbCalls() / nbCalls + " | ");
			for (Integer i : sequence.getBlockSequence()){
				System.out.print(i + " ");
			}
			System.out.println();
		}
	}
	
	
	public int getNbBlocks(){
		return maxBlockNum + 1;
	}
	
	/// Appel de add_new_sequence();
	public void printSequences(PrintStream fichierSortie){
		fichierSortie.println("   int nb_total_times = " + nbCalls +";");
		for (Sequence sequence : sequences){
			fichierSortie.print("   add_new_sequence(block_parcours_1, block_parcours_2, id," + sequence.getNbCalls() + ", " + sequence.getBlockSequence().size());
			for (Integer i : sequence.getBlockSequence()){
				fichierSortie.print(", " + i);
			}
			fichierSortie.println(");");
		}
	}
	
	public void traceAffectationValue(int numAffect, double value){
		boolean found = false;
		for (AffectationValues affect : valuesList.getAffectationList()){
			if (affect.getAffectationNumber() == numAffect){
				affect.getValues().add(value);
				found = true;
				break;
			}
		}
		if (!found){
			AffectationValues newAffect = AffectationValueBySimulationFactory.eINSTANCE.createAffectationValues();
			newAffect.setAffectationNumber(numAffect);
			newAffect.getValues().add(value);

			valuesList.getAffectationList().add(newAffect);
		}
	}
	
	
}


