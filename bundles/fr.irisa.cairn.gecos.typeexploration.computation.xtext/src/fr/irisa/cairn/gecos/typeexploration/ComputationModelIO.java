package fr.irisa.cairn.gecos.typeexploration;

import java.io.File;
import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.xtext.resource.SaveOptions;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceFactory;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.serializer.ISerializer;
import org.eclipse.xtext.serializer.impl.Serializer;

import com.google.inject.Injector;

import typeexploration.computation.ComputationModel;
import typeexploration.computation.ComputationPackage;

public class ComputationModelIO {

	private ComputationModelIO() {
	}

	/**
	 * Load the specified file as an Alpha program.
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static ComputationModel loadModel(String filename) throws IOException {

		final Injector injector = new ComputationStandaloneSetup().createInjectorAndDoEMFRegistration();

		final ResourceSet set = injector.getInstance(XtextResourceSet.class);
		set.getPackageRegistry().put(ComputationPackage.eNS_URI, ComputationPackage.eINSTANCE);

		final Resource res = set.getResource(URI.createFileURI(filename), true);

		EObject root = res.getContents().get(0);
		ComputationModel toplevel = (ComputationModel) root;

		return toplevel;
	}
	public static ComputationModel loadModel(File file) throws IOException {
		return loadModel(file.getAbsolutePath());
	}
	
	public static void formatFile(String filename) throws IOException {
		ComputationModel prog = loadModel(filename);

		final Injector injector = new ComputationStandaloneSetup().createInjectorAndDoEMFRegistration();
		final XtextResourceSet set = injector.getInstance(XtextResourceSet.class);

		set.getResourceFactoryRegistry().getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION,
				injector.getInstance(XtextResourceFactory.class));

		SaveOptions opt = SaveOptions.newBuilder().format().getOptions();

		XtextResource resource = (XtextResource) set.createResource(URI.createFileURI(filename));
		resource.getContents().add(prog);
		resource.save(opt.toOptionsMap());
	}

	public static void saveAsXML(ComputationModel seq, String modelfilename) throws IOException {
		final Injector injector = new ComputationStandaloneSetup().createInjectorAndDoEMFRegistration();
		final ResourceSet set = injector.getInstance(XtextResourceSet.class);

		set.getResourceFactoryRegistry().getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION,
				new XMIResourceFactoryImpl());

		Resource resource = set.createResource(URI.createFileURI(modelfilename));
		resource.getContents().add(seq);
		resource.save(null);
	}
	
	public static String unparse(ComputationModel seq) {
		final Injector injector = new ComputationStandaloneSetup().createInjectorAndDoEMFRegistration();
		ISerializer serializer = injector.getInstance(Serializer.class);
		return serializer.serialize(seq);
	}
}
