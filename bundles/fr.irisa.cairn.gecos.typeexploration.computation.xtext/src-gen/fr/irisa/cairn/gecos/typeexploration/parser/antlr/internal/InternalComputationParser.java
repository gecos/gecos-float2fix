package fr.irisa.cairn.gecos.typeexploration.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.irisa.cairn.gecos.typeexploration.services.ComputationGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalComputationParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'param'", "'='", "';'", "'target'", "'{'", "'frequency'", "'outputsPerCycle'", "'problemSize'", "'}'", "'group'", "','", "'block'", "'ADD'", "'MUL'", "':'", "'op'", "'x'", "'max'", "'('", "')'", "'+'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalComputationParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalComputationParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalComputationParser.tokenNames; }
    public String getGrammarFileName() { return "InternalComputation.g"; }



     	private ComputationGrammarAccess grammarAccess;

        public InternalComputationParser(TokenStream input, ComputationGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "ComputationModel";
       	}

       	@Override
       	protected ComputationGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleComputationModel"
    // InternalComputation.g:64:1: entryRuleComputationModel returns [EObject current=null] : iv_ruleComputationModel= ruleComputationModel EOF ;
    public final EObject entryRuleComputationModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComputationModel = null;


        try {
            // InternalComputation.g:64:57: (iv_ruleComputationModel= ruleComputationModel EOF )
            // InternalComputation.g:65:2: iv_ruleComputationModel= ruleComputationModel EOF
            {
             newCompositeNode(grammarAccess.getComputationModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleComputationModel=ruleComputationModel();

            state._fsp--;

             current =iv_ruleComputationModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComputationModel"


    // $ANTLR start "ruleComputationModel"
    // InternalComputation.g:71:1: ruleComputationModel returns [EObject current=null] : ( ( (lv_parameters_0_0= ruleParameter ) )* ( (lv_targetDesign_1_0= ruleHWTargetDesign ) ) ( (lv_blockGroups_2_0= ruleBlockGroup ) )* ( (lv_blocks_3_0= ruleComputationBlock ) )+ ) ;
    public final EObject ruleComputationModel() throws RecognitionException {
        EObject current = null;

        EObject lv_parameters_0_0 = null;

        EObject lv_targetDesign_1_0 = null;

        EObject lv_blockGroups_2_0 = null;

        EObject lv_blocks_3_0 = null;



        	enterRule();

        try {
            // InternalComputation.g:77:2: ( ( ( (lv_parameters_0_0= ruleParameter ) )* ( (lv_targetDesign_1_0= ruleHWTargetDesign ) ) ( (lv_blockGroups_2_0= ruleBlockGroup ) )* ( (lv_blocks_3_0= ruleComputationBlock ) )+ ) )
            // InternalComputation.g:78:2: ( ( (lv_parameters_0_0= ruleParameter ) )* ( (lv_targetDesign_1_0= ruleHWTargetDesign ) ) ( (lv_blockGroups_2_0= ruleBlockGroup ) )* ( (lv_blocks_3_0= ruleComputationBlock ) )+ )
            {
            // InternalComputation.g:78:2: ( ( (lv_parameters_0_0= ruleParameter ) )* ( (lv_targetDesign_1_0= ruleHWTargetDesign ) ) ( (lv_blockGroups_2_0= ruleBlockGroup ) )* ( (lv_blocks_3_0= ruleComputationBlock ) )+ )
            // InternalComputation.g:79:3: ( (lv_parameters_0_0= ruleParameter ) )* ( (lv_targetDesign_1_0= ruleHWTargetDesign ) ) ( (lv_blockGroups_2_0= ruleBlockGroup ) )* ( (lv_blocks_3_0= ruleComputationBlock ) )+
            {
            // InternalComputation.g:79:3: ( (lv_parameters_0_0= ruleParameter ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalComputation.g:80:4: (lv_parameters_0_0= ruleParameter )
            	    {
            	    // InternalComputation.g:80:4: (lv_parameters_0_0= ruleParameter )
            	    // InternalComputation.g:81:5: lv_parameters_0_0= ruleParameter
            	    {

            	    					newCompositeNode(grammarAccess.getComputationModelAccess().getParametersParameterParserRuleCall_0_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_parameters_0_0=ruleParameter();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getComputationModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"parameters",
            	    						lv_parameters_0_0,
            	    						"fr.irisa.cairn.gecos.typeexploration.Computation.Parameter");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalComputation.g:98:3: ( (lv_targetDesign_1_0= ruleHWTargetDesign ) )
            // InternalComputation.g:99:4: (lv_targetDesign_1_0= ruleHWTargetDesign )
            {
            // InternalComputation.g:99:4: (lv_targetDesign_1_0= ruleHWTargetDesign )
            // InternalComputation.g:100:5: lv_targetDesign_1_0= ruleHWTargetDesign
            {

            					newCompositeNode(grammarAccess.getComputationModelAccess().getTargetDesignHWTargetDesignParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_targetDesign_1_0=ruleHWTargetDesign();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getComputationModelRule());
            					}
            					set(
            						current,
            						"targetDesign",
            						lv_targetDesign_1_0,
            						"fr.irisa.cairn.gecos.typeexploration.Computation.HWTargetDesign");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalComputation.g:117:3: ( (lv_blockGroups_2_0= ruleBlockGroup ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==20) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalComputation.g:118:4: (lv_blockGroups_2_0= ruleBlockGroup )
            	    {
            	    // InternalComputation.g:118:4: (lv_blockGroups_2_0= ruleBlockGroup )
            	    // InternalComputation.g:119:5: lv_blockGroups_2_0= ruleBlockGroup
            	    {

            	    					newCompositeNode(grammarAccess.getComputationModelAccess().getBlockGroupsBlockGroupParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_4);
            	    lv_blockGroups_2_0=ruleBlockGroup();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getComputationModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"blockGroups",
            	    						lv_blockGroups_2_0,
            	    						"fr.irisa.cairn.gecos.typeexploration.Computation.BlockGroup");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // InternalComputation.g:136:3: ( (lv_blocks_3_0= ruleComputationBlock ) )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==22) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalComputation.g:137:4: (lv_blocks_3_0= ruleComputationBlock )
            	    {
            	    // InternalComputation.g:137:4: (lv_blocks_3_0= ruleComputationBlock )
            	    // InternalComputation.g:138:5: lv_blocks_3_0= ruleComputationBlock
            	    {

            	    					newCompositeNode(grammarAccess.getComputationModelAccess().getBlocksComputationBlockParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_5);
            	    lv_blocks_3_0=ruleComputationBlock();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getComputationModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"blocks",
            	    						lv_blocks_3_0,
            	    						"fr.irisa.cairn.gecos.typeexploration.Computation.ComputationBlock");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComputationModel"


    // $ANTLR start "entryRuleParameter"
    // InternalComputation.g:159:1: entryRuleParameter returns [EObject current=null] : iv_ruleParameter= ruleParameter EOF ;
    public final EObject entryRuleParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameter = null;


        try {
            // InternalComputation.g:159:50: (iv_ruleParameter= ruleParameter EOF )
            // InternalComputation.g:160:2: iv_ruleParameter= ruleParameter EOF
            {
             newCompositeNode(grammarAccess.getParameterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleParameter=ruleParameter();

            state._fsp--;

             current =iv_ruleParameter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameter"


    // $ANTLR start "ruleParameter"
    // InternalComputation.g:166:1: ruleParameter returns [EObject current=null] : (otherlv_0= 'param' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_INT ) ) otherlv_4= ';' ) ;
    public final EObject ruleParameter() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_value_3_0=null;
        Token otherlv_4=null;


        	enterRule();

        try {
            // InternalComputation.g:172:2: ( (otherlv_0= 'param' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_INT ) ) otherlv_4= ';' ) )
            // InternalComputation.g:173:2: (otherlv_0= 'param' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_INT ) ) otherlv_4= ';' )
            {
            // InternalComputation.g:173:2: (otherlv_0= 'param' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_INT ) ) otherlv_4= ';' )
            // InternalComputation.g:174:3: otherlv_0= 'param' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_INT ) ) otherlv_4= ';'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getParameterAccess().getParamKeyword_0());
            		
            // InternalComputation.g:178:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalComputation.g:179:4: (lv_name_1_0= RULE_ID )
            {
            // InternalComputation.g:179:4: (lv_name_1_0= RULE_ID )
            // InternalComputation.g:180:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_7); 

            					newLeafNode(lv_name_1_0, grammarAccess.getParameterAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getParameterRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_8); 

            			newLeafNode(otherlv_2, grammarAccess.getParameterAccess().getEqualsSignKeyword_2());
            		
            // InternalComputation.g:200:3: ( (lv_value_3_0= RULE_INT ) )
            // InternalComputation.g:201:4: (lv_value_3_0= RULE_INT )
            {
            // InternalComputation.g:201:4: (lv_value_3_0= RULE_INT )
            // InternalComputation.g:202:5: lv_value_3_0= RULE_INT
            {
            lv_value_3_0=(Token)match(input,RULE_INT,FOLLOW_9); 

            					newLeafNode(lv_value_3_0, grammarAccess.getParameterAccess().getValueINTTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getParameterRule());
            					}
            					setWithLastConsumed(
            						current,
            						"value",
            						lv_value_3_0,
            						"org.eclipse.xtext.common.Terminals.INT");
            				

            }


            }

            otherlv_4=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getParameterAccess().getSemicolonKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameter"


    // $ANTLR start "entryRuleHWTargetDesign"
    // InternalComputation.g:226:1: entryRuleHWTargetDesign returns [EObject current=null] : iv_ruleHWTargetDesign= ruleHWTargetDesign EOF ;
    public final EObject entryRuleHWTargetDesign() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleHWTargetDesign = null;


        try {
            // InternalComputation.g:226:55: (iv_ruleHWTargetDesign= ruleHWTargetDesign EOF )
            // InternalComputation.g:227:2: iv_ruleHWTargetDesign= ruleHWTargetDesign EOF
            {
             newCompositeNode(grammarAccess.getHWTargetDesignRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleHWTargetDesign=ruleHWTargetDesign();

            state._fsp--;

             current =iv_ruleHWTargetDesign; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHWTargetDesign"


    // $ANTLR start "ruleHWTargetDesign"
    // InternalComputation.g:233:1: ruleHWTargetDesign returns [EObject current=null] : (otherlv_0= 'target' otherlv_1= '{' otherlv_2= 'frequency' otherlv_3= '=' ( (lv_frequency_4_0= RULE_INT ) ) otherlv_5= 'outputsPerCycle' otherlv_6= '=' ( (lv_outputsPerCycle_7_0= RULE_INT ) ) otherlv_8= 'problemSize' otherlv_9= '=' ( (lv_problemSizeExpr_10_0= ruleSummationExpression ) ) otherlv_11= '}' ) ;
    public final EObject ruleHWTargetDesign() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_frequency_4_0=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token lv_outputsPerCycle_7_0=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        EObject lv_problemSizeExpr_10_0 = null;



        	enterRule();

        try {
            // InternalComputation.g:239:2: ( (otherlv_0= 'target' otherlv_1= '{' otherlv_2= 'frequency' otherlv_3= '=' ( (lv_frequency_4_0= RULE_INT ) ) otherlv_5= 'outputsPerCycle' otherlv_6= '=' ( (lv_outputsPerCycle_7_0= RULE_INT ) ) otherlv_8= 'problemSize' otherlv_9= '=' ( (lv_problemSizeExpr_10_0= ruleSummationExpression ) ) otherlv_11= '}' ) )
            // InternalComputation.g:240:2: (otherlv_0= 'target' otherlv_1= '{' otherlv_2= 'frequency' otherlv_3= '=' ( (lv_frequency_4_0= RULE_INT ) ) otherlv_5= 'outputsPerCycle' otherlv_6= '=' ( (lv_outputsPerCycle_7_0= RULE_INT ) ) otherlv_8= 'problemSize' otherlv_9= '=' ( (lv_problemSizeExpr_10_0= ruleSummationExpression ) ) otherlv_11= '}' )
            {
            // InternalComputation.g:240:2: (otherlv_0= 'target' otherlv_1= '{' otherlv_2= 'frequency' otherlv_3= '=' ( (lv_frequency_4_0= RULE_INT ) ) otherlv_5= 'outputsPerCycle' otherlv_6= '=' ( (lv_outputsPerCycle_7_0= RULE_INT ) ) otherlv_8= 'problemSize' otherlv_9= '=' ( (lv_problemSizeExpr_10_0= ruleSummationExpression ) ) otherlv_11= '}' )
            // InternalComputation.g:241:3: otherlv_0= 'target' otherlv_1= '{' otherlv_2= 'frequency' otherlv_3= '=' ( (lv_frequency_4_0= RULE_INT ) ) otherlv_5= 'outputsPerCycle' otherlv_6= '=' ( (lv_outputsPerCycle_7_0= RULE_INT ) ) otherlv_8= 'problemSize' otherlv_9= '=' ( (lv_problemSizeExpr_10_0= ruleSummationExpression ) ) otherlv_11= '}'
            {
            otherlv_0=(Token)match(input,14,FOLLOW_10); 

            			newLeafNode(otherlv_0, grammarAccess.getHWTargetDesignAccess().getTargetKeyword_0());
            		
            otherlv_1=(Token)match(input,15,FOLLOW_11); 

            			newLeafNode(otherlv_1, grammarAccess.getHWTargetDesignAccess().getLeftCurlyBracketKeyword_1());
            		
            otherlv_2=(Token)match(input,16,FOLLOW_7); 

            			newLeafNode(otherlv_2, grammarAccess.getHWTargetDesignAccess().getFrequencyKeyword_2());
            		
            otherlv_3=(Token)match(input,12,FOLLOW_8); 

            			newLeafNode(otherlv_3, grammarAccess.getHWTargetDesignAccess().getEqualsSignKeyword_3());
            		
            // InternalComputation.g:257:3: ( (lv_frequency_4_0= RULE_INT ) )
            // InternalComputation.g:258:4: (lv_frequency_4_0= RULE_INT )
            {
            // InternalComputation.g:258:4: (lv_frequency_4_0= RULE_INT )
            // InternalComputation.g:259:5: lv_frequency_4_0= RULE_INT
            {
            lv_frequency_4_0=(Token)match(input,RULE_INT,FOLLOW_12); 

            					newLeafNode(lv_frequency_4_0, grammarAccess.getHWTargetDesignAccess().getFrequencyINTTerminalRuleCall_4_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getHWTargetDesignRule());
            					}
            					setWithLastConsumed(
            						current,
            						"frequency",
            						lv_frequency_4_0,
            						"org.eclipse.xtext.common.Terminals.INT");
            				

            }


            }

            otherlv_5=(Token)match(input,17,FOLLOW_7); 

            			newLeafNode(otherlv_5, grammarAccess.getHWTargetDesignAccess().getOutputsPerCycleKeyword_5());
            		
            otherlv_6=(Token)match(input,12,FOLLOW_8); 

            			newLeafNode(otherlv_6, grammarAccess.getHWTargetDesignAccess().getEqualsSignKeyword_6());
            		
            // InternalComputation.g:283:3: ( (lv_outputsPerCycle_7_0= RULE_INT ) )
            // InternalComputation.g:284:4: (lv_outputsPerCycle_7_0= RULE_INT )
            {
            // InternalComputation.g:284:4: (lv_outputsPerCycle_7_0= RULE_INT )
            // InternalComputation.g:285:5: lv_outputsPerCycle_7_0= RULE_INT
            {
            lv_outputsPerCycle_7_0=(Token)match(input,RULE_INT,FOLLOW_13); 

            					newLeafNode(lv_outputsPerCycle_7_0, grammarAccess.getHWTargetDesignAccess().getOutputsPerCycleINTTerminalRuleCall_7_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getHWTargetDesignRule());
            					}
            					setWithLastConsumed(
            						current,
            						"outputsPerCycle",
            						lv_outputsPerCycle_7_0,
            						"org.eclipse.xtext.common.Terminals.INT");
            				

            }


            }

            otherlv_8=(Token)match(input,18,FOLLOW_7); 

            			newLeafNode(otherlv_8, grammarAccess.getHWTargetDesignAccess().getProblemSizeKeyword_8());
            		
            otherlv_9=(Token)match(input,12,FOLLOW_14); 

            			newLeafNode(otherlv_9, grammarAccess.getHWTargetDesignAccess().getEqualsSignKeyword_9());
            		
            // InternalComputation.g:309:3: ( (lv_problemSizeExpr_10_0= ruleSummationExpression ) )
            // InternalComputation.g:310:4: (lv_problemSizeExpr_10_0= ruleSummationExpression )
            {
            // InternalComputation.g:310:4: (lv_problemSizeExpr_10_0= ruleSummationExpression )
            // InternalComputation.g:311:5: lv_problemSizeExpr_10_0= ruleSummationExpression
            {

            					newCompositeNode(grammarAccess.getHWTargetDesignAccess().getProblemSizeExprSummationExpressionParserRuleCall_10_0());
            				
            pushFollow(FOLLOW_15);
            lv_problemSizeExpr_10_0=ruleSummationExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getHWTargetDesignRule());
            					}
            					set(
            						current,
            						"problemSizeExpr",
            						lv_problemSizeExpr_10_0,
            						"fr.irisa.cairn.gecos.typeexploration.Computation.SummationExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_11=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_11, grammarAccess.getHWTargetDesignAccess().getRightCurlyBracketKeyword_11());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHWTargetDesign"


    // $ANTLR start "entryRuleBlockGroup"
    // InternalComputation.g:336:1: entryRuleBlockGroup returns [EObject current=null] : iv_ruleBlockGroup= ruleBlockGroup EOF ;
    public final EObject entryRuleBlockGroup() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBlockGroup = null;


        try {
            // InternalComputation.g:336:51: (iv_ruleBlockGroup= ruleBlockGroup EOF )
            // InternalComputation.g:337:2: iv_ruleBlockGroup= ruleBlockGroup EOF
            {
             newCompositeNode(grammarAccess.getBlockGroupRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBlockGroup=ruleBlockGroup();

            state._fsp--;

             current =iv_ruleBlockGroup; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBlockGroup"


    // $ANTLR start "ruleBlockGroup"
    // InternalComputation.g:343:1: ruleBlockGroup returns [EObject current=null] : (otherlv_0= 'group' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* otherlv_6= '}' ) ;
    public final EObject ruleBlockGroup() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;


        	enterRule();

        try {
            // InternalComputation.g:349:2: ( (otherlv_0= 'group' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* otherlv_6= '}' ) )
            // InternalComputation.g:350:2: (otherlv_0= 'group' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* otherlv_6= '}' )
            {
            // InternalComputation.g:350:2: (otherlv_0= 'group' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* otherlv_6= '}' )
            // InternalComputation.g:351:3: otherlv_0= 'group' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,20,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getBlockGroupAccess().getGroupKeyword_0());
            		
            // InternalComputation.g:355:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalComputation.g:356:4: (lv_name_1_0= RULE_ID )
            {
            // InternalComputation.g:356:4: (lv_name_1_0= RULE_ID )
            // InternalComputation.g:357:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_10); 

            					newLeafNode(lv_name_1_0, grammarAccess.getBlockGroupAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getBlockGroupRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,15,FOLLOW_6); 

            			newLeafNode(otherlv_2, grammarAccess.getBlockGroupAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalComputation.g:377:3: ( (otherlv_3= RULE_ID ) )
            // InternalComputation.g:378:4: (otherlv_3= RULE_ID )
            {
            // InternalComputation.g:378:4: (otherlv_3= RULE_ID )
            // InternalComputation.g:379:5: otherlv_3= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getBlockGroupRule());
            					}
            				
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_16); 

            					newLeafNode(otherlv_3, grammarAccess.getBlockGroupAccess().getBlocksComputationBlockCrossReference_3_0());
            				

            }


            }

            // InternalComputation.g:390:3: (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==21) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalComputation.g:391:4: otherlv_4= ',' ( (otherlv_5= RULE_ID ) )
            	    {
            	    otherlv_4=(Token)match(input,21,FOLLOW_6); 

            	    				newLeafNode(otherlv_4, grammarAccess.getBlockGroupAccess().getCommaKeyword_4_0());
            	    			
            	    // InternalComputation.g:395:4: ( (otherlv_5= RULE_ID ) )
            	    // InternalComputation.g:396:5: (otherlv_5= RULE_ID )
            	    {
            	    // InternalComputation.g:396:5: (otherlv_5= RULE_ID )
            	    // InternalComputation.g:397:6: otherlv_5= RULE_ID
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getBlockGroupRule());
            	    						}
            	    					
            	    otherlv_5=(Token)match(input,RULE_ID,FOLLOW_16); 

            	    						newLeafNode(otherlv_5, grammarAccess.getBlockGroupAccess().getBlocksComputationBlockCrossReference_4_1_0());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_6=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getBlockGroupAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBlockGroup"


    // $ANTLR start "entryRuleComputationBlock"
    // InternalComputation.g:417:1: entryRuleComputationBlock returns [EObject current=null] : iv_ruleComputationBlock= ruleComputationBlock EOF ;
    public final EObject entryRuleComputationBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComputationBlock = null;


        try {
            // InternalComputation.g:417:57: (iv_ruleComputationBlock= ruleComputationBlock EOF )
            // InternalComputation.g:418:2: iv_ruleComputationBlock= ruleComputationBlock EOF
            {
             newCompositeNode(grammarAccess.getComputationBlockRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleComputationBlock=ruleComputationBlock();

            state._fsp--;

             current =iv_ruleComputationBlock; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComputationBlock"


    // $ANTLR start "ruleComputationBlock"
    // InternalComputation.g:424:1: ruleComputationBlock returns [EObject current=null] : (otherlv_0= 'block' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_operations_3_0= ruleOperation ) )+ otherlv_4= '}' ) ;
    public final EObject ruleComputationBlock() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_operations_3_0 = null;



        	enterRule();

        try {
            // InternalComputation.g:430:2: ( (otherlv_0= 'block' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_operations_3_0= ruleOperation ) )+ otherlv_4= '}' ) )
            // InternalComputation.g:431:2: (otherlv_0= 'block' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_operations_3_0= ruleOperation ) )+ otherlv_4= '}' )
            {
            // InternalComputation.g:431:2: (otherlv_0= 'block' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_operations_3_0= ruleOperation ) )+ otherlv_4= '}' )
            // InternalComputation.g:432:3: otherlv_0= 'block' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_operations_3_0= ruleOperation ) )+ otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,22,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getComputationBlockAccess().getBlockKeyword_0());
            		
            // InternalComputation.g:436:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalComputation.g:437:4: (lv_name_1_0= RULE_ID )
            {
            // InternalComputation.g:437:4: (lv_name_1_0= RULE_ID )
            // InternalComputation.g:438:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_10); 

            					newLeafNode(lv_name_1_0, grammarAccess.getComputationBlockAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getComputationBlockRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,15,FOLLOW_17); 

            			newLeafNode(otherlv_2, grammarAccess.getComputationBlockAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalComputation.g:458:3: ( (lv_operations_3_0= ruleOperation ) )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>=23 && LA5_0<=24)) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalComputation.g:459:4: (lv_operations_3_0= ruleOperation )
            	    {
            	    // InternalComputation.g:459:4: (lv_operations_3_0= ruleOperation )
            	    // InternalComputation.g:460:5: lv_operations_3_0= ruleOperation
            	    {

            	    					newCompositeNode(grammarAccess.getComputationBlockAccess().getOperationsOperationParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_18);
            	    lv_operations_3_0=ruleOperation();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getComputationBlockRule());
            	    					}
            	    					add(
            	    						current,
            	    						"operations",
            	    						lv_operations_3_0,
            	    						"fr.irisa.cairn.gecos.typeexploration.Computation.Operation");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);

            otherlv_4=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getComputationBlockAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComputationBlock"


    // $ANTLR start "entryRuleOP"
    // InternalComputation.g:485:1: entryRuleOP returns [String current=null] : iv_ruleOP= ruleOP EOF ;
    public final String entryRuleOP() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOP = null;


        try {
            // InternalComputation.g:485:42: (iv_ruleOP= ruleOP EOF )
            // InternalComputation.g:486:2: iv_ruleOP= ruleOP EOF
            {
             newCompositeNode(grammarAccess.getOPRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOP=ruleOP();

            state._fsp--;

             current =iv_ruleOP.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOP"


    // $ANTLR start "ruleOP"
    // InternalComputation.g:492:1: ruleOP returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'ADD' | kw= 'MUL' ) ;
    public final AntlrDatatypeRuleToken ruleOP() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalComputation.g:498:2: ( (kw= 'ADD' | kw= 'MUL' ) )
            // InternalComputation.g:499:2: (kw= 'ADD' | kw= 'MUL' )
            {
            // InternalComputation.g:499:2: (kw= 'ADD' | kw= 'MUL' )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==23) ) {
                alt6=1;
            }
            else if ( (LA6_0==24) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalComputation.g:500:3: kw= 'ADD'
                    {
                    kw=(Token)match(input,23,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOPAccess().getADDKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalComputation.g:506:3: kw= 'MUL'
                    {
                    kw=(Token)match(input,24,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOPAccess().getMULKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOP"


    // $ANTLR start "entryRuleOperation"
    // InternalComputation.g:515:1: entryRuleOperation returns [EObject current=null] : iv_ruleOperation= ruleOperation EOF ;
    public final EObject entryRuleOperation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOperation = null;


        try {
            // InternalComputation.g:515:50: (iv_ruleOperation= ruleOperation EOF )
            // InternalComputation.g:516:2: iv_ruleOperation= ruleOperation EOF
            {
             newCompositeNode(grammarAccess.getOperationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOperation=ruleOperation();

            state._fsp--;

             current =iv_ruleOperation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOperation"


    // $ANTLR start "ruleOperation"
    // InternalComputation.g:522:1: ruleOperation returns [EObject current=null] : ( ( (lv_opType_0_0= ruleOP ) ) otherlv_1= ':' ( (lv_outputExpr_2_0= ruleOperandExpression ) ) otherlv_3= '=' ( (lv_inputExpr1_4_0= ruleOperandExpression ) ) otherlv_5= 'op' ( (lv_inputExpr2_6_0= ruleOperandExpression ) ) otherlv_7= 'x' ( (lv_numOps_8_0= ruleSummationExpression ) ) otherlv_9= ';' ) ;
    public final EObject ruleOperation() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        AntlrDatatypeRuleToken lv_opType_0_0 = null;

        EObject lv_outputExpr_2_0 = null;

        EObject lv_inputExpr1_4_0 = null;

        EObject lv_inputExpr2_6_0 = null;

        EObject lv_numOps_8_0 = null;



        	enterRule();

        try {
            // InternalComputation.g:528:2: ( ( ( (lv_opType_0_0= ruleOP ) ) otherlv_1= ':' ( (lv_outputExpr_2_0= ruleOperandExpression ) ) otherlv_3= '=' ( (lv_inputExpr1_4_0= ruleOperandExpression ) ) otherlv_5= 'op' ( (lv_inputExpr2_6_0= ruleOperandExpression ) ) otherlv_7= 'x' ( (lv_numOps_8_0= ruleSummationExpression ) ) otherlv_9= ';' ) )
            // InternalComputation.g:529:2: ( ( (lv_opType_0_0= ruleOP ) ) otherlv_1= ':' ( (lv_outputExpr_2_0= ruleOperandExpression ) ) otherlv_3= '=' ( (lv_inputExpr1_4_0= ruleOperandExpression ) ) otherlv_5= 'op' ( (lv_inputExpr2_6_0= ruleOperandExpression ) ) otherlv_7= 'x' ( (lv_numOps_8_0= ruleSummationExpression ) ) otherlv_9= ';' )
            {
            // InternalComputation.g:529:2: ( ( (lv_opType_0_0= ruleOP ) ) otherlv_1= ':' ( (lv_outputExpr_2_0= ruleOperandExpression ) ) otherlv_3= '=' ( (lv_inputExpr1_4_0= ruleOperandExpression ) ) otherlv_5= 'op' ( (lv_inputExpr2_6_0= ruleOperandExpression ) ) otherlv_7= 'x' ( (lv_numOps_8_0= ruleSummationExpression ) ) otherlv_9= ';' )
            // InternalComputation.g:530:3: ( (lv_opType_0_0= ruleOP ) ) otherlv_1= ':' ( (lv_outputExpr_2_0= ruleOperandExpression ) ) otherlv_3= '=' ( (lv_inputExpr1_4_0= ruleOperandExpression ) ) otherlv_5= 'op' ( (lv_inputExpr2_6_0= ruleOperandExpression ) ) otherlv_7= 'x' ( (lv_numOps_8_0= ruleSummationExpression ) ) otherlv_9= ';'
            {
            // InternalComputation.g:530:3: ( (lv_opType_0_0= ruleOP ) )
            // InternalComputation.g:531:4: (lv_opType_0_0= ruleOP )
            {
            // InternalComputation.g:531:4: (lv_opType_0_0= ruleOP )
            // InternalComputation.g:532:5: lv_opType_0_0= ruleOP
            {

            					newCompositeNode(grammarAccess.getOperationAccess().getOpTypeOPParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_19);
            lv_opType_0_0=ruleOP();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getOperationRule());
            					}
            					set(
            						current,
            						"opType",
            						lv_opType_0_0,
            						"fr.irisa.cairn.gecos.typeexploration.Computation.OP");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,25,FOLLOW_20); 

            			newLeafNode(otherlv_1, grammarAccess.getOperationAccess().getColonKeyword_1());
            		
            // InternalComputation.g:553:3: ( (lv_outputExpr_2_0= ruleOperandExpression ) )
            // InternalComputation.g:554:4: (lv_outputExpr_2_0= ruleOperandExpression )
            {
            // InternalComputation.g:554:4: (lv_outputExpr_2_0= ruleOperandExpression )
            // InternalComputation.g:555:5: lv_outputExpr_2_0= ruleOperandExpression
            {

            					newCompositeNode(grammarAccess.getOperationAccess().getOutputExprOperandExpressionParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_7);
            lv_outputExpr_2_0=ruleOperandExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getOperationRule());
            					}
            					set(
            						current,
            						"outputExpr",
            						lv_outputExpr_2_0,
            						"fr.irisa.cairn.gecos.typeexploration.Computation.OperandExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_20); 

            			newLeafNode(otherlv_3, grammarAccess.getOperationAccess().getEqualsSignKeyword_3());
            		
            // InternalComputation.g:576:3: ( (lv_inputExpr1_4_0= ruleOperandExpression ) )
            // InternalComputation.g:577:4: (lv_inputExpr1_4_0= ruleOperandExpression )
            {
            // InternalComputation.g:577:4: (lv_inputExpr1_4_0= ruleOperandExpression )
            // InternalComputation.g:578:5: lv_inputExpr1_4_0= ruleOperandExpression
            {

            					newCompositeNode(grammarAccess.getOperationAccess().getInputExpr1OperandExpressionParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_21);
            lv_inputExpr1_4_0=ruleOperandExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getOperationRule());
            					}
            					set(
            						current,
            						"inputExpr1",
            						lv_inputExpr1_4_0,
            						"fr.irisa.cairn.gecos.typeexploration.Computation.OperandExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,26,FOLLOW_20); 

            			newLeafNode(otherlv_5, grammarAccess.getOperationAccess().getOpKeyword_5());
            		
            // InternalComputation.g:599:3: ( (lv_inputExpr2_6_0= ruleOperandExpression ) )
            // InternalComputation.g:600:4: (lv_inputExpr2_6_0= ruleOperandExpression )
            {
            // InternalComputation.g:600:4: (lv_inputExpr2_6_0= ruleOperandExpression )
            // InternalComputation.g:601:5: lv_inputExpr2_6_0= ruleOperandExpression
            {

            					newCompositeNode(grammarAccess.getOperationAccess().getInputExpr2OperandExpressionParserRuleCall_6_0());
            				
            pushFollow(FOLLOW_22);
            lv_inputExpr2_6_0=ruleOperandExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getOperationRule());
            					}
            					set(
            						current,
            						"inputExpr2",
            						lv_inputExpr2_6_0,
            						"fr.irisa.cairn.gecos.typeexploration.Computation.OperandExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_7=(Token)match(input,27,FOLLOW_14); 

            			newLeafNode(otherlv_7, grammarAccess.getOperationAccess().getXKeyword_7());
            		
            // InternalComputation.g:622:3: ( (lv_numOps_8_0= ruleSummationExpression ) )
            // InternalComputation.g:623:4: (lv_numOps_8_0= ruleSummationExpression )
            {
            // InternalComputation.g:623:4: (lv_numOps_8_0= ruleSummationExpression )
            // InternalComputation.g:624:5: lv_numOps_8_0= ruleSummationExpression
            {

            					newCompositeNode(grammarAccess.getOperationAccess().getNumOpsSummationExpressionParserRuleCall_8_0());
            				
            pushFollow(FOLLOW_9);
            lv_numOps_8_0=ruleSummationExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getOperationRule());
            					}
            					set(
            						current,
            						"numOps",
            						lv_numOps_8_0,
            						"fr.irisa.cairn.gecos.typeexploration.Computation.SummationExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_9=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getOperationAccess().getSemicolonKeyword_9());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOperation"


    // $ANTLR start "entryRuleOperandExpression"
    // InternalComputation.g:649:1: entryRuleOperandExpression returns [EObject current=null] : iv_ruleOperandExpression= ruleOperandExpression EOF ;
    public final EObject entryRuleOperandExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOperandExpression = null;


        try {
            // InternalComputation.g:649:58: (iv_ruleOperandExpression= ruleOperandExpression EOF )
            // InternalComputation.g:650:2: iv_ruleOperandExpression= ruleOperandExpression EOF
            {
             newCompositeNode(grammarAccess.getOperandExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOperandExpression=ruleOperandExpression();

            state._fsp--;

             current =iv_ruleOperandExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOperandExpression"


    // $ANTLR start "ruleOperandExpression"
    // InternalComputation.g:656:1: ruleOperandExpression returns [EObject current=null] : (this_MaxExpression_0= ruleMaxExpression | this_TerminalOperandExpression_1= ruleTerminalOperandExpression ) ;
    public final EObject ruleOperandExpression() throws RecognitionException {
        EObject current = null;

        EObject this_MaxExpression_0 = null;

        EObject this_TerminalOperandExpression_1 = null;



        	enterRule();

        try {
            // InternalComputation.g:662:2: ( (this_MaxExpression_0= ruleMaxExpression | this_TerminalOperandExpression_1= ruleTerminalOperandExpression ) )
            // InternalComputation.g:663:2: (this_MaxExpression_0= ruleMaxExpression | this_TerminalOperandExpression_1= ruleTerminalOperandExpression )
            {
            // InternalComputation.g:663:2: (this_MaxExpression_0= ruleMaxExpression | this_TerminalOperandExpression_1= ruleTerminalOperandExpression )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==28) ) {
                alt7=1;
            }
            else if ( ((LA7_0>=RULE_ID && LA7_0<=RULE_INT)||LA7_0==29) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalComputation.g:664:3: this_MaxExpression_0= ruleMaxExpression
                    {

                    			newCompositeNode(grammarAccess.getOperandExpressionAccess().getMaxExpressionParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_MaxExpression_0=ruleMaxExpression();

                    state._fsp--;


                    			current = this_MaxExpression_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalComputation.g:673:3: this_TerminalOperandExpression_1= ruleTerminalOperandExpression
                    {

                    			newCompositeNode(grammarAccess.getOperandExpressionAccess().getTerminalOperandExpressionParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_TerminalOperandExpression_1=ruleTerminalOperandExpression();

                    state._fsp--;


                    			current = this_TerminalOperandExpression_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOperandExpression"


    // $ANTLR start "entryRuleMaxExpression"
    // InternalComputation.g:685:1: entryRuleMaxExpression returns [EObject current=null] : iv_ruleMaxExpression= ruleMaxExpression EOF ;
    public final EObject entryRuleMaxExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMaxExpression = null;


        try {
            // InternalComputation.g:685:54: (iv_ruleMaxExpression= ruleMaxExpression EOF )
            // InternalComputation.g:686:2: iv_ruleMaxExpression= ruleMaxExpression EOF
            {
             newCompositeNode(grammarAccess.getMaxExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMaxExpression=ruleMaxExpression();

            state._fsp--;

             current =iv_ruleMaxExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMaxExpression"


    // $ANTLR start "ruleMaxExpression"
    // InternalComputation.g:692:1: ruleMaxExpression returns [EObject current=null] : (otherlv_0= 'max' otherlv_1= '(' ( (lv_exprs_2_0= ruleTerminalOperandExpression ) ) (otherlv_3= ',' ( (lv_exprs_4_0= ruleTerminalOperandExpression ) ) )+ otherlv_5= ')' ) ;
    public final EObject ruleMaxExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_exprs_2_0 = null;

        EObject lv_exprs_4_0 = null;



        	enterRule();

        try {
            // InternalComputation.g:698:2: ( (otherlv_0= 'max' otherlv_1= '(' ( (lv_exprs_2_0= ruleTerminalOperandExpression ) ) (otherlv_3= ',' ( (lv_exprs_4_0= ruleTerminalOperandExpression ) ) )+ otherlv_5= ')' ) )
            // InternalComputation.g:699:2: (otherlv_0= 'max' otherlv_1= '(' ( (lv_exprs_2_0= ruleTerminalOperandExpression ) ) (otherlv_3= ',' ( (lv_exprs_4_0= ruleTerminalOperandExpression ) ) )+ otherlv_5= ')' )
            {
            // InternalComputation.g:699:2: (otherlv_0= 'max' otherlv_1= '(' ( (lv_exprs_2_0= ruleTerminalOperandExpression ) ) (otherlv_3= ',' ( (lv_exprs_4_0= ruleTerminalOperandExpression ) ) )+ otherlv_5= ')' )
            // InternalComputation.g:700:3: otherlv_0= 'max' otherlv_1= '(' ( (lv_exprs_2_0= ruleTerminalOperandExpression ) ) (otherlv_3= ',' ( (lv_exprs_4_0= ruleTerminalOperandExpression ) ) )+ otherlv_5= ')'
            {
            otherlv_0=(Token)match(input,28,FOLLOW_23); 

            			newLeafNode(otherlv_0, grammarAccess.getMaxExpressionAccess().getMaxKeyword_0());
            		
            otherlv_1=(Token)match(input,29,FOLLOW_20); 

            			newLeafNode(otherlv_1, grammarAccess.getMaxExpressionAccess().getLeftParenthesisKeyword_1());
            		
            // InternalComputation.g:708:3: ( (lv_exprs_2_0= ruleTerminalOperandExpression ) )
            // InternalComputation.g:709:4: (lv_exprs_2_0= ruleTerminalOperandExpression )
            {
            // InternalComputation.g:709:4: (lv_exprs_2_0= ruleTerminalOperandExpression )
            // InternalComputation.g:710:5: lv_exprs_2_0= ruleTerminalOperandExpression
            {

            					newCompositeNode(grammarAccess.getMaxExpressionAccess().getExprsTerminalOperandExpressionParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_24);
            lv_exprs_2_0=ruleTerminalOperandExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getMaxExpressionRule());
            					}
            					add(
            						current,
            						"exprs",
            						lv_exprs_2_0,
            						"fr.irisa.cairn.gecos.typeexploration.Computation.TerminalOperandExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalComputation.g:727:3: (otherlv_3= ',' ( (lv_exprs_4_0= ruleTerminalOperandExpression ) ) )+
            int cnt8=0;
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==21) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalComputation.g:728:4: otherlv_3= ',' ( (lv_exprs_4_0= ruleTerminalOperandExpression ) )
            	    {
            	    otherlv_3=(Token)match(input,21,FOLLOW_20); 

            	    				newLeafNode(otherlv_3, grammarAccess.getMaxExpressionAccess().getCommaKeyword_3_0());
            	    			
            	    // InternalComputation.g:732:4: ( (lv_exprs_4_0= ruleTerminalOperandExpression ) )
            	    // InternalComputation.g:733:5: (lv_exprs_4_0= ruleTerminalOperandExpression )
            	    {
            	    // InternalComputation.g:733:5: (lv_exprs_4_0= ruleTerminalOperandExpression )
            	    // InternalComputation.g:734:6: lv_exprs_4_0= ruleTerminalOperandExpression
            	    {

            	    						newCompositeNode(grammarAccess.getMaxExpressionAccess().getExprsTerminalOperandExpressionParserRuleCall_3_1_0());
            	    					
            	    pushFollow(FOLLOW_25);
            	    lv_exprs_4_0=ruleTerminalOperandExpression();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getMaxExpressionRule());
            	    						}
            	    						add(
            	    							current,
            	    							"exprs",
            	    							lv_exprs_4_0,
            	    							"fr.irisa.cairn.gecos.typeexploration.Computation.TerminalOperandExpression");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt8 >= 1 ) break loop8;
                        EarlyExitException eee =
                            new EarlyExitException(8, input);
                        throw eee;
                }
                cnt8++;
            } while (true);

            otherlv_5=(Token)match(input,30,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getMaxExpressionAccess().getRightParenthesisKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMaxExpression"


    // $ANTLR start "entryRuleTerminalOperandExpression"
    // InternalComputation.g:760:1: entryRuleTerminalOperandExpression returns [EObject current=null] : iv_ruleTerminalOperandExpression= ruleTerminalOperandExpression EOF ;
    public final EObject entryRuleTerminalOperandExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTerminalOperandExpression = null;


        try {
            // InternalComputation.g:760:66: (iv_ruleTerminalOperandExpression= ruleTerminalOperandExpression EOF )
            // InternalComputation.g:761:2: iv_ruleTerminalOperandExpression= ruleTerminalOperandExpression EOF
            {
             newCompositeNode(grammarAccess.getTerminalOperandExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTerminalOperandExpression=ruleTerminalOperandExpression();

            state._fsp--;

             current =iv_ruleTerminalOperandExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTerminalOperandExpression"


    // $ANTLR start "ruleTerminalOperandExpression"
    // InternalComputation.g:767:1: ruleTerminalOperandExpression returns [EObject current=null] : (this_AddExpression_0= ruleAddExpression | this_OperandTerm_1= ruleOperandTerm ) ;
    public final EObject ruleTerminalOperandExpression() throws RecognitionException {
        EObject current = null;

        EObject this_AddExpression_0 = null;

        EObject this_OperandTerm_1 = null;



        	enterRule();

        try {
            // InternalComputation.g:773:2: ( (this_AddExpression_0= ruleAddExpression | this_OperandTerm_1= ruleOperandTerm ) )
            // InternalComputation.g:774:2: (this_AddExpression_0= ruleAddExpression | this_OperandTerm_1= ruleOperandTerm )
            {
            // InternalComputation.g:774:2: (this_AddExpression_0= ruleAddExpression | this_OperandTerm_1= ruleOperandTerm )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==29) ) {
                alt9=1;
            }
            else if ( ((LA9_0>=RULE_ID && LA9_0<=RULE_INT)) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalComputation.g:775:3: this_AddExpression_0= ruleAddExpression
                    {

                    			newCompositeNode(grammarAccess.getTerminalOperandExpressionAccess().getAddExpressionParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_AddExpression_0=ruleAddExpression();

                    state._fsp--;


                    			current = this_AddExpression_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalComputation.g:784:3: this_OperandTerm_1= ruleOperandTerm
                    {

                    			newCompositeNode(grammarAccess.getTerminalOperandExpressionAccess().getOperandTermParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_OperandTerm_1=ruleOperandTerm();

                    state._fsp--;


                    			current = this_OperandTerm_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTerminalOperandExpression"


    // $ANTLR start "entryRuleAddExpression"
    // InternalComputation.g:796:1: entryRuleAddExpression returns [EObject current=null] : iv_ruleAddExpression= ruleAddExpression EOF ;
    public final EObject entryRuleAddExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAddExpression = null;


        try {
            // InternalComputation.g:796:54: (iv_ruleAddExpression= ruleAddExpression EOF )
            // InternalComputation.g:797:2: iv_ruleAddExpression= ruleAddExpression EOF
            {
             newCompositeNode(grammarAccess.getAddExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAddExpression=ruleAddExpression();

            state._fsp--;

             current =iv_ruleAddExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAddExpression"


    // $ANTLR start "ruleAddExpression"
    // InternalComputation.g:803:1: ruleAddExpression returns [EObject current=null] : (otherlv_0= '(' ( (lv_op1_1_0= ruleOperandExpression ) ) otherlv_2= '+' ( (lv_op2_3_0= ruleOperandExpression ) ) otherlv_4= ')' ) ;
    public final EObject ruleAddExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_op1_1_0 = null;

        EObject lv_op2_3_0 = null;



        	enterRule();

        try {
            // InternalComputation.g:809:2: ( (otherlv_0= '(' ( (lv_op1_1_0= ruleOperandExpression ) ) otherlv_2= '+' ( (lv_op2_3_0= ruleOperandExpression ) ) otherlv_4= ')' ) )
            // InternalComputation.g:810:2: (otherlv_0= '(' ( (lv_op1_1_0= ruleOperandExpression ) ) otherlv_2= '+' ( (lv_op2_3_0= ruleOperandExpression ) ) otherlv_4= ')' )
            {
            // InternalComputation.g:810:2: (otherlv_0= '(' ( (lv_op1_1_0= ruleOperandExpression ) ) otherlv_2= '+' ( (lv_op2_3_0= ruleOperandExpression ) ) otherlv_4= ')' )
            // InternalComputation.g:811:3: otherlv_0= '(' ( (lv_op1_1_0= ruleOperandExpression ) ) otherlv_2= '+' ( (lv_op2_3_0= ruleOperandExpression ) ) otherlv_4= ')'
            {
            otherlv_0=(Token)match(input,29,FOLLOW_20); 

            			newLeafNode(otherlv_0, grammarAccess.getAddExpressionAccess().getLeftParenthesisKeyword_0());
            		
            // InternalComputation.g:815:3: ( (lv_op1_1_0= ruleOperandExpression ) )
            // InternalComputation.g:816:4: (lv_op1_1_0= ruleOperandExpression )
            {
            // InternalComputation.g:816:4: (lv_op1_1_0= ruleOperandExpression )
            // InternalComputation.g:817:5: lv_op1_1_0= ruleOperandExpression
            {

            					newCompositeNode(grammarAccess.getAddExpressionAccess().getOp1OperandExpressionParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_26);
            lv_op1_1_0=ruleOperandExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAddExpressionRule());
            					}
            					set(
            						current,
            						"op1",
            						lv_op1_1_0,
            						"fr.irisa.cairn.gecos.typeexploration.Computation.OperandExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,31,FOLLOW_20); 

            			newLeafNode(otherlv_2, grammarAccess.getAddExpressionAccess().getPlusSignKeyword_2());
            		
            // InternalComputation.g:838:3: ( (lv_op2_3_0= ruleOperandExpression ) )
            // InternalComputation.g:839:4: (lv_op2_3_0= ruleOperandExpression )
            {
            // InternalComputation.g:839:4: (lv_op2_3_0= ruleOperandExpression )
            // InternalComputation.g:840:5: lv_op2_3_0= ruleOperandExpression
            {

            					newCompositeNode(grammarAccess.getAddExpressionAccess().getOp2OperandExpressionParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_27);
            lv_op2_3_0=ruleOperandExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAddExpressionRule());
            					}
            					set(
            						current,
            						"op2",
            						lv_op2_3_0,
            						"fr.irisa.cairn.gecos.typeexploration.Computation.OperandExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,30,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getAddExpressionAccess().getRightParenthesisKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAddExpression"


    // $ANTLR start "entryRuleOperandTerm"
    // InternalComputation.g:865:1: entryRuleOperandTerm returns [EObject current=null] : iv_ruleOperandTerm= ruleOperandTerm EOF ;
    public final EObject entryRuleOperandTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOperandTerm = null;


        try {
            // InternalComputation.g:865:52: (iv_ruleOperandTerm= ruleOperandTerm EOF )
            // InternalComputation.g:866:2: iv_ruleOperandTerm= ruleOperandTerm EOF
            {
             newCompositeNode(grammarAccess.getOperandTermRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOperandTerm=ruleOperandTerm();

            state._fsp--;

             current =iv_ruleOperandTerm; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOperandTerm"


    // $ANTLR start "ruleOperandTerm"
    // InternalComputation.g:872:1: ruleOperandTerm returns [EObject current=null] : ( ( (lv_coef_0_0= RULE_INT ) )? ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleOperandTerm() throws RecognitionException {
        EObject current = null;

        Token lv_coef_0_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalComputation.g:878:2: ( ( ( (lv_coef_0_0= RULE_INT ) )? ( (lv_name_1_0= RULE_ID ) ) ) )
            // InternalComputation.g:879:2: ( ( (lv_coef_0_0= RULE_INT ) )? ( (lv_name_1_0= RULE_ID ) ) )
            {
            // InternalComputation.g:879:2: ( ( (lv_coef_0_0= RULE_INT ) )? ( (lv_name_1_0= RULE_ID ) ) )
            // InternalComputation.g:880:3: ( (lv_coef_0_0= RULE_INT ) )? ( (lv_name_1_0= RULE_ID ) )
            {
            // InternalComputation.g:880:3: ( (lv_coef_0_0= RULE_INT ) )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==RULE_INT) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalComputation.g:881:4: (lv_coef_0_0= RULE_INT )
                    {
                    // InternalComputation.g:881:4: (lv_coef_0_0= RULE_INT )
                    // InternalComputation.g:882:5: lv_coef_0_0= RULE_INT
                    {
                    lv_coef_0_0=(Token)match(input,RULE_INT,FOLLOW_6); 

                    					newLeafNode(lv_coef_0_0, grammarAccess.getOperandTermAccess().getCoefINTTerminalRuleCall_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getOperandTermRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"coef",
                    						lv_coef_0_0,
                    						"org.eclipse.xtext.common.Terminals.INT");
                    				

                    }


                    }
                    break;

            }

            // InternalComputation.g:898:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalComputation.g:899:4: (lv_name_1_0= RULE_ID )
            {
            // InternalComputation.g:899:4: (lv_name_1_0= RULE_ID )
            // InternalComputation.g:900:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getOperandTermAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOperandTermRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOperandTerm"


    // $ANTLR start "entryRuleSummationExpression"
    // InternalComputation.g:920:1: entryRuleSummationExpression returns [EObject current=null] : iv_ruleSummationExpression= ruleSummationExpression EOF ;
    public final EObject entryRuleSummationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSummationExpression = null;


        try {
            // InternalComputation.g:920:60: (iv_ruleSummationExpression= ruleSummationExpression EOF )
            // InternalComputation.g:921:2: iv_ruleSummationExpression= ruleSummationExpression EOF
            {
             newCompositeNode(grammarAccess.getSummationExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSummationExpression=ruleSummationExpression();

            state._fsp--;

             current =iv_ruleSummationExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSummationExpression"


    // $ANTLR start "ruleSummationExpression"
    // InternalComputation.g:927:1: ruleSummationExpression returns [EObject current=null] : ( ( ( ( (lv_terms_0_0= ruleProductExpression ) ) (otherlv_1= '+' ( (lv_terms_2_0= ruleProductExpression ) ) )* ) (otherlv_3= '+' ( (lv_constant_4_0= RULE_INT ) ) )? ) | ( (lv_constant_5_0= RULE_INT ) ) ) ;
    public final EObject ruleSummationExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_constant_4_0=null;
        Token lv_constant_5_0=null;
        EObject lv_terms_0_0 = null;

        EObject lv_terms_2_0 = null;



        	enterRule();

        try {
            // InternalComputation.g:933:2: ( ( ( ( ( (lv_terms_0_0= ruleProductExpression ) ) (otherlv_1= '+' ( (lv_terms_2_0= ruleProductExpression ) ) )* ) (otherlv_3= '+' ( (lv_constant_4_0= RULE_INT ) ) )? ) | ( (lv_constant_5_0= RULE_INT ) ) ) )
            // InternalComputation.g:934:2: ( ( ( ( (lv_terms_0_0= ruleProductExpression ) ) (otherlv_1= '+' ( (lv_terms_2_0= ruleProductExpression ) ) )* ) (otherlv_3= '+' ( (lv_constant_4_0= RULE_INT ) ) )? ) | ( (lv_constant_5_0= RULE_INT ) ) )
            {
            // InternalComputation.g:934:2: ( ( ( ( (lv_terms_0_0= ruleProductExpression ) ) (otherlv_1= '+' ( (lv_terms_2_0= ruleProductExpression ) ) )* ) (otherlv_3= '+' ( (lv_constant_4_0= RULE_INT ) ) )? ) | ( (lv_constant_5_0= RULE_INT ) ) )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==RULE_INT) ) {
                int LA13_1 = input.LA(2);

                if ( (LA13_1==EOF||LA13_1==13||LA13_1==19) ) {
                    alt13=2;
                }
                else if ( (LA13_1==RULE_ID) ) {
                    alt13=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 13, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA13_0==RULE_ID) ) {
                alt13=1;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // InternalComputation.g:935:3: ( ( ( (lv_terms_0_0= ruleProductExpression ) ) (otherlv_1= '+' ( (lv_terms_2_0= ruleProductExpression ) ) )* ) (otherlv_3= '+' ( (lv_constant_4_0= RULE_INT ) ) )? )
                    {
                    // InternalComputation.g:935:3: ( ( ( (lv_terms_0_0= ruleProductExpression ) ) (otherlv_1= '+' ( (lv_terms_2_0= ruleProductExpression ) ) )* ) (otherlv_3= '+' ( (lv_constant_4_0= RULE_INT ) ) )? )
                    // InternalComputation.g:936:4: ( ( (lv_terms_0_0= ruleProductExpression ) ) (otherlv_1= '+' ( (lv_terms_2_0= ruleProductExpression ) ) )* ) (otherlv_3= '+' ( (lv_constant_4_0= RULE_INT ) ) )?
                    {
                    // InternalComputation.g:936:4: ( ( (lv_terms_0_0= ruleProductExpression ) ) (otherlv_1= '+' ( (lv_terms_2_0= ruleProductExpression ) ) )* )
                    // InternalComputation.g:937:5: ( (lv_terms_0_0= ruleProductExpression ) ) (otherlv_1= '+' ( (lv_terms_2_0= ruleProductExpression ) ) )*
                    {
                    // InternalComputation.g:937:5: ( (lv_terms_0_0= ruleProductExpression ) )
                    // InternalComputation.g:938:6: (lv_terms_0_0= ruleProductExpression )
                    {
                    // InternalComputation.g:938:6: (lv_terms_0_0= ruleProductExpression )
                    // InternalComputation.g:939:7: lv_terms_0_0= ruleProductExpression
                    {

                    							newCompositeNode(grammarAccess.getSummationExpressionAccess().getTermsProductExpressionParserRuleCall_0_0_0_0());
                    						
                    pushFollow(FOLLOW_28);
                    lv_terms_0_0=ruleProductExpression();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getSummationExpressionRule());
                    							}
                    							add(
                    								current,
                    								"terms",
                    								lv_terms_0_0,
                    								"fr.irisa.cairn.gecos.typeexploration.Computation.ProductExpression");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    // InternalComputation.g:956:5: (otherlv_1= '+' ( (lv_terms_2_0= ruleProductExpression ) ) )*
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( (LA11_0==31) ) {
                            int LA11_1 = input.LA(2);

                            if ( (LA11_1==RULE_INT) ) {
                                int LA11_3 = input.LA(3);

                                if ( (LA11_3==RULE_ID) ) {
                                    alt11=1;
                                }


                            }
                            else if ( (LA11_1==RULE_ID) ) {
                                alt11=1;
                            }


                        }


                        switch (alt11) {
                    	case 1 :
                    	    // InternalComputation.g:957:6: otherlv_1= '+' ( (lv_terms_2_0= ruleProductExpression ) )
                    	    {
                    	    otherlv_1=(Token)match(input,31,FOLLOW_14); 

                    	    						newLeafNode(otherlv_1, grammarAccess.getSummationExpressionAccess().getPlusSignKeyword_0_0_1_0());
                    	    					
                    	    // InternalComputation.g:961:6: ( (lv_terms_2_0= ruleProductExpression ) )
                    	    // InternalComputation.g:962:7: (lv_terms_2_0= ruleProductExpression )
                    	    {
                    	    // InternalComputation.g:962:7: (lv_terms_2_0= ruleProductExpression )
                    	    // InternalComputation.g:963:8: lv_terms_2_0= ruleProductExpression
                    	    {

                    	    								newCompositeNode(grammarAccess.getSummationExpressionAccess().getTermsProductExpressionParserRuleCall_0_0_1_1_0());
                    	    							
                    	    pushFollow(FOLLOW_28);
                    	    lv_terms_2_0=ruleProductExpression();

                    	    state._fsp--;


                    	    								if (current==null) {
                    	    									current = createModelElementForParent(grammarAccess.getSummationExpressionRule());
                    	    								}
                    	    								add(
                    	    									current,
                    	    									"terms",
                    	    									lv_terms_2_0,
                    	    									"fr.irisa.cairn.gecos.typeexploration.Computation.ProductExpression");
                    	    								afterParserOrEnumRuleCall();
                    	    							

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop11;
                        }
                    } while (true);


                    }

                    // InternalComputation.g:982:4: (otherlv_3= '+' ( (lv_constant_4_0= RULE_INT ) ) )?
                    int alt12=2;
                    int LA12_0 = input.LA(1);

                    if ( (LA12_0==31) ) {
                        alt12=1;
                    }
                    switch (alt12) {
                        case 1 :
                            // InternalComputation.g:983:5: otherlv_3= '+' ( (lv_constant_4_0= RULE_INT ) )
                            {
                            otherlv_3=(Token)match(input,31,FOLLOW_8); 

                            					newLeafNode(otherlv_3, grammarAccess.getSummationExpressionAccess().getPlusSignKeyword_0_1_0());
                            				
                            // InternalComputation.g:987:5: ( (lv_constant_4_0= RULE_INT ) )
                            // InternalComputation.g:988:6: (lv_constant_4_0= RULE_INT )
                            {
                            // InternalComputation.g:988:6: (lv_constant_4_0= RULE_INT )
                            // InternalComputation.g:989:7: lv_constant_4_0= RULE_INT
                            {
                            lv_constant_4_0=(Token)match(input,RULE_INT,FOLLOW_2); 

                            							newLeafNode(lv_constant_4_0, grammarAccess.getSummationExpressionAccess().getConstantINTTerminalRuleCall_0_1_1_0());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getSummationExpressionRule());
                            							}
                            							setWithLastConsumed(
                            								current,
                            								"constant",
                            								lv_constant_4_0,
                            								"org.eclipse.xtext.common.Terminals.INT");
                            						

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalComputation.g:1008:3: ( (lv_constant_5_0= RULE_INT ) )
                    {
                    // InternalComputation.g:1008:3: ( (lv_constant_5_0= RULE_INT ) )
                    // InternalComputation.g:1009:4: (lv_constant_5_0= RULE_INT )
                    {
                    // InternalComputation.g:1009:4: (lv_constant_5_0= RULE_INT )
                    // InternalComputation.g:1010:5: lv_constant_5_0= RULE_INT
                    {
                    lv_constant_5_0=(Token)match(input,RULE_INT,FOLLOW_2); 

                    					newLeafNode(lv_constant_5_0, grammarAccess.getSummationExpressionAccess().getConstantINTTerminalRuleCall_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getSummationExpressionRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"constant",
                    						lv_constant_5_0,
                    						"org.eclipse.xtext.common.Terminals.INT");
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSummationExpression"


    // $ANTLR start "entryRuleProductExpression"
    // InternalComputation.g:1030:1: entryRuleProductExpression returns [EObject current=null] : iv_ruleProductExpression= ruleProductExpression EOF ;
    public final EObject entryRuleProductExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProductExpression = null;


        try {
            // InternalComputation.g:1030:58: (iv_ruleProductExpression= ruleProductExpression EOF )
            // InternalComputation.g:1031:2: iv_ruleProductExpression= ruleProductExpression EOF
            {
             newCompositeNode(grammarAccess.getProductExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleProductExpression=ruleProductExpression();

            state._fsp--;

             current =iv_ruleProductExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProductExpression"


    // $ANTLR start "ruleProductExpression"
    // InternalComputation.g:1037:1: ruleProductExpression returns [EObject current=null] : ( ( (lv_constant_0_0= RULE_INT ) )? ( ( (otherlv_1= RULE_ID ) ) ( (otherlv_2= RULE_ID ) )* ) ) ;
    public final EObject ruleProductExpression() throws RecognitionException {
        EObject current = null;

        Token lv_constant_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalComputation.g:1043:2: ( ( ( (lv_constant_0_0= RULE_INT ) )? ( ( (otherlv_1= RULE_ID ) ) ( (otherlv_2= RULE_ID ) )* ) ) )
            // InternalComputation.g:1044:2: ( ( (lv_constant_0_0= RULE_INT ) )? ( ( (otherlv_1= RULE_ID ) ) ( (otherlv_2= RULE_ID ) )* ) )
            {
            // InternalComputation.g:1044:2: ( ( (lv_constant_0_0= RULE_INT ) )? ( ( (otherlv_1= RULE_ID ) ) ( (otherlv_2= RULE_ID ) )* ) )
            // InternalComputation.g:1045:3: ( (lv_constant_0_0= RULE_INT ) )? ( ( (otherlv_1= RULE_ID ) ) ( (otherlv_2= RULE_ID ) )* )
            {
            // InternalComputation.g:1045:3: ( (lv_constant_0_0= RULE_INT ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==RULE_INT) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalComputation.g:1046:4: (lv_constant_0_0= RULE_INT )
                    {
                    // InternalComputation.g:1046:4: (lv_constant_0_0= RULE_INT )
                    // InternalComputation.g:1047:5: lv_constant_0_0= RULE_INT
                    {
                    lv_constant_0_0=(Token)match(input,RULE_INT,FOLLOW_6); 

                    					newLeafNode(lv_constant_0_0, grammarAccess.getProductExpressionAccess().getConstantINTTerminalRuleCall_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getProductExpressionRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"constant",
                    						lv_constant_0_0,
                    						"org.eclipse.xtext.common.Terminals.INT");
                    				

                    }


                    }
                    break;

            }

            // InternalComputation.g:1063:3: ( ( (otherlv_1= RULE_ID ) ) ( (otherlv_2= RULE_ID ) )* )
            // InternalComputation.g:1064:4: ( (otherlv_1= RULE_ID ) ) ( (otherlv_2= RULE_ID ) )*
            {
            // InternalComputation.g:1064:4: ( (otherlv_1= RULE_ID ) )
            // InternalComputation.g:1065:5: (otherlv_1= RULE_ID )
            {
            // InternalComputation.g:1065:5: (otherlv_1= RULE_ID )
            // InternalComputation.g:1066:6: otherlv_1= RULE_ID
            {

            						if (current==null) {
            							current = createModelElement(grammarAccess.getProductExpressionRule());
            						}
            					
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_29); 

            						newLeafNode(otherlv_1, grammarAccess.getProductExpressionAccess().getTermsParameterCrossReference_1_0_0());
            					

            }


            }

            // InternalComputation.g:1077:4: ( (otherlv_2= RULE_ID ) )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==RULE_ID) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalComputation.g:1078:5: (otherlv_2= RULE_ID )
            	    {
            	    // InternalComputation.g:1078:5: (otherlv_2= RULE_ID )
            	    // InternalComputation.g:1079:6: otherlv_2= RULE_ID
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getProductExpressionRule());
            	    						}
            	    					
            	    otherlv_2=(Token)match(input,RULE_ID,FOLLOW_29); 

            	    						newLeafNode(otherlv_2, grammarAccess.getProductExpressionAccess().getTermsParameterCrossReference_1_1_0());
            	    					

            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProductExpression"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000004800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000500000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000500002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000280000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000001800000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000001880000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000030000030L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000040200000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000080000002L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000000000012L});

}