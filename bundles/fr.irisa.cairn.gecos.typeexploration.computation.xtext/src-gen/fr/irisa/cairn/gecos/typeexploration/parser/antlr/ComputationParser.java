/*
 * generated by Xtext 2.14.0
 */
package fr.irisa.cairn.gecos.typeexploration.parser.antlr;

import com.google.inject.Inject;
import fr.irisa.cairn.gecos.typeexploration.parser.antlr.internal.InternalComputationParser;
import fr.irisa.cairn.gecos.typeexploration.services.ComputationGrammarAccess;
import org.eclipse.xtext.parser.antlr.AbstractAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;

public class ComputationParser extends AbstractAntlrParser {

	@Inject
	private ComputationGrammarAccess grammarAccess;

	@Override
	protected void setInitialHiddenTokens(XtextTokenStream tokenStream) {
		tokenStream.setInitialHiddenTokens("RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT");
	}
	

	@Override
	protected InternalComputationParser createParser(XtextTokenStream stream) {
		return new InternalComputationParser(stream, getGrammarAccess());
	}

	@Override 
	protected String getDefaultRuleName() {
		return "ComputationModel";
	}

	public ComputationGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}

	public void setGrammarAccess(ComputationGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
}
