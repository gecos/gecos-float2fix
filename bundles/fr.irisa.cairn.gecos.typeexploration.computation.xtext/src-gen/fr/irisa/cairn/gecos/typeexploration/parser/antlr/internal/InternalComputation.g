/*
 * generated by Xtext 2.14.0
 */
grammar InternalComputation;

options {
	superClass=AbstractInternalAntlrParser;
}

@lexer::header {
package fr.irisa.cairn.gecos.typeexploration.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;
}

@parser::header {
package fr.irisa.cairn.gecos.typeexploration.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.irisa.cairn.gecos.typeexploration.services.ComputationGrammarAccess;

}

@parser::members {

 	private ComputationGrammarAccess grammarAccess;

    public InternalComputationParser(TokenStream input, ComputationGrammarAccess grammarAccess) {
        this(input);
        this.grammarAccess = grammarAccess;
        registerRules(grammarAccess.getGrammar());
    }

    @Override
    protected String getFirstRuleName() {
    	return "ComputationModel";
   	}

   	@Override
   	protected ComputationGrammarAccess getGrammarAccess() {
   		return grammarAccess;
   	}

}

@rulecatch {
    catch (RecognitionException re) {
        recover(input,re);
        appendSkippedTokens();
    }
}

// Entry rule entryRuleComputationModel
entryRuleComputationModel returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getComputationModelRule()); }
	iv_ruleComputationModel=ruleComputationModel
	{ $current=$iv_ruleComputationModel.current; }
	EOF;

// Rule ComputationModel
ruleComputationModel returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			(
				{
					newCompositeNode(grammarAccess.getComputationModelAccess().getParametersParameterParserRuleCall_0_0());
				}
				lv_parameters_0_0=ruleParameter
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getComputationModelRule());
					}
					add(
						$current,
						"parameters",
						lv_parameters_0_0,
						"fr.irisa.cairn.gecos.typeexploration.Computation.Parameter");
					afterParserOrEnumRuleCall();
				}
			)
		)*
		(
			(
				{
					newCompositeNode(grammarAccess.getComputationModelAccess().getTargetDesignHWTargetDesignParserRuleCall_1_0());
				}
				lv_targetDesign_1_0=ruleHWTargetDesign
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getComputationModelRule());
					}
					set(
						$current,
						"targetDesign",
						lv_targetDesign_1_0,
						"fr.irisa.cairn.gecos.typeexploration.Computation.HWTargetDesign");
					afterParserOrEnumRuleCall();
				}
			)
		)
		(
			(
				{
					newCompositeNode(grammarAccess.getComputationModelAccess().getBlockGroupsBlockGroupParserRuleCall_2_0());
				}
				lv_blockGroups_2_0=ruleBlockGroup
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getComputationModelRule());
					}
					add(
						$current,
						"blockGroups",
						lv_blockGroups_2_0,
						"fr.irisa.cairn.gecos.typeexploration.Computation.BlockGroup");
					afterParserOrEnumRuleCall();
				}
			)
		)*
		(
			(
				{
					newCompositeNode(grammarAccess.getComputationModelAccess().getBlocksComputationBlockParserRuleCall_3_0());
				}
				lv_blocks_3_0=ruleComputationBlock
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getComputationModelRule());
					}
					add(
						$current,
						"blocks",
						lv_blocks_3_0,
						"fr.irisa.cairn.gecos.typeexploration.Computation.ComputationBlock");
					afterParserOrEnumRuleCall();
				}
			)
		)+
	)
;

// Entry rule entryRuleParameter
entryRuleParameter returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getParameterRule()); }
	iv_ruleParameter=ruleParameter
	{ $current=$iv_ruleParameter.current; }
	EOF;

// Rule Parameter
ruleParameter returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='param'
		{
			newLeafNode(otherlv_0, grammarAccess.getParameterAccess().getParamKeyword_0());
		}
		(
			(
				lv_name_1_0=RULE_ID
				{
					newLeafNode(lv_name_1_0, grammarAccess.getParameterAccess().getNameIDTerminalRuleCall_1_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getParameterRule());
					}
					setWithLastConsumed(
						$current,
						"name",
						lv_name_1_0,
						"org.eclipse.xtext.common.Terminals.ID");
				}
			)
		)
		otherlv_2='='
		{
			newLeafNode(otherlv_2, grammarAccess.getParameterAccess().getEqualsSignKeyword_2());
		}
		(
			(
				lv_value_3_0=RULE_INT
				{
					newLeafNode(lv_value_3_0, grammarAccess.getParameterAccess().getValueINTTerminalRuleCall_3_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getParameterRule());
					}
					setWithLastConsumed(
						$current,
						"value",
						lv_value_3_0,
						"org.eclipse.xtext.common.Terminals.INT");
				}
			)
		)
		otherlv_4=';'
		{
			newLeafNode(otherlv_4, grammarAccess.getParameterAccess().getSemicolonKeyword_4());
		}
	)
;

// Entry rule entryRuleHWTargetDesign
entryRuleHWTargetDesign returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getHWTargetDesignRule()); }
	iv_ruleHWTargetDesign=ruleHWTargetDesign
	{ $current=$iv_ruleHWTargetDesign.current; }
	EOF;

// Rule HWTargetDesign
ruleHWTargetDesign returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='target'
		{
			newLeafNode(otherlv_0, grammarAccess.getHWTargetDesignAccess().getTargetKeyword_0());
		}
		otherlv_1='{'
		{
			newLeafNode(otherlv_1, grammarAccess.getHWTargetDesignAccess().getLeftCurlyBracketKeyword_1());
		}
		otherlv_2='frequency'
		{
			newLeafNode(otherlv_2, grammarAccess.getHWTargetDesignAccess().getFrequencyKeyword_2());
		}
		otherlv_3='='
		{
			newLeafNode(otherlv_3, grammarAccess.getHWTargetDesignAccess().getEqualsSignKeyword_3());
		}
		(
			(
				lv_frequency_4_0=RULE_INT
				{
					newLeafNode(lv_frequency_4_0, grammarAccess.getHWTargetDesignAccess().getFrequencyINTTerminalRuleCall_4_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getHWTargetDesignRule());
					}
					setWithLastConsumed(
						$current,
						"frequency",
						lv_frequency_4_0,
						"org.eclipse.xtext.common.Terminals.INT");
				}
			)
		)
		otherlv_5='outputsPerCycle'
		{
			newLeafNode(otherlv_5, grammarAccess.getHWTargetDesignAccess().getOutputsPerCycleKeyword_5());
		}
		otherlv_6='='
		{
			newLeafNode(otherlv_6, grammarAccess.getHWTargetDesignAccess().getEqualsSignKeyword_6());
		}
		(
			(
				lv_outputsPerCycle_7_0=RULE_INT
				{
					newLeafNode(lv_outputsPerCycle_7_0, grammarAccess.getHWTargetDesignAccess().getOutputsPerCycleINTTerminalRuleCall_7_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getHWTargetDesignRule());
					}
					setWithLastConsumed(
						$current,
						"outputsPerCycle",
						lv_outputsPerCycle_7_0,
						"org.eclipse.xtext.common.Terminals.INT");
				}
			)
		)
		otherlv_8='problemSize'
		{
			newLeafNode(otherlv_8, grammarAccess.getHWTargetDesignAccess().getProblemSizeKeyword_8());
		}
		otherlv_9='='
		{
			newLeafNode(otherlv_9, grammarAccess.getHWTargetDesignAccess().getEqualsSignKeyword_9());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getHWTargetDesignAccess().getProblemSizeExprSummationExpressionParserRuleCall_10_0());
				}
				lv_problemSizeExpr_10_0=ruleSummationExpression
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getHWTargetDesignRule());
					}
					set(
						$current,
						"problemSizeExpr",
						lv_problemSizeExpr_10_0,
						"fr.irisa.cairn.gecos.typeexploration.Computation.SummationExpression");
					afterParserOrEnumRuleCall();
				}
			)
		)
		otherlv_11='}'
		{
			newLeafNode(otherlv_11, grammarAccess.getHWTargetDesignAccess().getRightCurlyBracketKeyword_11());
		}
	)
;

// Entry rule entryRuleBlockGroup
entryRuleBlockGroup returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getBlockGroupRule()); }
	iv_ruleBlockGroup=ruleBlockGroup
	{ $current=$iv_ruleBlockGroup.current; }
	EOF;

// Rule BlockGroup
ruleBlockGroup returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='group'
		{
			newLeafNode(otherlv_0, grammarAccess.getBlockGroupAccess().getGroupKeyword_0());
		}
		(
			(
				lv_name_1_0=RULE_ID
				{
					newLeafNode(lv_name_1_0, grammarAccess.getBlockGroupAccess().getNameIDTerminalRuleCall_1_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getBlockGroupRule());
					}
					setWithLastConsumed(
						$current,
						"name",
						lv_name_1_0,
						"org.eclipse.xtext.common.Terminals.ID");
				}
			)
		)
		otherlv_2='{'
		{
			newLeafNode(otherlv_2, grammarAccess.getBlockGroupAccess().getLeftCurlyBracketKeyword_2());
		}
		(
			(
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getBlockGroupRule());
					}
				}
				otherlv_3=RULE_ID
				{
					newLeafNode(otherlv_3, grammarAccess.getBlockGroupAccess().getBlocksComputationBlockCrossReference_3_0());
				}
			)
		)
		(
			otherlv_4=','
			{
				newLeafNode(otherlv_4, grammarAccess.getBlockGroupAccess().getCommaKeyword_4_0());
			}
			(
				(
					{
						if ($current==null) {
							$current = createModelElement(grammarAccess.getBlockGroupRule());
						}
					}
					otherlv_5=RULE_ID
					{
						newLeafNode(otherlv_5, grammarAccess.getBlockGroupAccess().getBlocksComputationBlockCrossReference_4_1_0());
					}
				)
			)
		)*
		otherlv_6='}'
		{
			newLeafNode(otherlv_6, grammarAccess.getBlockGroupAccess().getRightCurlyBracketKeyword_5());
		}
	)
;

// Entry rule entryRuleComputationBlock
entryRuleComputationBlock returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getComputationBlockRule()); }
	iv_ruleComputationBlock=ruleComputationBlock
	{ $current=$iv_ruleComputationBlock.current; }
	EOF;

// Rule ComputationBlock
ruleComputationBlock returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='block'
		{
			newLeafNode(otherlv_0, grammarAccess.getComputationBlockAccess().getBlockKeyword_0());
		}
		(
			(
				lv_name_1_0=RULE_ID
				{
					newLeafNode(lv_name_1_0, grammarAccess.getComputationBlockAccess().getNameIDTerminalRuleCall_1_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getComputationBlockRule());
					}
					setWithLastConsumed(
						$current,
						"name",
						lv_name_1_0,
						"org.eclipse.xtext.common.Terminals.ID");
				}
			)
		)
		otherlv_2='{'
		{
			newLeafNode(otherlv_2, grammarAccess.getComputationBlockAccess().getLeftCurlyBracketKeyword_2());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getComputationBlockAccess().getOperationsOperationParserRuleCall_3_0());
				}
				lv_operations_3_0=ruleOperation
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getComputationBlockRule());
					}
					add(
						$current,
						"operations",
						lv_operations_3_0,
						"fr.irisa.cairn.gecos.typeexploration.Computation.Operation");
					afterParserOrEnumRuleCall();
				}
			)
		)+
		otherlv_4='}'
		{
			newLeafNode(otherlv_4, grammarAccess.getComputationBlockAccess().getRightCurlyBracketKeyword_4());
		}
	)
;

// Entry rule entryRuleOP
entryRuleOP returns [String current=null]:
	{ newCompositeNode(grammarAccess.getOPRule()); }
	iv_ruleOP=ruleOP
	{ $current=$iv_ruleOP.current.getText(); }
	EOF;

// Rule OP
ruleOP returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		kw='ADD'
		{
			$current.merge(kw);
			newLeafNode(kw, grammarAccess.getOPAccess().getADDKeyword_0());
		}
		    |
		kw='MUL'
		{
			$current.merge(kw);
			newLeafNode(kw, grammarAccess.getOPAccess().getMULKeyword_1());
		}
	)
;

// Entry rule entryRuleOperation
entryRuleOperation returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getOperationRule()); }
	iv_ruleOperation=ruleOperation
	{ $current=$iv_ruleOperation.current; }
	EOF;

// Rule Operation
ruleOperation returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			(
				{
					newCompositeNode(grammarAccess.getOperationAccess().getOpTypeOPParserRuleCall_0_0());
				}
				lv_opType_0_0=ruleOP
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getOperationRule());
					}
					set(
						$current,
						"opType",
						lv_opType_0_0,
						"fr.irisa.cairn.gecos.typeexploration.Computation.OP");
					afterParserOrEnumRuleCall();
				}
			)
		)
		otherlv_1=':'
		{
			newLeafNode(otherlv_1, grammarAccess.getOperationAccess().getColonKeyword_1());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getOperationAccess().getOutputExprOperandExpressionParserRuleCall_2_0());
				}
				lv_outputExpr_2_0=ruleOperandExpression
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getOperationRule());
					}
					set(
						$current,
						"outputExpr",
						lv_outputExpr_2_0,
						"fr.irisa.cairn.gecos.typeexploration.Computation.OperandExpression");
					afterParserOrEnumRuleCall();
				}
			)
		)
		otherlv_3='='
		{
			newLeafNode(otherlv_3, grammarAccess.getOperationAccess().getEqualsSignKeyword_3());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getOperationAccess().getInputExpr1OperandExpressionParserRuleCall_4_0());
				}
				lv_inputExpr1_4_0=ruleOperandExpression
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getOperationRule());
					}
					set(
						$current,
						"inputExpr1",
						lv_inputExpr1_4_0,
						"fr.irisa.cairn.gecos.typeexploration.Computation.OperandExpression");
					afterParserOrEnumRuleCall();
				}
			)
		)
		otherlv_5='op'
		{
			newLeafNode(otherlv_5, grammarAccess.getOperationAccess().getOpKeyword_5());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getOperationAccess().getInputExpr2OperandExpressionParserRuleCall_6_0());
				}
				lv_inputExpr2_6_0=ruleOperandExpression
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getOperationRule());
					}
					set(
						$current,
						"inputExpr2",
						lv_inputExpr2_6_0,
						"fr.irisa.cairn.gecos.typeexploration.Computation.OperandExpression");
					afterParserOrEnumRuleCall();
				}
			)
		)
		otherlv_7='x'
		{
			newLeafNode(otherlv_7, grammarAccess.getOperationAccess().getXKeyword_7());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getOperationAccess().getNumOpsSummationExpressionParserRuleCall_8_0());
				}
				lv_numOps_8_0=ruleSummationExpression
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getOperationRule());
					}
					set(
						$current,
						"numOps",
						lv_numOps_8_0,
						"fr.irisa.cairn.gecos.typeexploration.Computation.SummationExpression");
					afterParserOrEnumRuleCall();
				}
			)
		)
		otherlv_9=';'
		{
			newLeafNode(otherlv_9, grammarAccess.getOperationAccess().getSemicolonKeyword_9());
		}
	)
;

// Entry rule entryRuleOperandExpression
entryRuleOperandExpression returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getOperandExpressionRule()); }
	iv_ruleOperandExpression=ruleOperandExpression
	{ $current=$iv_ruleOperandExpression.current; }
	EOF;

// Rule OperandExpression
ruleOperandExpression returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		{
			newCompositeNode(grammarAccess.getOperandExpressionAccess().getMaxExpressionParserRuleCall_0());
		}
		this_MaxExpression_0=ruleMaxExpression
		{
			$current = $this_MaxExpression_0.current;
			afterParserOrEnumRuleCall();
		}
		    |
		{
			newCompositeNode(grammarAccess.getOperandExpressionAccess().getTerminalOperandExpressionParserRuleCall_1());
		}
		this_TerminalOperandExpression_1=ruleTerminalOperandExpression
		{
			$current = $this_TerminalOperandExpression_1.current;
			afterParserOrEnumRuleCall();
		}
	)
;

// Entry rule entryRuleMaxExpression
entryRuleMaxExpression returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getMaxExpressionRule()); }
	iv_ruleMaxExpression=ruleMaxExpression
	{ $current=$iv_ruleMaxExpression.current; }
	EOF;

// Rule MaxExpression
ruleMaxExpression returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='max'
		{
			newLeafNode(otherlv_0, grammarAccess.getMaxExpressionAccess().getMaxKeyword_0());
		}
		otherlv_1='('
		{
			newLeafNode(otherlv_1, grammarAccess.getMaxExpressionAccess().getLeftParenthesisKeyword_1());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getMaxExpressionAccess().getExprsTerminalOperandExpressionParserRuleCall_2_0());
				}
				lv_exprs_2_0=ruleTerminalOperandExpression
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getMaxExpressionRule());
					}
					add(
						$current,
						"exprs",
						lv_exprs_2_0,
						"fr.irisa.cairn.gecos.typeexploration.Computation.TerminalOperandExpression");
					afterParserOrEnumRuleCall();
				}
			)
		)
		(
			otherlv_3=','
			{
				newLeafNode(otherlv_3, grammarAccess.getMaxExpressionAccess().getCommaKeyword_3_0());
			}
			(
				(
					{
						newCompositeNode(grammarAccess.getMaxExpressionAccess().getExprsTerminalOperandExpressionParserRuleCall_3_1_0());
					}
					lv_exprs_4_0=ruleTerminalOperandExpression
					{
						if ($current==null) {
							$current = createModelElementForParent(grammarAccess.getMaxExpressionRule());
						}
						add(
							$current,
							"exprs",
							lv_exprs_4_0,
							"fr.irisa.cairn.gecos.typeexploration.Computation.TerminalOperandExpression");
						afterParserOrEnumRuleCall();
					}
				)
			)
		)+
		otherlv_5=')'
		{
			newLeafNode(otherlv_5, grammarAccess.getMaxExpressionAccess().getRightParenthesisKeyword_4());
		}
	)
;

// Entry rule entryRuleTerminalOperandExpression
entryRuleTerminalOperandExpression returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getTerminalOperandExpressionRule()); }
	iv_ruleTerminalOperandExpression=ruleTerminalOperandExpression
	{ $current=$iv_ruleTerminalOperandExpression.current; }
	EOF;

// Rule TerminalOperandExpression
ruleTerminalOperandExpression returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		{
			newCompositeNode(grammarAccess.getTerminalOperandExpressionAccess().getAddExpressionParserRuleCall_0());
		}
		this_AddExpression_0=ruleAddExpression
		{
			$current = $this_AddExpression_0.current;
			afterParserOrEnumRuleCall();
		}
		    |
		{
			newCompositeNode(grammarAccess.getTerminalOperandExpressionAccess().getOperandTermParserRuleCall_1());
		}
		this_OperandTerm_1=ruleOperandTerm
		{
			$current = $this_OperandTerm_1.current;
			afterParserOrEnumRuleCall();
		}
	)
;

// Entry rule entryRuleAddExpression
entryRuleAddExpression returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getAddExpressionRule()); }
	iv_ruleAddExpression=ruleAddExpression
	{ $current=$iv_ruleAddExpression.current; }
	EOF;

// Rule AddExpression
ruleAddExpression returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='('
		{
			newLeafNode(otherlv_0, grammarAccess.getAddExpressionAccess().getLeftParenthesisKeyword_0());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getAddExpressionAccess().getOp1OperandExpressionParserRuleCall_1_0());
				}
				lv_op1_1_0=ruleOperandExpression
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getAddExpressionRule());
					}
					set(
						$current,
						"op1",
						lv_op1_1_0,
						"fr.irisa.cairn.gecos.typeexploration.Computation.OperandExpression");
					afterParserOrEnumRuleCall();
				}
			)
		)
		otherlv_2='+'
		{
			newLeafNode(otherlv_2, grammarAccess.getAddExpressionAccess().getPlusSignKeyword_2());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getAddExpressionAccess().getOp2OperandExpressionParserRuleCall_3_0());
				}
				lv_op2_3_0=ruleOperandExpression
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getAddExpressionRule());
					}
					set(
						$current,
						"op2",
						lv_op2_3_0,
						"fr.irisa.cairn.gecos.typeexploration.Computation.OperandExpression");
					afterParserOrEnumRuleCall();
				}
			)
		)
		otherlv_4=')'
		{
			newLeafNode(otherlv_4, grammarAccess.getAddExpressionAccess().getRightParenthesisKeyword_4());
		}
	)
;

// Entry rule entryRuleOperandTerm
entryRuleOperandTerm returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getOperandTermRule()); }
	iv_ruleOperandTerm=ruleOperandTerm
	{ $current=$iv_ruleOperandTerm.current; }
	EOF;

// Rule OperandTerm
ruleOperandTerm returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			(
				lv_coef_0_0=RULE_INT
				{
					newLeafNode(lv_coef_0_0, grammarAccess.getOperandTermAccess().getCoefINTTerminalRuleCall_0_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getOperandTermRule());
					}
					setWithLastConsumed(
						$current,
						"coef",
						lv_coef_0_0,
						"org.eclipse.xtext.common.Terminals.INT");
				}
			)
		)?
		(
			(
				lv_name_1_0=RULE_ID
				{
					newLeafNode(lv_name_1_0, grammarAccess.getOperandTermAccess().getNameIDTerminalRuleCall_1_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getOperandTermRule());
					}
					setWithLastConsumed(
						$current,
						"name",
						lv_name_1_0,
						"org.eclipse.xtext.common.Terminals.ID");
				}
			)
		)
	)
;

// Entry rule entryRuleSummationExpression
entryRuleSummationExpression returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getSummationExpressionRule()); }
	iv_ruleSummationExpression=ruleSummationExpression
	{ $current=$iv_ruleSummationExpression.current; }
	EOF;

// Rule SummationExpression
ruleSummationExpression returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			(
				(
					(
						{
							newCompositeNode(grammarAccess.getSummationExpressionAccess().getTermsProductExpressionParserRuleCall_0_0_0_0());
						}
						lv_terms_0_0=ruleProductExpression
						{
							if ($current==null) {
								$current = createModelElementForParent(grammarAccess.getSummationExpressionRule());
							}
							add(
								$current,
								"terms",
								lv_terms_0_0,
								"fr.irisa.cairn.gecos.typeexploration.Computation.ProductExpression");
							afterParserOrEnumRuleCall();
						}
					)
				)
				(
					otherlv_1='+'
					{
						newLeafNode(otherlv_1, grammarAccess.getSummationExpressionAccess().getPlusSignKeyword_0_0_1_0());
					}
					(
						(
							{
								newCompositeNode(grammarAccess.getSummationExpressionAccess().getTermsProductExpressionParserRuleCall_0_0_1_1_0());
							}
							lv_terms_2_0=ruleProductExpression
							{
								if ($current==null) {
									$current = createModelElementForParent(grammarAccess.getSummationExpressionRule());
								}
								add(
									$current,
									"terms",
									lv_terms_2_0,
									"fr.irisa.cairn.gecos.typeexploration.Computation.ProductExpression");
								afterParserOrEnumRuleCall();
							}
						)
					)
				)*
			)
			(
				otherlv_3='+'
				{
					newLeafNode(otherlv_3, grammarAccess.getSummationExpressionAccess().getPlusSignKeyword_0_1_0());
				}
				(
					(
						lv_constant_4_0=RULE_INT
						{
							newLeafNode(lv_constant_4_0, grammarAccess.getSummationExpressionAccess().getConstantINTTerminalRuleCall_0_1_1_0());
						}
						{
							if ($current==null) {
								$current = createModelElement(grammarAccess.getSummationExpressionRule());
							}
							setWithLastConsumed(
								$current,
								"constant",
								lv_constant_4_0,
								"org.eclipse.xtext.common.Terminals.INT");
						}
					)
				)
			)?
		)
		    |
		(
			(
				lv_constant_5_0=RULE_INT
				{
					newLeafNode(lv_constant_5_0, grammarAccess.getSummationExpressionAccess().getConstantINTTerminalRuleCall_1_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getSummationExpressionRule());
					}
					setWithLastConsumed(
						$current,
						"constant",
						lv_constant_5_0,
						"org.eclipse.xtext.common.Terminals.INT");
				}
			)
		)
	)
;

// Entry rule entryRuleProductExpression
entryRuleProductExpression returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getProductExpressionRule()); }
	iv_ruleProductExpression=ruleProductExpression
	{ $current=$iv_ruleProductExpression.current; }
	EOF;

// Rule ProductExpression
ruleProductExpression returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			(
				lv_constant_0_0=RULE_INT
				{
					newLeafNode(lv_constant_0_0, grammarAccess.getProductExpressionAccess().getConstantINTTerminalRuleCall_0_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getProductExpressionRule());
					}
					setWithLastConsumed(
						$current,
						"constant",
						lv_constant_0_0,
						"org.eclipse.xtext.common.Terminals.INT");
				}
			)
		)?
		(
			(
				(
					{
						if ($current==null) {
							$current = createModelElement(grammarAccess.getProductExpressionRule());
						}
					}
					otherlv_1=RULE_ID
					{
						newLeafNode(otherlv_1, grammarAccess.getProductExpressionAccess().getTermsParameterCrossReference_1_0_0());
					}
				)
			)
			(
				(
					{
						if ($current==null) {
							$current = createModelElement(grammarAccess.getProductExpressionRule());
						}
					}
					otherlv_2=RULE_ID
					{
						newLeafNode(otherlv_2, grammarAccess.getProductExpressionAccess().getTermsParameterCrossReference_1_1_0());
					}
				)
			)*
		)
	)
;

RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

RULE_INT : ('0'..'9')+;

RULE_STRING : ('"' ('\\' .|~(('\\'|'"')))* '"'|'\'' ('\\' .|~(('\\'|'\'')))* '\'');

RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_ANY_OTHER : .;
