/*
* NLM: Non-local means denoising
* 
* Function: void nlm(float mat_out[H][W], float mat_in[H][W], float np1, float np2, float thr, float black, float f, float m)
*
* output:
*	mat_out[H][W]: output image (gray image), dimension: H x W
*
* input:
*	mat_in[H][W]: input image (gray image), dimension: H x W
*	np1, np2: noise profile in the image (signal dependent and constant)
*	thr: patch similarity threshold
*	black: black level value
*	f, m: Special controls to accumlate the contribution of the noisy pixels to be denoised 
*
* Author: Van-Phu HA
* Email: van-phu.ha@inria.fr
* Date: 15/5/2018
*/

#define __GECOS_TYPE_EXPL__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "utils.h"

#ifndef __GECOS_TYPE_EXPL__
#include "io_png.h"
#endif

#define K 5
#define N 11

//K2 and N2 are floor(K/2) and floor(N/2)
#define K2 2
#define N2 5
#define KK 25
// CROP = N2 - K2
#define CROP 3

#define H 384
#define W 512
//HW=H*W
#define HW 196608
//size_h_pad = H+2*N2;
#define SIZE_H_PAD 394
//size_w_pad = W+2*N2;
#define SIZE_W_PAD 522
//size_h_ref = H + N2 - K2
#define SIZE_H_REF 387
//size_w_ref = H + N2 - K2
#define SIZE_W_REF 515



#ifndef max
    #define max(a,b) ((a) > (b) ? (a) : (b))
#endif

#ifndef min
    #define min(a,b) ((a) < (b) ? (a) : (b))
#endif

#pragma hls_design top
void do_loop(float mat_out[H][W], float mat_in_pad[SIZE_H_PAD][SIZE_W_PAD], float mat_sqdiff[SIZE_H_REF][SIZE_W_REF], float mat_acc_weight[H][W], float sigma, float thr)
{
	int i, j, x, y;
	float sqdiff_current;
//#pragma EXPLORE_CONSTRAINT SAME = sqdiff_current
	float sqdiff_prev;
//#pragma EXPLORE_CONSTRAINT SAME = sqdiff_current
	float sqdiff_save;
//#pragma EXPLORE_CONSTRAINT SAME = mat_sqdiff
	float tmp_dist;
	float pre_exp;
//#pragma EXPLORE_FIX W={8} I={2}
	float weight;
//#pragma EXPLORE_CONSTRAINT SAME = mat_in_pad
	float denoised;
	
			
	float sigma2 = sigma * sigma;
	float fH = thr * sigma;
	float fH2 = fH*fH;
	fH2 = fH2*KK;

	for (x = -K2; x <= K2; x++)
		for (y = -K2; y <= K2; y++)
		if (!(x==0 && y==0)) {
		
		for (i = CROP; i < SIZE_H_REF + CROP; i++)
		{
			for (j = CROP; j < SIZE_W_REF + CROP; j++)
			{
				// Calculate square difference (and its prefix sum)
				sqdiff_current = (float)((mat_in_pad[i][j] - mat_in_pad[i+y][j+x])*(mat_in_pad[i][j] - mat_in_pad[i+y][j+x]));
				if (i==CROP && j==CROP) {
					sqdiff_save = sqdiff_current;
					sqdiff_prev = sqdiff_current;
				} else if (i==CROP && j> CROP) {
					sqdiff_save = sqdiff_current + sqdiff_prev;
					sqdiff_prev = sqdiff_current + sqdiff_prev;
				} else if(i> CROP && j==CROP) {
					sqdiff_save = sqdiff_current + mat_sqdiff[i-CROP-1][j-CROP];
					sqdiff_prev = sqdiff_current;
				} else if(i> CROP && j> CROP) {
					sqdiff_save = sqdiff_current + sqdiff_prev + mat_sqdiff[i-CROP-1][j-CROP];
					sqdiff_prev = sqdiff_current + sqdiff_prev;
				}
				mat_sqdiff[i-CROP][j-CROP] = sqdiff_save;

				// Calculate mat_denoised and mat_acc_weight
				if((i>=CROP+K-1) && (j>=CROP+K-1))
				{
					if(i==CROP+K-1 && j==CROP+K-1) {
						tmp_dist = mat_sqdiff[i-CROP][j-CROP];
					} else if(i> CROP+K-1 && j==CROP+K-1) {
						tmp_dist = mat_sqdiff[i-CROP][j-CROP] - mat_sqdiff[i-K-CROP][j-CROP];
					} else if(i==CROP+K-1 && j> CROP+K-1) {
						tmp_dist = mat_sqdiff[i-CROP][j-CROP] - mat_sqdiff[i-CROP][j-K-CROP];
					} else if(i> CROP+K-1 && j> CROP+K-1) {
						tmp_dist = mat_sqdiff[i-CROP][j-CROP] - mat_sqdiff[i-K-CROP][j-CROP] - mat_sqdiff[i-CROP][j-K-CROP] + mat_sqdiff[i-K-CROP][j-K-CROP];
					}

					#ifdef __GECOS_REF_IMPL__
					//pre_exp = -max(((float)tmp_dist)/(K*K) - 2*sigma, 0);
					//denoised = pow(2.7182817, pre_exp/(thr*thr*sigma));
				
					pre_exp = -max(tmp_dist - 2.0f*KK*sigma2, 0);
					pre_exp = pre_exp / (fH2);
					weight = exp(pre_exp);

					#else
					pre_exp = tmp_dist - 2*sigma2*KK;
					if (pre_exp <= 0)
						weight = 1;
					else
						weight = 0;
					#endif
					
					// Compute and accumulate denoised pixels
					denoised = weight * mat_in_pad[i-K2+y][j-K2+x];
					mat_out[i-CROP-K+1][j-CROP-K+1] += denoised;
					// Update accumulated weights
					mat_acc_weight[i-CROP-K+1][j-CROP-K+1] += weight;
				}
			
			}
		}
	}


	for(i=0; i<H; i++)
	{
		for(j=0; j<W; j++)
		{
			mat_out[i][j] = (mat_out[i][j] + mat_in_pad[i+N2][j+N2])/(mat_acc_weight[i][j] + 1);
		}
	}

}

void pad_matrix( float orig[H][W], float padded[SIZE_H_PAD][SIZE_W_PAD]) {
	int i,j;
	for(i=-N2; i<H+N2; i++)
	{
		for(j=-N2; j<W+N2; j++)
		{
			padded[(i+N2)][j+N2] = orig[min(max(i,0),H-1)][min(max(j,0),W-1)];
		}
	}
}


#pragma MAIN_FUNC
void nlm(float mat_out[H][W],
//#pragma EXPLORE_FIX W={8..12} I={1}
		float mat_in[H][W],
//#pragma EXPLORE_CONSTRAINT SAME = mat_in
		float mat_in_pad[SIZE_H_PAD][SIZE_W_PAD],
		float mat_sqdiff[SIZE_H_REF][SIZE_W_REF],
		float mat_acc_weight[H][W],
		float sigma, float thr)
{
	#ifdef __GECOS_TYPE_EXPL__
	$inject(mat_in, $from_file("/Users/sentieys/Desktop/Projects/Huawei/testimages/i01_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i02_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i03_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i04_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i05_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i06_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i07_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i08_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i09_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i10_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i11_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i12_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i13_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i14_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i15_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i16_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i17_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i18_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i19_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i20_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i21_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i22_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i23_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i24_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i25_gray_01_3.png"));
	$inject(mat_out, $from_var( 0 ), H, W);
	$inject(mat_sqdiff, $from_var( 0 ), SIZE_H_REF, SIZE_W_REF);
	$inject(mat_acc_weight, $from_var( 0 ), H, W);
	$inject(sigma, $from_var( 0.04466835921509631));
	$inject(thr, $from_var( 0.4));
	#endif

	/* replicate padding for input image */
	pad_matrix(mat_in, mat_in_pad);
	
	do_loop(mat_out, mat_in_pad,  mat_sqdiff, mat_acc_weight, sigma, thr);

	#ifdef __GECOS_TYPE_EXPL__
	$save(mat_out);
	#endif


}

#ifndef __GECOS_TYPE_EXPL__
void treat_one_image(const char* infile, const char* outfile) {
    // read input
    size_t nx,ny,nc;
    float *mat_in = NULL;
    mat_in = io_png_read_f32(infile, &nx, &ny, &nc);
    if (!mat_in) {
        printf("error :: %s not found  or not a correct png image \n", infile);
        exit(-1);
    }

    // variables
    int d_w = (int) nx;
    int d_h = (int) ny;
    int d_c = (int) nc;
    int d_wh = d_w * d_h;

   if (d_c != 1) {
        printf("error :: expecting single channel image \n");
        exit(-1);
   }
   if (d_h != H || d_w != W) {
        printf("error :: expecting %d x %d image\n", H, W);
        exit(-1);
   }

   int i;
   //normalize to 0-1
   for (i =0; i < d_wh; i++) {
      mat_in[i] = mat_in[i] / 255.0;
   }

   float *denoised = (float*) malloc(sizeof(float)*H*W);
   float *mat_in_pad = (float*) malloc (SIZE_H_PAD*SIZE_W_PAD*sizeof(float));
   float *mat_sqdiff = (float*) malloc (SIZE_H_REF*SIZE_W_REF*sizeof(float));
   float *mat_acc_weight = (float*) malloc (H*W*sizeof(float));

//   float sigma = 11.4;
   float sigma = 0.04466835921509631;
   float thr = 0.4;

   nlm((float (*)[W])denoised, (float (*)[W])mat_in,
         (float (*)[SIZE_W_PAD])mat_in_pad, (float (*)[SIZE_W_REF])mat_sqdiff, (float (*)[W])mat_acc_weight,
         sigma, thr);



   //denormalize to 0-255
   for (i =0; i < d_wh; i++) {
      denoised[i] = denoised[i] * 255.0;
   }

   if (outfile != NULL) {
      // save noisy and denoised images
      if (io_png_write_f32(outfile, denoised, (size_t) d_w, (size_t) d_h, (size_t) d_c) != 0) {
          printf("... failed to save png image %s", outfile);
      }
   }

   free(mat_in);
   free(denoised);
   free(mat_in_pad);
   free(mat_sqdiff);
   free(mat_acc_weight);

}
#endif

#ifndef __GECOS_TYPE_EXPL__
int main(int argc, char **argv) {

    if (argc < 3) {
        printf("usage: nlm image denoised \n");
        exit(-1);
    }

    treat_one_image(argv[1], argv[2]);
}
#endif



