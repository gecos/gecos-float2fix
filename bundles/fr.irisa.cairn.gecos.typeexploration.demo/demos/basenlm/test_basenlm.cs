debug(2);

sourceDir = "src-c/";

p = CreateGecosProject("basenlm");
AddSourceToGecosProject(p, sourceDir + "basenlm.c");
AddSourceToGecosProject(p, sourceDir + "utils.c");
AddIncDirToGecosProject(p, sourceDir);

#TypesExploration("."); #regenearate default.properties

TypesExploration(p, "exploration.properties");
#TypesExploration(p, "exploration.properties", "nlm_png.costdsl");
