/*
* BASENLM: Huawei's non-local means denoising
* 
* Date: 15/5/2018
*/

#define __GECOS_TYPE_EXPL__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "utils.h"

#ifndef __GECOS_TYPE_EXPL__
#include "io_png.h"
#endif


#define H 384
#define W 512

typedef float F_ABS;
static F_ABS ABS(float x)
{
	float y = x;
	if (x<0)
		y = -x;
	return y;
}

static void LPFilter(double *pSrc, double *pDst,
    int width, int height, int strideSrc, int strideDst,
    int radius, int divisor,
    int *pLutLpf)
{
    int m, n, y, x, posSrc, posDst;
    double gV[32];
    double gResult;

#pragma EXPLORE_CONSTRAINT SAME = pSrc
    double *pSrcCur;

    for ( y = radius; y < height-radius; y++)
    {
        for ( x = radius; x < width-radius;  x++)
        {
            posSrc = y * strideSrc + x;
            posDst = y * strideDst + x;

            pSrcCur = pSrc + posSrc;

            for(m = -radius; m <= radius; m++)
            {
                gV[m+radius] = 0;

                for(n = -radius; n < 0; n++)
                {
                    gV[m+radius] += (pSrcCur[m*strideSrc + n] + pSrcCur[m*strideSrc - n]) * pLutLpf[n+radius];
                }

                gV[m+radius] += pSrcCur[m*strideSrc]  * pLutLpf[radius];
            }

            gResult =  0;

            for(n = -radius; n < 0; n++)
            {
                gResult += (gV[n+radius] + gV[-n+radius]) * pLutLpf[n+radius];
            }

            gResult += (gV[radius]) * pLutLpf[radius];

            pDst[posDst] = gResult/(1<<divisor);
        }
    }
}



#pragma MAIN_FUNC
void basenlm(
#pragma EXPLORE_FIX W={8..24} I={6}
		double fpI[W*H],          // Input
#pragma EXPLORE_CONSTRAINT SAME = fpI
		double fpO[W*H],
		int LUT[256],
		double center_t[4],
		double bl,
		int grey_mode)
{

	#ifdef __GECOS_TYPE_EXPL__
	$inject(fpI, $from_file("/Users/sentieys/Desktop/Projects/Huawei/testimages/i01_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i02_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i03_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i04_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i05_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i06_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i07_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i08_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i09_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i10_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i11_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i12_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i13_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i14_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i15_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i16_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i17_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i18_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i19_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i20_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i21_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i22_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i23_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i24_gray_01_3.png",
							   "/Users/sentieys/Desktop/Projects/Huawei/testimages/i25_gray_01_3.png"));
	#endif

	int radiusLpf;
	$inject(radiusLpf, $from_var(2,2,3,4));
	//int radiusLpf = 2;
	int lutLpf[3] = {1, 4, 6};
	int divisor = 8;

	LPFilter(fpI, fpO,
	    W, H, W, H,
		radiusLpf, divisor,
		lutLpf);


	// test structure ?
	F_ABS dist=0.0;
	typedef float F_ABS2;
	F_ABS2 dist2=0.0;
	float dif=fpO[0];
    //dist = dist + (dif < 0 ? -dif : dif);
    //if (dif<0) dist = dist-dif; else dist = dist+dif;
	dist = dist + ABS(dif);
	dist2 = dist2 + ABS(dif);

	// test malloc
	typedef double TYPE_interm;
	TYPE_interm *interm = (TYPE_interm *) malloc(W*H*sizeof(TYPE_interm));
	for (int i=0; i<W*H; i++)
		interm[i] = fpO[i]+(i%8);
	for (int i=0; i<W*H; i++)
		fpO[i] = interm[i];
	free(interm);

	
	#ifdef __GECOS_TYPE_EXPL__
	$save(fpO);
	#endif


}

#ifndef __GECOS_TYPE_EXPL__
void treat_one_image(const char* infile, const char* outfile) {

	// read input
    size_t nx,ny,nc;
    float *mat_in = io_png_read_f32(infile, &nx, &ny, &nc);
    if (!mat_in) {
        printf("error :: %s not found  or not a correct png image \n", infile);
        exit(-1);
    }

    // variables
    int d_w = (int) nx;
    int d_h = (int) ny;
    int d_c = (int) nc;
    int d_wh = d_w * d_h;

   if (d_c != 1) {
        printf("error :: expecting single channel image \n");
        exit(-1);
   }
   if (d_h != H || d_w != W) {
        printf("error :: expecting %d x %d image\n", H, W);
        exit(-1);
   }


   double *fpI = (double*) malloc(sizeof(double)*H*W);
   double *fpO = (double*) malloc(sizeof(double)*H*W);

   int i;
   //normalize to 0-1
   for (i =0; i < d_wh; i++) {
      fpI[i] = (double) (mat_in[i] / 255.0);
   }



   int LUT[256];
   double center_t[4];
   double bl;
   int grey_mode;

   basenlm(fpI, fpO, LUT, center_t, bl, grey_mode);


   //denormalize to 0-255
   for (i =0; i < d_wh; i++) {
	   mat_in[i] = (float) (fpO[i] * 255.0);
   }

   if (outfile != NULL) {
      // save noisy and denoised images
      if (io_png_write_f32(outfile, mat_in, (size_t) d_w, (size_t) d_h, (size_t) d_c) != 0) {
          printf("... failed to save png image %s", outfile);
      }
   }

//   for (i =0; i < d_wh; i++) {
//	   mat_in[i] = (float) (fpI[i] * 255.0);
//   }
//   // test to write original png image
//   io_png_write_f32("test.png", mat_in, (size_t) d_w, (size_t) d_h, (size_t) d_c);

   free(mat_in);
   free(fpI);
   free(fpO);

}
#endif

#ifndef __GECOS_TYPE_EXPL__
int main(int argc, char **argv) {

    if (argc < 3) {
        printf("usage: nlm image denoised \n");
        exit(-1);
    }

    treat_one_image(argv[1], argv[2]);
}
#endif



