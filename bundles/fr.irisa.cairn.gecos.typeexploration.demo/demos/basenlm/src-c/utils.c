#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void read_image(const char image[], int nrOfRows, int nrOfColumns, double *data){
	FILE *fp;
	fp = fopen(image , "rb" );
	fread(data, sizeof(double), nrOfRows*nrOfColumns, fp);
	fclose(fp);
}

void write_image(const char image[], int nrOfRows, int nrOfColumns, double *data){
	FILE *fp;
	fp = fopen(image , "wb" );
	if(fp == NULL)
	{
		puts("Cannot open file!");
		exit(1);
	}
	fwrite(data, sizeof(double), nrOfRows*nrOfColumns, fp);
	fclose(fp);
}