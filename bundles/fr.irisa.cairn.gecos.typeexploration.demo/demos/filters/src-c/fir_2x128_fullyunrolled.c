/*
* Cascade FIR filter
*
*
* Author: Olivier SENTIEYS
* Data: 26/12/2018
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define __GECOS_TYPE_EXPL__

#define N 129
#define C 2
#define NSIM 300

double x0_0=0.0;  double x0_1=0.0;  double x0_2=0.0;  double x0_3=0.0;  double x0_4=0.0;  double x0_5=0.0;  double x0_6=0.0;  double x0_7=0.0;  double x0_8=0.0;  double x0_9=0.0;  double x0_10=0.0;  double x0_11=0.0;  double x0_12=0.0;  double x0_13=0.0;  double x0_14=0.0;  double x0_15=0.0;  double x0_16=0.0;  double x0_17=0.0;  double x0_18=0.0;  double x0_19=0.0;  double x0_20=0.0;  double x0_21=0.0;  double x0_22=0.0;  double x0_23=0.0;  double x0_24=0.0;  double x0_25=0.0;  double x0_26=0.0;  double x0_27=0.0;  double x0_28=0.0;  double x0_29=0.0;  double x0_30=0.0;  double x0_31=0.0;  double x0_32=0.0;  double x0_33=0.0;  double x0_34=0.0;  double x0_35=0.0;  double x0_36=0.0;  double x0_37=0.0;  double x0_38=0.0;  double x0_39=0.0;  double x0_40=0.0;  double x0_41=0.0;  double x0_42=0.0;  double x0_43=0.0;  double x0_44=0.0;  double x0_45=0.0;  double x0_46=0.0;  double x0_47=0.0;  double x0_48=0.0;  double x0_49=0.0;  double x0_50=0.0;  double x0_51=0.0;  double x0_52=0.0;  double x0_53=0.0;  double x0_54=0.0;  double x0_55=0.0;  double x0_56=0.0;  double x0_57=0.0;  double x0_58=0.0;  double x0_59=0.0;  double x0_60=0.0;  double x0_61=0.0;  double x0_62=0.0;  double x0_63=0.0;  double x0_64=0.0;  double x0_65=0.0;  double x0_66=0.0;  double x0_67=0.0;  double x0_68=0.0;  double x0_69=0.0;  double x0_70=0.0;  double x0_71=0.0;  double x0_72=0.0;  double x0_73=0.0;  double x0_74=0.0;  double x0_75=0.0;  double x0_76=0.0;  double x0_77=0.0;  double x0_78=0.0;  double x0_79=0.0;  double x0_80=0.0;  double x0_81=0.0;  double x0_82=0.0;  double x0_83=0.0;  double x0_84=0.0;  double x0_85=0.0;  double x0_86=0.0;  double x0_87=0.0;  double x0_88=0.0;  double x0_89=0.0;  double x0_90=0.0;  double x0_91=0.0;  double x0_92=0.0;  double x0_93=0.0;  double x0_94=0.0;  double x0_95=0.0;  double x0_96=0.0;  double x0_97=0.0;  double x0_98=0.0;  double x0_99=0.0;  double x0_100=0.0;  double x0_101=0.0;  double x0_102=0.0;  double x0_103=0.0;  double x0_104=0.0;  double x0_105=0.0;  double x0_106=0.0;  double x0_107=0.0;  double x0_108=0.0;  double x0_109=0.0;  double x0_110=0.0;  double x0_111=0.0;  double x0_112=0.0;  double x0_113=0.0;  double x0_114=0.0;  double x0_115=0.0;  double x0_116=0.0;  double x0_117=0.0;  double x0_118=0.0;  double x0_119=0.0;  double x0_120=0.0;  double x0_121=0.0;  double x0_122=0.0;  double x0_123=0.0;  double x0_124=0.0;  double x0_125=0.0;  double x0_126=0.0;  double x0_127=0.0;  double x0_128=0.0;
double x1_0=0.0;  double x1_1=0.0;  double x1_2=0.0;  double x1_3=0.0;  double x1_4=0.0;  double x1_5=0.0;  double x1_6=0.0;  double x1_7=0.0;  double x1_8=0.0;  double x1_9=0.0;  double x1_10=0.0;  double x1_11=0.0;  double x1_12=0.0;  double x1_13=0.0;  double x1_14=0.0;  double x1_15=0.0;  double x1_16=0.0;  double x1_17=0.0;  double x1_18=0.0;  double x1_19=0.0;  double x1_20=0.0;  double x1_21=0.0;  double x1_22=0.0;  double x1_23=0.0;  double x1_24=0.0;  double x1_25=0.0;  double x1_26=0.0;  double x1_27=0.0;  double x1_28=0.0;  double x1_29=0.0;  double x1_30=0.0;  double x1_31=0.0;  double x1_32=0.0;  double x1_33=0.0;  double x1_34=0.0;  double x1_35=0.0;  double x1_36=0.0;  double x1_37=0.0;  double x1_38=0.0;  double x1_39=0.0;  double x1_40=0.0;  double x1_41=0.0;  double x1_42=0.0;  double x1_43=0.0;  double x1_44=0.0;  double x1_45=0.0;  double x1_46=0.0;  double x1_47=0.0;  double x1_48=0.0;  double x1_49=0.0;  double x1_50=0.0;  double x1_51=0.0;  double x1_52=0.0;  double x1_53=0.0;  double x1_54=0.0;  double x1_55=0.0;  double x1_56=0.0;  double x1_57=0.0;  double x1_58=0.0;  double x1_59=0.0;  double x1_60=0.0;  double x1_61=0.0;  double x1_62=0.0;  double x1_63=0.0;  double x1_64=0.0;  double x1_65=0.0;  double x1_66=0.0;  double x1_67=0.0;  double x1_68=0.0;  double x1_69=0.0;  double x1_70=0.0;  double x1_71=0.0;  double x1_72=0.0;  double x1_73=0.0;  double x1_74=0.0;  double x1_75=0.0;  double x1_76=0.0;  double x1_77=0.0;  double x1_78=0.0;  double x1_79=0.0;  double x1_80=0.0;  double x1_81=0.0;  double x1_82=0.0;  double x1_83=0.0;  double x1_84=0.0;  double x1_85=0.0;  double x1_86=0.0;  double x1_87=0.0;  double x1_88=0.0;  double x1_89=0.0;  double x1_90=0.0;  double x1_91=0.0;  double x1_92=0.0;  double x1_93=0.0;  double x1_94=0.0;  double x1_95=0.0;  double x1_96=0.0;  double x1_97=0.0;  double x1_98=0.0;  double x1_99=0.0;  double x1_100=0.0;  double x1_101=0.0;  double x1_102=0.0;  double x1_103=0.0;  double x1_104=0.0;  double x1_105=0.0;  double x1_106=0.0;  double x1_107=0.0;  double x1_108=0.0;  double x1_109=0.0;  double x1_110=0.0;  double x1_111=0.0;  double x1_112=0.0;  double x1_113=0.0;  double x1_114=0.0;  double x1_115=0.0;  double x1_116=0.0;  double x1_117=0.0;  double x1_118=0.0;  double x1_119=0.0;  double x1_120=0.0;  double x1_121=0.0;  double x1_122=0.0;  double x1_123=0.0;  double x1_124=0.0;  double x1_125=0.0;  double x1_126=0.0;  double x1_127=0.0;  double x1_128=0.0;



void fir(
#pragma EXPLORE_FIX W={8..32} I={4}
		double fpI[1],
#pragma EXPLORE_CONSTRAINT SAME = fpI
		double fpO[1])
{

	int i, j;

	double acc0_0;  double acc0_1;  double acc0_2;  double acc0_3;  double acc0_4;  double acc0_5;  double acc0_6;  double acc0_7;  double acc0_8;  double acc0_9;  double acc0_10;  double acc0_11;  double acc0_12;  double acc0_13;  double acc0_14;  double acc0_15;  double acc0_16;  double acc0_17;  double acc0_18;  double acc0_19;  double acc0_20;  double acc0_21;  double acc0_22;  double acc0_23;  double acc0_24;  double acc0_25;  double acc0_26;  double acc0_27;  double acc0_28;  double acc0_29;  double acc0_30;  double acc0_31;  double acc0_32;  double acc0_33;  double acc0_34;  double acc0_35;  double acc0_36;  double acc0_37;  double acc0_38;  double acc0_39;  double acc0_40;  double acc0_41;  double acc0_42;  double acc0_43;  double acc0_44;  double acc0_45;  double acc0_46;  double acc0_47;  double acc0_48;  double acc0_49;  double acc0_50;  double acc0_51;  double acc0_52;  double acc0_53;  double acc0_54;  double acc0_55;  double acc0_56;  double acc0_57;  double acc0_58;  double acc0_59;  double acc0_60;  double acc0_61;  double acc0_62;  double acc0_63;  double acc0_64;  double acc0_65;  double acc0_66;  double acc0_67;  double acc0_68;  double acc0_69;  double acc0_70;  double acc0_71;  double acc0_72;  double acc0_73;  double acc0_74;  double acc0_75;  double acc0_76;  double acc0_77;  double acc0_78;  double acc0_79;  double acc0_80;  double acc0_81;  double acc0_82;  double acc0_83;  double acc0_84;  double acc0_85;  double acc0_86;  double acc0_87;  double acc0_88;  double acc0_89;  double acc0_90;  double acc0_91;  double acc0_92;  double acc0_93;  double acc0_94;  double acc0_95;  double acc0_96;  double acc0_97;  double acc0_98;  double acc0_99;  double acc0_100;  double acc0_101;  double acc0_102;  double acc0_103;  double acc0_104;  double acc0_105;  double acc0_106;  double acc0_107;  double acc0_108;  double acc0_109;  double acc0_110;  double acc0_111;  double acc0_112;  double acc0_113;  double acc0_114;  double acc0_115;  double acc0_116;  double acc0_117;  double acc0_118;  double acc0_119;  double acc0_120;  double acc0_121;  double acc0_122;  double acc0_123;  double acc0_124;  double acc0_125;  double acc0_126;  double acc0_127;  double acc0_128;
	double acc1_0;  double acc1_1;  double acc1_2;  double acc1_3;  double acc1_4;  double acc1_5;  double acc1_6;  double acc1_7;  double acc1_8;  double acc1_9;  double acc1_10;  double acc1_11;  double acc1_12;  double acc1_13;  double acc1_14;  double acc1_15;  double acc1_16;  double acc1_17;  double acc1_18;  double acc1_19;  double acc1_20;  double acc1_21;  double acc1_22;  double acc1_23;  double acc1_24;  double acc1_25;  double acc1_26;  double acc1_27;  double acc1_28;  double acc1_29;  double acc1_30;  double acc1_31;  double acc1_32;  double acc1_33;  double acc1_34;  double acc1_35;  double acc1_36;  double acc1_37;  double acc1_38;  double acc1_39;  double acc1_40;  double acc1_41;  double acc1_42;  double acc1_43;  double acc1_44;  double acc1_45;  double acc1_46;  double acc1_47;  double acc1_48;  double acc1_49;  double acc1_50;  double acc1_51;  double acc1_52;  double acc1_53;  double acc1_54;  double acc1_55;  double acc1_56;  double acc1_57;  double acc1_58;  double acc1_59;  double acc1_60;  double acc1_61;  double acc1_62;  double acc1_63;  double acc1_64;  double acc1_65;  double acc1_66;  double acc1_67;  double acc1_68;  double acc1_69;  double acc1_70;  double acc1_71;  double acc1_72;  double acc1_73;  double acc1_74;  double acc1_75;  double acc1_76;  double acc1_77;  double acc1_78;  double acc1_79;  double acc1_80;  double acc1_81;  double acc1_82;  double acc1_83;  double acc1_84;  double acc1_85;  double acc1_86;  double acc1_87;  double acc1_88;  double acc1_89;  double acc1_90;  double acc1_91;  double acc1_92;  double acc1_93;  double acc1_94;  double acc1_95;  double acc1_96;  double acc1_97;  double acc1_98;  double acc1_99;  double acc1_100;  double acc1_101;  double acc1_102;  double acc1_103;  double acc1_104;  double acc1_105;  double acc1_106;  double acc1_107;  double acc1_108;  double acc1_109;  double acc1_110;  double acc1_111;  double acc1_112;  double acc1_113;  double acc1_114;  double acc1_115;  double acc1_116;  double acc1_117;  double acc1_118;  double acc1_119;  double acc1_120;  double acc1_121;  double acc1_122;  double acc1_123;  double acc1_124;  double acc1_125;  double acc1_126;  double acc1_127;  double acc1_128;

	double h0_0=2.180439459399e-06;  double h0_1=7.307213536984e-06;  double h0_2=-3.827449957177e-06;  double h0_3=-1.470454016318e-05;  double h0_4=3.336797554673e-07;  double h0_5=2.815913109337e-05;  double h0_6=1.116724973772e-05;  double h0_7=-4.416534274066e-05;  double h0_8=-3.661105503317e-05;  double h0_9=5.733435304333e-05;  double h0_10=8.078317610662e-05;  double h0_11=-5.763052084949e-05;  double h0_12=-1.453858201791e-04;  double h0_13=3.066685610897e-05;  double h0_14=2.258620282981e-04;  double h0_15=4.056900826165e-05;  double h0_16=-3.083188294445e-04;  double h0_17=-1.720322251861e-04;  double h0_18=3.674277018329e-04;  double h0_19=3.730012024050e-04;  double h0_20=-3.665116133200e-04;  double h0_21=-6.392079118609e-04;  double h0_22=2.608282746315e-04;  double h0_23=9.459340394518e-04;  double h0_24=-4.714989225618e-06;  double h0_25=-1.242888193439e-03;  double h0_26=-4.375037249037e-04;  double h0_27=1.452873243087e-03;  double h0_28=1.077893303753e-03;  double h0_29=-1.476114602752e-03;  double h0_30=-1.890627483287e-03;  double h0_31=1.201438616975e-03;  double h0_32=2.799916832080e-03;  double h0_33=-5.243490768216e-04;  double h0_34=-3.673384850180e-03;  double h0_35=-6.293437919844e-04;  double h0_36=4.323894493196e-03;  double h0_37=2.277193333143e-03;  double h0_38=-4.521857372403e-03;  double h0_39=-4.355041875729e-03;  double h0_40=4.018432361165e-03;  double h0_41=6.697649371841e-03;  double h0_42=-2.577981761378e-03;  double h0_43=-9.028932733824e-03;  double h0_44=1.604923980305e-05;  double h0_45=1.096401653245e-02;  double h0_46=3.762616934979e-03;  double h0_47=-1.202217302654e-02;  double h0_48=-8.732413937178e-03;  double h0_49=1.164473684619e-02;  double h0_50=1.472789712797e-02;  double h0_51=-9.203865740475e-03;  double h0_52=-2.144235094715e-02;  double h0_53=3.971105287937e-03;  double h0_54=2.844643347479e-02;  double h0_55=5.030229352632e-03;  double h0_56=-3.522619285465e-02;  double h0_57=-1.952087661328e-02;  double h0_58=4.123651346992e-02;  double h0_59=4.375713370901e-02;  double h0_60=-4.596309477471e-02;  double h0_61=-9.358118837624e-02;  double h0_62=4.898425197719e-02;  double h0_63=3.140359689280e-01;  double h0_64=4.499765913967e-01;  double h0_65=3.140359689280e-01;  double h0_66=4.898425197719e-02;  double h0_67=-9.358118837624e-02;  double h0_68=-4.596309477471e-02;  double h0_69=4.375713370901e-02;  double h0_70=4.123651346992e-02;  double h0_71=-1.952087661328e-02;  double h0_72=-3.522619285465e-02;  double h0_73=5.030229352632e-03;  double h0_74=2.844643347479e-02;  double h0_75=3.971105287937e-03;  double h0_76=-2.144235094715e-02;  double h0_77=-9.203865740475e-03;  double h0_78=1.472789712797e-02;  double h0_79=1.164473684619e-02;  double h0_80=-8.732413937178e-03;  double h0_81=-1.202217302654e-02;  double h0_82=3.762616934979e-03;  double h0_83=1.096401653245e-02;  double h0_84=1.604923980305e-05;  double h0_85=-9.028932733824e-03;  double h0_86=-2.577981761378e-03;  double h0_87=6.697649371841e-03;  double h0_88=4.018432361165e-03;  double h0_89=-4.355041875729e-03;  double h0_90=-4.521857372403e-03;  double h0_91=2.277193333143e-03;  double h0_92=4.323894493196e-03;  double h0_93=-6.293437919844e-04;  double h0_94=-3.673384850180e-03;  double h0_95=-5.243490768216e-04;  double h0_96=2.799916832080e-03;  double h0_97=1.201438616975e-03;  double h0_98=-1.890627483287e-03;  double h0_99=-1.476114602752e-03;  double h0_100=1.077893303753e-03;  double h0_101=1.452873243087e-03;  double h0_102=-4.375037249037e-04;  double h0_103=-1.242888193439e-03;  double h0_104=-4.714989225618e-06;  double h0_105=9.459340394518e-04;  double h0_106=2.608282746315e-04;  double h0_107=-6.392079118609e-04;  double h0_108=-3.665116133200e-04;  double h0_109=3.730012024050e-04;  double h0_110=3.674277018329e-04;  double h0_111=-1.720322251861e-04;  double h0_112=-3.083188294445e-04;  double h0_113=4.056900826165e-05;  double h0_114=2.258620282981e-04;  double h0_115=3.066685610897e-05;  double h0_116=-1.453858201791e-04;  double h0_117=-5.763052084949e-05;  double h0_118=8.078317610662e-05;  double h0_119=5.733435304333e-05;  double h0_120=-3.661105503317e-05;  double h0_121=-4.416534274066e-05;  double h0_122=1.116724973772e-05;  double h0_123=2.815913109337e-05;  double h0_124=3.336797554673e-07;  double h0_125=-1.470454016318e-05;  double h0_126=-3.827449957177e-06;  double h0_127=7.307213536984e-06;  double h0_128=2.180439459399e-06;
	double h1_0=2.180439459399e-06;  double h1_1=7.307213536984e-06;  double h1_2=-3.827449957177e-06;  double h1_3=-1.470454016318e-05;  double h1_4=3.336797554673e-07;  double h1_5=2.815913109337e-05;  double h1_6=1.116724973772e-05;  double h1_7=-4.416534274066e-05;  double h1_8=-3.661105503317e-05;  double h1_9=5.733435304333e-05;  double h1_10=8.078317610662e-05;  double h1_11=-5.763052084949e-05;  double h1_12=-1.453858201791e-04;  double h1_13=3.066685610897e-05;  double h1_14=2.258620282981e-04;  double h1_15=4.056900826165e-05;  double h1_16=-3.083188294445e-04;  double h1_17=-1.720322251861e-04;  double h1_18=3.674277018329e-04;  double h1_19=3.730012024050e-04;  double h1_20=-3.665116133200e-04;  double h1_21=-6.392079118609e-04;  double h1_22=2.608282746315e-04;  double h1_23=9.459340394518e-04;  double h1_24=-4.714989225618e-06;  double h1_25=-1.242888193439e-03;  double h1_26=-4.375037249037e-04;  double h1_27=1.452873243087e-03;  double h1_28=1.077893303753e-03;  double h1_29=-1.476114602752e-03;  double h1_30=-1.890627483287e-03;  double h1_31=1.201438616975e-03;  double h1_32=2.799916832080e-03;  double h1_33=-5.243490768216e-04;  double h1_34=-3.673384850180e-03;  double h1_35=-6.293437919844e-04;  double h1_36=4.323894493196e-03;  double h1_37=2.277193333143e-03;  double h1_38=-4.521857372403e-03;  double h1_39=-4.355041875729e-03;  double h1_40=4.018432361165e-03;  double h1_41=6.697649371841e-03;  double h1_42=-2.577981761378e-03;  double h1_43=-9.028932733824e-03;  double h1_44=1.604923980305e-05;  double h1_45=1.096401653245e-02;  double h1_46=3.762616934979e-03;  double h1_47=-1.202217302654e-02;  double h1_48=-8.732413937178e-03;  double h1_49=1.164473684619e-02;  double h1_50=1.472789712797e-02;  double h1_51=-9.203865740475e-03;  double h1_52=-2.144235094715e-02;  double h1_53=3.971105287937e-03;  double h1_54=2.844643347479e-02;  double h1_55=5.030229352632e-03;  double h1_56=-3.522619285465e-02;  double h1_57=-1.952087661328e-02;  double h1_58=4.123651346992e-02;  double h1_59=4.375713370901e-02;  double h1_60=-4.596309477471e-02;  double h1_61=-9.358118837624e-02;  double h1_62=4.898425197719e-02;  double h1_63=3.140359689280e-01;  double h1_64=4.499765913967e-01;  double h1_65=3.140359689280e-01;  double h1_66=4.898425197719e-02;  double h1_67=-9.358118837624e-02;  double h1_68=-4.596309477471e-02;  double h1_69=4.375713370901e-02;  double h1_70=4.123651346992e-02;  double h1_71=-1.952087661328e-02;  double h1_72=-3.522619285465e-02;  double h1_73=5.030229352632e-03;  double h1_74=2.844643347479e-02;  double h1_75=3.971105287937e-03;  double h1_76=-2.144235094715e-02;  double h1_77=-9.203865740475e-03;  double h1_78=1.472789712797e-02;  double h1_79=1.164473684619e-02;  double h1_80=-8.732413937178e-03;  double h1_81=-1.202217302654e-02;  double h1_82=3.762616934979e-03;  double h1_83=1.096401653245e-02;  double h1_84=1.604923980305e-05;  double h1_85=-9.028932733824e-03;  double h1_86=-2.577981761378e-03;  double h1_87=6.697649371841e-03;  double h1_88=4.018432361165e-03;  double h1_89=-4.355041875729e-03;  double h1_90=-4.521857372403e-03;  double h1_91=2.277193333143e-03;  double h1_92=4.323894493196e-03;  double h1_93=-6.293437919844e-04;  double h1_94=-3.673384850180e-03;  double h1_95=-5.243490768216e-04;  double h1_96=2.799916832080e-03;  double h1_97=1.201438616975e-03;  double h1_98=-1.890627483287e-03;  double h1_99=-1.476114602752e-03;  double h1_100=1.077893303753e-03;  double h1_101=1.452873243087e-03;  double h1_102=-4.375037249037e-04;  double h1_103=-1.242888193439e-03;  double h1_104=-4.714989225618e-06;  double h1_105=9.459340394518e-04;  double h1_106=2.608282746315e-04;  double h1_107=-6.392079118609e-04;  double h1_108=-3.665116133200e-04;  double h1_109=3.730012024050e-04;  double h1_110=3.674277018329e-04;  double h1_111=-1.720322251861e-04;  double h1_112=-3.083188294445e-04;  double h1_113=4.056900826165e-05;  double h1_114=2.258620282981e-04;  double h1_115=3.066685610897e-05;  double h1_116=-1.453858201791e-04;  double h1_117=-5.763052084949e-05;  double h1_118=8.078317610662e-05;  double h1_119=5.733435304333e-05;  double h1_120=-3.661105503317e-05;  double h1_121=-4.416534274066e-05;  double h1_122=1.116724973772e-05;  double h1_123=2.815913109337e-05;  double h1_124=3.336797554673e-07;  double h1_125=-1.470454016318e-05;  double h1_126=-3.827449957177e-06;  double h1_127=7.307213536984e-06;  double h1_128=2.180439459399e-06;


//	for (i = 0; i < C-1; i++) {
//		acc[i] = x[i][N-1] * h[N-1];
//		for (j = N-2; j >= 0; j--){
//			acc[i] += x[i][j] * h[j];
//			x[i][j+1] = x[i][j];
//		}
//		x[i + 1][0] = acc[i];
//	}
//	acc[C-1] = x[C-1][N-1] * h[N-1];
//	for (j = N-2; j >= 0; j--){
//		acc[C-1] += x[C-1][j] * h[j];
//		x[C-1][j+1] = x[C-1][j];
//	}
//
//	fpO[0] = acc[C-1];


	x0_0 = fpI[0];
	acc0_128 = x0_128 * h0_128;
	acc0_127 = acc0_128 + x0_127 * h0_127;
	x0_128 = x0_127;
	acc0_126 = acc0_127 + x0_126 * h0_126;
	x0_127 = x0_126;
	acc0_125 = acc0_126 + x0_125 * h0_125;
	x0_126 = x0_125;
	acc0_124 = acc0_125 + x0_124 * h0_124;
	x0_125 = x0_124;
	acc0_123 = acc0_124 + x0_123 * h0_123;
	x0_124 = x0_123;
	acc0_122 = acc0_123 + x0_122 * h0_122;
	x0_123 = x0_122;
	acc0_121 = acc0_122 + x0_121 * h0_121;
	x0_122 = x0_121;
	acc0_120 = acc0_121 + x0_120 * h0_120;
	x0_121 = x0_120;
	acc0_119 = acc0_120 + x0_119 * h0_119;
	x0_120 = x0_119;
	acc0_118 = acc0_119 + x0_118 * h0_118;
	x0_119 = x0_118;
	acc0_117 = acc0_118 + x0_117 * h0_117;
	x0_118 = x0_117;
	acc0_116 = acc0_117 + x0_116 * h0_116;
	x0_117 = x0_116;
	acc0_115 = acc0_116 + x0_115 * h0_115;
	x0_116 = x0_115;
	acc0_114 = acc0_115 + x0_114 * h0_114;
	x0_115 = x0_114;
	acc0_113 = acc0_114 + x0_113 * h0_113;
	x0_114 = x0_113;
	acc0_112 = acc0_113 + x0_112 * h0_112;
	x0_113 = x0_112;
	acc0_111 = acc0_112 + x0_111 * h0_111;
	x0_112 = x0_111;
	acc0_110 = acc0_111 + x0_110 * h0_110;
	x0_111 = x0_110;
	acc0_109 = acc0_110 + x0_109 * h0_109;
	x0_110 = x0_109;
	acc0_108 = acc0_109 + x0_108 * h0_108;
	x0_109 = x0_108;
	acc0_107 = acc0_108 + x0_107 * h0_107;
	x0_108 = x0_107;
	acc0_106 = acc0_107 + x0_106 * h0_106;
	x0_107 = x0_106;
	acc0_105 = acc0_106 + x0_105 * h0_105;
	x0_106 = x0_105;
	acc0_104 = acc0_105 + x0_104 * h0_104;
	x0_105 = x0_104;
	acc0_103 = acc0_104 + x0_103 * h0_103;
	x0_104 = x0_103;
	acc0_102 = acc0_103 + x0_102 * h0_102;
	x0_103 = x0_102;
	acc0_101 = acc0_102 + x0_101 * h0_101;
	x0_102 = x0_101;
	acc0_100 = acc0_101 + x0_100 * h0_100;
	x0_101 = x0_100;
	acc0_99 = acc0_100 + x0_99 * h0_99;
	x0_100 = x0_99;
	acc0_98 = acc0_99 + x0_98 * h0_98;
	x0_99 = x0_98;
	acc0_97 = acc0_98 + x0_97 * h0_97;
	x0_98 = x0_97;
	acc0_96 = acc0_97 + x0_96 * h0_96;
	x0_97 = x0_96;
	acc0_95 = acc0_96 + x0_95 * h0_95;
	x0_96 = x0_95;
	acc0_94 = acc0_95 + x0_94 * h0_94;
	x0_95 = x0_94;
	acc0_93 = acc0_94 + x0_93 * h0_93;
	x0_94 = x0_93;
	acc0_92 = acc0_93 + x0_92 * h0_92;
	x0_93 = x0_92;
	acc0_91 = acc0_92 + x0_91 * h0_91;
	x0_92 = x0_91;
	acc0_90 = acc0_91 + x0_90 * h0_90;
	x0_91 = x0_90;
	acc0_89 = acc0_90 + x0_89 * h0_89;
	x0_90 = x0_89;
	acc0_88 = acc0_89 + x0_88 * h0_88;
	x0_89 = x0_88;
	acc0_87 = acc0_88 + x0_87 * h0_87;
	x0_88 = x0_87;
	acc0_86 = acc0_87 + x0_86 * h0_86;
	x0_87 = x0_86;
	acc0_85 = acc0_86 + x0_85 * h0_85;
	x0_86 = x0_85;
	acc0_84 = acc0_85 + x0_84 * h0_84;
	x0_85 = x0_84;
	acc0_83 = acc0_84 + x0_83 * h0_83;
	x0_84 = x0_83;
	acc0_82 = acc0_83 + x0_82 * h0_82;
	x0_83 = x0_82;
	acc0_81 = acc0_82 + x0_81 * h0_81;
	x0_82 = x0_81;
	acc0_80 = acc0_81 + x0_80 * h0_80;
	x0_81 = x0_80;
	acc0_79 = acc0_80 + x0_79 * h0_79;
	x0_80 = x0_79;
	acc0_78 = acc0_79 + x0_78 * h0_78;
	x0_79 = x0_78;
	acc0_77 = acc0_78 + x0_77 * h0_77;
	x0_78 = x0_77;
	acc0_76 = acc0_77 + x0_76 * h0_76;
	x0_77 = x0_76;
	acc0_75 = acc0_76 + x0_75 * h0_75;
	x0_76 = x0_75;
	acc0_74 = acc0_75 + x0_74 * h0_74;
	x0_75 = x0_74;
	acc0_73 = acc0_74 + x0_73 * h0_73;
	x0_74 = x0_73;
	acc0_72 = acc0_73 + x0_72 * h0_72;
	x0_73 = x0_72;
	acc0_71 = acc0_72 + x0_71 * h0_71;
	x0_72 = x0_71;
	acc0_70 = acc0_71 + x0_70 * h0_70;
	x0_71 = x0_70;
	acc0_69 = acc0_70 + x0_69 * h0_69;
	x0_70 = x0_69;
	acc0_68 = acc0_69 + x0_68 * h0_68;
	x0_69 = x0_68;
	acc0_67 = acc0_68 + x0_67 * h0_67;
	x0_68 = x0_67;
	acc0_66 = acc0_67 + x0_66 * h0_66;
	x0_67 = x0_66;
	acc0_65 = acc0_66 + x0_65 * h0_65;
	x0_66 = x0_65;
	acc0_64 = acc0_65 + x0_64 * h0_64;
	x0_65 = x0_64;
	acc0_63 = acc0_64 + x0_63 * h0_63;
	x0_64 = x0_63;
	acc0_62 = acc0_63 + x0_62 * h0_62;
	x0_63 = x0_62;
	acc0_61 = acc0_62 + x0_61 * h0_61;
	x0_62 = x0_61;
	acc0_60 = acc0_61 + x0_60 * h0_60;
	x0_61 = x0_60;
	acc0_59 = acc0_60 + x0_59 * h0_59;
	x0_60 = x0_59;
	acc0_58 = acc0_59 + x0_58 * h0_58;
	x0_59 = x0_58;
	acc0_57 = acc0_58 + x0_57 * h0_57;
	x0_58 = x0_57;
	acc0_56 = acc0_57 + x0_56 * h0_56;
	x0_57 = x0_56;
	acc0_55 = acc0_56 + x0_55 * h0_55;
	x0_56 = x0_55;
	acc0_54 = acc0_55 + x0_54 * h0_54;
	x0_55 = x0_54;
	acc0_53 = acc0_54 + x0_53 * h0_53;
	x0_54 = x0_53;
	acc0_52 = acc0_53 + x0_52 * h0_52;
	x0_53 = x0_52;
	acc0_51 = acc0_52 + x0_51 * h0_51;
	x0_52 = x0_51;
	acc0_50 = acc0_51 + x0_50 * h0_50;
	x0_51 = x0_50;
	acc0_49 = acc0_50 + x0_49 * h0_49;
	x0_50 = x0_49;
	acc0_48 = acc0_49 + x0_48 * h0_48;
	x0_49 = x0_48;
	acc0_47 = acc0_48 + x0_47 * h0_47;
	x0_48 = x0_47;
	acc0_46 = acc0_47 + x0_46 * h0_46;
	x0_47 = x0_46;
	acc0_45 = acc0_46 + x0_45 * h0_45;
	x0_46 = x0_45;
	acc0_44 = acc0_45 + x0_44 * h0_44;
	x0_45 = x0_44;
	acc0_43 = acc0_44 + x0_43 * h0_43;
	x0_44 = x0_43;
	acc0_42 = acc0_43 + x0_42 * h0_42;
	x0_43 = x0_42;
	acc0_41 = acc0_42 + x0_41 * h0_41;
	x0_42 = x0_41;
	acc0_40 = acc0_41 + x0_40 * h0_40;
	x0_41 = x0_40;
	acc0_39 = acc0_40 + x0_39 * h0_39;
	x0_40 = x0_39;
	acc0_38 = acc0_39 + x0_38 * h0_38;
	x0_39 = x0_38;
	acc0_37 = acc0_38 + x0_37 * h0_37;
	x0_38 = x0_37;
	acc0_36 = acc0_37 + x0_36 * h0_36;
	x0_37 = x0_36;
	acc0_35 = acc0_36 + x0_35 * h0_35;
	x0_36 = x0_35;
	acc0_34 = acc0_35 + x0_34 * h0_34;
	x0_35 = x0_34;
	acc0_33 = acc0_34 + x0_33 * h0_33;
	x0_34 = x0_33;
	acc0_32 = acc0_33 + x0_32 * h0_32;
	x0_33 = x0_32;
	acc0_31 = acc0_32 + x0_31 * h0_31;
	x0_32 = x0_31;
	acc0_30 = acc0_31 + x0_30 * h0_30;
	x0_31 = x0_30;
	acc0_29 = acc0_30 + x0_29 * h0_29;
	x0_30 = x0_29;
	acc0_28 = acc0_29 + x0_28 * h0_28;
	x0_29 = x0_28;
	acc0_27 = acc0_28 + x0_27 * h0_27;
	x0_28 = x0_27;
	acc0_26 = acc0_27 + x0_26 * h0_26;
	x0_27 = x0_26;
	acc0_25 = acc0_26 + x0_25 * h0_25;
	x0_26 = x0_25;
	acc0_24 = acc0_25 + x0_24 * h0_24;
	x0_25 = x0_24;
	acc0_23 = acc0_24 + x0_23 * h0_23;
	x0_24 = x0_23;
	acc0_22 = acc0_23 + x0_22 * h0_22;
	x0_23 = x0_22;
	acc0_21 = acc0_22 + x0_21 * h0_21;
	x0_22 = x0_21;
	acc0_20 = acc0_21 + x0_20 * h0_20;
	x0_21 = x0_20;
	acc0_19 = acc0_20 + x0_19 * h0_19;
	x0_20 = x0_19;
	acc0_18 = acc0_19 + x0_18 * h0_18;
	x0_19 = x0_18;
	acc0_17 = acc0_18 + x0_17 * h0_17;
	x0_18 = x0_17;
	acc0_16 = acc0_17 + x0_16 * h0_16;
	x0_17 = x0_16;
	acc0_15 = acc0_16 + x0_15 * h0_15;
	x0_16 = x0_15;
	acc0_14 = acc0_15 + x0_14 * h0_14;
	x0_15 = x0_14;
	acc0_13 = acc0_14 + x0_13 * h0_13;
	x0_14 = x0_13;
	acc0_12 = acc0_13 + x0_12 * h0_12;
	x0_13 = x0_12;
	acc0_11 = acc0_12 + x0_11 * h0_11;
	x0_12 = x0_11;
	acc0_10 = acc0_11 + x0_10 * h0_10;
	x0_11 = x0_10;
	acc0_9 = acc0_10 + x0_9 * h0_9;
	x0_10 = x0_9;
	acc0_8 = acc0_9 + x0_8 * h0_8;
	x0_9 = x0_8;
	acc0_7 = acc0_8 + x0_7 * h0_7;
	x0_8 = x0_7;
	acc0_6 = acc0_7 + x0_6 * h0_6;
	x0_7 = x0_6;
	acc0_5 = acc0_6 + x0_5 * h0_5;
	x0_6 = x0_5;
	acc0_4 = acc0_5 + x0_4 * h0_4;
	x0_5 = x0_4;
	acc0_3 = acc0_4 + x0_3 * h0_3;
	x0_4 = x0_3;
	acc0_2 = acc0_3 + x0_2 * h0_2;
	x0_3 = x0_2;
	acc0_1 = acc0_2 + x0_1 * h0_1;
	x0_2 = x0_1;
	acc0_0 = acc0_1 + x0_0 * h0_0;
	x0_1 = x0_0;
	x1_0 = acc0_0;
	acc1_128 = x1_128 * h1_128;
	acc1_127 = acc1_128 + x1_127 * h1_127;
	x1_128 = x1_127;
	acc1_126 = acc1_127 + x1_126 * h1_126;
	x1_127 = x1_126;
	acc1_125 = acc1_126 + x1_125 * h1_125;
	x1_126 = x1_125;
	acc1_124 = acc1_125 + x1_124 * h1_124;
	x1_125 = x1_124;
	acc1_123 = acc1_124 + x1_123 * h1_123;
	x1_124 = x1_123;
	acc1_122 = acc1_123 + x1_122 * h1_122;
	x1_123 = x1_122;
	acc1_121 = acc1_122 + x1_121 * h1_121;
	x1_122 = x1_121;
	acc1_120 = acc1_121 + x1_120 * h1_120;
	x1_121 = x1_120;
	acc1_119 = acc1_120 + x1_119 * h1_119;
	x1_120 = x1_119;
	acc1_118 = acc1_119 + x1_118 * h1_118;
	x1_119 = x1_118;
	acc1_117 = acc1_118 + x1_117 * h1_117;
	x1_118 = x1_117;
	acc1_116 = acc1_117 + x1_116 * h1_116;
	x1_117 = x1_116;
	acc1_115 = acc1_116 + x1_115 * h1_115;
	x1_116 = x1_115;
	acc1_114 = acc1_115 + x1_114 * h1_114;
	x1_115 = x1_114;
	acc1_113 = acc1_114 + x1_113 * h1_113;
	x1_114 = x1_113;
	acc1_112 = acc1_113 + x1_112 * h1_112;
	x1_113 = x1_112;
	acc1_111 = acc1_112 + x1_111 * h1_111;
	x1_112 = x1_111;
	acc1_110 = acc1_111 + x1_110 * h1_110;
	x1_111 = x1_110;
	acc1_109 = acc1_110 + x1_109 * h1_109;
	x1_110 = x1_109;
	acc1_108 = acc1_109 + x1_108 * h1_108;
	x1_109 = x1_108;
	acc1_107 = acc1_108 + x1_107 * h1_107;
	x1_108 = x1_107;
	acc1_106 = acc1_107 + x1_106 * h1_106;
	x1_107 = x1_106;
	acc1_105 = acc1_106 + x1_105 * h1_105;
	x1_106 = x1_105;
	acc1_104 = acc1_105 + x1_104 * h1_104;
	x1_105 = x1_104;
	acc1_103 = acc1_104 + x1_103 * h1_103;
	x1_104 = x1_103;
	acc1_102 = acc1_103 + x1_102 * h1_102;
	x1_103 = x1_102;
	acc1_101 = acc1_102 + x1_101 * h1_101;
	x1_102 = x1_101;
	acc1_100 = acc1_101 + x1_100 * h1_100;
	x1_101 = x1_100;
	acc1_99 = acc1_100 + x1_99 * h1_99;
	x1_100 = x1_99;
	acc1_98 = acc1_99 + x1_98 * h1_98;
	x1_99 = x1_98;
	acc1_97 = acc1_98 + x1_97 * h1_97;
	x1_98 = x1_97;
	acc1_96 = acc1_97 + x1_96 * h1_96;
	x1_97 = x1_96;
	acc1_95 = acc1_96 + x1_95 * h1_95;
	x1_96 = x1_95;
	acc1_94 = acc1_95 + x1_94 * h1_94;
	x1_95 = x1_94;
	acc1_93 = acc1_94 + x1_93 * h1_93;
	x1_94 = x1_93;
	acc1_92 = acc1_93 + x1_92 * h1_92;
	x1_93 = x1_92;
	acc1_91 = acc1_92 + x1_91 * h1_91;
	x1_92 = x1_91;
	acc1_90 = acc1_91 + x1_90 * h1_90;
	x1_91 = x1_90;
	acc1_89 = acc1_90 + x1_89 * h1_89;
	x1_90 = x1_89;
	acc1_88 = acc1_89 + x1_88 * h1_88;
	x1_89 = x1_88;
	acc1_87 = acc1_88 + x1_87 * h1_87;
	x1_88 = x1_87;
	acc1_86 = acc1_87 + x1_86 * h1_86;
	x1_87 = x1_86;
	acc1_85 = acc1_86 + x1_85 * h1_85;
	x1_86 = x1_85;
	acc1_84 = acc1_85 + x1_84 * h1_84;
	x1_85 = x1_84;
	acc1_83 = acc1_84 + x1_83 * h1_83;
	x1_84 = x1_83;
	acc1_82 = acc1_83 + x1_82 * h1_82;
	x1_83 = x1_82;
	acc1_81 = acc1_82 + x1_81 * h1_81;
	x1_82 = x1_81;
	acc1_80 = acc1_81 + x1_80 * h1_80;
	x1_81 = x1_80;
	acc1_79 = acc1_80 + x1_79 * h1_79;
	x1_80 = x1_79;
	acc1_78 = acc1_79 + x1_78 * h1_78;
	x1_79 = x1_78;
	acc1_77 = acc1_78 + x1_77 * h1_77;
	x1_78 = x1_77;
	acc1_76 = acc1_77 + x1_76 * h1_76;
	x1_77 = x1_76;
	acc1_75 = acc1_76 + x1_75 * h1_75;
	x1_76 = x1_75;
	acc1_74 = acc1_75 + x1_74 * h1_74;
	x1_75 = x1_74;
	acc1_73 = acc1_74 + x1_73 * h1_73;
	x1_74 = x1_73;
	acc1_72 = acc1_73 + x1_72 * h1_72;
	x1_73 = x1_72;
	acc1_71 = acc1_72 + x1_71 * h1_71;
	x1_72 = x1_71;
	acc1_70 = acc1_71 + x1_70 * h1_70;
	x1_71 = x1_70;
	acc1_69 = acc1_70 + x1_69 * h1_69;
	x1_70 = x1_69;
	acc1_68 = acc1_69 + x1_68 * h1_68;
	x1_69 = x1_68;
	acc1_67 = acc1_68 + x1_67 * h1_67;
	x1_68 = x1_67;
	acc1_66 = acc1_67 + x1_66 * h1_66;
	x1_67 = x1_66;
	acc1_65 = acc1_66 + x1_65 * h1_65;
	x1_66 = x1_65;
	acc1_64 = acc1_65 + x1_64 * h1_64;
	x1_65 = x1_64;
	acc1_63 = acc1_64 + x1_63 * h1_63;
	x1_64 = x1_63;
	acc1_62 = acc1_63 + x1_62 * h1_62;
	x1_63 = x1_62;
	acc1_61 = acc1_62 + x1_61 * h1_61;
	x1_62 = x1_61;
	acc1_60 = acc1_61 + x1_60 * h1_60;
	x1_61 = x1_60;
	acc1_59 = acc1_60 + x1_59 * h1_59;
	x1_60 = x1_59;
	acc1_58 = acc1_59 + x1_58 * h1_58;
	x1_59 = x1_58;
	acc1_57 = acc1_58 + x1_57 * h1_57;
	x1_58 = x1_57;
	acc1_56 = acc1_57 + x1_56 * h1_56;
	x1_57 = x1_56;
	acc1_55 = acc1_56 + x1_55 * h1_55;
	x1_56 = x1_55;
	acc1_54 = acc1_55 + x1_54 * h1_54;
	x1_55 = x1_54;
	acc1_53 = acc1_54 + x1_53 * h1_53;
	x1_54 = x1_53;
	acc1_52 = acc1_53 + x1_52 * h1_52;
	x1_53 = x1_52;
	acc1_51 = acc1_52 + x1_51 * h1_51;
	x1_52 = x1_51;
	acc1_50 = acc1_51 + x1_50 * h1_50;
	x1_51 = x1_50;
	acc1_49 = acc1_50 + x1_49 * h1_49;
	x1_50 = x1_49;
	acc1_48 = acc1_49 + x1_48 * h1_48;
	x1_49 = x1_48;
	acc1_47 = acc1_48 + x1_47 * h1_47;
	x1_48 = x1_47;
	acc1_46 = acc1_47 + x1_46 * h1_46;
	x1_47 = x1_46;
	acc1_45 = acc1_46 + x1_45 * h1_45;
	x1_46 = x1_45;
	acc1_44 = acc1_45 + x1_44 * h1_44;
	x1_45 = x1_44;
	acc1_43 = acc1_44 + x1_43 * h1_43;
	x1_44 = x1_43;
	acc1_42 = acc1_43 + x1_42 * h1_42;
	x1_43 = x1_42;
	acc1_41 = acc1_42 + x1_41 * h1_41;
	x1_42 = x1_41;
	acc1_40 = acc1_41 + x1_40 * h1_40;
	x1_41 = x1_40;
	acc1_39 = acc1_40 + x1_39 * h1_39;
	x1_40 = x1_39;
	acc1_38 = acc1_39 + x1_38 * h1_38;
	x1_39 = x1_38;
	acc1_37 = acc1_38 + x1_37 * h1_37;
	x1_38 = x1_37;
	acc1_36 = acc1_37 + x1_36 * h1_36;
	x1_37 = x1_36;
	acc1_35 = acc1_36 + x1_35 * h1_35;
	x1_36 = x1_35;
	acc1_34 = acc1_35 + x1_34 * h1_34;
	x1_35 = x1_34;
	acc1_33 = acc1_34 + x1_33 * h1_33;
	x1_34 = x1_33;
	acc1_32 = acc1_33 + x1_32 * h1_32;
	x1_33 = x1_32;
	acc1_31 = acc1_32 + x1_31 * h1_31;
	x1_32 = x1_31;
	acc1_30 = acc1_31 + x1_30 * h1_30;
	x1_31 = x1_30;
	acc1_29 = acc1_30 + x1_29 * h1_29;
	x1_30 = x1_29;
	acc1_28 = acc1_29 + x1_28 * h1_28;
	x1_29 = x1_28;
	acc1_27 = acc1_28 + x1_27 * h1_27;
	x1_28 = x1_27;
	acc1_26 = acc1_27 + x1_26 * h1_26;
	x1_27 = x1_26;
	acc1_25 = acc1_26 + x1_25 * h1_25;
	x1_26 = x1_25;
	acc1_24 = acc1_25 + x1_24 * h1_24;
	x1_25 = x1_24;
	acc1_23 = acc1_24 + x1_23 * h1_23;
	x1_24 = x1_23;
	acc1_22 = acc1_23 + x1_22 * h1_22;
	x1_23 = x1_22;
	acc1_21 = acc1_22 + x1_21 * h1_21;
	x1_22 = x1_21;
	acc1_20 = acc1_21 + x1_20 * h1_20;
	x1_21 = x1_20;
	acc1_19 = acc1_20 + x1_19 * h1_19;
	x1_20 = x1_19;
	acc1_18 = acc1_19 + x1_18 * h1_18;
	x1_19 = x1_18;
	acc1_17 = acc1_18 + x1_17 * h1_17;
	x1_18 = x1_17;
	acc1_16 = acc1_17 + x1_16 * h1_16;
	x1_17 = x1_16;
	acc1_15 = acc1_16 + x1_15 * h1_15;
	x1_16 = x1_15;
	acc1_14 = acc1_15 + x1_14 * h1_14;
	x1_15 = x1_14;
	acc1_13 = acc1_14 + x1_13 * h1_13;
	x1_14 = x1_13;
	acc1_12 = acc1_13 + x1_12 * h1_12;
	x1_13 = x1_12;
	acc1_11 = acc1_12 + x1_11 * h1_11;
	x1_12 = x1_11;
	acc1_10 = acc1_11 + x1_10 * h1_10;
	x1_11 = x1_10;
	acc1_9 = acc1_10 + x1_9 * h1_9;
	x1_10 = x1_9;
	acc1_8 = acc1_9 + x1_8 * h1_8;
	x1_9 = x1_8;
	acc1_7 = acc1_8 + x1_7 * h1_7;
	x1_8 = x1_7;
	acc1_6 = acc1_7 + x1_6 * h1_6;
	x1_7 = x1_6;
	acc1_5 = acc1_6 + x1_5 * h1_5;
	x1_6 = x1_5;
	acc1_4 = acc1_5 + x1_4 * h1_4;
	x1_5 = x1_4;
	acc1_3 = acc1_4 + x1_3 * h1_3;
	x1_4 = x1_3;
	acc1_2 = acc1_3 + x1_2 * h1_2;
	x1_3 = x1_2;
	acc1_1 = acc1_2 + x1_1 * h1_1;
	x1_2 = x1_1;
	acc1_0 = acc1_1 + x1_0 * h1_0;
	x1_1 = x1_0;
	fpO[0] = acc1_0;

}


#ifdef __GECOS_TYPE_EXPL__
#pragma MAIN_FUNC
void test_fir_RI() {
	int i, j;

	double fpI[1];
	double fpO[1];
	double result[NSIM];


	for (i = 0; i < NSIM; i++) {
		if (i == 0)
			fpI[0] = 1.0;
		else
			fpI[0] = 0.0;

		fir(fpI, fpO);

		result[i] = fpO[0];
	}

	$save(result);
}
#endif


#ifndef __GECOS_TYPE_EXPL__
int main(int argc, char **argv) {

    if (argc < 1) {
        printf("usage: fir_Nx512 \n");
        exit(-1);
    }
    int i,j;

    double fpI;
    double fpO;


    for (i = 0; i < NSIM; i++) {
    	if (i == 0)
    		fpI = 1.0;
    	else
    		fpI = 0.0;

    	fir(&fpI,&fpO);

    	//printf("iteration %d : %f %f\n",i,fpI,fpO);
    	printf("%f ",fpO);
    }




}
#endif

