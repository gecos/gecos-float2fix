/*
* FIR filter
*
*
* Author: Olivier SENTIEYS
* Data: 26/12/2018
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 17
#define NSIM 30

static double x[N];

#define __GECOS_TYPE_EXPL__


void fir(
#pragma EXPLORE_FIX W={8..32} I={6}
		double fpI[1],
#pragma EXPLORE_CONSTRAINT SAME = fpI
		double fpO[1])
{

	int i, j;
	double acc;

#pragma EXPLORE_CONSTRAINT SAME = fpI
	double h[N] = {
			-0.043954858295096467524221139910878264345,
			-0.035981065546366389207300073849182808772,
			 0.050699349043343402976269373994000488892,
			 0.030422136814391986803229883662424981594,
			-0.036391465734247646635068917930766474456,
			-0.096520567607945445542938500693708192557,
			 0.052875967350882055462157893543917452917,
			 0.309230129128371000746255958802066743374,
			 0.453542015270236742452425460214726626873,
			 0.309230129128371000746255958802066743374,
			 0.052875967350882055462157893543917452917,
			-0.096520567607945445542938500693708192557,
			-0.036391465734247646635068917930766474456,
			 0.030422136814391986803229883662424981594,
			 0.050699349043343402976269373994000488892,
			-0.035981065546366389207300073849182808772,
			-0.043954858295096467524221139910878264345 };


	x[0] = fpI[0];
	acc = x[N-1] * h[N-1];
	for (i = N-2; i >= 0; i--){
	   	acc += x[i] * h[i];
	    x[i+1] = x[i];
	}
	fpO[0] = acc;

}


#pragma MAIN_FUNC
void test_fir_RI() {
	int i, j;

	double fpI[1];
	double fpO[1];
	double result[NSIM];

	for (i = 0; i < N; i++) {
		x[i] = 0.0;
	}

	for (i = 0; i < NSIM; i++) {
		if (i == 0)
			fpI[0] = 1.0;
		else
			fpI[0] = 0.0;

		fir(fpI, fpO);

		result[i] = fpO[0];
	}

#ifdef __GECOS_TYPE_EXPL__
	$save(result);
#endif

}

// This one generate a bug during exploration (apparently due to $inject)
//#pragma MAIN_FUNC
//void test_fir_RANDOM() {
//	int i, j;
//
//	double fpI[1];
//	double fpO[1];
//	double result[NSIM];
//
//	#ifdef __GECOS_TYPE_EXPL__
//	$inject(x, $from_var(0.0));
//	#endif
//
//
//	for (i = 0; i < NSIM; i++) {
//
//		#ifdef __GECOS_TYPE_EXPL__
//		$inject(fpI[1], $random_uniform(-1, 1));
//		#endif
//
//		fir(fpI, fpO);
//
//		result[i] = fpO[0];
//	}
//
//#ifdef __GECOS_TYPE_EXPL__
//	$save(result);
//#endif
//
//}

#ifndef __GECOS_TYPE_EXPL__
int main(int argc, char **argv) {

    if (argc < 1) {
        printf("usage: fir \n");
        exit(-1);
    }
    int i,j;

    double fpI;
    double fpO;

	for (i = 0; i < N; i++) {
		x[i] = 0.0;
	}

    for (i = 0; i < 30; i++) {
    	if (i == 0)
    		fpI = 1.0;
    	else
    		fpI = 0.0;

    	fir(&fpI,&fpO);

    	printf("iteration %d : %f %f\n",i,fpI,fpO);
    	//for (j = 0; j < N; j++) printf("%f ",x[j]); printf("\n");
    	//printf("%f ",fpO);
    }
}
#endif
