/*
* FIR filter
*
*
* Author: Olivier SENTIEYS
* Data: 26/12/2018
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 129
#define NSIM 300

static double x[N];

//#define __GECOS_TYPE_EXPL__


void fir(
#pragma EXPLORE_FIX W={8..32} I={6}
		double fpI[1],
#pragma EXPLORE_CONSTRAINT SAME = fpI
		double fpO[1])
{

	int i, j;
	double acc;

#pragma EXPLORE_CONSTRAINT SAME = fpI
	double h[N]={0.000002, 0.000007, -0.000004, -0.000015, 0.000000, 0.000028, 0.000011, -0.000044, -0.000037, 0.000057, 0.000081, -0.000058, -0.000145,
			0.000031, 0.000226, 0.000041, -0.000308, -0.000172, 0.000367, 0.000373, -0.000367, -0.000639, 0.000261, 0.000946, -0.000005, -0.001243, -0.000438,
			0.001453, 0.001078, -0.001476, -0.001891, 0.001201, 0.002800, -0.000524, -0.003673, -0.000629, 0.004324, 0.002277, -0.004522, -0.004355, 0.004018,
			0.006698, -0.002578, -0.009029, 0.000016, 0.010964, 0.003763, -0.012022, -0.008732, 0.011645, 0.014728, -0.009204, -0.021442, 0.003971, 0.028446,
			0.005030, -0.035226, -0.019521, 0.041237, 0.043757, -0.045963, -0.093581, 0.048984, 0.314036, 0.449977, 0.314036, 0.048984, -0.093581, -0.045963,
			0.043757, 0.041237, -0.019521, -0.035226, 0.005030, 0.028446, 0.003971, -0.021442, -0.009204, 0.014728, 0.011645, -0.008732, -0.012022, 0.003763,
			0.010964, 0.000016, -0.009029, -0.002578, 0.006698, 0.004018, -0.004355, -0.004522, 0.002277, 0.004324, -0.000629, -0.003673, -0.000524, 0.002800,
			0.001201, -0.001891, -0.001476, 0.001078, 0.001453, -0.000438, -0.001243, -0.000005, 0.000946, 0.000261, -0.000639, -0.000367, 0.000373, 0.000367,
			-0.000172, -0.000308, 0.000041, 0.000226, 0.000031, -0.000145, -0.000058, 0.000081, 0.000057, -0.000037, -0.000044, 0.000011, 0.000028, 0.000000,
			-0.000015, -0.000004, 0.000007, 0.000002 };


	x[0] = fpI[0];
	acc = x[N-1] * h[N-1];
	for (i = N-2; i >= 0; i--){
	   	acc += x[i] * h[i];
	    x[i+1] = x[i];
	}
	fpO[0] = acc;

}


#pragma MAIN_FUNC
void test_fir_RI() {
	int i, j;

	double fpI[1];
	double fpO[1];
	double result[NSIM];

	for (i = 0; i < N; i++) {
		x[i] = 0.0;
	}

	for (i = 0; i < NSIM; i++) {
		if (i == 0)
			fpI[0] = 1.0;
		else
			fpI[0] = 0.0;

		fir(fpI, fpO);

		result[i] = fpO[0];
	}

#ifdef __GECOS_TYPE_EXPL__
	$save(result);
#endif

}

// This one generate a bug during exploration (apparently due to $inject)
//#pragma MAIN_FUNC
//void test_fir_RANDOM() {
//	int i, j;
//
//	double fpI[1];
//	double fpO[1];
//	double result[NSIM];
//
//	#ifdef __GECOS_TYPE_EXPL__
//	$inject(x, $from_var(0.0));
//	#endif
//
//
//	for (i = 0; i < NSIM; i++) {
//
//		#ifdef __GECOS_TYPE_EXPL__
//		$inject(fpI[1], $random_uniform(-1, 1));
//		#endif
//
//		fir(fpI, fpO);
//
//		result[i] = fpO[0];
//	}
//
//#ifdef __GECOS_TYPE_EXPL__
//	$save(result);
//#endif
//
//}

#ifndef __GECOS_TYPE_EXPL__
int main(int argc, char **argv) {

    if (argc < 1) {
        printf("usage: fir \n");
        exit(-1);
    }
    int i,j;

    double fpI;
    double fpO;

	for (i = 0; i < N; i++) {
		x[i] = 0.0;
	}

    for (i = 0; i < NSIM; i++) {
    	if (i == 0)
    		fpI = 1.0;
    	else
    		fpI = 0.0;

    	fir(&fpI,&fpO);

    	printf("iteration %d : %f %f\n",i,fpI,fpO);
    	//for (j = 0; j < N; j++) printf("%f ",x[j]); printf("\n");
    	//printf("%f ",fpO);
    }
}
#endif
