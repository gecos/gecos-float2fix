debug(2);

sourceDir = "src-c/";

p = CreateGecosProject("fir_2x128_fullyunrolled");
AddSourceToGecosProject(p, sourceDir + "fir_2x128_fullyunrolled.c");

#p = CreateGecosProject("fir_4x512_unrolled");
#AddSourceToGecosProject(p, sourceDir + "fir_4x512_unrolled.c");

#p = CreateGecosProject("fir_NxC");
#AddSourceToGecosProject(p, sourceDir + "fir_NxC.c");

#p = CreateGecosProject("fir");
#AddSourceToGecosProject(p, sourceDir + "fir.c");

AddIncDirToGecosProject(p, sourceDir);

#TypesExploration("."); #regenearate default.properties

TypesExploration(p, "default.properties");
