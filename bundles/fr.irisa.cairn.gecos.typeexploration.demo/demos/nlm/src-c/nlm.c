/*
* NLM: Non-local means denoising
*
* Function: void nlm(float img_in[], float sd, int k, int n, float img_out[])
*
* output:
*	mat_out[]: output image (gray image), dimension: size_h x size_w
*
* input:
*	mat_in[]: input image (gray image), dimension: size_h x size_w
*	sd: standard deviation of the noise in the image
*	k: size of the patch used in matching (assumed square if scalar)
*	n: size of the search neighborhood (assumed square if scalar)
* 	s: strike
*	thr: patch similarity threshold
*
* Author: Van-Phu HA
* Email: van-phu.ha@inria.fr
* Data: 22/3/2018
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "utils.h"

#define DTYPE float
#define DTYPE_DIST double

#define H  240 //2976
#define W  320 //3968

#define K 5
#define N 13

//K2 and N2 are floor(K/2) and floor(N/2)
#define K2 2
#define N2 6
// CROP = N2 - K2
#define CROP 4

//size_h_pad = H+2*N2;
#define SIZE_H_PAD  (H+2*N2)  //2988
//size_w_pad = W+2*N2;
#define SIZE_W_PAD  (W+2*N2)  //3980

#define S 2

//size_h_ref = H + N2 - K2
#define SIZE_H_REF (H + N2 - K2)  //2980
//size_w_ref = H + N2 - K2
#define SIZE_W_REF (H + N2 - K2)  //3972

// row_filt = 3
#define ROW_FILT 3
// col_filt = 3
#define COL_FILT 3

const int dx[25] = {-4,-2,0,2,4,-4,-2,0,2,4,-4,-2,0,2,4,-4,-2,0,2,4,-4,-2,0,2,4};
const int dy[25] = {-4,-4,-4,-4,-4,-2,-2,-2,-2,-2,0,0,0,0,0,2,2,2,2,2,4,4,4,4,4};
const int dx_patch[24] = {-4,-2,0,2,4,-4,-2,0,2,4,-4,-2,2,4,-4,-2,0,2,4,-4,-2,0,2,4};
const int dy_patch[24] = {-4,-4,-4,-4,-4,-2,-2,-2,-2,-2,0,0,0,0,2,2,2,2,2,4,4,4,4,4};
const DTYPE filter[3][3] = {{0.0625, 0.1250, 0.0625}, {0.1250, 0.2500, 0.1250}, {0.0625, 0.1250, 0.0625}};

#ifndef max
    #define max(a,b) ((a) > (b) ? (a) : (b))
#endif

#ifndef min
    #define min(a,b) ((a) < (b) ? (a) : (b))
#endif

#ifndef sqr
	#define sqr(a) ((a)*(a))
#endif

void my_imfilter(DTYPE mat_out[H][W], DTYPE mat_in[H][W], const DTYPE filter[ROW_FILT][COL_FILT])
{
	int i, j, p, q;

	int r2 = floor(ROW_FILT/2.0);
	int c2 = floor(COL_FILT/2.0);

	int size_h_in_pad = 2*r2+H;
	int size_w_in_pad = 2*c2+W;


	//!! added: DTYPE will be replace by a hard coded type which won't change when the type of in_pad changes
	typedef DTYPE TYPE_in_pad;

#pragma EXPLORE_CONSTRAINT SAME = mat_in
	TYPE_in_pad *in_pad = (TYPE_in_pad*) malloc (size_h_in_pad*size_w_in_pad*sizeof(TYPE_in_pad));


	/* replicate padding for input image */

	for(i=-r2; i<H+r2; i++)
	{
		for(j=-c2; j<W+c2; j++)
		{
			in_pad[(i+r2)*size_w_in_pad + j+c2] = mat_in[min(max(i,0),H-1)][min(max(j,0),W-1)];
		}
	}

#pragma EXPLORE_CONSTRAINT SAME = mat_out
	DTYPE tmp;
	// Do filtering
	for(i=r2; i<H+r2; i++)
	{
		for(j=c2; j<W+c2; j++)
		{
			tmp = 0.0;
			for(p=0; p<ROW_FILT; p++)
			{
				for(q=0; q<COL_FILT; q++)
				{
					tmp = tmp + filter[p][q]*in_pad[((i-r2)+p)*size_w_in_pad + (j-c2)+q];
				}
			}
			mat_out[(i-r2)][(j-c2)] = tmp;
		}
	}

	free(in_pad);
}

#pragma hls_design top
void do_loop(DTYPE mat_out[H][W], DTYPE mat_in[H][W], DTYPE filt_matrix[H][W], DTYPE mat_in_pad[SIZE_H_PAD][SIZE_W_PAD],
		DTYPE_DIST mat_sqdiff[SIZE_H_REF][SIZE_W_REF],
#pragma EXPLORE_FIX W={28..32} I={16}
		DTYPE mat_denoised[H][W],
		DTYPE mat_acc_weight[H][W], DTYPE np1, DTYPE np2, DTYPE thr, DTYPE black, DTYPE f, DTYPE m)
{
	int i, j, k;

#pragma EXPLORE_CONSTRAINT SAME = mat_sqdiff  //!! this could be automatically derived by looking into all tmp defs..
	DTYPE_DIST tmp;

#pragma EXPLORE_CONSTRAINT SAME = mat_sqdiff //!! might overflow !!
	DTYPE_DIST tmp_dist;

	DTYPE_DIST tmp_diff;
	DTYPE nv;
	DTYPE fm;

#pragma EXPLORE_FIX W={32} I={1}
	DTYPE thrSqaured = thr*thr;

	for (k = 0; k < (int)(sizeof(dx_patch)/sizeof(DTYPE)); k++)
	{
		tmp = 0.0;
		tmp_dist = 0.0;
		tmp_diff = 0.0;
		for (int i = CROP; i < SIZE_H_REF + CROP; i++)
		{
			for (int j = CROP; j < SIZE_W_REF + CROP; j++)
			{
				// Calculate mat_sqdiff
				// tmp_diff = (mat_in_pad[i][j] - mat_in_pad[i+dy_patch[k]][j+dx_patch[k]]);
				// mat_sqdiff[i-CROP][j-CROP] = (DTYPE_DIST)tmp_diff*tmp_diff;
				mat_sqdiff[i-CROP][j-CROP]  = (DTYPE_DIST)((mat_in_pad[i][j] - mat_in_pad[i+dy_patch[k]][j+dx_patch[k]])*(mat_in_pad[i][j] - mat_in_pad[i+dy_patch[k]][j+dx_patch[k]]));
				if((i==CROP) && (j>CROP))
				{
					tmp = mat_sqdiff[i-CROP][j-CROP-1];
				}
				if(j>CROP)
				{
					mat_sqdiff[i-CROP][j-CROP] = mat_sqdiff[i-CROP][j-CROP] + tmp;
				}
				if(i>CROP)
				{
					tmp = mat_sqdiff[i-CROP][j-CROP];
					mat_sqdiff[i-CROP][j-CROP] = mat_sqdiff[i-CROP][j-CROP] + mat_sqdiff[i-CROP-1][j-CROP];
				}

				// Calculate mat_denoised and mat_acc_weight
				if((i>=CROP+K-1) && (j>=CROP+K-1))
				{
					tmp_dist = mat_sqdiff[i-CROP][j-CROP];

					if(i>(CROP+K-1))
					{
						tmp_dist = tmp_dist - mat_sqdiff[i-K-CROP][j-CROP];
					}
					if(j>(CROP+K-1))
					{
						tmp_dist = tmp_dist - mat_sqdiff[i-CROP][j-K-CROP];
					}
					if((i>(CROP+K-1)) && (j>(CROP+K-1)))
					{
						tmp_dist = tmp_dist + mat_sqdiff[i-K-CROP][j-K-CROP];
					}

					//!! changed
//					mat_denoised[i-CROP-K+1][j-CROP-K+1] = (DTYPE)tmp_dist;
					mat_denoised[i-CROP-K+1][j-CROP-K+1] = tmp_dist;

					nv = np1*filt_matrix[i-CROP-K+1][j-CROP-K+1] + np2;

					//!! changed
//					mat_denoised[i-CROP-K+1][j-CROP-K+1] = -max(mat_denoised[i-CROP-K+1][j-CROP-K+1]/(K*K) - 2*nv, 0);
					mat_denoised[i-CROP-K+1][j-CROP-K+1] = -max((double)(mat_denoised[i-CROP-K+1][j-CROP-K+1])/(25.0) - (double)(2*nv), 0);

					mat_denoised[i-CROP-K+1][j-CROP-K+1] = mat_denoised[i-CROP-K+1][j-CROP-K+1]/(thrSqaured*nv);

					mat_denoised[i-CROP-K+1][j-CROP-K+1] = pow(2.7182817, mat_denoised[i-CROP-K+1][j-CROP-K+1]);

					// Compute and accumulate denoised pixels
					mat_out[i-CROP-K+1][j-CROP-K+1] +=mat_denoised[i-CROP-K+1][j-CROP-K+1] * mat_in_pad[i-K2+dy_patch[k]][j-K2+dx_patch[k]];
					// Update accumlated weights
					mat_acc_weight[i-CROP-K+1][j-CROP-K+1] += mat_denoised[i-CROP-K+1][j-CROP-K+1];
				}

			}
		}
	}

	fm = f*m;

	for(i=0; i<H; i++)
	{
		for(j=0; j<W; j++)
		{
			mat_out[i][j] = (mat_out[i][j] + fm*mat_in[i][j])/(mat_acc_weight[i][j] + fm);
		}
	}
}


#pragma MAIN_FUNC
void nlm(
#pragma EXPLORE_FIX W={8..12} I={1}
		DTYPE mat_out[H][W],
#pragma EXPLORE_FIX W={8..12} I={1}
		DTYPE mat_in[H][W],
		DTYPE np1,
#pragma EXPLORE_FIX W={32} I={1}
		DTYPE np2,
		DTYPE thr, DTYPE black, DTYPE f, DTYPE m)
{
	int i, j;	/* Counter variables */



	//!! added
	$inject(mat_in, $random_uniform(0, 1, 3));
	$inject(np1, $from_var(0.00152334432));
	$inject(np2, $from_var(9.92565509172e-006));
	$inject(thr, $from_var(0.4));
	$inject(black, $from_var(64.0/1023.0));
	$inject(f, $from_var(1.0));
	$inject(m, $from_var(1.0));



	//!! changed:
	// DTYPE will be replace by a hard coded type which won't change when the type of in_pad changes.
	// or use a typedef for each symbol

	// Allocate memory for filt_matrix
//	DTYPE *filt_matrix;
//	filt_matrix = (DTYPE*) malloc (H*W*sizeof(DTYPE));
//#pragma EXPLORE_FIX W={8..12} I={1}
	DTYPE filt_matrix[H][W];

	/* allocate memory for mat_in_pad */
//	DTYPE *mat_in_pad;
//	mat_in_pad = (DTYPE*) malloc (SIZE_H_PAD*SIZE_W_PAD*sizeof(DTYPE));
#pragma EXPLORE_CONSTRAINT SAME = mat_in
	DTYPE mat_in_pad[SIZE_H_PAD][SIZE_W_PAD];

	/* allocate memory for mat_sqdiff */
//	DTYPE_DIST *mat_sqdiff;
//	mat_sqdiff = (DTYPE_DIST*) malloc (SIZE_H_REF*SIZE_W_REF*sizeof(DTYPE_DIST));
//#pragma EXPLORE_FIX W={8..12} I={5}
	DTYPE_DIST mat_sqdiff[SIZE_H_REF][SIZE_W_REF];

	/* allocate memory for denoised pixel matrix mat_denoised */
//	DTYPE *mat_denoised;
//	mat_denoised = (DTYPE*) malloc (H*W*sizeof(DTYPE));
//#pragma EXPLORE_FIX W={12..16} I={1}
	DTYPE mat_denoised[H][W];

	/* allocate memory for accumulated weight matrix mat_acc_weight */
//	DTYPE *mat_acc_weight;
//	mat_acc_weight = (DTYPE*) malloc (H*W*sizeof(DTYPE));
//#pragma EXPLORE_FIX W={12..16} I={1}
	DTYPE mat_acc_weight[H][W];

	/* replicate padding for input image */
	for(i=-N2; i<H+N2; i++)
	{
		for(j=-N2; j<W+N2; j++)
		{
//			mat_in_pad[(i+N2)*SIZE_W_PAD + j+N2] = mat_in[min(max(i,0),H-1)][min(max(j,0),W-1)];
			mat_in_pad[(i+N2)][j+N2] = mat_in[min(max(i,0),H-1)][min(max(j,0),W-1)];
		}
	}

	/* Precalculate pseudo luma signal for noise variance estimation */
	//!! changed:
//	my_imfilter((DTYPE (*)[W])filt_matrix, mat_in, filter);
	my_imfilter(filt_matrix, mat_in, filter);

	for(i=0; i<H; i++)
	{
		for(j=0; j<W; j++)
		{
			//!! changed:
//			filt_matrix[i*W+j] = max(filt_matrix[i*W+j] - black, 0.0);
			filt_matrix[i][j] = max((double)(filt_matrix[i][j] - black), 0.0);
		}
	}

//	do_loop(mat_out, mat_in, (DTYPE (*)[W])filt_matrix, (DTYPE (*)[SIZE_W_PAD])mat_in_pad, (DTYPE_DIST (*)[SIZE_W_REF])mat_sqdiff, (DTYPE (*)[W])mat_denoised, (DTYPE (*)[W])mat_acc_weight, np1, np2, thr, black, f, m);
	do_loop(mat_out, mat_in, filt_matrix, mat_in_pad, mat_sqdiff, mat_denoised, mat_acc_weight, np1, np2, thr, black, f, m);


	//!! added
	$save(mat_out);
}



//int main(void)
//{
//	// Variables declarations
//	char input_img[32];
//	char output_img[32];
//
//	// DTYPE sd;
//	DTYPE np1, np2;
//	DTYPE thr;
//	DTYPE black;
//	DTYPE f, m; // Special controls to accumlate the contribution of the noisy pixels to be denoised
//
//	int i, j;
//
//
//	typedef double TYPE_data_in;
//	typedef double TYPE_data_out;
//
//	TYPE_data_in * data_in = (TYPE_data_in*)malloc(H*W*sizeof(TYPE_data_in));
//	TYPE_data_out * data_out = (TYPE_data_out*)malloc(H*W*sizeof(TYPE_data_out));
//
//
//	/* allocate memory for mat_in */
//	typedef DTYPE TYPE_mat_in;
//	TYPE_mat_in *mat_in = (TYPE_mat_in*) malloc (H*W*sizeof(TYPE_mat_in));
//
//	/* allocate memory for mat_out */
//	DTYPE *mat_out;
//	mat_out = (DTYPE*) malloc (H*W*sizeof(DTYPE));
//
//	// configure parameters
//	np1 = 0.00152334432;
//	np2 = 9.92565509172e-006;
//	thr = 0.4;
//	black = 64.0/1023.0;
//	f = 1.0;
//	m = 1.0;
//
//	/*Input processing***********************************************************************/
//	// Read file
//	sprintf(input_img, "./data_in.bin");
//	read_image(input_img, H, W, data_in);
//
//	/* Convert input matrix */
//	for (i=0; i < H; i++)
//	{
//		for (j=0; j < W; j++)
//		{
//			mat_in[i*W+j] = (DTYPE) data_in[i+j*H];
//		}
//	}
//	/****************************************************************************************/
//
//	// Your code is in here!
//	// call nlm
//	nlm((DTYPE (*)[W])mat_out, (DTYPE (*)[W])mat_in, np1, np2, thr, black, f, m);
//
//	/*Output processing**********************************************************************/
//	/* Convert output matrix */
//	for(i=0; i<H; i++)
//	{
//		for(j=0; j<W; j++)
//		{
//			data_out[i+j*H] = (double) mat_out[i*W+j];
//		}
//	}
//
//	// Write file
//	sprintf(output_img, "./data_out.bin");
//	write_image(output_img, H, W, data_out);
//	/****************************************************************************************/
//}
