#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void read_image(const char image[], int nrOfRows, int nrOfColumns, double *data){
	FILE *fp;
	fp = fopen(image , "rb" );
	if (fread(data, sizeof(double), nrOfRows*nrOfColumns, fp) != nrOfRows*nrOfColumns) {
		printf("error :: problem occurred when reading file %s\n", image);
	}
	fclose(fp);
}

void write_image(const char image[], int nrOfRows, int nrOfColumns, double *data){
	FILE *fp;
	fp = fopen(image , "wb" );
	if(fp == NULL)
	{
		puts("Cannot open file!");
		exit(1);
	}
	if (fwrite(data, sizeof(double), nrOfRows*nrOfColumns, fp) != nrOfRows*nrOfColumns) {
		printf("error :: problem occurred when writing file %s\n", image);
	}
	fclose(fp);
}
