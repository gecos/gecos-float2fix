/*******************************************************************************

  Copyright (c) 2018 Huawei Technologies Co., Ltd.

********************************************************************************
  $Author: m00360708 $
  $Date: 2018-03-22 14:43:56 +0100 (jeu., 22 mars 2018) $
*******************************************************************************/
//#include "globals.h"


#define DTYPE float
#define SIZE_T int
 #define W 128//320//3968
 #define H 128//240 //2976
#define W_IN W//37
#define H_IN H//99
#define W_OUT W//31
#define H_OUT H//93
#define HSIZE 7



// Matrix to convert RGB to YCbCr
const DTYPE coeff[3][3] = { { 0.299f, 0.587f, 0.114f}, {-0.168736f, -0.331264f, 0.5f}, {0.5f, -0.418688f, -0.081312f}};
// Matrix to convert back YCbCr to RGB
const DTYPE coefb[3][3] = {{1.0f, 0.0f, 1.402f}, {1.0f, -0.34414f, -0.71414f}, {1.0f, 1.772f, 0.0f}};

#ifndef absolute
    #define absolute(a) ((a) >= (0.0) ? (a) : -(a))
#endif



#pragma hls_design top
void unsharpf(
//#pragma EXPLORE_FIX W={4, 6, 8, 10} I={2}
#pragma EXPLORE_FIX W={8..16} I={4}
		DTYPE in[W_IN*H_IN*3],

#pragma EXPLORE_FIX W={8..16} I={4}
		DTYPE out[W_OUT*H_OUT*3],

#pragma EXPLORE_FIX W={8..16} I={4}
		DTYPE f[HSIZE],

#pragma EXPLORE_FIX W={8} I={2}
		DTYPE amount,

#pragma EXPLORE_FIX W={8} I={2}
		DTYPE thr)
{

#pragma EXPLORE_CONSTRAINT SAME = in
	DTYPE filt_tmp[7];
#pragma EXPLORE_CONSTRAINT SAME = in
	DTYPE tmp_c1;
#pragma EXPLORE_CONSTRAINT SAME = in
	DTYPE tmp_c2;
#pragma EXPLORE_CONSTRAINT SAME = in
	DTYPE tmp_c3;

#pragma EXPLORE_CONSTRAINT SAME=out
	DTYPE tmp;
	
//#pragma EXPLORE_FIX W={8..16} I={4}
	DTYPE mask;
//#pragma EXPLORE_FIX W={8..16} I={4}
	DTYPE y_channel[7][W_IN];
//#pragma EXPLORE_FIX W={8..16} I={4}
	DTYPE cb_channel[7][W_IN];
//#pragma EXPLORE_FIX W={8..16} I={4}
	DTYPE cr_channel[7][W_IN];

	//!! added to convert double to fixed-point: to avoid error: ambiguous overload for ‘operator*/+’
#pragma EXPLORE_FIX W={16} I={1}
	double CST_1 = 0.858823529;
#pragma EXPLORE_FIX W={16} I={1}
	double CST_2 = 0.062745098;
#pragma EXPLORE_FIX W={16} I={2}
	double CST_3 = 1.164383562;
#pragma EXPLORE_FIX W={16} I={1}
	double CST_4 = 0.073059361;

	//!! separate declarations from init to avoid moving declarations (which won't have same init value !!)
	DTYPE blur_h;
	DTYPE blur_v;
	DTYPE g;
	DTYPE l_full;


	SIZE_T i, j;
	SIZE_T k;


	//!! added
	$inject(in, $random_uniform(0, 1, 1));
	$inject(f, $random_uniform(0, 1, 2));
	$inject(amount, $from_var(1.2));
	$inject(thr, $from_var(0.01));
//	$inject(in, $from_file("<NEED TO BE AN OBSOLUTE PATH>/inputs/normalized/lena_128x128.input.norm"));


	#pragma omp parallel for
	for(i=HSIZE/2; i<H_OUT+HSIZE/2; i++)
	{
		for(j=0; j<W_IN; j++)
		{
			blur_h = 0.0;
			for(k=0; k<HSIZE; k++)
			{
				// Convert RGB to YCbCr
				if (i==HSIZE/2)
				{
					tmp_c1 = in[(k+i-HSIZE/2)*W_IN + j];
					tmp_c2 = in[(k+i-HSIZE/2)*W_IN + j +W_IN*H_IN];
					tmp_c3 = in[(k+i-HSIZE/2)*W_IN + j +2*W_IN*H_IN];
//!! changed because error: ambiguous overload for ‘operator*’ (operand types are ‘ac_fixed<65, 33, true, (ac_q_mode)0u, (ac_o_mode)0u>::rt<64, 32, true>::plus {aka ac_fixed<66, 34, true, (ac_q_mode)0u, (ac_o_mode)0u>}’ and ‘double’)
//					y_channel[(k+i-HSIZE/2)%HSIZE][j] = (tmp_c1*coeff[0][0] + tmp_c2*coeff[0][1] + tmp_c3*coeff[0][2]) * 0.858823529 + 0.062745098;
					y_channel[(k+i-HSIZE/2)%HSIZE][j] = (tmp_c1*coeff[0][0] + tmp_c2*coeff[0][1] + tmp_c3*coeff[0][2]) * CST_1 + CST_2;
					cb_channel[(k+i-HSIZE/2)%HSIZE][j] = tmp_c1*coeff[1][0] + tmp_c2*coeff[1][1] + tmp_c3*coeff[1][2];
					cr_channel[(k+i-HSIZE/2)%HSIZE][j] = tmp_c1*coeff[2][0] + tmp_c2*coeff[2][1] + tmp_c3*coeff[2][2];
				}
				if ((i>HSIZE/2) && (k==0))
				{
					tmp_c1 = in[(i+HSIZE/2)*W_IN + j];
					tmp_c2 = in[(i+HSIZE/2)*W_IN + j +W_IN*H_IN];
					tmp_c3 = in[(i+HSIZE/2)*W_IN + j +2*W_IN*H_IN];
//!! changed (idem)
//					y_channel[(i+HSIZE/2)%HSIZE][j] = (tmp_c1*coeff[0][0] + tmp_c2*coeff[0][1] + tmp_c3*coeff[0][2]) * 0.858823529 + 0.062745098;
					y_channel[(i+HSIZE/2)%HSIZE][j] = (tmp_c1*coeff[0][0] + tmp_c2*coeff[0][1] + tmp_c3*coeff[0][2]) * CST_1 + CST_2;
					cb_channel[(i+HSIZE/2)%HSIZE][j] = tmp_c1*coeff[1][0] + tmp_c2*coeff[1][1] + tmp_c3*coeff[1][2];
					cr_channel[(i+HSIZE/2)%HSIZE][j] = tmp_c1*coeff[2][0] + tmp_c2*coeff[2][1] + tmp_c3*coeff[2][2];
				}
				// Vertical gaussian filter, Horizontal direction
				blur_h += y_channel[(k+i-HSIZE/2)%HSIZE][j]*f[k];
			}

			filt_tmp[j%HSIZE] = blur_h;
			blur_v = 0.0;


			if(j>= 6)
			{
				// Horizontal gaussian filter, Vertical direction
				for(k = 0; k<HSIZE; k++)
				{
					blur_v += filt_tmp[(j+k-6)%HSIZE] * f[k];
				}


				// MASK function
				g = y_channel[i%HSIZE][j-3] - blur_v;

				if((g <= thr) && (g>=-thr)) {
					g = 0;
				}


				mask = y_channel[i%HSIZE][j-3] + amount*g;
//!! changed
//				mask = (mask < (0.0)) ? (0.0) : mask;
//				mask = (mask > (1.0)) ? (1.0) : mask;
				if(mask < (0.0)) mask = (0.0);
				if(mask > (1.0)) mask = (1.0);

				// Convert YCbCr to RGB
				l_full = (mask * CST_3 - CST_4);
				tmp = l_full*coefb[0][0] + cb_channel[i%HSIZE][j-3]*coefb[0][1] + cr_channel[i%HSIZE][j-3]*coefb[0][2];
//!! changed
//				tmp = (tmp < (0.0)) ? (0.0) : tmp;
//				tmp = (tmp > (1.0)) ? (1.0) : tmp;
				if (tmp < (0.0)) tmp = (0.0);
				if (tmp > (1.0)) tmp = (1.0);
				out[(i-3)*W_OUT + j-6] =  tmp;

				tmp = l_full*coefb[1][0] + cb_channel[i%HSIZE][j-3]*coefb[1][1] + cr_channel[i%HSIZE][j-3]*coefb[1][2];
//!! changed
//				tmp = (tmp < (0.0)) ? (0.0) : tmp;
//				tmp = (tmp > (1.0)) ? (1.0) : tmp;
				if (tmp < (0.0)) tmp = (0.0);
				if (tmp > (1.0)) tmp = (1.0);

				out[(i-3)*W_OUT + j-6 + W_OUT*H_OUT] = tmp;

				tmp = l_full*coefb[2][0] + cb_channel[i%HSIZE][j-3]*coefb[2][1] + cr_channel[i%HSIZE][j-3]*coefb[2][2];
//				tmp = (tmp < (0.0)) ? (0.0) : tmp;
//				tmp = (tmp > (1.0)) ? (1.0) : tmp;
				if (tmp < (0.0)) tmp = (0.0);
				if (tmp > (1.0)) tmp = (1.0);
				out[(i-3)*W_OUT + j-6 + 2*W_OUT*H_OUT] = tmp;
			}

		}
	}

	$save(out);
}
