debug(2);

sourceDir = "src-c/";

p = CreateGecosProject("isp_fixed");
AddSourceToGecosProject(p, sourceDir + "isp_core.c");
AddSourceToGecosProject(p, sourceDir + "utils.c");
AddIncDirToGecosProject(p, sourceDir);

#TypesExploration("."); #regenearate default.properties
TypesExploration(p, "default.properties", "isp_fixed.costdsl");
#TypesExploration(p, "default.properties");

