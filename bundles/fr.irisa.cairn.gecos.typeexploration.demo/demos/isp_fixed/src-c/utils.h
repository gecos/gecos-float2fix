#ifndef UTILS_H
#define UTILS_H

void read_image(const char image[], int nrOfRows, int nrOfColumns, double *data);
void write_image(const char image[], int nrOfRows, int nrOfColumns, double *data);

#endif//UTILS_H

