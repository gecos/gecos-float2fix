#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "utils.h"

#define __GECOS_TYPE_EXPL__

// #define __NLM_WLO__
#define __UNSHARP_WLO__

// Parameters
//#define H  2976
//#define W  3968
#define H  240
#define W  320
#define H3 720
#define IMAGE_SIZE H*W
#define K 5
#define N 13
#define K2 2
#define N2 6
#define CROP 4
#define KK 25

//size_h_pad = H+2*N2;
#define SIZE_H_PAD  (H+2*N2)  //2988
//size_w_pad = W+2*N2;
#define SIZE_W_PAD  (W+2*N2)  //3980

#define S 2

//size_h_ref = H + N2 - K2
#define SIZE_H_REF (H + N2 - K2)  //2980
//size_w_ref = W + N2 - K2
#define SIZE_W_REF (W + N2 - K2)  //3972

#define ROW_FILT 3
#define COL_FILT 3

#define PATCH_SIZE 24
#define HSIZE 7

#define SIZE_H_FIL_3 (H+2)
#define SIZE_W_FIL_3 (W+2)

#define SIZE_H_FIL_5 (H+4)
#define SIZE_W_FIL_5 (W+4)

// Macro functions
#ifndef max
#define max(a,b) ((a) > (b) ? (a) : (b))
#endif

#ifndef min
#define min(a,b) ((a) < (b) ? (a) : (b))
#endif

#ifndef sqr
#define sqr(a) ((a)*(a))
#endif

// Bayer types
enum bayer_type{rggb, bggr, grbg, gbrg};
enum padding_option{replicate, zeros};

// Parameter
#define DTYPE float
#define DTYPE_DOUBLE double
#define SIZE_T int

// nlm's constants
const int dx_patch[24] = {-4,-2,0,2,4,-4,-2,0,2,4,-4,-2,2,4,-4,-2,0,2,4,-4,-2,0,2,4};
const int dy_patch[24] = {-4,-4,-4,-4,-4,-2,-2,-2,-2,-2,0,0,0,0,2,2,2,2,2,4,4,4,4,4};


// wb's constants
const DTYPE wb_multipliers[4] = {1.259157509, 1.0, 2.666666667, 1.0};

// cc's constants
const DTYPE cam2rgb[9] = {1.952148438,-0.837890625,-0.114257813,-0.387695313,1.728515625,-0.340820313,-0.083984375,-1.018554688,2.102539062};
#define PIXEL_NUM ((H-8)*(W-8)/4)


void my_imfilter_5(DTYPE mat_out[H][W], DTYPE mat_in[H][W], DTYPE in_pad[SIZE_H_FIL_5][SIZE_W_FIL_5], const DTYPE* filter, padding_option mode)
{
	SIZE_T i, j, p, q;

	SIZE_T r2 = 2;
	SIZE_T c2 = 2;
	SIZE_T row = 5;
	SIZE_T col = 5;

	for(i=-r2; i<H+r2; i++)
	{
		for(j=-c2; j<W+c2; j++)
		{
			if (mode == replicate)
			{
				/* replicate padding for input image */
				in_pad[(i+r2)][j+c2] = mat_in[min(max(i,0),H-1)][min(max(j,0),W-1)];
			}
			else
			{
				/* zero padding for input image */
				if(i>=0 && i<H && j>=0 && j<W)
					in_pad[(i+r2)][j+c2] = mat_in[i][j];
				else
					in_pad[(i+r2)][j+c2] = 0.0;
			}
		}
	}

	DTYPE tmp;
	// Do filtering
	for(i=r2; i<H+r2; i++)
	{
		for(j=c2; j<W+c2; j++)
		{
			tmp = 0.0;
			for(p=0; p<row; p++)
			{
				for(q=0; q<col; q++)
				{
					tmp = tmp + filter[p*col+q]*in_pad[((i-r2)+p)][(j-c2)+q];
				}
			}
			mat_out[(i-r2)][(j-c2)] = tmp;
		}
	}

}


void my_imfilter_3_nlm(DTYPE filt_matrix_nlm[H][W], DTYPE mat_in_nlm[H][W], DTYPE in_pad_nlm[SIZE_H_FIL_3][SIZE_W_FIL_3], const DTYPE* filter_nlm, padding_option mode)
{
	SIZE_T i, j, p, q;

	SIZE_T r2 = 1;
	SIZE_T c2 = 1;
	SIZE_T row = 3;
	SIZE_T col = 3;

	for(i=-r2; i<H+r2; i++)
	{
		for(j=-c2; j<W+c2; j++)
		{
			if (mode == replicate)
			{
				/* replicate padding for input image */
				in_pad_nlm[(i+r2)][j+c2] = mat_in_nlm[min(max(i,0),H-1)][min(max(j,0),W-1)];
			}
			else
			{
				/* zero padding for input image */
				if(i>=0 && i<H && j>=0 && j<W)
					in_pad_nlm[(i+r2)][j+c2] = mat_in_nlm[i][j];
				else
					in_pad_nlm[(i+r2)][j+c2] = 0.0;
			}
		}
	}
#ifdef __NLM_WLO__
#pragma EXPLORE_CONSTRAINT SAME = mat_in_nlm
#endif	
	DTYPE tmp_fil_nlm;
	// Do filtering
	for(i=r2; i<H+r2; i++)
	{
		for(j=c2; j<W+c2; j++)
		{
			tmp_fil_nlm = 0.0;
			for(p=0; p<row; p++)
			{
				for(q=0; q<col; q++)
				{
					tmp_fil_nlm = tmp_fil_nlm + filter_nlm[p*col+q]*in_pad_nlm[((i-r2)+p)][(j-c2)+q];
				}
			}
			filt_matrix_nlm[(i-r2)][(j-c2)] = tmp_fil_nlm;
		}
	}

}

void my_imfilter_3(DTYPE mat_out[H][W], DTYPE mat_in[H][W], DTYPE in_pad[SIZE_H_FIL_3][SIZE_W_FIL_3], const DTYPE* filter, padding_option mode)
{
	SIZE_T i, j, p, q;

	SIZE_T r2 = 1;
	SIZE_T c2 = 1;
	SIZE_T row = 3;
	SIZE_T col = 3;

	for(i=-r2; i<H+r2; i++)
	{
		for(j=-c2; j<W+c2; j++)
		{
			if (mode == replicate)
			{
				/* replicate padding for input image */
				in_pad[(i+r2)][j+c2] = mat_in[min(max(i,0),H-1)][min(max(j,0),W-1)];
			}
			else
			{
				/* zero padding for input image */
				if(i>=0 && i<H && j>=0 && j<W)
					in_pad[(i+r2)][j+c2] = mat_in[i][j];
				else
					in_pad[(i+r2)][j+c2] = 0.0;
			}
		}
	}


	DTYPE tmp;
	// Do filtering
	for(i=r2; i<H+r2; i++)
	{
		for(j=c2; j<W+c2; j++)
		{
			tmp = 0.0;
			for(p=0; p<row; p++)
			{
				for(q=0; q<col; q++)
				{
					tmp = tmp + filter[p*col+q]*in_pad[((i-r2)+p)][(j-c2)+q];
				}
			}
			mat_out[(i-r2)][(j-c2)] = tmp;
		}
	}

}

void my_imfilter_3_1(DTYPE mat_out[H][W], DTYPE mat_in[H][W], DTYPE in_pad[SIZE_H_FIL_3][SIZE_W_FIL_3], const DTYPE* filter, padding_option mode)
{
	SIZE_T i, j, p, q;

	SIZE_T r2 = 1;
	SIZE_T c2 = 1;
	SIZE_T row = 3;
	SIZE_T col = 3;

	for(i=-r2; i<H+r2; i++)
	{
		for(j=-c2; j<W+c2; j++)
		{
			if (mode == replicate)
			{
				/* replicate padding for input image */
				in_pad[(i+r2)][j+c2] = mat_in[min(max(i,0),H-1)][min(max(j,0),W-1)];
			}
			else
			{
				/* zero padding for input image */
				if(i>=0 && i<H && j>=0 && j<W)
					in_pad[(i+r2)][j+c2] = mat_in[i][j];
				else
					in_pad[(i+r2)][j+c2] = 0.0;
			}
		}
	}

	DTYPE tmp;
	// Do filtering
	for(i=r2; i<H+r2; i++)
	{
		for(j=c2; j<W+c2; j++)
		{
			tmp = 0.0;
			for(p=0; p<row; p++)
			{
				for(q=0; q<col; q++)
				{
					tmp = tmp + filter[p*col+q]*in_pad[((i-r2)+p)][(j-c2)+q];
				}
			}
			mat_out[(i-r2)][(j-c2)] = tmp;
		}
	}

}

void my_imfilter_two_input_5(DTYPE mat_out[H][W], DTYPE mat_in_1[H][W], DTYPE mat_in_2[H][W], DTYPE in_pad[SIZE_H_FIL_5][SIZE_W_FIL_5], const DTYPE* filter)
{
	SIZE_T i, j, p, q;

	SIZE_T r2 = 2;
	SIZE_T c2 = 2;
	SIZE_T row = 5;
	SIZE_T col = 5;

	for(i=-r2; i<H+r2; i++)
	{
		for(j=-c2; j<W+c2; j++)
		{
			if(i>=0 && i<H && j>=0 && j<W)
				in_pad[(i+r2)][j+c2] = mat_in_1[i][j] + mat_in_2[i][j];
			else
				in_pad[(i+r2)][j+c2] = 0.0;
		}
	}

	DTYPE tmp;
	// Do filtering
	for(i=r2; i<H+r2; i++)
	{
		for(j=c2; j<W+c2; j++)
		{
			tmp = 0.0;
			for(p=0; p<row; p++)
			{
				for(q=0; q<col; q++)
				{
					tmp = tmp + filter[p*col+q]*in_pad[((i-r2)+p)][(j-c2)+q];
				}
			}
			mat_out[(i-r2)][(j-c2)] = tmp;
		}
	}
}



void do_loop(
	DTYPE mat_out_nlm[H][W],
	DTYPE filt_matrix_nlm[H][W],
	DTYPE mat_in_pad[SIZE_H_PAD][SIZE_W_PAD],
	DTYPE_DOUBLE mat_sqdiff[SIZE_H_REF][SIZE_W_REF],
	DTYPE mat_acc_weight[H][W],
	DTYPE np1,
	DTYPE np2,
	DTYPE thr
)
{
	SIZE_T i, j, k;
#ifdef __NLM_WLO__
#pragma EXPLORE_FIX W={7..16} I={5}
	DTYPE_DOUBLE tmp_dist;
#pragma EXPLORE_FIX W={7..16} I={5}
	DTYPE pre_exp;
#pragma EXPLORE_FIX W={7..16} I={5}
	DTYPE denoised;
#pragma EXPLORE_FIX W={7..16} I={5}
	DTYPE nv;

#pragma EXPLORE_FIX W={7..16} I={5}
	DTYPE_DOUBLE sqdiff_current;
#pragma EXPLORE_FIX W={7..16} I={5}
	DTYPE_DOUBLE sqdiff_save;
#pragma EXPLORE_FIX W={7..16} I={5}
// #pragma EXPLORE_CONSTRAINT SAME = sqdiff_current
	DTYPE_DOUBLE sqdiff_prev;
#pragma EXPLORE_FIX W={7..16} I={5}
	DTYPE weight;
#else
	DTYPE_DOUBLE tmp_dist;
	DTYPE pre_exp;
	DTYPE denoised;
	DTYPE nv;

	DTYPE_DOUBLE sqdiff_current;
	DTYPE_DOUBLE sqdiff_save;
	DTYPE_DOUBLE sqdiff_prev;
	DTYPE weight;
#endif

	// Initialization
	for(i=0;i<H;i++)
	{
		for(j=0;j<W;j++)
		{
			mat_out_nlm[i][j] = 0;
			mat_acc_weight[i][j] = 0;
		}
	}

	for (int k = 0; k < PATCH_SIZE; k++)
	{
		tmp_dist = 0;
		sqdiff_prev = 0;
		sqdiff_save = 0;
		for (int i = CROP; i < SIZE_H_REF + CROP; i++)
		{
			for (int j = CROP; j < SIZE_W_REF + CROP; j++)
			{
				sqdiff_current = (DTYPE_DOUBLE)((mat_in_pad[i][j] - mat_in_pad[i+dy_patch[k]][j+dx_patch[k]])*(mat_in_pad[i][j] - mat_in_pad[i+dy_patch[k]][j+dx_patch[k]]));

				if (i==CROP && j==CROP) {
					sqdiff_save = sqdiff_current;
					sqdiff_prev = sqdiff_current;
				} else if (i==CROP && j> CROP) {
					sqdiff_save = sqdiff_current + sqdiff_prev;
					sqdiff_prev = sqdiff_current + sqdiff_prev;
				} else if(i> CROP && j==CROP) {
					sqdiff_save = sqdiff_current + mat_sqdiff[i-CROP-1][j-CROP];
					sqdiff_prev = sqdiff_current;
				} else if(i> CROP && j> CROP) {
					sqdiff_save = sqdiff_current + sqdiff_prev + mat_sqdiff[i-CROP-1][j-CROP];
					sqdiff_prev = sqdiff_current + sqdiff_prev;
				}
				mat_sqdiff[i-CROP][j-CROP] = sqdiff_save;

				/// Calculate mat_denoised and mat_acc_weight
				if((i>=CROP+K-1) && (j>=CROP+K-1))
				{
					if(i==CROP+K-1 && j==CROP+K-1) {
						tmp_dist = mat_sqdiff[i-CROP][j-CROP];
					} else if(i> CROP+K-1 && j==CROP+K-1) {
						tmp_dist = mat_sqdiff[i-CROP][j-CROP] - mat_sqdiff[i-K-CROP][j-CROP];
					} else if(i==CROP+K-1 && j> CROP+K-1) {
						tmp_dist = mat_sqdiff[i-CROP][j-CROP] - mat_sqdiff[i-CROP][j-K-CROP];
					} else if(i> CROP+K-1 && j> CROP+K-1) {
						tmp_dist = mat_sqdiff[i-CROP][j-CROP] - mat_sqdiff[i-K-CROP][j-CROP] - mat_sqdiff[i-CROP][j-K-CROP] + mat_sqdiff[i-K-CROP][j-K-CROP];
					}

					nv = np1*filt_matrix_nlm[i-CROP-K+1][j-CROP-K+1] + np2;


#ifndef __GECOS_TYPE_EXPL__
					// !! changed
					// pre_exp = -max((tmp_dist)/(K*K) - 2*nv, 0);
					pre_exp = -max((double)(tmp_dist)/KK - (double)(2*nv), 0);
					weight = pow(2.7182817, pre_exp/(thr*thr*nv));
#else
					pre_exp = tmp_dist - 2*nv*KK;
					if (pre_exp <= 0)
						weight = 1;
					else
						weight = 0;
#endif


					// Compute and accumulate denoised pixels
					denoised = weight * mat_in_pad[i-K2+dy_patch[k]][j-K2+dx_patch[k]];
					mat_out_nlm[i-CROP-K+1][j-CROP-K+1] = mat_out_nlm[i-CROP-K+1][j-CROP-K+1] + denoised;

					// Update accumlated weights
					mat_acc_weight[i-CROP-K+1][j-CROP-K+1] = mat_acc_weight[i-CROP-K+1][j-CROP-K+1] + weight;
				}

			}
		}
	}


	for(i=0; i<H; i++)
	{
		for(j=0; j<W; j++)
		{
			mat_out_nlm[i][j] = (mat_out_nlm[i][j] + mat_in_pad[i+N2][j+N2])/(mat_acc_weight[i][j] + 1);
		}
	}
}

void nlm(
#ifdef __NLM_WLO__
#pragma EXPLORE_FIX W={7..16} I={5}
		DTYPE mat_out[H][W],
#pragma EXPLORE_FIX W={7..16} I={5}
		DTYPE mat_in_nlm[H][W],
#pragma EXPLORE_FIX W={7..16} I={5}
		DTYPE filt_matrix_nlm[H][W],
#pragma EXPLORE_FIX W={7..16} I={5}
		DTYPE mat_in_pad[SIZE_H_PAD][SIZE_W_PAD],
#pragma EXPLORE_FIX W={7..16} I={5}
		DTYPE in_pad_nlm[SIZE_H_FIL_3][SIZE_W_FIL_3],
#pragma EXPLORE_FIX W={7..16} I={5}
		DTYPE_DOUBLE mat_sqdiff[SIZE_H_REF][SIZE_W_REF],
#pragma EXPLORE_FIX W={7..16} I={5}
		DTYPE mat_acc_weight[H][W],
#pragma EXPLORE_FIX W={7..16} I={5}
		DTYPE np1,
#pragma EXPLORE_FIX W={7..16} I={5}
		DTYPE np2,
#pragma EXPLORE_FIX W={7..16} I={5}
		DTYPE thr,
#pragma EXPLORE_FIX W={7..16} I={5}
		DTYPE black
#else
		DTYPE mat_out[H][W],
		DTYPE mat_in_nlm[H][W],
		DTYPE filt_matrix_nlm[H][W],
		DTYPE mat_in_pad[SIZE_H_PAD][SIZE_W_PAD],
		DTYPE in_pad_nlm[SIZE_H_FIL_3][SIZE_W_FIL_3],
		DTYPE_DOUBLE mat_sqdiff[SIZE_H_REF][SIZE_W_REF],
		DTYPE mat_acc_weight[H][W],
		DTYPE np1,
		DTYPE np2,
		DTYPE thr,
		DTYPE black
#endif
		)
{

#ifdef __NLM_WLO__
#pragma EXPLORE_FIX W={7..16} I={5}
	const DTYPE filter_nlm[9] = {0.0625, 0.1250, 0.0625, 0.1250, 0.2500, 0.1250, 0.0625, 0.1250, 0.0625};
#else
	const DTYPE filter_nlm[9] = {0.0625, 0.1250, 0.0625, 0.1250, 0.2500, 0.1250, 0.0625, 0.1250, 0.0625};
#endif

	SIZE_T i, j;	/* Counter variables */

	/* replicate padding for input image */
	for(i=-N2; i<H+N2; i++)
	{
		for(j=-N2; j<W+N2; j++)
		{
			mat_in_pad[i+N2][j+N2] = mat_in_nlm[min(max(i,0),H-1)][min(max(j,0),W-1)];
		}
	}

	padding_option option = replicate;

	my_imfilter_3_nlm(filt_matrix_nlm, mat_in_nlm, in_pad_nlm, filter_nlm, option);

	for(i=0; i<H; i++)
	{
		for(j=0; j<W; j++)
		{
			filt_matrix_nlm[i][j] = max((double)(filt_matrix_nlm[i][j] - black), 0.0);
		}
	}


	do_loop(mat_out, filt_matrix_nlm, mat_in_pad, mat_sqdiff, mat_acc_weight, np1, np2, thr);

}

void linearize(DTYPE in[H][W], DTYPE out[H][W], DTYPE black)
{
	SIZE_T i, j;

	double cnt = 1.066736184;

	for(i=0; i<H; i++)
	{
		for(j=0; j<W; j++)
		{
#ifdef __GECOS_TYPE_EXPL__
			out[i][j] = (in[i][j] - black)*cnt;
#else
			out[i][j] = (in[i][j] - black)/(1.0 - black);
#endif

			if(out[i][j] < 0.0) out[i][j] = 0.0;
			if(out[i][j] > 1.0) out[i][j] = 1.0; // bypass Vignetting Correction
		}
	}
}

void wb(DTYPE in[H][W], DTYPE out[H][W], bayer_type type)
{
	SIZE_T i, j;

	// SIZE_T len_mat = sizeof(wb_multipliers)/sizeof(wb_multipliers[0]);
	// if(len_mat == 3) // only one G multiplier supplied
	// {
	// 	wb_multipliers = (MULT_MAT_WB*)realloc(wb_multipliers, sizeof(MULT_MAT_WB)*4);
	// 	wb_multipliers[3] = wb_multipliers[1];
	// }

	// for(i=0; i<H; i++)
	// {
	// 	for(j=0; j<W; j++)
	// 	{
	// 		out[i][j] = wb_multipliers[1];
	// 	}
	// }

	for (i=0; i<H; i++)
	{
		for(j=0; j<W; j++)
		{
			if((i%2 == 0) && (j%2 == 0))
			{
				if (type == rggb) out[i][j] = wb_multipliers[0];
				else if (type == bggr) out[i][j] = wb_multipliers[2];
				else if (type == grbg) out[i][j] = wb_multipliers[1];
				else out[i][j] = wb_multipliers[3];
			}
			else if ((i%2 == 0) && (j%2 == 1))
			{
				if (type == rggb) out[i][j] = wb_multipliers[1];
				else if (type == bggr) out[i][j] = wb_multipliers[3];
				else if (type == grbg) out[i][j] = wb_multipliers[2];
				else out[i][j] = wb_multipliers[2];
			}
			else if ((i%2 == 1) && (j%2 == 0))
			{
				if (type == rggb) out[i][j] = wb_multipliers[3];
				else if (type == bggr) out[i][j] = wb_multipliers[1];
				else if (type == grbg) out[i][j] = wb_multipliers[1];
				else out[i][j] = wb_multipliers[0];
			}
			else
			{
				if (type == rggb) out[i][j] = wb_multipliers[2];
				else if (type == bggr) out[i][j] = wb_multipliers[0];
				else if (type == grbg) out[i][j] = wb_multipliers[3];
				else out[i][j] = wb_multipliers[1];
			}

			out[i][j] = out[i][j] * in[i][j];
			if(out[i][j] > 1.0) out[i][j] = 1.0;
		}
	}

	// free(wb_multipliers);

}

void demmhc(
		DTYPE in[H][W],
		DTYPE out[3*H*W],
		bayer_type type,
		DTYPE r_dc[H][W],
		DTYPE g_dc[H][W],
		DTYPE b_dc[H][W],
		DTYPE gh[H][W],
		DTYPE gv[H][W],
		DTYPE tmp_dc_0[H][W],
		DTYPE tmp_dc_1[H][W],
		DTYPE tmp_dc_2[H][W],
		DTYPE tmp_dc_3[H][W],
		DTYPE tmp_dc_4[H][W],
		DTYPE in_pad_gh[SIZE_H_FIL_5][SIZE_W_FIL_5],
		DTYPE in_pad_gv[SIZE_H_FIL_5][SIZE_W_FIL_5],
		DTYPE in_pad_tmp_dc_0[SIZE_H_FIL_3][SIZE_W_FIL_3],
		DTYPE in_pad_tmp_dc_1[SIZE_H_FIL_5][SIZE_W_FIL_5],
		DTYPE in_pad_tmp_dc_2[SIZE_H_FIL_5][SIZE_W_FIL_5],
		DTYPE in_pad_tmp_dc_3[SIZE_H_FIL_3][SIZE_W_FIL_3],
		DTYPE in_pad_tmp_dc_4[SIZE_H_FIL_3][SIZE_W_FIL_3]
)
{

	DTYPE f_g_0[25] = {0.0, 0.0, 0.0625, 0.0, 0.0, 0.0, -0.125, 0.0, -0.125, 0.0, -0.125, 0.5, 0.625, 0.5, -0.125, 0.0, -0.125, 0.0, -0.125, 0.0, 0.0, 0.0, 0.0625, 0.0, 0.0};
	DTYPE f_g_1[25] = {0.0, 0.0,-0.125, 0.0, 0.0, 0.0,-0.125, 0.5, -0.125, 0.0, 0.0625, 0.0, 0.625, 0.0, 0.0625, 0.0, -0.125, 0.5, -0.125, 0.0, 0.0, 0.0, -0.125, 0.0, 0.0};
	DTYPE f_g_2[9] = {0.0, 0.25, 0.0, 0.25, 0.0, 0.25, 0.0, 0.25, 0.0};
	DTYPE f_g_3[25] = {0.0, 0.0, -0.125, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.125, 0.0, 0.5, 0.0, -0.125, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.125, 0.0, 0.0};
	DTYPE f_rb_0[25] = {0.0, 0.0, -0.1875, 0.0, 0.0, 0.0, 0.25, 0.0, 0.25, 0.0, -0.1875, 0.0, 0.75, 0.0, -0.1875, 0.0, 0.25, 0.0, 0.25, 0.0, 0.0, 0.0, -0.1875, 0.0,0.0};
	DTYPE f_rb_1[9] = {0.0 ,0.5, 0.0, 0.5, 0.0, 0.5, 0.0, 0.5, 0.0};


	SIZE_T i,j;
	padding_option option = zeros;

	// if(type == rggb)
	// {
	// 	int r[4] = {1, 0, 0, 0};
	// 	int gr[4] = {0, 1, 0, 0};
	// 	int gb[4] = {0, 0, 1, 0};
	// 	int b[4] = {0, 0, 0, 1};
	// }
	// else if(type == bggr)
	// {
	SIZE_T r[4] = {0, 0, 0, 1};
	SIZE_T gr[4] = {0, 0, 1, 0};
	SIZE_T gb[4] = {0, 1, 0, 0};
	SIZE_T b[4] = {1, 0, 0, 0};
	// }
	// else if(type == gbrg)
	// {
	// 	int r[4] = {0, 0, 1, 0};
	// 	int gr[4] = {1, 0, 0, 0};
	// 	int gb[4] = {0, 0, 0, 1};
	// 	int b[4] = {0, 1, 0, 0};
	// }
	// else
	// {
	// 	int r[4] = {0, 1, 0, 0};
	// 	int gr[4] = {0, 0, 0, 1};
	// 	int gb[4] = {1, 0, 0, 0};
	// 	int b[4] = {0, 0, 1, 0};
	// }



	for(i=0; i<H; i++)
	{
		for(j=0; j<W; j++)
		{
			if((i%2 == 0) && (j%2 == 0))
			{
				if (r[0] == 1) r_dc[i][j] = in[i][j];
				else r_dc[i][j] = 0.0;

				if((gr[0] | gb[0]) == 1) g_dc[i][j] = in[i][j];
				else g_dc[i][j] = 0.0;

				if(b[0] == 1) b_dc[i][j] = in[i][j];
				else b_dc[i][j] = 0.0;
			}
			else if ((i%2 == 0) && (j%2 == 1))
			{
				if (r[1] == 1) r_dc[i][j] = in[i][j];
				else r_dc[i][j] = 0.0;

				if((gr[1] | gb[1]) == 1) g_dc[i][j] = in[i][j];
				else g_dc[i][j] = 0.0;

				if(b[1] == 1) b_dc[i][j] = in[i][j];
				else b_dc[i][j] = 0.0;
			}
			else if ((i%2 == 1) && (j%2 == 0))
			{
				if (r[2] == 1) r_dc[i][j] = in[i][j];
				else r_dc[i][j] = 0.0;

				if((gr[2] | gb[2]) == 1) g_dc[i][j] = in[i][j];
				else g_dc[i][j] = 0.0;

				if(b[2] == 1) b_dc[i][j] = in[i][j];
				else b_dc[i][j] = 0.0;
			}
			else
			{
				if (r[3] == 1) r_dc[i][j] = in[i][j];
				else r_dc[i][j] = 0.0;

				if((gr[3] | gb[3]) == 1) g_dc[i][j] = in[i][j];
				else g_dc[i][j] = 0.0;

				if(b[3] == 1) b_dc[i][j] = in[i][j];
				else b_dc[i][j] = 0.0;
			}
		}
	}


	my_imfilter_5(gh, g_dc, in_pad_gh, f_g_0, option);
	my_imfilter_5(gv, g_dc, in_pad_gv, f_g_1, option);
	my_imfilter_3(tmp_dc_0, g_dc, in_pad_tmp_dc_0, f_g_2, option);
	my_imfilter_two_input_5(tmp_dc_1, r_dc, b_dc, in_pad_tmp_dc_1, f_g_3);

	DTYPE tmp0, tmp1;

	for (i=0; i<H; i++)
	{
		for (j=0; j<W; j++)
		{
			tmp0 = g_dc[i][j] + tmp_dc_0[i][j] + tmp_dc_1[i][j];
			if (tmp0 < 0.0) tmp0 = 0.0;
			if (tmp0 > 1.0) tmp0 = 1.0;
			out[i*W+j + IMAGE_SIZE] = tmp0;
			//			out_g[i][j] = tmp0;
		}
	}

	my_imfilter_two_input_5(tmp_dc_2, r_dc, b_dc, in_pad_tmp_dc_2, f_rb_0);
	my_imfilter_3_1(tmp_dc_3, b_dc, in_pad_tmp_dc_3, f_rb_1, option);



	for (i=0; i<H; i++)
	{
		for (j=0; j<W; j++)
		{
			if((i%2 == 0) && (j%2 == 0))
			{
				tmp1 = tmp_dc_2[i][j]*r[0] + gh[i][j]*gb[0] + gv[i][j]*gr[0];
			}
			else if ((i%2 == 0) && (j%2 == 1))
			{
				tmp1 = tmp_dc_2[i][j]*r[1] + gh[i][j]*gb[1] + gv[i][j]*gr[1];
			}
			else if ((i%2 == 1) && (j%2 == 0))
			{
				tmp1 = tmp_dc_2[i][j]*r[2] + gh[i][j]*gb[2] + gv[i][j]*gr[2];
			}
			else
			{
				tmp1 = tmp_dc_2[i][j]*r[3] + gh[i][j]*gb[3] + gv[i][j]*gr[3];
			}
			tmp0 = b_dc[i][j] + tmp_dc_3[i][j] + tmp1;
			if (tmp0 < 0.0) tmp0 = 0.0;
			if (tmp0 > 1.0) tmp0 = 1.0;
			out[i*W+j + 2*IMAGE_SIZE] = tmp0;
			//			out_b[i][j] = tmp0;
		}
	}

	my_imfilter_3_1(tmp_dc_4, r_dc, in_pad_tmp_dc_4, f_rb_1, option);
	for (i=0; i<H; i++)
	{
		for (j=0; j<W; j++)
		{
			if((i%2 == 0) && (j%2 == 0))
			{
				tmp1 = tmp_dc_2[i][j]*b[0] + gh[i][j]*gr[0] + gv[i][j]*gb[0];
			}
			else if ((i%2 == 0) && (j%2 == 1))
			{
				tmp1 = tmp_dc_2[i][j]*b[1] + gh[i][j]*gr[1] + gv[i][j]*gb[1];
			}
			else if ((i%2 == 1) && (j%2 == 0))
			{
				tmp1 = tmp_dc_2[i][j]*b[2] + gh[i][j]*gr[2] + gv[i][j]*gb[2];
			}
			else
			{
				tmp1 = tmp_dc_2[i][j]*b[3] + gh[i][j]*gr[3] + gv[i][j]*gb[3];
			}
			tmp0 = r_dc[i][j] + tmp_dc_4[i][j] + tmp1;
			if (tmp0 < 0.0) tmp0 = 0.0;
			if (tmp0 > 1.0) tmp0 = 1.0;
			out[i*W+j] = tmp0;
			//			out_r[i][j] = tmp0;
		}
	}


}

void color_correction(DTYPE in[3*IMAGE_SIZE], DTYPE out[3*IMAGE_SIZE])
{
	SIZE_T i, j, k;

	DTYPE tmp;

	for(i=0; i<H; i++)
	{
		for(j=0; j<W; j++)
		{
			tmp = in[i*W+j]*cam2rgb[0] + in[i*W+j + IMAGE_SIZE]*cam2rgb[1] + in[i*W+j + 2*IMAGE_SIZE]*cam2rgb[2];
			if (tmp < (0.0)) tmp = (0.0);
			if (tmp > (1.0)) tmp = (1.0);
			out[i*W+j] = tmp;

			tmp = in[i*W+j]*cam2rgb[3] + in[i*W+j + IMAGE_SIZE]*cam2rgb[4] + in[i*W+j + 2*IMAGE_SIZE]*cam2rgb[5];
			if (tmp < (0.0)) tmp = (0.0);
			if (tmp > (1.0)) tmp = (1.0);
			out[i*W+j + IMAGE_SIZE] = tmp;

			tmp = in[i*W+j]*cam2rgb[6] + in[i*W+j + IMAGE_SIZE]*cam2rgb[7] + in[i*W+j + 2*IMAGE_SIZE]*cam2rgb[8];
			if (tmp < (0.0)) tmp = (0.0);
			if (tmp > (1.0)) tmp = (1.0);
			out[i*W+j + 2*IMAGE_SIZE] = tmp;
		}
	}

}

void gamma_bright_correction(DTYPE in[3*IMAGE_SIZE], DTYPE out[3*IMAGE_SIZE])
{
	DTYPE_DOUBLE tmp, tmp1, tmp2, tmp3;
	DTYPE grayscale;
	SIZE_T i, j, k;

	double CNT_GB_0 = 0.2989;
	double CNT_GB_1 = 0.5870;
	double CNT_GB_2 = 0.1140;
	double CNT_GB_3 = 0.045;
	double CNT_GB_4 = 1.055;
	double CNT_GB_5 = 0.055;
	double CNT_GB_6 = 1.0/2.4;
	double CNT_GB_7 = 0.0031308;
	double CNT_GB_8 = 12.92;
	double CNT_GB_9 = 0.18;
	// RGB to Gray
	tmp = 0;
	for(i=4; i<H-4; i=i+2)
	{
		for(j=4; j<W-4; j=j+2)
		{
			tmp += (in[i*W+j]*CNT_GB_0 + in[i*W+j + IMAGE_SIZE]*CNT_GB_1 + in[i*W+j + 2*IMAGE_SIZE]*CNT_GB_2)/PIXEL_NUM;
		}
	}

	//	grayscale = 0.18*(H-8)*(W-8)/(4*tmp);
	//	grayscale = CNT_GB_3*(H-8)*(W-8)/tmp;
	grayscale = CNT_GB_9/tmp;

	tmp1 = 0;
	for(k=0; k<3; k++)
	{
		for(i=0; i<H; i++)
		{
			for(j=0; j<W; j++)
			{
				// Brightness correction
				tmp1 = grayscale*in[i*W+j+k*W*H];
				if (tmp1 > 1) tmp1 = 1;

				// Gamma correction
				tmp3 = pow(tmp1, CNT_GB_6);
				tmp2 = CNT_GB_4*tmp3 - CNT_GB_5;
				if(tmp1<=CNT_GB_7) tmp2 = CNT_GB_8*tmp1;
				out[i*W+j+k*W*H] = tmp2;
				//				out[i*W+j+k*W*H] = in[i*W+j+k*W*H];

			}
		}
	}

}

void unsharpf(
#ifdef __UNSHARP_WLO__
#pragma EXPLORE_FIX W={7..16} I={5}
	DTYPE in_unsharpf[W*H*3],
#pragma EXPLORE_FIX W={7..16} I={5}
	DTYPE out_unsharpf[W*H*3],
#pragma EXPLORE_FIX W={7..16} I={5}
	DTYPE f_unsharpf[HSIZE],
#pragma EXPLORE_FIX W={7..16} I={5}
	DTYPE amount_unsharpf,
#pragma EXPLORE_FIX W={7..16} I={5}
	DTYPE thr_unsharpf
#else
	DTYPE in_unsharpf[W*H*3],
	DTYPE out_unsharpf[W*H*3],
	DTYPE f_unsharpf[HSIZE],
	DTYPE amount_unsharpf,
	DTYPE thr_unsharpf
#endif
	)
{

	SIZE_T i, j;
	SIZE_T x, y;
	SIZE_T k;

// unsharp's constants
#ifdef __UNSHARP_WLO__
#pragma EXPLORE_FIX W={7..16} I={5}
const DTYPE coeff[3][3] = { { 0.299f, 0.587f, 0.114f}, {-0.168736f, -0.331264f, 0.5f}, {0.5f, -0.418688f, -0.081312f}};
#pragma EXPLORE_FIX W={7..16} I={5}
const DTYPE coefb[3][3] = {{1.0f, 0.0f, 1.402f}, {1.0f, -0.34414f, -0.71414f}, {1.0f, 1.772f, 0.0f}};
#pragma EXPLORE_FIX W={7..16} I={5}
const double CST_1 = 0.858823529;
#pragma EXPLORE_FIX W={7..16} I={5}
const double CST_2 = 0.062745098;
#pragma EXPLORE_FIX W={7..16} I={5}
const double CST_3 = 1.164383562;
#pragma EXPLORE_FIX W={7..16} I={5}
const double CST_4 = 0.073059361;

#pragma EXPLORE_FIX W={7..16} I={5}
// #pragma EXPLORE_CONSTRAINT SAME= blur_v
	DTYPE filt_tmp[7];

#pragma EXPLORE_CONSTRAINT SAME = in_unsharpf
	DTYPE tmp_in_1;
#pragma EXPLORE_CONSTRAINT SAME = in_unsharpf
	DTYPE tmp_in_2;
#pragma EXPLORE_CONSTRAINT SAME= in_unsharpf
	DTYPE tmp_in_3;
#pragma EXPLORE_CONSTRAINT SAME= out_unsharpf
	DTYPE tmp_out_1;
#pragma EXPLORE_CONSTRAINT SAME= out_unsharpf
	DTYPE tmp_out_2;
#pragma EXPLORE_CONSTRAINT SAME= out_unsharpf
	DTYPE tmp_out_3;
#pragma EXPLORE_FIX W={7..16} I={5}
	DTYPE mask;
#pragma EXPLORE_FIX W={7..16} I={5}
	DTYPE blur_v;
#pragma EXPLORE_FIX W={7..16} I={5}
	DTYPE blur_h;
#pragma EXPLORE_FIX W={7..16} I={5}
	DTYPE g;
#pragma EXPLORE_FIX W={7..16} I={5}
	DTYPE l_full;

#else
	const DTYPE coeff[3][3] = { { 0.299f, 0.587f, 0.114f}, {-0.168736f, -0.331264f, 0.5f}, {0.5f, -0.418688f, -0.081312f}};
	const DTYPE coefb[3][3] = {{1.0f, 0.0f, 1.402f}, {1.0f, -0.34414f, -0.71414f}, {1.0f, 1.772f, 0.0f}};
	const double CST_1 = 0.858823529;
	const double CST_2 = 0.062745098;
	const double CST_3 = 1.164383562;
	const double CST_4 = 0.073059361;

	DTYPE filt_tmp[7];
	DTYPE tmp_in_1, tmp_in_2, tmp_in_3;
	DTYPE tmp_out_1, tmp_out_2, tmp_out_3;
	DTYPE mask;
	DTYPE blur_v;
	DTYPE blur_h;
	DTYPE g;
	DTYPE l_full;
#endif

	for(i = 0; i<W*(H+3); i++)
	{
		y = i/(H+3);
		x = i%(H+3);
		blur_v = 0.0;
		// Vertical blur pass and Convert RGB to YCbCr
		if(x<H) // from 0 to H-1
		{
			if((y>0) && (y<W-3))
			{
				tmp_in_1 = in_unsharpf[(y+3)*H+x];
				tmp_in_2 = in_unsharpf[(y+3)*H+x+W*H];
				tmp_in_3 = in_unsharpf[(y+3)*H+x+2*W*H];

				in_unsharpf[(y+3)*H+x] = (tmp_in_1*coeff[0][0] + tmp_in_2*coeff[0][1] + tmp_in_3*coeff[0][2]) * CST_1 + CST_2;
				in_unsharpf[(y+3)*H+x+W*H] = tmp_in_1*coeff[1][0] + tmp_in_2*coeff[1][1] + tmp_in_3*coeff[1][2];
				in_unsharpf[(y+3)*H+x+2*W*H] = tmp_in_1*coeff[2][0] + tmp_in_2*coeff[2][1] + tmp_in_3*coeff[2][2];
			}
			for(k = 0; k<HSIZE; k++)
			{
				j = y + k;
				// j = x + k;
				j = j <= HSIZE/2 ? 0 : j-HSIZE/2;
				j = j >= W ? W-1 : j;

				// Convert RGB to YCbCr
				if((y==0) && ((k==0) || (k>3)))
				{
					tmp_in_1 = in_unsharpf[j*H+x];
					tmp_in_2 = in_unsharpf[j*H+x+W*H];
					tmp_in_3 = in_unsharpf[j*H+x+2*W*H];

					in_unsharpf[j*H+x] = (tmp_in_1*coeff[0][0] + tmp_in_2*coeff[0][1] + tmp_in_3*coeff[0][2]) * CST_1 + CST_2;
					in_unsharpf[j*H+x+W*H] = tmp_in_1*coeff[1][0] + tmp_in_2*coeff[1][1] + tmp_in_3*coeff[1][2];
					in_unsharpf[j*H+x+2*W*H] = tmp_in_1*coeff[2][0] + tmp_in_2*coeff[2][1] + tmp_in_3*coeff[2][2];
				}
				blur_v += in_unsharpf[j*H+x]*f_unsharpf[k];
				// blur_v += in_unsharpf[y*H+j]*f_unsharpf[k];
			}
			filt_tmp[x%7] = blur_v;
		}

		blur_h = 0.0;
		// Horizontal blur pass and Convert YCbCr to RGB
		if(x>2) // from 3 to H+2
		{
			// Horizontal blur pass
			for(k = 0; k<HSIZE; k++)
			{
				j = x + k - 3; // because we are calculating for the index i-3
				j = j <= HSIZE/2 ? 0 : j-HSIZE/2;
				j = j >= H ? H-1 : j;
				blur_h += filt_tmp[j%7]*f_unsharpf[k];
			}

			// MASK function
			g = in_unsharpf[i-3*(y+1)] - blur_h;

			if((g <= thr_unsharpf) && (g>=-thr_unsharpf)) {
				g = 0;
			}
			mask = in_unsharpf[i-3*(y+1)] + amount_unsharpf*g;
			if(mask < (0.0)) mask = (0.0);
			if(mask > (1.0)) mask = (1.0);

			// Convert YCbCr to RGB
			l_full = (mask * CST_3 - CST_4);
			tmp_out_1 = l_full*coefb[0][0] + in_unsharpf[i-3*(y+1)+W*H]*coefb[0][1] + in_unsharpf[i-3*(y+1)+2*W*H]*coefb[0][2];
			if (tmp_out_1 < (0.0)) tmp_out_1 = (0.0);
			if (tmp_out_1 > (1.0)) tmp_out_1 = (1.0);
			out_unsharpf[i-3*(y+1)] =  tmp_out_1;

			tmp_out_2 = l_full*coefb[1][0] + in_unsharpf[i-3*(y+1)+W*H]*coefb[1][1] + in_unsharpf[i-3*(y+1)+2*W*H]*coefb[1][2];
			if (tmp_out_2 < (0.0)) tmp_out_2 = (0.0);
			if (tmp_out_2 > (1.0)) tmp_out_2 = (1.0);
			out_unsharpf[(i-3*(y+1))+W*H] = tmp_out_2;

			tmp_out_3 = l_full*coefb[2][0] + in_unsharpf[i-3*(y+1)+W*H]*coefb[2][1] + in_unsharpf[i-3*(y+1)+2*W*H]*coefb[2][2];
			if (tmp_out_3 < (0.0)) tmp_out_3 = (0.0);
			if (tmp_out_3 > (1.0)) tmp_out_3 = (1.0);
			out_unsharpf[(i-3*(y+1))+2*W*H] = tmp_out_3;

		}
	}
}



#pragma MAIN_FUNC
void isp_core(
		DTYPE in_nlm[H][W],
		DTYPE out_nlm[H][W],
		DTYPE filt_matrix[H][W],
		DTYPE mat_in_pad[SIZE_H_PAD][SIZE_W_PAD],
		DTYPE in_pad_nlm[SIZE_H_FIL_3][SIZE_W_FIL_3],
		DTYPE_DOUBLE mat_sqdiff[SIZE_H_REF][SIZE_W_REF],
		DTYPE mat_acc_weight[H][W],
		DTYPE out_linear[H][W],
		DTYPE out_wb[H][W],
		DTYPE r_dc[H][W],
		DTYPE g_dc[H][W],
		DTYPE b_dc[H][W],
		DTYPE out_dc[3*H*W],
		DTYPE gh[H][W],
		DTYPE gv[H][W],
		DTYPE tmp_dc_0[H][W],
		DTYPE tmp_dc_1[H][W],
		DTYPE tmp_dc_2[H][W],
		DTYPE tmp_dc_3[H][W],
		DTYPE tmp_dc_4[H][W],
		DTYPE in_pad_gh[SIZE_H_FIL_5][SIZE_W_FIL_5],
		DTYPE in_pad_gv[SIZE_H_FIL_5][SIZE_W_FIL_5],
		DTYPE in_pad_tmp_dc_0[SIZE_H_FIL_3][SIZE_W_FIL_3],
		DTYPE in_pad_tmp_dc_1[SIZE_H_FIL_5][SIZE_W_FIL_5],
		DTYPE in_pad_tmp_dc_2[SIZE_H_FIL_5][SIZE_W_FIL_5],
		DTYPE in_pad_tmp_dc_3[SIZE_H_FIL_3][SIZE_W_FIL_3],
		DTYPE in_pad_tmp_dc_4[SIZE_H_FIL_3][SIZE_W_FIL_3],
		DTYPE out_cc[3*H*W],
		DTYPE out_gbc[3*H*W],
		DTYPE tmp_abc[3*H*W],
		DTYPE out_unsharp[3*H*W],
		DTYPE out_unsharp_r[H][W],
		DTYPE out_unsharp_g[H][W],
		DTYPE out_unsharp_b[H][W],

		DTYPE matrix_shape[H3][W]
)
{
	// Variables declarations
	int i,j,k; // counter variables

	// nlm
	DTYPE np1;
	DTYPE np2;
	DTYPE thr_nlm;
	DTYPE black1;
	DTYPE black2;

	bayer_type type = bggr;

	// Unsharp
	DTYPE amount;
	DTYPE thr_unsharp;
	DTYPE radius;

	//	DTYPE out_unsharp[3*H*W];


#ifdef __GECOS_TYPE_EXPL__
	DTYPE filter[HSIZE];

	$inject(in_nlm, $random_uniform(0, 1, 3));
	$inject(filter, $random_uniform(0, 1, 2));
	$inject(np1, $from_var(0.00152334432));
	$inject(np2, $from_var(9.92565509172e-006));
	$inject(thr_nlm, $from_var(0.4));
	$inject(black1, $from_var(64.0/1023.0));
	$inject(black2, $from_var(64.0/1023.0));
	$inject(thr_unsharp, $from_var(0.01));
	$inject(amount, $from_var(1.2));
	$inject(radius, $from_var(1.5));
#else
	char input_img[32];
	char output_img[32];

	double *data_in;
	data_in = (double*)malloc(H*W*sizeof(double));


	np1 = 0.00152334432;
	np2 = 9.92565509172e-006;
	thr_nlm = 0.4;
	black1 = 64.0/1023.0;
	black2 = 64.0/1023.0;

	thr_unsharp = 0.01;
	amount = 1.2;
	radius = 1.5;
	// Define the Gaussian blur filter
	SIZE_T hsize = 2*ceil(2*radius)+1;
	hsize = hsize < 3 ? 3 : hsize;
	DTYPE *filter;
	filter = (DTYPE*)malloc(hsize*sizeof(DTYPE));
	DTYPE norm = 0;
	for (SIZE_T i=0; i<hsize; ++i) {
		double k = (double)i-hsize/2;
		DTYPE tmp = exp(-(k*k)/(2*radius*radius));
		norm += tmp;
		filter[i] = tmp;
	}
	for (SIZE_T i=0; i<hsize; ++i) {
		filter[i] = filter[i]/norm;
	}

	sprintf(input_img, "./input_nlm_acc.bin");
	read_image(input_img, H, W, data_in);

	for (i=0; i < H; i++)
	{
		for (j=0; j < W; j++)
		{
			in_nlm[i][j] = data_in[i+j*H];
		}
	}
#endif

	nlm(out_nlm, in_nlm, filt_matrix, mat_in_pad, in_pad_nlm, mat_sqdiff, mat_acc_weight, np1, np2, thr_nlm, black1);
	linearize(out_nlm, out_linear, black2);
	wb(out_linear, out_wb, type);
	demmhc(out_wb, out_dc, type, r_dc, g_dc, b_dc, gh, gv, tmp_dc_0, tmp_dc_1, tmp_dc_2, tmp_dc_3, tmp_dc_4, in_pad_gh, in_pad_gv, in_pad_tmp_dc_0, in_pad_tmp_dc_1,in_pad_tmp_dc_2, in_pad_tmp_dc_3, in_pad_tmp_dc_4);
	color_correction(out_dc, out_cc);
	gamma_bright_correction(out_cc, out_gbc);

	for(k=0; k<3; k++)
	{
		for (i=0; i < H; i++)
		{
			for (j=0; j < W; j++)
			{
				tmp_abc[i+j*H + k*H*W] = out_gbc[i*W+j + k*H*W];
			}
		}
	}

	unsharpf(tmp_abc, out_unsharp, filter, amount, thr_unsharp);


	for (i=0; i < H; i++)
	{
		for (j=0; j < W; j++)
		{
			out_unsharp_r[i][j] = out_unsharp[i*W+j];
			out_unsharp_g[i][j] = out_unsharp[i*W+j + H*W];
			out_unsharp_b[i][j] = out_unsharp[i*W+j + 2*H*W];
		}
	}

	for(k=0; k<3; k++)
	{
		for(i=0; i<H; i++)
		{
			for(j=0; j<W; j++)
			{
				matrix_shape[k*H+i][j] = out_unsharp[i+j*H + k*H*W];
			}
		}
	}


#ifdef __GECOS_TYPE_EXPL__
	$save(matrix_shape, H3, W);
#endif

#ifndef __GECOS_TYPE_EXPL__
	double *data_out;
	data_out = (double*)malloc(3*H*W*sizeof(double));

	for (k=0; k<3; k++)
		for (i=0; i<H; i++)
			for (j=0; j<W; j++)
				data_out[i+j*H + k*H*W] = (double)out_unsharp[i+j*H + k*H*W];
	// data_out[i+j*H + k*H*W] = (double)out_gbc[i*W+j + k*H*W];

	sprintf(output_img, "./data_out.bin");
	write_image(output_img, 3*H, W, data_out);
#endif

#ifndef __GECOS_TYPE_EXPL__
	free(data_in);
	free(data_out);
	free(filter);
#endif
}

#ifndef __GECOS_TYPE_EXPL__
int main(int argc, char * * argv) {
	DTYPE (* in_nlm)[W] = (DTYPE (*)[W])malloc(sizeof (DTYPE)*H*W);
	DTYPE (* out_nlm)[W] = (DTYPE (*)[W])malloc(sizeof (DTYPE)*H*W);

	DTYPE (* filt_matrix)[W] = (DTYPE (*)[W])malloc(sizeof (DTYPE)*H*W);
	DTYPE (* mat_in_pad)[SIZE_W_PAD] = (DTYPE (*)[SIZE_W_PAD])malloc(sizeof (DTYPE)*SIZE_H_PAD*SIZE_W_PAD);
	DTYPE (* in_pad_nlm)[SIZE_W_FIL_3] = (DTYPE (*)[SIZE_W_FIL_3])malloc(SIZE_H_FIL_3*SIZE_W_FIL_3*sizeof(DTYPE));
	DTYPE_DOUBLE (* mat_sqdiff)[SIZE_W_REF] = (DTYPE_DOUBLE (*)[SIZE_W_REF])malloc(sizeof (DTYPE_DOUBLE)*SIZE_H_REF*SIZE_W_REF);
	DTYPE (* mat_acc_weight)[W] = (DTYPE (*)[W])malloc(sizeof (DTYPE)*H*W);

	DTYPE (* out_linear)[W] = (DTYPE (*)[W])malloc(sizeof (DTYPE)*H*W);
	DTYPE (* out_wb)[W] = (DTYPE (*)[W])malloc(sizeof (DTYPE)*H*W);
	DTYPE (* r_dc)[W] = (DTYPE (*)[W])malloc(sizeof (DTYPE)*H*W);
	DTYPE (* g_dc)[W] = (DTYPE (*)[W])malloc(sizeof (DTYPE)*H*W);
	DTYPE (* b_dc)[W] = (DTYPE (*)[W])malloc(sizeof (DTYPE)*H*W);
	DTYPE * out_dc = (DTYPE *)malloc(sizeof (DTYPE) * (3*H*W));
	DTYPE (* gh)[W] = (DTYPE (*)[W])malloc(sizeof (DTYPE)*H*W);
	DTYPE (* gv)[W] = (DTYPE (*)[W])malloc(sizeof (DTYPE)*H*W);
	DTYPE (* tmp_dc_0)[W] = (DTYPE (*)[W])malloc(sizeof (DTYPE)*H*W);
	DTYPE (* tmp_dc_1)[W] = (DTYPE (*)[W])malloc(sizeof (DTYPE)*H*W);
	DTYPE (* tmp_dc_2)[W] = (DTYPE (*)[W])malloc(sizeof (DTYPE)*H*W);
	DTYPE (* tmp_dc_3)[W] = (DTYPE (*)[W])malloc(sizeof (DTYPE)*H*W);
	DTYPE (* tmp_dc_4)[W] = (DTYPE (*)[W])malloc(sizeof (DTYPE)*H*W);
	DTYPE (* in_pad_gh)[W + 4] = (DTYPE (*)[W + 4])malloc(sizeof (DTYPE) * (H + 4) * (W + 4));
	DTYPE (* in_pad_gv)[W + 4] = (DTYPE (*)[W + 4])malloc(sizeof (DTYPE) * (H + 4) * (W + 4));
	DTYPE (* in_pad_tmp_dc_0)[W + 2] = (DTYPE (*)[W + 2])malloc(sizeof (DTYPE) * (H + 2) * (W + 2));
	DTYPE (* in_pad_tmp_dc_1)[W + 4] = (DTYPE (*)[W + 4])malloc(sizeof (DTYPE) * (H + 4) * (W + 4));
	DTYPE (* in_pad_tmp_dc_2)[W + 4] = (DTYPE (*)[W + 4])malloc(sizeof (DTYPE) * (H + 4) * (W + 4));
	DTYPE (* in_pad_tmp_dc_3)[W + 2] = (DTYPE (*)[W + 2])malloc(sizeof (DTYPE) * (H + 2) * (W + 2));
	DTYPE (* in_pad_tmp_dc_4)[W + 2] = (DTYPE (*)[W + 2])malloc(sizeof (DTYPE) * (H + 2) * (W + 2));
	DTYPE * out_cc = (DTYPE *)malloc(sizeof (DTYPE) * (3*H*W));
	DTYPE * out_gbc = (DTYPE *)malloc(sizeof (DTYPE) * (3*H*W));
	DTYPE * tmp_abc = (DTYPE *)malloc(sizeof (DTYPE) * (3*H*W));
	DTYPE * out_unsharp = (DTYPE *)malloc(sizeof (DTYPE) * (3*H*W));
	DTYPE (* out_unsharp_r)[W] = (DTYPE (*)[W])malloc(sizeof (DTYPE)*H*W);
	DTYPE (* out_unsharp_g)[W] = (DTYPE (*)[W])malloc(sizeof (DTYPE)*H*W);
	DTYPE (* out_unsharp_b)[W] = (DTYPE (*)[W])malloc(sizeof (DTYPE)*H*W);

	DTYPE (* matrix_shape)[W] = (DTYPE (*)[W])malloc(sizeof(DTYPE) *3*H*W);

	isp_core(in_nlm, out_nlm, filt_matrix, mat_in_pad, in_pad_nlm, mat_sqdiff, mat_acc_weight, out_linear, out_wb, r_dc, g_dc, b_dc, out_dc, gh, gv, tmp_dc_0, tmp_dc_1, tmp_dc_2, tmp_dc_3, tmp_dc_4, in_pad_gh, in_pad_gv, in_pad_tmp_dc_0, in_pad_tmp_dc_1, in_pad_tmp_dc_2, in_pad_tmp_dc_3, in_pad_tmp_dc_4, out_cc, out_gbc, tmp_abc, out_unsharp, out_unsharp_r, out_unsharp_g, out_unsharp_b, matrix_shape);
	// isp_core(in_nlm, out_nlm, filt_matrix, mat_in_pad, in_pad_nlm, mat_sqdiff, mat_acc_weight, out_unsharp_r, out_unsharp_g, out_unsharp_b);



	free(in_nlm);
	free(out_nlm);
	free(filt_matrix);
	free(mat_in_pad);
	free(in_pad_nlm);
	free(mat_sqdiff);
	free(mat_acc_weight);
	free(out_linear);
	free(out_wb);
	free(r_dc);
	free(g_dc);
	free(b_dc);
	free(out_dc);
	free(gh);
	free(gv);
	free(tmp_dc_0);
	free(tmp_dc_1);
	free(tmp_dc_2);
	free(tmp_dc_3);
	free(tmp_dc_4);
	free(in_pad_gh);
	free(in_pad_gv);
	free(in_pad_tmp_dc_0);
	free(in_pad_tmp_dc_1);
	free(in_pad_tmp_dc_2);
	free(in_pad_tmp_dc_3);
	free(in_pad_tmp_dc_4);
	free(out_cc);
	free(out_gbc);
	free(tmp_abc);
	free(out_unsharp);
	free(out_unsharp_r);
	free(out_unsharp_g);
	free(out_unsharp_b);

	free(matrix_shape);
	return 0;
}

#endif


