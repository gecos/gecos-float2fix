package typeexploration.computation.concrete;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ConcreteComputationBlock {
	private Map<ConcreteOperation, Long> opMap = new HashMap<ConcreteOperation, Long>();
	
	public void addOperation(ConcreteOperation op, long numOps) {
		if (opMap.containsKey(op)){
			Long current = opMap.get(op);
			opMap.put(op, current + numOps);
		} else {
			opMap.put(op, numOps);
		}
	}
	
	public void addOperations(ConcreteComputationBlock block) {
		for (Entry<ConcreteOperation, Long> entry : block.getOperations().entrySet()) {
			addOperation(entry.getKey(), entry.getValue());
		}
	}
	
	public Map<ConcreteOperation, Long> getOperations() {
		return Collections.unmodifiableMap(opMap);
	}
	
	// Show information of block
	public void showInfo() {
		for (Entry<ConcreteOperation, Long> entry : opMap.entrySet()) {
			System.out.println(entry.getKey().toString() + " numOps: " + entry.getValue().toString());
		}
		
	}

}
