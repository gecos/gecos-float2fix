package typeexploration.computation.concrete;

import java.util.Map.Entry;

import typeexploration.computation.HWTargetDesign;

public class ConcreteComputationModelEvaluator {

	protected HWModelParameters data;
	
	public ConcreteComputationModelEvaluator(HWModelParameters data) {
		this.data = data;
	}

	// Power Computation
	public double energyEstimation(ConcreteComputationBlock block, HWTargetDesign spec) {
		double pTotal = 0; // total power of the block in W
		double p; // power in W

		for (Entry<ConcreteOperation, Long> entry : block.getOperations().entrySet()) {
			ConcreteOperation tmpOps = entry.getKey();
			Long numOps = entry.getValue();
			p = data.getData(tmpOps, spec.getFrequency()).getPower();
			pTotal += numOps*p;
		}
		return pTotal;
	}

	// Area Computation
	public double areaEstimation(ConcreteComputationBlock block, HWTargetDesign spec) {
		double aTotal = 0; // total area of the block in um^2
		double a; // area in um^2
		double delay; // delay in ns
		
		for (Entry<ConcreteOperation, Long> entry : block.getOperations().entrySet()) {
			ConcreteOperation tmpOps = entry.getKey();
			Long numOps = entry.getValue();
			OperatorSpec opSpec = data.getData(tmpOps, spec.getFrequency());
			a = opSpec.getArea();
			delay = opSpec.getDelay();
			aTotal += a * Math.ceil(numOps*delay/spec.getTotalLatency());
		}
		
		return aTotal;
	}
}
