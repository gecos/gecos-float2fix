package typeexploration.computation.concrete;

public class OperatorSpec {
	private final double area;
	private final double power;
	private final double delay;
	private final double qem; // Quantization error
	
	public OperatorSpec(double area, double power, double delay, double qem) {
		super();
		this.area = area;
		this.power = power;
		this.delay = delay;
		this.qem = qem;
	}
	
	public double getqError() {
		return qem;
	}

	public double getArea() {
		return area;
	}

	public double getPower() {
		return power;
	}

	public double getDelay() {
		return delay;
	}
	
	@Override
	public String toString() {
		return " Quantization Error: " + qem + " Area: " + area + " Power: " + power + " Delay: " + delay;
	}

	public static OperatorSpec takeMean(OperatorSpec val1, OperatorSpec val2) {
		final double area  = (val1.area  + val2.area )*0.5;
		final double power = (val1.power + val2.power)*0.5;
		final double delay = (val1.delay + val2.delay)*0.5;
		final double qem   = (val1.qem   + val2.qem  )*0.5;
		
		return new OperatorSpec(area, power, delay, qem);
	}
}
