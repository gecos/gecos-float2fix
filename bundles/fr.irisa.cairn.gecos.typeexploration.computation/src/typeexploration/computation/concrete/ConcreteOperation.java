package typeexploration.computation.concrete;

import typeexploration.computation.HWOpType;

public class ConcreteOperation {

	private final HWOpType type;
	private final int BWin1;
	private final int BWin2;
	private final int BWout;

	
	public ConcreteOperation(HWOpType type, int BWin1, int BWin2, int BWout) {
		this.type = type;
		
		//Force max WL due to limited data
		if (BWin1 > 32) { System.err.println("Input1 of concrete operation reduced from " + BWin1 + " to 32 due to library limitations."); BWin1 = 32; }
		if (BWin2 > 32) { System.err.println("Input2 of concrete operation reduced from " + BWin2 + " to 32 due to library limitations."); BWin2 = 32; }
		if (BWout > 32) { System.err.println("Output of concrete operation reduced from " + BWout + " to 32 due to library limitations."); BWout = 32; }
			
		//Force output consistency
		{
			int BWtmp = BWout;
			if (type == HWOpType.MUL) {
				// BWout = Math.max(Math.max(BWin1, BWin2), BWout);
				BWtmp = Math.min(Math.max(Math.max(BWin1, BWin2), BWout), BWin1+BWin2);
	
			} else if (type == HWOpType.ADD) {
				BWtmp = Math.min(BWout, Math.max(BWin1, BWin2));
			}
			if (BWtmp != BWout) {
				System.err.println("Output of concrete operation reduced from " + BWout + " to " + BWtmp + " for consistency reasons.");
			}
			BWout = BWtmp;
		}
		
		if (BWin2 > BWin1) {
			int tmp = BWin1;
			BWin1 = BWin2;
			BWin2 = tmp;
		}
		
		this.BWin1 = BWin1;
		this.BWin2 = BWin2;
		this.BWout = BWout;
	}
	

	public HWOpType getType() {
		return type;
	}

	public int getInput1BW() {
		return BWin1;
	}

	public int getInput2BW() {
		return BWin2;
	}


	public int getOutputBW() {
		return BWout;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + BWin1;
		result = prime * result + BWin2;
		result = prime * result + BWout;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConcreteOperation other = (ConcreteOperation) obj;
		if (BWin1 != other.BWin1)
			return false;
		if (BWin2 != other.BWin2)
			return false;
		if (BWout != other.BWout)
			return false;
		if (type != other.type)
			return false;
		return true;
	}


	@Override
	public String toString() {
		return " type: " + type + " BW: " + BWin1 + " op " + BWin2 + " => " + BWout;
	}
}
