package typeexploration.computation.concrete;

import typeexploration.computation.HWOpType;

public class OperatorConfiguration extends ConcreteOperation{

	public OperatorConfiguration(HWOpType type, int BWin1, int BWin2, int BWout) {
		super(type, BWin1, BWin2, BWout);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

	@Override
	public String toString() {
		return " type: " + super.getType() +" BW: " + super.getInput1BW() + " op" + super.getInput2BW() + " => " + super.getOutputBW();
	}
	
}
