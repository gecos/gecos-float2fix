package typeexploration.computation.concrete;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import typeexploration.computation.HWOpType;

public class HWModelParameters {
	
	public static boolean DEBUG = true;
	
	private static final Path RESOURCES_LOCATION = Paths.get(
		HWModelParameters.class.getProtectionDomain().getCodeSource().getLocation().getPath()).resolve("resources");

	private Map<OperatorConfiguration, OperatorSpec> data = new HashMap<OperatorConfiguration, OperatorSpec>();

	public HWModelParameters() {
		Path filePath = RESOURCES_LOCATION.resolve("data_fixed_v2.csv");
		File file = filePath.toFile();
		
		try {
			Scanner scanner = new Scanner(file);
			scanner.useDelimiter(",");
			OperatorConfiguration opTmp;
			OperatorSpec opSpecTmp;
			
			// Skip the first line
			scanner.nextLine();
			
			while(scanner.hasNextLine()) {
				String tmp = scanner.nextLine();
				String[] tmpSplit = tmp.split(",");
				if (tmpSplit.length!=8) continue;
				
				HWOpType type = HWOpType.valueOf(tmpSplit[0]);
				//OPE	IN1	IN2	OUT	AREA	POWER	DELAY(ns)	EQM
				int BWin1 = Integer.parseInt(tmpSplit[1]);
				int BWin2 = Integer.parseInt(tmpSplit[2]);
				int BWout = Integer.parseInt(tmpSplit[3]);
				opTmp = new OperatorConfiguration(type, BWin1, BWin2, BWout);
				double area = Double.parseDouble(tmpSplit[4]);
				double power = Double.parseDouble(tmpSplit[5]);
				double delay = Double.parseDouble(tmpSplit[6]);
				double eqm = Double.parseDouble(tmpSplit[7]);
				
				opSpecTmp = new OperatorSpec(area, power, delay, eqm);
				this.data.put(opTmp, opSpecTmp);
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private void checkOpConf(OperatorConfiguration conf) {
		if (!data.containsKey(conf)) {
			throw new RuntimeException("HW parameter for operation does not exist: " + conf);
		}
	}
	
	public OperatorSpec getData(ConcreteOperation op, int freq) {
		
		if (op.getInput1BW() < 2 || op.getInput1BW() > 32 || op.getInput2BW() < 2 || op.getInput2BW() > 32 || op.getOutputBW() < 2 || op.getOutputBW() > 32) {
			throw new RuntimeException("Cost model only supports wordlengths in [2,32].");
		}
		
		boolean in1even = op.getInput1BW() % 2 == 0;
		boolean in2even = op.getInput2BW() % 2 == 0;
		boolean outeven = op.getOutputBW() % 2 == 0;
		
		final OperatorSpec value;
		
		//If all values are even, use data from CSV directly
		if (in1even && in2even && outeven) {
			OperatorConfiguration operatorTmp = new OperatorConfiguration(op.getType(), op.getInput1BW(), op.getInput2BW(), op.getOutputBW());
			
			checkOpConf(operatorTmp);
			
			value = this.data.get(operatorTmp);
		
		//else need to interpolate
		} else {
			int[] in1range = in1even?new int[] {op.getInput1BW(),op.getInput1BW()}:new int[] {op.getInput1BW()-1,op.getInput1BW()+1};
			int[] in2range = in2even?new int[] {op.getInput2BW(),op.getInput2BW()}:new int[] {op.getInput2BW()-1,op.getInput2BW()+1};
			int[] outrange = outeven?new int[] {op.getOutputBW(),op.getOutputBW()}:new int[] {op.getOutputBW()-1,op.getOutputBW()+1};
			
			if (DEBUG) {
				System.err.println(String.format("Interpolating OpConfig: (%d,%d,%d) -> (%d,%d,%d) (%d,%d,%d)", 
						op.getInput1BW(), op.getInput2BW(), op.getOutputBW(),
						in1range[0], in2range[0], outrange[0], in1range[1], in2range[1], outrange[1]));
			}
			
			OperatorConfiguration opConf1 = new OperatorConfiguration(op.getType(), in1range[0], in2range[0], outrange[0]);
			OperatorConfiguration opConf2 = new OperatorConfiguration(op.getType(), in1range[1], in2range[1], outrange[1]);
			
			checkOpConf(opConf1);
			checkOpConf(opConf2);

			OperatorSpec val1 = data.get(opConf1);
			OperatorSpec val2 = data.get(opConf2);

			value = OperatorSpec.takeMean(val1,  val2);
		}
		
		return value;
	}
		
}
