/**
 */
package typeexploration.computation;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Max Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.computation.MaxExpression#getExprs <em>Exprs</em>}</li>
 * </ul>
 *
 * @see typeexploration.computation.ComputationPackage#getMaxExpression()
 * @model
 * @generated
 */
public interface MaxExpression extends OperandExpression {
	/**
	 * Returns the value of the '<em><b>Exprs</b></em>' containment reference list.
	 * The list contents are of type {@link typeexploration.computation.OperandExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exprs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exprs</em>' containment reference list.
	 * @see typeexploration.computation.ComputationPackage#getMaxExpression_Exprs()
	 * @model containment="true"
	 * @generated
	 */
	EList<OperandExpression> getExprs();

} // MaxExpression
