/**
 */
package typeexploration.computation;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>HW Target Design</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.computation.HWTargetDesign#getFrequency <em>Frequency</em>}</li>
 *   <li>{@link typeexploration.computation.HWTargetDesign#getOutputsPerCycle <em>Outputs Per Cycle</em>}</li>
 *   <li>{@link typeexploration.computation.HWTargetDesign#getProblemSizeExpr <em>Problem Size Expr</em>}</li>
 *   <li>{@link typeexploration.computation.HWTargetDesign#getTotalLatency <em>Total Latency</em>}</li>
 * </ul>
 *
 * @see typeexploration.computation.ComputationPackage#getHWTargetDesign()
 * @model
 * @generated
 */
public interface HWTargetDesign extends EObject {
	/**
	 * Returns the value of the '<em><b>Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Frequency</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Frequency</em>' attribute.
	 * @see #setFrequency(int)
	 * @see typeexploration.computation.ComputationPackage#getHWTargetDesign_Frequency()
	 * @model unique="false"
	 * @generated
	 */
	int getFrequency();

	/**
	 * Sets the value of the '{@link typeexploration.computation.HWTargetDesign#getFrequency <em>Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Frequency</em>' attribute.
	 * @see #getFrequency()
	 * @generated
	 */
	void setFrequency(int value);

	/**
	 * Returns the value of the '<em><b>Outputs Per Cycle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outputs Per Cycle</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outputs Per Cycle</em>' attribute.
	 * @see #setOutputsPerCycle(int)
	 * @see typeexploration.computation.ComputationPackage#getHWTargetDesign_OutputsPerCycle()
	 * @model unique="false"
	 * @generated
	 */
	int getOutputsPerCycle();

	/**
	 * Sets the value of the '{@link typeexploration.computation.HWTargetDesign#getOutputsPerCycle <em>Outputs Per Cycle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Outputs Per Cycle</em>' attribute.
	 * @see #getOutputsPerCycle()
	 * @generated
	 */
	void setOutputsPerCycle(int value);

	/**
	 * Returns the value of the '<em><b>Problem Size Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Problem Size Expr</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Problem Size Expr</em>' containment reference.
	 * @see #setProblemSizeExpr(SizeExpression)
	 * @see typeexploration.computation.ComputationPackage#getHWTargetDesign_ProblemSizeExpr()
	 * @model containment="true"
	 * @generated
	 */
	SizeExpression getProblemSizeExpr();

	/**
	 * Sets the value of the '{@link typeexploration.computation.HWTargetDesign#getProblemSizeExpr <em>Problem Size Expr</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Problem Size Expr</em>' containment reference.
	 * @see #getProblemSizeExpr()
	 * @generated
	 */
	void setProblemSizeExpr(SizeExpression value);

	/**
	 * Returns the value of the '<em><b>Total Latency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Total Latency</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Total Latency</em>' attribute.
	 * @see typeexploration.computation.ComputationPackage#getHWTargetDesign_TotalLatency()
	 * @model unique="false" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='double _xblockexpression = (double) 0;\n{\n\tdouble _doubleValue = &lt;%java.lang.Long%&gt;.valueOf(this.getProblemSizeExpr().evaluate()).doubleValue();\n\tdouble _doubleValue_1 = &lt;%java.lang.Integer%&gt;.valueOf(this.getOutputsPerCycle()).doubleValue();\n\tfinal double numCycles = (_doubleValue / _doubleValue_1);\n\tint _frequency = this.getFrequency();\n\tfinal double period = (1000.0 / _frequency);\n\t_xblockexpression = (period * numCycles);\n}\nreturn _xblockexpression;'"
	 * @generated
	 */
	double getTotalLatency();

} // HWTargetDesign
