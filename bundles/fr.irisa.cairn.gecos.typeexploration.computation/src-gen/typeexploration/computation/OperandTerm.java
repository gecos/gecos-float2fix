/**
 */
package typeexploration.computation;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operand Term</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.computation.OperandTerm#getCoef <em>Coef</em>}</li>
 *   <li>{@link typeexploration.computation.OperandTerm#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see typeexploration.computation.ComputationPackage#getOperandTerm()
 * @model
 * @generated
 */
public interface OperandTerm extends OperandExpression {
	/**
	 * Returns the value of the '<em><b>Coef</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Coef</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coef</em>' attribute.
	 * @see #setCoef(int)
	 * @see typeexploration.computation.ComputationPackage#getOperandTerm_Coef()
	 * @model default="1" unique="false"
	 * @generated
	 */
	int getCoef();

	/**
	 * Sets the value of the '{@link typeexploration.computation.OperandTerm#getCoef <em>Coef</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Coef</em>' attribute.
	 * @see #getCoef()
	 * @generated
	 */
	void setCoef(int value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see typeexploration.computation.ComputationPackage#getOperandTerm_Name()
	 * @model unique="false"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link typeexploration.computation.OperandTerm#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // OperandTerm
