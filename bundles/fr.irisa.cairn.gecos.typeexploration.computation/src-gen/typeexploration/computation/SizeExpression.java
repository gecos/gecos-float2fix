/**
 */
package typeexploration.computation;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Size Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see typeexploration.computation.ComputationPackage#getSizeExpression()
 * @model abstract="true"
 * @generated
 */
public interface SizeExpression extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 * @generated
	 */
	long evaluate();

} // SizeExpression
