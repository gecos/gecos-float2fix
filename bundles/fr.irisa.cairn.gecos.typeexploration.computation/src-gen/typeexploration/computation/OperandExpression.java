/**
 */
package typeexploration.computation;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operand Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see typeexploration.computation.ComputationPackage#getOperandExpression()
 * @model abstract="true"
 * @generated
 */
public interface OperandExpression extends EObject {
} // OperandExpression
