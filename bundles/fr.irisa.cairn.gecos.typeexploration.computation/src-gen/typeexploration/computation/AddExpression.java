/**
 */
package typeexploration.computation;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Add Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.computation.AddExpression#getOp1 <em>Op1</em>}</li>
 *   <li>{@link typeexploration.computation.AddExpression#getOp2 <em>Op2</em>}</li>
 * </ul>
 *
 * @see typeexploration.computation.ComputationPackage#getAddExpression()
 * @model
 * @generated
 */
public interface AddExpression extends OperandExpression {
	/**
	 * Returns the value of the '<em><b>Op1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Op1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Op1</em>' containment reference.
	 * @see #setOp1(OperandExpression)
	 * @see typeexploration.computation.ComputationPackage#getAddExpression_Op1()
	 * @model containment="true"
	 * @generated
	 */
	OperandExpression getOp1();

	/**
	 * Sets the value of the '{@link typeexploration.computation.AddExpression#getOp1 <em>Op1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Op1</em>' containment reference.
	 * @see #getOp1()
	 * @generated
	 */
	void setOp1(OperandExpression value);

	/**
	 * Returns the value of the '<em><b>Op2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Op2</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Op2</em>' containment reference.
	 * @see #setOp2(OperandExpression)
	 * @see typeexploration.computation.ComputationPackage#getAddExpression_Op2()
	 * @model containment="true"
	 * @generated
	 */
	OperandExpression getOp2();

	/**
	 * Sets the value of the '{@link typeexploration.computation.AddExpression#getOp2 <em>Op2</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Op2</em>' containment reference.
	 * @see #getOp2()
	 * @generated
	 */
	void setOp2(OperandExpression value);

} // AddExpression
