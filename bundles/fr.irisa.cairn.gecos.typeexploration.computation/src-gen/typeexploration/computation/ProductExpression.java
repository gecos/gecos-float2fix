/**
 */
package typeexploration.computation;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Product Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.computation.ProductExpression#getConstant <em>Constant</em>}</li>
 *   <li>{@link typeexploration.computation.ProductExpression#getTerms <em>Terms</em>}</li>
 * </ul>
 *
 * @see typeexploration.computation.ComputationPackage#getProductExpression()
 * @model
 * @generated
 */
public interface ProductExpression extends SizeExpression {
	/**
	 * Returns the value of the '<em><b>Constant</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant</em>' attribute.
	 * @see #setConstant(int)
	 * @see typeexploration.computation.ComputationPackage#getProductExpression_Constant()
	 * @model default="1" unique="false"
	 * @generated
	 */
	int getConstant();

	/**
	 * Sets the value of the '{@link typeexploration.computation.ProductExpression#getConstant <em>Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant</em>' attribute.
	 * @see #getConstant()
	 * @generated
	 */
	void setConstant(int value);

	/**
	 * Returns the value of the '<em><b>Terms</b></em>' reference list.
	 * The list contents are of type {@link typeexploration.computation.Parameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Terms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Terms</em>' reference list.
	 * @see typeexploration.computation.ComputationPackage#getProductExpression_Terms()
	 * @model
	 * @generated
	 */
	EList<Parameter> getTerms();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%java.lang.Integer%&gt; _xblockexpression = null;\n{\n\tfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%typeexploration.computation.Parameter%&gt;, &lt;%java.lang.Integer%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%typeexploration.computation.Parameter%&gt;, &lt;%java.lang.Integer%&gt;&gt;()\n\t{\n\t\tpublic &lt;%java.lang.Integer%&gt; apply(final &lt;%typeexploration.computation.Parameter%&gt; t)\n\t\t{\n\t\t\treturn &lt;%java.lang.Integer%&gt;.valueOf(t.getValue());\n\t\t}\n\t};\n\tfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function2%&gt;&lt;&lt;%java.lang.Integer%&gt;, &lt;%java.lang.Integer%&gt;, &lt;%java.lang.Integer%&gt;&gt; _function_1 = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function2%&gt;&lt;&lt;%java.lang.Integer%&gt;, &lt;%java.lang.Integer%&gt;, &lt;%java.lang.Integer%&gt;&gt;()\n\t{\n\t\tpublic &lt;%java.lang.Integer%&gt; apply(final &lt;%java.lang.Integer%&gt; p1, final &lt;%java.lang.Integer%&gt; p2)\n\t\t{\n\t\t\treturn &lt;%java.lang.Integer%&gt;.valueOf(((p1).intValue() * (p2).intValue()));\n\t\t}\n\t};\n\tfinal &lt;%java.lang.Integer%&gt; tmp = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%java.lang.Integer%&gt;&gt;reduce(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%typeexploration.computation.Parameter%&gt;, &lt;%java.lang.Integer%&gt;&gt;map(this.getTerms(), _function), _function_1);\n\t&lt;%java.lang.Integer%&gt; _xifexpression = null;\n\tint _constant = this.getConstant();\n\tboolean _notEquals = (_constant != 0);\n\tif (_notEquals)\n\t{\n\t\tint _constant_1 = this.getConstant();\n\t\t_xifexpression = &lt;%java.lang.Integer%&gt;.valueOf(((tmp).intValue() * _constant_1));\n\t}\n\telse\n\t{\n\t\t_xifexpression = tmp;\n\t}\n\t_xblockexpression = _xifexpression;\n}\nreturn (_xblockexpression).intValue();'"
	 * @generated
	 */
	long evaluate();

} // ProductExpression
