/**
 */
package typeexploration.computation;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Summation Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.computation.SummationExpression#getConstant <em>Constant</em>}</li>
 *   <li>{@link typeexploration.computation.SummationExpression#getTerms <em>Terms</em>}</li>
 * </ul>
 *
 * @see typeexploration.computation.ComputationPackage#getSummationExpression()
 * @model
 * @generated
 */
public interface SummationExpression extends SizeExpression {
	/**
	 * Returns the value of the '<em><b>Constant</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant</em>' attribute.
	 * @see #setConstant(int)
	 * @see typeexploration.computation.ComputationPackage#getSummationExpression_Constant()
	 * @model default="0" unique="false"
	 * @generated
	 */
	int getConstant();

	/**
	 * Sets the value of the '{@link typeexploration.computation.SummationExpression#getConstant <em>Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant</em>' attribute.
	 * @see #getConstant()
	 * @generated
	 */
	void setConstant(int value);

	/**
	 * Returns the value of the '<em><b>Terms</b></em>' containment reference list.
	 * The list contents are of type {@link typeexploration.computation.ProductExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Terms</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Terms</em>' containment reference list.
	 * @see typeexploration.computation.ComputationPackage#getSummationExpression_Terms()
	 * @model containment="true"
	 * @generated
	 */
	EList<ProductExpression> getTerms();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='final &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%typeexploration.computation.ProductExpression%&gt;, &lt;%java.lang.Long%&gt;&gt; _function = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function1%&gt;&lt;&lt;%typeexploration.computation.ProductExpression%&gt;, &lt;%java.lang.Long%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Long%&gt; apply(final &lt;%typeexploration.computation.ProductExpression%&gt; t)\n\t{\n\t\treturn &lt;%java.lang.Long%&gt;.valueOf(t.evaluate());\n\t}\n};\nfinal &lt;%org.eclipse.xtext.xbase.lib.Functions.Function2%&gt;&lt;&lt;%java.lang.Long%&gt;, &lt;%java.lang.Long%&gt;, &lt;%java.lang.Long%&gt;&gt; _function_1 = new &lt;%org.eclipse.xtext.xbase.lib.Functions.Function2%&gt;&lt;&lt;%java.lang.Long%&gt;, &lt;%java.lang.Long%&gt;, &lt;%java.lang.Long%&gt;&gt;()\n{\n\tpublic &lt;%java.lang.Long%&gt; apply(final &lt;%java.lang.Long%&gt; p1, final &lt;%java.lang.Long%&gt; p2)\n\t{\n\t\treturn &lt;%java.lang.Long%&gt;.valueOf(((p1).longValue() + (p2).longValue()));\n\t}\n};\n&lt;%java.lang.Long%&gt; _reduce = &lt;%org.eclipse.xtext.xbase.lib.IterableExtensions%&gt;.&lt;&lt;%java.lang.Long%&gt;&gt;reduce(&lt;%org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions%&gt;.&lt;&lt;%typeexploration.computation.ProductExpression%&gt;, &lt;%java.lang.Long%&gt;&gt;map(this.getTerms(), _function), _function_1);\nint _constant = this.getConstant();\nreturn ((_reduce).longValue() + _constant);'"
	 * @generated
	 */
	long evaluate();

} // SummationExpression
