/**
 */
package typeexploration.computation;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.computation.Operation#getOpType <em>Op Type</em>}</li>
 *   <li>{@link typeexploration.computation.Operation#getOutputExpr <em>Output Expr</em>}</li>
 *   <li>{@link typeexploration.computation.Operation#getInputExpr1 <em>Input Expr1</em>}</li>
 *   <li>{@link typeexploration.computation.Operation#getInputExpr2 <em>Input Expr2</em>}</li>
 *   <li>{@link typeexploration.computation.Operation#getNumOps <em>Num Ops</em>}</li>
 * </ul>
 *
 * @see typeexploration.computation.ComputationPackage#getOperation()
 * @model
 * @generated
 */
public interface Operation extends EObject {
	/**
	 * Returns the value of the '<em><b>Op Type</b></em>' attribute.
	 * The literals are from the enumeration {@link typeexploration.computation.HWOpType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Op Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Op Type</em>' attribute.
	 * @see typeexploration.computation.HWOpType
	 * @see #setOpType(HWOpType)
	 * @see typeexploration.computation.ComputationPackage#getOperation_OpType()
	 * @model unique="false"
	 * @generated
	 */
	HWOpType getOpType();

	/**
	 * Sets the value of the '{@link typeexploration.computation.Operation#getOpType <em>Op Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Op Type</em>' attribute.
	 * @see typeexploration.computation.HWOpType
	 * @see #getOpType()
	 * @generated
	 */
	void setOpType(HWOpType value);

	/**
	 * Returns the value of the '<em><b>Output Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Expr</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Expr</em>' containment reference.
	 * @see #setOutputExpr(OperandExpression)
	 * @see typeexploration.computation.ComputationPackage#getOperation_OutputExpr()
	 * @model containment="true"
	 * @generated
	 */
	OperandExpression getOutputExpr();

	/**
	 * Sets the value of the '{@link typeexploration.computation.Operation#getOutputExpr <em>Output Expr</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Expr</em>' containment reference.
	 * @see #getOutputExpr()
	 * @generated
	 */
	void setOutputExpr(OperandExpression value);

	/**
	 * Returns the value of the '<em><b>Input Expr1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Expr1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Expr1</em>' containment reference.
	 * @see #setInputExpr1(OperandExpression)
	 * @see typeexploration.computation.ComputationPackage#getOperation_InputExpr1()
	 * @model containment="true"
	 * @generated
	 */
	OperandExpression getInputExpr1();

	/**
	 * Sets the value of the '{@link typeexploration.computation.Operation#getInputExpr1 <em>Input Expr1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Expr1</em>' containment reference.
	 * @see #getInputExpr1()
	 * @generated
	 */
	void setInputExpr1(OperandExpression value);

	/**
	 * Returns the value of the '<em><b>Input Expr2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Expr2</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Expr2</em>' containment reference.
	 * @see #setInputExpr2(OperandExpression)
	 * @see typeexploration.computation.ComputationPackage#getOperation_InputExpr2()
	 * @model containment="true"
	 * @generated
	 */
	OperandExpression getInputExpr2();

	/**
	 * Sets the value of the '{@link typeexploration.computation.Operation#getInputExpr2 <em>Input Expr2</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Expr2</em>' containment reference.
	 * @see #getInputExpr2()
	 * @generated
	 */
	void setInputExpr2(OperandExpression value);

	/**
	 * Returns the value of the '<em><b>Num Ops</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Num Ops</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Num Ops</em>' containment reference.
	 * @see #setNumOps(SizeExpression)
	 * @see typeexploration.computation.ComputationPackage#getOperation_NumOps()
	 * @model containment="true"
	 * @generated
	 */
	SizeExpression getNumOps();

	/**
	 * Sets the value of the '{@link typeexploration.computation.Operation#getNumOps <em>Num Ops</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Num Ops</em>' containment reference.
	 * @see #getNumOps()
	 * @generated
	 */
	void setNumOps(SizeExpression value);

} // Operation
