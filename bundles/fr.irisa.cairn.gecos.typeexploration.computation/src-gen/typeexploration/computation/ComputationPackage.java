/**
 */
package typeexploration.computation;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see typeexploration.computation.ComputationFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel basePackage='typeexploration'"
 * @generated
 */
public interface ComputationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "computation";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.gecos.org/float2fix/computation";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "computation";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ComputationPackage eINSTANCE = typeexploration.computation.impl.ComputationPackageImpl.init();

	/**
	 * The meta object id for the '{@link typeexploration.computation.impl.ComputationModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.computation.impl.ComputationModelImpl
	 * @see typeexploration.computation.impl.ComputationPackageImpl#getComputationModel()
	 * @generated
	 */
	int COMPUTATION_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_MODEL__PARAMETERS = 0;

	/**
	 * The feature id for the '<em><b>Target Design</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_MODEL__TARGET_DESIGN = 1;

	/**
	 * The feature id for the '<em><b>Block Groups</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_MODEL__BLOCK_GROUPS = 2;

	/**
	 * The feature id for the '<em><b>Blocks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_MODEL__BLOCKS = 3;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_MODEL_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link typeexploration.computation.impl.ParameterImpl <em>Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.computation.impl.ParameterImpl
	 * @see typeexploration.computation.impl.ComputationPackageImpl#getParameter()
	 * @generated
	 */
	int PARAMETER = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__NAME = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link typeexploration.computation.impl.HWTargetDesignImpl <em>HW Target Design</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.computation.impl.HWTargetDesignImpl
	 * @see typeexploration.computation.impl.ComputationPackageImpl#getHWTargetDesign()
	 * @generated
	 */
	int HW_TARGET_DESIGN = 2;

	/**
	 * The feature id for the '<em><b>Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_TARGET_DESIGN__FREQUENCY = 0;

	/**
	 * The feature id for the '<em><b>Outputs Per Cycle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_TARGET_DESIGN__OUTPUTS_PER_CYCLE = 1;

	/**
	 * The feature id for the '<em><b>Problem Size Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_TARGET_DESIGN__PROBLEM_SIZE_EXPR = 2;

	/**
	 * The feature id for the '<em><b>Total Latency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_TARGET_DESIGN__TOTAL_LATENCY = 3;

	/**
	 * The number of structural features of the '<em>HW Target Design</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_TARGET_DESIGN_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>HW Target Design</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_TARGET_DESIGN_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link typeexploration.computation.impl.BlockGroupImpl <em>Block Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.computation.impl.BlockGroupImpl
	 * @see typeexploration.computation.impl.ComputationPackageImpl#getBlockGroup()
	 * @generated
	 */
	int BLOCK_GROUP = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_GROUP__NAME = 0;

	/**
	 * The feature id for the '<em><b>Blocks</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_GROUP__BLOCKS = 1;

	/**
	 * The number of structural features of the '<em>Block Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_GROUP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Block Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_GROUP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link typeexploration.computation.impl.ComputationBlockImpl <em>Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.computation.impl.ComputationBlockImpl
	 * @see typeexploration.computation.impl.ComputationPackageImpl#getComputationBlock()
	 * @generated
	 */
	int COMPUTATION_BLOCK = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_BLOCK__NAME = 0;

	/**
	 * The feature id for the '<em><b>Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_BLOCK__OPERATIONS = 1;

	/**
	 * The number of structural features of the '<em>Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_BLOCK_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_BLOCK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link typeexploration.computation.impl.OperationImpl <em>Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.computation.impl.OperationImpl
	 * @see typeexploration.computation.impl.ComputationPackageImpl#getOperation()
	 * @generated
	 */
	int OPERATION = 5;

	/**
	 * The feature id for the '<em><b>Op Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__OP_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Output Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__OUTPUT_EXPR = 1;

	/**
	 * The feature id for the '<em><b>Input Expr1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__INPUT_EXPR1 = 2;

	/**
	 * The feature id for the '<em><b>Input Expr2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__INPUT_EXPR2 = 3;

	/**
	 * The feature id for the '<em><b>Num Ops</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__NUM_OPS = 4;

	/**
	 * The number of structural features of the '<em>Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link typeexploration.computation.impl.OperandExpressionImpl <em>Operand Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.computation.impl.OperandExpressionImpl
	 * @see typeexploration.computation.impl.ComputationPackageImpl#getOperandExpression()
	 * @generated
	 */
	int OPERAND_EXPRESSION = 6;

	/**
	 * The number of structural features of the '<em>Operand Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERAND_EXPRESSION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Operand Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERAND_EXPRESSION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link typeexploration.computation.impl.MaxExpressionImpl <em>Max Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.computation.impl.MaxExpressionImpl
	 * @see typeexploration.computation.impl.ComputationPackageImpl#getMaxExpression()
	 * @generated
	 */
	int MAX_EXPRESSION = 7;

	/**
	 * The feature id for the '<em><b>Exprs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAX_EXPRESSION__EXPRS = OPERAND_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Max Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAX_EXPRESSION_FEATURE_COUNT = OPERAND_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Max Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAX_EXPRESSION_OPERATION_COUNT = OPERAND_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link typeexploration.computation.impl.AddExpressionImpl <em>Add Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.computation.impl.AddExpressionImpl
	 * @see typeexploration.computation.impl.ComputationPackageImpl#getAddExpression()
	 * @generated
	 */
	int ADD_EXPRESSION = 8;

	/**
	 * The feature id for the '<em><b>Op1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADD_EXPRESSION__OP1 = OPERAND_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Op2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADD_EXPRESSION__OP2 = OPERAND_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Add Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADD_EXPRESSION_FEATURE_COUNT = OPERAND_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Add Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADD_EXPRESSION_OPERATION_COUNT = OPERAND_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link typeexploration.computation.impl.OperandTermImpl <em>Operand Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.computation.impl.OperandTermImpl
	 * @see typeexploration.computation.impl.ComputationPackageImpl#getOperandTerm()
	 * @generated
	 */
	int OPERAND_TERM = 9;

	/**
	 * The feature id for the '<em><b>Coef</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERAND_TERM__COEF = OPERAND_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERAND_TERM__NAME = OPERAND_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Operand Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERAND_TERM_FEATURE_COUNT = OPERAND_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Operand Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERAND_TERM_OPERATION_COUNT = OPERAND_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link typeexploration.computation.impl.SizeExpressionImpl <em>Size Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.computation.impl.SizeExpressionImpl
	 * @see typeexploration.computation.impl.ComputationPackageImpl#getSizeExpression()
	 * @generated
	 */
	int SIZE_EXPRESSION = 10;

	/**
	 * The number of structural features of the '<em>Size Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZE_EXPRESSION_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZE_EXPRESSION___EVALUATE = 0;

	/**
	 * The number of operations of the '<em>Size Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZE_EXPRESSION_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link typeexploration.computation.impl.SummationExpressionImpl <em>Summation Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.computation.impl.SummationExpressionImpl
	 * @see typeexploration.computation.impl.ComputationPackageImpl#getSummationExpression()
	 * @generated
	 */
	int SUMMATION_EXPRESSION = 11;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUMMATION_EXPRESSION__CONSTANT = SIZE_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Terms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUMMATION_EXPRESSION__TERMS = SIZE_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Summation Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUMMATION_EXPRESSION_FEATURE_COUNT = SIZE_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUMMATION_EXPRESSION___EVALUATE = SIZE_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Summation Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUMMATION_EXPRESSION_OPERATION_COUNT = SIZE_EXPRESSION_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link typeexploration.computation.impl.ProductExpressionImpl <em>Product Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.computation.impl.ProductExpressionImpl
	 * @see typeexploration.computation.impl.ComputationPackageImpl#getProductExpression()
	 * @generated
	 */
	int PRODUCT_EXPRESSION = 12;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCT_EXPRESSION__CONSTANT = SIZE_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Terms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCT_EXPRESSION__TERMS = SIZE_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Product Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCT_EXPRESSION_FEATURE_COUNT = SIZE_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCT_EXPRESSION___EVALUATE = SIZE_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Product Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCT_EXPRESSION_OPERATION_COUNT = SIZE_EXPRESSION_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link typeexploration.computation.HWOpType <em>HW Op Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see typeexploration.computation.HWOpType
	 * @see typeexploration.computation.impl.ComputationPackageImpl#getHWOpType()
	 * @generated
	 */
	int HW_OP_TYPE = 13;


	/**
	 * Returns the meta object for class '{@link typeexploration.computation.ComputationModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see typeexploration.computation.ComputationModel
	 * @generated
	 */
	EClass getComputationModel();

	/**
	 * Returns the meta object for the containment reference list '{@link typeexploration.computation.ComputationModel#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see typeexploration.computation.ComputationModel#getParameters()
	 * @see #getComputationModel()
	 * @generated
	 */
	EReference getComputationModel_Parameters();

	/**
	 * Returns the meta object for the containment reference '{@link typeexploration.computation.ComputationModel#getTargetDesign <em>Target Design</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Target Design</em>'.
	 * @see typeexploration.computation.ComputationModel#getTargetDesign()
	 * @see #getComputationModel()
	 * @generated
	 */
	EReference getComputationModel_TargetDesign();

	/**
	 * Returns the meta object for the containment reference list '{@link typeexploration.computation.ComputationModel#getBlockGroups <em>Block Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Block Groups</em>'.
	 * @see typeexploration.computation.ComputationModel#getBlockGroups()
	 * @see #getComputationModel()
	 * @generated
	 */
	EReference getComputationModel_BlockGroups();

	/**
	 * Returns the meta object for the containment reference list '{@link typeexploration.computation.ComputationModel#getBlocks <em>Blocks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Blocks</em>'.
	 * @see typeexploration.computation.ComputationModel#getBlocks()
	 * @see #getComputationModel()
	 * @generated
	 */
	EReference getComputationModel_Blocks();

	/**
	 * Returns the meta object for class '{@link typeexploration.computation.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter</em>'.
	 * @see typeexploration.computation.Parameter
	 * @generated
	 */
	EClass getParameter();

	/**
	 * Returns the meta object for the attribute '{@link typeexploration.computation.Parameter#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see typeexploration.computation.Parameter#getName()
	 * @see #getParameter()
	 * @generated
	 */
	EAttribute getParameter_Name();

	/**
	 * Returns the meta object for the attribute '{@link typeexploration.computation.Parameter#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see typeexploration.computation.Parameter#getValue()
	 * @see #getParameter()
	 * @generated
	 */
	EAttribute getParameter_Value();

	/**
	 * Returns the meta object for class '{@link typeexploration.computation.HWTargetDesign <em>HW Target Design</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HW Target Design</em>'.
	 * @see typeexploration.computation.HWTargetDesign
	 * @generated
	 */
	EClass getHWTargetDesign();

	/**
	 * Returns the meta object for the attribute '{@link typeexploration.computation.HWTargetDesign#getFrequency <em>Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Frequency</em>'.
	 * @see typeexploration.computation.HWTargetDesign#getFrequency()
	 * @see #getHWTargetDesign()
	 * @generated
	 */
	EAttribute getHWTargetDesign_Frequency();

	/**
	 * Returns the meta object for the attribute '{@link typeexploration.computation.HWTargetDesign#getOutputsPerCycle <em>Outputs Per Cycle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Outputs Per Cycle</em>'.
	 * @see typeexploration.computation.HWTargetDesign#getOutputsPerCycle()
	 * @see #getHWTargetDesign()
	 * @generated
	 */
	EAttribute getHWTargetDesign_OutputsPerCycle();

	/**
	 * Returns the meta object for the containment reference '{@link typeexploration.computation.HWTargetDesign#getProblemSizeExpr <em>Problem Size Expr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Problem Size Expr</em>'.
	 * @see typeexploration.computation.HWTargetDesign#getProblemSizeExpr()
	 * @see #getHWTargetDesign()
	 * @generated
	 */
	EReference getHWTargetDesign_ProblemSizeExpr();

	/**
	 * Returns the meta object for the attribute '{@link typeexploration.computation.HWTargetDesign#getTotalLatency <em>Total Latency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Total Latency</em>'.
	 * @see typeexploration.computation.HWTargetDesign#getTotalLatency()
	 * @see #getHWTargetDesign()
	 * @generated
	 */
	EAttribute getHWTargetDesign_TotalLatency();

	/**
	 * Returns the meta object for class '{@link typeexploration.computation.BlockGroup <em>Block Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Block Group</em>'.
	 * @see typeexploration.computation.BlockGroup
	 * @generated
	 */
	EClass getBlockGroup();

	/**
	 * Returns the meta object for the attribute '{@link typeexploration.computation.BlockGroup#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see typeexploration.computation.BlockGroup#getName()
	 * @see #getBlockGroup()
	 * @generated
	 */
	EAttribute getBlockGroup_Name();

	/**
	 * Returns the meta object for the reference list '{@link typeexploration.computation.BlockGroup#getBlocks <em>Blocks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Blocks</em>'.
	 * @see typeexploration.computation.BlockGroup#getBlocks()
	 * @see #getBlockGroup()
	 * @generated
	 */
	EReference getBlockGroup_Blocks();

	/**
	 * Returns the meta object for class '{@link typeexploration.computation.ComputationBlock <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Block</em>'.
	 * @see typeexploration.computation.ComputationBlock
	 * @generated
	 */
	EClass getComputationBlock();

	/**
	 * Returns the meta object for the attribute '{@link typeexploration.computation.ComputationBlock#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see typeexploration.computation.ComputationBlock#getName()
	 * @see #getComputationBlock()
	 * @generated
	 */
	EAttribute getComputationBlock_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link typeexploration.computation.ComputationBlock#getOperations <em>Operations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operations</em>'.
	 * @see typeexploration.computation.ComputationBlock#getOperations()
	 * @see #getComputationBlock()
	 * @generated
	 */
	EReference getComputationBlock_Operations();

	/**
	 * Returns the meta object for class '{@link typeexploration.computation.Operation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation</em>'.
	 * @see typeexploration.computation.Operation
	 * @generated
	 */
	EClass getOperation();

	/**
	 * Returns the meta object for the attribute '{@link typeexploration.computation.Operation#getOpType <em>Op Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Op Type</em>'.
	 * @see typeexploration.computation.Operation#getOpType()
	 * @see #getOperation()
	 * @generated
	 */
	EAttribute getOperation_OpType();

	/**
	 * Returns the meta object for the containment reference '{@link typeexploration.computation.Operation#getOutputExpr <em>Output Expr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Output Expr</em>'.
	 * @see typeexploration.computation.Operation#getOutputExpr()
	 * @see #getOperation()
	 * @generated
	 */
	EReference getOperation_OutputExpr();

	/**
	 * Returns the meta object for the containment reference '{@link typeexploration.computation.Operation#getInputExpr1 <em>Input Expr1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Input Expr1</em>'.
	 * @see typeexploration.computation.Operation#getInputExpr1()
	 * @see #getOperation()
	 * @generated
	 */
	EReference getOperation_InputExpr1();

	/**
	 * Returns the meta object for the containment reference '{@link typeexploration.computation.Operation#getInputExpr2 <em>Input Expr2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Input Expr2</em>'.
	 * @see typeexploration.computation.Operation#getInputExpr2()
	 * @see #getOperation()
	 * @generated
	 */
	EReference getOperation_InputExpr2();

	/**
	 * Returns the meta object for the containment reference '{@link typeexploration.computation.Operation#getNumOps <em>Num Ops</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Num Ops</em>'.
	 * @see typeexploration.computation.Operation#getNumOps()
	 * @see #getOperation()
	 * @generated
	 */
	EReference getOperation_NumOps();

	/**
	 * Returns the meta object for class '{@link typeexploration.computation.OperandExpression <em>Operand Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operand Expression</em>'.
	 * @see typeexploration.computation.OperandExpression
	 * @generated
	 */
	EClass getOperandExpression();

	/**
	 * Returns the meta object for class '{@link typeexploration.computation.MaxExpression <em>Max Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Max Expression</em>'.
	 * @see typeexploration.computation.MaxExpression
	 * @generated
	 */
	EClass getMaxExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link typeexploration.computation.MaxExpression#getExprs <em>Exprs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Exprs</em>'.
	 * @see typeexploration.computation.MaxExpression#getExprs()
	 * @see #getMaxExpression()
	 * @generated
	 */
	EReference getMaxExpression_Exprs();

	/**
	 * Returns the meta object for class '{@link typeexploration.computation.AddExpression <em>Add Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Add Expression</em>'.
	 * @see typeexploration.computation.AddExpression
	 * @generated
	 */
	EClass getAddExpression();

	/**
	 * Returns the meta object for the containment reference '{@link typeexploration.computation.AddExpression#getOp1 <em>Op1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Op1</em>'.
	 * @see typeexploration.computation.AddExpression#getOp1()
	 * @see #getAddExpression()
	 * @generated
	 */
	EReference getAddExpression_Op1();

	/**
	 * Returns the meta object for the containment reference '{@link typeexploration.computation.AddExpression#getOp2 <em>Op2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Op2</em>'.
	 * @see typeexploration.computation.AddExpression#getOp2()
	 * @see #getAddExpression()
	 * @generated
	 */
	EReference getAddExpression_Op2();

	/**
	 * Returns the meta object for class '{@link typeexploration.computation.OperandTerm <em>Operand Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operand Term</em>'.
	 * @see typeexploration.computation.OperandTerm
	 * @generated
	 */
	EClass getOperandTerm();

	/**
	 * Returns the meta object for the attribute '{@link typeexploration.computation.OperandTerm#getCoef <em>Coef</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Coef</em>'.
	 * @see typeexploration.computation.OperandTerm#getCoef()
	 * @see #getOperandTerm()
	 * @generated
	 */
	EAttribute getOperandTerm_Coef();

	/**
	 * Returns the meta object for the attribute '{@link typeexploration.computation.OperandTerm#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see typeexploration.computation.OperandTerm#getName()
	 * @see #getOperandTerm()
	 * @generated
	 */
	EAttribute getOperandTerm_Name();

	/**
	 * Returns the meta object for class '{@link typeexploration.computation.SizeExpression <em>Size Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Size Expression</em>'.
	 * @see typeexploration.computation.SizeExpression
	 * @generated
	 */
	EClass getSizeExpression();

	/**
	 * Returns the meta object for the '{@link typeexploration.computation.SizeExpression#evaluate() <em>Evaluate</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Evaluate</em>' operation.
	 * @see typeexploration.computation.SizeExpression#evaluate()
	 * @generated
	 */
	EOperation getSizeExpression__Evaluate();

	/**
	 * Returns the meta object for class '{@link typeexploration.computation.SummationExpression <em>Summation Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Summation Expression</em>'.
	 * @see typeexploration.computation.SummationExpression
	 * @generated
	 */
	EClass getSummationExpression();

	/**
	 * Returns the meta object for the attribute '{@link typeexploration.computation.SummationExpression#getConstant <em>Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Constant</em>'.
	 * @see typeexploration.computation.SummationExpression#getConstant()
	 * @see #getSummationExpression()
	 * @generated
	 */
	EAttribute getSummationExpression_Constant();

	/**
	 * Returns the meta object for the containment reference list '{@link typeexploration.computation.SummationExpression#getTerms <em>Terms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Terms</em>'.
	 * @see typeexploration.computation.SummationExpression#getTerms()
	 * @see #getSummationExpression()
	 * @generated
	 */
	EReference getSummationExpression_Terms();

	/**
	 * Returns the meta object for the '{@link typeexploration.computation.SummationExpression#evaluate() <em>Evaluate</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Evaluate</em>' operation.
	 * @see typeexploration.computation.SummationExpression#evaluate()
	 * @generated
	 */
	EOperation getSummationExpression__Evaluate();

	/**
	 * Returns the meta object for class '{@link typeexploration.computation.ProductExpression <em>Product Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Product Expression</em>'.
	 * @see typeexploration.computation.ProductExpression
	 * @generated
	 */
	EClass getProductExpression();

	/**
	 * Returns the meta object for the attribute '{@link typeexploration.computation.ProductExpression#getConstant <em>Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Constant</em>'.
	 * @see typeexploration.computation.ProductExpression#getConstant()
	 * @see #getProductExpression()
	 * @generated
	 */
	EAttribute getProductExpression_Constant();

	/**
	 * Returns the meta object for the reference list '{@link typeexploration.computation.ProductExpression#getTerms <em>Terms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Terms</em>'.
	 * @see typeexploration.computation.ProductExpression#getTerms()
	 * @see #getProductExpression()
	 * @generated
	 */
	EReference getProductExpression_Terms();

	/**
	 * Returns the meta object for the '{@link typeexploration.computation.ProductExpression#evaluate() <em>Evaluate</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Evaluate</em>' operation.
	 * @see typeexploration.computation.ProductExpression#evaluate()
	 * @generated
	 */
	EOperation getProductExpression__Evaluate();

	/**
	 * Returns the meta object for enum '{@link typeexploration.computation.HWOpType <em>HW Op Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>HW Op Type</em>'.
	 * @see typeexploration.computation.HWOpType
	 * @generated
	 */
	EEnum getHWOpType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ComputationFactory getComputationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link typeexploration.computation.impl.ComputationModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.computation.impl.ComputationModelImpl
		 * @see typeexploration.computation.impl.ComputationPackageImpl#getComputationModel()
		 * @generated
		 */
		EClass COMPUTATION_MODEL = eINSTANCE.getComputationModel();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPUTATION_MODEL__PARAMETERS = eINSTANCE.getComputationModel_Parameters();

		/**
		 * The meta object literal for the '<em><b>Target Design</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPUTATION_MODEL__TARGET_DESIGN = eINSTANCE.getComputationModel_TargetDesign();

		/**
		 * The meta object literal for the '<em><b>Block Groups</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPUTATION_MODEL__BLOCK_GROUPS = eINSTANCE.getComputationModel_BlockGroups();

		/**
		 * The meta object literal for the '<em><b>Blocks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPUTATION_MODEL__BLOCKS = eINSTANCE.getComputationModel_Blocks();

		/**
		 * The meta object literal for the '{@link typeexploration.computation.impl.ParameterImpl <em>Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.computation.impl.ParameterImpl
		 * @see typeexploration.computation.impl.ComputationPackageImpl#getParameter()
		 * @generated
		 */
		EClass PARAMETER = eINSTANCE.getParameter();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER__NAME = eINSTANCE.getParameter_Name();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER__VALUE = eINSTANCE.getParameter_Value();

		/**
		 * The meta object literal for the '{@link typeexploration.computation.impl.HWTargetDesignImpl <em>HW Target Design</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.computation.impl.HWTargetDesignImpl
		 * @see typeexploration.computation.impl.ComputationPackageImpl#getHWTargetDesign()
		 * @generated
		 */
		EClass HW_TARGET_DESIGN = eINSTANCE.getHWTargetDesign();

		/**
		 * The meta object literal for the '<em><b>Frequency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HW_TARGET_DESIGN__FREQUENCY = eINSTANCE.getHWTargetDesign_Frequency();

		/**
		 * The meta object literal for the '<em><b>Outputs Per Cycle</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HW_TARGET_DESIGN__OUTPUTS_PER_CYCLE = eINSTANCE.getHWTargetDesign_OutputsPerCycle();

		/**
		 * The meta object literal for the '<em><b>Problem Size Expr</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HW_TARGET_DESIGN__PROBLEM_SIZE_EXPR = eINSTANCE.getHWTargetDesign_ProblemSizeExpr();

		/**
		 * The meta object literal for the '<em><b>Total Latency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HW_TARGET_DESIGN__TOTAL_LATENCY = eINSTANCE.getHWTargetDesign_TotalLatency();

		/**
		 * The meta object literal for the '{@link typeexploration.computation.impl.BlockGroupImpl <em>Block Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.computation.impl.BlockGroupImpl
		 * @see typeexploration.computation.impl.ComputationPackageImpl#getBlockGroup()
		 * @generated
		 */
		EClass BLOCK_GROUP = eINSTANCE.getBlockGroup();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLOCK_GROUP__NAME = eINSTANCE.getBlockGroup_Name();

		/**
		 * The meta object literal for the '<em><b>Blocks</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK_GROUP__BLOCKS = eINSTANCE.getBlockGroup_Blocks();

		/**
		 * The meta object literal for the '{@link typeexploration.computation.impl.ComputationBlockImpl <em>Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.computation.impl.ComputationBlockImpl
		 * @see typeexploration.computation.impl.ComputationPackageImpl#getComputationBlock()
		 * @generated
		 */
		EClass COMPUTATION_BLOCK = eINSTANCE.getComputationBlock();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPUTATION_BLOCK__NAME = eINSTANCE.getComputationBlock_Name();

		/**
		 * The meta object literal for the '<em><b>Operations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPUTATION_BLOCK__OPERATIONS = eINSTANCE.getComputationBlock_Operations();

		/**
		 * The meta object literal for the '{@link typeexploration.computation.impl.OperationImpl <em>Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.computation.impl.OperationImpl
		 * @see typeexploration.computation.impl.ComputationPackageImpl#getOperation()
		 * @generated
		 */
		EClass OPERATION = eINSTANCE.getOperation();

		/**
		 * The meta object literal for the '<em><b>Op Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION__OP_TYPE = eINSTANCE.getOperation_OpType();

		/**
		 * The meta object literal for the '<em><b>Output Expr</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION__OUTPUT_EXPR = eINSTANCE.getOperation_OutputExpr();

		/**
		 * The meta object literal for the '<em><b>Input Expr1</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION__INPUT_EXPR1 = eINSTANCE.getOperation_InputExpr1();

		/**
		 * The meta object literal for the '<em><b>Input Expr2</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION__INPUT_EXPR2 = eINSTANCE.getOperation_InputExpr2();

		/**
		 * The meta object literal for the '<em><b>Num Ops</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION__NUM_OPS = eINSTANCE.getOperation_NumOps();

		/**
		 * The meta object literal for the '{@link typeexploration.computation.impl.OperandExpressionImpl <em>Operand Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.computation.impl.OperandExpressionImpl
		 * @see typeexploration.computation.impl.ComputationPackageImpl#getOperandExpression()
		 * @generated
		 */
		EClass OPERAND_EXPRESSION = eINSTANCE.getOperandExpression();

		/**
		 * The meta object literal for the '{@link typeexploration.computation.impl.MaxExpressionImpl <em>Max Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.computation.impl.MaxExpressionImpl
		 * @see typeexploration.computation.impl.ComputationPackageImpl#getMaxExpression()
		 * @generated
		 */
		EClass MAX_EXPRESSION = eINSTANCE.getMaxExpression();

		/**
		 * The meta object literal for the '<em><b>Exprs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAX_EXPRESSION__EXPRS = eINSTANCE.getMaxExpression_Exprs();

		/**
		 * The meta object literal for the '{@link typeexploration.computation.impl.AddExpressionImpl <em>Add Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.computation.impl.AddExpressionImpl
		 * @see typeexploration.computation.impl.ComputationPackageImpl#getAddExpression()
		 * @generated
		 */
		EClass ADD_EXPRESSION = eINSTANCE.getAddExpression();

		/**
		 * The meta object literal for the '<em><b>Op1</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ADD_EXPRESSION__OP1 = eINSTANCE.getAddExpression_Op1();

		/**
		 * The meta object literal for the '<em><b>Op2</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ADD_EXPRESSION__OP2 = eINSTANCE.getAddExpression_Op2();

		/**
		 * The meta object literal for the '{@link typeexploration.computation.impl.OperandTermImpl <em>Operand Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.computation.impl.OperandTermImpl
		 * @see typeexploration.computation.impl.ComputationPackageImpl#getOperandTerm()
		 * @generated
		 */
		EClass OPERAND_TERM = eINSTANCE.getOperandTerm();

		/**
		 * The meta object literal for the '<em><b>Coef</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERAND_TERM__COEF = eINSTANCE.getOperandTerm_Coef();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERAND_TERM__NAME = eINSTANCE.getOperandTerm_Name();

		/**
		 * The meta object literal for the '{@link typeexploration.computation.impl.SizeExpressionImpl <em>Size Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.computation.impl.SizeExpressionImpl
		 * @see typeexploration.computation.impl.ComputationPackageImpl#getSizeExpression()
		 * @generated
		 */
		EClass SIZE_EXPRESSION = eINSTANCE.getSizeExpression();

		/**
		 * The meta object literal for the '<em><b>Evaluate</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SIZE_EXPRESSION___EVALUATE = eINSTANCE.getSizeExpression__Evaluate();

		/**
		 * The meta object literal for the '{@link typeexploration.computation.impl.SummationExpressionImpl <em>Summation Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.computation.impl.SummationExpressionImpl
		 * @see typeexploration.computation.impl.ComputationPackageImpl#getSummationExpression()
		 * @generated
		 */
		EClass SUMMATION_EXPRESSION = eINSTANCE.getSummationExpression();

		/**
		 * The meta object literal for the '<em><b>Constant</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUMMATION_EXPRESSION__CONSTANT = eINSTANCE.getSummationExpression_Constant();

		/**
		 * The meta object literal for the '<em><b>Terms</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUMMATION_EXPRESSION__TERMS = eINSTANCE.getSummationExpression_Terms();

		/**
		 * The meta object literal for the '<em><b>Evaluate</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SUMMATION_EXPRESSION___EVALUATE = eINSTANCE.getSummationExpression__Evaluate();

		/**
		 * The meta object literal for the '{@link typeexploration.computation.impl.ProductExpressionImpl <em>Product Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.computation.impl.ProductExpressionImpl
		 * @see typeexploration.computation.impl.ComputationPackageImpl#getProductExpression()
		 * @generated
		 */
		EClass PRODUCT_EXPRESSION = eINSTANCE.getProductExpression();

		/**
		 * The meta object literal for the '<em><b>Constant</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRODUCT_EXPRESSION__CONSTANT = eINSTANCE.getProductExpression_Constant();

		/**
		 * The meta object literal for the '<em><b>Terms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRODUCT_EXPRESSION__TERMS = eINSTANCE.getProductExpression_Terms();

		/**
		 * The meta object literal for the '<em><b>Evaluate</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PRODUCT_EXPRESSION___EVALUATE = eINSTANCE.getProductExpression__Evaluate();

		/**
		 * The meta object literal for the '{@link typeexploration.computation.HWOpType <em>HW Op Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see typeexploration.computation.HWOpType
		 * @see typeexploration.computation.impl.ComputationPackageImpl#getHWOpType()
		 * @generated
		 */
		EEnum HW_OP_TYPE = eINSTANCE.getHWOpType();

	}

} //ComputationPackage
