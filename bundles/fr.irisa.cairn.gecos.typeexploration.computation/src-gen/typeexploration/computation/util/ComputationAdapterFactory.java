/**
 */
package typeexploration.computation.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import typeexploration.computation.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see typeexploration.computation.ComputationPackage
 * @generated
 */
public class ComputationAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ComputationPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComputationAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ComputationPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComputationSwitch<Adapter> modelSwitch =
		new ComputationSwitch<Adapter>() {
			@Override
			public Adapter caseComputationModel(ComputationModel object) {
				return createComputationModelAdapter();
			}
			@Override
			public Adapter caseParameter(Parameter object) {
				return createParameterAdapter();
			}
			@Override
			public Adapter caseHWTargetDesign(HWTargetDesign object) {
				return createHWTargetDesignAdapter();
			}
			@Override
			public Adapter caseBlockGroup(BlockGroup object) {
				return createBlockGroupAdapter();
			}
			@Override
			public Adapter caseComputationBlock(ComputationBlock object) {
				return createComputationBlockAdapter();
			}
			@Override
			public Adapter caseOperation(Operation object) {
				return createOperationAdapter();
			}
			@Override
			public Adapter caseOperandExpression(OperandExpression object) {
				return createOperandExpressionAdapter();
			}
			@Override
			public Adapter caseMaxExpression(MaxExpression object) {
				return createMaxExpressionAdapter();
			}
			@Override
			public Adapter caseAddExpression(AddExpression object) {
				return createAddExpressionAdapter();
			}
			@Override
			public Adapter caseOperandTerm(OperandTerm object) {
				return createOperandTermAdapter();
			}
			@Override
			public Adapter caseSizeExpression(SizeExpression object) {
				return createSizeExpressionAdapter();
			}
			@Override
			public Adapter caseSummationExpression(SummationExpression object) {
				return createSummationExpressionAdapter();
			}
			@Override
			public Adapter caseProductExpression(ProductExpression object) {
				return createProductExpressionAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.computation.ComputationModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.computation.ComputationModel
	 * @generated
	 */
	public Adapter createComputationModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.computation.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.computation.Parameter
	 * @generated
	 */
	public Adapter createParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.computation.HWTargetDesign <em>HW Target Design</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.computation.HWTargetDesign
	 * @generated
	 */
	public Adapter createHWTargetDesignAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.computation.BlockGroup <em>Block Group</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.computation.BlockGroup
	 * @generated
	 */
	public Adapter createBlockGroupAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.computation.ComputationBlock <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.computation.ComputationBlock
	 * @generated
	 */
	public Adapter createComputationBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.computation.Operation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.computation.Operation
	 * @generated
	 */
	public Adapter createOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.computation.OperandExpression <em>Operand Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.computation.OperandExpression
	 * @generated
	 */
	public Adapter createOperandExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.computation.MaxExpression <em>Max Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.computation.MaxExpression
	 * @generated
	 */
	public Adapter createMaxExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.computation.AddExpression <em>Add Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.computation.AddExpression
	 * @generated
	 */
	public Adapter createAddExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.computation.OperandTerm <em>Operand Term</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.computation.OperandTerm
	 * @generated
	 */
	public Adapter createOperandTermAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.computation.SizeExpression <em>Size Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.computation.SizeExpression
	 * @generated
	 */
	public Adapter createSizeExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.computation.SummationExpression <em>Summation Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.computation.SummationExpression
	 * @generated
	 */
	public Adapter createSummationExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link typeexploration.computation.ProductExpression <em>Product Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see typeexploration.computation.ProductExpression
	 * @generated
	 */
	public Adapter createProductExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ComputationAdapterFactory
