/**
 */
package typeexploration.computation.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import typeexploration.computation.BlockGroup;
import typeexploration.computation.ComputationBlock;
import typeexploration.computation.ComputationModel;
import typeexploration.computation.ComputationPackage;
import typeexploration.computation.HWTargetDesign;
import typeexploration.computation.Parameter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.computation.impl.ComputationModelImpl#getParameters <em>Parameters</em>}</li>
 *   <li>{@link typeexploration.computation.impl.ComputationModelImpl#getTargetDesign <em>Target Design</em>}</li>
 *   <li>{@link typeexploration.computation.impl.ComputationModelImpl#getBlockGroups <em>Block Groups</em>}</li>
 *   <li>{@link typeexploration.computation.impl.ComputationModelImpl#getBlocks <em>Blocks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComputationModelImpl extends MinimalEObjectImpl.Container implements ComputationModel {
	/**
	 * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<Parameter> parameters;

	/**
	 * The cached value of the '{@link #getTargetDesign() <em>Target Design</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetDesign()
	 * @generated
	 * @ordered
	 */
	protected HWTargetDesign targetDesign;

	/**
	 * The cached value of the '{@link #getBlockGroups() <em>Block Groups</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlockGroups()
	 * @generated
	 * @ordered
	 */
	protected EList<BlockGroup> blockGroups;

	/**
	 * The cached value of the '{@link #getBlocks() <em>Blocks</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlocks()
	 * @generated
	 * @ordered
	 */
	protected EList<ComputationBlock> blocks;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComputationModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ComputationPackage.Literals.COMPUTATION_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Parameter> getParameters() {
		if (parameters == null) {
			parameters = new EObjectContainmentEList<Parameter>(Parameter.class, this, ComputationPackage.COMPUTATION_MODEL__PARAMETERS);
		}
		return parameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HWTargetDesign getTargetDesign() {
		return targetDesign;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTargetDesign(HWTargetDesign newTargetDesign, NotificationChain msgs) {
		HWTargetDesign oldTargetDesign = targetDesign;
		targetDesign = newTargetDesign;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ComputationPackage.COMPUTATION_MODEL__TARGET_DESIGN, oldTargetDesign, newTargetDesign);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetDesign(HWTargetDesign newTargetDesign) {
		if (newTargetDesign != targetDesign) {
			NotificationChain msgs = null;
			if (targetDesign != null)
				msgs = ((InternalEObject)targetDesign).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ComputationPackage.COMPUTATION_MODEL__TARGET_DESIGN, null, msgs);
			if (newTargetDesign != null)
				msgs = ((InternalEObject)newTargetDesign).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ComputationPackage.COMPUTATION_MODEL__TARGET_DESIGN, null, msgs);
			msgs = basicSetTargetDesign(newTargetDesign, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComputationPackage.COMPUTATION_MODEL__TARGET_DESIGN, newTargetDesign, newTargetDesign));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BlockGroup> getBlockGroups() {
		if (blockGroups == null) {
			blockGroups = new EObjectContainmentEList<BlockGroup>(BlockGroup.class, this, ComputationPackage.COMPUTATION_MODEL__BLOCK_GROUPS);
		}
		return blockGroups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComputationBlock> getBlocks() {
		if (blocks == null) {
			blocks = new EObjectContainmentEList<ComputationBlock>(ComputationBlock.class, this, ComputationPackage.COMPUTATION_MODEL__BLOCKS);
		}
		return blocks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ComputationPackage.COMPUTATION_MODEL__PARAMETERS:
				return ((InternalEList<?>)getParameters()).basicRemove(otherEnd, msgs);
			case ComputationPackage.COMPUTATION_MODEL__TARGET_DESIGN:
				return basicSetTargetDesign(null, msgs);
			case ComputationPackage.COMPUTATION_MODEL__BLOCK_GROUPS:
				return ((InternalEList<?>)getBlockGroups()).basicRemove(otherEnd, msgs);
			case ComputationPackage.COMPUTATION_MODEL__BLOCKS:
				return ((InternalEList<?>)getBlocks()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ComputationPackage.COMPUTATION_MODEL__PARAMETERS:
				return getParameters();
			case ComputationPackage.COMPUTATION_MODEL__TARGET_DESIGN:
				return getTargetDesign();
			case ComputationPackage.COMPUTATION_MODEL__BLOCK_GROUPS:
				return getBlockGroups();
			case ComputationPackage.COMPUTATION_MODEL__BLOCKS:
				return getBlocks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ComputationPackage.COMPUTATION_MODEL__PARAMETERS:
				getParameters().clear();
				getParameters().addAll((Collection<? extends Parameter>)newValue);
				return;
			case ComputationPackage.COMPUTATION_MODEL__TARGET_DESIGN:
				setTargetDesign((HWTargetDesign)newValue);
				return;
			case ComputationPackage.COMPUTATION_MODEL__BLOCK_GROUPS:
				getBlockGroups().clear();
				getBlockGroups().addAll((Collection<? extends BlockGroup>)newValue);
				return;
			case ComputationPackage.COMPUTATION_MODEL__BLOCKS:
				getBlocks().clear();
				getBlocks().addAll((Collection<? extends ComputationBlock>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ComputationPackage.COMPUTATION_MODEL__PARAMETERS:
				getParameters().clear();
				return;
			case ComputationPackage.COMPUTATION_MODEL__TARGET_DESIGN:
				setTargetDesign((HWTargetDesign)null);
				return;
			case ComputationPackage.COMPUTATION_MODEL__BLOCK_GROUPS:
				getBlockGroups().clear();
				return;
			case ComputationPackage.COMPUTATION_MODEL__BLOCKS:
				getBlocks().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ComputationPackage.COMPUTATION_MODEL__PARAMETERS:
				return parameters != null && !parameters.isEmpty();
			case ComputationPackage.COMPUTATION_MODEL__TARGET_DESIGN:
				return targetDesign != null;
			case ComputationPackage.COMPUTATION_MODEL__BLOCK_GROUPS:
				return blockGroups != null && !blockGroups.isEmpty();
			case ComputationPackage.COMPUTATION_MODEL__BLOCKS:
				return blocks != null && !blocks.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ComputationModelImpl
