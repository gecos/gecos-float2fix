/**
 */
package typeexploration.computation.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import typeexploration.computation.ComputationPackage;
import typeexploration.computation.HWOpType;
import typeexploration.computation.OperandExpression;
import typeexploration.computation.Operation;
import typeexploration.computation.SizeExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.computation.impl.OperationImpl#getOpType <em>Op Type</em>}</li>
 *   <li>{@link typeexploration.computation.impl.OperationImpl#getOutputExpr <em>Output Expr</em>}</li>
 *   <li>{@link typeexploration.computation.impl.OperationImpl#getInputExpr1 <em>Input Expr1</em>}</li>
 *   <li>{@link typeexploration.computation.impl.OperationImpl#getInputExpr2 <em>Input Expr2</em>}</li>
 *   <li>{@link typeexploration.computation.impl.OperationImpl#getNumOps <em>Num Ops</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationImpl extends MinimalEObjectImpl.Container implements Operation {
	/**
	 * The default value of the '{@link #getOpType() <em>Op Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpType()
	 * @generated
	 * @ordered
	 */
	protected static final HWOpType OP_TYPE_EDEFAULT = HWOpType.ADD;

	/**
	 * The cached value of the '{@link #getOpType() <em>Op Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpType()
	 * @generated
	 * @ordered
	 */
	protected HWOpType opType = OP_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOutputExpr() <em>Output Expr</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputExpr()
	 * @generated
	 * @ordered
	 */
	protected OperandExpression outputExpr;

	/**
	 * The cached value of the '{@link #getInputExpr1() <em>Input Expr1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputExpr1()
	 * @generated
	 * @ordered
	 */
	protected OperandExpression inputExpr1;

	/**
	 * The cached value of the '{@link #getInputExpr2() <em>Input Expr2</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputExpr2()
	 * @generated
	 * @ordered
	 */
	protected OperandExpression inputExpr2;

	/**
	 * The cached value of the '{@link #getNumOps() <em>Num Ops</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumOps()
	 * @generated
	 * @ordered
	 */
	protected SizeExpression numOps;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ComputationPackage.Literals.OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HWOpType getOpType() {
		return opType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOpType(HWOpType newOpType) {
		HWOpType oldOpType = opType;
		opType = newOpType == null ? OP_TYPE_EDEFAULT : newOpType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComputationPackage.OPERATION__OP_TYPE, oldOpType, opType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperandExpression getOutputExpr() {
		return outputExpr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOutputExpr(OperandExpression newOutputExpr, NotificationChain msgs) {
		OperandExpression oldOutputExpr = outputExpr;
		outputExpr = newOutputExpr;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ComputationPackage.OPERATION__OUTPUT_EXPR, oldOutputExpr, newOutputExpr);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutputExpr(OperandExpression newOutputExpr) {
		if (newOutputExpr != outputExpr) {
			NotificationChain msgs = null;
			if (outputExpr != null)
				msgs = ((InternalEObject)outputExpr).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ComputationPackage.OPERATION__OUTPUT_EXPR, null, msgs);
			if (newOutputExpr != null)
				msgs = ((InternalEObject)newOutputExpr).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ComputationPackage.OPERATION__OUTPUT_EXPR, null, msgs);
			msgs = basicSetOutputExpr(newOutputExpr, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComputationPackage.OPERATION__OUTPUT_EXPR, newOutputExpr, newOutputExpr));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperandExpression getInputExpr1() {
		return inputExpr1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInputExpr1(OperandExpression newInputExpr1, NotificationChain msgs) {
		OperandExpression oldInputExpr1 = inputExpr1;
		inputExpr1 = newInputExpr1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ComputationPackage.OPERATION__INPUT_EXPR1, oldInputExpr1, newInputExpr1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInputExpr1(OperandExpression newInputExpr1) {
		if (newInputExpr1 != inputExpr1) {
			NotificationChain msgs = null;
			if (inputExpr1 != null)
				msgs = ((InternalEObject)inputExpr1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ComputationPackage.OPERATION__INPUT_EXPR1, null, msgs);
			if (newInputExpr1 != null)
				msgs = ((InternalEObject)newInputExpr1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ComputationPackage.OPERATION__INPUT_EXPR1, null, msgs);
			msgs = basicSetInputExpr1(newInputExpr1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComputationPackage.OPERATION__INPUT_EXPR1, newInputExpr1, newInputExpr1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperandExpression getInputExpr2() {
		return inputExpr2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInputExpr2(OperandExpression newInputExpr2, NotificationChain msgs) {
		OperandExpression oldInputExpr2 = inputExpr2;
		inputExpr2 = newInputExpr2;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ComputationPackage.OPERATION__INPUT_EXPR2, oldInputExpr2, newInputExpr2);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInputExpr2(OperandExpression newInputExpr2) {
		if (newInputExpr2 != inputExpr2) {
			NotificationChain msgs = null;
			if (inputExpr2 != null)
				msgs = ((InternalEObject)inputExpr2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ComputationPackage.OPERATION__INPUT_EXPR2, null, msgs);
			if (newInputExpr2 != null)
				msgs = ((InternalEObject)newInputExpr2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ComputationPackage.OPERATION__INPUT_EXPR2, null, msgs);
			msgs = basicSetInputExpr2(newInputExpr2, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComputationPackage.OPERATION__INPUT_EXPR2, newInputExpr2, newInputExpr2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SizeExpression getNumOps() {
		return numOps;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNumOps(SizeExpression newNumOps, NotificationChain msgs) {
		SizeExpression oldNumOps = numOps;
		numOps = newNumOps;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ComputationPackage.OPERATION__NUM_OPS, oldNumOps, newNumOps);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumOps(SizeExpression newNumOps) {
		if (newNumOps != numOps) {
			NotificationChain msgs = null;
			if (numOps != null)
				msgs = ((InternalEObject)numOps).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ComputationPackage.OPERATION__NUM_OPS, null, msgs);
			if (newNumOps != null)
				msgs = ((InternalEObject)newNumOps).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ComputationPackage.OPERATION__NUM_OPS, null, msgs);
			msgs = basicSetNumOps(newNumOps, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComputationPackage.OPERATION__NUM_OPS, newNumOps, newNumOps));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ComputationPackage.OPERATION__OUTPUT_EXPR:
				return basicSetOutputExpr(null, msgs);
			case ComputationPackage.OPERATION__INPUT_EXPR1:
				return basicSetInputExpr1(null, msgs);
			case ComputationPackage.OPERATION__INPUT_EXPR2:
				return basicSetInputExpr2(null, msgs);
			case ComputationPackage.OPERATION__NUM_OPS:
				return basicSetNumOps(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ComputationPackage.OPERATION__OP_TYPE:
				return getOpType();
			case ComputationPackage.OPERATION__OUTPUT_EXPR:
				return getOutputExpr();
			case ComputationPackage.OPERATION__INPUT_EXPR1:
				return getInputExpr1();
			case ComputationPackage.OPERATION__INPUT_EXPR2:
				return getInputExpr2();
			case ComputationPackage.OPERATION__NUM_OPS:
				return getNumOps();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ComputationPackage.OPERATION__OP_TYPE:
				setOpType((HWOpType)newValue);
				return;
			case ComputationPackage.OPERATION__OUTPUT_EXPR:
				setOutputExpr((OperandExpression)newValue);
				return;
			case ComputationPackage.OPERATION__INPUT_EXPR1:
				setInputExpr1((OperandExpression)newValue);
				return;
			case ComputationPackage.OPERATION__INPUT_EXPR2:
				setInputExpr2((OperandExpression)newValue);
				return;
			case ComputationPackage.OPERATION__NUM_OPS:
				setNumOps((SizeExpression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ComputationPackage.OPERATION__OP_TYPE:
				setOpType(OP_TYPE_EDEFAULT);
				return;
			case ComputationPackage.OPERATION__OUTPUT_EXPR:
				setOutputExpr((OperandExpression)null);
				return;
			case ComputationPackage.OPERATION__INPUT_EXPR1:
				setInputExpr1((OperandExpression)null);
				return;
			case ComputationPackage.OPERATION__INPUT_EXPR2:
				setInputExpr2((OperandExpression)null);
				return;
			case ComputationPackage.OPERATION__NUM_OPS:
				setNumOps((SizeExpression)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ComputationPackage.OPERATION__OP_TYPE:
				return opType != OP_TYPE_EDEFAULT;
			case ComputationPackage.OPERATION__OUTPUT_EXPR:
				return outputExpr != null;
			case ComputationPackage.OPERATION__INPUT_EXPR1:
				return inputExpr1 != null;
			case ComputationPackage.OPERATION__INPUT_EXPR2:
				return inputExpr2 != null;
			case ComputationPackage.OPERATION__NUM_OPS:
				return numOps != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (opType: ");
		result.append(opType);
		result.append(')');
		return result.toString();
	}

} //OperationImpl
