/**
 */
package typeexploration.computation.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import typeexploration.computation.ComputationPackage;
import typeexploration.computation.HWTargetDesign;
import typeexploration.computation.SizeExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HW Target Design</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.computation.impl.HWTargetDesignImpl#getFrequency <em>Frequency</em>}</li>
 *   <li>{@link typeexploration.computation.impl.HWTargetDesignImpl#getOutputsPerCycle <em>Outputs Per Cycle</em>}</li>
 *   <li>{@link typeexploration.computation.impl.HWTargetDesignImpl#getProblemSizeExpr <em>Problem Size Expr</em>}</li>
 *   <li>{@link typeexploration.computation.impl.HWTargetDesignImpl#getTotalLatency <em>Total Latency</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HWTargetDesignImpl extends MinimalEObjectImpl.Container implements HWTargetDesign {
	/**
	 * The default value of the '{@link #getFrequency() <em>Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrequency()
	 * @generated
	 * @ordered
	 */
	protected static final int FREQUENCY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getFrequency() <em>Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrequency()
	 * @generated
	 * @ordered
	 */
	protected int frequency = FREQUENCY_EDEFAULT;

	/**
	 * The default value of the '{@link #getOutputsPerCycle() <em>Outputs Per Cycle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputsPerCycle()
	 * @generated
	 * @ordered
	 */
	protected static final int OUTPUTS_PER_CYCLE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getOutputsPerCycle() <em>Outputs Per Cycle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputsPerCycle()
	 * @generated
	 * @ordered
	 */
	protected int outputsPerCycle = OUTPUTS_PER_CYCLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getProblemSizeExpr() <em>Problem Size Expr</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProblemSizeExpr()
	 * @generated
	 * @ordered
	 */
	protected SizeExpression problemSizeExpr;

	/**
	 * The default value of the '{@link #getTotalLatency() <em>Total Latency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotalLatency()
	 * @generated
	 * @ordered
	 */
	protected static final double TOTAL_LATENCY_EDEFAULT = 0.0;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HWTargetDesignImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ComputationPackage.Literals.HW_TARGET_DESIGN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getFrequency() {
		return frequency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFrequency(int newFrequency) {
		int oldFrequency = frequency;
		frequency = newFrequency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComputationPackage.HW_TARGET_DESIGN__FREQUENCY, oldFrequency, frequency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getOutputsPerCycle() {
		return outputsPerCycle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutputsPerCycle(int newOutputsPerCycle) {
		int oldOutputsPerCycle = outputsPerCycle;
		outputsPerCycle = newOutputsPerCycle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComputationPackage.HW_TARGET_DESIGN__OUTPUTS_PER_CYCLE, oldOutputsPerCycle, outputsPerCycle));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SizeExpression getProblemSizeExpr() {
		return problemSizeExpr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProblemSizeExpr(SizeExpression newProblemSizeExpr, NotificationChain msgs) {
		SizeExpression oldProblemSizeExpr = problemSizeExpr;
		problemSizeExpr = newProblemSizeExpr;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ComputationPackage.HW_TARGET_DESIGN__PROBLEM_SIZE_EXPR, oldProblemSizeExpr, newProblemSizeExpr);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProblemSizeExpr(SizeExpression newProblemSizeExpr) {
		if (newProblemSizeExpr != problemSizeExpr) {
			NotificationChain msgs = null;
			if (problemSizeExpr != null)
				msgs = ((InternalEObject)problemSizeExpr).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ComputationPackage.HW_TARGET_DESIGN__PROBLEM_SIZE_EXPR, null, msgs);
			if (newProblemSizeExpr != null)
				msgs = ((InternalEObject)newProblemSizeExpr).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ComputationPackage.HW_TARGET_DESIGN__PROBLEM_SIZE_EXPR, null, msgs);
			msgs = basicSetProblemSizeExpr(newProblemSizeExpr, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComputationPackage.HW_TARGET_DESIGN__PROBLEM_SIZE_EXPR, newProblemSizeExpr, newProblemSizeExpr));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getTotalLatency() {
		double _xblockexpression = (double) 0;
		{
			double _doubleValue = Long.valueOf(this.getProblemSizeExpr().evaluate()).doubleValue();
			double _doubleValue_1 = Integer.valueOf(this.getOutputsPerCycle()).doubleValue();
			final double numCycles = (_doubleValue / _doubleValue_1);
			int _frequency = this.getFrequency();
			final double period = (1000.0 / _frequency);
			_xblockexpression = (period * numCycles);
		}
		return _xblockexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ComputationPackage.HW_TARGET_DESIGN__PROBLEM_SIZE_EXPR:
				return basicSetProblemSizeExpr(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ComputationPackage.HW_TARGET_DESIGN__FREQUENCY:
				return getFrequency();
			case ComputationPackage.HW_TARGET_DESIGN__OUTPUTS_PER_CYCLE:
				return getOutputsPerCycle();
			case ComputationPackage.HW_TARGET_DESIGN__PROBLEM_SIZE_EXPR:
				return getProblemSizeExpr();
			case ComputationPackage.HW_TARGET_DESIGN__TOTAL_LATENCY:
				return getTotalLatency();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ComputationPackage.HW_TARGET_DESIGN__FREQUENCY:
				setFrequency((Integer)newValue);
				return;
			case ComputationPackage.HW_TARGET_DESIGN__OUTPUTS_PER_CYCLE:
				setOutputsPerCycle((Integer)newValue);
				return;
			case ComputationPackage.HW_TARGET_DESIGN__PROBLEM_SIZE_EXPR:
				setProblemSizeExpr((SizeExpression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ComputationPackage.HW_TARGET_DESIGN__FREQUENCY:
				setFrequency(FREQUENCY_EDEFAULT);
				return;
			case ComputationPackage.HW_TARGET_DESIGN__OUTPUTS_PER_CYCLE:
				setOutputsPerCycle(OUTPUTS_PER_CYCLE_EDEFAULT);
				return;
			case ComputationPackage.HW_TARGET_DESIGN__PROBLEM_SIZE_EXPR:
				setProblemSizeExpr((SizeExpression)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ComputationPackage.HW_TARGET_DESIGN__FREQUENCY:
				return frequency != FREQUENCY_EDEFAULT;
			case ComputationPackage.HW_TARGET_DESIGN__OUTPUTS_PER_CYCLE:
				return outputsPerCycle != OUTPUTS_PER_CYCLE_EDEFAULT;
			case ComputationPackage.HW_TARGET_DESIGN__PROBLEM_SIZE_EXPR:
				return problemSizeExpr != null;
			case ComputationPackage.HW_TARGET_DESIGN__TOTAL_LATENCY:
				return getTotalLatency() != TOTAL_LATENCY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (frequency: ");
		result.append(frequency);
		result.append(", outputsPerCycle: ");
		result.append(outputsPerCycle);
		result.append(')');
		return result.toString();
	}

} //HWTargetDesignImpl
