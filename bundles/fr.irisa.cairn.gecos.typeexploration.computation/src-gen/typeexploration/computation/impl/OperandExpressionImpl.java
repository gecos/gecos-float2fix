/**
 */
package typeexploration.computation.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import typeexploration.computation.ComputationPackage;
import typeexploration.computation.OperandExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operand Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class OperandExpressionImpl extends MinimalEObjectImpl.Container implements OperandExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperandExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ComputationPackage.Literals.OPERAND_EXPRESSION;
	}

} //OperandExpressionImpl
