/**
 */
package typeexploration.computation.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.emf.ecore.xcore.lib.XcoreEListExtensions;

import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.Functions.Function2;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

import typeexploration.computation.ComputationPackage;
import typeexploration.computation.Parameter;
import typeexploration.computation.ProductExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Product Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.computation.impl.ProductExpressionImpl#getConstant <em>Constant</em>}</li>
 *   <li>{@link typeexploration.computation.impl.ProductExpressionImpl#getTerms <em>Terms</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProductExpressionImpl extends SizeExpressionImpl implements ProductExpression {
	/**
	 * The default value of the '{@link #getConstant() <em>Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstant()
	 * @generated
	 * @ordered
	 */
	protected static final int CONSTANT_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getConstant() <em>Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstant()
	 * @generated
	 * @ordered
	 */
	protected int constant = CONSTANT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTerms() <em>Terms</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTerms()
	 * @generated
	 * @ordered
	 */
	protected EList<Parameter> terms;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProductExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ComputationPackage.Literals.PRODUCT_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getConstant() {
		return constant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstant(int newConstant) {
		int oldConstant = constant;
		constant = newConstant;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComputationPackage.PRODUCT_EXPRESSION__CONSTANT, oldConstant, constant));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Parameter> getTerms() {
		if (terms == null) {
			terms = new EObjectResolvingEList<Parameter>(Parameter.class, this, ComputationPackage.PRODUCT_EXPRESSION__TERMS);
		}
		return terms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long evaluate() {
		Integer _xblockexpression = null;
		{
			final Function1<Parameter, Integer> _function = new Function1<Parameter, Integer>() {
				public Integer apply(final Parameter t) {
					return Integer.valueOf(t.getValue());
				}
			};
			final Function2<Integer, Integer, Integer> _function_1 = new Function2<Integer, Integer, Integer>() {
				public Integer apply(final Integer p1, final Integer p2) {
					return Integer.valueOf(((p1).intValue() * (p2).intValue()));
				}
			};
			final Integer tmp = IterableExtensions.<Integer>reduce(XcoreEListExtensions.<Parameter, Integer>map(this.getTerms(), _function), _function_1);
			Integer _xifexpression = null;
			int _constant = this.getConstant();
			boolean _notEquals = (_constant != 0);
			if (_notEquals) {
				int _constant_1 = this.getConstant();
				_xifexpression = Integer.valueOf(((tmp).intValue() * _constant_1));
			}
			else {
				_xifexpression = tmp;
			}
			_xblockexpression = _xifexpression;
		}
		return (_xblockexpression).intValue();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ComputationPackage.PRODUCT_EXPRESSION__CONSTANT:
				return getConstant();
			case ComputationPackage.PRODUCT_EXPRESSION__TERMS:
				return getTerms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ComputationPackage.PRODUCT_EXPRESSION__CONSTANT:
				setConstant((Integer)newValue);
				return;
			case ComputationPackage.PRODUCT_EXPRESSION__TERMS:
				getTerms().clear();
				getTerms().addAll((Collection<? extends Parameter>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ComputationPackage.PRODUCT_EXPRESSION__CONSTANT:
				setConstant(CONSTANT_EDEFAULT);
				return;
			case ComputationPackage.PRODUCT_EXPRESSION__TERMS:
				getTerms().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ComputationPackage.PRODUCT_EXPRESSION__CONSTANT:
				return constant != CONSTANT_EDEFAULT;
			case ComputationPackage.PRODUCT_EXPRESSION__TERMS:
				return terms != null && !terms.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ComputationPackage.PRODUCT_EXPRESSION___EVALUATE:
				return evaluate();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (constant: ");
		result.append(constant);
		result.append(')');
		return result.toString();
	}

} //ProductExpressionImpl
