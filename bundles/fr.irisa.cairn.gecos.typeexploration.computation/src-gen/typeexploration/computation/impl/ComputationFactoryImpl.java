/**
 */
package typeexploration.computation.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import typeexploration.computation.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ComputationFactoryImpl extends EFactoryImpl implements ComputationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ComputationFactory init() {
		try {
			ComputationFactory theComputationFactory = (ComputationFactory)EPackage.Registry.INSTANCE.getEFactory(ComputationPackage.eNS_URI);
			if (theComputationFactory != null) {
				return theComputationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ComputationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComputationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ComputationPackage.COMPUTATION_MODEL: return createComputationModel();
			case ComputationPackage.PARAMETER: return createParameter();
			case ComputationPackage.HW_TARGET_DESIGN: return createHWTargetDesign();
			case ComputationPackage.BLOCK_GROUP: return createBlockGroup();
			case ComputationPackage.COMPUTATION_BLOCK: return createComputationBlock();
			case ComputationPackage.OPERATION: return createOperation();
			case ComputationPackage.MAX_EXPRESSION: return createMaxExpression();
			case ComputationPackage.ADD_EXPRESSION: return createAddExpression();
			case ComputationPackage.OPERAND_TERM: return createOperandTerm();
			case ComputationPackage.SUMMATION_EXPRESSION: return createSummationExpression();
			case ComputationPackage.PRODUCT_EXPRESSION: return createProductExpression();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ComputationPackage.HW_OP_TYPE:
				return createHWOpTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ComputationPackage.HW_OP_TYPE:
				return convertHWOpTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComputationModel createComputationModel() {
		ComputationModelImpl computationModel = new ComputationModelImpl();
		return computationModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Parameter createParameter() {
		ParameterImpl parameter = new ParameterImpl();
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HWTargetDesign createHWTargetDesign() {
		HWTargetDesignImpl hwTargetDesign = new HWTargetDesignImpl();
		return hwTargetDesign;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlockGroup createBlockGroup() {
		BlockGroupImpl blockGroup = new BlockGroupImpl();
		return blockGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComputationBlock createComputationBlock() {
		ComputationBlockImpl computationBlock = new ComputationBlockImpl();
		return computationBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation createOperation() {
		OperationImpl operation = new OperationImpl();
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaxExpression createMaxExpression() {
		MaxExpressionImpl maxExpression = new MaxExpressionImpl();
		return maxExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AddExpression createAddExpression() {
		AddExpressionImpl addExpression = new AddExpressionImpl();
		return addExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperandTerm createOperandTerm() {
		OperandTermImpl operandTerm = new OperandTermImpl();
		return operandTerm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SummationExpression createSummationExpression() {
		SummationExpressionImpl summationExpression = new SummationExpressionImpl();
		return summationExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProductExpression createProductExpression() {
		ProductExpressionImpl productExpression = new ProductExpressionImpl();
		return productExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HWOpType createHWOpTypeFromString(EDataType eDataType, String initialValue) {
		HWOpType result = HWOpType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertHWOpTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComputationPackage getComputationPackage() {
		return (ComputationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ComputationPackage getPackage() {
		return ComputationPackage.eINSTANCE;
	}

} //ComputationFactoryImpl
