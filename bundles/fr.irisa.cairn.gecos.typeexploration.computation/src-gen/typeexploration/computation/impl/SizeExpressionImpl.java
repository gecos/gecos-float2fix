/**
 */
package typeexploration.computation.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import typeexploration.computation.ComputationPackage;
import typeexploration.computation.SizeExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Size Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class SizeExpressionImpl extends MinimalEObjectImpl.Container implements SizeExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SizeExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ComputationPackage.Literals.SIZE_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long evaluate() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ComputationPackage.SIZE_EXPRESSION___EVALUATE:
				return evaluate();
		}
		return super.eInvoke(operationID, arguments);
	}

} //SizeExpressionImpl
