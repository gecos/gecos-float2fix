/**
 */
package typeexploration.computation;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link typeexploration.computation.ComputationModel#getParameters <em>Parameters</em>}</li>
 *   <li>{@link typeexploration.computation.ComputationModel#getTargetDesign <em>Target Design</em>}</li>
 *   <li>{@link typeexploration.computation.ComputationModel#getBlockGroups <em>Block Groups</em>}</li>
 *   <li>{@link typeexploration.computation.ComputationModel#getBlocks <em>Blocks</em>}</li>
 * </ul>
 *
 * @see typeexploration.computation.ComputationPackage#getComputationModel()
 * @model
 * @generated
 */
public interface ComputationModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link typeexploration.computation.Parameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference list.
	 * @see typeexploration.computation.ComputationPackage#getComputationModel_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<Parameter> getParameters();

	/**
	 * Returns the value of the '<em><b>Target Design</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Design</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Design</em>' containment reference.
	 * @see #setTargetDesign(HWTargetDesign)
	 * @see typeexploration.computation.ComputationPackage#getComputationModel_TargetDesign()
	 * @model containment="true"
	 * @generated
	 */
	HWTargetDesign getTargetDesign();

	/**
	 * Sets the value of the '{@link typeexploration.computation.ComputationModel#getTargetDesign <em>Target Design</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Design</em>' containment reference.
	 * @see #getTargetDesign()
	 * @generated
	 */
	void setTargetDesign(HWTargetDesign value);

	/**
	 * Returns the value of the '<em><b>Block Groups</b></em>' containment reference list.
	 * The list contents are of type {@link typeexploration.computation.BlockGroup}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Block Groups</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Block Groups</em>' containment reference list.
	 * @see typeexploration.computation.ComputationPackage#getComputationModel_BlockGroups()
	 * @model containment="true"
	 * @generated
	 */
	EList<BlockGroup> getBlockGroups();

	/**
	 * Returns the value of the '<em><b>Blocks</b></em>' containment reference list.
	 * The list contents are of type {@link typeexploration.computation.ComputationBlock}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Blocks</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Blocks</em>' containment reference list.
	 * @see typeexploration.computation.ComputationPackage#getComputationModel_Blocks()
	 * @model containment="true"
	 * @generated
	 */
	EList<ComputationBlock> getBlocks();

} // ComputationModel
