variables:
  # include git submodule (only at top-level)
  # to include all submodules recursively use 'recursive'
  GIT_SUBMODULE_STRATEGY: normal

  # shell environment variable to specify timezone (necessary for Maven)
  TZ: "UTC-2"

  # Maven options
  # use '/cache/.m2/repository" as local maven repository.
  # '/cache' is automatically added as volume for docker executors
  # to use another cachedir it need to be added to the docker
  # executor's 'volumes' parameter (in config.toml)
  MAVEN_OPTS: "-Dmaven.repo.local=/cache/.m2/repository -Xmx3072m"

  # Uncomment to enable debug traces
  # !!! WARNING !!! 
  #     This will expose secret variables !
  #CI_DEBUG_TRACE: "true"

  # Update site (p2 repository) base URL
  UPSITE_URL: "http://gecos.gforge.inria.fr/updatesite/gecos"

  # Update site artifacts storage dir (local) 
  UPSITE_ARTIFACTS_DIR: "artifacts__updatesite"


# default docker image. can be overriden per job.
image: maven:3-jdk-8


# Trigger build on Jenkins and wait for it to finish
# then, if successful, dowload its artifacts
# !!! secret variables 'JENKINS_USER' and 'JENKINS_TOKEN' must be defined
.jenkins_build: &jenkins_build
  before_script:
    - gecos-buildtools/scripts/build_on_jenkins.sh "${JENKINS_USER}" "${JENKINS_TOKEN}" "${CI_PROJECT_NAME}"
      "${CI_COMMIT_SHA}" "${JENKINS_FAIL_MODE}" "${CI_COMMIT_REF_NAME}" "${UPSITE_ARTIFACTS_DIR}"

build:
  <<: *jenkins_build
  stage: build
  script:
    - ls ${UPSITE_ARTIFACTS_DIR}
  artifacts:
    paths:
      - "${UPSITE_ARTIFACTS_DIR}"


# Install ssh, run ssh agent and add gecos ci bot key in 'before_script'.
# Make sure it is running using a docker executor.
# !!! WARNING !!!
#   The gecos-bot-ci private ssh key is stored in a gitlab secret varibale, however
#   its value is passed as an environment variable to the build environment
#   i.e. it might be plainly visible in the build console output!
#   !!! DO NOT give public access to pipelines (in Project Settings -> General) !!!
.init_ssh: &init_ssh
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - mkdir -p ~/.ssh
    - echo "${CI_BOT_SSH_PRIVATE_KEY}" > ~/.ssh/id_rsa
    - chmod 600 ~/.ssh/id_rsa
    - ssh-add ~/.ssh/id_rsa
#    - '[[ -f /.dockerenv ]] && echo "${GFORGE_SSH_SERVER_HOSTKEYS}" > ~/.ssh/known_hosts'
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - git config --global user.email "gecos-ci-bot@users.gforge.inria.fr"
    - git config --global user.name "gecos-ci-bot"


# Perform deploy to ${UPSITE_SSH_HOSTNAME} using ssh in 'script'
# Require variables: 
# - ENVIRONMENT_NAME: used to specify special actions per environment..
#      Supported values are: "testing", "release" and "snapshot".
#      !!! This is different from predefined ${CI_ENVIRONMENT_NAME}.
.deploy: &deploy
  <<: *init_ssh
  stage: deploy
  # allow artifacts from 'build' job to be downloaded at the begining of 'deploy' job
  dependencies: 
    - build
  script:
    # locate update site zip (archived by 'build' job)
    # fail if not found
    - UPSITE_ARTIFACT="`find "${UPSITE_ARTIFACTS_DIR}" -maxdepth 1 -name "*.zip" | head -1`"
    - gecos-buildtools/scripts/deploy.sh "${UPSITE_ARTIFACT}" "${ENVIRONMENT_NAME}"
      "${CI_COMMIT_SHA}" "${CI_COMMIT_REF_NAME}" "${CI_PROJECT_NAME}"
# Create environment:
# Currently using user variables are not supported for name and url in 'environment'
# This is a workarround using Gitlab API
#    - apt-get install curl
#    - export timestamp=`echo "${localArtifactFile}" | sed "s/^.*-\([0-9]*\).zip$/\1/"`
#    - deployName="${ENVIRONMENT_NAME}/${timestamp}"
#    - deployUrl="${UPSITE_URL}/$(echo ${CI_PROJECT_NAME} | sed 's/^gecos-//')/${deployName}"
#    - 'echo "[INFO] Creating environment: name=${deployName}  external_url=${deployUrl}"'
#    - 'curl -s --data "name=${deployName}&external_url=${deployUrl}" --header "PRIVATE-TOKEN: ${CI_JOB_TOKEN}" "https://gitlab.inria.fr/api/v4/projects/${CI_PROJECT_ID}/environments"'


# Deploy to testing updatesite (manually)
# triggered from the GUI for every commit (no restrictions)
deploy:testing:
  <<: *deploy
  variables:
    ENVIRONMENT_NAME: "testing"
  when: manual
  environment:
    # !!! ${timestamp} cannot be used here (not properly expanded)
    #     only predefined varibales can be currently used
    name: "testing" #"testing/${timestamp}"
    # such variable expansion is currently not supported
    url: "${UPSITE_URL}/$(echo ${CI_PROJECT_NAME} | sed 's/^gecos-//')/testing" #"${UPSITE_URL}/testing/${timestamp}"


# Deploy to snapshot updatesite (manually)
# triggered from the GUI for every commit to 'develop'
deploy:snapshot:
  <<: *deploy
  variables:
    ENVIRONMENT_NAME: "snapshot"
  environment:
    name: "snapshot" #"snapshot/${timestamp}"
    url: "${UPSITE_URL}/$(echo ${CI_PROJECT_NAME} | sed 's/^gecos-//')/snapshot" #"${UPSITE_URL}/snapshot/${timestamp}"
  when: manual
  only:
    - develop


# Deploy to release updatesite (manually)
# triggered from the GUI only for 'tags' to 'master'
deploy:release:
  <<: *deploy
  variables:
    ENVIRONMENT_NAME: "release"
  environment:
    name: "release/${CI_COMMIT_TAG}"
    url: "${UPSITE_URL}/$(echo ${CI_PROJECT_NAME} | sed 's/^gecos-//')/release/${CI_COMMIT_TAG}" #! ${CI_COMMIT_TAG} is only defined when building tags 
  when: manual
  only:
    - /^(v|V|ver|Ver|version|Version)?-?[0-9]+\.[0-9]+\.[0-9]+[[:print:]]*$/
  except:
    - branches


