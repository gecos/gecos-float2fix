This repository contains multiple projects related to float-to-fix converstion.
The following is about building and running ```typeexploration.*``` projects, initially written by Antoine Morvan.

Gecos Type Exploration Flow
===========================

Objectives : 
* build Gecos float2fix source from sources in Eclipse and run it;
    * See [this page](https://gitlab.inria.fr/gecos/gecos-core/wikis/home) for installing from Update Site;
* User documentation is available here [bundles/fr.irisa.cairn.gecos.typeexploration/USER_GUIDE.md](bundles/fr.irisa.cairn.gecos.typeexploration/USER_GUIDE.md);
* Demos are located in the [fr.irisa.cairn.gecos.typeexploration.demo](bundles/fr.irisa.cairn.gecos.typeexploration.demo) project; 
    * If proper plugins are exported Eclipse Wizard is also available (New / Example... / Gecos / GeCoS Type Exporation);

## Building the Flow from Source

### Requirements

Make sure to have Java 8+ and Maven 3.5.0+ installed on your system. Building Maven projects and installing Eclipse plugins requires Internet connection.

### Download Required Projects from Git Repository

From https://gitlab.inria.fr/gecos, clone following repositories:
* gecos-builtools
* gecos-core
* gecos-float2fix
* gecos-framework
* gecos-testframework
* under GeCoS Tools:
    * gecos-tools-emf
    * gecos-tools-graph
    * gecos-tools-tommapping
    * gecos-tools-tomsdk

![](https://i.imgur.com/qAIcxVx.png)

### Install Parent Project in Local M2 repo

In order to prevent errors in later Maven resolving steps when building with Eclipse, it is necessary to first install the **gecos-buildtools** project in the local Maven repository. To do so, open a terminal in the **gecos-buildtools** and run **mvn clean install**

```
$ cd ~/git/gecos-buildtools/
$ mvn clean install
```

### Setup Eclipse and required Plugins

In order to build Gecos F2F in Eclipse, you need a set of extra plugins to support Xtext and Xtend.
1. Downlad Eclipse Modeling Tools from packages list at https://www.eclipse.org/downloads/packages/release/2018-12/r/eclipse-modeling-tools
2. Run Eclipse and select an empty workspace
3. Help / Install New Software...
4. Select repository "2018-12 - http://download.eclipse.org/releases/2018-12"
5. Select the following features
    * Programming Languages / C/C++ Development Tools SDK
    * Modeling / Eclipse Modeling Framework Xcore SDK
    * Modeling / MWE 2 runtime SDK
    * Modeling / MWE 2 language SDK
6. Click on Next then follow procedure then restart Eclipse when asked

### Import Projects and Build

After Eclipse restart, import and build projects:
1. File / Import ...
2. General / Existing Projects into Workspace
3. Select root directory / select the git folder where all the git repositories are
4. Make sur the box "Search for nested projects" is ticked
5. In the list of projects, unselect the following projects:
    * gecos-buildtools
    * gecos.ci.jenkins.api
    * test_basenlm
6. Click on Finish and wait for the build process to terminate
7. Some error should appear because of missing resources. Simply create the folders on the projects:
    * fr.irisa.cairn.gecos.testframework.resources/src
    * fr.irisa.cairn.gecos.model/xtend-gen
    * fr.irisa.cairn.eclipse.tom/src
8. If some errors are still present, click on Project / Clean ... and clean all projets.

### Run Gecos F2F

1. The exploration tool is invoked by Compiler Script commands
2. Command TypesExploration takes a GecosProject and a path to property file
3. Run TypesExploration(".") to generate a default property file
4. See 'fr.irisa.cairn.gecos.typeexploration.demo/demos/conv/' for an example
