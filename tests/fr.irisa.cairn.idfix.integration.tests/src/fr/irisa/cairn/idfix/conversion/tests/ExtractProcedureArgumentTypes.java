package fr.irisa.cairn.idfix.conversion.tests;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;
import com.google.common.io.Files;

import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.c.generator.ExtendableTypeCGenerator;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;

public class ExtractProcedureArgumentTypes {

	public static String ACFIXED_ANNOTATION_KEY = "ac_fixed";
	public static String PARAM_TYPE_PREFIX = "TYPE_";
	public static String MAKEFILE_CONFIG_NAME = "makefile.cfg";
	public static String TYPEDEF_HEADERFILE_NAME = "defs.h";
	
	private File makefileConfig;
	private File typeDefFile;
	private String floatType;
	private String procName;
	private Procedure proc;
	

	public ExtractProcedureArgumentTypes(GecosProject proj, String procName, String outDir, String float_type) {
		this(findProc(proj.listProcedureSets(), procName, outDir, float_type), outDir, float_type);
	}
	
	public ExtractProcedureArgumentTypes (ProcedureSet ps, String procName, String outDir, String float_type) {
		this(findProc(Lists.newArrayList(ps), procName, outDir, float_type), outDir, float_type);
	}
	
	private static Procedure findProc(Collection<ProcedureSet> procedureSets, String procName, String outDir, String float_type) {
		for(ProcedureSet ps : procedureSets) {
			Procedure findProcedure = ps.findProcedure(procName);
			if(findProcedure != null) {
				return findProcedure;
			}
		}
		throw new RuntimeException("Couldn't find procedure: " + procName);
	}			
	
	public ExtractProcedureArgumentTypes(Procedure p, String outDir, String float_type) {
		this.proc = p;
		this.procName = proc.getSymbol().getName();
		this.floatType = float_type;
		this.typeDefFile = new File(outDir, TYPEDEF_HEADERFILE_NAME);
		this.makefileConfig = new File(outDir, MAKEFILE_CONFIG_NAME);
	}
	
	public void compute() {
		try {
			generateTypeDefFile();
			generateMakefileConfig();
		} catch (Exception e) {
			System.err.println("ExtractProcedureArgumentTypes failed for: " + procName);
			e.printStackTrace();
		}
	}
	
	private void generateMakefileConfig() throws IOException {
		List<String> f2fxParamsList = new ArrayList<String>();
		for(ParameterSymbol param : proc.listParameters()) {
			if(param.getPragma() != null) {
				String f2fxparam = "";
				for(String prag : param.getPragma().getContent()) {
					if(prag.startsWith(ACFIXED_ANNOTATION_KEY )) {
						String[] parts = prag.substring(ACFIXED_ANNOTATION_KEY.length()+1).split(",");
						int wd = Integer.parseInt(parts[0]);
						String wip = parts[1];
						boolean signed = parts[2].equals("true");
						
						f2fxparam = floatType + " " + (!signed ? "u" : "") + (wd == 8? "char" : (wd == 16? "short" : "int")) + " " + wip; 
						
						System.out.println(f2fxparam);
						break;
					}
				}
				if(!f2fxparam.isEmpty())
					f2fxParamsList.add(f2fxparam);
			}
		}
		makefileConfig.createNewFile();
		
		//XXX custom
		if(procName.equals("fft") || procName.equals("ifft")) {
			Files.write ("_INPUT_1_FL2FX := " + f2fxParamsList.get(0) + "\n", makefileConfig, StandardCharsets.UTF_8);
			Files.append("_INPUT_2_FL2FX := " + f2fxParamsList.get(1) + "\n", makefileConfig, StandardCharsets.UTF_8);
			Files.append("_OUTPUT_FL2FX := "  + f2fxParamsList.get(2) + " " + f2fxParamsList.get(2) + "\n", makefileConfig, StandardCharsets.UTF_8);
		}
		else {
			Files.write ("_INPUT_1_FL2FX := " + f2fxParamsList.get(0) + "\n", makefileConfig, StandardCharsets.UTF_8);
			Files.append("_OUTPUT_FL2FX := "  + f2fxParamsList.get(1) + "\n", makefileConfig, StandardCharsets.UTF_8);
		}
	}
	
	private void generateTypeDefFile() throws FileNotFoundException, UnsupportedEncodingException {
		PrintWriter defsWriter = new PrintWriter(typeDefFile , "UTF-8");
		int paramNumber = 0;
		
		for(ParameterSymbol param : proc.listParameters()) {
			TypeAnalyzer ta = new TypeAnalyzer(param.getType());
			if(ta.isBase())
				continue;
			
			String s = "#define   " + PARAM_TYPE_PREFIX+(paramNumber++) + "   " 
					+ ExtendableTypeCGenerator.eInstance.generate(ta.getBaseLevel().getAlias() == null ? ta.getBase() : ta.getBaseLevel().getAlias());
			defsWriter.println(s);
		}
		defsWriter.close();
	}
}