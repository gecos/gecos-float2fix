package fr.irisa.cairn.idfix.conversion.tests;

import static fr.irisa.cairn.gecos.core.testframework.impl.GSOperators.customDataConvertor;
import static fr.irisa.cairn.gecos.core.testframework.impl.GSOperators.versionCopier;
import static fr.irisa.cairn.gecos.core.testframework.utils.GSProjectUtils.setProjectSourceFiles;
import static fr.irisa.cairn.gecos.testframework.s2s.Operators.defaultExecutor;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.CHECK;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.CONVERT;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.RUN;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.TRANSFORM;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.NOP;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.chain;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.convert;
import static fr.irisa.cairn.gecos.testframework.stages.Stages.forEach;
import static fr.irisa.cairn.gecos.testframework.utils.OperationUtils.filter;
import static java.util.Arrays.asList;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import org.junit.Ignore;
import org.junit.Test;

import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.core.testframework.impl.GSProjectVersion;
import fr.irisa.cairn.gecos.core.testframework.impl.GecosS2STestTemplate;
import fr.irisa.cairn.gecos.testframework.data.CxxFilesGroupData;
import fr.irisa.cairn.gecos.testframework.data.ICxxProjectData;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataFromPathProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.ResourcesLocation;
import fr.irisa.cairn.gecos.testframework.model.ITestStage;
import fr.irisa.cairn.gecos.testframework.model.IVersion;
import fr.irisa.cairn.gecos.testframework.model.IVersionBiOperator;
import fr.irisa.cairn.gecos.testframework.model.IVersionFactory;
import fr.irisa.cairn.gecos.testframework.s2s.Comparators;
import fr.irisa.cairn.gecos.testframework.stages.Stages;
import fr.irisa.cairn.gecos.testframework.utils.ResourceLocator;
import fr.irisa.cairn.idfix.model.factory.IDFixUserIDFixProjectFactory;
import fr.irisa.cairn.idfix.model.idfixproject.IdfixProject;
import fr.irisa.cairn.idfix.model.userconfiguration.DATA_TYPE_REGENERATION;
import fr.irisa.cairn.idfix.model.userconfiguration.UserConfiguration;
import fr.irisa.cairn.idfix.simulation.noiseevaluation.utils.result.ErrorStatsCalculator;
import fr.irisa.cairn.idfix.transforms.Float2FixConversion;
import fr.irisa.r2d2.gecos.framework.utils.FileUtils;
import gecos.gecosproject.GecosProject;

/**
 * Tests {@link Float2FixConversion} 
 * 
 * @author aelmouss
 */
public class Float2FixCodeGenIT extends GecosS2STestTemplate<GSProjectVersion> {

	private static final String IDFIX_TMP_OUTPUT = "./target/idfix-tmp";
	private static final String IDFIX_CONFIG_FILE = "./resources/idfix.cfg";
	private static final String OP_LIBRARY = "./resources/opLib_8-16-32.xml";
	private static final double ACC_CONST_VIOLATION_TOLERANCE = 2; 

	/* Options */
	private int wloAlgo = 4;
	private double accConstraint = -40;
	private DATA_TYPE_REGENERATION codegenType;
	
	//XXX
	private static ResourceLocator locator = new ResourceLocator(Float2FixCodeGenIT.class, "resources/inputs");
	private static List<Path> inputFiles = Arrays.asList(
		locator.locate("resources/inputs/1d/float/normalized/lena_32x32_norm.input"),
		locator.locate("resources/inputs/1d/float/normalized/sounds/bass1024.input")
	);	
	
	protected Path getOutputFile(IVersion v) {
		return v.getOutputDir().resolve(v.getName() + ".output");
	}
	
	private String getOutputFile(Path i, IVersion v) {
		return v.getOutputDir().resolve(i.getFileName()).toString() + ".output";
	}
	
	@Override
	protected void configure() {
		super.configure();
		
		IVersionBiOperator<GSProjectVersion> comparator = Comparators.regularFileComparator(this::getOutputFile, 
			(fi, f0) -> {
				double[] floatValues = loadData(f0);
				double[] fixedValues = loadData(fi);
				
				ErrorStatsCalculator stats = new ErrorStatsCalculator(floatValues, fixedValues);
				stats.compute();
				
				System.out.println("Accuracy Constraint: " + accConstraint + " dB");
				System.out.println(stats.generateReport());
				
				if(stats.getNoisePowerdB() - accConstraint > ACC_CONST_VIOLATION_TOLERANCE) {
					System.err.println("Noise Power (dB) = " + stats.getNoisePowerdB() + " exceds the specified constraint " + accConstraint);
					return false;
				}
				
				if(FileUtils.areFilesEquals(fi.toFile(), f0.toFile()))
					System.out.println("\t> diff: Files are identical.");
				
				return true;
			}
		);
		
		IVersionFactory<GSProjectVersion> versionFactory = GSProjectVersion::new;
		Predicate<Path> sourceFilter = s -> !s.getFileName().toString().startsWith("main.");
		
		findTestFlow(CxxFilesGroupData.class)
			.replaceStage(CONVERT, convert(customDataConvertor(versionFactory,
					(data, proj) -> setProjectSourceFiles(proj, filter(data.getSourceFiles(), sourceFilter)), 
					(data, v) -> v.setExternalSources(filter(data.getSourceFiles(), sourceFilter.negate())))))
			.replaceStage(TRANSFORM, Stages.transform(versionCopier(versionFactory, v -> 
					v.setExternalSources(v.getPrevious().getExternalSourceFiles()))))
			.replaceStage(CHECK, NOP) //FIXME has dangling references
			.replaceStage(RUN, chain(inputFiles.stream()
					.map(i -> forEach(defaultExecutor(v -> asList(i.toString(), getOutputFile(i, v)))))
					.toArray(ITestStage[]::new)))
					
//			.replaceStage(VERIFY, Stages.forEachPairWithFirst(comparator))
			;
		
	}

	protected void transform(GSProjectVersion version) {
		GecosProject gProject = version.getProject();
		IdfixProject idfixProj = IDFixUserIDFixProjectFactory.IDFIXPROJECT(gProject, IDFIX_TMP_OUTPUT, new File(IDFIX_CONFIG_FILE).toPath());
		UserConfiguration config = idfixProj.getUserConfiguration();
		config.setDataTypeRegneration(codegenType);
		Float2FixConversion fixConvertor = new Float2FixConversion(idfixProj, String.valueOf(accConstraint), OP_LIBRARY, wloAlgo , 0);
		gProject = fixConvertor.compute();
		version.setProject(gProject);
		
		//FIXME
		String procName = data.getName().replace(".group", ""); //XXX requires the procedure to have the same name as the app root dir !!!
		Path outDir = this.getTestMethodOutputDir().resolve(version.getName());
		outDir.toFile().mkdirs();
		new ExtractProcedureArgumentTypes(gProject, procName, outDir.toString(), "float").compute();
	}
	
	@Ignore //disabled because idfix prompt for input configuration ..
	@Test
	@UseDataProvider(location = DataFromPathProvider.class, value = DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation("resources/src-c/fir.group")
	public void testF2Fx_CinlineCodegen(ICxxProjectData d) {
//		DefaultOptimization.setENFORCE_FIXEDPT_CONTRAINTS(true);
//		FixedPtSpecificationApplier.CHECK_SPEC_LSBMODE = true;
		
		this.codegenType = DATA_TYPE_REGENERATION.CINLINE;
		runTest(d, this::transform);
	}
	
//	@Ignore
//	@Test
//	@UseDataProvider(location = FDP_app_idfix.class, value = FDP_app_idfix.PROVIDER_NAME)
//	public void testF2Fx_ACFixedCodegen(IData d) {
////		DefaultOptimization.setENFORCE_FIXEDPT_CONTRAINTS(false);
////		FixedPtSpecificationApplier.CHECK_SPEC_LSBMODE = false;
//		
//		codegenType = DATA_TYPE_REGENERATION.AC_FIXED;
//		runTest(d, this::transform);
//	}
//	
	
	
//	@Override
//	protected String provideMakeCompileTarget(GSProjectVersion v) {
//		String cmd = super.provideMakeCompileTarget(v);
//		switch (codegenType) {
//		case AC_FIXED:
//			File acIncludesDir = new ResourceLocator("fr.irisa.cairn.gecos.model.includes.std", "").locate("includes/ac_datatypes2.0");
//			cmd += "CC=g++  INC_DIRS="+acIncludesDir.getPath();
//			break;
//		case SC_FIXED:
//			cmd += "CC=g++  INC_DIRS="; //TODO
//			break;
//		default:
//			break;
//		}
//		return cmd;
//	}
	
	
	
	private double[] loadData(Path file) {
		try {
			List<String> lines = Files.readAllLines(file);
			
			int nbDim = Integer.parseInt(lines.get(0).trim().split(" | ")[0]);
			int[] dimSizes = new int[nbDim]; // outermost first
			int totalSize = 1;
			int i = 1; for(; i < nbDim; ++i) {
				dimSizes[i] = Integer.valueOf(lines.get(i).trim().split(" ")[0]);
				totalSize *= dimSizes[i];
			}
			
			double[] values = new double[totalSize];
			for(; i < totalSize; ++i) {
				values[i] = Double.valueOf(lines.get(i));
			}
			
			return values;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
		
	}
	
}
