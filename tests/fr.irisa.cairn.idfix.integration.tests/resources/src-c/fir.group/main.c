#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "defs.h"

#define MACRO_VAL(macro) 	#macro
#define TYPE2STR(type)		MACRO_VAL(type)
#define PRINT_ID(type)		(strcmp(TYPE2STR(type), "float")? "%d" : "%f")


extern void fir(TYPE_0 xn[], TYPE_1 yn[]);


int main(int argc, const char **argv) {
	int i,j,n;
	char tmp[128];
	FILE *outfile, *infile;
	int size, nDims;
	unsigned long long time0, time1;

	TYPE_0 *X;//Nc-1+N
	TYPE_1 *out;//N


	if(argc < 3) {
		fprintf(stderr, "Usage: %s input_file output_file\n", argv[0]);
		exit(-1);
	}

	infile = fopen(argv[1], "r");
	if(infile == NULL) {
		fprintf(stderr, "couldn't open file \"%s\"!\n", argv[1]);
		exit(-2);
	}

	outfile = fopen(argv[2], "w");
	if(outfile == NULL) {
		fprintf(stderr, "couldn't open file \"%s\"!\n", argv[2]);
		exit(-2);
	}

	/* init data */
	fgets(tmp, 128, infile);
	nDims = atoi(tmp);
	if(nDims != 1) {
		fprintf(stderr, "nb of dimensions in '%s' (%d) is different from 1\n", argv[1], nDims);
		exit(-3);
	}

	fgets(tmp, 128, infile);
	size = atoi(tmp);
	if(size < 0) {
		fprintf(stderr, "nb of samples %d in '%s' is not valid\n", size, argv[1]);
		exit(-3);
	}

	X    = (TYPE_0 *) malloc(size * sizeof(TYPE_0));
	out  = (TYPE_1 *) malloc(size * sizeof(TYPE_1));

	for(j=0; j<size; j++) {
		fgets(tmp, 128, infile);
		X[j] = (TYPE_0) (strcmp(TYPE2STR(TYPE_0), "float")? strtol(tmp, NULL, 10) : atof(tmp));

		out[j] = 0;
	}
	fclose(infile);


//	for(j=NC-1; j<N+NC-1; j++)
//		printf("%.1f ", X[j]);
//	printf("\n\n");


	/* 2- call fir() */
	for(n=0; n<size; n++) {
		fir(&X[n], &out[n]);
	}


	/* output result */
	fprintf(outfile, "1\n"); //number of dimensions
	fprintf(outfile, "%d\n", size);
	for (j = 0; j < size; j++) {
		fprintf(outfile, PRINT_ID(TYPE_1), out[j]);
		fprintf(outfile, "\n");
	}
	fclose(outfile);

	free(X);
	free(out);

	return 0;
}
